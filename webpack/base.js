const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const env = require('dotenv').config()

const appConfig = {
  project_name: env.parsed.PROJECT_NAME || 'mitraaca-web-revamp',
  node_env: env.parsed.NODE_ENV || 'development',
  auth_cookie_name: env.parsed.AUTH_COOKIE_NAME || 'mai',
  api_url: env.parsed.API_USER || '',
  api_url_content: env.parsed.API_CONTENT || '',
  api_url_payment: env.parsed.API_PAYMENT || '',
  api_key: env.parsed.API_KEY || '',
  URL_PAYMENT_BCA: process.env.URL_PAYMENT_BCA || 'http://149.129.248.88/PGW/Payment/Payment/Payment',
  googleAPIs: env.parsed.GOOGLE_API_KEY || '',
  recaptcha: env.parsed.RECAPTCHA_KEY || '',
  port: env.parsed.PORT || 8000,
}

const postcssLoaderOptions = {
  plugins() {
    return [autoprefixer]
  },
}

const config = {
  cache: true,
  mode: 'none',
  entry: {},
  output: {
    path: path.join(__dirname, '..', 'public'),
    filename: (appConfig.node_env === 'production' ? 'javascripts/[name]-[hash].js' : 'javascripts/[name].js'),
    sourceMapFilename: (appConfig.node_env === 'production' ? 'javascripts/[name]-[hash].js.map' : 'javascripts/[name].js.map'),
  },
  optimization: {
    minimize: (appConfig.node_env === 'production'),
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }, // translates CSS into CommonJS
          {
            loader: 'less-loader', // compiles Less to CSS
            options: {
              lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                modifyVars: {
                  'primary-color': '#2b57b7',
                  'secondary-color': '#95a5a6',
                  'link-color': '#1890ff',
                  'normal-color': '#2c2e30',
                  'border-radius-base': '10px',
                },
                javascriptEnabled: true,
              },
            },
          },
        ],
      },
      {
        test: /\.js$/,
        use: ['babel-loader?cacheDirectory'],
        exclude: /node_modules/,
      },
      {
        test: /\.jsx$/,
        use: ['babel-loader?cacheDirectory'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader', options: postcssLoaderOptions },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader', options: postcssLoaderOptions },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        use: ['url-loader?limit=1000'],
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
        type: 'javascript/auto',
      },
      {
        test: /\.html$/,
        use: ['html-loader'],
      },
      {
        test: /\.(eot|ttf|woff2?)$/,
        use: ['file-loader'],
      },
      {
        test: /\.yml$/,
        use: ['yml-loader'],
      },
      {
        test: /\.svg$/,
        loader: 'raw-loader',
      },
    ],
  },
  resolve: {
    modules: [
      path.join(__dirname, '..', 'app'),
      path.join(__dirname, '..'),
      'node_modules',
    ],
    extensions: ['.jsx', '.js'],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(appConfig.node_env),
        APP_CONFIG: JSON.stringify(appConfig),
      },
    }),
    new webpack.ProvidePlugin({ React: 'react' }),
    new CopyWebpackPlugin([
      { from: `app/assets/images/favicon-${appConfig.project_name}.png`, to: 'favicon.png' },
      { from: 'app/assets/fonts', to: 'assets/fonts' },
      { from: 'app/assets/images', to: 'assets' },
    ]),
  ],
  devtool: process.env.DEVTOOL || 'cheap-module-eval-source-map',
}

module.exports = { config, appConfig }
