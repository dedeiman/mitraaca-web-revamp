run_setup:
	cp .env.staging .env
	yarn install

run_local:
	yarn build:vendor && yarn build:dev
	node server.js

run_docker:
	docker build --tag mitraaca-web:dev .
	docker run --name mitraacawebserver -d -p 3000:3000 mitraaca-web:dev
