import {
  ANNOUNCEMENT_REQUEST,
  ANNOUNCEMENT_SUCCESS,
  ANNOUNCEMENT_FAILURE,
  ANNOUNCEMENT_UPDATED,
  ANNOUNCEMENT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataAnnouncement: [],
  detailAnnouncement: {},
  metaAnnouncement: {
    total_count: 0,
    current_page: 0,
  },
}

export default function announcement(state = initialState, action) {
  switch (action.type) {
    case ANNOUNCEMENT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataAnnouncement: action.data,
        metaAnnouncement: action.meta,
      }
    case ANNOUNCEMENT_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case ANNOUNCEMENT_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataAnnouncement: state.dataAnnouncement.filter(item => item.id !== action.data.id),
      }
    case ANNOUNCEMENT_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailAnnouncement: action.data,
      }
    default:
      return state
  }
}
