import {
  CREATE_IO_LIABILITY_REQUEST,
  CREATE_IO_LIABILITY_SUCCESS,
  CREATE_IO_LIABILITY_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  data: [],
  detail: {},
  meta: {
    total_count: 0,
    current_page: 0,
  },
}

export default function createIOLiability(state = initialState, action) {
  switch (action.type) {
    case CREATE_IO_LIABILITY_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CREATE_IO_LIABILITY_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        data: action.data,
        meta: action.meta,
      }
    case CREATE_IO_LIABILITY_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    default:
      return state
  }
}
