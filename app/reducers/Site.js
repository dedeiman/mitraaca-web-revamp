import {
  SET_SITE_THEME,
  SET_SITE_CONFIGURATION,
} from 'constants/ActionTypes'

const initialState = {
  light: false,
  titlePage: 'Dasboard',
  activePage: 'dashboard',
  activeSubPage: 'dashboard',
  breadList: ['Home'],
  isActiveSidebar: true,
}

export default function site(state = initialState, action) {
  switch (action.type) {
    case SET_SITE_THEME:
      return {
        ...state,
        light: action.theme,
      }
    case SET_SITE_CONFIGURATION:
      return {
        ...state,
        [action.dataKey]: action.dataValue,
      }
    default:
      return state
  }
}
