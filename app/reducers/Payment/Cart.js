import {
  LIST_CART_REQUEST,
  LIST_CART_SUCCESS,
  LIST_CART_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: true,
  metaCart: {
    total_count: 0,
    current_page: 0,
  },
}

export default function listCart(state = initialState, action) {
  switch (action.type) {
    case LIST_CART_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case LIST_CART_SUCCESS:
      return {
        ...state,
        isFetching: false,
        metaCart: action.metaCart,
        listCart: action.listCart,
      }
    case LIST_CART_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    default:
      return state
  }
}
