
import { LIST_CHECKOUT_FAILURE, LIST_CHECKOUT_REQUEST, LIST_CHECKOUT_SUCCESS } from '../../constants/ActionTypes'

const initialState = {
  isFetching: false,
}

export default function listCheckout(state = initialState, action) {
  switch (action.type) {
    case LIST_CHECKOUT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case LIST_CHECKOUT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        listCart: action.data,
      }
    case LIST_CHECKOUT_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    default:
      return state
  }
}
