import {
  CONTEST_REQUEST,
  CONTEST_SUCCESS,
  CONTEST_FAILURE,
  CONTEST_UPDATED,
  CONTEST_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataContest: [],
  detailContest: {},
  metaContest: {
    total_count: 0,
    current_page: 0,
  },
}

export default function contest(state = initialState, action) {
  switch (action.type) {
    case CONTEST_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CONTEST_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataContest: action.data,
        metaContest: action.meta,
      }
    case CONTEST_FAILURE:
      return {
        ...state,
        dataContest: [],
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case CONTEST_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataContest: state.dataContest.filter(item => item.id !== action.data.id),
      }
    case CONTEST_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailContest: action.data,
      }
    default:
      return state
  }
}
