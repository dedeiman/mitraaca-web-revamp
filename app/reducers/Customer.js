import {
  CUSTOMER_REQUEST,
  CUSTOMER_SUCCESS,
  CUSTOMER_FAILURE,
  CUSTOMER_UPDATED,
  CUSTOMER_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataCustomer: [],
  detailCustomer: {},
  metaCustomer: {
    total_count: 0,
    current_page: 0,
  },
}

export default function customer(state = initialState, action) {
  switch (action.type) {
    case CUSTOMER_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CUSTOMER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataCustomer: action.data,
        metaCustomer: action.meta,
      }
    case CUSTOMER_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case CUSTOMER_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataCustomer: state.dataCustomer.filter(item => item.id !== action.data.id),
      }
    case CUSTOMER_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailCustomer: action.data,
      }
    default:
      return state
  }
}
