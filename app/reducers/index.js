import { combineReducers } from 'redux'
import site from 'reducers/Site'
import role from 'reducers/Role'
import auth from 'reducers/Auth'
import faq from 'reducers/Faq'
import announcement from 'reducers/Announcement'
import materi from 'reducers/Materi'
import contest from 'reducers/Contest'
import cargoType from 'reducers/CargoType'
import product from 'reducers/Product'
import reportProduct from 'reducers/Report/Product'
import customer from 'reducers/Customer'
import user from 'reducers/User'
import agent from 'reducers/Agent'
import createIOLiability from 'reducers/CreateIOLiability'
import createIOProperty from 'reducers/CreateIOProperty'
import CreateIOCargo from 'reducers/CreateIOCargo'
import CreateSPPACargo from 'reducers/CreateSPPACargo'
import CreateSPPAAsri from 'reducers/CreateSPPAAsri'
import contestWinner from 'reducers/Report/ContestWinner'
import agentList from 'reducers/Report/AgentList'
import approvalReport from 'reducers/Report/Approval'
import sppaReport from 'reducers/Report/SPPA'
import trainingReport from 'reducers/Report/Training'
import commisionReport from 'reducers/Report/DetailCommision'
import expiredReport from 'reducers/Report/ExpiredPolicy'
import outstandingClaimReport from 'reducers/Report/OutstandingClaim'
import restrictedProduct from 'reducers/Report/RestrictedProduct'
import createSPPALiability from 'reducers/CreateSPPALiability'
import training from 'reducers/Training'
import cart from 'reducers/Payment/Cart'
import listCheckout from 'reducers/Payment/Checkout'

export default combineReducers({
  site,
  role,
  auth,
  faq,
  announcement,
  materi,
  contest,
  customer,
  user,
  agent,
  product,
  reportProduct,
  createIOLiability,
  createIOProperty,
  CreateIOCargo,
  CreateSPPACargo,
  CreateSPPAAsri,
  cargoType,
  contestWinner,
  restrictedProduct,
  createSPPALiability,
  training,
  trainingReport,
  cart,
  listCheckout,
  agentList,
  approvalReport,
  sppaReport,
  commisionReport,
  expiredReport,
  outstandingClaimReport,
})
