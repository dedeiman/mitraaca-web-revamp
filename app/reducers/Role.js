import {
  USER_ROLE,
} from 'constants/ActionTypes'

const initialState = {
  group: {},
}

export default function role(state = initialState, action) {
  switch (action.type) {
    case USER_ROLE:
      return {
        ...state,
        ...action.role,
      }
    default:
      return state
  }
}
