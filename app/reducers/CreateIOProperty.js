import {
  CREATE_IO_PROPERTY_REQUEST,
  CREATE_IO_PROPERTY_SUCCESS,
  CREATE_IO_PROPERTY_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  data: [],
  detail: {},
  meta: {
    total_count: 0,
    current_page: 0,
  },
}

export default function createIOProperty(state = initialState, action) {
  switch (action.type) {
    case CREATE_IO_PROPERTY_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CREATE_IO_PROPERTY_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        data: action.data,
        meta: action.meta,
      }
    case CREATE_IO_PROPERTY_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    default:
      return state
  }
}
