import {
  CARGO_TYPE_REQUEST,
  CARGO_TYPE_SUCCESS,
  CARGO_TYPE_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  data: [],
  detail: {},
  meta: {
    total_count: 0,
    current_page: 0,
  },
}

export default function materi(state = initialState, action) {
  switch (action.type) {
    case CARGO_TYPE_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CARGO_TYPE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        data: action.data,
        meta: action.meta,
      }
    case CARGO_TYPE_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    default:
      return state
  }
}
