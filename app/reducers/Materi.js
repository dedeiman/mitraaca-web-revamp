import {
  MATERI_REQUEST,
  MATERI_SUCCESS,
  MATERI_FAILURE,
  MATERI_UPDATED,
  MATERI_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataMateri: [],
  detailMateri: {},
  metaMateri: {
    total_count: 0,
    current_page: 0,
  },
}

export default function materi(state = initialState, action) {
  switch (action.type) {
    case MATERI_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case MATERI_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataMateri: action.data,
        metaMateri: action.meta,
      }
    case MATERI_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case MATERI_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataMateri: state.dataMateri.filter(item => item.id !== action.data.id),
      }
    case MATERI_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailMateri: action.data,
      }
    default:
      return state
  }
}
