import {
  USER_REQUEST,
  USER_SUCCESS,
  USER_FAILURE,
  USER_UPDATED,
  USER_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  detailProfile: {},
}

export default function user(state = initialState, action) {
  switch (action.type) {
    case USER_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataUser: action.data,
      }
    case USER_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case USER_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
      }
    case USER_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailProfile: action.data,
      }
    default:
      return state
  }
}
