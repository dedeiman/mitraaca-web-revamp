import {
  AGENT_REQUEST,
  AGENT_SUCCESS,
  AGENT_FAILURE,
  AGENT_UPDATED,
  AGENT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataAgent: [],
  detailAgent: {},
  metaAgent: {
    total_count: 0,
    current_page: 0,
  },
}

export default function agent(state = initialState, action) {
  switch (action.type) {
    case AGENT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case AGENT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataAgent: action.data,
        metaAgent: action.meta,
      }
    case AGENT_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case AGENT_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataAgent: state.dataAgent.filter(item => item.id !== action.data.id),
      }
    case AGENT_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailAgent: action.data,
      }
    default:
      return state
  }
}
