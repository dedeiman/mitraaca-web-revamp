import {
  AUTHENTICATE_USER_REQUEST,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_FAILURE,
  UPDATE_AUTH_CURRENT_USER,
  SUCCESS_CHANGE_PASSWORD,
} from 'constants/ActionTypes'

const initialState = {
  isAuthenticating: false,
  currentUser: {},
}

export default function auth(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATE_USER_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
      }
    case AUTHENTICATE_USER_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        currentUser: action.currentUser,
      }
    case AUTHENTICATE_USER_FAILURE:
      return {
        ...state,
        isAuthenticating: false,
        errorMessage: action.errorMessage,
      }
    case UPDATE_AUTH_CURRENT_USER:
      return {
        ...state,
        currentUser: action.currentUser,
      }
    case SUCCESS_CHANGE_PASSWORD: {
      const { validations } = state.currentUser
      const idxPassword = (validations || []).findIndex(item => item.name === 'change_password')
      const idxAlternativePassword = (validations || []).findIndex(item => item.name === 'change_alternative_password')
      if (idxPassword >= 0) validations[idxPassword].is_validate = true
      if (idxAlternativePassword >= 0) validations[idxAlternativePassword].is_validate = true

      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          validations,
        },
      }
    }
    default:
      return state
  }
}
