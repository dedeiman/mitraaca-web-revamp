import {
  PRODUCT_REQUEST,
  PRODUCT_SUCCESS,
  PRODUCT_FAILURE,
  PRODUCT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  data: [],
  detail: {},
  meta: {
    total_count: 0,
    current_page: 0,
  },
}

export default function product(state = initialState, action) {
  switch (action.type) {
    case PRODUCT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case PRODUCT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        data: action.data,
        meta: action.meta,
      }
    case PRODUCT_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case PRODUCT_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detail: action.data,
      }
    default:
      return state
  }
}
