const initialState = {
  isPreview: false,
}

export default function CreateSPPALiability(state = initialState, action) {
  switch (action.type) {
    default:
      return {
        ...state,
        isPreview: action.isPreviewMode,
      }
  }
}
