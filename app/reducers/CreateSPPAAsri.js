import {
  CREATE_SPPA_ASRI_REQUEST,
  CREATE_SPPA_ASRI_SUCCESS,
  CREATE_SPPA_ASRI_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  data: [],
  detail: {},
  meta: {
    total_count: 0,
    current_page: 0,
  },
}

export default function createSPPAAsri(state = initialState, action) {
  switch (action.type) {
    case CREATE_SPPA_ASRI_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case CREATE_SPPA_ASRI_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        data: action.data,
        meta: action.meta,
      }
    case CREATE_SPPA_ASRI_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    default:
      return state
  }
}
