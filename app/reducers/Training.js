import {
  TRAINING_CLASS_REQUEST,
  TRAINING_CLASS_SUCCESS,
  TRAINING_CLASS_FAILURE,
  TRAINING_CLASS_UPDATED,
  TRAINING_CLASS_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataTraining: [],
  detailTraining: {},
  metaTraining: {
    total_count: 0,
    current_page: 0,
  },
}

export default function training(state = initialState, action) {
  switch (action.type) {
    case TRAINING_CLASS_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case TRAINING_CLASS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataTraining: action.data,
        metaTraining: action.meta,
      }
    case TRAINING_CLASS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
        errorObject: action.errorObject,
      }
    case TRAINING_CLASS_UPDATED:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        dataTraining: state.dataTraining.filter(item => item.id !== action.data.id),
      }
    case TRAINING_CLASS_DETAIL_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        isFetching: false,
        detailTraining: action.data,
      }
    default:
      return state
  }
}
