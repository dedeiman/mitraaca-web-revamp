import axios from 'axios'
import config from 'app/config'
import Browser from 'utils/Browser'
import Swal from 'sweetalert2'
import { getAccessToken, removeToken } from 'actions/Auth'
import { mainPersistConfig } from 'store/configureStore'
import { purgeStoredState } from 'redux-persist'

const apiBaseURL = config.api_url

axios.defaults.baseURL = apiBaseURL
axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'

axios.interceptors.request.use(
  (response) => {
    if (!response.headers.Authorization) {
      const token = getAccessToken()

      if (token) {
        response.headers.Authorization = token
      }
    }

    return response
  },
  error => Promise.reject(error),
)

axios.interceptors.response.use(
  (response) => {
    if (response.data.meta) {
      if (response.data.meta.message === 'Token signature is expired') {
        purgeStoredState(mainPersistConfig).then(() => {
          removeToken()
          Browser.setWindowHref('/')
        })
      }
    }

    return response
  },
  (error) => {
    if (error.response) {
      const { data } = error.response
      if (data.meta && data.meta.message.includes('not authorized')) {
        Swal.fire(data.meta.message, '', 'error').then(() => (
          purgeStoredState(mainPersistConfig).then(() => {
            removeToken()
            Browser.setWindowHref('/')
          })
        ))
      }
      if (data.meta) {
        return Promise.reject(data.meta)
      }
    }
    return Promise.reject(error)
  },
)

export default class API {
  static get(path, options = {}) {
    return axios.get(
      path, options,
    )
  }

  static post(path, data = {}, options = {}) {
    return axios.post(
      path, data, { ...options },
    )
  }

  static put(path, data = {}, options = {}) {
    return axios.put(
      path, data, { ...options },
    )
  }

  static delete(path, data = {}, options = {}) {
    return axios.delete(
      path, { data }, { ...options },
    )
  }
}
