import {
  AGENT_CODE,
  AGENCY_CODE,
  PO_CODE,
  BRANCH_CODE,
} from 'constants/ActionTypes'

const PermissionPage = (type, value) => {
  const page = {
    // SECTION PAGE AGENT
    agentList: [BRANCH_CODE, AGENCY_CODE],
    detailAgent: [BRANCH_CODE, AGENCY_CODE],
    levelTreeAgent: [BRANCH_CODE, AGENCY_CODE],
    formAgent: [AGENCY_CODE],

    // SECTION PAGE APPROVAL
    approvalList: [BRANCH_CODE, PO_CODE],
    approvalHistory: [BRANCH_CODE, PO_CODE],
    approvalPolicy: [BRANCH_CODE, PO_CODE],

    // SECTION PAGE CONTENT
    formContent: [AGENT_CODE, AGENCY_CODE],

    // SECTION PAGE CONTEST
    contestList: [AGENT_CODE, AGENCY_CODE],
    contestDetail: [AGENT_CODE],
    contestForm: [AGENCY_CODE],
    contestUpload: [AGENCY_CODE],

    // SECTION PAGE CUSTOMERS
    customersList: [AGENT_CODE, AGENCY_CODE],
    customersDetail: [AGENT_CODE, AGENCY_CODE],
    customersForm: [AGENT_CODE, AGENCY_CODE],

    // SECTION PAGE FAQ
    faqForm: [AGENT_CODE],

    // SECTION PAGE PAYMENT
    paymentCart: [AGENT_CODE],
    paymentLogin: [AGENT_CODE],
    paymentHistory: [AGENT_CODE],
    paymentCheckout: [AGENT_CODE],
    paymentStatus: [AGENT_CODE],

    // SECTION PAGE PRINT POLICIES
    printPoliciesList: [BRANCH_CODE],

    // SECTION PAGE PRODUCTION
    productionAll: [PO_CODE, BRANCH_CODE, AGENCY_CODE],

    // SECTION PAGE PROFILE
    profileDetail: [AGENT_CODE],
    profileCareer: [AGENT_CODE],

    // SECTION PAGE REPORT
    reportAgentList: [AGENT_CODE, PO_CODE],
    reportContestWinner: [AGENT_CODE, PO_CODE, BRANCH_CODE],
    reportRestrictedProduct: [AGENT_CODE, PO_CODE, BRANCH_CODE],
    reportTraining: [AGENT_CODE, PO_CODE, BRANCH_CODE],
    reportApproval: [AGENT_CODE, BRANCH_CODE],
    reportSPPA: [AGENT_CODE, BRANCH_CODE],
    reportCommision: [PO_CODE, BRANCH_CODE],
    reportLossBusiness: [PO_CODE, BRANCH_CODE, AGENT_CODE],
    reportCareer: [PO_CODE, BRANCH_CODE, AGENT_CODE],
    reportBenefitYearly: [PO_CODE, BRANCH_CODE, AGENT_CODE],
    reportBenefitMonthly: [PO_CODE, BRANCH_CODE, AGENT_CODE],
    reportFinanceCommission: [PO_CODE, BRANCH_CODE, AGENT_CODE],
    reportProduction: [AGENT_CODE],
    reportClaimSettle: [AGENT_CODE, PO_CODE, BRANCH_CODE],
    reportOutstandingClaim: [AGENT_CODE, PO_CODE, BRANCH_CODE],
    reportExpiredPolicy: [AGENT_CODE],

    // SECTION PAGE TRAINING
    trainingDetail: [AGENT_CODE],
    trainingForm: [AGENCY_CODE],
    trainingUpload: [AGENCY_CODE],
  }

  return page[type].includes(value)
}

const PermissionButton = (type, value) => {
  const button = {
    // SECTION PROFILE FORGOT SECONDARY PASSWORD
    forgotSecondPass: [PO_CODE, BRANCH_CODE, AGENCY_CODE],
  }

  return button[type].includes(value)
}

export { PermissionPage, PermissionButton }
