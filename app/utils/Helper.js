/* eslint-disable react/jsx-closing-tag-location */
import {
  message, Card, Popconfirm,
  Typography, Avatar, Menu,
} from 'antd'
import { Link } from 'react-router-dom'
import React from 'react'
import Swal from 'sweetalert2'
import { TreeNode } from 'react-organizational-chart'
import { isEmpty, capitalize } from 'lodash'
import Browser from 'utils/Browser'
import { getAccessToken, removeToken } from 'actions/Auth'
import { mainPersistConfig } from 'store/configureStore'
import { purgeStoredState } from 'redux-persist'
import history from './history'

const { SubMenu } = Menu

export default class Helper {
  static getValue(value) {
    if (!value) return '-'
    return value
  }

  static getBase64(img, callback) {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  static beforeUpload(file) {
    const isFileType = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isFileType) {
      message.error('You can only upload JPG/PNG file!')
    }

    return false
  }

  static sessionTimeout() {
    const accessToken = getAccessToken()

    if (accessToken) {
      setTimeout(() => {
        Swal.fire({
          title: 'Session expired',
          text: 'Your will get time out. You want stay?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#2b57b7',
          cancelButtonColor: '#6E7D88',
          confirmButtonText: 'Yes, Logout!',
        }).then((result) => {
          if (result.isConfirmed) {
            purgeStoredState(mainPersistConfig).then(() => {
              removeToken()
              Browser.setWindowHref('/')
            })
          }
        })
      }, 1000 * 60 * 60 * 24)
    }
  }

  static fieldRules(rules = [], name) {
    const formRule = []
    rules.forEach((item) => {
      if (item === 'email') formRule.push({ type: 'email', message: `*${name} tidak valid\n` })
      if (item === 'number') formRule.push({ type: 'number', transform: value => (value ? Number(value) : ''), message: `*${name} harus menggunakan angka\n` })
      if (item === 'positiveNumber') {
        formRule.push({
          type: 'number', min: 0, transform: value => (value ? Number(value) : ''), message: `*${name} harus menggunakan angka positif\n`,
        })
      }
      if (item === 'required') formRule.push({ required: true, message: `*${name} is required\n` })
      if (item === 'requiredDropdown') formRule.push({ required: true, message: '*Wajib dipilih\n' })
      if (item === 'oneUpperCase') formRule.push({ pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar\n' })
      if (item === 'oneLowerCase') formRule.push({ pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil\n' })
      if (item === 'oneNumber') formRule.push({ pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka\n' })
      if (item === 'notSpecial') formRule.push({ pattern: /^[a-zA-Z., ]+$/, message: `*${name} Tidak boleh menggunakan angka dan spesial karakter\n` })
      if (item === 'bilBulat') formRule.push({ pattern: /^[0-9]+$/, message: `*${name} harus bilangan bulat\n` })
      if (item === 'alphabet') formRule.push({ pattern: /^[a-zA-Z ]+$/, message: `*${name} hanya boleh menggunakan huruf\n` })
      if (item === 'maxDiscount') {
        formRule.push({
          type: 'number', max: 100, transform: value => (value ? Number(value) : ''), message: `*${name} maksimal 100%\n`,
        })
      }
    })
    return formRule
  }

  static renderChildMenu(datas, permission) {
    const newArr = []
    for (let i = 0; i < datas.length; i += 1) {
      newArr.push(
        datas[i].item === 'menu' ? (
          (permission && permission.indexOf(datas[i].ref) > -1) && (
            <Menu.Item key={datas[i].key}>
              <Link
                to={{
                  pathname: `/${datas[i].key}`,
                  state: i,
                }}
                type="link"
                className={`px-0 btn-signout restore-${i}`}
              >
                <span className="nav-text">{datas[i].name}</span>
              </Link>
            </Menu.Item>
          )
        ) : (
          (permission && permission.indexOf(datas[i].ref) > -1) && (
            <SubMenu
              key={datas[i].key}
              className="zoom-icon"
              title={(
                <span>
                  <span className="nav-text">
                    {datas[i].name}
                  </span>
                </span>
              )}
            >
              {!isEmpty(datas[i].subMenu) ? this.renderChildMenu(datas[i].subMenu, permission) : null}
            </SubMenu>
          )
        ),
      )
    }
    return newArr
  }

  static renderChild(datas, isAgent) {
    const newArr = []
    for (let i = 0; i < datas.length; i += 1) {
      newArr.push(
        <TreeNode
          key={`tree_${Math.random()}`}
          label={
            isAgent
              ? (
                <Card className={`card-career ${datas[i].status}`}>
                  <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                    {datas[i].level_name}
                  </Typography.Paragraph>
                  <Typography.Paragraph className="name" ellipsis={{ rows: 1 }}>
                    <Avatar className="initial">{capitalize(datas[i].agent_name.replace(' ', '')[0])}</Avatar>
                    {datas[i].agent_name}
                  </Typography.Paragraph>
                  <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                    {`Branch ${datas[i].branch_name}`}
                  </Typography.Paragraph>
                </Card>
              ) : (
                <Popconfirm
                  icon={null}
                  cancelText="View Profile"
                  okText="Move This Agent to First Level"
                  overlayClassName="popconfirm-tree"
                  okButtonProps={{
                    type: 'primary',
                    size: 'large',
                    className: 'w-100 ml-0',
                  }}
                  cancelButtonProps={{
                    type: 'primary',
                    size: 'large',
                    className: 'w-100 ml-0 mb-3',
                  }}
                  onCancel={() => history.push(`/agent-search-menu/${datas[i].id}`)}
                  onConfirm={() => {
                    const { pathname } = history.location

                    if (pathname.includes('/agent-search-menu/tree')) {
                      history.push(`${history.location.pathname}?key=${datas[i].agent_id}`)
                    }
                  }}
                >
                  <Card className={`card-career ${datas[i].status}`}>
                    <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                      {datas[i].level_name}
                    </Typography.Paragraph>
                    <Typography.Paragraph className="name" ellipsis={{ rows: 1 }}>
                      <Avatar className="initial">{capitalize(datas[i].agent_name.replace(' ', '')[0])}</Avatar>
                      {datas[i].agent_name}
                    </Typography.Paragraph>
                    <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                      {`Branch ${datas[i].branch_name}`}
                    </Typography.Paragraph>
                  </Card>
                </Popconfirm>
              )
          }
        >
          {!isEmpty(datas[i].partners) ? this.renderChild(datas[i].partners, isAgent) : null}
        </TreeNode>,
      )
    }
    return newArr
  }

  static renderChildList(datas, isAgent) {
    const newArr = []
    for (let i = 0; i < datas.length; i += 1) {
      newArr.push({
        title: <Popconfirm
          icon={null}
          cancelText="View Profile"
          okText="Move This Agent to First Level"
          overlayClassName="popconfirm-tree"
          okButtonProps={{
            type: 'primary',
            size: 'large',
            className: 'w-100 ml-0',
          }}
          cancelButtonProps={{
            type: 'primary',
            size: 'large',
            className: 'w-100 ml-0 mb-3',
          }}
          onCancel={() => history.push(`/agent-search-menu/${datas[i].id}`)}
          onConfirm={() => {
            const { pathname } = history.location

            if (pathname.includes('/agent-search-menu/tree')) {
              history.push(`${history.location.pathname}?key=${datas[i].agent_id}`)
            }
          }}
        >
          <Card
            style={{
              marginBottom: '10px',
              width: '350px',
            }}
          >
            <React.Fragment key={`list_tree_${Math.random()}`}>
              <div className="d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-between align-items-center">
                  <Avatar
                    size="large"
                    className="mr-3"
                    style={{
                      fontSize: '18px',
                      backgroundColor: '#2b57b7',
                      fontFamily: 'Poppins-Bold',
                    }}
                  >
                    {capitalize((datas[i].agent_name).replace(' ', '')[0])}
                  </Avatar>
                  <div>
                    <p
                      className="title-card mb-0"
                      style={{ textAlign: 'left' }}
                    >
                      {(datas[i].agent_name || '').toUpperCase()}
                    </p>
                    <p
                      className="mb-0"
                      style={{ textAlign: 'left' }}
                    >
                      {datas[i].level_name}
                    </p>
                  </div>
                </div>
              </div>
            </React.Fragment>
          </Card>
        </Popconfirm>,
        key: `${Math.random()}`,
        children: !isEmpty(datas[i].partners) ? this.renderChildList(datas[i].partners, isAgent) : null,
      })
    }

    return newArr
  }

  static currency(angka, prefix, suffix) {
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(angka)) return ''
    const rev = parseInt(angka, 10)
      .toString()
      .split('')
      .reverse()
      .join('')

    let rev2 = ''
    for (let i = 0; i < rev.length; i += 1) {
      rev2 += rev[i]
      if ((i + 1) % 3 === 0 && i !== rev.length - 1) {
        rev2 += '.'
      }
    }

    return `${prefix || ''}${rev2
      .split('')
      .reverse()
      .join('')}${suffix || ''}`
  }

  static currencyToInt(currency) {
    return parseInt(currency.replace(/,.*|[^0-9]/g, ''), 10)
  }

  static convertBase64ToFile(image) {
    const byteString = atob(image.split(',')[1])
    const ab = new ArrayBuffer(byteString.length)
    const ia = new Uint8Array(ab)
    for (let i = 0; i < byteString.length; i += 1) {
      ia[i] = byteString.charCodeAt(i)
    }
    const newBlob = new Blob([ab], {
      type: 'image/jpeg',
    })
    return newBlob
  }


  static loginValidations(data) {
    const needChangePassword = ((data || []).find(item => item.name === 'change_password') || {}).is_validate
    // const needChangeAlternativePassword = ((data || []).find(item => item.name === 'change_alternative_password') || {}).is_validate

    if (!needChangePassword) return true

    return false
  }

  static replaceNumber(number) {
    if (number === undefined) {
      return 0
    }

    if (typeof number !== 'number') {
      const replaceRp = number.replace(/Rp./g, '')
      const valueNum = replaceRp.replace(/,/g, '')

      return Number(valueNum)
    }

    return number
  }
}
