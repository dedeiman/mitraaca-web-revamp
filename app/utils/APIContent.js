import axios from 'axios'
import config from 'app/config'
import Browser from 'utils/Browser'
import { getAccessToken, removeToken } from 'actions/Auth'
import { mainPersistConfig } from 'store/configureStore'
import { purgeStoredState } from 'redux-persist'

const instanceApi = axios.create({
  baseURL: config.api_url_content,
})

instanceApi.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'
instanceApi.defaults.headers['X-Requested-With'] = 'app-id=mitra-aca'
// instanceApi.defaults.headers['Access-Control-Allow-Origin'] = '*'
instanceApi.defaults.headers['x-api-key'] = 'Z89xN3LjZIABWRO2nLNTsbUVGYSAKcuTHFnrLWexZV99RguE49mmxlrqblH2'

instanceApi.interceptors.request.use(
  (response) => {
    if (!response.headers.Authorization) {
      const token = getAccessToken()

      if (token) {
        response.headers.Authorization = token
      }
    }

    return response
  },
  error => Promise.reject(error),
)

instanceApi.interceptors.response.use(
  (response) => {
    if (response.data.data) {
      if (response.data.data.message === 'Token signature is expired') {
        purgeStoredState(mainPersistConfig).then(() => {
          removeToken()
          Browser.setWindowHref('/')
        })
      }
    }

    return response
  },
  error => Promise.reject(error),
)

export default class API {
  static get(path, options = {}) {
    return instanceApi.get(
      path, { ...options },
    )
  }

  static post(path, data = {}, options = {}) {
    return instanceApi.post(
      path, data, { ...options },
    )
  }

  static put(path, data = {}, options = {}) {
    return instanceApi.put(
      path, data, { ...options },
    )
  }

  static delete(path, options = {}) {
    return instanceApi.delete(
      path, { ...options },
    )
  }
}
