import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  compose,
  mapProps,
  lifecycle,
} from 'recompose'
import { omit } from 'lodash'
import { authenticateByToken, clearCurrentUser } from 'actions/Auth'
import Cookies from 'js-cookie'
import config from 'app/config'

export const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  authenticateByToken: bindActionCreators(authenticateByToken, dispatch),
  clearCurrentUser: bindActionCreators(clearCurrentUser, dispatch),
})

const privateComponent = SecureComponent => compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  mapProps(
    props => omit(props, ['authenticateByToken']),
  ),
  lifecycle({
    componentDidMount() {
      if (!Cookies.get(config.auth_cookie_name)) {
        this.props.clearCurrentUser()
      }
    },
  }),
)(SecureComponent)

export default privateComponent
