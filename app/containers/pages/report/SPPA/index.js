import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchSPPAReport } from 'actions/Report/SPPA'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import moment from 'moment'
import SPPAReport from 'components/pages/report/SPPA'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import QueryString from 'query-string'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    sppaReport,
  } = state.root

  return {
    sppaReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchSPPAReport, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('optionYear', 'changeOptionYear', []),
  withState('state', 'changeState', {
    branch: '',
    keyword: '',
    agent_representative: '',
    join_from_date: moment().format('YYYY-MM-DD'),
    join_to_date: moment().add(7, 'day').format('YYYY-MM-DD'),
    format: '',
    product: '',
    cob: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list } = props
      list({
        page: page.current,
        per_page: page.pageSize,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        format: 'excel',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        status: state.status,
        product_id: state.product,
        cob_id: state.cob,
      })

      props.getData(
        { base: 'apiUser', url: `/report/insurance-letters/export?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        format: '',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        agent_keyword: state.keyword,
        branch_id: state.branch,
        branch_perwakilan_id: state.agent_representative,
        product_id: state.product,
        cob_id: state.cob,
      })
    },
    handleRepresentative: props => (id) => {
      if (id !== '') {
        props.getData(
          { base: 'apiUser', url: `/branch-perwakilan/${id}`, method: 'get' },
        ).then((res) => {
          props.changeState({
            ...props.state,
            agentRepresentLoad: false,
            agentRepresentList: res.data,
            branch: id,
          })
        }).catch((err) => {
          message.error(err)
          props.changeState({
            ...props.state,
            agentRepresentLoad: false,
            agentRepresentList: [],
            branch: id,
          })
        })
      } else {
        props.changeState({
          ...props.state,
          branch: id,
        })
      }
    },
    handleProduct: props => (id) => {
      props.getData(
        { base: 'apiUser', url: `/product/${id}`, method: 'get' },
      ).then((res) => {
        props.changeState({
          ...props.state,
          product: '',
          productLoad: false,
          productList: res.data,
          cob: id,
        })
      }).catch((err) => {
        message.error(err)
        props.changeState({
          ...props.state,
          product: '',
          productLoad: false,
          productList: [],
          cob: id,
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/class-of-business', name: 'cob' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('activeSubPage', 'report-administrator-approval')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(SPPAReport)
