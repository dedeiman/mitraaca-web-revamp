import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchProduction } from 'actions/Report/Product'
import ProductionReport from 'components/pages/report/Production/Production'
import { message } from 'antd'
import QueryString from 'query-string'
import { getDatas } from 'actions/Option'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'

export function mapStateToProps(state) {
  const {
    reportProduct,
  } = state.root

  return {
    reportProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  list: bindActionCreators(fetchProduction, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    page: '',
    cob_id: '',
    product_id: '',
    issued_to: '',
    issued_from: '',
    start_period_to: '',
    start_period_from: '',
    agent_status: '',
    policy_status: '',
    branch_id: '',
    branch_perwakilan_id: '',
    policy_number: '',
    is_detail: '',
    per_page: '',
  }),
  withHandlers({
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        format: 'excel',
        from_date: state.start_period_from,
        to_date: state.start_period_to,
        status: state.status,
        product_id: state.product_id,
        cob_id: state.cob_id,
      })

      props.getData(
        { base: 'apiUser', url: `/report/production/download?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleFilter: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: state.page,
        cob_id: state.cob_id,
        product_id: state.product_id,
        issued_to: state.issued_to,
        issued_from: state.issued_from,
        start_period_to: state.start_period_to,
        start_period_from: state.start_period_from,
        agent_status: state.agent_status,
        policy_status: state.policy_status,
        branch_id: state.branch_id,
        branch_perwakilan_id: state.branch_perwakilan_id,
        policy_number: state.policy_number,
        is_detail: state.is_detail,
        per_page: state.per_page,
      })
    },
    handleBranchPerwakilan: props => (val) => {
      props.getData(
        { base: 'apiUser', url: `/branch-perwakilan/${val}`, method: 'get' },
      ).then((res) => {
        props.changeState({
          ...props.state,
          branch_id: val,
          branchPerLoad: false,
          branchPerList: res.data,
          branch_perwakilan_id: undefined,
        })
      }).catch((err) => {
        message.error(err)
        props.changeState({
          ...props.state,
          branch_id: val,
          branchPerLoad: false,
          branchPerList: [],
          branch_perwakilan_id: undefined,
        })
      })
    },
    handleChecked: props => () => {
      const { list, state } = props

      props.changeState({
        ...props.state,
        is_detail: !props.state.is_detail,
      })

      list({
        page: 1,
        cob_id: state.cob_id,
        product_id: state.product_id,
        issued_to: state.issued_to,
        issued_from: state.issued_from,
        start_period_to: state.start_period_to,
        start_period_from: state.start_period_from,
        agent_status: state.agent_status,
        policy_status: state.policy_status,
        branch_id: state.branch_id,
        branch_perwakilan_id: state.branch_perwakilan_id,
        policy_number: state.policy_number,
        is_detail: !state.is_detail,
        per_page: state.per_page,
      })
    },
    handleTableChange: props => (page) => {
      const { list, state } = props

      list({
        page: page.current,
        cob_id: state.cob_id,
        product_id: state.product_id,
        issued_to: state.issued_to,
        issued_from: state.issued_from,
        start_period_to: state.start_period_to,
        start_period_from: state.start_period_from,
        agent_status: state.agent_status,
        policy_status: state.policy_status,
        branch_id: state.branch_id,
        branch_perwakilan_id: state.branch_perwakilan_id,
        policy_number: state.policy_number,
        is_detail: state.is_detail,
        per_page: state.per_page,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'report-production-list')
      const { list } = this.props
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/class-of-business', name: 'cob' },
        { url: '/products?purpose=select', name: 'product' },
        { url: '/agent-change-reasons', name: 'agentStatus' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(ProductionReport)
