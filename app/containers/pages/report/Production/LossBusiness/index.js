import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import LossBusiness from 'components/pages/report/Production/LossBusiness'
import { fetchReporLossBusinesstList, fetchDownloadReportLossBusiness } from 'actions/Report/LossBusiness'
import { getBranches, getRepresentativeBranch, getProducts, getCOB } from 'actions/Option'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import qs from 'query-string'
import moment from 'moment'
import { pickBy, identity, first, last } from 'lodash'

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReporLossBusinesstList: bindActionCreators(fetchReporLossBusinesstList, dispatch),
  fetchDownloadReportLossBusiness: bindActionCreators(fetchDownloadReportLossBusiness, dispatch),
  getRepresentativeBranch: bindActionCreators(getRepresentativeBranch, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
  getProducts: bindActionCreators(getProducts, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch)
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withState('dataList', 'setDataList', {}),
  withState('branchList', 'setBranchList', []),
  withState('cobList', 'setCobList', []),
  withState('productList', 'setProductList', []),
  withState('representativeBranchList', 'setRepresentativeBranchList', []),
  withState('isLoading', 'setIsLoading', true),
  withState('state', 'changeState', {
    page: '1',
    per_page: '10',
    format: 'excel',
    start_date: '',
    end_date: '',
    agent_keyword: '',
    branch_id: '',
    branch_perwakilan_id: '',
    product_id: '',
    cob_id: '',
    policy_no: ''
  }),
  withHandlers({
    getList: props => async (params) => {
      const { fetchReporLossBusinesstList, setDataList, setIsLoading } = props
      try {
        const res = await fetchReporLossBusinesstList(params)
        setDataList(res)
        setIsLoading(false)
      } catch (err) {
        setDataList()
        setIsLoading(false)
      }
    },
    handleGetBranches: props => async (params) => {
      const { setBranchList, getBranches, setIsLoading } = props
      try {
        const res = await getBranches(params)
        setBranchList(res)
        setIsLoading(false)
      } catch (err) {
        setBranchList([])
        setIsLoading(false)
      }
    },
    handleGetCob: props => async (params) => {
      const { setCobList, getCOB, setIsLoading } = props
      try {
        const res = await getCOB(params)
        setCobList(res)
        setIsLoading(false)
      } catch (err) {
        setCobList([])
        setIsLoading(false)
      }
    },
    handleGetProduct: props => async (params) => {
      const { setProductList, getProducts, setIsLoading } = props
      try {
        const res = await getProducts(params)
        setProductList(res)
        setIsLoading(false)
      } catch (err) {
        setProductList([])
        setIsLoading(false)
      }
    }
  }),
  withHandlers({
    handleFilter: props => async (value, field) => {
      const { changeState, state, getRepresentativeBranch, setRepresentativeBranchList } = props
      if (field.includes('Date')) {
        if (field === 'policyDate') {
          changeState({
            ...props.state,
            start_date: moment(value[0]).format('YYYY-MM-DD'),
            end_date: moment(value[1]).format('YYYY-MM-DD')
          })
        }
      } else {
        if (field === 'branch_id') {
          try {
            const res = await getRepresentativeBranch(value)
            setRepresentativeBranchList(res)
          } catch {
            setRepresentativeBranchList()
          }
        }

        changeState({
          ...props.state,
          [field]: value
        })
      }
    },
    handleSubmitFilter: props => (params) => {
      params.preventDefault()
      const { getList, state, setDetail, setIsLoading } = props

      setIsLoading(true)
      getList(`?${qs.stringify(pickBy(state, identity))}`)
    },
    handlePage: props => (dataPage) => {
      const { getList, state } = props
      const payload = {
        ...state,
        page: dataPage
      }
      getList(`?${qs.stringify(pickBy(payload, identity))}`)
    },
    handleDownload: props => async (event, field) => {
      const { fetchDownloadReportLossBusiness, state, setIsLoading } = props
      const payload = { ...state, format: field}
      setIsLoading(true)

      try {
        const res = await fetchDownloadReportLossBusiness(`?${qs.stringify(pickBy(payload, identity))}`)
        let anchor = document.createElement('a')
        anchor.href = res.data.file_url
        anchor.target = '_blank'
        anchor.click()
        setIsLoading(false)
      } catch (err) {
        setIsLoading(false)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { updateSiteConfiguration, getList, state, handleGetBranches, handleGetCob, handleGetProduct } = this.props

      getList(`?${qs.stringify(pickBy(state, identity))}`)
      handleGetBranches()
      handleGetCob()
      handleGetProduct()

      updateSiteConfiguration('breadList', ['Home', 'Produksi', 'Loss Business'])
      updateSiteConfiguration('activeSubPage', 'reports')
      updateSiteConfiguration('activePage', 'reports/loss-business')
    },
  }),
)(LossBusiness)

