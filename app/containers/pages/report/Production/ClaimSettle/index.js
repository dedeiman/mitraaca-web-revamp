import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import ClaimSettle from 'components/pages/report/Production/ClaimSettle'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'

export function mapStateToProps(state) {
  const {
    auth,
  } = state.root

  return {
    auth,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    detail: false,
    policy_no: '',
    representative: '',
    branch: '',
    product: '',
    cob: '',
    search: '',
    date_settle_from_date: '',
    date_settle_to_date: '',
  }),
  withHandlers({
    // handleFilter: props => (e) => {
    //   e.preventDefault()
    //   const { list, state } = props

    //   list({
    //     page: 1,
    //     search: state.search,
    //     branch_id: state.branch,
    //     agent_type: state.agent_type,
    //     representative: state.representative,
    //     mob: state.mob,
    //     religion: state.religion,
    //     join_from_date: state.join_from_date,
    //     join_to_date: state.join_to_date,
    //     license_type: state.license_type,
    //     license_from_date: state.license_from_date,
    //     license_to_date: state.license_to_date,
    //   })
    // },
    // handleReport: props => () => {
    //   const { state } = props

    //   const payload = QueryString.stringify({
    //     format: 'excel',
    //     search: state.search,
    //     branch_id: state.branch,
    //     agent_type: state.agent_type,
    //     representative: state.representative,
    //     mob: state.mob,
    //     religion: state.religion,
    //     join_from_date: state.join_from_date,
    //     join_to_date: state.join_to_date,
    //     license_type: state.license_type,
    //     license_from_date: state.license_from_date,
    //     license_to_date: state.license_to_date,
    //   })

    //   props.getData(
    //     { base: 'apiUser', url: `/reports/agents/download?${payload}`, method: 'get' },
    //   ).then((res) => {
    //     window.open(
    //       res.data.file_url,
    //       '_blank',
    //     )
    //   }).catch((err) => {
    //     message.error(err.message)
    //   })
    // },
  }),
  lifecycle({
    // componentDidMount() {},
  }),
)(ClaimSettle)
