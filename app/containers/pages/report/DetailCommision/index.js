import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchCommision } from 'actions/Report/DetailCommision'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import DetailCommision from 'components/pages/report/DetailCommision'
import QueryString from 'query-string'
import config from 'config'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    commisionReport,
    auth,
  } = state.root
  return {
    auth,
    commisionReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchCommision, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    policy_number: '',
    payment_date_from: '',
    payment_date_to: '',
    periode_date_from: '',
    periode_date_to: '',
    cob_id: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list, state } = props

      list({
        page: page.current,
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        policy_number: state.policy_number,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
        policy_number: state.policy_number,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        cob_id: state.cob,
        payment_date_from: state.payment_date_from,
        payment_date_to: state.payment_date_to,
        periode_date_from: state.periode_date_from,
        periode_date_to: state.periode_date_to,
        policy_number: state.policy_number,
        format: 'excel',
      })

      props.getData(
        { base: 'apiUser', url: `/agent-commision-report/download?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
  }),
  
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/class-of-business', name: 'cob' },
      ]
     
      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('activePage', 'report-agent-commission-detail')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(DetailCommision)
