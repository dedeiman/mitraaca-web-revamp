import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import BenefitFinanceYearly from 'components/pages/report/Finance/BenefitYearly'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import { fetchReportYearlyList, fetchDownloadReportYearly } from 'actions/Report/Finance/BenefitYearly'
import { getBranches, getRepresentativeBranch } from 'actions/Option'
import qs from 'query-string'
import moment from 'moment'
import { pickBy, identity, first, last } from 'lodash'

const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReportYearlyList: bindActionCreators(fetchReportYearlyList, dispatch),
  fetchDownloadReportYearly: bindActionCreators(fetchDownloadReportYearly, dispatch),
  getRepresentativeBranch: bindActionCreators(getRepresentativeBranch, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch)
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withState('dataList', 'setDataList', {}),
  withState('isLoading', 'setIsLoading', true),
  withState('branchList', 'setBranchList', []),
  withState('representativeBranchList', 'setRepresentativeBranchList', []),
  withState('state', 'changeState', {
    start_date: '',
    end_date: '',
    claim_start_date: '',
    claim_end_date: '',
    agent_keyword: '',
    claim_ratio: '',
    branch_id: '',
    branch_perwakilan_id: '',
    agent_status: '',
    format: 'pdf',
    page: '1',
    per_page: '10'
  }),
  withHandlers({
    getList: props => async (params) => {
      const { fetchReportYearlyList, setDataList, setIsLoading } = props
      try {
        const res = await fetchReportYearlyList(params)
        setDataList(res)
        setIsLoading(false)
      } catch (err) {
        setDataList()
        setIsLoading(false)
      }
    },
    handleGetBranches: props => async (params) => {
      const { setBranchList, getBranches, setIsLoading } = props
      try {
        const res = await getBranches(params)
        setBranchList(res)
        setIsLoading(false)
      } catch (err) {
        setBranchList([])
        setIsLoading(false)
      }
    }
  }),
  withHandlers({
    handleFilter: props => async (value, field) => {
      const { changeState, state, getRepresentativeBranch, setRepresentativeBranchList } = props
      if (field.includes('Date')) {
        if (field === 'production') {
          changeState({
            ...props.state,
            start_date: moment(value[0]).format('YYYY-MM-DD'),
            end_date: moment(value[1]).format('YYYY-MM-DD')
          })
        } else {
          changeState({
            ...props.state,
            claim_start_date: moment(first(value)).format('YYYY-MM-DD'),
            claim_end_date: moment(last(value)).format('YYYY-MM-DD')
          })
        }
      } else {
        if (field === 'branch_id') {
          try {
            const res = await getRepresentativeBranch(value)
            setRepresentativeBranchList(res)
          } catch {
            setRepresentativeBranchList()
          }
        }

        changeState({
          ...props.state,
          [field]: value
        })
      }
    },
    handleSubmitFilter: props => (params) => {
      params.preventDefault()
      const { getList, state, setIsLoading } = props

      setIsLoading(true)
      getList(`?${qs.stringify(pickBy(state, identity))}`)
    },
    handlePage: props => (dataPage) => {
      const { getList, state } = props
      const payload = {
        ...state,
        page: dataPage
      }
      getList(`?${qs.stringify(pickBy(payload, identity))}`)
    },
    handleDownload: props => async (event, field) => {
      const { fetchDownloadReportYearly, state, setIsLoading } = props
      const payload = { ...state, format: field}
      setIsLoading(true)

      try {
        const res = await fetchDownloadReportYearly(`?${qs.stringify(pickBy(payload, identity))}`)
        let anchor = document.createElement('a')
        anchor.href = res.data.file_url
        anchor.target = '_blank'
        anchor.click()
        setIsLoading(false)
      } catch (err) {
        setIsLoading(false)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { getList, updateSiteConfiguration, state, handleGetBranches } = this.props

      getList(`?${qs.stringify(pickBy(state, identity))}`)
      handleGetBranches()
      updateSiteConfiguration('breadList', ['Home', 'Benefit', 'Yearly'])
      updateSiteConfiguration('activeSubPage', 'reports')
      updateSiteConfiguration('activePage', 'reports/benefit-yearly')
    }
  })
)(BenefitFinanceYearly)
