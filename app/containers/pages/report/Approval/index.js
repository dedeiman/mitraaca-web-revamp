import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchApprovalReport } from 'actions/Report/Approval'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import Approval from 'components/pages/report/Approval'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import QueryString from 'query-string'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    approvalReport,
  } = state.root

  return {
    approvalReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchApprovalReport, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('optionYear', 'changeOptionYear', []),
  withState('submitted', 'setSubmitted', false),
  withState('state', 'changeState', {
    search: '',
    branch: '',
    agent_type: '',
    level: '',
    status: '',
    mob: '',
    religion: '',
    join_from_date: '',
    join_to_date: '',
    license_type: '',
    license_from_date: '',
    license_to_date: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list } = props
      list({
        page: page.current,
        per_page: page.pageSize,
      })
    },
    handleReport: props => () => {
      const { state } = props

      props.setSubmitted(true)

      const payload = QueryString.stringify({
        format: 'excel',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        status: state.status,
        product_id: state.product,
        cob_id: state.cob,
      })

      props.getData(
        { base: 'apiUser', url: `/report/approvals/history/download?${payload}`, method: 'get' },
      ).then((res) => {
        props.setSubmitted(false)
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        format: '',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        status: state.status,
        product_id: state.product,
        cob_id: state.cob,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/products', name: 'product' },
        { url: '/class-of-business', name: 'cob' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('activeSubPage', 'report-administrator-approval')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(Approval)
