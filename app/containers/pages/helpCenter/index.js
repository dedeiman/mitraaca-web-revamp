import { connect } from 'react-redux'
import {
  compose,
  withHandlers,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import HelpCenter from 'components/pages/helpCenter'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  return {
    currentUser,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formHelpCenter' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withHandlers({
      onSubmit: props => (e) => {
        e.preventDefault()
        props.form.validateFields(async (err, values) => {
          if (!err) {
            props.getDatas(
              { base: 'apiUser', url: '/announcements', method: 'post' }, {
                ...values,
                type_announcement: 'pusat_bantuan',
                end_date: new Date(),
              },
            ).then(() => {
              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Berhasil Dikirim!</span>',
                'success',
              ).then(() => history.push('/dashboard'))
            }).catch((error) => {
              Swal.fire(
                error.message,
                '',
                'error',
              )
            })
          }
        })
      },
    }),
  )(HelpCenter),
)
