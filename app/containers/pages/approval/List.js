/* eslint-disable no-undef */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import ListApproval from 'components/pages/approval/List'
import { getDatas } from 'actions/Option'
import { pickBy, identity } from 'lodash'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const { currentUser } = state.root.auth

  return {
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateApproval', 'setStateListApproval', {
    loading: true,
    status: 'need-approval',
    search: '',
    date: undefined,
    list: [],
    page: 1,
  }),
  withHandlers({
    loadPolicy: props => (isSearch) => {
      const {
        status, search,
        page, date,
      } = props.stateApproval

      const payload = {
        tanggal_polis: date,
        status,
        search,
        page: isSearch ? 1 : page,
      }

      history.push(`/approval-list-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handlePrintPolicies: props => (id) => {
      props.getDatas(
        {
          base: 'apiUser',
          url: '/print-pending-insurance-letters',
          method: 'post',
        },
        {
          sppa_id: id,
        },
      )
    },
    handleFilter: props => (name, val) => {
      const { stateApproval, setStateListApproval, loadPolicy } = props

      switch (name) {
        case 'search':
          setStateListApproval({
            ...stateApproval,
            [`${name}`]: val,
          })
          break
        case 'status':
          setStateListApproval({
            ...stateApproval,
            [`${name}`]: val,
          })
          setTimeout(() => {
            loadPolicy(true)
          }, 300)
          break
        case 'date':
          setStateListApproval({
            ...stateApproval,
            [`${name}`]: val,
          })
          setTimeout(() => {
            loadPolicy(true)
          }, 300)
          break
        default:
      }
    },
    handlePage: props => (value) => {
      const { stateApproval, setStateListApproval, loadPolicy } = props
      setStateListApproval({
        ...stateApproval,
        page: value,
      })
      setTimeout(() => {
        loadPolicy()
      }, 300)
    },
    downloadApproval: props => () => {
      const {
        status, search,
      } = props.stateApproval

      if (search === undefined && status !== 'all') {
        props.getDatas(
          { base: 'apiUser', url: `/report/approvals/download?status=${status}&format=excel`, method: 'get' },
        ).then((res) => {
          window.open(
            res.data.file_url,
            '_blank',
          )
        }).catch((err) => {
          message.error(err)
        })
      } else if (search === undefined && status === 'all') {
        props.getDatas(
          { base: 'apiUser', url: '/report/approvals/download?format=excel', method: 'get' },
        ).then((res) => {
          window.open(
            res.data.file_url,
            '_blank',
          )
        }).catch((err) => {
          message.error(err)
        })
      } else if (search !== undefined && status === 'all') {
        props.getDatas(
          { base: 'apiUser', url: `/report/approvals/download?search=${search}&format=excel`, method: 'get' },
        ).then((res) => {
          window.open(
            res.data.file_url,
            '_blank',
          )
        }).catch((err) => {
          message.error(err)
        })
      } else {
        props.getDatas(
          { base: 'apiUser', url: `/report/approvals/download?status=${status}&search=${search}&format=excel`, method: 'get' },
        ).then((res) => {
          window.open(
            res.data.file_url,
            '_blank',
          )
        }).catch((err) => {
          message.error(err)
        })
      }
    },
    fetchApproval: props => () => {
      const newState = (qs.parse(props.location.search) || {})

      props.getDatas(
        { base: 'apiUser', url: `/approvals${props.location.search || '?status=need-approval'}`, method: 'get' },
      ).then((res) => {
        props.setStateListApproval({
          ...props.stateApproval,
          loading: false,
          list: res.data,
          search: newState.search,
          status: newState.status || 'need-approval',
          date: newState.tanggal_polis,
          page: res.meta,
        })
      }).catch(() => {
        props.setStateListApproval({
          ...props.stateApproval,
          loading: false,
          date: newState.tanggal_polis,
          search: newState.search,
          status: newState.status || 'need-approval',
          list: [],
          page: {},
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
     
      const {
        fetchApproval,
      } = this.props
      this.props.updateSiteConfiguration('activePage', 'pending-policy-list')

      window.onresize = () => {
        fetchApproval()
      }

      fetchApproval()
    },
  }),
)(ListApproval)
