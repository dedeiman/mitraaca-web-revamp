import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  lifecycle,
  withHandlers,
  withState,
} from 'recompose'
import history from 'utils/history'
import Swal from 'sweetalert2'
import { Form } from '@ant-design/compatible'
import { updateSiteConfiguration } from 'actions/Site'
import ApprovalView from 'components/pages/approval/Approval'
import { getDatas } from 'actions/Option'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default Form.create({ name: 'formApprovalList' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('detailHistory', 'setDetailHistory', {
      loading: true,
      listDetailHistory: {},
    }),
    withState('loadingApprove', 'setLoadingApprove', false),
    withHandlers({
      onSubmit: props => (value) => {
        value.preventDefault()
        props.form.validateFields(async (err, values) => {
          const payload = {
            ...values,
            sppa_id: props.match.params.id,
          }

          props.setLoadingApprove(true)

          if (!err) {
            props.getDatas(
              { base: 'apiUser', url: '/approvals', method: 'post' }, payload,
            ).then(() => {
              props.setLoadingApprove(false)

              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Approval berhasil dikirim!</span>',
                'success',
              ).then(() => {
                history.push('/approval-list-menu')
              })
            }).catch((error) => {
              props.setLoadingApprove(false)
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`, 'error')
            })
          } else {
            props.setLoadingApprove(false)

            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },

      getDetailPolices: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailHistory({
            ...props.detailHistory,
            loading: false,
            listDetailHistory: res.data,
          })
        }).catch(() => {
          props.setDetailHistory({
            ...props.detailHistory,
            loading: false,
            listDetailHistory: {},
          })
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('activePage', 'pending-policy-list')
        this.props.getDetailPolices()
      },
    }),
  )(ApprovalView),
)
