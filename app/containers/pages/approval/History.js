import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  lifecycle,
  withHandlers,
  withState,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import ApprovalHistory from 'components/pages/approval/History'
import { getDatas } from 'actions/Option'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('dataHistory', 'setDataHistory', {
    loading: true,
    listHistory: [],
  }),
  withState('detailHistory', 'setDetailHistory', {
    loading: true,
    listDetailHistory: {},
  }),
  withHandlers({
    getDetailPolices: props => () => {
      props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
      ).then((res) => {
        props.setDetailHistory({
          ...props.detailHistory,
          loading: false,
          listDetailHistory: res.data,
        })
      }).catch(() => {
        props.setDetailHistory({
          ...props.detailHistory,
          loading: false,
          listDetailHistory: {},
        })
      })
    },

    getListApprovals: props => () => {
      props.getDatas(
        { base: 'apiUser', url: `/approvals/${props.match.params.id}/history`, method: 'get' },
      ).then((res) => {
        props.setDataHistory({
          ...props.dataHistory,
          loading: false,
          listHistory: res.data,
        })
      }).catch(() => {
        props.setDataHistory({
          ...props.dataHistory,
          loading: false,
          listHistory: [],
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
     
      this.props.updateSiteConfiguration('activePage', 'pending-policy-list')
      this.props.getDetailPolices()
      this.props.getListApprovals()
    },
  }),
)(ApprovalHistory)
