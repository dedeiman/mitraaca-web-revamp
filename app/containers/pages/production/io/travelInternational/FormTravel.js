/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormTravel from 'components/pages/production/io/travelInternational/FormTravel'
import { getDatas } from 'actions/Option'
import moment from 'moment'
import history from 'utils/history'
import {
  fetchDetailProduct,
} from 'actions/Product'

import _ from 'lodash'
import Swal from 'sweetalert2'
import qs from 'query-string'
import { param } from 'jquery'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateSelects', 'setStateSelects', {
      countryLoad: false,
      countryList: [],
      travelDestinationLoad: false,
      travelDestinationList: [],
      relationshipLoad: false,
      relationshipList: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      premi_travel_calculation: [],
      admin_fee: 0,
      discount_percentage: 0,
      discount_amount: 0,
      loading_premi: 0,
      policy_printing_fee: 0,
      premi: 0,
      price_additional_week: 0,
      price_days_insured: 0,
      stamp_fee: 0,
      total_additional_week: 0,
      total_days_insured: 0,
      total_payment: 0,
    }),
    withState('exchangeRate', 'setExchangeRate', {
      idr_exchange_rate: 0,
    }),
    withState('detailInternational', 'setDetailInternational', {
      loading: true,
      list: {},
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()

        const payload = {
          product_id: Number(props.productID),
          premi_travel_international_calculation: {
            currency_id: formData.currency_id,
            insured_dob: formData.insured_dob ? moment(formData.insured_dob).format('YYYY-MM-DD') : '',
            package_type: formData.package_type,
            destination_country_ids: formData.destination_country_ids,
            is_annual: formData.is_annual,
            departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
            return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
            plan_type: formData.plan_type,
            print_policy_book: formData.print_policy_book === true,
            discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
            discount_amount: formData.discount_amount ? parseFloat(formData.discount_amount) : 0,
            print_policy_book: formData.print_policy_book === true,
          },
        }

        return props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          payload,
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_travel_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
            premi_travel_calculation: res.data.premi_travel_calculation.other_currency_premi
          })
        })
      },
    }),
    withHandlers({
      submitData: props => async () => {
        const parsed = qs.parse(window.location.search)
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const formData = values

            console.log(formData)
            const discountBy = formData.discount_amount ? 'currency' : 'percentage'
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)

            console.log(codeId)

            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)
            await props.getPremi({ discountBy, withDiscount: true }).then(async () => {
              const familyMember = []
              if (!_.isEmpty(formData.family_members)) {
                const hasDuplicate = (arrayObj, colName) => {
                  return arrayObj.some((arr) => {
                    return arr[colName] && (arrayObj[arr[colName]] || !(arrayObj[arr[colName]] = true))
                  })
                }
                const isDuplicate = hasDuplicate(formData.family_members, 'passport_number')
                const isDuplicateAhliWaris = formData.family_members.some(substring => formData.passport_number.includes(substring.passport_number))
                if (isDuplicate || isDuplicateAhliWaris) {
                  return Swal.fire(
                    '',
                    '<span style="color:#2b57b7;font-weight:bold">Terdapat duplikasi Passport Number pada data Hubungan !</span>',
                    'error',
                  )
                }
                _.forEach(formData.family_members, (item) => {
                  if (item.full_name && item.date_of_birth && item.passport_number) {
                    // eslint-disable-next-line no-param-reassign
                    item.date_of_birth = moment(item.date_of_birth).format('YYYY-MM-DD')
                    item.full_name = (item.full_name || '').toUpperCase()
                    item.full_name = item.relationship_id || ''
                    item.passport_number = (item.passport_number || '').toUpperCase()
                    familyMember.push(item)
                  }
                })
              }
              await props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: params.id ? 'put' : 'post' }, {
                product_id: Number(parsed.product_id),
                travel_international: {
                  name: formData.name,
                  phone_number: formData.phone_number,
                  phone_country_code: formData.prefix,
                  phone_country_code_id: codeId[0].id,
                  email: formData.email,
                  insured_dob: formData.insured_dob ? moment(formData.insured_dob).format('YYYY-MM-DD') : '',
                  print_policy_book: formData.print_policy_book === true,
                  currency_id: formData.currency_id,
                  information: {
                    package_type: formData.package_type,
                    passport_number: (formData.passport_number || '').toUpperCase(),
                    travel_destination_id: formData.travel_destination_id,
                    destination_country_ids: formData.destination_country_ids,
                    hometown_id: params.id ? props.detailInternational.list.travel_information.hometown.id : formData.hometown_id,
                    heir_name: (formData.heir_name || '').toUpperCase(),
                    relationship_id: formData.relationship_id,
                    is_annual: formData.is_annual === true,
                    departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
                    return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
                    plan_type: formData.plan_type,
                  },
                  family_members: formData.family_members || [],
                  discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
                  discount_amount: formData.discount_amount ? Number(formData.discount_amount) : 0,
                },
              }).then((res) => {
                if (res) {
                  Swal.fire(
                    '',
                    '<span style="color:#2b57b7;font-weight:bold">Travel IO berhasil dibuat!</span>',
                    'success',
                  ).then(() => {
                    history.push(`/production-create-io/detail/${res.data.id}?product=${Number(res.data.product.id)}`)
                  })
                }
              }).catch((error) => {
                Swal.fire(
                  '',
                  `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                  'error',
                )
              })

              props.setVisiblePreview(false)
              props.setSubmitted(false)
              props.setLoadSubmitBtn(false)
            }).catch((error) => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message || 'Mohon Periksa kembali data Anda !'}</span>`,
                'error',
              )
            })
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
      getDetailInternational: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailInternational({
            ...props.detailInternational,
            loading: false,
            list: res.data,
          })

          props.setPremi({
            admin_fee: 0,
            discount_amount: res.data.travel_premi_calculation.discount_amount,
            discount_percentage: res.data.travel_premi_calculation.discount_percentage,
            loading_premi: res.data.travel_premi_calculation.loading_premi,
            policy_printing_fee: res.data.policy_printing_fee,
            premi: res.data.travel_premi_calculation.premi,
            price_additional_week: res.data.travel_premi_calculation.price_additional_week,
            price_days_insured: res.data.travel_premi_calculation.price_days_insured,
            stamp_fee: res.data.stamp_fee,
            total_additional_week: res.data.travel_premi_calculation.total_additional_week,
            total_days_insured: res.data.travel_premi_calculation.total_days_insured,
            total_payment: res.data.total_payment,
          })
        })
      },
    }),
    lifecycle({
      componentDidMount() {
       
        const query = qs.parse(window.location.search)
        const productID = query.product_id
        const {
          match, getDetailInternational, setDetailInternational, detailInternational,
        } = this.props

        this.props.setProductID(productID)

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        const config = [
          { url: '/travel-destinations', name: 'travelDestination' },
          { url: '/relationships', name: 'relationship' },
          { url: '/countries', name: 'country' },
          { url: '/domestic-cities', name: 'domestikCity' },
          { url: '/currencies', name: 'currency' },
        ]

        config.map((item) => {
          const { url, name } = item

          return this.props.getDatas(
            { base: 'apiUser', url, method: 'get' },
          ).then((res) => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: res.data,
            })
          }).catch(() => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: [],
            })
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          getDetailInternational()
        } else {
          setDetailInternational({
            ...detailInternational,
            loading: false,
          })
        }
      },
    }),
  )(FormTravel),
)
