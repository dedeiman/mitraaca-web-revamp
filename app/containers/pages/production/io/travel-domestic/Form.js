/* eslint-disable no-case-declarations */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormTravelDomestic from 'components/pages/production/io/travel-domestic/Form'
import { getDatas } from 'actions/Option'
import moment from 'moment'
import history from 'utils/history'
import {
  fetchDetailProduct,
} from 'actions/Product'

import _ from 'lodash'
import Swal from 'sweetalert2'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateSelects', 'setStateSelects', {
      domestikCityLoad: false,
      domestikCityList: [],
      travelDestinationLoad: false,
      travelDestinationList: [],
      relationshipLoad: false,
      relationshipList: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('travelDestination', 'setTravelDestination', {}),
    withState('travelRelationship', 'setTravelRelationship', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('submitDisabled', 'setSubmitDisabled', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      admin_fee: 0,
      discount_amount: 0,
      discount_percentage: 0,
      loading_premi: 0,
      policy_printing_fee: 0,
      premi: 0,
      price_additional_week: 0,
      price_days_insured: 0,
      stamp_fee: 0,
      total_additional_week: 0,
      total_days_insured: 0,
      total_payment: 0,
    }),
    withState('detailDomestic', 'setDetailDomestic', {
      loading: true,
      list: {},
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withHandlers({
      submitData: props => async () => {
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const formData = values

            console.log(values)
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)

            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)
            props.setSubmitDisabled(true)

            await props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: params.id ? 'put' : 'post' }, {
              product_id: Number(props.productID),
              travel_domestik: {
                name: (formData.name || '').toUpperCase(),
                phone_number: formData.phone_number,
                phone_country_code: formData.prefix,
                phone_country_code_id: codeId[0].id,
                email: (formData.email || '').toUpperCase(),
                insured_dob: formData.insured_dob ? moment(formData.insured_dob).format('YYYY-MM-DD') : '',
                print_policy_book: formData.print_policy_book === true,
                information: {
                  travel_destination_id: formData.travel_destination_id,
                  destination_city_id: formData.destination_city_id,
                  heir_name: (formData.heir_name || '').toUpperCase(),
                  relationship_id: formData.relationship_id,
                  departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
                  return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
                  plan_type: formData.plan_type,
                },
                discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
                discount_amount: formData.discount_amount ? Number(formData.discount_amount) : 0,
              },
            }).then((res) => {
              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Travel IO berhasil dibuat!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`, 'error')
            })

            props.setVisiblePreview(false)
            props.setSubmitted(false)
            props.setLoadSubmitBtn(false)
            props.setSubmitDisabled(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: Number(props.productID),
            premi_travel_domestik_calculation: {
              insured_dob: formData.insured_dob ? moment(formData.insured_dob).format('YYYY-MM-DD') : '',
              departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
              return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
              plan_type: formData.plan_type,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_amount: formData.discount_amount ? parseFloat(formData.discount_amount) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_travel_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
      getDetailDomestic: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailDomestic({
            ...props.detailDomestic,
            loading: false,
            list: res.data,
          })

          props.setTravelDestination({
            ...res.data.travel_information.destination_city,
          })

          props.setTravelRelationship({
            ...res.data.travel_information.relationship,
          })

          props.setPremi({
            admin_fee: 0,
            discount_amount: res.data.travel_premi_calculation.discount_amount,
            discount_percentage: res.data.travel_premi_calculation.discount_percentage,
            loading_premi: res.data.travel_premi_calculation.loading_premi,
            policy_printing_fee: res.data.policy_printing_fee,
            premi: res.data.travel_premi_calculation.premi,
            price_additional_week: res.data.travel_premi_calculation.price_additional_week,
            price_days_insured: res.data.travel_premi_calculation.price_days_insured,
            stamp_fee: res.data.stamp_fee,
            total_additional_week: res.data.travel_premi_calculation.total_additional_week,
            total_days_insured: res.data.travel_premi_calculation.total_days_insured,
            total_payment: res.data.total_payment,
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
    }),
    lifecycle({
      componentDidMount() {
       
        const query = qs.parse(this.props.location.search)
        const productID = query.product_id
        const {
          getDetailDomestic, setDetailDomestic, detailDomestic, match, setProductID, setproductDetail, stateCountry, setStateCountry,
        } = this.props

        setProductID(productID)

        this.props.fetchDetailProduct(productID).then((response) => {
          setproductDetail(response.data.data)
        })

        const config = [
          { url: '/travel-destinations', name: 'travelDestination' },
          { url: '/relationships', name: 'relationship' },
          { url: '/domestic-cities', name: 'domestikCity' },
        ]

        config.map((item) => {
          const { url, name } = item

          return this.props.getDatas(
            { base: 'apiUser', url, method: 'get' },
          ).then((res) => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: res.data,
            })
          }).catch(() => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: [],
            })
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          getDetailDomestic()
        } else {
          setDetailDomestic({
            ...detailDomestic,
            loading: false,
          })
        }
      },
    }),
  )(FormTravelDomestic),
)
