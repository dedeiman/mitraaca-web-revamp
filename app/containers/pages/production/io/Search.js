import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import SearchIO from 'components/pages/production/io/Search'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    dataContest,
    metaContest,
  } = state.root.contest

  return {
    isFetching,
    dataContest,
    metaContest,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateIO', 'setStateIO', {
    loading: true,
    searchSPPA: '',
    searchName: '',
    list: [],
    page: {},
  }),
  withState('stateFilter', 'setStateFilter', {
    loading: true,
    sppa_number: '',
    name: '',
    keyword: '',
    search: '',
    list: [],
    status_printed: '',
    page: 1,
    filter: undefined,
  }),
  withHandlers({
    loadIO: props => (isSearch) => {
      const {
        filter, search,
        page, limit,
      } = props.stateFilter
      const payload = {
        status: filter,
        keyword: search,
        page: isSearch ? 1 : page,
        limit,
      }

      history.push(`/production-search-io-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handlePrintPolicies: props => (id) => {
      props.getDatas(
        {
          base: 'apiUser',
          url: '/print-pending-insurance-letters',
          method: 'post',
        },
        {
          sppa_id: id,
        },
      )
    },
    handleFilter: props => (val) => {
      const { stateFilter, setStateFilter, loadIO } = props
      setStateFilter({
        ...stateFilter,
        filter: val,
      })
      setTimeout(() => {
        loadIO(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateFilter, setStateFilter, loadIO } = props
      setStateFilter({
        ...stateFilter,
        page: value,
      })
      setTimeout(() => {
        loadIO()
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        location,
        setStateIO, stateIO,
        setStateFilter, stateFilter,
      } = this.props

      const newState = (qs.parse(window.location.search) || {})
      this.props.updateSiteConfiguration('activePage', 'production-search-io-menu')
      const parsed = qs.parse(window.location.search)

      this.props.getDatas(
        { base: 'apiUser', url: `/io?keyword=${parsed.keyword || ''}&status=${parsed.status || ''}&page=${parsed.page ? parsed.page : 1}&limit=10`, method: 'get' },
        null,
      ).then((res) => {
        setStateIO({
          ...stateIO,
          loading: false,
          list: res.data,
          page: res.meta,
        })
        setStateFilter({
          ...stateFilter,
          loading: false,
          list: [
            { id: 'complete', name: 'Complete' },
            { id: 'expired', name: 'Expired' },
          ],
          search: newState.keyword,
          filter: newState.status,
          limit: newState.limit,
          page: res.meta.current_page,
        })
      }).catch((err) => {
        setStateIO({
          ...stateIO,
          loading: false,
          list: [],
        })
        setStateFilter({
          ...stateFilter,
          loading: false,
          list: [
            { id: 'complete', name: 'Complete' },
            { id: 'expired', name: 'Expired' },
          ],
          search: newState.keyword,
          filter: newState.status,
        })
        message.error(err.message)
      })
    },
  }),
)(SearchIO)
