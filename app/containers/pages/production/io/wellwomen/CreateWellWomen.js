import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
} from 'recompose'
import Swal from 'sweetalert2'
import queryString from 'query-string'
import history from 'utils/history'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import moment from 'moment'
import CreateWellWomen from 'components/pages/production/io/wellwomen/CreateWellWomen'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateIO' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('paymentTotal', 'setPaymentTotal', 0),
    withState('premi', 'setPremi', {
      premi: 0,
      discount_percentage: 0,
      discount_currency: 0,
      stamp_fee: 0,
      admin_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('detailWellwoman', 'setDetailWellwoman', {
      loading: true,
      list: {},
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('submitted', 'setSubmitted', false),
    withState('visiblePreview', 'setVisiblePreview', false),

    withHandlers({
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
    }),
    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const parsed = queryString.parse(window.location.search)
        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: parseInt(parsed.product_id),
            premi_wellwoman_calculation: {
              start_period: moment().format('YYYY-MM-DD'),
              insured_dob: formData.insured_dob.format('YYYY-MM-DD'),
              plan: `${formData.plan_type}`,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_wellwoman_calculation
          props.form.setFieldsValue({
            premi: premiData.premi,
          })
          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },

      onSubmit: props => (event) => {
        const parsed = queryString.parse(window.location.search)
        const { params } = props.match
        if (event !== undefined) {
          event.preventDefault()
        }
          props.form.validateFields(async (err, values) => {
            console.log(values)
          if (!err) {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_country_code)

            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)
            props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: params.id ? 'put' : 'post' }, {
              product_id: Number(parsed.product_id),
              wellwoman: {
                name: values.insured_name,
                phone_number: values.phone_number,
                phone_country_code: values.phone_country_code,
                phone_country_code_id: codeId[0].id,
                email: values.email,
                print_policy_book: values.print_policy_book === true,
                insured_dob: moment(values.insured_dob).format('YYYY-MM-DD'),
                premi_calculation: {
                  premi: parseFloat(values.premi),
                  discount_percentage: parseFloat(values.discount_percentage) || 0,
                  discount_currency: parseFloat(values.discount_currency) || 0,
                  plan: `${values.plan_type}`,
                },
              },
            }).then((res) => {
              if (res) {
                props.setSubmitted(false)
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Wellwoman IO berhasil dibuat!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setSubmitted(false)
              props.setLoadSubmitBtn(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            props.setLoadSubmitBtn(false)
            props.setSubmitted(false)
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },
      getDetailWellwoman: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          // props.setPaymentTotal(res.data.total_payment + res.data.stamp_fee)
          props.setDetailWellwoman({
            ...props.detailWellwoman,
            loading: false,
            list: res.data,
          })

          props.setPremi({
            premi: res.data.wellwoman_premi_calculation.premi,
            discount_percentage: res.data.wellwoman_premi_calculation.discount_percentage,
            discount_currency: res.data.wellwoman_premi_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })
        })
      },
    }),
    withHandlers({
      submitData: props => (e) => {
        props.onSubmit(e)
      },
    }),
    lifecycle({
      componentDidMount() {
       
        const {
          match, getDetailWellwoman, setDetailWellwoman, detailWellwoman, stateCountry, setStateCountry,
        } = this.props
        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          getDetailWellwoman()
        } else {
          setDetailWellwoman({
            ...detailWellwoman,
            loading: false,
          })
          this.props.getPremi()
        }
      },
    }),
  )(CreateWellWomen),
)
