import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
} from 'recompose'
import DetailIO from 'components/pages/production/io/DetailIO'
import { getDatas } from 'actions/Option'
import { capitalize } from 'lodash'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,

    mapDispatchToProps,
  ),
  withState('detailData', 'setDetailData', {
    loading: true,
    type: '',
    list: [],
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'production-search-io-menu')
      this.props.getDatas(
        { base: 'apiUser', url: `/io/${this.props.match.params.id}`, method: 'get' },
      ).then((res) => {
        this.props.setDetailData({
          ...this.props.detailData,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        if (err.code === 404) {
          Swal.fire(
            '',
            `<span style="color:#2b57b7;font-weight:bold">${err.message}</span>`,
            'error',
          ).then(() => {
            history.push('/production-search-io-menu')
          })
        }

        this.props.setDetailData({
          loading: false,
          type: '',
          list: [],
        })
      })
    },
  }),
)(DetailIO)
