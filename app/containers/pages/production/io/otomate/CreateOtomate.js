/* eslint-disable default-case */
/* eslint-disable array-callback-return */
/* eslint-disable no-duplicate-case */
import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import Swal from 'sweetalert2'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import history from 'utils/history'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import CreateIO from 'components/pages/production/io/otomate/CreateOtomate'
import { isEmpty } from 'lodash'
import { handleProduct } from 'constants/ActionTypes'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateIO' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('listBrand', 'setListBrand', {
      loading: true,
      options: [],
    }),
    withState('listSeries', 'setListSeries', {
      loading: false,
      disabled: true,
      options: [],
    }),
    withState('listPlateCode', 'setListPlateCode', {
      loading: true,
      options: [],
    }),
    withState('listYear', 'setListYear', {
      loading: true,
      options: [],
    }),
    withState('floodMotorCar', 'setFloodMotorCar', {
      loading: true,
      floodInit: '',
      initPercent: 0,
      disabled: false,
      disablePercantage: false,
      options: [{
        label: '10%',
        value: 10,
      }, {
        label: '100%',
        value: 100,
      }],
    }),
    withState('eqMotorCar', 'setEqMotorCar', {
      loading: true,
      eqInit: '',
      initPercent: 0,
      disabled: false,
      disablePercantage: false,
      options: [{
        label: '10%',
        value: 10,
      }, {
        label: '100%',
        value: 100,
      }],
    }),
    withState('huraMotorCar', 'setHuraMotorCar', {
      loading: true,
      rsccInit: '',
      disabled: false,
    }),
    withState('terrorismMotorCar', 'setTerrorismMotorCar', {
      loading: true,
      tsInit: '',
      disabled: false,
    }),
    withState('tjhMotorCar', 'setTjhMotorCar', {
      loading: true,
      tjhInit: '',
      initAmount: '',
      disabled: false,
      options: [],
    }),
    withState('papMotorCar', 'setPapMotorCar', {
      loading: true,
      papInit: '',
      initAmount: '',
      papPassenger: 1,
      disabled: false,
      options: [],
    }),
    withState('pllMotorCar', 'setPllMotorCar', {
      loading: true,
      pllInit: '',
      initAmount: '',
      pllPassenger: '',
      disabled: false,
      options: [],
    }),
    withState('padMotorCar', 'setPadMotorCar', {
      loading: true,
      padInit: '',
      initAmount: '',
      padPassenger: '',
      disabled: false,
      options: [],
    }),
    withState('bengkelAuthMotorCar', 'setBengkelAuthMotorCar', {
      loading: true,
      bengkelNotHide: true,
      bengkelInit: '',
      atpmPercentage: '',
      disabled: false,
    }),
    withState('premi', 'setPremi', {
      premi: 0,
      value: 0,
      rate: 0,
      stamp_fee: 0,
      admin_fee: 0,
      policy_printing_fee: 0,
      total_payment: 0,
      discount_percentage: 0,
      discount_currency: 0,
    }),
    withState('rateTypeDisabled', 'setRateTypeDisabled', {
      tidak: false,
      rate: false,
      risiko: false,
    }),
    withState('hiddenJenisRate', 'setHiddenJenisRate', false),
    withState('detailOtomate', 'setDetailOtomate', {
      loading: true,
      list: [],
    }),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('commercial', 'setCommercial', true),
    withState('limits', 'setLimits', []),

    withHandlers({
      onSubmit: props => async () => {
        const parsed = queryString.parse(window.location.search)
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (!err) {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)

            props.setLoadSubmitBtn(true)
            props.setSubmitted(true)
            props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: Number(parsed.product_id),
              ottomate: {
                name: values.insured_name ? (values.insured_name).toUpperCase() : '',
                phone_country_code: values.phone_country_code,
                phone_country_code_id: codeId[0].id ,
                phone_number: values.phone_number,
                email: values.email ? (values.email).toUpperCase() : '',
                period_in_year: values.insurance_period,
                print_policy_book: values.print_policy_book === true,
                vehicle_information: {
                  brand_id: values.brand,
                  series_id: values.series,
                  production_year: values.production_year,
                  description_series: values.desc_vehicle ? (values.desc_vehicle).toUpperCase() : '',
                  license_plate_code: values.license_plate_code,
                  middle_police_number: values.middle_policy_number,
                  end_police_number: values.rear_vehicle_year ? (values.rear_vehicle_year).toUpperCase() : '',
                  vehicle_use: values.vehicle_use,
                },
                premi_calculation: {
                  coverage: Helper.replaceNumber(values.coverage_value),
                  additional_accesoris_costs: Helper.replaceNumber(values.additional_accessories_costs),
                  own_risk_value: Helper.replaceNumber(values.own_risk_value),
                  loading_rate_type: values.loading_rate_type,
                  discount_percentage: parseFloat(values.discount_percentage),
                  discount_currency: parseFloat(values.discount_currency),
                  rate_type: values.rate_type,
                  rate: Number(props.premi.rate_premi),
                  premi: Number(props.premi.premi),
                },
                additional_limit: {
                  limits: props.limits,
                  pap_total: Number(values.pap_passenger),
                  pap_amount: typeof (values.pap_amount) !== 'string' ? values.pap_amount : Helper.currencyToInt(values.pap_amount),
                  pll_amount: typeof (values.pll_amount) !== 'string' ? values.pll_amount : Helper.currencyToInt(values.pll_amount),
                  pad_amount: typeof (values.pad_amount) !== 'string' ? values.pad_amount : Helper.currencyToInt(values.pad_amount),
                  tjh: typeof (values.tjh_amount) !== 'string' ? values.tjh_amount : Helper.currencyToInt(values.tjh_amount),
                  atpm_percentage: values.atpm_percentage ? parseFloat(values.atpm_percentage) : 0,
                  flood_percentage: values.flood_percentage ? values.flood_percentage : 0,
                  eq_percentage: values.eq_percentage ? values.eq_percentage : 0,
                },
              },
            }).then((res) => {
              if (res) {
                props.setSubmitted(false)
                Swal.fire(
                  ``,
                  `<span style="color:#2b57b7;font-weight:bold">IO ${handleProduct(parsed.product)} berhasil dibuat!</span>`,
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              props.setSubmitted(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            props.setLoadSubmitBtn(false)
            props.setSubmitted(false)
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },

      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },

      initPremi: props => (key, val) => {
        const parsed = queryString.parse(window.location.search)
        const formData = props.form.getFieldsValue()

        const tjhAmount = !isEmpty(formData.tjh) ? formData.tjh_amount : 0
        const papAmount = !isEmpty(formData.pap) ? formData.pap_amount : 0
        const pllAmount = !isEmpty(formData.pll) ? formData.pll_amount : 0
        const padAmount = !isEmpty(formData.pad) ? formData.pad_amount : 0

        let addtionalLimits = []
        if (props.limits.includes(key)) {
          props.setLimits(props.limits.filter(item => item !== key))
          const dataLimits = props.limits.filter(item => item !== key)

          addtionalLimits = dataLimits
        } else if (val !== undefined) {
          props.setLimits((limit) => {
            addtionalLimits.push(...limit, val)

            return [...limit, val]
          })
        }

        const payload = {
          product_id: Number(parsed.product_id),
          premi_ottomate_calculation: {
            product_id: Number(parsed.product_id),
            brand_id: formData.brand ? Number(formData.brand) : 0,
            series_id: formData.series ? Number(formData.series) : 0,
            sum_insured: Number(props.premi.sum_insured),
            coverage: Helper.replaceNumber(formData.coverage_value),
            rate_type: formData.rate_type || '',
            loading_rate_type: formData.loading_rate_type || 'rate',
            production_year: formData.production_year || 0,
            license_plate_code: formData.license_plate_code,
            insurance_period: formData.insurance_period,
            additional_accessories_costs: Helper.replaceNumber(formData.additional_accessories_costs),
            pap_total: Number(formData.pap_passenger),
            pap_amount: typeof (papAmount) !== 'string' ? papAmount : Helper.currencyToInt(papAmount),
            pll_amount: typeof (pllAmount) !== 'string' ? pllAmount : Helper.currencyToInt(pllAmount),
            pad_amount: typeof (padAmount) !== 'string' ? padAmount : Helper.currencyToInt(padAmount),
            tjh: typeof (tjhAmount) !== 'string' ? tjhAmount : Helper.currencyToInt(tjhAmount),
            print_policy_book: formData.print_policy_book === true,
            discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
            discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
            additional_limits: !isEmpty(addtionalLimits) ? addtionalLimits : props.limits,
            atpm_percentage: formData.atpm_percentage ? Number(formData.atpm_percentage) : 0,
            flood_percentage: formData.flood_percentage ? Number(formData.flood_percentage) : 0,
            eq_percentage: formData.eq_percentage ? Number(formData.eq_percentage) : 0,
          },
        }

        return props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          payload,
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_ottomate_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },

      handleSelect: props => (key, value, keyType) => {
        props[`setList${keyType}`]({
          ...props[`list${keyType}`],
          loading: true,
        })

        props.getDatas(
          { base: 'apiUser', url: `/${key}/${value}`, method: 'get' },
          null,
        ).then((res) => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            disabled: false,
            options: res.data.length > 0 ? res.data.map(item => ({
              value: item.id, label: item.name, category: item.category, price: item.series_price.price, seat: item.available_seat,
            })) : [],
          })
        }).catch(() => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            options: [],
          })
        })
      },
    }),
    withHandlers({
      getDetailOtomate: props => () => {
        // const parsed = queryString.parse(window.location.search)
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.handleSelect('series-car', res.data.ottomate_vehicle_information.brand_id, 'Series')
          props.setDetailOtomate({
            ...props.detailOtomate,
            loading: false,
            list: res.data,
          })
          props.setPremi({
            premi: res.data.ottomate_premi_calculation.premi,
            admin_fee: res.data.admin_fee,
            rate: res.data.ottomate_premi_calculation.rate,
            stamp_fee: res.data.stamp_fee,
            discount_percentage: res.data.ottomate_premi_calculation.discount_percentage,
            discount_currency: res.data.ottomate_premi_calculation.discount_currency,
            total_payment: res.data ? res.data.total_payment : 0,
            policy_printing_fee: res.data.policy_printing_fee,
            print_policy_book: res.data.print_policy_book,
          })

          props.setLimits(res.data.ottomate_additional_limit.limits)
        })
      },

      handleSeriesPrice: props => (seriesId = '', year = '') => {
        props.getDatas(
          { base: 'apiUser', url: `/series-price-car?production_year=${year}&series_id=${seriesId}`, method: 'get' },
          null,
        ).then((res) => {
          props.setPremi({
            ...props.premi,
            coverage: res.data.price,
          })
        }).catch(() => {
          props.setPremi({
            ...props.premi,
            coverage: 0,
          })
        })
      },

      submitData: props => () => {
        props.onSubmit()
      },
    }),
    withPropsOnChange(
      ['initPremi'],
      ({ initPremi }) => ({
        initPremi: _.debounce(initPremi, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {

        const {
          setListBrand, listBrand, setListPlateCode, listPlateCode, setListYear, listYear, setFloodMotorCar, floodMotorCar, setEqMotorCar, eqMotorCar, tjhMotorCar, setTjhMotorCar, papMotorCar, setPapMotorCar,
          pllMotorCar, setPllMotorCar, setPadMotorCar, padMotorCar, setBengkelAuthMotorCar, bengkelAuthMotorCar, setHuraMotorCar, huraMotorCar, terrorismMotorCar, setTerrorismMotorCar, match, getDetailOtomate, setDetailOtomate,
          detailOtomate, setCommercial, setHiddenJenisRate, setLimits, stateCountry, setStateCountry,
        } = this.props
        const parsed = queryString.parse(window.location.search)

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          getDetailOtomate()
        } else {
          setDetailOtomate({
            ...detailOtomate,
            loading: false,
          })
        }

        switch (parsed.product) {
          // Product Otomate
          case 'PO001':
          case 'POS01':
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: 'flood',
              initPercent: 10,
              disabled: true,
              options: [{
                label: '10%',
                value: 10,
              }, {
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: 'eq',
              initPercent: 10,
              disabled: true,
              options: [{
                label: '10%',
                value: 10,
              }, {
                label: '100%',
                value: 100,
              }],
            })
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              bengkelInit: '',
              disabled: false,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: 'rscc',
              disabled: true,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              tsInit: 'ts',
              disabled: true,
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['flood', 'eq', 'rscc', 'ts', 'tjh', 'pap', 'pad'])
            }

            break
          // Product Otomate Smart
          case 'PO002':
          case 'POS02':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              bengkelInit: '',
              disabled: false,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: '',
              disabled: false,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              tsInit: '',
              disabled: false,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['tjh'])
            }
            break
          // Product Otomate Soliter
          case 'PO003':
          case 'POS03':
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: 'flood',
              initPercent: 100,
              disabled: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: 'eq',
              initPercent: 100,
              disabled: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              atpmPercentage: '0.25',
              bengkelInit: 'atpm',
              disabled: true,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: 'rscc',
              disabled: true,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              tsInit: 'ts',
              disabled: true,
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['flood', 'eq', 'ts', 'rscc', 'atpm', 'tjh', 'pap', 'pad'])
            }
            break
          // Product TLO
          case 'PO004':
          case 'POS04':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: false,
              bengkelInit: '',
              disabled: true,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            break
          // Product Comprehensive
          case 'PO005':
          case 'POS05':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: false,
              bengkelInit: '',
              disabled: true,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            break

          default:
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: '',
              disabled: false,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              tsInit: '',
              disabled: false,
            })
            break
        }

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=tjh&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          const options = res.data.length !== 0 ? res.data.map(item => ({
            value: item.price,
            label: item.price,
          })) : []

          switch (parsed.product) {
            case 'PO001':
            case 'POS01': {
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 50000000,
                disabled: true,
                options,
              })
              break
            }

            case 'PO002':
            case 'POS02': {
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 25000000,
                disabled: true,
                options,
              })
              break
            }

            case 'PO003':
            case 'POS03':
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 75000000,
                disabled: true,
                options,
              })
              break


            case 'PO004':
            case 'PO005':
            case 'POS04':
            case 'POS05':
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: '',
                initAmount: 0,
                disabled: false,
                options,
              })
              break

            default:
              break
          }
        }).catch(() => {

        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pll&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          switch (parsed.product) {
            case 'PO001':
            case 'POS01':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            default:
              break
          }
        }).catch(() => {

        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pad&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          const options = res.data.length !== 0 ? res.data.map(item => ({
            value: item.price,
            label: item.price,
          })) : []

          switch (parsed.product) {
            case 'PO001':
            case 'POS01': {
              const initAmount = 10000000
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: 'pad',
                initAmount,
                disabled: true,
                options,
              })
              break
            }
            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price,
                  label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03': {
              const initAmount = 10000000
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: 'pad',
                initAmount,
                disabled: true,
                options,
              })
              break
            }

            default:
              break
          }
        }).catch(() => {

        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pap&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          const options = res.data.length !== 0 ? res.data.map(item => ({
            value: item.price,
            label: item.price,
          })) : []

          switch (parsed.product) {
            case 'PO001':
            case 'POS01': {
              const initAmount = 10000000
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: 'pap',
                initAmount,
                disabled: true,
                papPassenger: 4,
                options,
              })
              break
            }

            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: '',
                initAmount: 0,
                disabled: false,
                papPassenger: 1,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03': {
              const initAmount = 10000000
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: 'pap',
                initAmount,
                disabled: true,
                papPassenger: 4,
                options,
              })
              break
            }

            default:
              break
          }
        }).catch(() => {

        })

        this.props.getDatas(
          { base: 'apiUser', url: '/brand-car', method: 'get' },
          null,
        ).then((res) => {
          setListBrand({
            ...listBrand,
            loading: false,
            options: res.data.map(item => ({ value: item.id, label: item.name })) || [],
          })
        }).catch(() => {
          setListBrand({
            ...listBrand,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/license-plate-code', method: 'get' },
          null,
        ).then((res) => {
          setListPlateCode({
            ...listPlateCode,
            loading: false,
            options: res.data.map(item => ({ value: item.code, label: item.code })) || [],
          })
        }).catch(() => {
          setListPlateCode({
            ...listPlateCode,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/production-year', method: 'get' },
          null,
        ).then((res) => {
          setListYear({
            ...listYear,
            loading: false,
            options: res.data.reverse().map(item => ({ value: item.year, label: item.year })) || [],
          })
        }).catch(() => {
          setListYear({
            ...listYear,
            loading: false,
            options: [],
          })
        })

        switch (parsed.product) {
          case 'PO001':
          case 'PO002':
          case 'PO003':
          case 'POS01':
          case 'POS02':
          case 'POS03':
            setCommercial(false)
            break
          default:
            setCommercial(true)
        }
      },
    }),
  )(CreateIO),
)
