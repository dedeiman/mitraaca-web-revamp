/* eslint-disable no-case-declarations */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormAsri from 'components/pages/production/io/asri/FormAsri'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import {
  fetchDetailProduct,
} from 'actions/Product'
import {
  createIO,
} from 'actions/CreateIOProperty'
import _ from 'lodash'
import Swal from 'sweetalert2'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createIO: bindActionCreators(createIO, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateSelects', 'setStateSelects', {
      provinceLoad: false,
      provinceList: [],
      cityLoad: false,
      cityList: [],
      subDistrictLoad: false,
      subDistrictList: [],
      villageLoad: false,
      villageList: [],
      floorLoad: false,
      floorList: [
        { label: '1 Lantai', value: 1 },
        { label: '2 Lantai', value: 2 },
      ],
      constructLoad: false,
      constructList: [
        { label: 'Kelas 1', value: 1 },
        { label: 'Kelas 2', value: 2 },
      ],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('coverage', 'setCoverage', 0),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      premi: 0,
      value: 0,
      rate: 0,
      stamp_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
    }),
    withState('detailData', 'setDetailData', {
      loading: false,
      list: [],
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('geoLocation', 'setGeoLocation', {
      address: '',
      long: '',
      lat: '',
    }),

    withHandlers({
      initListData: props => (url, name) => props.getDatas(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        props.setStateSelects({
          ...props.stateSelects,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch(() => {
        props.setStateSelects({
          ...props.stateSelects,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      }),
    }),
    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()

        const coverage = (formData.building_price ? parseFloat(formData.building_price) : 0) + (formData.furniture_price ? parseFloat(formData.furniture_price) : 0)
        props.setCoverage(coverage)

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            // eslint-disable-next-line radix
            product_id: parseInt(props.productID),
            premi_asri_calculation: {
              class: formData.class_construction || 0,
              coverage,
              coverage_period: 1,
              additional_limt: formData.additional_limits,
              city: formData.city_id,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_asri_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })

        })
      },
      onChangeStateLocationAlternative: props => async (stateType, value) => {
        switch (stateType) {
          case 'province':
            props.setStateSelects({
              ...props.stateSelects,
              cityLoad: false,
              cityList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              city_id: undefined,
              sub_district_id: undefined,
              village_id: undefined,
            })

            return props.initListData(`/cities/${value}`, 'city')
          case 'city':
            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              sub_district_id: undefined,
              village_id: undefined,
            })

            return props.initListData(`/sub-district/${value}`, 'subDistrict')
          case 'subDistrict':

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              village_id: undefined,
            })

            return props.initListData(`/villages/${value}`, 'village')
          default:
            break
        }

        return true
      },
    }),
    withHandlers({
      submitData: props => async () => {
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              'Terjadi kesalahan',
              'Mohon Periksa kembali data Anda !',
              'error',
            )
          } else {
            const formData = values
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_country_code)

            formData.product_id = props.productID

            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)

            const payload = {
              product_id: Number(formData.product_id),
              asri: {
                name: (formData.name || '').toUpperCase(),
                phone_number: formData.phone_number,
                phone_country_code: formData.phone_country_code,
                phone_country_code_id: codeId[0].id,
                email: (formData.email || '').toUpperCase(),
                period_in_year: 1,
                print_policy_book: formData.print_policy_book === true,
                building_information: {
                  class_construction: formData.class_construction,
                  class_construction_description: formData.class_construction_description,
                  building_area: Number(formData.building_area),
                  // total_floor: Number(formData.total_floor),
                },
                address: {
                  province_id: formData.province_id,
                  city_id: formData.city_id,
                  sub_district_id: formData.sub_district_id,
                  village_id: formData.village_id,
                  address: (formData.address || '').toUpperCase(),
                  same_address_with_insured: true,
                  lat: Number(formData.lat),
                  lng: Number(formData.lng),
                  postal_code: formData.postal_code,
                  total_floor: Number(formData.total_floor),
                },
                premi_calculation: {
                  rate: Number(props.premi.rate),
                  premi: Number(props.premi.value),
                  policy_printing_fee: Number(props.premi.policy_printing_fee),
                  total_payment: Number(props.premi.total_payment),
                  stamp_fee: Number(props.premi.stamp_fee),
                  building_price: formData.building_price ? Number(formData.building_price) : 0,
                  furniture_price: formData.furniture_price ? Number(formData.furniture_price) : 0,
                  discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
                  discount_currency: formData.discount_currency ? Number(formData.discount_currency) : 0,
                  additional_limits: formData.additional_limits || [],
                },
              },
            }

            await props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, payload).then((res) => {
              if (res) {
                Swal.fire(
                  'Data berhasil disimpan!',
                  '',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              Swal.fire(
                error.message,
                '',
                'error',
              )

              props.setVisiblePreview(false)
            })

            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              'Terjadi kesalahan',
              'Mohon Periksa kembali data Anda !',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      setListLocation: props => (url, name) => {
        props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch(() => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onChangeStateLocation: props => (stateType, value) => {
        switch (stateType) {
          case 'province':
            props.setStateSelects({
              ...props.stateSelects,
              cityLoad: false,
              cityList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              city_id: undefined,
              sub_district_id: undefined,
              village_id: undefined,
              float_area: undefined,
              earth_quake_area: undefined,
            })

            props.setListLocation(`/cities/${value}`, 'city')
            break
          case 'city':
            props.getPremi()

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            const city = props.stateSelects.cityList.find(o => o.id === value)

            props.form.setFieldsValue({
              sub_district_id: undefined,
              village_id: undefined,
              float_area: city.flood_area ? 'YA' : 'TIDAK',
              earth_quake_area: city.earthquake_area ? 'YA' : 'TIDAK',
            })

            props.setListLocation(`/sub-district/${value}`, 'subDistrict')

            break

          case 'subDistrict':
            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              village_id: undefined,
            })

            props.setListLocation(`/villages/${value}`, 'village')

            break
          default:
            break
        }

        const addtionalLimitValue = props.form.getFieldValue('additional_limits')

        if (typeof addtionalLimitValue !== 'undefined' && props.form.getFieldValue('earth_quake_area') !== 'YA') {
          const idx = addtionalLimitValue.indexOf('eqvet')
          if (idx > -1) {
            addtionalLimitValue.splice(addtionalLimitValue.indexOf('eqvet'), 1)
          }
        }

        props.form.setFieldsValue({
          additional_limits: addtionalLimitValue,
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
    }),
    withHandlers({
      initDetailData: props => async () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then(async (res) => {
          console.log(res)
          const { data } = res
          props.setDetailData({
            ...props.detailData,
            loading: false,
            list: data,
          })

          props.setPremi({
            premi: res.data.asri_premi_calculation.premi,
            discount_percentage: res.data.asri_premi_calculation.discount_percentage,
            discount_currency: res.data.asri_premi_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })

          props.setGeoLocation({
            ...props.geoLocation,
            address: _.get(data, 'asri_building_address.address'),
            lng: _.get(data, 'asri_building_address.lng'),
            lat: _.get(data, 'asri_building_address.lat'),
          })

          const provinceId = data.asri_building_address.province_id
          const cityId = data.asri_building_address.city_id
          const subDistrictId = data.asri_building_address.sub_district_id
          const villageId = data.asri_building_address.village_id

          await props.onChangeStateLocationAlternative('province', provinceId)
          await props.onChangeStateLocationAlternative('city', cityId)
          await props.onChangeStateLocationAlternative('subDistrict', subDistrictId)

          props.form.setFieldsValue({
            city_id: cityId || undefined,
            sub_district_id: subDistrictId || undefined,
            village_id: villageId || undefined,
          })

          setTimeout(() => props.getPremi())
        })
      },
    }),
    lifecycle({
      componentWillMount() {
        const query = qs.parse(this.props.location.search)
        const productID = query.product_id

        this.props.setProductID(productID)

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        const config = [
          { url: '/provinces', name: 'province' },
        ]

        config.map(item => (
          this.loadMasterData(item.url, item.name)
        ))
      },
      componentDidMount() {

        google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line

        if (this.props.match.params.id) {
          this.props.initDetailData()
        }

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
        })
      },
      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch(() => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
      initAutocomplete() {
        const inputNode = document.getElementById('convert-geolocation')
        const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
        /* eslint-disable */
        window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
          if (event.key === 'Enter') {
            event.preventDefault()
          }
        })
        /* eslint-enable */
        autoComplete.addListener('place_changed', () => {
          const place = autoComplete.getPlace()
          const location = JSON.stringify(place.geometry.location)
          const data = JSON.parse(location)
          data.address = place.formatted_address
          this.props.setGeoLocation({
            address: data.address,
            long: data.lng,
            lat: data.lat,
          })

          this.props.form.setFieldsValue({
            lng: data.lng,
            lat: data.lat,
          })
        })
      },
    }),
  )(FormAsri),
)