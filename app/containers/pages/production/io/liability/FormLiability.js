import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormLiability from 'components/pages/production/io/liability/FormLiability'
import history from 'utils/history'
import { getDatas } from 'actions/Option'
import isEmpty from 'lodash'
import Swal from 'sweetalert2'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product
  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('productID', 'setProductID', 0),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('productDetail', 'setProductDetail', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('paymentTotal', 'setPaymentTotal', 0),
    withState('detailLiability', 'setDetailLiability', {
      loading: true,
      list: [],
      fileUrl : null,
    }),
    withState('premi', 'setPremi', {
      premi: 0,
      limitPremi: 0,
      value: 0,
      discount: 0,
      stamp_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
      limit_policy_amount: 0,

    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),

    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const parsed = qs.parse(window.location.search)

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: parseInt(parsed.product_id),
            premi_liability_calculation: {
              start_period: !isEmpty(formData.start_period) ? formData.start_period.format('YYYY-MM-DD') : undefined,
              end_period: !isEmpty(formData.end_period) ? formData.end_period.format('YYYY-MM-DD') : undefined,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_liability_calculation
          props.form.setFieldsValue({
            premi: premiData.premi,
          })

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
    }),
    withHandlers({
      submitData: props => async () => {
        const parsed = qs.parse(window.location.search)
        const { params } = props.match

        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_country_code)
            
            props.setLoadSubmitBtn(true)
            props.setSubmitted(true)

            props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: Number(parsed.product_id),
              liability: {
                name: values.name,
                phone_number: values.phone_number,
                phone_country_code: values.phone_country_code,
                phone_country_code_id: codeId[0].id,
                email: values.email,
                print_policy_book: values.print_policy_book === true,
                premi_calculation: {
                  print_policy_book: values.print_policy_book === true,
                  discount_percentage: values.discount_percentage ? parseFloat(values.discount_percentage) : 0,
                  discount_currency: values.discount_currency ? parseFloat(values.discount_currency) : 0,
                },
              },
            }).then((res) => {
              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )

              props.setVisiblePreview(false)
            })

            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
    }),    
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()

        props.submitPreview()
      },
      getDetailLiability: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/indicative-offers/${props.match.params.id}/sendattachio`, method: 'post' },
        ).then((res) => {
          props.setDetailLiability({
            ...props.detailLiability,
            loading: false,
            fileUrl: res.data,
          })

        })
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailLiability({
            ...props.detailLiability,
            loading: false,
            list: res.data,
          })
          props.setPremi({
            premi: res.data.liability_premi_calculation.premi,
            limit_policy_amount: res.data.liability_premi_calculation.coverage_limit,
            discount_percentage: res.data.liability_premi_calculation.discount_percentage,
            discount_currency: res.data.liability_premi_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            total_payment: res.data.total_payment,
            policy_printing_fee: res.data.policy_printing_fee,
          })

        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        const {
          setDetailLiability, detailLiability, match, setProductID, getDetailLiability, stateCountry, setStateCountry,
        } = this.props
        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        const query = qs.parse(window.location.search)
        const productID = query.product_id

        setProductID(productID)

        if (match.params.id) {
          getDetailLiability()
        } else {
          setDetailLiability({
            ...detailLiability,
            loading: false,
          })
          // this.props.getPremi()
        }
      },
    }),
  )(FormLiability),
)
