import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormCargo from 'components/pages/production/io/cargo/FormCargo'
import { getDatas } from 'actions/Option'
import {
  fetchDetailProduct,
} from 'actions/Product'
import {
  createIO,
} from 'actions/CreateIOCargo'
import _ from 'lodash'
import history from 'utils/history'
import Swal from 'sweetalert2'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createIO: bindActionCreators(createIO, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('stateSelects', 'setStateSelects', {}),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('cargoTypeList', 'setCargoTypeList', []),
    withState('cargoTypeList', 'setCargoTypeList', []),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      premi: 0,
      discount_percentage: 0,
      discount_amount: 0,
      admin_fee: 0,
      policy_printing_fee: 0,
      stamp_fee: 0,
      total_payment: 0,
    }),
    withState('exchangeRate', 'setExchangeRate', {
      idr_exchange_rate: 0,
    }),
    withState('detailData', 'setDetailData', {
      loading: false,
      list: [],
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('submitted', 'setSubmitted', false),
    
    withHandlers({
      submitData: props => async () => {
        const { params } = props.match

        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)

            const escapeHtml = (text) => {
              const map = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#039;',
              }

              return text.replace(/[&<>"']/g, m => map[m])
            }

            const otherInformation = escapeHtml(
              `
              <ul>
                <li>Institute Cargo Clauses (AIR), (Excluding sendings by Post)</li>
                <li>Institute Extended Radioactive Contamination Exclusive Clause</li>
                <li>Electronic Date Recognition Exclusion Clause (for Marine Business)</li>
                <li>Seepage and Pollution Exclusion Clause (01.01.89)</li>
                <li>Information Technology Hazards Clause)</li>
                <li>Institute Chemical, Biological, Bio-Chemical, Electro Magnetic, Weapons and Cyber Attack Exclusion Clause (01/11/02)</li>
                <li>Terrorism Exclusion Clause</li>
                <li>Sanction limitation exclusion clause</li>
                <li>For shipment by sea, vessel must be maximum 35 years old and minimum 100 GRT and  subject to excluding Tug and Barge, LCT/LST and wooden vessel</li>
                <li>Cargo must be packed in standard packing</li>
                <li>For sending by open truck, cargo must be sufficiently covered by waterproof tarpaulin</li>
                <li>For Non-containerized cargo, it must be loaded under deck, if on deck it will be covered under ICC 'C' 1/1/82</li>
                <li>Cargo must be in safety lashing (for cargoes in form of Heavy equipment and Car)</li>
                <li>Land Conveyance must be approved by transportation authority</li>
                <li>Excluding back-dated cover</li>
                <li>Coverage will start as per time and date of issuance policy/certificate</li>
                <li>Sanction limitation exclusion clause</li>
                <li>The condition of cargo must be in brand new product for machineries, Spare-parts and Equipments</li>
                <li>Including loading and unloading</li>
                <li>Excluding Freight Forwarder Liability</li>
                <li>Excluding denting, bending, and scratching unless due to insured perils</li>
                <li>Excluding Rusting, Oxidation, Discoloration unless due to insured perils</li>
                <li>Excluding Mechanical and Electrical Derangement</li>
                <li>Excluding pre-existing Damages</li>
                <li>Excluding mysterious loss, shortage and loss of volume and loss of weight</li>
                <li>Excluding loss of cargo due to Fraud by own driver of the cargo owner and/or carrier company</li>
                <li>Los notification clause (30 days)</li>
              </ul>
              `,
            )

            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)

            const payload = {
              product_id: Number(props.productID),
              cargo: {
                name: values.name,
                phone_number: values.phone_number,
                phone_country_code: values.prefix,
                phone_country_code_id: codeId[0].id,
                email: values.email,
                print_policy_book: values.print_policy_book === true,
                vessel_information: {
                  cargo_type_id: values.cargo_type_id,
                  vehicle_type_id: values.vehicle_type_id,
                },
                cargo_information: {
                  // interest_detail: values.interest_detail,
                  currency_id: values.currency_id,
                  sum_insured: Number(values.sum_insured),
                  main_coverage: "ICC 'A'",
                  other_information: otherInformation.trim(),
                },
                cargo_premi_calculation: {
                  rate: Number(values.rate),
                  premi: Number(props.premi.premi),
                  discount_percentage: Number(values.discount_percentage),
                  discount_amount: Number(values.discount_amount),
                },
              },
            }

            await props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, payload).then((res) => {
              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )

              props.setVisiblePreview(false)
            })

            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const convertExchange = (props.stateSelects.currencyList || []).find(item => item.id === formData.currency_id).idr_exchange_rate * formData.sum_insured
        props.setExchangeRate({
          ...props.exchangeRate,
          idr_exchange_rate: convertExchange,
        })

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: parseInt(props.productID),
            premi_cargo_calculation: {
              currency_id: formData.currency_id,
              idr_exchange_rate: formData.idr_exchange_rate,
              sum_insured: parseFloat(formData.sum_insured),
              rate: parseFloat(formData.rate),
              discount_percentage: parseFloat(formData.discount_percentage),
              discount_amount: parseFloat(formData.discount_amount),
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_cargo_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
    }),
    withHandlers({
      initDetailData: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailData({
            ...props.detailData,
            loading: false,
            list: res.data,
          })

          setTimeout(() => props.getPremi())
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
    }),

    lifecycle({
      componentDidMount() {
        const query = qs.parse(this.props.location.search)
        const productID = query.product_id

        this.props.setProductID(productID)

        const config = [
          { url: '/cargo-types', name: 'cargoType' },
          { url: '/cargo-categories', name: 'cargoCategory' },
          { url: '/vehicle-types', name: 'vehicleType' },
          { url: '/currencies', name: 'currency' },
        ]

        config.map(item => (
          this.loadMasterData(item.url, item.name)
        ))

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
        }).catch(() => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
        })

        if (this.props.match.params.id) {
          this.props.initDetailData()
        }
      },

      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          if (name === 'cargoType') {
            this.props.setCargoTypeList(res.data)
          }

          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch(() => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
  )(FormCargo),
)
