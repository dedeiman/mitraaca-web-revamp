import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
} from 'recompose'
import DetailAmanah from 'components/pages/production/io/amanah/DetailAmanah'
import { message } from 'antd'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,

    mapDispatchToProps,
  ),
  withState('detailAmanah', 'setDetailAmanah', {
    loading: true,
    type: '',
    list: [],
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'production-create-io')
      this.props.getDatas(
        { base: 'apiUser', url: `/io/${this.props.match.params.id}`, method: 'get' },
      ).then((res) => {
        this.props.setDetailAmanah({
          ...this.props.detailAmanah,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setDetailAmanah({
          loading: false,
          type: '',
          list: [],
        })
      })
    },
  }),
)(DetailAmanah)
