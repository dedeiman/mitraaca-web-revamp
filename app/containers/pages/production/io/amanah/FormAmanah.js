import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import FormAmanah from 'components/pages/production/io/amanah/FormAmanah'
import history from 'utils/history'
import queryString from 'query-string'
import Swal from 'sweetalert2'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formAmanah' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      premi: 0,
      discount_percentage: 0,
      discount_currency: 0,
      stamp_fee: 0,
      admin_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
    }),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('premiPayload', 'setPremiPayload', {
      coverage: 0,
      job_class: '',
      discount_percentage: 0,
      discount_currency: 0,
    }),
    withState('detailAmanah', 'setDetailAmanah', {
      loading: true,
      list: [],
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),

    withHandlers({
      handlePremi: props => (key, value) => {
        const parsed = queryString.parse(window.location.search)

        props.setPremiPayload({
          ...props.premiPayload,
          [key]: value,
        })
        
        setTimeout(() => {
          props.getDatas(
            { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' }, {
              product_id: Number(parsed.product_id),
              premi_pa_amanah_calculation: {
                ...props.premiPayload,
                [key]: value,
              },
            },
          ).then((res) => {
            props.setPremi({
              premi: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.premi : 0,
              discount_currency: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.discount_currency : 0,
              discount_percentage: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.discount_percentage : 0,
              stamp_fee: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.stamp_fee : 0,
              total_payment: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.total_payment : 0,
              policy_printing_fee: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.policy_printing_fee : 0,
              max_discount: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.max_discount : 0,
              max_out_go: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.max_out_go : 0,
            })

          })
        }, 300)
      },

      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },

      onSubmit: props => async () => {
        const parsed = queryString.parse(window.location.search)
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (!err) {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_country_code)

            props.setLoadSubmitBtn(true)
            props.setSubmitted(true)

            props.getDatas({ base: 'apiUser', url: `/io${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: parseInt(parsed.product_id, 10),
              pa_amanah: {
                name: values.insured_name ? (values.insured_name).toUpperCase() : '',
                phone_number: values.phone_number,
                phone_country_code: values.phone_country_code,
                phone_country_code_id: codeId[0].id,
                email: values.email,
                print_policy_book: values.print_policy_book === true,
                premi_calculation: {
                  job_class: values.job_class,
                  coverage: parseFloat(values.coverage),
                  discount_percentage: parseFloat(values.discount_percentage) || 0,
                  discount_currency: parseFloat(values.discount_currency) || 0,
                  premi: Number(props.premi.premi),
                },
              },
            }).then((res) => {
              props.setSubmitted(false)

              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-io/detail/${res.data.id}?product=${res.data.product.id}`)
                })
              }
            }).catch((error) => {
              props.setSubmitted(false)
              props.setLoadSubmitBtn(false)

              Swal.fire(
                error.message,
                '',
                'error',
              )
            })
          } else {
            props.setSubmitted(false)
            props.setLoadSubmitBtn(false)

            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },
    }),
    withHandlers({
      getDetailAmanah: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/io/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          console.log(res)
          props.setDetailAmanah({
            ...props.detailAmanah,
            loading: false,
            list: res.data,
          })

          props.handlePremi('coverage', res.data.pa_amanah_premi_calculation.coverage)
          props.handlePremi('discount_currency', parseFloat(res.data.pa_amanah_premi_calculation.discount_currency))
          props.handlePremi('discount_percentage', parseFloat(res.data.pa_amanah_premi_calculation.discount_percentage))
          props.handlePremi('job_class', res.data.pa_amanah_premi_calculation.job_class)
          props.handlePremi('premi', res.data.pa_amanah_premi_calculation.premi)

          props.setPremi({
            premi: res.data.pa_amanah_premi_calculation.premi,
            discount_percentage: res.data.pa_amanah_premi_calculation.discount_percentage,
            discount_currency: res.data.pa_amanah_premi_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })
        })
      },

      submitData: props => () => {
        props.onSubmit()
      },
    }),
    withPropsOnChange(
      ['handlePremi'],
      ({ handlePremi }) => ({
        handlePremi: _.debounce(handlePremi, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        const {
          match, getDetailAmanah, setDetailAmanah, detailAmanah, stateCountry, setStateCountry,
        } = this.props

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        if (match.params.id) {
          getDetailAmanah()
        } else {
          setDetailAmanah({
            ...detailAmanah,
            loading: false,
          })
          this.props.handlePremi()
        }
      },
    }),
  )(FormAmanah),
)