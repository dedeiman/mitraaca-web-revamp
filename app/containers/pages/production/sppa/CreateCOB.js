import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Form } from '@ant-design/compatible'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import COB from 'components/pages/production/sppa/CreateCOB'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import { filter } from 'lodash'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default Form.create({ name: 'createCOBForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateCategory', 'setStateCategory', {
      loading: false,
      list: [],
    }),
    withState('stateCategorySyariah', 'setStateCategorySyariah', {
      loading: false,
      list: [],
    }),
    withState('getProduct', 'setProduct', {
      loading: false,
      type: 'konvensional',
      list: [],
    }),
    withHandlers({
      handleProductType: props => (value) => {
        props.setProduct({
          ...props.getProduct,
          type: value,
        })
      },
      handleProduct: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/product/${id}?product_type=${props.getProduct.type}`, method: 'get' },
        ).then((res) => {
          props.setProduct({
            ...props.getProduct,
            loading: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setProduct({})
        })
      },
      onSubmit: props => (e) => {
        e.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            history.push(`/production-create-sppa/create?cob=${values.cob}&type=${values.type_product}&product=${values.product}&product_id=${values.product_id}`)
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('activePage', 'production-create-sppa')
        window.onresize = () => {
          this.props.getDatas(
            { base: 'apiUser', url: '/class-of-business', method: 'get' },
          )
        }

        this.props.getDatas(
          { base: 'apiUser', url: '/class-of-business', method: 'get' },
        ).then((res) => {
          this.props.setStateCategory({
            ...this.props.stateCategory,
            loading: false,
            list: res.data,
          })

          // Filter manual syariah product
          const listSyariah = []
          filter(res.data, (item) => {
            if (item.name !== 'Travel') if (item.name !== 'Liability') if (item.name !== 'Cargo') listSyariah.push(item)
          })
          this.props.setStateCategorySyariah({
            ...this.props.stateCategorySyariah,
            loading: false,
            list: listSyariah,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateCategory({})
        })
      },
    }),
  )(COB),
)
