/* eslint-disable arrow-body-style */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Form } from '@ant-design/compatible'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
  withPropsOnChange,
} from 'recompose'
import FormAsri from 'components/pages/production/sppa/asri/FormAsri'

import { getDatas } from 'actions/Option'
import history from 'utils/history'
import _ from 'lodash'
import Swal from 'sweetalert2'
import {
  fetchDetailProduct,
} from 'actions/Product'
import moment from 'moment'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
})

export default Form.create({ name: 'FormAsri' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('listIO', 'setListIO', {
      loading: true,
      data: [],
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('stateSelects', 'setStateSelects', {
      provinceLoad: false,
      provinceList: [],
      cityLoad: false,
      cityList: [],
      subDistrictLoad: false,
      subDistrictList: [],
      villageLoad: false,
      villageList: [],
      floorLoad: false,
      floorList: [
        { label: '1 Lantai', value: 1 },
        { label: '2 Lantai', value: 2 },
      ],
      constructLoad: false,
      constructList: [
        { label: 'Kelas 1', value: 1 },
        { label: 'Kelas 2', value: 2 },
      ],
      causeOfLossLoad: false,
      causeOfLossList: [],
      amountOfLossLoad: false,
      amountOfLossList: [],
    }),
    withState('causeOfLoss', 'setCauseOfLoss', {}),
    withState('amountOfLoss', 'setAmountOfLoss', {}),
    withState('detailData', 'setDetailData', {
      loading: false,
      list: [],
    }),
    // withState('documentData', 'setDocumentData', {
    //   loading: false,
    //   list: [],
    // }),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedIO', 'setSelectedIO', {}),
    withState('submitted', 'setSubmitted', false),
    withState('coverage', 'setCoverage', 0),
    withState('geoLocation', 'setGeoLocation', {
      address: '',
      lng: '',
      lat: '',
    }),
    withState('premi', 'setPremi', {
      premi: 0,
      rate: 0,
      stamp_fee: 0,
      policy_printing_fee: 0,
      total_payment: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withHandlers({
      initListData: props => (url, name) => {
        return props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch(() => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
    withHandlers({
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()

        const coverage = (formData.property_price ? parseFloat(formData.property_price) : 0) + (formData.furniture_price ? parseFloat(formData.furniture_price) : 0)
        props.setCoverage(coverage)

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            // eslint-disable-next-line radix
            product_id: parseInt(props.productID),
            premi_asri_calculation: {
              class: formData.class_construction || 0,
              coverage,
              coverage_period: 1,
              additional_limt: formData.additional_limits || [],
              city: formData.city_id,
              total_payment: formData.total_payment,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_asri_calculation
          props.form.setFieldsValue({
            premi: premiData.premi,
          })

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
      onChangeStateLocationAlternative: props => async (stateType, value) => {
        switch (stateType) {
          case 'province':
            props.setStateSelects({
              ...props.stateSelects,
              cityLoad: false,
              cityList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              city_id: undefined,
              sub_district_id: undefined,
              village_id: undefined,
            })

            return props.initListData(`/cities/${value}`, 'city')
          case 'city':
            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              sub_district_id: undefined,
              village_id: undefined,
            })

            return props.initListData(`/sub-district/${value}`, 'subDistrict')
          case 'subDistrict':

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              village_id: undefined,
            })

            return props.initListData(`/villages/${value}`, 'village')
          default:
            break
        }

        return true
      },
      clearAddress: props => () => {
        props.form.setFieldsValue({
          address: undefined,
          rt: undefined,
          rw: undefined,
          postal_code: undefined,
          lat: undefined,
          lng: undefined,
          earth_quake_area: undefined,
          float_area: undefined,
          province_id: undefined,
          city_id: undefined,
          sub_district_id: undefined,
          village_id: undefined,
        })
      },
      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${qs.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            data: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            data: [],
          })
        })
      },
    }),
    withHandlers({
      initDetailData: props => async () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then(async (res) => {
          const { data } = res

          console.log(data)

          props.setDetailData({
            ...props.detailData,
            loading: false,
            list: data,
          })

          props.setPremi({
            premi: res.data.asri_calculation.premi,
            rate: res.data.asri_calculation.rate,
            limit_policy_amount: res.data.asri_calculation.limit_policy_amount,
            discount_percentage: res.data.asri_calculation.discount_percentage,
            discount_currency: res.data.asri_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })

          props.setGeoLocation({
            address: data.asri_insured_object.address,
            lng: data.asri_insured_object.lng,
            lat: data.asri_insured_object.lat,
          })

          props.setSelectedHolder(data.policy_holder)
          props.setSelectedInsured(data.insured)

          const provinceId = data.asri_insured_object.province_id
          const cityId = data.asri_insured_object.city_id
          const subDistrictId = data.asri_insured_object.sub_district_id
          const villageId = data.asri_insured_object.village_id

          // props.onChangeStateLocation() // tadinya mau manggil onChangeStateLocation tapi ga bisa jadi terpaksa copy jadi onChangeStateLocationAlternative ... (gabisa manggil function dibawahnya ?)
          await props.onChangeStateLocationAlternative('province', provinceId)
          await props.onChangeStateLocationAlternative('city', cityId)
          await props.onChangeStateLocationAlternative('subDistrict', subDistrictId)

          props.form.setFieldsValue({
            city_id: cityId || undefined,
            sub_district_id: subDistrictId || undefined,
            village_id: villageId || undefined,
          })

          props.handleRenewal(data.sppa_type)

          // props.getPremi()
        })
      },
      submitData: props => async () => {
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const formData = values

            console.log(formData)

            if (props.selectedHolder.id) {
              formData.policy_holder_id = props.selectedHolder.id
            }

            if (props.selectedInsured.id) {
              formData.insured_id = props.selectedInsured.id
            }

            if (formData.have_property_loss && !_.isEmpty(formData.property_loss)) {
              const propertyLoss = []
              _.forEach(formData.property_loss, (item) => {
                propertyLoss.push({
                  // eslint-disable-next-line radix
                  year_of_incident: item.year_of_incident ? parseInt(item.year_of_incident) : 0,
                  cause_of_loss_code: item.cause_of_loss || '',
                  amount_of_loss_code: item.amount_of_loss  || '',
                })
              })

              formData.property_loss = propertyLoss
            }

            if (formData.start_period) {
              formData.start_period = moment(formData.start_period).format('YYYY-MM-DD')
            }

            if (formData.end_period) {
              formData.end_period = moment(formData.end_period).format('YYYY-MM-DD')
            }

            const payload = {
              product_id: Number(props.productID),
              asri: {
                sppa_type: formData.sppa_type,
                io_id: formData.io_id,
                renewal_id: formData.renewal_id,
                policy_holder_id: formData.policy_holder_id,
                insured_id: formData.insured_id,
                name_on_policy: (formData.name_on_policy || '').toUpperCase(),
                start_period: formData.start_period,
                end_period: formData.end_period,
                total_payment: formData.total_payment,
                insured_object: {
                  qq: (formData.qq || '').toUpperCase(),
                  same_with_insured_address: formData.same_with_insured_address,
                  address: (formData.address || '').toUpperCase(),
                  rt: formData.rt,
                  rw: formData.rw,
                  province_id: formData.province_id,
                  city_id: formData.city_id,
                  sub_district_id: formData.sub_district_id,
                  village_id: formData.village_id,
                  lat: Number(formData.lat),
                  lng: Number(formData.lng),
                  class_construction: formData.class_construction,
                  class_construction_description: formData.class_construction_description,
                  used_by: formData.used_by,
                  total_floor: Number(formData.total_floor),
                  postal_code: formData.postal_code,
                },
                property_loss: (values.property_loss || []),
                premi_calculation: {
                  building_area: Number(formData.building_area),
                  property_price: formData.property_price ? Number(formData.property_price) : 0,
                  furniture_price: formData.furniture_price ? Number(formData.furniture_price) : 0,
                  discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
                  discount_currency: formData.discount_currency ? Number(formData.discount_currency) : 0,
                  additional_limits: formData.additional_limits || [],
                  book_policy: formData.print_policy_book === true,
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
              },
            }

            if (formData.io_number && props.selectedIO.id) {
              if (formData.sppa_type === 'new') {
                payload.asri.io_id = props.selectedIO.id
              } else {
                payload.asri.renewal_id = props.selectedIO.id
              }
            }

            props.setLoadSubmitBtn(true)
            props.setSubmitted(true)

            const paramId = props.match.params.id

            await props.getDatas({ base: 'apiUser', url: `/insurance-letters${paramId ? `/${paramId}` : ''}`, method: `${paramId ? 'put' : 'post'}` }, payload).then((response) => {
              if (response) {
                const { data } = response
                Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan</span>', 'success').then(() => history.push(`/production-create-sppa/confirm-asri/${paramId ? `${paramId}` : data.id}`))
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              props.setSubmitted(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
            props.setLoadSubmitBtn(false)
            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },
      setFieldByIO: props => async (type, selectedData) => {
        console.log(selectedData)
        if (!selectedData) {
          props.setSelectedIO({})
        } else {
          props.setSelectedIO(selectedData)
        }

        let provinceId
        let cityId
        let subDistrictId
        let villageId

        const fieldData = {}

        if (type === 'new') {
          props.setGeoLocation({
            ...props.geoLocation,
            address: selectedData.asri_building_address.address,
            lat: selectedData.asri_building_address.lat,
            long: selectedData.asri_building_address.lng,
          })

          provinceId = selectedData.asri_building_address.province_id
          cityId = selectedData.asri_building_address.city_id
          subDistrictId = selectedData.asri_building_address.sub_district_id
          villageId = selectedData.asri_building_address.village_id

          fieldData.property_price = selectedData.asri_premi_calculation.building_price
          fieldData.furniture_price = selectedData.asri_premi_calculation.furniture_price
          fieldData.discount_percent = selectedData.asri_premi_calculation.discount_percentage
          fieldData.discount_nominal = selectedData.asri_premi_calculation.discount_currency
          fieldData.building_area = selectedData.asri_building_information.building_area
          fieldData.additional_limits = selectedData.asri_premi_calculation.additional_limits || []
          fieldData.class_construction = selectedData.asri_building_information.class_construction
          fieldData.class_construction_description = selectedData.asri_building_information.class_construction_description
          fieldData.address = selectedData.asri_building_address.address
          fieldData.rw = selectedData.asri_building_address.rw
          fieldData.rt = selectedData.asri_building_address.rt
          fieldData.lat = selectedData.asri_building_address.lat
          fieldData.lng = selectedData.asri_building_address.lng
          fieldData.postal_code = selectedData.asri_building_address.postal_code
          fieldData.same_with_insured_address = selectedData.asri_building_address.same_address_with_insured
          fieldData.earth_quake_area = selectedData.asri_building_address.city.earthquake_area ? 'YA' : 'TIDAK'
          fieldData.float_area = selectedData.asri_building_address.city.flood_area ? 'YA' : 'TIDAK'
          fieldData.total_floor = selectedData.asri_building_address.total_floor
          fieldData.loss_property = true
          fieldData.list_item = []
          fieldData.used_by = undefined
          fieldData.info_policy = {}
          fieldData.stamp_fee = selectedData.stamp_fee
          fieldData.total_price = selectedData.asri_premi_calculation.property_price + selectedData.asri_premi_calculation.furniture_price
          fieldData.floors = selectedData.asri_building_information.total_floor
        }

        props.form.setFieldsValue(fieldData)

        await props.onChangeStateLocationAlternative('province', provinceId)
        await props.onChangeStateLocationAlternative('city', cityId)
        await props.onChangeStateLocationAlternative('subDistrict', subDistrictId)

        props.form.setFieldsValue({
          province_id: provinceId || undefined,
          city_id: cityId || undefined,
          sub_district_id: subDistrictId || undefined,
          village_id: villageId || undefined,
        })
        props.getPremi()
      },
      setSameAddress: props => async () => {
        setTimeout(() => props.clearAddress())

        if (!_.keys(props.selectedHolder).length) return

        const provinceId = props.selectedHolder.province_id || undefined
        const cityId = props.selectedHolder.city_id || undefined
        const subDistrictId = props.selectedHolder.sub_district_id || undefined
        const villageId = props.selectedHolder.urban_village_id || undefined

        setTimeout(() => {
          props.setGeoLocation({
            ...props.geoLocation,
            address: props.selectedHolder.address,
            lat: props.selectedHolder.lat,
            long: props.selectedHolder.lng,
          })

          props.form.setFieldsValue({
            address: props.selectedHolder.address,
            rt: props.selectedHolder.rt,
            rw: props.selectedHolder.rw,
            postal_code: props.selectedHolder.postal_code,
            lat: props.selectedHolder.lat,
            lng: props.selectedHolder.lng,
            earth_quake_area: _.get(props.selectedHolder, 'city.earthquake_area', false) ? 'YA' : 'TIDAK',
            float_area: _.get(props.selectedHolder, 'city.flood_area', false) ? 'YA' : 'TIDAK',
          })
        })

        await props.onChangeStateLocationAlternative('province', provinceId)
        await props.onChangeStateLocationAlternative('city', cityId)
        await props.onChangeStateLocationAlternative('subDistrict', subDistrictId)

        props.form.setFieldsValue({
          province_id: provinceId || undefined,
          city_id: cityId || undefined,
          sub_district_id: subDistrictId || undefined,
          village_id: villageId || undefined,
        })
      },
      clearAddress: props => () => {
        setTimeout(() => {
          props.setGeoLocation({
            ...props.geoLocation,
            address: '',
            lat: '',
            long: '',
          })
          props.form.setFieldsValue({
            address: undefined,
            rt: undefined,
            rw: undefined,
            postal_code: undefined,
            lat: undefined,
            lng: undefined,
            earth_quake_area: undefined,
            float_area: undefined,
            province_id: undefined,
            city_id: undefined,
            sub_district_id: undefined,
            village_id: undefined,
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onChangeStateLocation: props => (stateType, value) => {
        switch (stateType) {
          case 'province':
            props.setStateSelects({
              ...props.stateSelects,
              cityLoad: false,
              cityList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              city_id: undefined,
              sub_district_id: undefined,
              village_id: undefined,
              float_area: undefined,
              earth_quake_area: undefined,
            })

            props.initListData(`/cities/${value}`, 'city')
            break
          case 'city':
            props.getPremi()

            props.setStateSelects({
              ...props.stateSelects,
              subDistrictLoad: false,
              subDistrictList: [],
            })

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            const city = (props.stateSelects.cityList).find(o => o.id === value) // eslint-disable-line

            props.form.setFieldsValue({
              sub_district_id: undefined,
              village_id: undefined,
              float_area: city.flood_area ? 'YA' : 'TIDAK',
              earth_quake_area: city.earthquake_area ? 'YA' : 'TIDAK',
            })

            props.initListData(`/sub-district/${value}`, 'subDistrict')
            break
          case 'subDistrict':

            props.setStateSelects({
              ...props.stateSelects,
              villageLoad: false,
              villageList: [],
            })

            props.form.setFieldsValue({
              village_id: undefined,
            })

            props.initListData(`/villages/${value}`, 'village')
            break
          default:
            break
        }
      },
    }),
    lifecycle({
      componentDidMount() {
       
        google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line

        const query = qs.parse(this.props.location.search)
        const productID = query.product_id

        this.props.setProductID(productID)

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        this.props.searchCustomer()

        this.props.initListData('/provinces', 'province')

        const config = [
          { url: '/cause-of-loss', name: 'causeOfLoss' },
          { url: '/amount-of-loss', name: 'amountOfLoss' },
        ]

        config.map((item) => {
          const { url, name } = item

          return this.props.getDatas(
            { base: 'apiUser', url, method: 'get' },
          ).then((res) => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: res.data,
            })
          }).catch(() => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: [],
            })
          })
        })
        

        if (this.props.match.params.id) {
          this.props.initDetailData()
        }
      },
      initAutocomplete() {
        const inputNode = document.getElementById('convert-geolocation')
        const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
        /* eslint-disable */
        window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
          if (event.key === 'Enter') {
            event.preventDefault()
          }
        })
        /* eslint-enable */
        autoComplete.addListener('place_changed', () => {
          const place = autoComplete.getPlace()
          const location = JSON.stringify(place.geometry.location)
          const data = JSON.parse(location)
          data.address = place.formatted_address
          this.props.setGeoLocation({
            address: data.address,
            lng: data.lng,
            lat: data.lat,
          })

          this.props.form.setFieldsValue({
            lng: data.lng,
            lat: data.lat,
          })
        })
      },
    }),
  )(FormAsri),
)
