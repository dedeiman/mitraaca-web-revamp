import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormCargo from 'components/pages/production/sppa/cargo/FormCargo'
import { getDatas } from 'actions/Option'
import _ from 'lodash'
import {
  fetchDetailProduct,
} from 'actions/Product'
import {
  createSPPA,
} from 'actions/CreateSPPACargo'
import moment from 'moment'
import history from 'utils/history'
import Swal from 'sweetalert2'
import queryString from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createSPPA: bindActionCreators(createSPPA, dispatch),
})

export default Form.create({ name: 'formCreateIo' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('customers', 'setCustumers', {
      loading: false,
      data: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('productID', 'setProductID', 0),
    withState('stateSelects', 'setStateSelects', {}),
    withState('productDetail', 'setproductDetail', {}),
    withState('detailData', 'setDetailData', {
      loading: false,
      list: [],
    }),
    withState('cargoTypeList', 'setCargoTypeList', []),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('submitted', 'setSubmitted', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedPolicyType', 'setSelectedPolicyType', {}),
    withState('selectedIO', 'setSelectedIO', {}),
    withState('premi', 'setPremi', {
      premi: 0,
      discount_percentage: 0,
      discount_amount: 0,
      admin_fee: 0,
      policy_printing_fee: 0,
      stamp_fee: 0,
      total_payment: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withState('exchangeRate', 'setExchangeRate', {
      idr_exchange_rate: 0,
    }),
    withState('optionsSPPAType', 'setOptionsSPPAType', [
      { value: 'new', label: 'New' },
      { value: 'renewal', label: 'Renewal' },
    ]),
    withState('listPolis', 'setListPolis', {
      loading: true,
      data: [],
    }),
    withHandlers({
      appendCustomer: props => (data) => {
        props.setCustumers({
          ...props.customers,
          data: [...props.customers.data, data],
        })
      },
      submitData: props => async () => {
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const formData = values
            formData.product_id = props.productID
            formData.premi = props.premi.premi
            if (props.selectedIO) {
              formData.io_id = props.selectedIO.id
            }

            if (props.selectedHolder) {
              formData.policy_holder_id = props.selectedHolder.id
            }

            if (props.selectedInsured) {
              formData.insured_id = props.selectedInsured.id
            }

            if (formData.est_time_departure) {
              formData.est_time_departure = moment(formData.est_time_departure).format('YYYY-MM-DD')
            }
            props.setSubmitted(true)
            props.setLoadSubmitBtn(true)

            const paramId = props.match.params.id
            const payload = {
              product_id: parseFloat(formData.product_id),
              cargo: {
                sppa_type: formData.sppa_type,
                io_id: formData.io_id,
                policy_holder_id: formData.policy_holder_id,
                insured_id: formData.insured_id,
                name_on_policy: (formData.name_on_policy || '').toUpperCase(),
                qq: formData.qq,
                print_policy_book: formData.print_policy_book,
                vessel_information: {
                  cargo_type_id: formData.cargo_type_id,
                  vehicle_type_id: formData.vehicle_type_id,
                  vehicle_id: formData.vehicle_id,
                },
                cargo_information: {
                  policy_type_id: formData.policy_type_id,
                  bl_or_awb_number: (formData.bl_or_awb_number || '').toUpperCase(),
                  lc_number: (formData.lc_number || '').toUpperCase(),
                  invoice_number: (formData.invoice_number || '').toUpperCase(),
                  interest_detail: (formData.interest_detail || '').toUpperCase(),
                  interest_quantity: parseFloat(formData.interest_quantity),
                  currency_id: formData.currency_id,
                  sum_insured: parseFloat(formData.sum_insured),
                  voyage_number: (formData.voyage_number || '').toUpperCase(),
                  est_time_departure: formData.est_time_departure,
                  main_coverage: "ICC 'A'",
                  other_information: formData.other_information,
                  voyage_from: (formData.voyage_from || '').toUpperCase(),
                  voyage_to: (formData.voyage_to || '').toUpperCase(),
                  transhipment: (formData.transhipment || '').toUpperCase(),
                  destination_country_id: formData.destination_country_id || '',
                  origin_port_id: formData.origin_port_id || '',
                  destination_port_id: formData.destination_port_id || '',
                },
                cargo_premi_calculation: {
                  rate: parseFloat(formData.rate),
                  premi: parseFloat(formData.premi),
                  discount_percentage: parseFloat(formData.discount_percentage),
                  discount_amount: parseFloat(formData.discount_amount),
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
              },
            }

            await props.getDatas({ base: 'apiUser', url: `/insurance-letters${paramId ? `/${paramId}` : ''}`, method: `${paramId ? 'put' : 'post'}` }, payload).then((response) => {
              if (response) {
                const { data } = response
                Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan</span>', 'success').then(() => history.push(`/production-create-sppa/confirm-cargo/${paramId ? `${paramId}` : data.id}`))
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              props.setSubmitted(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
            props.setLoadSubmitBtn(false)
            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      getPreviousPolicy: props => (key, type) => {
        props.setListPolis({
          ...props.listPolis,
          loading: true,
        })

        const payload = {
          sppa_type: type || '',
          product_id: props.productID,
          document_number: key || '',
        }
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?${queryString.stringify(payload)}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListPolis({
            loading: false,
            data: res.data || [],
          })
        }).catch(() => {
          props.setListPolis({
            loading: false,
            data: [],
          })
        })
      },
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const convertExchange = (props.stateSelects.currencyList || []).find(item => item.id === formData.currency_id).idr_exchange_rate * formData.sum_insured
        props.setExchangeRate({
          ...props.exchangeRate,
          idr_exchange_rate: convertExchange,
        })

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            // eslint-disable-next-line radix
            product_id: parseInt(props.productID),
            premi_cargo_calculation: {
              currency_id: formData.currency_id,
              sum_insured: parseFloat(formData.sum_insured),
              rate: parseFloat(formData.rate),
              discount_percentage: parseFloat(formData.discount_percentage),
              discount_amount: parseFloat(formData.discount_amount),
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_cargo_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
    }),
    withHandlers({
      setFieldByIO: props => async (type, selectedData) => {
        if (!selectedData) {
          props.setSelectedIO({})
        } else {
          props.setSelectedIO(selectedData)
        }

        const fieldData = {}
        if (type === 'new') {
          fieldData.currency_id = _.get(selectedData, 'cargo_information.currency.id', undefined)
          // fieldData.other_information = _.get(selectedData, 'cargo_information.other_information', undefined)
          fieldData.sum_insured = _.get(selectedData, 'cargo_information.sum_insured', undefined)
          fieldData.rate = _.get(selectedData, 'cargo_premi_calculation.rate', undefined)
          fieldData.discount_percentage = _.get(selectedData, 'cargo_premi_calculation.discount_percentage', undefined)
          // fieldData.discount_amount = _.get(selectedData, 'cargo_premi_calculation.discount_amount', undefined)

          fieldData.cargo_category_id = _.get(selectedData, 'vessel_information.cargo_type.category.id', undefined)
          fieldData.vehicle_type_id = _.get(selectedData, 'vessel_information.vehicle_type.id', undefined)
          fieldData.print_policy_book = _.get(selectedData, 'print_policy_book', undefined)
        }

        props.form.setFieldsValue(fieldData)

        if (fieldData.cargo_category_id) {
          const cargoTypes = props.stateSelects.cargoTypeList.filter(o => o.category.id === fieldData.cargo_category_id)
          props.setCargoTypeList(cargoTypes)
          setTimeout(() => props.form.setFieldsValue({ cargo_type_id: _.get(selectedData, 'vessel_information.cargo_type.id', undefined) }))
        }

        props.form.setFieldsValue({
          cargo_type_id: _.get(selectedData, 'vessel_information.cargo_type.id', undefined),
        })

        props.getPremi()
      },
      initDetailData: props => async () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then(async (res) => {
          const { data } = res

          props.setDetailData({
            ...props.detailData,
            loading: false,
            list: data,
          })

          props.setSelectedHolder(data.policy_holder)
          props.setSelectedInsured(data.insured)

          setTimeout(() => props.getPremi())
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
    }),

    lifecycle({
      componentWillMount() {
        const query = queryString.parse(this.props.location.search)
        const productID = query.product_id

        this.props.setProductID(productID)

        const config = [
          { url: '/cargo-policy-types', name: 'policyType' },
          { url: '/cargo-types', name: 'cargoType' },
          { url: '/cargo-categories', name: 'cargoCategory' },
          { url: '/vehicle-types', name: 'vehicleType' },
          { url: '/vehicles', name: 'vehicle' },
          { url: '/currencies', name: 'currency' },
          { url: '/countries', name: 'country' },
          { url: '/ports', name: 'port' },
        ]

        config.map(item => (
          this.loadMasterData(item.url, item.name)
        ))

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        // Ini init customers harusnya sih get ketika search , ini sementawis aja.
        this.props.getDatas(
          { base: 'apiUser', url: '/customers?&per_page=', method: 'get' },
          null,
        ).then((res) => {
          this.props.setCustumers({
            loading: false,
            data: res.data,
          })
        }).catch(() => {
          this.props.setCustumers({
            loading: false,
            options: [],
          })
        })
      },

      componentDidMount() {
        if (this.props.match.params.id) {
          this.props.initDetailData()
        }
      },

      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch(() => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
  )(FormCargo),
)
