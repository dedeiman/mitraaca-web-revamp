import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import FormAmanah from 'components/pages/production/sppa/amanah/FormAmanah'
import history from 'utils/history'
import queryString from 'query-string'
import Swal from 'sweetalert2'
import { isEmpty } from 'lodash'
import moment from 'moment'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formSppaAmanah' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('premi', 'setPremi', {
      premi: 0,
      discount_percentage: 0,
      discount_currency: 0,
      stamp_fee: 0,
      admin_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('submitted', 'setSubmitted', false),
    withState('relationships', 'setRelationships', {
      loading: false,
      disabled: false,
      options: [],
    }),
    withState('premiPayload', 'setPremiPayload', {
      coverage: 0,
      job_class: '',
      discount_percentage: 0,
      discount_currency: 0,
    }),
    withState('heirsState', 'setHeirsState', {
      loading: false,
      disabled: false,
      heirsCount: [{
        id: 0,
        name: '',
        phone_number: '',
        relationship: {},
      }],
    }),
    withState('listIO', 'setListIO', {
      loading: true,
      options: [],
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('selectedIO', 'setSelectedIO'),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('loadCreateSppa', 'setLoadCreateSppa', false),
    withState('detailAmanah', 'setDetailAmanah', {
      loading: true,
      list: {},
    }),
    withState('costumerDetail', 'setCostumerDetail', null),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withHandlers({
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      handlePremi: props => (key, value) => {
        const parsed = queryString.parse(window.location.search)

        props.setPremiPayload({
          ...props.premiPayload,
          [key]: value,
        })

        setTimeout(() => {
          props.getDatas(
            { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' }, {
              product_id: Number(parsed.product_id),
              premi_pa_amanah_calculation: {
                ...props.premiPayload,
                [key]: value,
              },
            },
          ).then((res) => {
            props.setPremi({
              premi: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.premi : 0,
              discount_currency: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.discount_currency : 0,
              discount_percentage: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.discount_percentage : 0,
              stamp_fee: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.stamp_fee : 0,
              total_payment: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.total_payment : 0,
              policy_printing_fee: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.policy_printing_fee : 0,
              max_discount: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.max_discount : 0,
              max_out_go: res.data.premi_pa_amanah_calculation ? res.data.premi_pa_amanah_calculation.max_out_go : 0,
            })

          })
        }, 300)
      },

      fetchIdCustomer: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/customers/${id}`, method: 'get' },
          null,
        ).then((res) => {
          props.setCostumerDetail({
            ...res.data,
          })
        })
      },

      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },

      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${queryString.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            options: [],
          })
        })
      },

      removeHeirs: props => (index) => {
        const dataEl = props.heirsState.heirsCount.filter(indexEl => indexEl.id !== index)

        props.setHeirsState({
          heirsCount: dataEl,
        })
      },

      handleHeirs: props => () => {
        if (props.heirsState.heirsCount.length < 3) {
          props.setHeirsState({
            loading: false,
            disabled: false,
            heirsCount: props.heirsState.heirsCount.concat({
              id: moment().milliseconds(),
              name: '',
              phone_number: '',
              relationship: {},
            }),
          })
        } else {
          props.setHeirsState({
            disabled: true,
            heirsCount: props.heirsState.heirsCount,
          })
        }
      },

      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },

      onSubmit: props => async () => {
        const parsed = queryString.parse(window.location.search)
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          const heirs = []

          if (params.id) {
            if (heirs.length === 0) {
              props.heirsState.heirsCount.map(item => (
                heirs.push({
                  name: item.name,
                  relationship_id: item.relationship.id,
                  phone_number: item.phone_number,
                })
              ))
            }

            if (!isEmpty(values.hubungan_0) && isEmpty(heirs[0].relationship_id)) {
              heirs.splice(0, 0, {
                name: values.nama_0,
                relationship_id: values.hubungan_0,
                phone_number: values.telp_0,
              })
            }

            if (!isEmpty(values.hubungan_1) && isEmpty(heirs[1].relationship_id)) {
              heirs.splice(1, 0, {
                name: values.nama_1,
                relationship_id: values.hubungan_1,
                phone_number: values.telp_1,
              })
            }

            if (!isEmpty(values.hubungan_2) && isEmpty(heirs[2].relationship_id)) {
              heirs.splice(2, 1, {
                name: values.nama_2,
                relationship_id: values.hubungan_2,
                phone_number: values.telp_2,
              })
            }
          } else {
            if (!isEmpty(values.hubungan_0)) {
              heirs.push({
                name: values.nama_0,
                relationship_id: values.hubungan_0,
                phone_number: values.telp_0,
              })
            }

            if (!isEmpty(values.hubungan_1)) {
              heirs.push({
                name: values.nama_1,
                relationship_id: values.hubungan_1,
                phone_number: values.telp_1,
              })
            }

            if (!isEmpty(values.hubungan_2)) {
              heirs.push({
                name: values.nama_2,
                relationship_id: values.hubungan_2,
                phone_number: values.telp_2,
              })
            }
          }

          props.setLoadCreateSppa(true)
          if (!err) {
            props.getDatas({ base: 'apiUser', url: `/insurance-letters${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: Number(parsed.product_id),
              pa_amanah: {
                sppa_type: values.sppa_type,
                io_id: values.sppa_type === 'new' && values.io_renewal !== '' ? props.selectedIO.id : null,
                renewal_id: values.sppa_type === 'renewal' && values.io_renewal !== '' ? props.selectedIO.id : null,
                policy_holder_id: props.selectedHolder.id,
                insured_id: props.selectedInsured.id,
                name_on_policy: values.name_on_policy,
                start_period: moment(values.start_period).format('YYYY-MM-DD'),
                end_period: moment(values.end_period).format('YYYY-MM-DD'),
                print_policy_book: values.print_policy_book === true,
                beneficiary_informations: heirs,
                premi_calculation: {
                  job_class: values.job_class,
                  coverage: parseFloat(values.coverage),
                  discount_percentage: 0,
                  discount_currency: values.discount_currency === undefined ? 0 : values.discount_currency,
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
              },
            }).then((res) => {
              props.setLoadCreateSppa(false)
              props.setSubmitted(false)

              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Data berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-sppa/confirm/${res.data.id}?product=${res.data.product.code}`)
                })
              }
            }).catch((error) => {
              props.setLoadCreateSppa(false)
              props.setSubmitted(false)

              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            props.setLoadCreateSppa(false)
            props.setSubmitted(false)

            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },

      getRelationships: props => () => {
        props.setRelationships({
          loading: true,
          disabled: true,
          options: [],
        })

        props.getDatas({
          base: 'apiUser', url: '/relationships', method: 'get',
        }).then((res) => {
          props.setRelationships({
            loading: false,
            disabled: false,
            options: res.data.length !== 0 ? res.data.map(item => ({
              value: item.id, label: item.name,
            })) : [],
          })
        }).catch((error) => {
          props.setRelationships({
            loading: false,
            disabled: false,
            options: [],
          })

          Swal.fire(
            '',
            `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
            'error',
          )
        })
      },
    }),
    withHandlers({
      getDetailAmanah: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setDetailAmanah({
            ...props.detailAmanah,
            loading: false,
            list: res.data,
          })

          props.handlePremi('coverage', res.data.pa_amanah_premi_calculation.coverage)
          props.handlePremi('discount_currency', parseFloat(res.data.pa_amanah_premi_calculation.discount_currency))
          props.handlePremi('discount_percentage', parseFloat(res.data.pa_amanah_premi_calculation.discount_percentage))
          props.handlePremi('job_class', res.data.pa_amanah_premi_calculation.job_class)
          props.handlePremi('premi', res.data.pa_amanah_premi_calculation.premi)

          props.setSelectedInsured(res.data.insured)
          props.setSelectedHolder(res.data.policy_holder)
          props.setHeirsState({
            loading: false,
            disabled: false,
            heirsCount: res.data.pa_amanah_beneficiary_information,
          })
        })
      },

      submitData: props => () => {
        props.setSubmitted(true)
        props.onSubmit()
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          getRelationships, searchCustomer, match, getDetailAmanah, setDetailAmanah, detailAmanah, stateCountry, setStateCountry,
        } = this.props

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          setStateCountry({
            ...stateCountry,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          setStateCountry({
            ...stateCountry,
            load: false,
            list: [],
          })
        })

        getRelationships()
        searchCustomer()

        if (match.params.id) {
          getDetailAmanah()
        } else {
          setDetailAmanah({
            ...detailAmanah,
            loading: false,
          })
          this.props.handlePremi()
        }
      },
    }),
  )(FormAmanah),
)
