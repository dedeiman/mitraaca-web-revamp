import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
} from 'recompose'
import UploadWellwomen from 'components/pages/production/sppa/wellwomen/UploadWellwomen'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default Form.create({ name: 'formUploadOtomate' })(
  compose(
    connect(
      mapStateToProps,

      mapDispatchToProps,
    ),
    withState('file', 'setFile', ''),
    withState('loadUploadSppa', 'setLoadUploadSppa', false),
    withState('docList', 'setDocList', [{
      label: '',
      id: 0,
      description: '',
      value: '',
    }]),
    withHandlers({
      handleUpload: props => (info, id) => {
        Helper.getBase64(info.file, (file) => {
          const tempData = props.docList
          tempData[id].value = file
          props.setDocList(tempData)
        })
      },
      handleInput: props => (e, id) => {
        const tempData = props.docList
        tempData[id].description = e.target.value
        props.setDocList(tempData)
      },
      onSubmit: props => (event) => {
        event.preventDefault()

        props.setLoadUploadSppa(true)

        props.form.validateFields((err) => {
          if (!err) {
            const bodyFormData = {
              documents: props.docList.map(key => ({
                type_id: 1,
                description: key.description,
                file: key.value,
              })),
            }

            props.getDatas(
              { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}/documents`, method: 'post' },
              bodyFormData,
            ).then(() => {
              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Upload Document Success!</span>',
                'success',
              ).then(() => {
                history.push(`/production-create-sppa/detail/${props.match.params.id}`)
              })
            }).catch((error) => {
              let messageErr = error
              if (error.response && error.response.data.meta) {
                messageErr = error.response.data.meta.message
              }
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${messageErr}</span>`, 'error')

              props.setLoadUploadSppa(false)
            })
          } else {
            props.form.validateFields(async (errs) => {
              if (errs) {
                props.setLoadUploadSppa(false)

                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
                  'error',
                )
              }
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('activePage', 'production-create-io')
      },
    }),
  )(UploadWellwomen),
)
