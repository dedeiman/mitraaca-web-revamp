import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import Swal from 'sweetalert2'
import queryString from 'query-string'
import history from 'utils/history'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import CreateWellWomen from 'components/pages/production/sppa/wellwomen/CreateWellWomen'
import API from 'utils/API'
import config from 'app/config'
import { isString, isEmpty } from 'lodash'


const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateIO' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('listIO', 'setListIO', {
      loading: true,
      options: [],
    }),
    withState('detailWellwomen', 'setDetailWellwomen', {
      loading: true,
      list: [],
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('listRelationship', 'setListRelationship', {
      loading: true,
      options: [],
    }),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedRelationship', 'setSelectedRelationship', {}),
    withState('selectedIO', 'setSelectedIO', {}),
    withState('premi', 'setPremi', {
      premi: 0,
      policy_printing_fee: 0,
      discount_percentage: 0,
      discount_currency: 0,
      stamp_fee: 0,
      admin_fee: 0,
      total_payment: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const parsed = queryString.parse(window.location.search)

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            // eslint-disable-next-line radix
            product_id: parseInt(parsed.product_id),
            premi_wellwoman_calculation: {
              insured_dob: !isEmpty(formData.birth_date) ? formData.birth_date.format('YYYY-MM-DD') : undefined,
              plan: `${formData.plan}`,
              start_period: !isEmpty(formData.start_period) ? formData.start_period.format('YYYY-MM-DD') : undefined,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_amount: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_wellwoman_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      setFieldByIO: props => async (type, selectedData) => {
        console.log(selectedData)
        if (!selectedData) {
          props.setSelectedIO({})
        } else {
          props.setSelectedIO(selectedData)
          
        }

        const fieldData = {}
        if (type === 'new') {
          // fieldData.policy_holder_name = selectedData.name
          // fieldData.travel_destination_id = selectedData.travel_information.travel_destination.id
          // fieldData.passport_number = selectedData.travel_information.passport_number
          // fieldData.destination_countries = selectedData.travel_information.destination_countries
          // fieldData.hometown_id = selectedData.travel_information.hometown.id
          // fieldData.heir_name = selectedData.travel_information.heir_name
          // fieldData.relationship_id = selectedData.travel_information.relationship.id
          // fieldData.is_annual = selectedData.travel_information.is_annual
          // fieldData.destination_country_ids = selectedData.travel_information.destination_countries.map(o => (o.id))
          // fieldData.departure_date = moment(selectedData.travel_information.departure_date).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD') ? '' : moment(selectedData.travel_information.departure_date)
          // fieldData.return_date = moment(selectedData.travel_information.return_date).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD') ? '' : moment(selectedData.travel_information.return_date)
          // fieldData.plan_type = selectedData.travel_information.plan_type
        }

        props.form.setFieldsValue(fieldData)

        props.getPremi()
      },
      getDetailWellwomen: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setSelectedInsured(res.data.insured)
          props.setSelectedHolder(res.data.policy_holder)
          props.setDetailWellwomen({
            ...props.detailWellwomen,
            loading: false,
            list: res.data,
          })

          props.setPremi({
            premi: res.data.wellwoman_premi_calculation.premi,
            discount_percentage: res.data.wellwoman_premi_calculation.discount_percentage,
            discount_currency: res.data.wellwoman_premi_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })

        })
      },
      onSubmit: props => (e) => {
        const parsed = queryString.parse(window.location.search)
        if (e !== undefined) {
          e.preventDefault()
        }
        props.form.validateFields(async (err, values) => {
          if (!err) {
            props.setLoadSubmitBtn(true)
            if (values.discount_currency !== undefined && isString(values.discount_currency)) {
              // eslint-disable-next-line no-param-reassign
              values.discount_currency = values.discount_currency.replace('Rp. ', '')
              // eslint-disable-next-line no-param-reassign
              values.discount_currency = values.discount_currency.replace(/,/g, '')
            }
            const { params } = props.match
            API[`${params.id ? 'put' : 'post'}`](`${config.api_url}/insurance-letters${params.id ? `/${params.id}` : ''}`, {
              product_id: Number(parsed.product_id),
              wellwoman: {
                sppa_type: values.sppa_type,
                // eslint-disable-next-line no-nested-ternary
                io_id: values.sppa_type === 'renewal' ? null : props.selectedIO.id === '' ? null : props.selectedIO.id,
                // eslint-disable-next-line no-nested-ternary
                renewal_id: values.sppa_type === 'new' ? null : props.selectedIO.id === '' ? null : props.selectedIO.id,
                policy_holder_id: props.selectedHolder.id,
                insured_id: props.selectedInsured.id,
                name_on_policy: values.name_on_policy ? (values.name_on_policy).toUpperCase() : '',
                start_period: values.start_period.format('YYYY-MM-DD'),
                end_period: values.end_period.format('YYYY-MM-DD'),
                insured_dob: values.birth_date.format('YYYY-MM-DD'),
                print_policy_book: values.print_policy_book,
                benefeciary_information: {
                  name: values.name_of_heir ? (values.name_of_heir).toUpperCase() : '',
                  relationship_id: values.heir_relation,
                },
                premi_calculation: {
                  discount_percentage: parseFloat(values.discount_percentage),
                  discount_currency: parseFloat(values.discount_currency),
                  plan: `${values.plan}`,
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
                insured_health_statement: {
                  first_answer: !!values.healthy_question_1_answer,
                  first_answer_description: values.healthy_question_1_esay,
                  second_answer: !!values.healthy_question_2_answer,
                  second_answer_description: values.healthy_question_2_esay,
                  third_answer: !!values.healthy_question_3_answer,
                  third_answer_description: values.healthy_question_3_esay,
                  fourth_answer: !!values.healthy_question_4_answer,
                  fourth_answer_description: values.healthy_question_4_esay,
                },
              },
            }).then((res) => {
              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Wellwoman SPPA berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-sppa/confirm/${params.id ? params.id : res.data.data.id}?product=${parsed.product}`)
                })
              }
            }).catch((error) => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })

            // }).then((res) => {
            //   if (res) {
            //     props.getDatas(
            //       { base: 'apiUser', url: `/insurance-letters/${params.id ? params.id : res.data.data.id}/check-documents`, method: 'get' },
            //       null,
            //     ).then((response) => {
            //       Swal.fire(
            //         '',
            //         '<span style="color:#2b57b7;font-weight:bold">Wellwomen SPPA berhasil disimpan!</span>',
            //         'success',
            //       ).then(() => {
            //         history.push(`/production-create-sppa/confirm/${params.id ? params.id : response.data.data.id}?product=${parsed.product}`)
            //       })
            //     }).catch((error) => {
            //       props.setLoadSubmitBtn(false)
            //       Swal.fire(
            //         '',
            //         `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
            //         'error',
            //       )
            //     })
            //   }
            // }).catch((error) => {
            //   props.setLoadSubmitBtn(false)
            //   Swal.fire(
            //     '',
            //     `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
            //     'error',
            //   )
            // })
            
          } else {
            props.setLoadSubmitBtn(false)
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },
      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${queryString.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            options: [],
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      submitData: props => (e) => {
        props.onSubmit(e)
      },
    }),
    lifecycle({
      componentDidMount() {
       
        this.props.searchCustomer()
        this.props.getDatas(
          { base: 'apiUser', url: '/relationships', method: 'get' },
          null,
        ).then((res) => {
          this.props.setListRelationship({
            ...this.props.listRelationship,
            loading: false,
            options: res.data.reverse().map(item => ({ value: item.id, label: item.name })) || [],
          })
        }).catch(() => {
          this.props.setListRelationship({
            ...this.props.listRelationship,
            loading: false,
            options: [],
          })
        })

        if (this.props.match.params.id) {
          this.props.getDetailWellwomen()
        } else {
          this.props.setDetailWellwomen({
            ...this.props.detailOtomate,
            loading: false,
          })
        }
      },
    }),
  )(CreateWellWomen),
)
