import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import {
  createCustomer,
} from 'actions/Customer'
import { Form } from '@ant-design/compatible'
import ModalAddCust from 'components/pages/production/sppa/ModalAddCustomer'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import { capitalize, isEmpty } from 'lodash'
import moment from 'moment'
import { message } from 'antd'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  createCustomer: bindActionCreators(createCustomer, dispatch),
})

export default Form.create({ name: 'formModalAddCutomer' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('visibleAddCust', 'setVisibleAddCust', false),
    withState('listFundSources', 'setListFundSources', {
      loading: true,
      options: [],
    }),
    withState('listInstitutionTypes', 'setListInstitutionTypes', {
      loading: true,
      options: [],
    }),
    withState('listStatuses', 'setListStatuses', {
      loading: true,
      options: [],
    }),
    withState('listBusinessPurposes', 'setListBusinessPurposes', {
      loading: true,
      options: [],
    }),
    withState('listBusinessTypes', 'setListBusinessTypes', {
      loading: true,
      options: [],
    }),
    withState('listEmployes', 'setListEmployes', {
      loading: true,
      options: [],
    }),
    withState('listRangeOfIncomes', 'setListRangeOfIncomes', {
      loading: false,
      options: [],
    }),
    withState('listProvincy', 'setListProvincy', {
      loading: true,
      options: [],
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('stateCountryCode', 'setStateCountryCode', {
      load: true,
      list: [],
    }),
    withState('listVillages', 'setListVillages', {
      loading: true,
      options: [],
    }),
    withState('listCities', 'setListCities', {
      loading: false,
      disabled: true,
      options: [],
    }),
    withState('listSubDistrict', 'setListSubDistrict', {
      loading: false,
      disabled: true,
      options: [],
    }),
    withState('state', 'changeState', {
      profile_id: '',
      id_no: '',
      tax_id_no: '',
      is_existing_core_customer: false,
    }),
    withHandlers({
      handleProfileId: props => async (value) => {
        var npwp = value
        npwp = npwp.split('-').join('')
        npwp = npwp.split('.').join('')
        npwp = npwp.split(' ').join('')
        const payload = {
          profile_id: '',
          id_no: '',
          tax_id_no: npwp,
        }
        props.getDatas(
          { base: 'apiUser', url: '/check-agent-core', method: 'post' },
          payload,
        ).then((res) => {
          props.form.setFields({
            address: {
              value: res.data.address,
            },
            number_id: {
              value: res.data.ktp_no,
            },
            pic_phone_number: {
              value: res.data.mobile,
            },
            name: {
              value: res.data.name,
            },
            ref_sys_user_id: {
              value: res.data.ref_sys_user_id,
            },
            sys_user_id: {
              value: res.data.sys_user_id,
            },
          })
          props.changeState({
            ...props.state,
            is_existing_core_customer: true,
          })
        }).catch((err) => {
          props.changeState({
            ...props.state,
            is_existing_core_customer: false,
          })
        })
      },
    }),
    withHandlers({
      showModal: props => () => {
        async function modalProccess() {
          props.setVisibleAddCust(true)
        }

        modalProccess().then(() => {
          const inputNode = document.getElementById('formModalAddCutomer_address')
          const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
          /*eslint-disable */
          window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
            if (event.key === 'Enter') {
              event.preventDefault()
            }
          })
          /* eslint-enable */
          autoComplete.addListener('place_changed', () => {
            const { setFieldsValue } = props.form
            const place = autoComplete.getPlace()
            const location = JSON.stringify(place.geometry.location)
            const data = JSON.parse(location)
            data.address = place.formatted_address

            setFieldsValue({
              lat: data.lat,
              lng: data.lng,
              address: data.address.toUpperCase(),
            })
          })
        })
      },

      handleSelectResidence: props => (key, value) => {
        let type = 'cities'
        if (key === 'city_id' || key === 'city') type = 'sub-district'
        if (key === 'sub_district_id') type = 'villages'
        const keyType = type !== 'sub-district' ? capitalize(type) : 'SubDistrict'

        props[`setList${keyType}`]({
          ...props[`list${keyType}`],
          loading: true,
        })

        props.getDatas(
          { base: 'apiUser', url: `/${type}/${value}`, method: 'get' },
          null,
        ).then((res) => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            disabled: false,
            options: keyType === 'Cities'
              ? res.data.map(item => ({
                value: item.id,
                label: item.name,
                flood_area: item.flood_area,
                earthquake_area: item.earthquake_area,
              })) : res.data.map(item => ({
                value: item.id,
                label: item.name,
              })),
          })
        }).catch(() => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            options: [],
          })
        })
      },

      submitAddCustomer: props => async () => {
        /*eslint-disable */
        event.preventDefault()
        /* eslint-enable */
        props.form.validateFields(async (err, values) => {
          console.log(values)
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            let npwp = isEmpty(values.npwp) ? '' : values.npwp
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)
            const picCodeId = (props.stateCountry.list).filter(code => code.phone_code === values.pic_phone_country_code)
            npwp = npwp.split('-').join('')
            npwp = npwp.split('.').join('')
            npwp = npwp.split(' ').join('')

            const payload = {
              ...values,
              number_id: values.number_id ? (values.number_id).toUpperCase() : '',
              pic_name: values.pic_name ? (values.pic_name).toUpperCase() : '',
              pic_phone_country_code: picCodeId[0].id,
              pic_phone_number: values.pic_phone_number,
              other_employment: values.other_employment ? (values.other_employment).toUpperCase() : '',
              birthplace: values.birthplace ? (values.birthplace).toUpperCase() : '',
              name: values.name ? (values.name).toUpperCase() : '',
              dob: values.dob ? moment(values.dob).format('YYYY-MM-DD') : '',
              lat: values.lat ? Number(values.lat) : 0,
              lng: values.lng ? Number(values.lng) : 0,
              phone_code_id: codeId[0].id,
              is_existing_core_customer: props.state.is_existing_core_customer,
              expiry_date_identity_id: !values.expiry_date_identity_id ? '' : moment(values.expiry_date_identity_id).format('YYYY-MM-DD'),
              npwp,
            }

            props.createCustomer(payload)
            .then((response) => {
              if (props.onSuccess) {
                props.onSuccess(response) // jangan dihapus ya ini buat handle customer data baru setelah submit,
              }
              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Customer berhasil ditambahkan!</span>',
                'success',
              ).then(() => {
                props.form.resetFields()
                props.setVisibleAddCust(false)
              })
            })
            .catch(error => (
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
            ))
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {

        setTimeout(() => {
          window.google.maps.event.addDomListener(window, 'load', this.props.modalProccess)
        }, 5000)
      },

      componentWillMount() {
        const {
          listFundSources,
          listInstitutionTypes,
          listBusinessPurposes,
          listBusinessTypes,
          listStatuses,
          listProvincy,
          listEmployes,
          setListStatuses,
          setListProvincy,
          setListEmployes,
          listRangeOfIncomes,
          setListRangeOfIncomes,
          setListFundSources,
          setListInstitutionTypes,
          setListBusinessPurposes,
          setListBusinessTypes,
        } = this.props

        this.props.getDatas(
          { base: 'apiUser', url: '/fund-sources', method: 'get' },
          null,
        ).then((res) => {
          setListFundSources({
            ...listFundSources,
            loading: false,
            options: res.data.map(item => ({ id: item.id, name: (item.name).toUpperCase() })),
          })
        }).catch(() => {
          setListFundSources({
            ...listFundSources,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/institution-types', method: 'get' },
          null,
        ).then((res) => {
          setListInstitutionTypes({
            ...listInstitutionTypes,
            loading: false,
            options: res.data.map(item => ({ id: item.id, name: (item.name).toUpperCase() })),
          })
        }).catch(() => {
          setListInstitutionTypes({
            ...listInstitutionTypes,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/business-purposes', method: 'get' },
          null,
        ).then((res) => {
          setListBusinessPurposes({
            ...listBusinessPurposes,
            loading: false,
            options: res.data.map(item => ({ id: item.id, name: (item.name).toUpperCase() })),
          })
        }).catch(() => {
          setListBusinessPurposes({
            ...listBusinessPurposes,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/business-types', method: 'get' },
          null,
        ).then((res) => {
          setListBusinessTypes({
            ...listBusinessTypes,
            loading: false,
            options: res.data.map(item => ({ id: item.id, name: (item.name).toUpperCase() })),
          })
        }).catch(() => {
          setListBusinessTypes({
            ...listBusinessTypes,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/provinces', method: 'get' },
          null,
        ).then((res) => {
          setListProvincy({
            ...listProvincy,
            loading: false,
            options: res.data.map(item => ({ value: item.id, label: item.name })),
          })
        }).catch(() => {
          setListProvincy({
            ...listProvincy,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/marital-statuses', method: 'get' },
          null,
        ).then((res) => {
          setListStatuses({
            ...listStatuses,
            loading: false,
            options: res.data.map(item => ({ value: item.name, label: item.display_name })),
          })
        }).catch(() => {
          setListStatuses({
            ...listStatuses,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/employments', method: 'get' },
          null,
        ).then((res) => {
          setListEmployes({
            ...listEmployes,
            loading: false,
            options: res.data.map(item => ({ value: item.name, label: (item.display_name).toUpperCase() })),
          })
        }).catch(() => {
          setListEmployes({
            ...listEmployes,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/range-of-incomes', method: 'get' },
          null,
        ).then((res) => {
          setListRangeOfIncomes({
            ...listRangeOfIncomes,
            loading: false,
            options: res.data.map(item => ({ value: item.id, label: item.display_name })),
          })
        }).catch(() => {
          setListRangeOfIncomes({
            ...listRangeOfIncomes,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
          this.props.setStateCountryCode({
            ...this.props.stateCountryCode,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
          this.props.setStateCountryCode({
            ...this.props.stateCountryCode,
            load: false,
            list: [],
          })
        })
      },
    }),
  )(ModalAddCust),
)
