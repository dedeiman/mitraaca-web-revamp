import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
} from 'recompose'
import DetailSPPA from 'components/pages/production/sppa/DetailSPPA'
import { message } from 'antd'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('detailData', 'setDetailData', {
    loading: true,
    type: '',
    list: [],
  }),
  withState('documentData', 'setDocumentData', {
    loading: false,
    list: [],
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'production-search-sppa-menu')
      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${this.props.match.params.id}`, method: 'get' },
      ).then((res) => {
        this.props.setDetailData({
          ...this.props.detailData,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setDetailData({
          loading: false,
          type: '',
          list: [],
        })
      })

      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${this.props.match.params.id}/documents`, method: 'get' },
      ).then((res) => {
        const { data } = res

        this.props.setDocumentData({
          ...this.props.documentData,
          loading: false,
          list: data,
        })
      })
    },
  }),
)(DetailSPPA)
