import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { isEmpty } from 'lodash'
import Swal from 'sweetalert2'
import queryString from 'query-string'
import history from 'utils/history'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import FormWLiability from 'components/pages/production/sppa/liability/Form'
import API from 'utils/API'
import qs from 'query-string'
import config from 'app/config'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateIO' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('listIO', 'setListIO', {
      loading: true,
      options: [],
    }),
    withState('detailLiability', 'setDetailLiability', {
      loading: true,
      list: {},
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('listRelationship', 'setListRelationship', {
      loading: true,
      options: [],
    }),
    withState('selectedIO', 'setSelectedIO'),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedRelationship', 'setSelectedRelationship', {}),
    withState('premi', 'setPremi', {
      premi: 0,
      limitPremi: 0,
      value: 0,
      discount: 0,
      stamp_fee: 0,
      total_payment: 0,
      policy_printing_fee: 0,
      limit_policy_amount: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withState('loadCreateSppa', 'setLoadCreateSppa', false),

    withHandlers({
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()
        const parsed = qs.parse(window.location.search)

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: parseInt(parsed.product_id),
            premi_liability_calculation: {
              start_period: !isEmpty(formData.start_period) ? formData.start_period.format('YYYY-MM-DD') : undefined,
              end_period: !isEmpty(formData.end_period) ? formData.end_period.format('YYYY-MM-DD') : undefined,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_currency: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_liability_calculation
          props.form.setFieldsValue({
            premi: premiData.premi,
          })

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
          
        })
      },
    }),

    withHandlers({
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      getDetailLiability: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          props.setSelectedInsured(res.data.insured)
          props.setSelectedHolder(res.data.policy_holder)
          props.setDetailLiability({
            ...props.detailLiability,
            loading: false,
            list: res.data,
          })

          props.setPremi({
            premi: res.data.liability_calculation.premi,
            limit_policy_amount: res.data.liability_calculation.limit_policy_amount,
            discount_percentage: res.data.liability_calculation.discount_percentage,
            discount_currency: res.data.liability_calculation.discount_currency,
            stamp_fee: res.data.stamp_fee,
            policy_printing_fee: res.data.policy_printing_fee,
            total_payment: res.data.total_payment,
          })
        })

      },
      setFieldByIO: props => async (type, selectedData) => {
        console.log(selectedData)
        if (!selectedData) {
          props.setSelectedIO({})
        } else {
          props.setSelectedIO(selectedData)
          
        }

        const fieldData = {}
        if (type === 'new') {
          fieldData.policy_holder_id = selectedData.name,
          fieldData.premi = selectedData.liability_premi_calculation.premi,
          fieldData.coverage_limit = selectedData.liability_premi_calculation.coverage_limit,
          fieldData.print_policy_book = selectedData.print_policy_book
        }

        props.form.setFieldsValue(fieldData)

        props.getPremi()
      },
      onSubmit: props => (e) => {
        const parsed = queryString.parse(window.location.search)
        if (e !== undefined) {
          e.preventDefault()
        }
        props.form.validateFields(async (err, values) => {
          if (!err) {
            props.setLoadCreateSppa(true)
            const { params } = props.match
            API[`${params.id ? 'put' : 'post'}`](`${config.api_url}/insurance-letters${params.id ? `/${params.id}` : ''}`, {
              product_id: params.id ? props.detailLiability.list.product.id : Number(parsed.product_id),
              liability: {
                sppa_type: values.sppa_type,
                // eslint-disable-next-line no-nested-ternary
                io_id: values.sppa_type === 'new' && values.io_renewal !== '' ? !isEmpty(props.selectedIO) ? props.selectedIO.id : null : null,
                renewal_id: values.sppa_type === 'renewal' && values.io_renewal !== '' ? props.selectedIO.id : null,
                // eslint-disable-next-line no-nested-ternary
                policy_holder_id: props.selectedHolder.id,
                insured_id: props.selectedInsured.id,
                name_on_policy: values.name_on_policy ? (values.name_on_policy).toUpperCase() : '',
                calculation: {
                  start_period: moment(values.start_period).format('YYYY-MM-DD'),
                  end_period: moment(values.end_period).format('YYYY-MM-DD'),
                  discount_percentage: parseFloat(values.discount_percentage),
                  discount_currency: parseFloat(values.discount_currency),
                  print_policy_book: values.print_policy_book,
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
                statement_insured: {
                  is_legal_processdings: values.is_legal_processdings,
                  legal_proceedings_date: values.legal_proceedings_date,
                  legal_proceedings_description: values.legal_proceedings_description ? (values.legal_proceedings_description).toUpperCase() : '',
                },
              },
            }).then((res) => {
              if (res) {
                props.setLoadCreateSppa(false)
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">SPPA Liability berhasil disimpan!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-sppa/confirm/${params.id ? params.id : res.data.data.id}?product=${parsed.product}`)
                })
              }
            }).catch((error) => {
              props.setLoadCreateSppa(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            props.setLoadCreateSppa(false)
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },

      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },
      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${queryString.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            options: [],
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      submitData: props => (e) => {
        props.onSubmit(e)
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.searchCustomer()
        this.props.getDatas(
          { base: 'apiUser', url: '/relationships', method: 'get' },
          null,
        ).then((res) => {
          this.props.setListRelationship({
            ...this.props.listRelationship,
            loading: false,
            options: res.data.reverse().map(item => ({ value: item.id, label: item.name })) || [],
          })
        }).catch(() => {
          this.props.setListRelationship({
            ...this.props.listRelationship,
            loading: false,
            options: [],
          })
        })

        if (this.props.match.params.id) {
          this.props.getDetailLiability()
        } else {
          this.props.setDetailLiability({
            ...this.props.detailLiability,
            loading: false,
          })
        }
      },
    }),
  )(FormWLiability),
)
