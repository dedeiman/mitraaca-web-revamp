import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
} from 'recompose'
import DetailDocument from 'components/pages/production/sppa/DetailDocument'
import { message } from 'antd'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,

    mapDispatchToProps,
  ),
  withState('detailData', 'setDetailData', {
    loading: true,
    type: '',
    list: [],
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'production-create-sppa')
      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${this.props.match.params.id}/documents`, method: 'get' },
      ).then((res) => {
        this.props.setDetailData({
          ...this.props.detailData,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setDetailData({
          loading: false,
          type: '',
          list: [],
        })
      })
    },
  }),
)(DetailDocument)
