import { connect } from 'react-redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import Swal from 'sweetalert2'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import history from 'utils/history'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import FormOtomate from 'components/pages/production/sppa/otomate/FormOtomate'
import moment from 'moment'
import { handleProduct } from 'constants/ActionTypes'
import { isEmpty } from 'lodash'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCreateOtomate' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('listIO', 'setListIO', {
      loading: true,
      options: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withState('selectedIO', 'setSelectedIO'),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('detailOtomate', 'setDetailOtomate', {
      loading: true,
      list: [],
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('listBrand', 'setListBrand', {
      loading: true,
      options: [],
    }),
    withState('listSeries', 'setListSeries', {
      loading: false,
      disabled: true,
      options: [],
    }),
    withState('listPlateCode', 'setListPlateCode', {
      loading: true,
      options: [],
    }),
    withState('listYear', 'setListYear', {
      loading: true,
      options: [],
    }),
    withState('floodMotorCar', 'setFloodMotorCar', {
      loading: true,
      floodInit: '',
      initPercent: 0,
      disabled: false,
      disablePercantage: false,
      options: [{
        label: '10%',
        value: 10,
      }, {
        label: '100%',
        value: 100,
      }],
    }),
    withState('eqMotorCar', 'setEqMotorCar', {
      loading: true,
      eqInit: '',
      initPercent: 0,
      disabled: false,
      disablePercantage: false,
      options: [{
        label: '10%',
        value: 10,
      }, {
        label: '100%',
        value: 100,
      }],
    }),
    withState('huraMotorCar', 'setHuraMotorCar', {
      loading: true,
      rsccInit: '',
      disabled: false,
    }),
    withState('terrorismMotorCar', 'setTerrorismMotorCar', {
      loading: true,
      terrorismInit: '',
      disabled: false,
    }),
    withState('tjhMotorCar', 'setTjhMotorCar', {
      loading: true,
      tjhInit: '',
      initAmount: '',
      disabled: false,
      options: [],
    }),
    withState('papMotorCar', 'setPapMotorCar', {
      loading: true,
      papInit: '',
      initAmount: '',
      papPassenger: 1,
      disabled: false,
      options: [],
    }),
    withState('pllMotorCar', 'setPllMotorCar', {
      loading: true,
      pllInit: '',
      initAmount: '',
      pllPassenger: '',
      disabled: true,
      options: [],
    }),
    withState('padMotorCar', 'setPadMotorCar', {
      loading: true,
      padInit: '',
      initAmount: '',
      padPassenger: '',
      disabled: false,
      options: [],
    }),
    withState('bengkelAuthMotorCar', 'setBengkelAuthMotorCar', {
      loading: true,
      bengkelNotHide: true,
      bengkelInit: '',
      disabled: false,
    }),
    withState('premi', 'setPremi', {
      premi: 0,
      rate: 0,
      stamp_fee: 0,
      coverage: 0,
      total_payment: 0,
      admin_fee: 0,
      policy_printing_fee: 0,
      max_discount: 0,
      max_out_go: 0,
    }),
    withState('rateTypeLoadingDisabled', 'setRateTypeLoadingDisabled', {
      tidak: false,
      rate: false,
      risiko: false,
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('hiddenJenisRate', 'setHiddenJenisRate', false),
    withState('rateType', 'setRateType', ''),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('commercial', 'setCommercial', true),
    withState('submitLoading', 'setSubmitLoading', false),
    withState('limits', 'setLimits', []),

    withHandlers({
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      onSubmit: props => async () => {
        const parsed = queryString.parse(window.location.search)
        const { params } = props.match
        props.form.validateFields(async (err, values) => {
          if (!err) {
            props.setSubmitLoading(true)
            props.setLoadSubmitBtn(true)
            props.getDatas({ base: 'apiUser', url: `/insurance-letters${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: Number(parsed.product_id),
              ottomate: {
                sppa_type: values.sppa_type,
                io_id: values.sppa_type === 'new' && values.io_renewal !== '' && props.selectedIO ? props.selectedIO.id : null,
                renewal_id: values.sppa_type === 'renewal' && values.io_renewal !== '' && props.selectedIO ? props.selectedIO.id : null,
                policy_holder_id: props.selectedHolder.id,
                insured_id: props.selectedInsured.id,
                name_on_policy: values.name_on_policy ? (values.name_on_policy).toUpperCase() : '',
                qq: values.qq ? (values.qq).toUpperCase() : '',
                start_period: moment(values.start_period).format('YYYY-MM-DD'),
                end_period: moment(values.end_period).format('YYYY-MM-DD'),
                print_policy_book: values.print_policy_book,
                premi: Number(props.premi.total_payment),
                vehicle_information: {
                  brand_id: values.brand,
                  series_id: values.series,
                  production_year: values.production_year,
                  description_series: values.desc_vehicle ? (values.desc_vehicle).toUpperCase() : '',
                  license_plate_code: values.license_plate_code,
                  middle_police_number: values.middle_policy_number,
                  end_police_number: values.rear_vehicle_year ? (values.rear_vehicle_year).toUpperCase() : '',
                  vehicle_use: values.vehicle_use,
                  color: values.color ? (values.color).toUpperCase() : '',
                  chassis_number: values.chassis_number ? (values.chassis_number).toUpperCase() : '',
                  machine_number: values.machine_number ? (values.machine_number).toUpperCase() : '',
                  equipment_type: values.equipment_type,
                  equipment_description: values.equipment_description ? (values.equipment_description).toUpperCase() : '',
                  vehicle_damage_type: values.vehicle_damage_type,
                  vehicle_damage_description: values.vehicle_damage_description ? (values.vehicle_damage_description).toUpperCase() : '',
                },
                premi_calculation: {
                  coverage: Helper.replaceNumber(values.coverage_value),
                  additional_accesoris_costs: Helper.replaceNumber(values.additional_accessories_costs),
                  own_risk_value: Helper.replaceNumber(values.own_risk_value),
                  loading_rate_type: values.loading_rate_type,
                  discount_percentage: parseFloat(values.discount_percentage),
                  discount_currency: parseFloat(values.discount_currency),
                  rate_type: values.rate_type,
                  rate: Number(props.premi.rate),
                  premi: Number(props.premi.premi),
                  max_discount: props.premi.max_discount,
                  max_out_go: props.premi.max_out_go,
                },
                additional_limit: {
                  limits: props.limits,
                  pap_total: Number(values.pap_passenger),
                  pap_amount: typeof (values.pap_amount) !== 'string' ? values.pap_amount : Helper.currencyToInt(values.pap_amount),
                  pll_amount: typeof (values.pll_amount) !== 'string' ? values.pll_amount : Helper.currencyToInt(values.pll_amount),
                  pad_amount: typeof (values.pad_amount) !== 'string' ? values.pad_amount : Helper.currencyToInt(values.pad_amount),
                  tjh: typeof (values.tjh_amount) !== 'string' ? values.tjh_amount : Helper.currencyToInt(values.tjh_amount),
                  atpm_percentage: values.atpm_percentage ? parseFloat(values.atpm_percentage) : 0,
                  // flood_percentage: values.flood_percentage ? values.flood_percentage : 0,
                  // eq_percentage: values.eq_percentage ? values.eq_percentage : 0,
                },
              },
            }).then((res) => {
              if (res) {
                props.setSubmitLoading(false)
                Swal.fire(
                  ``,
                  `<span style="color:#2b57b7;font-weight:bold">SPPA ${handleProduct(parsed.product)} berhasil dibuat!</span>`,
                  'success',
                ).then(() => {
                  history.push(`/production-create-sppa/confirm/${res.data.id}?product=${res.data.product.code}`)
                })
              }
            }).catch((error) => {
              props.setLoadSubmitBtn(false)
              props.setSubmitLoading(false)
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            props.setLoadSubmitBtn(false)
            props.setSubmitLoading(false)
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },

      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },

      loadAdditional: props => (value) => {
        setTimeout(() => props.setLimits(value))
      },

      initPremi: props => (key, val) => {
        const parsed = queryString.parse(window.location.search)
        const formData = props.form.getFieldsValue()
        const tjhAmount = !isEmpty(formData.tjh) ? formData.tjh_amount : 0
        const papAmount = !isEmpty(formData.pap) ? formData.pap_amount : 0
        const pllAmount = !isEmpty(formData.pll) ? formData.pll_amount : 0
        const padAmount = !isEmpty(formData.pad) ? formData.pad_amount : 0

        let addtionalLimits = []
        if (props.limits.includes(key)) {
          props.setLimits(props.limits.filter(item => item !== key))
          const dataLimits = props.limits.filter(item => item !== key)

          addtionalLimits = dataLimits
        } else if (val !== undefined) {
          props.setLimits((limit) => {
            addtionalLimits.push(...limit, val)

            return [...limit, val]
          })
        }

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
        {
          product_id: Number(parsed.product_id),
          premi_ottomate_calculation: {
            product_id: Number(parsed.product_id),
            brand_id: formData.brand ? Number(formData.brand) : 0,
            series_id: formData.series ? Number(formData.series) : 0,
            coverage: Helper.replaceNumber(formData.coverage_value),
            rate_type: formData.rate_type || '',
            loading_rate_type: formData.loading_rate_type || 'rate',
            production_year: formData.production_year || 0,
            license_plate_code: formData.license_plate_code,
            insurance_period: formData.period,
            print_policy_book: formData.print_policy_book,
            additional_accessories_costs: Helper.replaceNumber(formData.additional_accessories_costs),
            pap_total: Number(formData.pap_passenger),
            additional_limits: !isEmpty(addtionalLimits) ? addtionalLimits : props.limits,
            discount_percentage: parseFloat(formData.discount_percentage) || 0,
            discount_currency: parseFloat(formData.discount_currency) || 0,
            pap_amount: typeof (papAmount) !== 'string' ? papAmount : Helper.currencyToInt(papAmount),
            pll_amount: typeof (pllAmount) !== 'string' ? pllAmount : Helper.currencyToInt(pllAmount),
            pad_amount: typeof (padAmount) !== 'string' ? padAmount : Helper.currencyToInt(padAmount),
            tjh: typeof (tjhAmount) !== 'string' ? tjhAmount : Helper.currencyToInt(tjhAmount),
            atpm_percentage: formData.atpm_percentage ? Number(formData.atpm_percentage) : 0,
            // flood_percentage: formData.flood_percentage ? Number(formData.flood_percentage) : 0,
            // eq_percentage: formData.eq_percentage ? Number(formData.eq_percentage) : 0,
          },
        },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_ottomate_calculation
          props.form.setFieldsValue({
            premi: premiData.premi,
          })

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },

      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },
      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${queryString.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            options: [],
          })
        })
      },
      handleSelect: props => (key, value, keyType) => {
        props[`setList${keyType}`]({
          ...props[`list${keyType}`],
          loading: true,
        })

        props.getDatas(
          { base: 'apiUser', url: `/${key}/${value}`, method: 'get' },
          null,
        ).then((res) => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            disabled: false,
            options: res.data.length > 0 ? res.data.map(item => ({
              value: item.id, label: item.name, category: item.category, price: item.series_price.price, seat: item.available_seat,
            })) : [],
          })
        }).catch(() => {
          props[`setList${keyType}`]({
            ...props[`list${keyType}`],
            loading: false,
            options: [],
          })
        })
      },
      handleSeriesPrice: props => (seriesId = '', year = '') => {
        props.getDatas(
          { base: 'apiUser', url: `/series-price-car?production_year=${year}&series_id=${seriesId}`, method: 'get' },
          null,
        ).then((res) => {
          props.setPremi({
            ...props.premi,
            coverage: res.data.price,
          })
        })
      },
    }),
    withHandlers({
      getDetailOtomate: props => () => {
        const parsed = queryString.parse(window.location.search)
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then((res) => {
          const date = new Date()
          const getAgeCar = date.getFullYear() - (res.data.ottomate_vehicle.production_year)

          props.setDetailOtomate({
            ...props.detailOtomate,
            loading: false,
            list: res.data,
          })

          props.handleSelect('series-car', res.data.ottomate_vehicle.brand_id, 'Series')
          props.setSelectedInsured(res.data.insured)
          props.setSelectedHolder(res.data.policy_holder)

          props.setPremi({
            ...props.premi,
            premi: res.data.ottomate_calculation ? res.data.ottomate_calculation.premi : 0,
            rate: res.data.ottomate_calculation ? res.data.ottomate_calculation.rate : 0,
            stamp_fee: res.data ? res.data.stamp_fee : 0,
            total_payment: res.data.total_payment,
            print_policy_book: res.data.print_policy_book,
            discount_percentage: res.data.ottomate_calculation.discount_percentage,
            discount_currency: res.data.ottomate_calculation.discount_currency,
            policy_printing_fee: res.data.policy_printing_fee,
          })

          props.setLimits(res.data.ottomate_additional_limit.limits)

          switch (parsed.product) {
            case 'PO001':
            case 'PO002':
            case 'PO003':
            case 'PO005':
            case 'POS01':
            case 'POS02':
            case 'POS03':
            case 'POS05':
              if (getAgeCar === 6 || getAgeCar === 7) {
                props.setRateTypeLoadingDisabled({
                  tidak: true,
                  rate: false,
                  risiko: false,
                })
              } else if (getAgeCar > 7) {
                props.setRateTypeLoadingDisabled({
                  tidak: true,
                  rate: false,
                  risiko: true,
                })
              } else {
                props.setRateTypeLoadingDisabled({
                  tidak: false,
                  rate: true,
                  risiko: true,
                })
              }
              break

            case 'PO004':
            case 'POS04':
              if (getAgeCar >= 10) {
                props.setRateTypeLoadingDisabled({
                  risiko: true,
                  tidak: true,
                })
              } else {
                props.setRateTypeLoadingDisabled({
                  rate: false,
                  risiko: true,
                  tidak: false,
                })
              }
              break

            default:
              break
          }
        })
      },

      submitData: props => () => {
        props.onSubmit()
      },
    }),
    withPropsOnChange(
      ['initPremi'],
      ({ initPremi }) => ({
        initPremi: _.debounce(initPremi, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {

        const {
          setListBrand, listBrand, setListPlateCode, listPlateCode, setListYear, listYear, setFloodMotorCar, floodMotorCar, setEqMotorCar, eqMotorCar, tjhMotorCar, setTjhMotorCar, papMotorCar, setPapMotorCar,
          pllMotorCar, setPllMotorCar, setPadMotorCar, padMotorCar, setBengkelAuthMotorCar, bengkelAuthMotorCar, setHuraMotorCar, huraMotorCar, terrorismMotorCar, setTerrorismMotorCar, searchCustomer,
          getDetailOtomate, match, setRateType, setDetailOtomate, detailOtomate, setCommercial, setHiddenJenisRate, setLimits,
        } = this.props
        const parsed = queryString.parse(window.location.search)
        searchCustomer()

        switch (parsed.product) {
          // Product Otomate
          case 'PO001':
          case 'POS01':
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: 'flood',
              initPercent: 10,
              disabled: true,
              options: [{
                label: '10%',
                value: 10,
              }, {
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: 'eq',
              initPercent: 10,
              disabled: true,
              options: [{
                label: '10%',
                value: 10,
              }, {
                label: '100%',
                value: 100,
              }],
            })
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              bengkelInit: '',
              disabled: false,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: 'rscc',
              disabled: true,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              tsInit: 'ts',
              disabled: true,
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['flood', 'eq', 'rscc', 'ts', 'tjh', 'pap', 'pad'])
            }
            break
          // Product Otomate Smart
          case 'PO002':
          case 'POS02':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              bengkelInit: '',
              disabled: false,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: '',
              disabled: false,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              terrorismInit: '',
              disabled: false,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['tjh'])
            }
            break
          // Product Otomate Soliter
          case 'PO003':
          case 'POS03':
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: 'flood',
              initPercent: 100,
              disabled: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: 'eq',
              initPercent: 100,
              disabled: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: true,
              bengkelInit: 'atpm',
              disabled: true,
            })
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: 'rscc',
              disabled: true,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              terrorismInit: 'ts',
              disabled: true,
            })
            setHiddenJenisRate(true)

            if (!match.params.id) {
              setLimits(['flood', 'eq', 'ts', 'rscc', 'atpm', 'tjh', 'pap', 'pad'])
            }
            break
          // Product TLO
          case 'PO004':
          case 'POS04':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: false,
              bengkelInit: '',
              disabled: true,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              disablePercantage: true,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setRateType('bottom')
            break
          // Product Comprehensive
          case 'PO005':
          case 'POS05':
            setBengkelAuthMotorCar({
              ...bengkelAuthMotorCar,
              loading: false,
              bengkelNotHide: false,
              bengkelInit: '',
              disabled: true,
            })
            setFloodMotorCar({
              ...floodMotorCar,
              loading: false,
              floodInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setEqMotorCar({
              ...eqMotorCar,
              loading: false,
              eqInit: '',
              initPercent: 0,
              disabled: false,
              options: [{
                label: '100%',
                value: 100,
              }],
            })
            setRateType('bottom')
            break

          default:
            setHuraMotorCar({
              ...huraMotorCar,
              loading: false,
              rsccInit: '',
              disabled: false,
            })
            setTerrorismMotorCar({
              ...terrorismMotorCar,
              loading: false,
              terrorismInit: '',
              disabled: false,
            })
            break
        }

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=tjh&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          const options = res.data.length !== 0 ? res.data.map(item => ({
            value: item.price,
            label: item.price,
          })) : []

          switch (parsed.product) {
            case 'PO001':
            case 'POS01': {
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 50000000,
                disabled: true,
                options,
              })
              break
            }

            case 'PO002':
            case 'POS02': {
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 25000000,
                disabled: true,
                options,
              })
              break
            }

            case 'PO003':
            case 'POS03':
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: 'tjh',
                initAmount: 75000000,
                disabled: true,
                options,
              })
              break

            case 'PO004':
            case 'PO005':
            case 'POS04':
            case 'POS05':
              setTjhMotorCar({
                ...tjhMotorCar,
                loading: false,
                tjhInit: '',
                initAmount: 0,
                disabled: false,
                options,
              })
              break

            default:
              break
          }
        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pll&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          switch (parsed.product) {
            case 'PO001':
            case 'POS01':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03':
              setPllMotorCar({
                ...pllMotorCar,
                loading: false,
                pllInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            default:
              break
          }
        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pad&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          switch (parsed.product) {
            case 'PO001':
            case 'POS01':
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: 'pad',
                initAmount: 10000000,
                disabled: true,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: '',
                initAmount: 0,
                disabled: false,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03':
              setPadMotorCar({
                ...padMotorCar,
                loading: false,
                padInit: 'pad',
                initAmount: 10000000,
                disabled: true,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            default:
              break
          }
        })

        this.props.getDatas(
          { base: 'apiUser', url: `/otomate-additional-prices?limit_type=pap&product_code=${parsed.product}`, method: 'get' },
          null,
        ).then((res) => {
          switch (parsed.product) {
            case 'PO001':
            case 'POS01':
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: 'pap',
                initAmount: 10000000,
                disabled: true,
                papPassenger: 4,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO002':
            case 'PO004':
            case 'PO005':
            case 'POS02':
            case 'POS04':
            case 'POS05':
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: '',
                initAmount: 0,
                disabled: false,
                papPassenger: 1,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            case 'PO003':
            case 'POS03':
              setPapMotorCar({
                ...papMotorCar,
                loading: false,
                papInit: 'pap',
                initAmount: 10000000,
                disabled: true,
                papPassenger: 4,
                options: res.data.length !== 0 ? res.data.map(item => ({
                  value: item.price, label: item.price,
                })) : [],
              })
              break

            default:
              break
          }
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/brand-car', method: 'get' },
          null,
        ).then((res) => {
          setListBrand({
            ...listBrand,
            loading: false,
            options: res.data.map(item => ({ value: item.id, label: item.name })) || [],
          })
        }).catch(() => {
          setListBrand({
            ...listBrand,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/license-plate-code', method: 'get' },
          null,
        ).then((res) => {
          setListPlateCode({
            ...listPlateCode,
            loading: false,
            options: res.data.map(item => ({ value: item.code, label: item.code })) || [],
          })
        }).catch(() => {
          setListPlateCode({
            ...listPlateCode,
            loading: false,
            options: [],
          })
        })

        this.props.getDatas(
          { base: 'apiUser', url: '/production-year', method: 'get' },
          null,
        ).then((res) => {
          setListYear({
            ...listYear,
            loading: false,
            options: res.data.reverse().map(item => ({ value: item.year, label: item.year })) || [],
          })
        }).catch(() => {
          setListYear({
            ...listYear,
            loading: false,
            options: [],
          })
        })

        if (match.params.id) {
          getDetailOtomate()
        } else {
          setDetailOtomate({
            ...detailOtomate,
            loading: false,
          })
          // this.props.initPremi()
        }

        switch (parsed.product) {
          case 'PO001':
          case 'PO002':
          case 'PO003':
          case 'POS01':
          case 'POS02':
          case 'POS03':
            setCommercial(false)
            break
          default:
            setCommercial(true)
        }
      },
    }),
  )(FormOtomate),
)
