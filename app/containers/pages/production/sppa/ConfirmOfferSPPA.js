import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle, withHandlers,
  withState,
} from 'recompose'
import ConfirmOfferSPPA from 'components/pages/production/sppa/ConfirmOfferSPPA'
import { getDatas } from 'actions/Option'
import { capitalize } from 'lodash'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('detailData', 'setDetailData', {
    loading: true,
    id_sppa: '',
    status: '',
  }),
  withState('stateLoad', 'setStateLoad', {
    loading: false,
  }),
  withState('stateLoadIframe', 'setStateLoadIframe', {
    loadingIframe: true,
  }),
  withHandlers({
    handleLoadIframe: props => () => {
      props.setStateLoadIframe({
        loadingIframe: false,
      })
    },
    handleConfirmOffer: props => () => {
      props.setStateLoad({ ...props.stateLoad, loading: true })
      props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}/sendattachsppa`, method: 'get' },
      ).then(() => {
        Swal.fire(
          '', '<span style="color:#2b57b7;font-weight:bold">Konfirmasi Penawaran Berhasil Dikirim!</span>', 'success',
        ).then(() => {
          props.setStateLoad({ ...props.stateLoad, loading: false })
          history.push('/payment-filter')
        })
      }).catch((err) => {
        let messageErr = err
        if (err.response && err.response.data.meta) {
          messageErr = err.response.data.meta.message
        }
        props.setStateLoad({ ...props.stateLoad, loading: false })
        Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${messageErr.message}</span>`, 'error')
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', `production-create-sppa/confirm-offer/${this.props.match.params.id}`)
      this.props.setDetailData({
        ...this.props.detailData,
        loading: false,
        id_sppa: this.props.match.params.id,
        status: window.location.pathname.includes('complete') || window.location.pathname.includes('approved'),
      })
    },
  }),
)(ConfirmOfferSPPA)
