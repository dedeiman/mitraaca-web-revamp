import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { message } from 'antd'
import SearchSPPA from 'components/pages/production/sppa/Search'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import qs from 'query-string'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('statePolicy', 'setStatePolicy', {
    load: true,
    list: [],
    page: {},
  }),
  withState('stateFilter', 'setStateFilter', {
    load: true,
    list: [],
    search: '',
    filter: undefined,
    page: 1,
  }),
  withHandlers({
    loadPolicy: props => (isSearch) => {
      const {
        filter, search,
        page, limit,
      } = props.stateFilter
      const payload = {
        status: filter,
        keyword: search,
        page: isSearch ? 1 : page,
        limit,
      }

      history.push(`/production-search-sppa-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateFilter, setStateFilter, loadPolicy } = props
      setStateFilter({
        ...stateFilter,
        filter: val,
      })
      setTimeout(() => {
        loadPolicy(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateFilter, setStateFilter, loadPolicy } = props
      setStateFilter({
        ...stateFilter,
        page: value,
      })
      setTimeout(() => {
        loadPolicy()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { statePolicy, setStatePolicy, stateFilter } = props
      setStatePolicy({
        ...statePolicy,
        page: {
          ...statePolicy.page,
          limit: value,
        },
      })
      const payload = {
        limit: value,
        status: stateFilter.search ? stateFilter.filter : '',
        keyword: stateFilter.search,
      }

      history.push(`/policy-search-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        location,
        setStatePolicy, statePolicy,
        setStateFilter, stateFilter,
      } = this.props
      const newState = (qs.parse(location.search) || {})
     
      this.props.updateSiteConfiguration('activePage', 'production-search-sppa-menu')
      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters${location.search}`, method: 'get' },
        null,
      ).then((res) => {
        setStatePolicy({
          ...statePolicy,
          load: false,
          list: res.data,
          page: res.meta,
        })
        setStateFilter({
          ...stateFilter,
          load: false,
          list: [
            // { id: 'complete', name: 'Complete' },
            { id: 'need-approval', name: 'Need Approval' },
            { id: 'pending', name: 'Pending' },
            { id: 'approved', name: 'Approved' },
            { id: 'rejected', name: 'Rejected' },
            { id: 'in-process', name: 'In Process' },
            { id: 'expired', name: 'Expired' },
            { id: 'active', name: 'Active' },
            { id: 'ended', name: 'Ended' },
          ],
          search: newState.keyword,
          filter: newState.status,
          limit: newState.limit,
          page: res.meta.current_page,
        })
      }).catch((err) => {
        setStatePolicy({
          ...statePolicy,
          load: false,
          list: [],
        })
        setStateFilter({
          ...stateFilter,
          load: false,
          list: [
            { id: 'complete', name: 'Complete' },
            { id: 'need-approval', name: 'Need Approval' },
            { id: 'pending', name: 'Pending' },
            { id: 'approved', name: 'Approved' },
            { id: 'rejected', name: 'Rejected' },
            { id: 'in-process', name: 'In Process' },
            { id: 'expired', name: 'Expired' },
            { id: 'active', name: 'Active' },
            { id: 'ended', name: 'Ended' },
          ],
          search: newState.keyword,
          filter: newState.status,
        })
        message.error(err.message)
      })
    },
  }),
)(SearchSPPA)
