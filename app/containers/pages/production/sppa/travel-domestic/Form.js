/* eslint-disable no-case-declarations */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormTravel from 'components/pages/production/sppa/travel-domestic/Form'
import { getDatas } from 'actions/Option'
import moment from 'moment'
import history from 'utils/history'
import {
  fetchDetailProduct,
} from 'actions/Product'

import _ from 'lodash'
import Swal from 'sweetalert2'
import qs from 'query-string'

export function mapStateToProps(state) {

  const bookPrice = 50000

  const {
    isFetching: isFetchingProductDetail,
    detail: productDetail,
  } = state.root.product

  return {
    productDetail,
    isFetchingProductDetail,
    bookPrice,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
})

export default Form.create({ name: 'formCreateSPPA' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('listIO', 'setListIO', {
      loading: true,
      data: [],
    }),
    withState('listCustomer', 'setListCustomer', {
      loading: true,
      options: [],
    }),
    withState('stateSelects', 'setStateSelects', {
      domestikCityLoad: false,
      domestikCityList: [],
      travelDestinationLoad: false,
      travelDestinationList: [],
      relationshipLoad: false,
      relationshipList: [],
    }),
    withState('detailData', 'setDetailData', {
      loading: false,
      list: [],
    }),
    withState('customerDetail', 'setCustomerDetail', {}),
    withState('travelDestination', 'setTravelDestination', {}),
    withState('travelRelationship', 'setTravelRelationship', {}),
    withState('productID', 'setProductID', 0),
    withState('productDetail', 'setproductDetail', {}),
    withState('visiblePreview', 'setVisiblePreview', false),
    withState('visibleDiscount', 'setVisibleDiscount', false),
    withState('selectedHolder', 'setSelectedHolder', {}),
    withState('selectedInsured', 'setSelectedInsured', {}),
    withState('selectedIO', 'setSelectedIO', {}),
    withState('submitted', 'setSubmitted', false),
    withState('premi', 'setPremi', {
      admin_fee: 0,
      discount_amount: 0,
      discount_percentage: 0,
      loading_premi: 0,
      policy_printing_fee: 0,
      premi: 0,
      price_additional_week: 0,
      price_days_insured: 0,
      stamp_fee: 0,
      total_additional_week: 0,
      total_days_insured: 0,
      total_payment: 0,
      max_discount: 0,
      max_out_go: 0,
    }),

    withHandlers({
      appendCustomer: props => (data) => {
        props.setListCustomer({
          ...props.listCustomer,
          options: [...props.listCustomer.options, data],
        })
      },
      getPremi: props => () => {
        const formData = props.form.getFieldsValue()

        let insuredDob = null
        if (!_.isEmpty(props.selectedInsured)) {
          insuredDob = moment(props.selectedInsured.dob).format('YYYY-MM-DD')
        }

        props.getDatas(
          { base: 'apiUser', url: '/premi-calculations/mitra-aca', method: 'post' },
          {
            product_id: Number(props.productID),
            premi_travel_domestik_calculation: {
              insured_dob: insuredDob,
              destination_city_id: formData.destination_city_id,
              departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
              return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
              plan_type: formData.plan_type,
              discount_percentage: formData.discount_percentage ? parseFloat(formData.discount_percentage) : 0,
              discount_amount: formData.discount_currency ? parseFloat(formData.discount_currency) : 0,
              print_policy_book: formData.print_policy_book === true,
            },
          },
        ).then((res) => {
          const { data } = res
          const premiData = data.premi_travel_calculation

          props.setPremi({
            ...props.premi,
            ...premiData,
          })
        })
      },
      fetchDetailCustomer: props => (id) => {
        const {
          setCustomerDetail, customerDetail,
        } = props

        props.getDatas(
          { base: 'apiUser', url: `/customers/${id}`, method: 'get' },
        ).then((res) => {
          setCustomerDetail({
            ...customerDetail,
            ...res.data,
          })
        })
      },
    }),
    withHandlers({
      setFieldByIO: props => async (type, selectedData) => {
        if (!selectedData) {
          props.setSelectedIO({})
        } else {
          props.setSelectedIO(selectedData)
          
        }

        const fieldData = {}
        if (type === 'new') {
          fieldData.travel_destination_id = selectedData.travel_information.travel_destination.id
          fieldData.destination_city_id = selectedData.travel_information.destination_city.id
          fieldData.heir_name = selectedData.travel_information.heir_name
          fieldData.relationship_id = selectedData.travel_information.relationship.id
          fieldData.departure_date = moment(selectedData.travel_information.departure_date).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD') ? '' : moment(selectedData.travel_information.departure_date)
          fieldData.return_date = moment(selectedData.travel_information.return_date).format('YYYY-MM-DD') < moment().format('YYYY-MM-DD') ? '' : moment(selectedData.travel_information.return_date)
          fieldData.plan_type = selectedData.travel_information.plan_type
          fieldData.policy_printing_fee = selectedData.policy_printing_fee
          fieldData.print_policy_book = selectedData.print_policy_book
          fieldData.stamp_fee = selectedData.stamp_fee
          fieldData.policy_holder_name = selectedData.name
        }

        props.form.setFieldsValue(fieldData)

        props.getPremi()
      },
      initDetailData: props => async () => {
        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}`, method: 'get' },
        ).then(async (res) => {
          const { data } = res

          props.setDetailData({
            ...props.detailData,
            loading: false,
            list: data,
          })

          props.setSelectedInsured(data.insured)
          props.setSelectedHolder(data.policy_holder)
          props.setTravelDestination(data.travel_information.destination_city)
          props.setTravelRelationship(data.travel_information.relationship)

          props.setPremi({
            admin_fee: 0,
            discount_amount: res.data.travel_premi_calculation.discount_amount,
            discount_percentage: res.data.travel_premi_calculation.discount_percentage,
            loading_premi: res.data.travel_premi_calculation.loading_premi,
            policy_printing_fee: res.data.policy_printing_fee,
            premi: res.data.travel_premi_calculation.premi,
            price_additional_week: res.data.travel_premi_calculation.price_additional_week,
            price_days_insured: res.data.travel_premi_calculation.price_days_insured,
            stamp_fee: res.data.travel_premi_calculation.stamp_fee,
            total_additional_week: res.data.travel_premi_calculation.total_additional_week,
            total_days_insured: res.data.travel_premi_calculation.total_days_insured,
            total_payment: res.data.total_payment,
          })
        })
      },
      submitData: props => async () => {
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          } else {
            const formData = values
            const { params } = props.match

            props.setSubmitted(true)
            await props.getDatas({ base: 'apiUser', url: `/insurance-letters${params.id ? `/${params.id}` : ''}`, method: `${params.id ? 'put' : 'post'}` }, {
              product_id: Number(props.productID),
              travel_domestik: {
                sppa_type: formData.sppa_type,
                io_id: props.selectedIO.id,
                renewal_id: formData.renewal_id,
                policy_holder_id: props.selectedHolder.id,
                insured_id: props.selectedInsured.id,
                name_on_policy: (formData.name_on_policy || '').toUpperCase(),
                print_policy_book: formData.print_policy_book === true,
                information: {
                  insured_dob: formData.insured_dob ? moment(formData.insured_dob).format('YYYY-MM-DD') : '',
                  travel_destination_id: formData.travel_destination_id,
                  destination_city_id: formData.destination_city_id,
                  heir_name: (formData.heir_name || '').toUpperCase(),
                  relationship_id: formData.relationship_id,
                  departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : '',
                  return_date: formData.return_date ? moment(formData.return_date).format('YYYY-MM-DD') : '',
                  plan_type: formData.plan_type,
                },
                discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
                discount_amount: formData.discount_amount ? Number(formData.discount_amount) : 0,
                max_discount: props.premi.max_discount,
                max_out_go: props.premi.max_out_go,
              },
            }).then((res) => {
              if (res) {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Travel Domestic SPPA berhasil dibuat!</span>',
                  'success',
                ).then(() => {
                  history.push(`/production-create-sppa/confirm/${res.data.id}?product=${res.data.product.code}`)
                })
              }
            }).catch((error) => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })

            props.setVisiblePreview(false)
            props.setSubmitted(false)
          }
        })
      },
      submitPreview: props => () => {
        props.form.validateFields(async (err) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
            return
          }
          props.setVisiblePreview(true)
        })
      },
      handleRenewal: props => (type = 'new', documentNumber = '') => {
        props.setListIO({
          ...props.listIO,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/renewal-insurance-letters?sppa_type=${type}&product_id=${qs.parse(window.location.search).product_id}&document_number=${documentNumber}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListIO({
            loading: false,
            data: res.data,
          })
        }).catch(() => {
          props.setListIO({
            loading: false,
            data: [],
          })
        })
      },
      searchCustomer: props => (key) => {
        props.setListCustomer({
          ...props.listCustomer,
          loading: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers${key ? `?search=${key}&per_page=` : '?per_page='}`, method: 'get' },
          null,
        ).then((res) => {
          props.setListCustomer({
            loading: false,
            options: res.data,
          })
        }).catch(() => {
          props.setListCustomer({
            loading: false,
            options: [],
          })
        })
      },
    }),
    withPropsOnChange(
      ['getPremi'],
      ({ getPremi }) => ({
        getPremi: _.debounce(getPremi, 300),
      }),
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.submitData()
      },
      onSubmitPreview: props => (event) => {
        event.preventDefault()
        props.submitPreview()
      },
    }),
    lifecycle({
      componentDidMount() {
       
        const query = qs.parse(this.props.location.search)
        const productID = query.product_id

        this.props.setProductID(productID)

        this.props.fetchDetailProduct(productID).then((response) => {
          this.props.setproductDetail(response.data.data)
        })

        this.props.searchCustomer()

        const config = [
          { url: '/travel-destinations', name: 'travelDestination' },
          { url: '/relationships', name: 'relationship' },
          { url: '/domestic-cities', name: 'domestikCity' },
        ]

        config.map((item) => {
          const { url, name } = item

          return this.props.getDatas(
            { base: 'apiUser', url, method: 'get' },
          ).then((res) => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: res.data,
            })
          }).catch(() => {
            this.props.setStateSelects({
              ...this.props.stateSelects,
              [`${name}Load`]: false,
              [`${name}List`]: [],
            })
          })
        })

        if (this.props.match.params.id) {
          this.props.initDetailData()
        }
      },
    }),
  )(FormTravel),
)
