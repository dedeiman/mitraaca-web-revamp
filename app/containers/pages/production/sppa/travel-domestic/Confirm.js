import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
} from 'recompose'
import Confirm from 'components/pages/production/sppa/travel-domestic/Confirm'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import Helper from 'utils/Helper'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,

    mapDispatchToProps,
  ),
  withState('detailSPPA', 'setDetailSPPA', {
    loading: true,
    type: '',
    list: [],
  }),
  withState('signature', 'setSignature', {
    data: {},
  }),
  withHandlers({
    handleSignatureSave: props => (signature, redirectType) => {
      if (props.signature.data.isEmpty()) {
        Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Tanda Tangan Tertanggung harus diisi</span>', 'error')
        return
      }

      const image = Helper.convertBase64ToFile(signature)
      const bodyFormData = new FormData()
      bodyFormData.set('signature_file', image)

      props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}/signature`, method: 'put' },
        bodyFormData,
      ).then((res) => {
        Swal.fire(
          '',
          '<span style="color:#2b57b7;font-weight:bold">Konfirmasi SPPA berhasil dibuat!</span>',
          'success',
        ).then(() => {
          if (redirectType === 'next') {
            history.push(`/production-create-sppa/upload/${res.data.id}?product=${res.data.product.code}`)
          } else {
            history.push('/production-search-sppa-menu')
          }
        })
      }).catch((err) => {
        let messageErr = err
        if (err.response && err.response.data.meta) {
          messageErr = err.response.data.meta.message
        }
        Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${messageErr}<span>`, 'error')
      })
    },
    handleBack: () => (id, productCode, productID) => {
      history.push(`/production-create-sppa/edit/${id}?product=${productCode}&product_id=${productID}`)
    },
  }),
  lifecycle({
    componentDidMount() {
     
      this.props.updateSiteConfiguration('activePage', 'production-create-io')
      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters/${this.props.match.params.id}`, method: 'get' },
      ).then((res) => {
        this.props.setDetailSPPA({
          ...this.props.detailSPPA,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setDetailSPPA({
          loading: false,
          type: '',
          list: [],
        })
      })
    },
  }),
)(Confirm)
