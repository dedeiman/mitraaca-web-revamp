import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  lifecycle,
  withState,
  withHandlers,
} from 'recompose'
import { message } from 'antd'
import config from 'app/config'
import Swal from 'sweetalert2'
import { Form } from '@ant-design/compatible'
import PolicyDetail from 'components/pages/policies/Detail'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { isEmpty } from 'lodash'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default Form.create({ name: 'formDocument' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('stateModal', 'setStateModal', {
      visible: false,
      file: '',
    }),
    withState('statePolicy', 'setStatePolicy', {
      load: true,
      detail: {},
      list: [],
    }),
    withHandlers({
      loadAgent: props => () => {
        const { match, setStatePolicy, statePolicy } = props

        props.getDatas(
          { base: 'apiUser', url: `/insurance-letters/${match.params.id}`, method: 'get' },
          null,
        ).then((res) => {
          props.getDatas(
            { base: 'apiUser', url: `/insurance-letters/${match.params.id}/documents`, method: 'get' },
            null,
          ).then((response) => {
            setStatePolicy({
              ...statePolicy,
              load: false,
              detail: res.data,
              list: response.data,
            })
          }).catch((err) => {
            setStatePolicy({
              ...statePolicy,
              load: false,
              detail: res.data,
              list: [],
            })
            message.error(err.message)
          })
        }).catch((err) => {
          setStatePolicy({
            ...statePolicy,
            load: false,
            detail: {},
            list: [],
          })
          message.error(err.message)
        })
      },
    }),
    withHandlers({
      handleDelete: props => (event) => {
        event.preventDefault()

        props.form.validateFields(async (err, values) => {
          const formData = values
          const payload = []

          if (!isEmpty(formData.documents)) {
            formData.documents.map((item) => {
              const dataDoc = props.statePolicy.list.filter(doc => doc.id === item)

              payload.push({
                id: dataDoc[0].id,
                file: dataDoc[0].file,
                description: dataDoc[0].description,
              })

              return true
            })
          }

          if (!isEmpty(payload)) {
            props.getDatas(
              { base: 'apiUser', url: `/insurance-letters/${props.match.params.id}/documents`, method: 'delete' },
              payload,
            ).then(() => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">Berhasil Menghapus Dokumen!</span>`,
                'success',
              ).then(() => {
                props.loadAgent()
              })
            }).catch((error) => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          } else {
            Swal.fire(
              '',
              `<span style="color:#2b57b7;font-weight:bold">Minimal Pilih 1 Dokumen!</span>`,
              'error',
            )
          }
        })
      },

      handleDownload: props => (event) => {
        event.preventDefault()
        props.form.validateFields(async (err, values) => {
          const formData = values
          const downloadData = []

          if (!isEmpty(formData.documents)) {
            formData.documents.map((item) => {
              const dataFilter = props.statePolicy.list.filter(data => data.id === item)
              dataFilter.map(data => downloadData.push(data.id))
            })
          }

          if (!isEmpty(downloadData)) {
            window.open(`${config.api_url}/insurance-letters/${props.match.params.id}/download/documents?document_id=${downloadData.join(',')}`)
          } else {
            Swal.fire(
              '',
              `<span style="color:#2b57b7;font-weight:bold">Minimal Pilih 1 Dokumen!</span>`,
              'error',
            )
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
       
        this.props.updateSiteConfiguration('activePage', 'policy')
        this.props.loadAgent()
      },
    }),
  )(PolicyDetail),
)
