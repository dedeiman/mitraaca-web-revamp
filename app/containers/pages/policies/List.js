import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity, isEmpty } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { message } from 'antd'
import PolicyList from 'components/pages/policies/List'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  return {
    groupRole, currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('statePolicy', 'setStatePolicy', {
    load: true,
    list: [],
    page: {},
  }),
  withState('stateFilter', 'setStateFilter', {
    load: true,
    list: [],
    search: '',
    date: undefined,
    status: undefined,
    page: 1,
  }),
  withHandlers({
    loadPolicy: props => (isSearch) => {
      const {
        status, search,
        page, limit, date,
      } = props.stateFilter

      const payload = {
        date,
        status,
        keyword: search,
        page: isSearch ? 1 : page,
        limit,
      }

      history.push(`/policy-search-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (name, val) => {
      const { stateFilter, setStateFilter, loadPolicy } = props

      switch (name) {
        case 'search':
          setStateFilter({
            ...stateFilter,
            [`${name}`]: val,
          })
          break
        case 'status':
          setStateFilter({
            ...stateFilter,
            [`${name}`]: val,
          })
          setTimeout(() => {
            loadPolicy(true)
          }, 300)
          break
        case 'date':
          setStateFilter({
            ...stateFilter,
            [`${name}`]: val,
          })
          setTimeout(() => {
            loadPolicy(true)
          }, 300)
          break
        default:
      }
    },
    handlePage: props => (value) => {
      const { stateFilter, setStateFilter, loadPolicy } = props
      setStateFilter({
        ...stateFilter,
        page: value,
      })
      setTimeout(() => {
        loadPolicy()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { statePolicy, setStatePolicy, stateFilter } = props
      setStatePolicy({
        ...statePolicy,
        page: {
          ...statePolicy.page,
          limit: value,
        },
      })
      const payload = {
        limit: value,
        date: stateFilter.date,
        status: stateFilter.search ? stateFilter.status : '',
        keyword: stateFilter.search,
      }

      history.push(`/policy-search-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        location,
        setStatePolicy, statePolicy,
        setStateFilter, stateFilter,
      } = this.props
      const newState = (qs.parse(location.search) || {})

      this.props.updateSiteConfiguration('activePage', 'policy')
      this.props.getDatas(
        { base: 'apiUser', url: `/insurance-letters${location.search}`, method: 'get' },
        null,
      ).then((res) => {
        setStatePolicy({
          ...statePolicy,
          load: false,
          list: res.data,
          page: res.meta,
        })
        setStateFilter({
          ...stateFilter,
          load: false,
          list: [
            { id: 'complete', name: 'Complete' },
            { id: 'need-approval', name: 'Need Approval' },
            { id: 'pending', name: 'Pending' },
            { id: 'approved', name: 'Approved' },
            { id: 'rejected', name: 'Rejected' },
            { id: 'in-process', name: 'In Process' },
            { id: 'expired', name: 'Expired' },
            { id: 'active', name: 'Active' },
            { id: 'ended', name: 'Ended' },
          ],
          search: newState.keyword,
          status: newState.status,
          date: newState.date,
          limit: newState.limit,
          page: res.meta.current_page,
        })
      }).catch((err) => {
        setStatePolicy({
          ...statePolicy,
          load: false,
          list: [],
        })
        setStateFilter({
          ...stateFilter,
          load: false,
          list: [
            { id: 'complete', name: 'Complete' },
            { id: 'need-approval', name: 'Need Approval' },
            { id: 'pending', name: 'Pending' },
            { id: 'approved', name: 'Approved' },
            { id: 'rejected', name: 'Rejected' },
            { id: 'in-process', name: 'In Process' },
            { id: 'expired', name: 'Expired' },
            { id: 'active', name: 'Active' },
            { id: 'ended', name: 'Ended' },
          ],
          search: newState.keyword,
          status: newState.status,
          date: newState.date,
        })
        message.error(err.message)
      })
    },
  }),
)(PolicyList)
