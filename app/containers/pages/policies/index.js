import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, lifecycle, withState } from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import Policy from 'components/pages/policies'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  return {
    groupRole, currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateCheck', 'setStateCheck', {
    isLocked: false,
  }),
  lifecycle({
    componentDidMount() {
      this.props.getDatas(
        { base: 'apiUser', url: `/auth/check-islocked/${window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1)}`, method: 'get' },
      ).then((res) => {
        if (window.location.pathname !== window.localStorage.getItem('prevPath')) {
          window.localStorage.setItem('isTrueSecondary', false)
          window.localStorage.setItem('prevPath', window.location.pathname)
        }
        if (res.data.is_locked === true) {
          this.props.setStateCheck({
            ...this.props.stateCheck,
            isLocked: true,
          })
        } else {
          this.props.setStateCheck({
            ...this.props.stateCheck,
            isLocked: false,
          })
        }
      }).catch(() => {})
      this.props.updateSiteConfiguration('activePage', 'policy-search-menu')
    },
  }),
)(Policy)
