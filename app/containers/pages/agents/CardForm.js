import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
  withPropsOnChange,
} from 'recompose'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { capitalize, debounce, isEmpty } from 'lodash'
import moment from 'moment'
import Swal from 'sweetalert2'
import CardForm from 'components/pages/agents/CardForm'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    detailAgent,
  } = state.root.agent

  return {
    detailAgent,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formCard' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateForm', 'setStateForm', {}),
    withState('stateFile', 'setStateFile', {
      file: [],
    }),
    withState('stateSelect', 'setStateSelect', {}),
    withHandlers({
      handleUpload: props => (info) => {
        const { stateFile, setStateFile } = props

        setStateFile({
          ...stateFile,
          file: info.fileList,
        })
      },
      handleSearch: props => (val) => {
        props.setStateSelect({ ...props.stateSelect, agentLoad: true })
        props.getDatas(
          { base: 'apiUser', url: `/search-agents?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch(({ errors }) => {
          if (errors.keyword) {
            props.form.setFields({
              agent_id: {
                value: val.agent_id,
                errors: [new Error(capitalize(errors.keyword))],
              },
            })
          }

          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: [],
          })
        })
      },
      handleSearchProduct: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/all-products?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateForm({
            ...props.stateForm,
            productLoad: false,
            productList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateForm({
            ...props.stateForm,
            productLoad: false,
            productList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        console.log(props)
        event.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            const { title, isEdit, data: currentData } = props.data
            let payload = {}

            if (title.toLowerCase() === 'product') {
              payload = {
                id: isEdit ? currentData.id : currentData.id,
                product: isEdit ? props.data.data.product : (props.stateForm.productList || []).find(item => item.id === values.product_id),
                product_id: values.product_id,
                effective_date: values.effective_date ? moment(values.effective_date).format('YYYY-MM-DD') : '',
                expiry_date: values.expiry_date ? moment(values.expiry_date).format('YYYY-MM-DD') : '',
                expiration_reason: values.expiration_reason,
              }
            }
            if (title.toLowerCase() === 'document') {
              payload = {
                id: isEdit ? currentData.id : '',
                document_category: isEdit ? props.data.data.document_category : (props.stateForm.documentList || []).find(item => item.id === values.document_category_id),
                document_category_id: values.document_category_id,
                description: values.description,
                files: values.file || !isEmpty(props.stateFile.file) ? props.stateFile.file : currentData.files,
              }
            }
            if (title.toLowerCase() === 'taxation') {
              if (values.type_id && values.ptkp_status_id && values.name && values.id) {
                payload = {
                  id: isEdit ? currentData.id : '',
                  type_id: values.type_id,
                  type: (props.stateForm.taxationList || []).find(item => item.id === values.type_id),
                  ptkp_status_id: values.ptkp_status_id,
                  ptkp_status: (props.stateForm.ptkpList || []).find(item => item.id === values.ptkp_status_id),
                  name: values.name,
                  npwp_number: values.npwp_number,
                }
              } else {
                message.error('Data cannot be empty!')
              }
            }
            if (title.toLowerCase() === 'bank') {
              props.getDatas(
                { base: 'apiUser', url: `/agents/${props.detailAgent.id}/bank_accounts`, method: 'post' },
                {
                  id: isEdit ? currentData.id : '',
                  bank_id: values.bank_id ? values.bank_id : '2',
                  name: values.name ? values.name : 'John De',
                  account_number: values.account_number ? Number(values.account_number) : '1234567890',
                },
              ).then((res) => {
                if (res) {
                  Swal.fire(
                    'Retrieve Bank Berhasil!',
                    '<span style="color:#2b57b7;font-weight:bold">Silakan cek email Kamu</span>',
                    'success',
                  ).then(() => {
                    window.location.reload()
                  })
                }
              }).catch(() => {
                Swal.fire(
                  'Retrieve Bank Gagal!',
                  '<span style="color:#2b57b7;font-weight:bold">Silakan coba lagi!</span>',
                  'error',
                ).then(() => {
                  window.location.reload()
                })
              })
            }

            const cardContract = [...new Set(props.stateCard.contracts.map(item => item.contract_number))]
            if (((title.toLowerCase() === 'contract') || (title.toLowerCase() === 'license'))) {
              if (!cardContract.includes(values[`${title.toLowerCase()}_number`]) || isEdit) {
                payload = {
                  id: currentData.id,
                  [`${title.toLowerCase()}_type`]: (props.stateForm[`${title.toLowerCase()}List`] || []).find(item => item.id === values[`${title.toLowerCase()}_type_id`]),
                  [`${title.toLowerCase()}_type_id`]: values[`${title.toLowerCase()}_type_id`],
                  [`${title.toLowerCase()}_number`]: values[`${title.toLowerCase()}_number`],
                  start_date: !isEmpty(values.start_date) ? moment(values.start_date).format('YYYY-MM-DD') : '',
                  end_date: !isEmpty(values.end_date) ? moment(values.end_date).format('YYYY-MM-DD') : '',
                  termination_date: (title.toLowerCase() === 'contract') && !isEmpty(values.termination_date) ? moment(values.termination_date).format('YYYY-MM-DD') : '',
                  termination_reasons: (title.toLowerCase() === 'contract') ? values.termination_reasons : '',
                }
              } else {
                message.error('Contract number is already exists')
              }
            }

            props.submitForm(payload, title, isEdit)
          }
        })
      },
    }),
    withPropsOnChange(
      ['handleSearchProduct'],
      ({ handleSearchProduct }) => ({
        handleSearchProduct: debounce(handleSearchProduct, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
       
        const { data, setStateFile, stateFile } = this.props
        let url = ''
        if ((data.title).toLowerCase() === 'product') url = `/agents/${this.props.detailAgent.id}/products-list`
        if ((data.title).toLowerCase() === 'contract') url = '/contract_types'
        if ((data.title).toLowerCase() === 'license') url = '/license_types'
        if ((data.title).toLowerCase() === 'bank') url = '/banks'
        if ((data.title).toLowerCase() === 'document') url = `/agents/${this.props.detailAgent.id}/documents-category`
        if ((data.title).toLowerCase() === 'taxation') {
          url = '/tax-types'
          this.loadMasterData({
            base: 'apiUser',
            method: 'get',
            url: '/tax-ptkp-statuses',
            name: 'ptkp',
          })
        }
        if (url) {
          this.loadMasterData({
            url,
            base: 'apiUser',
            method: 'get',
            name: (data.title).toLowerCase(),
          })
        }
        if (data.isEdit && (data.data.files || data.data.file_url)) {
          const files = (data.data.files || []).map(dataFile => ({
            uid: dataFile.id,
            thumbUrl: (dataFile.file_url || []),
            name: '',
          }))

          setStateFile({ ...stateFile, file: files })
        } else {
          setStateFile({ ...stateFile, file: [] })
        }
      },
      componentDidUpdate(prevProps) {
        const { data, setStateFile, stateFile } = this.props
        let url = ''
        if (data !== prevProps.data) {
          if ((data.title).toLowerCase() === 'product') url = `/agents/${this.props.detailAgent.id}/products-list`
          if ((data.title).toLowerCase() === 'contract') url = '/contract_types'
          if ((data.title).toLowerCase() === 'license') url = '/license_types'
          if ((data.title).toLowerCase() === 'bank') url = '/banks'
          if ((data.title).toLowerCase() === 'document') url = `/agents/${this.props.detailAgent.id}/documents-category`
          if ((data.title).toLowerCase() === 'taxation') {
            url = '/tax-types'
            this.loadMasterData({
              base: 'apiUser',
              method: 'get',
              url: '/tax-ptkp-statuses',
              name: 'ptkp',
            })
          }
          if (url) {
            this.loadMasterData({
              url,
              base: 'apiUser',
              method: 'get',
              name: (data.title).toLowerCase(),
            })
          }

          if (data.isEdit && data.data.file_url) {
            setStateFile({ ...stateFile, file: data.data.file_url })
          } else {
            setStateFile({ ...stateFile, file: [] })
          }
        }
      },
      loadMasterData(data) {
        this.props.getDatas(
          { base: data.base, url: data.url, method: data.method },
        ).then((res) => {
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: [],
          })
        })
      },
    }),
  )(CardForm),
)
