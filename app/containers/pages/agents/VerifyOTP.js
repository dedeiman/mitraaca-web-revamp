import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import VerifyOTPForm from 'components/pages/agents/VerifyOTP'
import { isEmpty } from 'lodash'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const { currentUser } = state.root.auth

  return {
    groupRole, currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formVerifyOTP' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        const {
          setStateButton, setStateUpdateButton,
          setBlocking, match,
          stateButton, stateUpdateButton, stateModalUpdate,
          setStateModalUpdate, isUpdatePayload,
        } = props.data

        const payloadProducts = { products: isUpdatePayload.products.length !== 0 ? isUpdatePayload.products : null }
        const payloadDoc = { agent_documents: isUpdatePayload.documents.length !== 0 ? isUpdatePayload.documents : null }
        const payloadlicenses = { licenses: isUpdatePayload.licenses.length !== 0 ? isUpdatePayload.licenses : null }
        const payloadContracts = { contracts: isUpdatePayload.contracts.length !== 0 ? isUpdatePayload.contracts : null }
        const payloadTax = { taxation: isUpdatePayload.taxation.length !== 0 ? isUpdatePayload.taxation : null }
        const payloadBanks = isUpdatePayload.banks.length !== 0 ? isUpdatePayload.banks : null

        props.form.validateFields((err, values) => {
          if (!err) {
            props.getDatas(
              { base: 'apiUser', url: '/otp/verifyotp', method: 'post' }, values,
            ).then((res) => {
              message.success('Data has been successfully updated', 5)
              setTimeout(() => {
                setStateModalUpdate({ ...stateModalUpdate, visible: false })
                props.form.resetFields()
              }, 300)

              if (res.meta.status) {
                setStateButton({
                  ...stateButton,
                  isFetching: true,
                })
                if (payloadProducts.products !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/products`, method: 'post' },
                    payloadProducts,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      products: resp.data.data.products || [],
                      products_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      products: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }else{
                  console.log(props.data.stateButton.products)
                  const products__filters = (props.data.stateButton.products || []).filter(item => item.expiration_reason !== '')
                  const payload = {
                    products: (products__filters || []).map(item => ({
                      id: item.id || null,
                      expiry_date: item.expiry_date,
                      expiration_reason: item.expiration_reason,
                    })),
                  }
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/products-update`, method: 'put' },
                    payload,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      products: resp.data.data.products || [],
                      products_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      products: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }
                if (payloadDoc.agent_documents !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/documents`, method: 'post' },
                    payloadDoc,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      documents: resp.data.data.documents || [],
                      documents_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      documents: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }else{
                  const documents_filters = (props.data.stateButton.documents || []).filter(item => item.files[0].id !== null || item.files[0].id !== undefined)
                  const payload = {
                    documents: (documents_filters || []).map(item => ({
                      document_category_id: item.document_category_id,
                      description: item.description,
                      files: ((item.files || []) || (item.file_url || [])).map((dataThumb) => ({
                        id: dataThumb.uid || dataThumb.id,
                        is_delete: false,
                        file: ((dataThumb.thumbUrl || dataThumb.file_url).includes('https://aca-web.s3.ap-southeast-1.amazonaws.com/') ? '' : dataThumb.thumbUrl),
                      })),
                    })),
                  }
                  console.log(payload)
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/documents`, method: 'post' },
                    payload,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      documents: resp.data.data.documents || [],
                      documents_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      documents: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }
                if (payloadlicenses.licenses !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/licenses`, method: 'post' },
                    payloadlicenses,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      licenses: resp.data.data.licenses || [],
                      licenses_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      licenses: [],
                    })
                  }).catch((error) => {
                    // let dataContracts = []
                    // stateButton.contracts.map((contract) => {
                    //   if (error.message.includes(contract.contract_number)) {
                    //     dataContracts = (stateButton.contracts || []).filter(contractId => contractId.contract_number !== contract.contract_number)
                    //   } else {
                    //     dataContracts = stateButton.contracts
                    //   }
                    //
                    //   return true
                    // })
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                    // setStateButton({
                    //   ...stateButton,
                    //   contracts: dataContracts,
                    //   isFetching: false,
                    // })
                  })
                }else{
                  const payload = {
                    licenses: (props.data.stateButton.licenses || []).map(item => ({
                      id: item.id || null,
                      license_type_id: item.license_type_id,
                      license_number: item.license_number,
                      start_date: item.start_date,
                      end_date: item.end_date,
                    })),
                  }
                  console.log(payload)
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/licenses-update`, method: 'put' },
                    payload,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      licenses: resp.data.data.licenses || [],
                      licenses_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      licenses: [],
                    })
                  }).catch((error) => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }
                if (payloadContracts.contracts !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/contracts`, method: 'post' },
                    payloadContracts,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      contracts: resp.data.data.contracts || [],
                      contracts_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      contracts: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }else{
                  const payload = {
                    contracts: (props.data.stateButton.contracts || []).map(item => ({
                      id: item.id || null,
                      termination_date: item.termination_date ? moment(item.termination_date).format('YYYY-MM-DD') : '',
                      termination_reasons: item.termination_reasons,
                    })),
                  }
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/contracts-terminate`, method: 'put' },
                    payload,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      contracts: resp.data.data.contracts || [],
                      contracts_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      contracts: [],
                    })
                  }).catch(() => {
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                  })
                }
                if (payloadTax.taxation !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/taxations`, method: 'post' },
                    payloadTax,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      taxation: resp.data.data.taxation || [],
                      taxation_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      taxation: [],
                    })
                  }).catch(() => {
                    // let dataContracts = []
                    // stateButton.contracts.map((contract) => {
                    //   if (error.message.includes(contract.contract_number)) {
                    //     dataContracts = (stateButton.contracts || []).filter(contractId => contractId.contract_number !== contract.contract_number)
                    //   } else {
                    //     dataContracts = stateButton.contracts
                    //   }
                    //
                    //   return true
                    // })
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                    // setStateButton({
                    //   ...stateButton,
                    //   contracts: dataContracts,
                    //   isFetching: false,
                    // })
                  })
                }
                if (payloadBanks !== null) {
                  props.getDatas(
                    { base: 'apiUser', url: `/agents/${match.params.id}/banks`, method: 'post' },
                    payloadBanks,
                  ).then((resp) => {
                    setBlocking(false)
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                      banks: resp.data.data.banks,
                      banks_meta: resp.data.meta || [],
                    })
                    setStateUpdateButton({
                      ...stateUpdateButton,
                      isFetching: false,
                      banks: [],
                    })
                  }).catch(() => {
                    // let dataContracts = []
                    // stateButton.contracts.map((contract) => {
                    //   if (error.message.includes(contract.contract_number)) {
                    //     dataContracts = (stateButton.contracts || []).filter(contractId => contractId.contract_number !== contract.contract_number)
                    //   } else {
                    //     dataContracts = stateButton.contracts
                    //   }
                    //
                    //   return true
                    // })
                    setStateButton({
                      ...stateButton,
                      isFetching: false,
                    })
                    setBlocking(false)
                    // setStateButton({
                    //   ...stateButton,
                    //   contracts: dataContracts,
                    //   isFetching: false,
                    // })
                  })
                }
              }
            }).catch((error) => {
              setStateButton({
                isFetching: false,
              })
              message.error(error.message, 5)
            })
          }
        })
      },
    }),
  )(VerifyOTPForm),
)
