import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
  withPropsOnChange,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import {
  debounce, pickBy, identity,
} from 'lodash'
import qs from 'query-string'
import history from 'utils/history'
import LevelTree from 'components/pages/agents/LevelTree'
import config from "../../../config";

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  return {
    currentUser,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'levelTree' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('state', 'changeState', {
      id_agent: '',
      branch_id: '',
    }),
    withState('stateCareer', 'setStateCareer', {
      dataLoad: true,
      dataList: [],
      dataRoot: {
        level_name: '',
        type: '',
        company_name: '',
        agent_name: '',
        branch_name: '',
      },
      branchLoad: true,
      branchList: [],
      agentList: [],
      isTree: true,
    }),
    withHandlers({
      handleExport: props => () => {
        const { location } = props
        const qString = qs.parse(location.search)
        console.log(props.stateCareer)
        const payload = qs.stringify({
          id_agent: props.stateCareer.agent_id || '',
          branch_id: qString.branch || '',
        })
        window.open(
          `${config.api_url}/all-level-tree/download?${payload}`,
          '_blank',
        )
      },
      handleBranch: props => (value) => {
        const payload = {
          key: props.stateCareer.agentId || '',
          branch: value || '',
        }

        localStorage.setItem('isTree', props.stateCareer.isTree)

        history.push(`/agent-search-menu/tree?${qs.stringify(pickBy(payload, identity))}`)
      },
      onSubmit: props => (event) => {
        event.preventDefault()

        localStorage.setItem('isTree', props.stateCareer.isTree)

        props.form.validateFields((err, values) => {
          if (!err) history.push(`/agent-search-menu/tree?${qs.stringify(pickBy({ key: props.stateCareer.agentId, branch: values.branch }, identity))}`)
        })
      },
      handleSearchAgent: props => (val) => {
        console.log(props.stateCareer)
        props.getDatas(
          { base: 'apiUser', url: `/search-agent-partners?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateCareer({
            ...props.stateCareer,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch(() => {
          props.setStateCareer({
            ...props.stateCareer,
            agentLoad: false,
            agentList: [],
          })
        })
      },
    }),
    withPropsOnChange(
      ['handleSearchAgent'],
      ({ handleSearchAgent }) => ({
        handleSearchAgent: debounce(handleSearchAgent, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        const { location, stateCareer, setStateCareer } = this.props
        const qString = qs.parse(location.search)

        setStateCareer({
          ...stateCareer,
          isTree: localStorage.getItem('isTree') === 'true',
        })

        this.props.getDatas(
          { base: 'apiUser', url: `/search-agent-partners?keyword=${qString.key || ''}`, method: 'get' },
        ).then((res) => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            key: 'ACA',
            agentId: '',
            branch: qString.branch ? Number(qString.branch) : undefined,
            agentLoad: false,
            agentList: res.data,
          })
          this.loadMaster(
            {
              url: '/all-level-tree',
              method: 'post',
              payload: pickBy(
                {
                  id_agent: qString.key ? res.data[0].id : '',
                  branch_id: qString.branch ? Number(qString.branch) : '',
                },
                identity,
              ),
            },
          )
        }).catch(() => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            key: qString.key || '',
            branch: qString.branch ? Number(qString.branch) : undefined,
            agentLoad: false,
            agentList: [],
          })
          this.loadMaster(
            {
              url: '/all-level-tree',
              method: 'post',
              payload: pickBy(
                {
                  id_agent: '',
                  branch_id: qString.branch ? Number(qString.branch) : '',
                },
                identity,
              ),
            },
          )
        })
      },
      loadMaster(data) {
        const { url, method, payload } = data
        this.props.getDatas(
          { base: 'apiUser', url, method },
          payload,
        ).then((res) => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            dataLoad: false,
            dataRoot: res.data,
            dataList: res.data.partners,
          })
          this.loadBranch({
            url: '/branches', method: 'get', payload: null,
          })
        }).catch(() => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            dataLoad: false,
            dataRoot: [],
            dataList: [],
          })
          this.loadBranch({
            url: '/branches', method: 'get', payload: null,
          })
        })
      },
      loadBranch(data) {
        const { url, method, payload } = data
        this.props.getDatas(
          { base: 'apiUser', url, method },
          payload,
        ).then((res) => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            branchLoad: false,
            branchList: res.data,
          })
        }).catch(() => {
          this.props.setStateCareer({
            ...this.props.stateCareer,
            branchLoad: false,
            branchList: [],
          })
        })
      },
    }),
  )(LevelTree),
)
