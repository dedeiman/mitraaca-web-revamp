import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import LevelUpForm from 'components/pages/agents/LevelUp'

export function mapStateToProps(state) {
  const { role } = state.root


  return { role }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formLevelUp' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateForm', 'setStateForm', {
      levelLoad: false,
      levelList: [],
      isSubmiting: false,
    }),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          const payload = {
            ...values,
            reason: values.reason ? (values.reason).toUpperCase() : '',
          }

          if (!err) {
            const { data, stateForm, setStateForm } = props
            props.getDatas(
              { base: 'apiUser', url: `/agents/${data.agent_id}/level-up`, method: 'post' },
              payload,
            ).then(() => {
              setStateForm({ ...stateForm, isSubmiting: false })
              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Level Up berhasil !</span>',
                'success',
              ).then(() => {
                props.toggle('reload')
              })
            }).catch((error) => {
              message.error(error)
              setStateForm({ ...stateForm, isSubmiting: false })
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {

        this.props.getDatas(
          { base: 'apiUser', url: '/levels', method: 'get' },
        ).then((res) => {
          this.props.setStateForm({
            ...this.props.stateForm,
            levelLoad: false,
            levelList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateForm({
            ...this.props.stateForm,
            levelLoad: false,
            levelList: [],
          })
        })
      },
    }),
  )(LevelUpForm),
)
