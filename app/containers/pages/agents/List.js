import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { message } from 'antd'
import ListAgents from 'components/pages/agents/List'
import { fetchAgent } from 'actions/Agent'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    dataAgent,
    metaAgent,
  } = state.root.agent

  return {
    currentUser,
    isFetching,
    dataAgent,
    metaAgent,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchAgent: bindActionCreators(fetchAgent, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateLevelUp', 'setStateLevelUp', {
    visible: false,
    data: '',
  }),
  withState('stateAgent', 'setStateAgent', props => ({
    branchLoad: true,
    birthLoad: false,
    levelLoad: true,
    typeLoad: true,
    typeAgentLoad: true,
    branchList: [],
    perwakilanList: [],
    ulasList: [],
    levelList: [],
    search: '',
    searchType: 'Name',
    agentType: '',
    branch: '',
    perwakilan: '',
    birth: '',
    level: '',
    joinDate: [],
    page: props.metaAgent.current_page,
    perPage: props.metaAgent.per_page,
  })),
  withHandlers({
    loadAgent: props => (isSearch) => {
      const {
        search, searchType, agentType, branch,
        perwakilan, birth, level, joinDate, page,
      } = props.stateAgent
      const payload = {
        search,
        level,
        search_type: searchType,
        agent_type: agentType,
        branch_id: branch,
        branch_perwakilan_id: perwakilan,
        month_of_birth: Number(birth) < 10 ? `0${birth}` : birth,
        begin_date: joinDate[0],
        end_date: joinDate[1],
        page: isSearch ? '' : page,
      }

      history.push(`/agent-search-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    getPerwakilan: props => (value) => {
      props.getDatas(
        { base: 'apiUser', url: `/branch-perwakilan/${value}`, method: 'get' },
      ).then((res) => {
        props.setStateAgent({
          ...props.stateAgent,
          perwakilanLoad: false,
          branch: value,
          perwakilan: undefined,
          perwakilanList: res.data,
        })
      }).catch((err) => {
        message.error(err)
        props.setStateAgent({
          ...props.stateAgent,
          perwakilanLoad: false,
          branch: value,
          perwakilan: undefined,
          perwakilanList: [],
        })
      })
    },
    handlePage: props => (value) => {
      const { stateAgent, setStateAgent, loadAgent } = props
      setStateAgent({
        ...stateAgent,
        page: value,
      })
      setTimeout(() => {
        loadAgent()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { stateAgent, setStateAgent } = props
      setStateAgent({
        ...stateAgent,
        perPage: value,
      })
      setTimeout(() => {
        const payload = {
          per_page: value,
          customer_status: stateAgent.search ? stateAgent.filter : '',
          search: stateAgent.search,
        }
        props.fetchAgent(`?${qs.stringify(pickBy(payload, identity))}`)
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      if (window.location.pathname !== window.localStorage.getItem('prevPath')) {
        window.localStorage.setItem('isTrueSecondary', false)
        window.localStorage.setItem('prevPath', window.location.pathname)
      }
      const { location, setStateAgent, stateAgent } = this.props
      const newState = (qs.parse(location.search) || {})
      const config = [
        { url: '/branches', name: 'branch' },
        { url: '/levels', name: 'level' },
        { url: '/search-types', name: 'type' },
      ]

      this.props.fetchAgent(location.search || '?search_type=name')

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchAgent(location.search || '?search_type=name')
      }

      config.map(item => this.loadMasterData(item.url, item.name))

      if (newState.branch_id) {
        this.loadMasterData(`/branch-perwakilan/${newState.branch_id}`, 'perwakilan')
      }

      setStateAgent({
        ...stateAgent,
        search: newState.search,
        searchType: newState.search_type || 'Name',
        agentType: newState.agent_type || '',
        branch: newState.branch_id ? Number(newState.branch_id) : undefined,
        birth: newState.month_of_birth ? Number(newState.month_of_birth) : undefined,
        perwakilan: newState.branch_perwakilan_id ? Number(newState.branch_perwakilan_id) : undefined,
        level: newState.level ? Number(newState.level) : undefined,
        joinDate: (newState.begin_date && newState.end_date) ? [newState.begin_date, newState.end_date] : [],
        page: newState.page,
      })
    },
    loadMasterData(url, name) {
      this.props.getDatas(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        if (name === 'type') {
          const arrType = res.data
          arrType.forEach((item, i) => {
            if (item.value === 'Name') {
              arrType.splice(i, 1)
              arrType.unshift(item)
            }
          })

          this.props.setStateAgent({
            ...this.props.stateAgent,
            [`${name}Load`]: false,
            [`${name}List`]: arrType,
          })
        }

        this.props.setStateAgent({
          ...this.props.stateAgent,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateAgent({
          ...this.props.stateAgent,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(ListAgents)
