/* eslint-disable array-callback-return */
/* eslint-disable no-eval */
/* eslint-disable no-unused-vars */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-expressions */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers,
  lifecycle, withState,
  withPropsOnChange,
} from 'recompose'
import {
  createAgent, updateAgent,
  fetchDetailAgent, agentDetail,
} from 'actions/Agent'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { debounce, isEmpty } from 'lodash'
import FormAgents from 'components/pages/agents/Form'
import history from 'utils/history'
import Helper from 'utils/Helper'
import Swal from 'sweetalert2'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailAgent,
  } = state.root.agent

  return {
    isFetching,
    detailAgent,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  createAgent: bindActionCreators(createAgent, dispatch),
  updateAgent: bindActionCreators(updateAgent, dispatch),
  fetchDetailAgent: bindActionCreators(fetchDetailAgent, dispatch),
  agentDetail: bindActionCreators(agentDetail, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formAgents' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('file', 'setFile', ''),
    withState('stateSelects', 'setStateSelects', {
      source: [
        { id: 'individu', name: 'Individu' },
        { id: 'corporate', name: 'Corporate' },
      ],
    }),
    withState('stateCountry', 'setStateCountry', {
      load: true,
      list: [],
    }),
    withState('stateCountryCode', 'setStateCountryCode', {
      load: true,
      list: [],
    }),
    withState('stateUlas', 'setStateUlas', {
      ulasLoad: false,
      ulaslist: [],
    }),
    withState('state', 'changeState', {
      profile_id: '',
      id_no: '',
      tax_id_no: '',
      status: '',
      is_existing_core_agent: false,
    }),
    withHandlers({
      loadLocation: props => (url, name) => {
        props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
      loadUlas: props => (url, name) => {
        props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateUlas({
            ...props.stateUlas,
            ulasLoad: false,
            ulasList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateSelects({
            ...props.stateSelects,
            ulasLoad: false,
            ulasList: [],
          })
        })
      },
    }),
    withHandlers({
      handleProfileId: props => async (value) => {
        const payload = {
          profile_id: value,
          id_no: '',
          tax_id_no: '',
        }
        props.getDatas(
          { base: 'apiUser', url: '/check-agent-core', method: 'post' },
          payload,
        ).then((res) => {
          props.form.setFields({
            address: {
              value: res.data.address,
            },
            no_ktp: {
              value: res.data.ktp_no,
            },
            phone_number: {
              value: res.data.mobile,
            },
            name: {
              value: res.data.name,
            },
            npwp: {
              value: res.data.npwp,
            },
            ref_sys_user_id: {
              value: res.data.ref_sys_user_id,
            },
            sys_user_id: {
              value: res.data.sys_user_id,
            },
          })
          props.changeState({
            ...props.state,
            is_existing_core_agent: true,
          })
        }).catch((err) => {
          props.changeState({
            ...props.state,
            is_existing_core_agent: false,
          })
        })
      },
    }),
    withHandlers({
      handleLocation: props => (url, key) => {
        props.loadLocation(url, key)
      },
      handleUpload: props => (info) => {
        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'
        if (!isFileType) {
          message.error('You can only upload JPG/PNG file!')
          return props.form.setFieldsValue({ file: undefined })
        }
        return (
          Helper.getBase64(info.file, file => props.setFile(file))
        )
      },
      handleSearchAgent: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/upline-agents?upline_agent_id=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch(() => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        console.log(props)
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          console.log(values)
          const arrAgent = (props.stateSelects.agentList || []).map(item => item.agent_id)
          const uplineId = (values.upline_id || '').split(' - ')[0]

          if (!err) {
            const codeId = (props.stateCountry.list).filter(code => code.phone_code === values.phone_code_id)
            const picCodeId = (props.stateCountry.list).filter(code => code.phone_code === values.pic_phone_country_code)

            let npwp = isEmpty(values.taxation_id) ? '' : values.taxation_id

            npwp = npwp.split('-').join('')
            npwp = npwp.split('.').join('')
            npwp = npwp.split(' ').join('')
            const payload = {
              ...values,
              name: values.name ? values.name.toUpperCase() : '',
              address: values.address ? values.address.toUpperCase() : '',
              residence_address: values.residence_address ? values.residence_address.toUpperCase() : '',
              pic_name: values.pic_name ? values.pic_name.toUpperCase() : '',
              taxation_name: values.taxation_name ? values.taxation_name.toUpperCase() : '',
              birth_place: values.birth_place ? values.birth_place.toUpperCase() : '',
              other_employment: values.other_employment ? values.other_employment.toUpperCase() : '',
              email: values.email ? values.email.toUpperCase() : '',
              profile_id: values.profile_id ? values.profile_id.toUpperCase() : '',
              reference_id: values.reference_id ? values.reference_id.toUpperCase() : '',
              syariah_id: values.syariah_id ? values.syariah_id.toUpperCase() : '',
              upline_id: values.upline_id ? uplineId : '',
              phone_code_id: codeId[0].id,
              pic_phone_code_id: picCodeId[0].id,
              birthdate: values.birthdate ? moment(values.birthdate).format('YYYY-MM-DD') : '',
              file: values.file ? props.file : '',
              taxation_id: npwp,
              is_existing_core_agent: props.state.is_existing_core_agent,
            }
            props[`${params.id ? 'upd' : 'cre'}ateAgent`](payload, (params.id || null), true)
              .then(() => (
                Swal.fire(`Agent telah di${params.id ? 'perbarui' : 'tambahkan'}`, '<span style="color:#2b57b7;font-weight:bold">Kembali ke List Agent</span>', 'success')
                  .then(() => history.push('/agent-search-menu'))
              ))
              .catch((error) => {
                Swal.fire(
                  '',
                  '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
                  'error',
                )
                if (error.no_ktp) {
                  props.form.setFields({
                    no_ktp: {
                      value: values.no_ktp,
                      errors: [new Error(error.no_ktp)],
                    },
                  })
                }
                if (error.email) {
                  props.form.setFields({
                    email: {
                      value: values.email,
                      errors: [new Error(error.email)],
                    },
                  })
                }
                // if (error.phone_number) {
                //   props.form.setFields({
                //     phone_number: {
                //       value: values.phone_number,
                //       errors: [new Error(error.phone_number)],
                //     },
                //   })
                // }
                if (error.address_zip_code) {
                  props.form.setFields({
                    address_zip_code: {
                      value: values.address_zip_code,
                      errors: [new Error(error.address_zip_code)],
                    },
                  })
                }
                if (error.birthdate) {
                  props.form.setFields({
                    birthdate: {
                      value: values.birthdate,
                      errors: [new Error(error.birthdate)],
                    },
                  })
                }
                if (error.business_license_number) {
                  props.form.setFields({
                    business_license_number: {
                      value: values.business_license_number,
                      errors: [new Error(error.business_license_number)],
                    },
                  })
                }
                if (error.pic_name) {
                  props.form.setFields({
                    pic_name: {
                      value: values.pic_name,
                      errors: [new Error(error.pic_name)],
                    },
                  })
                }
                if (error.reference_id) {
                  props.form.setFields({
                    reference_id: {
                      value: values.reference_id,
                      errors: [new Error(error.reference_id)],
                    },
                  })
                }
                if (error.tax_type_id) {
                  props.form.setFields({
                    tax_type_id: {
                      value: values.tax_type_id,
                      errors: [new Error(error.tax_type_id)],
                    },
                  })
                }
                if (error.taxation_id) {
                  props.form.setFields({
                    taxation_id: {
                      value: values.taxation_id,
                      errors: [new Error(error.taxation_id)],
                    },
                  })
                }
                if (error.taxation_name) {
                  props.form.setFields({
                    taxation_name: {
                      value: values.taxation_name,
                      errors: [new Error(error.taxation_name)],
                    },
                  })
                }
                if (error.upline_id) {
                  props.form.setFields({
                    upline_id: {
                      value: values.upline_id,
                      errors: [new Error(error.upline_id)],
                    },
                  })
                }
              })
          } else {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
        })
      },
    }),
    withPropsOnChange(
      ['handleSearchAgent'],
      ({ handleSearchAgent }) => ({
        handleSearchAgent: debounce(handleSearchAgent, 300),
      }),
    ),
    lifecycle({
      componentWillMount() {
        const { match } = this.props
        const config = [
          { url: '/marital-statuses', name: 'marital' },
          { url: '/tax-types', name: 'tax' },
          { url: '/tax-ptkp-statuses', name: 'taxPtkpStatuses' },
          { url: '/religions/all', name: 'religions' },
          { url: '/genders/all', name: 'genders' },
          { url: '/employments', name: 'employment' },
          { url: '/levels', name: 'level' },
          { url: '/branches', name: 'branch' },
          { url: '/provinces', name: 'provincy' },
        ]
        if (match.params.id) {
          this.props.fetchDetailAgent(match.params.id)
          window.onresize = () => {
            this.props.fetchDetailAgent(match.params.id)
          }
        } else {
          this.props.agentDetail({})
          window.onresize = () => {
            this.props.agentDetail({})
          }
        }

        setTimeout(() => {
          config.map(item => (
            this.loadMasterData(item.url, item.name)
          ))
        }, 2000)
      },
      componentDidUpdate(prevProps) {
        const {
          detailAgent, setFile, loadLocation, match, handleSearchAgent,
        } = this.props
        if (match.params.id && (detailAgent !== prevProps.detailAgent)) {
          const { user, branch_id: branchId } = detailAgent
          setFile(user.profile_pic)
          handleSearchAgent(detailAgent.upline_id)
          loadLocation(`/branch-perwakilan/${branchId}`, 'perwakilan')
          loadLocation(`/ulas?branch_id=${branchId}`, 'ulas')
        }
      },
      componentDidMount() {
        window.google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line
        this.props.getDatas(
          { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
        ).then((res) => {
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: res.data,
          })
          this.props.setStateCountryCode({
            ...this.props.stateCountryCode,
            load: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          this.props.setStateCountry({
            ...this.props.stateCountry,
            load: false,
            list: [],
          })
          this.props.setStateCountryCode({
            ...this.props.stateCountryCode,
            load: false,
            list: [],
          })
        })
      },
      loadMasterData(url, name) {
        // const { setStateSelects, stateSelects } = this.props
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
      initAutocomplete() {
        const inputNodeAddress = document.getElementById('formAgents_address')
        const inputNodeAddressAlt = document.getElementById('formAgents_residence_address')

        // Don't remove variable
        const autoCompleteAddress = new window.google.maps.places.Autocomplete(inputNodeAddress)
        const autoCompleteAddressAlt = new window.google.maps.places.Autocomplete(inputNodeAddressAlt)
        const address = [
          { key: 'Address' },
          { key: 'AddressAlt' },
        ]

        address.map((item) => {
          window.google.maps.event.addDomListener(eval(`inputNode${item.key}`), 'keydown', () => {
            if (event.key === 'Enter') {
              event.preventDefault()
            }
          })

          eval(`autoComplete${item.key}`).addListener('place_changed', () => {
            const { setFieldsValue } = this.props.form
            const place = eval(`autoComplete${item.key}`).getPlace()
            const location = JSON.stringify(place.geometry.location)
            const data = JSON.parse(location)
            data.address = place.formatted_address

            if (item.key === 'Address') {
              setFieldsValue({ address: (data.address).toUpperCase() })
            } else {
              setFieldsValue({ residence_address: (data.address).toUpperCase() })
            }
          })
        })
      },
    }),
  )(FormAgents),
)
