/* eslint-disable no-unused-expressions */
/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import config from 'config'
import { message } from 'antd'
import { fetchDetailAgent, deleteAgent } from 'actions/Agent'
import { getDatas } from 'actions/Option'
import { updateSiteConfiguration } from 'actions/Site'
import Swal from 'sweetalert2'
import history from 'utils/history'
import DetailAgent from 'components/pages/agents/Detail'
import { capitalize, isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const { currentUser } = state.root.auth

  const {
    isFetching,
    detailAgent,
  } = state.root.agent

  return {
    isFetching,
    detailAgent,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailAgent: bindActionCreators(fetchDetailAgent, dispatch),
  deleteAgent: bindActionCreators(deleteAgent, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('avatar', 'setAvatar', props => props.detailAgent.profile_pic),
  withState('isBlocking', 'setBlocking', true),
  withState('isVerifyOtp', 'setVerifyOtp', {
    isLoading: false,
    count: 60,
  }),
  withState('stateModal', 'setStateModal', props => ({
    visible: false,
    data: props.detailAgent,
  })),
  withState('stateModalUpdate', 'setStateModalUpdate', {
    visible: false,
  }),
  withState('stateCard', 'setStateCard', {
    visible: false,
    title: '',
    data: {},
  }),
  withState('stateUpdateCard', 'setStateUpdateCard', {
    visible: false,
    title: '',
    data: {},
  }),
  withState('isUpdatePayload', 'setUpdatePayload', {}),
  withState('listButton', 'setListButton', {
    list: ['Product', 'Contract', 'License', 'Bank', 'Taxation', 'Document', 'Training', 'Level', 'Contest', 'History'],
    active: 'Product',
  }),
  withState('stateButton', 'setStateButton', {
    products: [],
    products_pagination: 1,
    contracts: [],
    contracts_pagination: 1,
    licenses: [],
    licenses_pagination: 1,
    taxation: [],
    taxation_pagination: 1,
    documents: [],
    documents_pagination: 1,
    training_histories: [],
    training_histories_pagination: 1,
    contest_histories: [],
    contest_histories_pagination: 1,
    bank_accounts: [],
    bank_accounts_pagination: 1,
    'audit-trails': [],
    audit_trails_pagination: 1,
    'level-histories': [],
    level_histories_pagination: 1,
    isFetching: false,
  }),
  withState('stateUpdateButton', 'setStateUpdateButton', {
    products: [],
    contracts: [],
    licenses: [],
    taxation: [],
    documents: [],
    training_histories: [],
    contest_histories: [],
    bank_accounts: [],
    'audit-trails': [],
    'level-histories': [],
    isFetching: false,
  }),
  withHandlers({
    handleBlockNavigation: props => (nextLocation) => {
      if (props.isBlocking) {
        Swal.fire(
          '',
          `<span style="color:#2b57b7;font-weight:bold">Are you sure to leave this page?</span>`,
          'warning',
        ).then((res) => {
          if (res.value) {
            props.setBlocking(false)
            return history.push(nextLocation.pathname)
          }
          return props.updateSiteConfiguration('activePage', 'agents')
        })
        return false
      }
      return true
    },
    handleDetail: props => (type) => {
      const { listButton, setListButton } = props
      setListButton({
        ...listButton,
        active: type,
      })
    },
    collectDataCard: props => (data, title, isEdit) => {
      const {
        stateButton, setStateButton, stateUpdateButton, setStateUpdateButton,
        stateCard, setStateCard, stateUpdateCard, setStateUpdateCard,
      } = props
      let currentState = ''
      if (title.toLowerCase() === 'product') currentState = 'products'
      if (title.toLowerCase() === 'contract') currentState = 'contracts'
      if (title.toLowerCase() === 'license') currentState = 'licenses'
      if (title.toLowerCase() === 'taxation') currentState = 'taxation'
      if (title.toLowerCase() === 'document') currentState = 'documents'
      if (title.toLowerCase() === 'bank') currentState = 'bank_accounts'
      const newState = stateButton[currentState] || []
      const newUpdateState = stateUpdateButton[currentState] || []
      if (isEdit) {
        const newData = {
          ...((stateButton[currentState] || []).find(item => item.id === data.id) || {}),
          ...data,
        }
        const arrKey = (stateButton[currentState] || []).findIndex(item => item.id === data.id)
        newState[arrKey] = newData
        setStateButton({ ...stateButton, [currentState]: newState })
        setStateCard({
          ...stateCard,
          visible: false,
          title: '',
          data: {},
        })
        setStateUpdateButton({ ...stateUpdateButton, [currentState]: newUpdateState })
        setStateUpdateCard({
          ...stateUpdateCard,
          visible: false,
          title: '',
          data: {},
        })
      } else {
        if (!isEmpty(data)) {
          newState.push(data)
          setStateCard({
            ...stateCard,
            visible: false,
            title: '',
            data: {},
          })
          newUpdateState.push(data)
          setStateUpdateCard({
            ...stateUpdateCard,
            visible: false,
            title: '',
            data: {},
          })
        }

        setStateButton({ ...stateButton, [currentState]: newState })
        setStateUpdateButton({ ...stateUpdateButton, [currentState]: newUpdateState })
      }

      props.setBlocking(false)
    },
    loadMasterData: props => () => {
      props.getDatas(
        { base: 'apiUser', url: `/agents/${props.match.params.id}/products`, method: 'get' },
      ).then((res) => {
        props.setStateButton({
          ...props.stateButton,
          isFetching: false,
          products: res.data,
        })
      }).catch((err) => {
        message.error(err)
        props.setStateButton({
          ...props.stateButton,
          isFetching: false,
          products: [],
        })
      })
    },
    handlePageProduct: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'products'
      const meta = 'products_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        products_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageContract: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'contracts'
      const meta = 'contracts_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        contracts_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageLicense: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'licenses'
      const meta = 'licenses_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        licenses_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageTaxation: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'taxation'
      const meta = 'taxation_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        taxation_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageBank: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'bank_accounts'
      const meta = 'bank_accounts_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        bank_accounts_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageDocument: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'documents'
      const meta = 'documents_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        documents_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageTraining: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'training_histories'
      const meta = 'training_histories_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        training_histories_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageLevel: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'level-histories'
      const meta = 'level_histories_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        level_histories_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageContest: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'contest_histories'
      const meta = 'contest_histories_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        contest_histories_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handlePageHistory: props => (value) => {
      const { stateButton, setStateButton } = props
      const url = 'audit-trails'
      const meta = 'audit_trails_meta'
      setStateButton({
        ...stateButton,
        isFetching: true,
        audit_trails_pagination: value,
      })
      setTimeout(() => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${props.match.params.id}/${url}?page=${value}&per_page=10`, method: 'get' },
        ).then((res) => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
    handleUpdateCard: props => () => {
      props.setStateModalUpdate({ ...props.stateModalUpdate, visible: true })

      const {
        products, documents, licenses, contracts,
        taxation, bank_accounts: bankAccounts,
      } = props.stateUpdateButton

      const payload = {
        products: (products || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          product_id: item.product_id,
          effective_date: item.effective_date,
          expiry_date: item.expiry_date,
          expiration_reason: item.expiration_reason,
        })),
        documents: (documents || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          document_category_id: (item.document_category_id || item.document_category.id),
          description: item.description,
          files: ((item.files || []) || (item.file_url || [])).map((dataThumb) => ({
            id: dataThumb.id || null,
            is_delete: false,
            file: ((dataThumb.thumbUrl || dataThumb.file_url).includes('https://aca-web.s3.ap-southeast-1.amazonaws.com/') ? '' : dataThumb.thumbUrl),
          })),
        })),
        licenses: (licenses || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          license_type_id: item.license_type_id,
          license_number: item.license_number,
          start_date: item.start_date,
          end_date: item.end_date,
        })),
        contracts: (contracts || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          contract_type_id: item.contract_type_id,
          contract_number: item.contract_number,
          start_date: item.start_date,
          end_date: item.end_date,
          termination_date: item.termination_date,
          termination_reasons: item.termination_reasons,
        })),
        taxation: (taxation || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          type_id: item.type_id,
          ptkp_status_id: item.ptkp_status_id,
          name: item.name,
          npwp_number: item.npwp_number,
        })),
        banks: (bankAccounts || []).map(item => ({
          id: !isEmpty(item.id) ? item.id : null,
          bank_id: item.bank_id,
          name: item.name,
          account_number: item.account_number ? Number(item.account_number) : null,
        })),
      }

      console.log(payload)

      props.setUpdatePayload(payload)
      props.setBlocking(true)
      props.setStateButton({
        ...props.stateButton,
        isFetching: true,
      })

      props.getDatas(
        { base: 'apiUser', url: '/otp/sendotp', method: 'post' }, {
          action: 'update-card',
        },
      ).then(() => {
        message.success('Berhasil Mengirim OTP, Silakan cek email!', 5)
      }).catch((err) => {
        message.error(err.message, 5)
      })
    },
    donwloadHistory: () => (id) => {
      window.open(
        `${config.api_url}/agents/${id}/audit-trails/download`,
        '_blank',
      )
    },
    sendVerification: props => () => {
      props.getDatas(
        { base: 'apiUser', url: `/agents/${props.match.params.id}/resend-verification`, method: 'post' },
      ).then((res) => {
        Swal.fire(
          '',
          `<span style="color:#2b57b7;font-weight:bold">${capitalize(res.meta.message)}</span>`,
          'success',
        )
      }).catch((err) => {
        Swal.fire(
          '',
          `<span style="color:#2b57b7;font-weight:bold">${capitalize(err.response.meta.message)}</span>`,
          'error',
        )
      })
    },
    handleResendVerifyOTP: props => () => {
      props.setVerifyOtp({ ...props.isVerifyOtp, isLoading: true })
      props.getDatas(
        { base: 'apiUser', url: '/otp/sendotp', method: 'post' }, {
          action: 'update-card',
        },
      ).then(() => {
        if (props.isVerifyOtp.count > 0) {
          props.setVerifyOtp({ isLoading: true, count: 59 })
        }
        message.success('Berhasil Mengirim OTP, Silakan cek email!', 5)
      }).catch((err) => {
        props.setVerifyOtp({ ...props.isVerifyOtp, isLoading: false })
        message.error(err.message, 5)
      })
    },
    handleCancelCard: props => () => {
      props.setStateButton({
        ...props.stateButton,
        isFetching: true,
      })

      Swal.fire({
        title: '',
        html: '<span style="color:#2b57b7;font-weight:bold">Semua data yang anda perbarui akan terhapus</span>',
        icon: 'warning',
        showCancelButton: true,
      }).then((res) => {
        if (res.value) {
          setTimeout(() => {
            const contractExist = props.stateButton.contracts.filter(contract => !isEmpty(contract.id))
            const productExist = props.stateButton.products.filter(product => !isEmpty(product.id))
            const licenseExist = props.stateButton.licenses.filter(license => !isEmpty(license.id))
            const documentExist = props.stateButton.documents.filter(document => !isEmpty(document.id))

            props.setStateButton({
              ...props.stateButton,
              contracts: contractExist,
              products: productExist,
              licenses: licenseExist,
              documents: documentExist,
              isFetching: false,
            })
          }, 300)
        } else {
          setTimeout(() => {
            props.setStateButton({
              ...props.stateButton,
              isFetching: false,
            })
          }, 300)
        }
      })
      return false
    },
  }),
  lifecycle({
    componentDidMount() {

      const { match, listButton } = this.props

      listButton.list.map((item) => {
        let url = `${item.toLowerCase()}s`
        let meta = `${item.toLowerCase()}s_meta`
        if (item === 'Taxation') {
          url = 'taxation'
          meta = 'taxation_meta'
        }
        if (item === 'Contest') {
          url = 'contest_histories'
          meta = 'contest_histories_meta'
        }
        if (item === 'Training') {
          url = 'training_histories'
          meta = 'training_histories_meta'
        }
        if (item === 'Bank') {
          url = 'bank_accounts'
          meta = 'bank_accounts_meta'
        }
        if (item === 'History') {
          url = 'audit-trails'
          meta = 'audit_trails_meta'
        }
        if (item === 'Level') {
          url = 'level-histories'
          meta = 'level_histories_meta'
        }

        return this.loadMasterCard(url, meta)
      })

      if (match.params.id) {
        this.props.fetchDetailAgent(match.params.id)
        window.onresize = () => {
          this.props.fetchDetailAgent(match.params.id)
        }
      } else {
        this.props.agentDetail({})
        window.onresize = () => {
          this.props.agentDetail({})
        }
      }
    },
    componentDidUpdate(prevProps) {
      const { detailAgent, setAvatar } = this.props
      if (detailAgent !== prevProps.detailAgent) {
        setAvatar(detailAgent.user.profile_pic)
      }

      if (this.props.isVerifyOtp.count !== prevProps.isVerifyOtp.count) {
        if (this.props.isVerifyOtp.count > 0) {
          setTimeout(() => this.props.setVerifyOtp({ ...this.props.isVerifyOtp, count: this.props.isVerifyOtp.count - 1 }), 1000)
        } else {
          this.props.setVerifyOtp({ count: 60, isLoading: false })
        }
      }
    },
    loadMasterCard(url, meta) {
      setTimeout(() => {
        this.props.getDatas(
          { base: 'apiUser', url: `/agents/${this.props.match.params.id}/${url}`, method: 'get' },
        ).then((res) => {
          this.props.setStateButton({
            ...this.props.stateButton,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [meta]: url === 'level-histories' ? res.data.histories : res.meta,
          })
        }).catch(() => {
          this.props.setStateButton({
            ...this.props.stateButton,
            isFetching: false,
            [url]: [],
            [meta]: [],
          })
        })
      }, 300)
    },
  }),
)(DetailAgent)
