import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import FAQ from 'components/pages/faq/List'
import { fetchFaq } from 'actions/Faq'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import config from 'app/config'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    dataFaq,
    metaFaq,
  } = state.root.faq

  return {
    isFetching,
    currentUser,
    dataFaq,
    metaFaq,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchFaq: bindActionCreators(fetchFaq, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateFaq', 'setStateFaq', props => ({
    load: true,
    list: [],
    search: '',
    filter: undefined,
    page: props.metaFaq.current_page,
  })),
  withHandlers({
    loadFaq: props => (isSearch) => {
      const { filter, search, page } = props.stateFaq
      const payload = {
        category_id: filter,
        question: search,
        page: isSearch ? 1 : page,
      }

      history.push(`/faq-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateFaq, setStateFaq, loadFaq } = props
      setStateFaq({
        ...stateFaq,
        filter: val,
      })
      setTimeout(() => {
        loadFaq(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateFaq, setStateFaq, loadFaq } = props
      setStateFaq({
        ...stateFaq,
        page: value,
      })
      setTimeout(() => {
        loadFaq()
      }, 300)
    },
    handleCategory: props => (value, category) => {
      props.fetchFaq(value.faq_search, '', category)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { location, setStateFaq, stateFaq } = this.props
      const newState = (qs.parse(location.search) || {})
      this.props.fetchFaq(location.search)

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchFaq(location.search)
      }

      this.props.getDatas(
        { base: 'apiContent', url: '/faq-categories', method: 'get' },
        { headers: { 'x-api-key': config.api_key } },
      ).then(res => (
        setStateFaq({
          ...stateFaq,
          load: false,
          list: res.data,
          search: newState.question,
          filter: newState.category_id,
          page: newState.page,
        })
      ))
    },
  }),
)(FAQ)
