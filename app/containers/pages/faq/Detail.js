import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
} from 'recompose'
import { fetchDetailFaq, deleteFaq } from 'actions/Faq'
import DetailFAQ from 'components/pages/faq/Detail'
import history from 'utils/history'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailFaq,
  } = state.root.faq

  return {
    isFetching,
    currentUser,
    detailFaq,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailFaq: bindActionCreators(fetchDetailFaq, dispatch),
  deleteFaq: bindActionCreators(deleteFaq, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    handleDelete: props => () => {
      const { params } = props.match
      props.deleteFaq(params.id)
        .then(() => (
          Swal.fire('FAQ telah dihapus', '<span style="color:#2b57b7;font-weight:bold">Kembali ke List FAQ!</span>', 'success')
            .then(() => history.push('/faq-menu'))
        ))
        .catch(err => (
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error')
        ))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { match } = this.props
      this.props.fetchDetailFaq(match.params.id)

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchDetailFaq(match.params.id)
      }
    },
  }),
)(DetailFAQ)
