import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import {
  fetchDetailFaq, faqDetail,
  createFaq, updateFaq,
} from 'actions/Faq'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import FormFAQ from 'components/pages/faq/Form'
import history from 'utils/history'
import config from 'app/config'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailFaq,
  } = state.root.faq

  return {
    isFetching,
    currentUser,
    detailFaq,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailFaq: bindActionCreators(fetchDetailFaq, dispatch),
  faqDetail: bindActionCreators(faqDetail, dispatch),
  createFaq: bindActionCreators(createFaq, dispatch),
  updateFaq: bindActionCreators(updateFaq, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formFaq' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('categories', 'setCategories', {
      load: true,
      list: [],
      // selected: undefined,
    }),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          if (!err) {
            props[`${params.id ? 'upd' : 'cre'}ateFaq`](values, (params.id || null))
              .then(() => (
                Swal.fire(`FAQ telah di${params.id ? 'perbarui' : 'tambahkan'}`, '<span style="color:#2b57b7;font-weight:bold">Kembali ke list FAQ!</span>', 'success')
                  .then(() => history.push('/faq-menu'))
              ))
              .catch(error => (
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
              ))
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {

        const { match, setCategories } = this.props
        if (match.params.id) {
          this.props.fetchDetailFaq(match.params.id)
        } else {
          this.props.faqDetail({})
        }

        window.onresize = () => {
          this.props.fetchDetailFaq(match.params.id)
        }

        this.props.getDatas(
          { base: 'apiContent', url: '/faq-categories', method: 'get' },
          { headers: { 'x-api-key': config.api_key } },
        ).then(res => (
          setCategories({
            load: false,
            list: res.data,
            // selected: newState.page,
          })
        ))
      },
    }),
  )(FormFAQ),
)
