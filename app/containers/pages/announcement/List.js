import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import Announcement from 'components/pages/Announcement/List'
import { fetchAnnouncement, deleteAnnouncement } from 'actions/Announcement'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import config from 'app/config'
import qs from 'query-string'
import { message } from 'antd'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    dataAnnouncement,
    metaAnnouncement,
  } = state.root.announcement

  return {
    isFetching,
    currentUser,
    dataAnnouncement,
    metaAnnouncement,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchAnnouncement: bindActionCreators(fetchAnnouncement, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  deleteAnnouncement: bindActionCreators(deleteAnnouncement, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('announcementsList', 'setAnnouncementList', {
    load: true,
    list: [],
    modal: false,
  }),
  withState('readStatus', 'setReadStatus', {
    load: true,
    list: [],
  }),
  withState('stateAnnouncement', 'setStateAnnouncement', props => ({
    load: true,
    list: [],
    search: '',
    filter: undefined,
    page: props.metaAnnouncement.current_page,
  })),
  withHandlers({
    handleDelete: props => (id) => {
      props.deleteAnnouncement(id)
        .then(() => (
          Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Notification telah dihapus</span>', 'success')
            .then(() => history.push('/announcement'))
        ))
        .catch(err => (
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error')
        ))
    },
    detailNotif: props => (id) => {
      if (id) {
        const { list } = props.announcementsList
        props.setAnnouncementList({
          ...props.announcementsList,
          modal: true,
          data: (list || []).find(item => item.id === id),
        })
        props.getDatas(
          { base: 'apiUser', url: `/announcements/read-status/${id}`, method: 'put' },
        )
          .then(res => props.setReadStatus({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            props.setReadStatus({ load: false, list: [] })
          })
      } else {
        props.setAnnouncementList({
          ...props.announcementsList,
          modal: false,
          data: {},
        })
      }
    },
    loadAnnouncement: props => (isSearch) => {
      const { page } = props.stateAnnouncement
      const payload = {
        page: isSearch ? 1 : page,
      }

      history.push(`/announcement?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateAnnouncement, setStateAnnouncement, loadAnnouncement } = props
      setStateAnnouncement({
        ...stateAnnouncement,
        filter: val,
      })
      setTimeout(() => {
        loadAnnouncement(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateAnnouncement, setStateAnnouncement, loadAnnouncement } = props
      setStateAnnouncement({
        ...stateAnnouncement,
        page: value,
      })
      setTimeout(() => {
        loadAnnouncement()
      }, 300)
    },
    handleCategory: props => (value, category) => {
      props.fetchAnnouncement(value.Announcement_search, '', category)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { location, setStateAnnouncement, stateAnnouncement } = this.props
      const newState = (qs.parse(location.search) || {})
      this.props.fetchAnnouncement(location.search)

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchAnnouncement(location.search)
      }

      this.props.getDatas(
        { base: 'apiUser', url: '/announcements/list', method: 'get' },
        { headers: { 'x-api-key': config.api_key } },
      ).then(res => (
        setStateAnnouncement({
          ...stateAnnouncement,
          load: false,
          list: res.data,
          // search: newState.question,
          // filter: newState.category_id,
          page: newState.page,
        })
      ))
      this.props.getDatas(
        { base: 'apiUser', url: '/announcements/list', method: 'get' },
      )
        .then(res => this.props.setAnnouncementList({ ...this.props.announcementsList, load: false, list: res.data }))
        .catch((err) => {
          message.error(err)
          this.props.setAnnouncementList({ ...this.props.announcementsList, load: false, list: [] })
        })
    },
  }),
)(Announcement)
