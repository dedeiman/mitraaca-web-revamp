import { connect } from 'react-redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchContest } from 'actions/Contest'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import Swal from 'sweetalert2'
import DashboardView from 'components/pages/dashboard'

export function mapStateToProps(state) {
  const { group } = state.root.role
  const { currentUser } = state.root.auth

  const {
    dataContest,
    isFetching: loadContest,
  } = state.root.contest

  return {
    group,
    dataContest,
    loadContest,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchContest: bindActionCreators(fetchContest, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('announcements', 'setAnnouncement', {
    load: true,
    list: [],
    modal: false,
  }),
  withState('agentsToday', 'setAgent', {
    load: true,
    list: [],
  }),
  withState('birthdays', 'setBirthday', {
    load: true,
    list: [],
  }),
  withState('readStatus', 'setReadStatus', {
    load: true,
    list: [],
  }),
  withState('approvals', 'setApproval', {
    load: true,
    list: [],
  }),
  withState('policies', 'setPolicy', {
    load: true,
    list: [],
  }),
  withState('sppa', 'setSPPA', {
    load: true,
    list: [],
  }),
  withState('iOffer', 'setIO', {
    load: true,
    list: [],
  }),
  withState('announcementsCount', 'setAnnouncementCount', {
    load: true,
    list: [],
  }),
  withHandlers({
    detailNotif: props => (id) => {
      if (id) {
        const { list } = props.announcements
        props.setAnnouncement({
          ...props.announcements,
          modal: true,
          data: (list || []).find(item => item.id === id),
        })
        props.getDatas(
          { base: 'apiUser', url: `/announcements/read-status/${id}`, method: 'put' },
        )
          .then(res => props.setReadStatus({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            props.setReadStatus({ load: false, list: [] })
          })
      } else {
        props.setAnnouncement({
          ...props.announcements,
          modal: false,
          data: {},
        })
      }
    },
    greeting: props => (id) => {
      props.setBirthday({ ...props.birthdays, load: true })

      props.getDatas(
        { base: 'apiUser', url: `/agents/${id}/happy-birthday`, method: 'post' },
      )
        .then(() => {
          Swal.fire(
            '',
            '<span style="color:#2b57b7;font-weight:bold">Birthday wishes have been sent</span>',
            'success',
          ).then(() => {
            props.getDatas(
              { base: 'apiUser', url: '/agents?dob=today&sort_by=birthdate&order_by=ASC&purpose=birthday', method: 'get' },
            )
              .then(res => props.setBirthday({ load: false, list: res.data }))
              .catch((err) => {
                message.error(err)
                props.setBirthday({ load: false, list: [] })
              })
          })
        })
        .catch(err => message.error(err))
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'dashboard')
      this.props.fetchContest()

      window.onresize = () => {
        this.props.fetchContest()
      }

      this.props.getDatas(
        { base: 'apiUser', url: '/agents?dob=today&sort_by=birthdate&order_by=ASC&purpose=birthday', method: 'get' },
      )
        .then(res => this.props.setBirthday({ load: false, list: res.data }))
        .catch((err) => {
          message.error(err)
          this.props.setBirthday({ load: false, list: [] })
        })

      if (this.props.currentUser.permissions && this.props.currentUser.permissions.indexOf('dashboard-notification') > -1) {
        this.props.getDatas(
          { base: 'apiUser', url: '/announcements/list', method: 'get' },
        )
          .then(res => this.props.setAnnouncement({ ...this.props.announcements, load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setAnnouncement({ ...this.props.announcements, load: false, list: [] })
          })

        this.props.getDatas(
          { base: 'apiUser', url: '/announcements/unread-count', method: 'get' },
        )
          .then(res => this.props.setAnnouncementCount({ ...this.props.announcementsCount, load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setAnnouncementCount({ ...this.props.announcementsCount, load: false, list: [] })
          })
        this.props.getDatas(
          { base: 'apiUser', url: '/agents?join_date=today', method: 'get' },
        )
          .then(res => this.props.setAgent({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setAgent({ load: false, list: [] })
          })
      }

      if (this.props.currentUser.permissions && this.props.currentUser.permissions.indexOf('dashboard-approval-list') > -1) {
        this.props.getDatas(
          { base: 'apiUser', url: '/approvals?status=need-approval', method: 'get' },
        )
          .then(res => this.props.setApproval({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setApproval({ load: false, list: [] })
          })
      }

      if (this.props.currentUser.permissions && this.props.currentUser.permissions.indexOf('dashboard-indicative-offer-index') > -1) {
        this.props.getDatas(
          { base: 'apiUser', url: '/dashboard-io?page=1&limit=10', method: 'get' },
        )
          .then(res => this.props.setIO({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setIO({ load: false, list: [] })
          })
      }

      if (this.props.currentUser.permissions && this.props.currentUser.permissions.indexOf('dashboard-sppa-list') > -1) {
        this.props.getDatas(
          { base: 'apiUser', url: '/dashboard-insurance-letters?page=1&limit=10', method: 'get' },
        )
          .then(res => this.props.setSPPA({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setSPPA({ load: false, list: [] })
          })
      }

      if (this.props.currentUser.permissions && this.props.currentUser.permissions.indexOf('dashboard-expiry-policy') > -1) {
        this.props.getDatas(
          { base: 'apiUser', url: '/insurance-letters?status=expired', method: 'get' },
        )
          .then(res => this.props.setPolicy({ load: false, list: res.data }))
          .catch((err) => {
            message.error(err)
            this.props.setPolicy({ load: false, list: [] })
          })
      }
    },
  }),
)(DashboardView)
