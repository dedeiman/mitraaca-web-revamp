import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import DetailPolicy from 'components/pages/printPolicies/Detail'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  return {
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('dataPolicy', 'setDataPolicy', null),
  withHandlers({
    handlePrintPolicies: props => (id) => {
      props.getDatas({
        base: 'apiUser',
        url: '/print-pending-insurance-letters',
        method: 'post',
      },
      {
        sppa_id: id,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
     
      this.props.updateSiteConfiguration('activePage', 'pending-policy-list')
      this.props.updateSiteConfiguration('isActiveSidebar', false)

      const detailPolicy = JSON.parse(localStorage.getItem('dataDetailPolicy'))
      this.props.setDataPolicy(detailPolicy)
    },
  }),
)(DetailPolicy)
