import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import ListPolicy from 'components/pages/printPolicies/List'
import { getDatas } from 'actions/Option'
import { pickBy, identity } from 'lodash'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  const {
    isFetching,
    dataContest,
    metaContest,
  } = state.root.contest

  return {
    isFetching,
    dataContest,
    metaContest,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('statePrint', 'setStatePrint', {
    loading: true,
    searchSPPA: '',
    searchName: '',
    list: [],
    page: {},
  }),
  withState('stateFilter', 'setStateFilter', {
    loading: true,
    sppa_number: '',
    name: '',
    status_printed: '',
    page: 1,
  }),
  withHandlers({
    loadPolicy: props => (isSearch) => {
      const { filter, search, page } = props.stateFilter
      const payload = {
        status: filter,
        keyword: search,
        page: isSearch ? 1 : page,
      }

      history.push(`/pending-policy-list?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    getPrintPolicies: props => (status = '') => {
      const parsed = qs.parse(window.location.search)
      props.getDatas(
        { base: 'apiUser', url: `/print-pending-insurance-letters?sppa_number=${props.statePrint.searchSPPA}&name=${props.statePrint.searchName}&status_printed=${status}&page=${parsed.page ? parsed.page : 1}&per_page=10`, method: 'get' },
      ).then((res) => {
        props.setStatePrint({
          ...props.statePrint,
          loading: false,
          list: res.data,
          page: res.meta,
        })
      }).catch(() => {
        props.setStatePrint({
          ...props.statePrint,
          loading: false,
          list: [],
        })
      })
    },
    openDetailPage: props => (id) => {
      const detailPolicy = (props.statePrint.list || []).filter(policy => policy.id === id)
      localStorage.setItem('dataDetailPolicy', JSON.stringify(detailPolicy[0]))

      history.push(`/pending-policy-list/${id}/detail`)
    },
    handlePage: props => (value) => {
      const { stateFilter, setStateFilter, loadPolicy } = props
      setStateFilter({
        ...stateFilter,
        page: value,
      })
      setTimeout(() => {
        loadPolicy()
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
     
      this.props.updateSiteConfiguration('activePage', 'pending-policy-list')
      this.props.getPrintPolicies()
    },
  }),
)(ListPolicy)
