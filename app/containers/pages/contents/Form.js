/* eslint-disable prefer-destructuring */
/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import {
  materiDetail,
  createMateri, updateMateri,
} from 'actions/Materi'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import {
  pickBy, identity, capitalize,
} from 'lodash'
import FormMateri from 'components/pages/contents/Form'
import history from 'utils/history'
import Helper from 'utils/Helper'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.materi

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  materiDetail: bindActionCreators(materiDetail, dispatch),
  createMateri: bindActionCreators(createMateri, dispatch),
  updateMateri: bindActionCreators(updateMateri, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formMateri' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateFile', 'setStateFile', {
      icon: '',
      file: [],
      currentFiles: [],
      description: '',
      language: '',
      link_embed_video: '',
      subject: '',
      type_of_material: '',
    }),
    withState('languageOptions', 'setLanguageOptions', {
      load: true,
      list: [],
    }),
    withHandlers({
      handleUploadFiles: props => (info) => {
        const { stateFile, setStateFile } = props

        if (info.fileList.length > stateFile) {
          Helper.getBase64(info.file, file => (
            setStateFile({
              ...stateFile,
              file,
            })
          ))
        } else {
          setStateFile({
            ...stateFile,
            file: info.fileList,
          })
        }
      },

      handleUploadIcon: props => (info) => {
        const { stateFile, setStateFile } = props
        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'

        if (!isFileType) {
          message.error('You can only upload JPG/PNG file!')
          return props.form.setFieldsValue({ icon: undefined })
        }

        Helper.getBase64(info.file, file => (
          setStateFile({
            ...stateFile,
            icon: file,
          })
        ))
      },

      onSubmit: props => (event) => {
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          let files = []
          if (values.files.fileList !== undefined) {
            files = values.files.fileList
          } else {
            files = values.files
          }

          if (!err) {
            const formData = new FormData()
            Object.keys(pickBy(values, identity)).map((item) => {
              if ((item === 'icon') && values[item]) {
                return formData.append(item, values[item].file)
              }
              if ((item === 'files') && values[item]) {
                files.map((dataFile) => {
                  if (dataFile.originFileObj) {
                    formData.append('files[]', dataFile.originFileObj)
                  }
                })
              } else {
                return formData.append(item, values[item])
              }
            })

            if (params.id) {
              formData.append('is_delete_previous_icon', 'false')

              if (values.files) {
                const fieldFileIds = files.map(o => o.uid)
                const arrDeletedFileIds = props.stateFile.currentFiles.filter(o => !fieldFileIds.includes(o.uid)).map(o => o.uid)

                if (arrDeletedFileIds.length > 0) {
                  formData.append('deleted_file_ids', arrDeletedFileIds.join())
                }
              }
            }

            props[`${params.id ? 'upd' : 'cre'}ateMateri`](formData, (params.id || null))
              .then(() => (
                Swal.fire(`Materi telah di${params.id ? 'perbarui' : 'tambahkan'}`, '<span style="color:#2b57b7;font-weight:bold">Kembali ke list Materi</span>', 'success')
                  .then(() => history.push('/materi-menu'))
              ))
              .catch((error) => {
                if (error.errors && Object.keys(error.errors).length) {
                  let objError = {}
                  Object.keys(error.errors).forEach((item) => {
                    objError = {
                      ...objError,
                      [item]: {
                        validateStatus: 'error',
                        errors: [new Error(capitalize(error.errors[item] || ''))],
                      },
                    }

                    props.form.setFields(objError)
                  })
                }
              })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
       
        const { params } = this.props.match

        window.onresize = () => {
          this.props.createMateri(window.location.search)
        }

        if (params.id) {
          this.props.getDatas(
            { base: 'apiContent', url: `/materials/${params.id}`, method: 'get' },
          ).then((res) => {
            const files = []
            res.data.files.map((dataFile, idx) => {
              files.push({
                uid: dataFile.id,
                thumbUrl: dataFile.file_url,
                name: `${capitalize((dataFile.title || '').split('-')[0])} - File ${idx + 1}`,
              })
            })

            this.props.setStateFile({
              icon: res.data.icon_url,
              file: files,
              currentFiles: files,
              description: res.data.description,
              language: res.data.language,
              link_embed_video: res.data.link_embed_video,
              subject: res.data.subject,
              type_of_material: res.data.type_of_material,
            })
          })
        } else {
          this.props.materiDetail({})
        }

        this.props.getDatas(
          { base: 'apiUser', url: '/countries', method: 'get' },
        ).then(res => (
          this.props.setLanguageOptions({
            ...this.props.languageOptions,
            load: false,
            list: res.data,
          })
        ))
      },
      componentDidUpdate(prevProps) {
        const { detailMateri, stateFile, setStateFile } = this.props

        if (detailMateri !== prevProps.detailMateri) {
          const { icon, file } = detailMateri
          setStateFile({
            ...stateFile,
            icon,
            file: (
              file
                ? [{
                  uid: 1, name: file, status: 'done', url: file,
                }]
                : []
            ),
          })
        }
      },
    }),
  )(FormMateri),
)
