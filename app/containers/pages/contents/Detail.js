/* eslint-disable no-plusplus */
/* eslint-disable global-require */
import { saveAs } from 'file-saver'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import config from 'app/config'
import {
  compose,
  withHandlers,
  lifecycle,
} from 'recompose'
import { fetchDetailMateri, deleteMateri } from 'actions/Materi'
import DetailMateri from 'components/pages/contents/Detail'
import history from 'utils/history'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailMateri,
  } = state.root.materi

  return {
    isFetching,
    currentUser,
    detailMateri,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailMateri: bindActionCreators(fetchDetailMateri, dispatch),
  deleteMateri: bindActionCreators(deleteMateri, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    handleDelete: props => () => {
      const { params } = props.match
      props.deleteMateri(params.id)
        .then(() => (
          Swal.fire('Materi telah dihapus', '<span style="color:#2b57b7;font-weight:bold">Kembali ke List Materi</span>', 'success')
            .then(() => history.push('/materi-menu'))
        ))
        .catch(err => (
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error')
        ))
    },

    handleDownloadMultiple: props => (event, files) => {
      const { params } = props.match
      event.preventDefault()
      window.open(`${config.api_url_content}/materials/${params.id}/download-files`)

      // const zip = require('jszip')()
      // const JSZipUtils = require('jszip-utils')
      // const urls = files.map(file => file.file_url)
      // const zipFilename = 'zipFilename.zip'
      // let count = 0

      // urls.forEach((url) => {
      //   window.open(url)
      // })

      // urls.forEach((url) => {
      //   const filename = 'filename'
      //   // loading a file and add it in a zip file
      //   JSZipUtils.getBinaryContent(url, (err, data) => {
      //     if (err) {
      //       throw err
      //     }
      //     zip.file(filename, data, { binary: true })

      //     count++
      //     if (count === urls.length) {
      //       zip.generateAsync({ type: 'blob' }).then((content) => {
      //         saveAs(content, zipFilename)
      //       })
      //     }
      //   })
      // })
    },
  }),
  lifecycle({
    componentDidMount() {
     
      const { match } = this.props
      this.props.fetchDetailMateri(match.params.id)

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchDetailMateri(window.location.search)
      }
    },
  }),
)(DetailMateri)
