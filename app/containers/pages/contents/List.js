import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import ListMateri from 'components/pages/contents/List'
import { fetchMateri } from 'actions/Materi'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth
  const {
    group: groupRole,
  } = state.root.role

  const {
    dataFaq,
    metaFaq,
  } = state.root.faq

  const {
    isFetching,
    dataMateri,
    metaMateri,
  } = state.root.materi

  return {
    isFetching,
    currentUser,
    dataFaq,
    metaFaq,
    dataMateri,
    metaMateri,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchMateri: bindActionCreators(fetchMateri, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateMateri', 'setStateMateri', props => ({
    load: true,
    search: '',
    type: undefined,
    page: props.metaMateri.current_page,
  })),
  withState('meterialType', 'setMeterialType', {
    isLoad: false,
    list: [],
  }),
  withHandlers({
    loadMateri: props => (isSearch) => {
      const {
        search, page, type,
      } = props.stateMateri
      const payload = {
        keyword: search,
        per_page: 12,
        type_of_material: type,
        page: isSearch ? 1 : page,
      }

      history.push(`/materi-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateMateri, setStateMateri, loadMateri } = props
      setStateMateri({
        ...stateMateri,
        filter: val,
      })
      setTimeout(() => {
        loadMateri(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateMateri, setStateMateri, loadMateri } = props
      setStateMateri({
        ...stateMateri,
        page: value,
      })
      setTimeout(() => {
        loadMateri()
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        location, setStateMateri, stateMateri, setMeterialType, meterialType,
      } = this.props
      const newState = (qs.parse(location.search) || {})
      this.props.fetchMateri(location.search || '?per_page=12')

      // Fetching component if screen device has change
      window.onresize = () => {
        this.props.fetchMateri(location.search || '?per_page=12')
      }

      setMeterialType({ ...meterialType, isLoad: true })
      this.props.getDatas(
        { base: 'apiContent', url: '/materials-type', method: 'get' },
      ).then((res) => {
        setMeterialType({
          ...meterialType,
          isLoad: false,
          list: res.data,
        })
      }).catch(() => {
        setMeterialType({
          ...meterialType,
          isLoad: false,
          list: [],
        })
      })

      setStateMateri({
        ...stateMateri,
        load: false,
        search: newState.keyword,
        type: newState.type_of_material,
        page: newState.page,
      })
    },
  }),
)(ListMateri)
