import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { getDatas } from 'actions/Option'
import CareerPath from 'components/pages/profiles/Career'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateChild', 'setStateChild', []),
  withState('stateCareer', 'setStateCareer', {
    load: true,
    list: [],
    isTree: true,
  }),
  withHandlers({
    openChild: props => (data, idx) => {
      const { stateChild, setStateChild } = props

      if (idx > 1) return
      if (idx === 0) {
        stateChild.pop()
      }

      stateChild[idx] = data
      setStateChild(stateChild)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { stateCareer, setStateCareer } = this.props
      this.props.getDatas(
        { base: 'apiUser', url: '/user/profile/partners', method: 'get' },
        null,
      ).then((res) => {
        setStateCareer({
          ...stateCareer,
          load: false,
          list: res.data,
        })
      })
    },
  }),
)(CareerPath)
