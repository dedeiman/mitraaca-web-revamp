import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, lifecycle, withState } from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import { AGENT_CODE } from 'constants/ActionTypes'
import PasswordChange from 'components/pages/profiles/Password'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  return {
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('isSecondary', 'toggleSecondary', false),
  lifecycle({
    componentDidMount() {
      const { code } = this.props.groupRole
      if (code === AGENT_CODE) {
        this.props.updateSiteConfiguration('activeSubPage', 'profile')
        this.props.updateSiteConfiguration('activePage', 'profile/change')
      } else {
        this.props.updateSiteConfiguration('activePage', 'profile/change')
      }
    },
  }),
)(PasswordChange)
