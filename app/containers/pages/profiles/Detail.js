import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { identity, pickBy } from 'lodash'
import qs from 'query-string'
import history from 'utils/history'
import { fetchDetailUser, getDetailUser } from 'actions/User'
import DetailProfile from 'components/pages/profiles/Detail'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailProfile,
  } = state.root.user
  const { currentUser } = state.root.auth

  return {
    isFetching,
    detailProfile,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailUser: bindActionCreators(fetchDetailUser, dispatch),
  getDetailUser: bindActionCreators(getDetailUser, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('profilePic', 'setProfilePic', props => (
    props.currentUser ? props.currentUser.profile_pic : '/assets/avatar-on-error.jpg'
  )),
  withState('stateButton', 'setStateButton', {
    list: ['Product', 'Contract', 'License', 'Bank', 'Level', 'Taxation', 'Document', 'Training', 'Contest'],
    active: 'Product',
    products: [],
    banks: [],
    contracts: [],
    licenses: [],
    level: [],
    taxation: [],
    documents: [],
    'training-histories': [],
    'contest-histories': [],
    isFetching: false,
    page: 1,
    meta_page: {
      per_page: 1,
      page: 1,
      total_count: 1
    }
  }),
  withHandlers({
    loadHistory: props => (isSearch) => {
      const {
        page, perPage,
      } = props.stateButton
      const payload = {
        page: isSearch ? '' : page,
        per_page: isSearch ? '' : perPage,
      }
      history.push(`/profile/detail?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleDetail: props => (type) => {
      const { stateButton, setStateButton } = props
      let url = `${type.toLowerCase()}s`
      if (type === 'Taxation') url = 'taxation'
      if (type === 'Level') url = 'levels'
      if (type === 'Bank') url = 'banks'
      if (type === 'Contest') url = 'contest-histories'
      if (type === 'Training') url = 'training-histories'
      
      setStateButton({
        ...stateButton,
        isFetching: true,
      })
      props.getDetailUser(url).then((res) => {
        setStateButton({
          ...stateButton,
          active: type,
          isFetching: false,
          [url]: res.data,
          meta_page: {
            per_page: res.meta.per_page,
            page: res.meta.current_page,
            total_count: res.meta.total_count,
          }
        })
      }).catch(() => {
        setStateButton({
          ...stateButton,
          active: type,
          isFetching: false,
          [url]: [],
          meta_page: {
            per_page: 1,
            page: 1,
            total_count: 1
          }
        })
      })
    },
    handlePage: props => (value) => {
      const { stateButton, setStateButton, loadHistory } = props
      let url = `${stateButton.active.toLowerCase()}s`
      if (stateButton.active === 'Taxation') url = 'taxation'
      if (stateButton.active === 'Level') url = 'levels'
      if (stateButton.active === 'Bank') url = 'banks'
      if (stateButton.active === 'Contest') url = 'contest-histories'
      if (stateButton.active === 'Training') url = 'training-histories'

      const payload = {
        page: value,
      }

      setStateButton({
        ...stateButton,
        isFetching: true,
      })
      props.getDetailUser(url+ '?' +qs.stringify(pickBy(payload, identity))).then((res) => {
        setStateButton({
          ...stateButton,
          active: stateButton.active,
          isFetching: false,
          [url]: res.data,
          meta_page: {
            per_page: res.meta.per_page,
            page: res.meta.current_page,
            total_count: res.meta.total_count,
          }
        })
      }).catch(() => {
        setStateButton({
          ...stateButton,
          active: stateButton.active,
          isFetching: false,
          [url]: [],
          meta_page: {
            per_page: 1,
            page: 1,
            total_count: 1
          }
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        setStateButton, stateButton,
      } = this.props
      const newProfile = (qs.parse(history.location.search) || {})

      setStateButton({
        ...stateButton,
        isFetching: false,
        search: newProfile.keyword,
        page: newProfile.page,
        meta_page: {
          ...stateButton.meta_page,
          page: newProfile.page,
        }
      })

      this.props.fetchDetailUser()
      this.props.handleDetail('Product')
    },
  }),
)(DetailProfile)
