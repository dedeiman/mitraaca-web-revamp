import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CryptoJS from 'crypto-js'
import {
  compose, withState, withHandlers,
  lifecycle,
} from 'recompose'
import { resetPassword, verifyPassword } from 'actions/Auth'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { isEmpty } from 'lodash'
import Swal from 'sweetalert2'
import PrimaryPasswordChange from 'components/pages/profiles/PrimaryPassword'

const mapDispatchToProps = dispatch => ({
  resetPassword: bindActionCreators(resetPassword, dispatch),
  verifyPassword: bindActionCreators(verifyPassword, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'changePrimaryPassword' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('isLoading', 'setLoading', false),
    withState('stateError', 'setStateError', {}),
    withHandlers({
      onVerify: props => async (value) => {
        const { stateError, setStateError } = props
        setStateError({
          ...stateError,
          old_password: undefined,
        })
        await props.verifyPassword({ password: value }).then(() => {
          setStateError({
            ...stateError,
            old_password: undefined,
          })
        })
          .catch(() => {
            setStateError({ ...stateError, old_password: { validateStatus: 'error', help: "Password is doesn't match" } })
          })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.setLoading(true)
        props.form.validateFields((err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }

          if (!err && isEmpty(props.stateError.old_password)) {
            props.resetPassword({ ...values, change_password: true }, true)
              .catch((error) => {
                props.setStateError({
                  ...props.stateError,
                  message: error,
                })
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
                  .then(() => props.setLoading(false))
              })
          } else {
            props.setLoading(false)
          }

          props.setLoading(false)
        })
      },
      decryptWithAES: () => (aes, key) => {
        const decrypted = CryptoJS.AES.decrypt(aes, key)

        return decrypted
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.getDatas(
          { base: 'apiUser', url: '/passwords/aes', method: 'get' },
        ).then((res) => {
          // this.props.form.setFieldsValue({
          //   old_password: res.data.aes_password,
          //   old_alternative_password: res.data.aes_alternative_password,
          // })
        })
      },
    }),
  )(PrimaryPasswordChange),
)
