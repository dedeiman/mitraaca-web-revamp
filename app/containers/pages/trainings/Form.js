import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers,
  lifecycle, withState,
} from 'recompose'
import {
  fetchDetailTraining, trainingDetail,
  createTraining, updateTraining,
} from 'actions/Training'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import FormTraining from 'components/pages/trainings/Form'
import history from 'utils/history'
import Swal from 'sweetalert2'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailTraining,
  } = state.root.training

  return {
    isFetching,
    detailTraining,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailTraining: bindActionCreators(fetchDetailTraining, dispatch),
  trainingDetail: bindActionCreators(trainingDetail, dispatch),
  createTraining: bindActionCreators(createTraining, dispatch),
  updateTraining: bindActionCreators(updateTraining, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formTraining' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateSelects', 'setStateSelects', {
      subjectLoad: true,
      subjectTypeLoad: true,
      branchLoad: true,
      levelLoad: true,
    }),
    withState('isLoadSubmit', 'setLoadSubmit', false),
    withHandlers({
      loadLocation: props => (url, name) => {
        props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateSelects({
            ...props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
    withHandlers({
      handleLocation: props => (url, key) => {
        props.loadLocation(url, key)
      },
      handleSubject: props => () => {
        props.getDatas(
          { base: 'apiUser', url: '/training-subjects/list', method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            subjectLoad: false,
            subjectList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateSelects({
            ...this.props.stateSelects,
            subjectLoad: false,
            subjectList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          props.setLoadSubmit(true)
          if (!err) {
            const payload = {
              ...values,
              location: values.location ? (values.location).toUpperCase() : '',
              location_url: values.location_url ? (values.location_url) : '',
              description: values.description ? (values.description).toUpperCase() : '',
              date: values.date ? moment(values.date).format('YYYY-MM-DD') : '',
              start_time: isEmpty(values.time) ? '' : moment(values.time[0]).format('HH:mm'),
              end_time: isEmpty(values.time) ? '' : moment(values.time[1]).format('HH:mm'),
              quota: Number(values.quota),
            }
            delete payload.time
            props[`${params.id ? 'upd' : 'cre'}ateTraining`](payload, (params.id || null))
              .then(() => {
                props.setLoadSubmit(false)
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">Training Class telah di${params.id ? 'perbarui' : 'tambahkan'}</span>`, 'success')
                  .then(() => history.push('/training-class-menu'))
              })
              .catch((error) => {
                props.setLoadSubmit(false)
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
              })
          } else {
            props.setLoadSubmit(false)
          }
        })
      },
    }),
    lifecycle({
      componentWillMount() {
        const { params } = this.props.match
        const config = [
          { url: '/training-subjects/types', name: 'subjectType' },
          { url: '/training-subjects/list', name: 'subject' },
          { url: '/branches', name: 'branch' },
          { url: '/levels', name: 'level' },
          { url: '/trainers', name: 'trainer' },
        ]

        if (params.id) {
          this.props.fetchDetailTraining(params.id)
        } else {
          this.props.trainingDetail({})
        }

        config.map(item => (
          this.loadMasterData(item.url, item.name)
        ))
      },
      componentDidMount() {
        const {
          location, location_url,
        } = this.props

        window.google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line

        window.onresize = () => {
          this.props.fetchDetailTraining(location.search, location_url.search)
        }
      },
      componentDidUpdate(nextProps) {
        const { detailTraining } = this.props
        if (detailTraining !== nextProps.detailTraining) {
          const config = []
          if (detailTraining.province_id) config.push({ url: `/cities/${detailTraining.province_id}`, name: 'city' })
          if (detailTraining.city_id) config.push({ url: `/sub-district/${detailTraining.city_id}`, name: 'subDistrict' })
          if (detailTraining.sub_district_id) config.push({ url: `/villages/${detailTraining.sub_district_id}`, name: 'villages' })

          config.map(item => this.loadMasterData(item.url, item.name))
        }
      },
      loadMasterData(url, name) {
        // const { setStateSelects, stateSelects } = this.props
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateSelects({
            ...this.props.stateSelects,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
      initAutocomplete() {
        const inputNode = document.getElementById('formTraining_location')
        const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
        /* eslint-disable */
        window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
          if (event.key === 'Enter') {
            event.preventDefault()
          }
        })
        /* eslint-enable */
        autoComplete.addListener('place_changed', () => {
          const { setFieldsValue } = this.props.form
          const place = autoComplete.getPlace()
          const location = JSON.stringify(place.geometry.location)
          const data = JSON.parse(location)
          data.address = `${place.name}, ${place.formatted_address}`
          data.url = place.url

          setFieldsValue({ location: (data.address).toUpperCase() })
          setFieldsValue({ location_url: data.url })
        })
      },
    }),
  )(FormTraining),
)
