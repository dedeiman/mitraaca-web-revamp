import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import Swal from 'sweetalert2'
import moment from 'moment'
import CancelClass from 'components/pages/trainings/CancelClass'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formCancel' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('cancellationTypes', 'setCancellationTypes', {}),
    withState('stateSelect', 'setStateSelect', {}),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            const { id } = props.data
            const payload = {
              ...values,
              cancellation_description: values.cancellation_description ? (values.cancellation_description).toUpperCase() : '',
              location: values.location ? (values.location).toUpperCase() : '',
              location_url: values.location_url ? (values.location_url) : '',
              date: values.date ? moment(values.date).format('YYYY-MM-DD') : '',
              start_time: values.time ? moment(values.time[0]).format('HH:mm') : '',
              end_time: values.time ? moment(values.time[1]).format('HH:mm') : '',
            }
            delete payload.time

            props.getDatas(
              { base: 'apiUser', url: `/training_classes/${id}/cancel`, method: 'put' },
              payload,
            ).then(() => {
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">Training has been ${values.is_class_reschedule ? 'reschedul' : 'cancel'}ed</span>`, 'success')
                .then(() => {
                  props.reload()
                  props.toggle()
                })
            }).catch((error) => {
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`, 'error')
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        [
          { url: '/branches', name: 'branch' },
          { url: '/training-cancellation-reasons', name: 'cancel' },
        ].map(item => this.loadMasterData(item.url, item.name))

        window.google.maps.event.addDomListener(window, 'load', this.initAutocomplete()) // eslint-disable-line
      },
      initAutocomplete() {
        const inputNode = document.getElementById('formCancel_location')
        const autoComplete = new window.google.maps.places.Autocomplete(inputNode)
        /* eslint-disable */
        window.google.maps.event.addDomListener(inputNode, 'keydown', () => {
          if (event.key === 'Enter') {
            event.preventDefault()
          }
        })
        /* eslint-enable */
        autoComplete.addListener('place_changed', () => {
          const { setFieldsValue } = this.props.form
          const place = autoComplete.getPlace()
          const location = JSON.stringify(place.geometry.location)
          const data = JSON.parse(location)
          data.address = place.name+', '+place.formatted_address
          data.url = place.url

          setFieldsValue({ location: (data.address).toUpperCase() })
          setFieldsValue({ location_url: data.url })
        })
      },
      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
  )(CancelClass),
)
