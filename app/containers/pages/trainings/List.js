import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity, isEmpty } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import ListTraining from 'components/pages/trainings/List'
import { fetchTraining } from 'actions/Training'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import Swal from 'sweetalert2'
import moment from 'moment'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    dataTraining,
    metaTraining,
  } = state.root.training

  return {
    isFetching,
    dataTraining,
    metaTraining,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchTraining: bindActionCreators(fetchTraining, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateModal', 'setStateModal', {
    visibleJoin: false,
    visibleCancel: false,
    dataJoin: {},
    dataCancel: {},
  }),
  withState('stateSelects', 'setStateSelects', {}),
  withState('stateTraining', 'setStateTraining', props => ({
    load: true,
    list: [],
    search: '',
    filter: '',
    page: props.metaTraining.current_page,
    perPage: props.metaTraining.per_page,
  })),
  withHandlers({
    loadTraining: props => (isSearch) => {
      const {
        status, level, trainer,
        subject, location,
        time, date, page,
        viewBy,
      } = props.stateTraining
      const payload = {
        status,
        agent_level_ids: (level || []).join(','),
        trainer_id: trainer || '',
        subject_id: subject || '',
        view_by: viewBy || '',
        location,
        time: time ? moment(time).format('HH:mm') : '',
        start_date: !isEmpty(date) ? moment(date[0]).format('YYYY-MM-DD') : '',
        end_date: !isEmpty(date) ? moment(date[1]).format('YYYY-MM-DD') : '',
        page: isSearch ? '' : page,
      }

      history.push(`/training-class-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateTraining, setStateTraining, loadTraining } = props
      setStateTraining({
        ...stateTraining,
        filter: val,
      })

      setTimeout(() => {
        loadTraining(true)
      }, 300)
    },
    handlePage: props => (value) => {
      const { stateTraining, setStateTraining, loadTraining } = props
      setStateTraining({
        ...stateTraining,
        page: value,
      })
      setTimeout(() => {
        loadTraining()
      }, 300)
    },
    openClass: props => (id) => {
      props.getDatas({ base: 'apiUser', url: `/training_classes/${id}/open`, method: 'put' })
        .then(() => history.push(`/training-class-menu/${id}/open`))
        .catch((err) => {
          Swal.fire({
            title: `<span style="color:#2b57b7;font-weight:bold">${err.message}</span>`,
            icon: 'error',
          })
        })
    },
    updatePerPage: props => (current, value) => {
      const { stateTraining, setStateTraining } = props
      setStateTraining({
        ...stateTraining,
        perPage: value,
      })
      setTimeout(() => {
        const payload = {
          per_page: value,
          customer_status: stateTraining.search ? stateTraining.filter : '',
          search: stateTraining.search,
        }
        props.fetchTraining(`?${qs.stringify(pickBy(payload, identity))}`)
      }, 300)
    },
    unjoinClass: props => (param) => {
      Swal.fire({
        title: '<span style="color:#2b57b7;font-weight:bold">Are you sure to unjoin this class?</span>',
        icon: 'warning',
        showCancelButton: true,
      }).then((res) => {
        if (res.value) {
          return (
            props.getDatas(
              { base: 'apiUser', url: `/training_classes/${param.id}/unjoin`, method: 'post' },
            ).then(() => {
              props.loadTraining()
              message.success('Success Unjoin Class')
            }).catch((err) => {
              message.success(err.message)
            })
          )
        }
        return false
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const { location, setStateTraining, stateTraining } = this.props
      const newState = (qs.parse(location.search) || {})
      window.onresize = () => {
        this.props.fetchTraining(location.search)
      }

      const config = [
        { url: '/training-subjects', name: 'subject' },
        { url: '/levels', name: 'level' },
        { url: '/trainers', name: 'trainer' },
        { url: '/training-status', name: 'status' },
      ]
      const payload = { ...newState }
      delete payload.time

      this.props.fetchTraining(`?${qs.stringify(pickBy(payload, identity))}&start_time=${newState.time || ''}`)

      const date = []
      if (!isEmpty(newState.start_date)) {
        date.push(moment(newState.start_date))
      }
      if (!isEmpty(newState.end_date)) {
        date.push(moment(newState.end_date))
      }

      setStateTraining({
        ...stateTraining,
        load: false,
        search: newState.search,
        filter: newState.customer_status || '',
        status: newState.status,
        level: newState.agent_level_ids ? (newState.agent_level_ids || '').split(',').map(Number) : undefined,
        subject: newState.subject_id ? Number(newState.subject_id) : undefined,
        trainer: newState.trainer_id || undefined,
        location: newState.location || '',
        viewBy: newState.view_by || undefined,
        time: newState.time ? moment(newState.time, 'HH:mm') : '',
        date,
      })

      config.map(item => (
        this.loadMasterData(item.url, item.name)
      ))
    },
    componentDidUpdate(prevProps) {
      const { metaTraining } = this.props
      if (metaTraining !== prevProps.metaTraining) {
        this.props.setStateTraining({
          ...this.props.stateTraining,
          page: metaTraining.current_page,
        })
      }
    },
    loadMasterData(url, name) {
      console.log(this.props.setStateSelects)
      // const { setStateSelects, stateSelects } = this.props
      this.props.getDatas(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.setStateSelects({
          ...this.props.stateSelects,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateSelects({
          ...this.props.stateSelects,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(ListTraining)
