import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import AddParticipant from 'components/pages/trainings/AddParticipant'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'formAddParticipant' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('cancellationTypes', 'setCancellationTypes', {}),
    withState('stateSelect', 'setStateSelect', {}),
    withHandlers({
      handleSearch: props => (val) => {
        if (val.length < 3) {
          return
        }
        props.setStateSelect({ ...props.stateSelect, agentLoad: true })
        props.getDatas(
          { base: 'apiUser', url: `/search-agents?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch(() => {
          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { idTraining, form } = props
        form.validateFields((err, values) => {
          if (!err) {
            props.getDatas(
              { base: 'apiUser', url: `/training_classes/${idTraining}/participants`, method: 'post' },
              { participant_id: values.id },
            ).then(() => {
              props.toggle(true)
            }).catch((error) => {
              message.error(error.message)
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        [
          { url: '/branches', name: 'branch' },
          { url: '/cancellation-types', name: 'cancel' },
        ].map(item => this.loadMasterData(item.url, item.name))
      },
      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
  )(AddParticipant),
)
