import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, withHandlers } from 'recompose'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import JoinClass from 'components/pages/trainings/JoinClass'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withHandlers({
    handleJoin: props => (id) => {
      props.getDatas(
        { base: 'apiUser', url: `/training_classes/${id}/join`, method: 'post' },
      ).then(() => {
        Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Berhasil Join Class</span>', 'success')
          .then(() => props.toggle(true))
      }).catch((err) => {
        if (err.message === 'Have reached the quota limit') {
          Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Kuota pada Training Class saat ini sudah penuh</span>', 'error')
            .then(() => props.toggle(true))
        } else {
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err.message}</span>`, 'error')
            .then(() => props.toggle(true))
        }
      })
    },
  }),
)(JoinClass)
