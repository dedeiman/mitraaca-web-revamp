import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import { fetchDetailTraining } from 'actions/Training'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import DetailTraining from 'components/pages/trainings/Detail'
import history from 'utils/history'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailTraining,
  } = state.root.training

  return {
    isFetching,
    detailTraining,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailTraining: bindActionCreators(fetchDetailTraining, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateParticipants', 'setStateParticipants', []),
  withState('dataUpdate', 'setdataUpdate', {
    list: [],
    load: false,
  }),
  withState('defaultCheck', 'setDefaultCheck', []),
  withState('stateModal', 'setStateModal', {
    visible: false,
  }),
  withState('checkAll', 'setCheckAll', true),
  withHandlers({
    updateParticipant: props => () => {
      const { id } = props.match.params
      const payload = (props.dataUpdate.list || []).map(item => ({ participant_id: item, is_present: true }))
      props.getDatas(
        { base: 'apiUser', url: `/training_classes/${id}/participants`, method: 'put' },
        { participants: payload },
      ).then(() => {
        Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Participant is updated</span>', 'success')
          .then(() => history.push('/training-class-menu'))
      }).catch(err => message.error(err))
    },
    onCheckAllChange: props => (e) => {
      props.setCheckAll(e.target.checked)
      props.setdataUpdate({
        ...props.dataUpdate,
        list: e.target.checked ? props.defaultCheck : [],
      })
    },
    handleChange: props => (val) => {
      props.setCheckAll(val.length === props.defaultCheck.length)
      props.setdataUpdate({ ...props.dataUpdate, list: val })
    },
    getParticipants: props => () => {
      props.setdataUpdate({ ...props.dataUpdate, load: true })
      props.getDatas(
        { base: 'apiUser', url: `/training_classes/${props.match.params.id}/participants`, method: 'get' },
      ).then((res) => {
        const newData = (res.data || []).map(item => ({
          id: item.participant_id,
          is_present: item.is_present,
          name: item.agent ? item.agent.name : '',
          agent_id: item.agent ? item.agent.agent_id : '',
          branch: item.agent ? item.agent.branch : {},
        }))
        props.setStateParticipants(newData)
        props.setDefaultCheck(res.data.map(item => item.participant_id))
        props.setdataUpdate({
          load: false,
          list: res.data.map(item => item.participant_id),
        })
      }).catch(err => message.error(err))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { match, getParticipants } = this.props
      this.props.fetchDetailTraining(match.params.id)
      getParticipants()
    },
  }),
)(DetailTraining)
