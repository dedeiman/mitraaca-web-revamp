import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  withState,
} from 'recompose'
import { uploadTraining } from 'actions/Training'
import history from 'utils/history'
import Swal from 'sweetalert2'
import UploadTraining from 'components/pages/trainings/Upload'

export function mapStateToProps(state) {
  const {
    isFetching,
  } = state.root.training

  return {
    isFetching,
  }
}

const mapDispatchToProps = dispatch => ({
  uploadTraining: bindActionCreators(uploadTraining, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateTraining', 'setStateTraining', {
    file: '',
    fileList: [],
    isFetching: false,
  }),
  withHandlers({
    handleUpload: props => () => {
      const { stateTraining } = props
      const dataTraining = stateTraining.fileList.map(list => list.originFileObj)

      const formData = new FormData()
      formData.append('file', ...dataTraining)

      props.setStateTraining({
        ...stateTraining,
        isFetching: true,
      })

      props.uploadTraining(formData)
        .then(() => {
          props.setStateTraining({
            ...stateTraining,
            isFetching: false,
          })
          Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Document has been uploaded</span>', 'success')
            .then(() => history.push('/training-class-menu'))
        })
        .catch((err) => {
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error')
          props.setStateTraining({
            ...stateTraining,
            isFetching: false,
          })
        })
    },
  }),
)(UploadTraining)
