import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { PermissionButton } from 'utils/Permission'
import {
  compose, withState, withHandlers, lifecycle,
} from 'recompose'
import { resetPassword, verifyPassword, checkSecondPassword } from 'actions/Auth'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import { message } from 'antd'
import Swal from 'sweetalert2'
import SecondaryPasswordCheck from 'components/pages/secondaryPassword/index'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  const {
    group: groupRole,
  } = state.root.role

  return {
    currentUser,
    groupRole,
  }
}
const mapDispatchToProps = dispatch => ({
  resetPassword: bindActionCreators(resetPassword, dispatch),
  verifyPassword: bindActionCreators(verifyPassword, dispatch),
  checkSecondPassword: bindActionCreators(checkSecondPassword, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'changeSecondaryPassword' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('isLoading', 'setLoading', false),
    withState('fetchForgot', 'setFetching', false),
    withState('stateError', 'setStateError', {}),
    withState('isVisible', 'toggleVisible', false),
    withHandlers({
      sendEmail: props => () => {
        const payload = {
          identifier: PermissionButton('forgotSecondPass', props.groupRole.code) ? props.currentUser.nik : props.currentUser.email,
          is_secondary: true,
          key: PermissionButton('forgotSecondPass', props.groupRole.code) ? 'nik' : 'email',
        }
        props.setFetching(true)

        setTimeout(() => {
          props.getDatas(
            { base: 'apiUser', url: '/passwords/reset', method: 'post' },
            payload,
          ).then(() => {
            props.setFetching(false)
            let roleMessage = ''
            if ((props.currentUser.role.name !== 'kepala-teknik') && (props.currentUser.role.name !== 'head-branch') && (props.currentUser.role.name !== 'head-agency')) {
              roleMessage = 'Silakan konfirmasi ke atasan Anda'
            } else {
              roleMessage = 'Silakan cek email Anda'
            }

            // Swal.fire('Link Forgot second password telah dikirim', roleMessage, 'success')
            Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Link Forgot second password telah dikirim</span>', 'success')
              .then(() => {
                props.toggleVisible(false)
                // removeToken()
                // Browser.setWindowHref('/')
              })
          }).catch((err) => {
            props.setFetching(false)
            message.error(err.message)
          })
        }, 300)
      },
      onVerify: props => async (value) => {
        const { stateError, setStateError } = props

        setStateError({
          ...stateError,
          old_alternative_password: undefined,
        })

        await props.verifyPassword({ alternative_password: value }).then(() => {
          setStateError({
            ...stateError,
            old_alternative_password: undefined,
          })
        })
          .catch(() => {
            setStateError({
              ...stateError,
              old_alternative_password: { validateStatus: 'error', help: "Password is doesn't match" },
            })
          })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.setLoading(true)
        props.form.validateFields((err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }

          if (!err) {
            props.checkSecondPassword({ ...values }, window.location.pathname)
              .catch((error) => {
                props.setStateError({
                  ...props.stateError,
                  message: error,
                })
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
                  .then(() => props.setLoading(false))
              })
          } else {
            props.setLoading(false)
          }
          props.setLoading(false)
        })
      },
    }),
    lifecycle({
      componentDidMount() {
      },
    }),
  )(SecondaryPasswordCheck),
)
