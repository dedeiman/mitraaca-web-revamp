import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import ListCustomer from 'components/pages/customers/List'
import { fetchCustomer } from 'actions/Customer'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  const {
    isFetching,
    dataCustomer,
    metaCustomer,
  } = state.root.customer

  return {
    isFetching,
    dataCustomer,
    metaCustomer,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchCustomer: bindActionCreators(fetchCustomer, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateCustomer', 'setStateCustomer', props => ({
    load: true,
    list: [],
    search: '',
    filter: '',
    page: props.metaCustomer.current_page,
    perPage: props.metaCustomer.per_page,
  })),
  withHandlers({
    loadCustomer: props => (isSearch) => {
      const { filter, search, page } = props.stateCustomer
      const payload = {
        keyword: search || '',
        per_page: 12,
        search_by: filter,
        page: isSearch ? 1 : page,
      }

      history.push(`/customer-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateCustomer, setStateCustomer } = props
      setStateCustomer({
        ...stateCustomer,
        filter: val,
      })
    },
    handlePage: props => (value) => {
      const { stateCustomer, setStateCustomer, loadCustomer } = props
      setStateCustomer({
        ...stateCustomer,
        page: value,
      })
      setTimeout(() => {
        loadCustomer()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { stateCustomer, setStateCustomer } = props
      setStateCustomer({
        ...stateCustomer,
        perPage: value,
      })
      setTimeout(() => {
        const payload = {
          per_page: value,
          customer_status: stateCustomer.search ? stateCustomer.filter : '',
          search: stateCustomer.search,
        }
        props.fetchCustomer(`?${qs.stringify(pickBy(payload, identity))}`)
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { location, setStateCustomer, stateCustomer } = this.props
      const newState = (qs.parse(location.search) || {})

      this.props.fetchCustomer(location.search)

      setStateCustomer({
        ...stateCustomer,
        load: false,
        search: newState.keyword,
        filter: newState.search_by || '',
        page: newState.page,
      })
    },
  }),
)(ListCustomer)
