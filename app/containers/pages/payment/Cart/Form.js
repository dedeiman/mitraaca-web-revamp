import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import qs from 'query-string'
import history from 'utils/history'
import { getDatas } from 'actions/Option'
import { listCartData } from 'actions/Payment/Cart'

import Form from 'components/pages/payment/Cart/Form'
import { message } from 'antd'


export function mapStateToProps() {
  return {}
}

const mapDispatchToProps = dispatch => ({
  getData: bindActionCreators(getDatas, dispatch),
  fetchSearch: bindActionCreators(listCartData, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    cob: '',
    product: '',
    number_sppa: '',
    policy_holder: '',
    insured: '',
    status: '',
  }),
  withHandlers({
    handleSearch: props => (e) => {
      e.preventDefault()
      const params = {
        status: props.state.status,
        no_sppa: props.state.number_sppa,
        class_bussiness: props.state.cob,
        type_product: props.state.product,
        policy_holder: props.state.policy_holder,
        insured: props.state.insured,
      }
      props.fetchSearch(params)
    },
    handleProduct: props => (id) => {
      props.getData(
        { base: 'apiUser', url: `/product/${id}?product_type=`, method: 'get' },
      ).then((res) => {
        props.changeState({
          ...props.state,
          cob: id,
          product: '',
          productLoad: false,
          productList: res.data,
        })
      }).catch((err) => {
        message.error(err)
        props.changeState({
          ...props.state,
          cob: id,
          product: '',
          productLoad: false,
          productList: [],
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetOption = [
        { url: '/class-of-business', name: 'cob' },
        { url: '/status', name: 'status' },
      ]
      configGetOption.map(item => (
        this.loadMasterData(item.url, item.name)
      ))
      const {
        location,
      } = history
      const newState = (qs.parse(location.search) || {})

      this.props.fetchSearch({
        status: '',
        no_sppa: '',
        class_bussiness: '',
        type_product: '',
        policy_holder: '',
        insured: '',
        page: newState.page,
      })
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(Form)
