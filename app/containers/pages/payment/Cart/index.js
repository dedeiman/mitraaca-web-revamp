import { connect } from 'react-redux'
import { compose } from 'recompose'
import Cart from 'components/pages/payment/Cart'

export function mapStateToProps() {
  return {}
}

const mapDispatchToProps = () => ({
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Cart)
