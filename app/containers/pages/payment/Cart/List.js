/* eslint-disable no-plusplus */
/* eslint-disable consistent-return */
import { connect } from 'react-redux'
import {
  compose, withState, withHandlers, lifecycle,
} from 'recompose'
import List from 'components/pages/payment/Cart/List'
import Swal from 'sweetalert2'
import history from 'utils/history'
import { updateSiteConfiguration } from 'actions/Site'
import qs from 'query-string'
import { isEmpty, pickBy, identity } from 'lodash'
import {
  OttomateCode, OttomateSmartCode,
  OttomateSolitareCode, OttomateTLOCode,
  OttomateComprehensiveCode, AsriCode,
  CargoMarineCode, CargoInlandTransitCode,
  LiabilityCode, OttomateSyariahCode, WellWomenCode,
  OttomateSyariahSmartCode, OttomateSyariahSolitareCode,
  OttomateSyariahTLOCode, OttomateSyariahComprehensiveCode,
  AsriSyariahCode, Amanah, TravelInternationalCode, TravelDomesticCode,
} from 'constants/ActionTypes'

import { getDatas } from 'actions/Option'
import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  const {
    cart,
  } = state.root
  const {
    currentUser,
  } = state.root.auth
  const {
    metaCart,
    isFetching,
  } = state.root.cart

  return {
    isFetching,
    cart,
    currentUser,
    metaCart,
  }
}


const mapDispatchToProps = dispatch => ({
  onCheckout: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})


function totalFromArray(arr) {
  if (!Array.isArray(arr)) return
  let totalNumber = 0
  for (let i = 0, l = arr.length; i < l; i++) {
    totalNumber += arr[i].value
  }
  return totalNumber
}


export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateCart', 'setStateCart', props => ({
    load: true,
    search: '',
    type: undefined,
    page: props.metaCart.current_page,
    perPage: props.metaCart.per_page,
  })),
  withState('state', 'changeState', {
    listCheck: [],
    listPrice: [],
  }),
  withState('loading', 'changeLoading', false),
  withHandlers({
    loadCart: props => (isSearch) => {
      const {
        search, page, type, perPage,
      } = props.stateCart
      const payload = {
        keyword: search,
        type_of_material: type,
        page: isSearch ? '' : page,
        per_page: isSearch ? '' : perPage,
      }

      history.push(`/payment-filter?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handlePage: props => (value) => {
      const { stateCart, setStateCart, loadCart } = props
      setStateCart({
        ...stateCart,
        page: value,
      })
      setTimeout(() => {
        loadCart()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { stateCart, setStateCart } = props
      setStateCart({
        ...stateCart,
        perPage: value,
      })
      setTimeout(() => {
        const payload = {
          per_page: value,
          search_by: stateCart.search ? stateCart.filter : '',
          keyword: stateCart.search,
        }
        props.fetchContest(`?${qs.stringify(pickBy(payload, identity))}`)
      }, 300)
    },
  }),
  withHandlers({
    handleCheckbox: props => (data) => {
      const { listCheck, listPrice } = props.state
      if (data.status === 'approved' || data.status === 'waiting-payment' || data.status === 'complete') {
        if (listCheck.includes(data.id)) {
          const resultFilter = listCheck.filter(key => key !== data.id)
          const resultFilterPrice = listPrice.filter(key => key.id !== data.id)
          props.changeState({
            ...props.state,
            listCheck: resultFilter,
            listPrice: resultFilterPrice,
            total: totalFromArray(resultFilterPrice),
          })
        } else {
          listCheck.push(data.id)
          listPrice.push({
            id: data.id,
            value: data.total_payment,
          })
          props.changeState({
            ...props.state,
            listCheck,
            listPrice,
            total: totalFromArray(listPrice),
          })
        }
      } else {
        Swal.fire({
          icon: 'error',
          title: '',
          html: "<span style='color:#2b57b7;font-weight:bold'>This item can't continue to process checkout</span>",
          customClass: {
            container: 'popup-container',
          },
        })
      }
    },
    handleSubmit: props => () => {
      const { listCheck } = props.state
      if (isEmpty(listCheck)) {
        return Swal.fire({
          icon: 'error',
          html: '<span style="color:#2b57b7;font-weight:bold">Please choose any item to checkout.</span>',
          customClass: {
            container: 'popup-container',
          },
        })
      }
      const params = {
        item_ids: listCheck,
      }
      props.onCheckout(
        { base: 'apiPayment', url: '/transactions/checkout', method: 'post' },
        params,
      ).then((res) => {
        if (res.meta.status) {
          history.push(`/checkout/${res.data.id}`)
        } else {
          Swal.fire({
            icon: 'error',
            html: '<span style="color:#2b57b7;font-weight:bold">Fail to create checkout.</span>',
            customClass: {
              container: 'popup-container',
            },
          })
        }
      }).catch(() => {
        Swal.fire({
          icon: 'error',
          html: '<span style="color:#2b57b7;font-weight:bold">Fail to create checkout.</span>',
          customClass: {
            container: 'popup-container',
          },
        })
      })
    },
    handleIO: () => (value, product) => {
      if (value !== null) {
        switch (product.code) {
          case OttomateCode:
          case OttomateSmartCode:
          case OttomateSolitareCode:
          case OttomateTLOCode:
          case OttomateComprehensiveCode:
          case OttomateSyariahCode:
          case OttomateSyariahSmartCode:
          case OttomateSyariahSolitareCode:
          case OttomateSyariahTLOCode:
          case OttomateSyariahComprehensiveCode:
            history.push(`/production-create-io/detail/${value}?product=${product.code}`)
            break
          case Amanah:
            return history.push(`/production-create-io/detail/${value}?product=${product.code}`)
          case AsriCode:
          case AsriSyariahCode:
            return ''
          case CargoMarineCode:
          case CargoInlandTransitCode:
            return ''
          case LiabilityCode:
            return ''
          default:
            return ''
        }
      } else {
        Swal.fire(
          '',
          '<span style="color:#2b57b7;font-weight:bold">Indicative Offer tidak ditemukan!</span>',
          'warning',
        )
      }
    },
    handleComingSoon() {
      Swal.fire(
        'Coming Soon!',
        '<span style="color:#2b57b7;font-weight:bold">Fitur akan segera hadir!</span>',
        'info',
      )
    },
    handleDetailDocument: props => (value) => {
      props.onCheckout(
        { base: 'apiUser', url: `/insurance-letters/${value.id}/documents`, method: 'get' },
        null,
      ).then((res) => {
        switch (value.product.code) {
          case OttomateCode:
          case OttomateSmartCode:
          case OttomateSolitareCode:
          case OttomateTLOCode:
          case OttomateComprehensiveCode:
          case OttomateSyariahCode:
          case OttomateSyariahSmartCode:
          case OttomateSyariahSolitareCode:
          case OttomateSyariahTLOCode:
          case OttomateSyariahComprehensiveCode:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          case Amanah:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          case AsriCode:
          case AsriSyariahCode:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          case CargoMarineCode:
          case CargoInlandTransitCode:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          case LiabilityCode:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          case WellWomenCode:
          case TravelInternationalCode:
          case TravelDomesticCode:
            if (res.data) {
              history.push(`/production-create-sppa/document/detail/${value.id}`)
            } else {
              history.push(`/production-create-sppa/upload/${value.id}?product=${value.product.code}`)
            }
            break
          default:
            return ''
        }
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        location, stateCart,
      } = history
      const newState = (qs.parse(location.search) || {})

      this.props.updateSiteConfiguration(location.search)

      this.props.setStateCart({
        ...stateCart,
        load: false,
        search: newState.keyword,
        type: newState.type_of_material,
        page: newState.page,
      })
    },
    componentWillReceiveProps(nextProps) {
     
      this.props.updateSiteConfiguration('activePage', 'payment-filter')
      const {
        cart,
      } = this.props

      if (cart !== nextProps.cart) {
        this.props.changeState({
          listCheck: [],
          listPrice: [],
          total: 0,
        })
      }
    },
  }),
)(List)
