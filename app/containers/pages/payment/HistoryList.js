/* eslint-disable consistent-return */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { getDatas } from 'actions/Option'
import { identify, pickBy } from 'lodash'
import { message } from 'antd'
import qs from 'query-string'
import HistoryList from 'components/pages/payment/HistoryList'

export function mapStateToProps(state) {
  const {
    cart,
  } = state.root
  return {
    cart,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    list: [],
    load: true,
    status: '',
    productList: [],
    page: 1,
    meta_page: {
      per_page: 1,
      page: 1,
      total_count: 1,
    },
  }),
  withHandlers({
    loadMasterData: props => (isSearch) => {
      const { state, changeState } = props
      const params = {
        page: isSearch ? 1 : state.page,
        product_id: state.product || '',
        status: state.status || '',
      }
      props.getDatas(
        { base: 'apiPayment', url: `/transaction-history/?${qs.stringify(pickBy(params, identify))}`, method: 'get' },
      ).then((res) => {
        changeState({
          ...state,
          meta_page: {
            per_page: res.meta.per_page,
            page: res.meta.current_page,
            total_count: res.meta.total_count,
          },
          load: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err.message)
        changeState({
          ...state,
          meta_page: {
            per_page: 1,
            page: 1,
            total_count: 1,
          },
          load: false,
          list: [],
        })
      })
    },
  }),
  withHandlers({
    handlePage: props => (value) => {
      const { state, changeState, loadMasterData } = props
      changeState({
        ...state,
        page: value,
      })
      setTimeout(() => {
        loadMasterData()
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.getDatas(
        { base: 'apiUser', url: '/products', method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          productList: res.data,
        })
        this.props.loadMasterData()
      }).catch((err) => {
        message.error(err.message)
        this.props.changeState({
          ...this.props.state,
          productList: [],
        })
      })
    },
  }),
)(HistoryList)