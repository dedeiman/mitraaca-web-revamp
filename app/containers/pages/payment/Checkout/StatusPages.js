import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, lifecycle, withState } from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import StatusPage from 'components/pages/payment/Checkout/StatusPages'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  return {
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('dataPayment', 'setDataPayment', {
    loadPayment: true,
    getDataPayment: {},
  }),
  lifecycle({
    componentDidMount() {
     
      this.props.updateSiteConfiguration('activePage', 'payment')
      this.props.updateSiteConfiguration('isActiveSidebar', false)
      this.props.setDataPayment({
        loadPayment: false,
        getDataPayment: JSON.parse(localStorage.getItem('dataVirtualAccount')),
      })
    },
  }),
)(StatusPage)
