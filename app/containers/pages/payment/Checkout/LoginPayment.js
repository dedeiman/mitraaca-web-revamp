import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose, withHandlers, lifecycle } from 'recompose'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import { updateSiteConfiguration } from 'actions/Site'
import LoginPayment from 'components/pages/payment/Checkout/LoginPayment'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default Form.create({ name: 'loginPaymentForm' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withHandlers({
      onChange: props => (e) => {
        const event = e.target || e
        const { name, value } = event

        props.form.setFieldsValue({
          [name]: { value },
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { form } = props

        form.validateFields((err) => {
          if (!err) {
            props.getDatas(
              { base: 'apiPayment', url: '/klik-mitraca/login', method: 'post' },
            ).then(() => {

            }).catch((error) => {
              Swal.fire(
                '',
                `<span style="color:#2b57b7;font-weight:bold">${error.message}</span>`,
                'error',
              )
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {

        this.props.updateSiteConfiguration('activePage', 'payment')
        this.props.updateSiteConfiguration('isActiveSidebar', false)
      },
    }),
  )(LoginPayment),
)
