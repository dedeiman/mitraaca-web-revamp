import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import Checkout from 'components/pages/payment/Checkout'
import { bindActionCreators } from 'redux'
import { getDatas } from 'actions/Option'
import Swal from 'sweetalert2'
import history from 'utils/history'

export function mapStateToProps(state) {
  const {
    listCheckout,
  } = state.root

  return {
    listCheckout,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('statePayment', 'setStatePayment', {
    paymentLoad: true,
    paymentList: [],
  }),
  withState('listCheckout', 'setListCheckout', {
    isFetching: true,
    listCart: null,
  }),
  withState('detailPayment', 'setDetailPayment', {}),
  withState('selectPayment', 'changeSelectPayment', ''),
  withState('loading', 'changeLoading', false),
  withHandlers({
    onChangeRadio: props => (id) => {
      props.changeSelectPayment(id)
    },

    handlePayment: props => () => {
      const params = {
        payment_method_id: props.selectPayment,
      }

      props.changeLoading(true)
      props.getDatas(
        { base: 'apiPayment', url: `/transactions/${props.match.params.id}/select-payment`, method: 'put' },
        params,
      ).then((res) => {
        switch (res.data.payment_method.code) {
          case 'KLIKMITRACA':
            localStorage.setItem('dataVirtualAccount', JSON.stringify(res.data))

            setTimeout(() => {
              history.push(`/status/${props.match.params.id}/${res.data.payment_method.slug}`)
            }, 400)

            props.changeLoading(false)
            break
          case 'MASTERCARDVISA':
            props.getDatas(
              { base: 'apiPayment', url: `/transactions/${res.data.id}/get-pg-request`, method: 'get' },
            ).then((resp) => {
              props.setDetailPayment(resp.data)
              setTimeout(() => {
                document.getElementById('btnSubmitValue').click()
                props.changeLoading(false)
              }, 400)
            }).catch(() => {
              props.changeLoading(false)
            })
            break
          case 'BCAVA':
            localStorage.setItem('dataVirtualAccount', JSON.stringify(res.data))

            setTimeout(() => {
              history.push(`/status/${props.match.params.id}/${res.data.payment_method.slug}`)
            }, 400)

            props.changeLoading(false)
            break
          default:
            break
        }
      }).catch(() => {
        Swal.fire({
          icon: 'error',
          html: '<span style="color:#2b57b7;font-weight:bold">Transaksi is Invalid!</span>',
          customClass: {
            container: 'popup-container',
          },
        })
      })
    },
  }),
  lifecycle({
    componentWillMount() {
      const {
        listCheckout, setListCheckout, setStatePayment, statePayment,
      } = this.props

      this.props.getDatas(
        { base: 'apiPayment', url: `/transactions/${this.props.match.params.id}/draft`, method: 'get' },
      ).then((res) => {
        setListCheckout({
          ...listCheckout,
          isFetching: false,
          listCart: res.data,
        })
      }).catch((err) => {
        Swal.fire({
          icon: 'error',
          html: `<span style="color:#2b57b7;font-weight:bold">${err.message}</span>`,
        })
      })

      this.props.getDatas(
        { base: 'apiPayment', url: '/payment-methods/', method: 'get' },
      ).then((res) => {
        setStatePayment({
          ...statePayment,
          paymentLoad: false,
          paymentList: res.data,
        })
      }).catch(() => {
        setStatePayment({
          ...statePayment,
          paymentLoad: false,
          paymentList: [],
        })
      })
    },
  }),
)(Checkout)
