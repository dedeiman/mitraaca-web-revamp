import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {compose, lifecycle} from 'recompose'
import { updateSiteConfiguration } from 'actions/Site'
import settingMenu from 'components/pages/settingMenu'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  return {
    groupRole, currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('activePage', 'setting-menu')
    },
  }),
)(settingMenu)
