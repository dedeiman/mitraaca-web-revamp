import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  lifecycle,
  withHandlers,
  withState,
} from 'recompose'
import history from 'utils/history'
import Swal from 'sweetalert2'
import { Form } from '@ant-design/compatible'
import { updateSiteConfiguration } from 'actions/Site'
import SettingMenu from 'components/pages/settingMenu/List'
import { getDatas } from 'actions/Option'
import { identity, pickBy, startCase } from 'lodash'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})
export default Form.create({ name: 'formSettingMenu' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('detailSettingMenu', 'setDetailSettingMenu', {
      loading: true,
      listDetailSettingMenu: [],
    }),
    withState('detailSettingMenuBefore', 'setDetailSettingMenuBefore', {
      loading: true,
      listDetailSettingMenuBefore: [],
    }),
    withState('checkedList', 'setCheckedList', []),
    withState('loadingSettingMenu', 'setLoadingSettingMenu', false),
    withHandlers({
      onChange: props => (list) => {
        props.setCheckedList(list)
      },
      getDetailSettingMenu: props => () => {
        const {
          getDatas,
          setDetailSettingMenu,
          detailSettingMenu,
          setDetailSettingMenuBefore,
          detailSettingMenuBefore,
        } = props
        getDatas(
          { base: 'apiUser', url: '/user/menu-settings', method: 'get' },
        ).then((res) => {
          setDetailSettingMenu({
            ...detailSettingMenu,
            loading: false,
            listDetailSettingMenu: (res.data || []).filter(item => item.is_configurable === true),
          })
          setDetailSettingMenuBefore({
            ...detailSettingMenuBefore,
            loading: false,
            listDetailSettingMenuBefore: (res.data || []).filter(item => item.is_configurable === true),
          })
        }).catch(() => {
          setDetailSettingMenu({
            ...detailSettingMenu,
            loading: false,
            listDetailSettingMenu: {},
          })
          setDetailSettingMenuBefore({
            ...detailSettingMenuBefore,
            loading: false,
            listDetailSettingMenuBefore: {},
          })
        })
      },
    }),
    withHandlers({
      onSubmit: props => () => {
        const { form, detailSettingMenu } = props

        form.validateFields((err, values) => {
          if (!err) {
            const finalPld = detailSettingMenu.listDetailSettingMenu.map(item => {
              if (item.name && values.permissions.indexOf(item.name) > -1) {
                return { permission_slug: item.name, is_locked: true }
              } return { permission_slug: item.name, is_locked: false }
            })
            const payload = { menu_settings: finalPld }
            props.getDatas(
              { base: 'apiUser', url: '/user/lock-menu', method: 'put' },
              payload,
            ).then(() => {
              Swal.fire(
                '',
                '<span style="color:#2b57b7;font-weight:bold">Update Setting Menu Success!</span>',
                'success',
              ).then(() => {
                history.push('/setting-menu')
              })
            }).catch((error) => {
              let messageErr = error
              if (error.response && error.response.data.meta) {
                messageErr = error.response.data.meta.message
              }
              Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${messageErr}</span>`, 'error')
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        if (window.location.pathname !== window.localStorage.getItem('prevPath')) {
          window.localStorage.setItem('isTrueSecondary', false)
          window.localStorage.setItem('prevPath', window.location.pathname)
        }
        // this.props.updateSiteConfiguration('activePage', 'setting-menu')
        this.props.getDetailSettingMenu()
      },
    }),
  )(SettingMenu),
)
