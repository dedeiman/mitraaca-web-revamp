import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchUser, deleteUser, userFailure } from 'actions/User'
import qs from 'query-string'
import history from 'utils/history'
import UserView from 'components/pages/users'
import { getDatas } from 'actions/Option'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataUser,
    metaUser,
  } = state.root.user

  return {
    isFetching,
    errorMessage,
    dataUser,
    metaUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchUser: bindActionCreators(fetchUser, dispatch),
  deleteUser: bindActionCreators(deleteUser, dispatch),
  userFailure: bindActionCreators(userFailure, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateCheck', 'setStateCheck', {
    isLocked: false,
  }),
  withState('keyword', 'setKeyword', ''),
  withState('currentPage', 'setCurrentPage', ''),
  withHandlers({
    handleFilter: props => () => {
      const payload = {
        keyword: props.keyword,
        page: props.currentPage,
      }
      history.push(`/users?${qs.stringify(payload)}`)
    },
  }),
  withHandlers({
    handleSearch: props => (value) => {
      props.setKeyword(value)
      setTimeout(() => {
        history.push(`/users${value ? `?keyword=${value}` : ''}`)
      }, 300)
    },
    handlePage: props => (page) => {
      props.setCurrentPage(page)
      setTimeout(() => {
        props.handleFilter()
      }, 300)
    },
    handleDelete: props => (id) => {
      props.deleteUser(id)
    },
    closeError: props => () => props.userFailure(''),
  }),
  lifecycle({
    componentDidMount() {
      const { setKeyword, setCurrentPage, location } = this.props
      const search = qs.parse(location.search) || {}
      setCurrentPage(Number(search.page))
      setKeyword(search.keyword)
      this.props.updateSiteConfiguration('breadList', ['Home', 'User'])
      this.props.fetchUser(location.search)
      this.props.getDatas(
        { base: 'apiUser', url: `/auth/check-islocked/${window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1)}`, method: 'get' },
      ).then((res) => {
        if (window.location.pathname !== window.localStorage.getItem('prevPath')) {
          window.localStorage.setItem('isTrueSecondary', false)
          window.localStorage.setItem('prevPath', window.location.pathname)
        }
        if (res.data.is_locked === true) {
          this.props.setStateCheck({
            ...this.props.stateCheck,
            isLocked: true,
          })
        } else {
          this.props.setStateCheck({
            ...this.props.stateCheck,
            isLocked: false,
          })
        }
      }).catch(() => {})
    },
  }),
)(UserView)
