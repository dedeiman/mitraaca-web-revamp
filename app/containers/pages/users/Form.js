import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  fetchDetailUser, createUser, updateUser,
  getRole, userDetail,
} from 'actions/User'
import { getCOB, getProducts, getBranches } from 'actions/Option'
import { message } from 'antd'
import FormUserView from 'components/pages/users/Form'
import Helper from 'utils/Helper'
import history from 'utils/history'
import {
  pickBy, identity, isEmpty, startCase,
} from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailUser,
    errorMessage,
    errorObject,
  } = state.root.user
  return {
    isFetching,
    detailUser,
    errorMessage,
    errorObject,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailUser: bindActionCreators(fetchDetailUser, dispatch),
  createUser: bindActionCreators(createUser, dispatch),
  updateUser: bindActionCreators(updateUser, dispatch),
  userDetail: bindActionCreators(userDetail, dispatch),
  getRole: bindActionCreators(getRole, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
  getProducts: bindActionCreators(getProducts, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('avatarFileList', 'setAvatarFileList', ''),
  withState('loadRole', 'setLoadRole', false),
  withState('selectOption', 'setSelectOption', []),
  withState('loadGroupRole', 'setLoadGroupRole', true),
  withState('selectGroupOption', 'setSelectGroupOption', []),
  withState('loadCOB', 'setLoadCOB', true),
  withState('selectCOBOption', 'setSelectCOBOption', []),
  withState('loadBranches', 'setLoadBranches', true),
  withState('selectBranchesOption', 'setSelectBranchesOption', []),
  withState('stateProducts', 'setStateProducts', []),
  withState('stateBranches', 'setStateBranches', []),
  withState('isEdit', 'setIsEdit', false),
  withState('errorsField', 'setErrorsField', {}),
  withState('currentState', 'setCurrentState', {
    keys: [],
    cob: [],
    productIds: [],
    branch: [],
    perwakilan: [],
  }),
  withHandlers({
    handleUpload: props => (info) => {
      Helper.getBase64(info.file, file => props.setAvatarFileList(file))
    },
    handleSelect: props => (val, key) => {
      if (key === 'role-group') {
        props.setLoadRole(true)
        props.getRole(`?group=${val}`).then((res) => {
          props.setSelectOption(res)
          props.setLoadRole(false)
        })
        if (val === 8) {
          props.getCOB().then((res) => {
            props.setSelectCOBOption(res)
            props.setLoadCOB(false)
            props.setStateProducts([{ load: false, list: [] }])
          })
        } else if (val === 9) {
          props.getBranches().then((res) => {
            props.setSelectBranchesOption(res)
            props.setLoadBranches(false)
            props.setStateBranches([{ load: false, list: [] }])
          })
        }
      }
      if (key.includes('class_of_business_')) {
        const idx = Number(key.replace('class_of_business_', ''))
        const currentArr = props.stateProducts
        currentArr[idx].load = true
        props.setStateProducts(currentArr)

        props.getProducts(`/${val}`).then((res) => {
          currentArr[idx].list = res
          currentArr[idx].load = false
          props.setStateProducts(currentArr)
        })
      }
      if (key.includes('branch_id_')) {
        const idx = Number(key.replace('branch_id_', ''))
        const currentArr = props.stateBranches
        currentArr[idx].load = true
        props.setStateBranches(currentArr)

        props.getBranches(`-perwakilan/${val}`).then((res) => {
          currentArr[idx].list = res
          currentArr[idx].load = false
          props.setStateBranches(currentArr)
        })
      }
    },
    handleSubmit: props => (values) => {
      const { params } = props.match
      const payload = {
        ...values,
        profile_pic_file: values.profile_pic_file ? props.avatarFileList : '',
        phone_number: values.phone_number.toString(),
        nik: values.nik.toString(),
      }
      delete payload.keys
      // const formData = new FormData()
      // Object.keys(pickBy(payload, identity)).map((item) => {
      //   if ((item === 'profile_pic_file') && payload[item]) return formData.append(item, payload[item].file)
      //   return formData.append(item, payload[item])
      // })

      props[`${params.id ? 'upd' : 'cre'}ateUser`](pickBy(payload, identity), (params.id || null)).then(() => {
        message.success(`User has been ${params.id ? 'upd' : 'cre'}ated`)
        history.push('/users')
      })
    },
    getErrors: props => () => {
      let objError = {}
      Object.keys(props.errorObject).forEach((item) => {
        objError = {
          ...objError,
          [item]: {
            validateStatus: 'error',
            help: startCase((props.errorObject[item]).replace('_id', '').replace('_', ' ')),
          },
        }
        return props.setErrorsField(objError)
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        match, setSelectGroupOption,
        setLoadGroupRole, setIsEdit,
      } = this.props
      this.props.updateSiteConfiguration('breadList', ['Home', 'User - Form'])
      this.props.userDetail({})
      this.props.getRole('-group').then((res) => {
        setSelectGroupOption(res)
        setLoadGroupRole(false)
      })
      if (match.params.id) {
        setIsEdit(true)
        this.props.fetchDetailUser(match.params.id)
      }
      // this.props.getErrors()
    },
    componentWillReceiveProps(nextProps) {
      const {
        detailUser, setAvatarFileList, setSelectOption, setLoadRole,
        setSelectBranchesOption, setLoadBranches, setSelectCOBOption,
        setLoadCOB, errorObject, setCurrentState, currentState,
      } = this.props
      if (detailUser !== nextProps.detailUser) {
        setAvatarFileList(nextProps.detailUser.profile_pic)
        if (nextProps.detailUser.role) {
          const { group } = nextProps.detailUser.role

          if (group.id === 8) {
            const { products: arrProducts } = nextProps.detailUser
            this.props.getCOB().then((res) => {
              setSelectCOBOption(res)
              setLoadCOB(false)
            })
            if (!isEmpty(arrProducts)) {
              const cob = []; const productIds = []; const
                keys = []
              arrProducts.forEach((item, idx) => {
                this.getDataProducts(item.cob_id)
                keys.push(idx)
                cob.push(item.cob_id)
                productIds.push(item.id)
              })
              setCurrentState({
                ...currentState,
                keys,
                cob,
                productIds,
              })
            }
          } else if (group.id === 9) {
            const { branch_perwakilan: arrBranches } = nextProps.detailUser
            this.props.getBranches().then((res) => {
              setSelectBranchesOption(res)
              setLoadBranches(false)
            })
            if (!isEmpty(arrBranches)) {
              const branch = []; const perwakilan = []; const
                keys = []
              arrBranches.forEach((item, idx) => {
                this.getDataBranch(item.branch_id)
                keys.push(idx)
                branch.push(item.branch_id)
                perwakilan.push(item.id)
              })
              setCurrentState({
                ...currentState,
                keys,
                branch,
                perwakilan,
              })
            }
          }
          this.props.getRole(`?group=${group.id}`).then((res) => {
            setSelectOption(res)
            setLoadRole(false)
          })
        }
      }

      if (errorObject !== nextProps.errorObject) {
        this.props.getErrors()
      }
    },
    getDataProducts(id) {
      const { stateProducts, setStateProducts } = this.props
      const newProducts = stateProducts

      this.props.getProducts(`/${id}`).then((res) => {
        newProducts.push({ load: false, list: res })
      })

      setStateProducts(newProducts)
    },
    getDataBranch(id) {
      const { stateBranches, setStateBranches } = this.props
      const newBranches = stateBranches

      this.props.getBranches(`-perwakilan/${id}`).then((res) => {
        newBranches.push({ load: false, list: res })
      })

      setStateBranches(newBranches)
    },
  }),
)(FormUserView)
