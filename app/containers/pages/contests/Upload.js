import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  withState,
} from 'recompose'
import { searchContest, uploadContest } from 'actions/Contest'
import UploadContest from 'components/pages/contests/Upload'
import history from 'utils/history'
import XLSX from 'xlsx'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    isFetching,
  } = state.root.contest

  return {
    isFetching,
  }
}

const mapDispatchToProps = dispatch => ({
  uploadContest: bindActionCreators(uploadContest, dispatch),
  searchContest: bindActionCreators(searchContest, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateContest', 'setStateContest', {
    list: [],
    load: false,
    pick: {},
    sheets: [],
    file: '',
    fileList: [],
    isFetching: false,
  }),
  withHandlers({
    handleSelect: props => (val) => {
      const { stateContest, setStateContest } = props
      const newData = stateContest.list.filter(item => item.contest_id === val)[0]
      setStateContest({
        ...stateContest,
        pick: newData,
      })
    },
    handleSearch: props => (val) => {
      const { stateContest, setStateContest } = props
      props.searchContest(val)
        .then(res => (
          setStateContest({
            ...stateContest,
            list: res,
          })
        ))
    },
    handleSheetChange: props => (info) => {
      const { file, fileList } = info
      const { setStateContest, stateContest } = props
      const reader = new FileReader()
      let newFile = fileList
      newFile = newFile.slice(-1)

      reader.onload = (e) => {
        const data = e.target.result
        const readedData = XLSX.read(data, { type: 'binary' })

        setStateContest({
          ...stateContest,
          file,
          fileList: newFile,
          sheets: readedData.SheetNames,
          selected: readedData.SheetNames[0],
        })
      }
      reader.readAsBinaryString(file)
    },
    handleUpload: props => () => {
      const { stateContest } = props
      const { id } = stateContest.pick

      const formData = new FormData()
      formData.append('file', stateContest.file)
      formData.append('worksheet', stateContest.selected)

      props.setStateContest({
        ...stateContest,
        isFetching: true,
      })

      props.uploadContest(formData, id)
        .then((res) => {
          props.setStateContest({
            ...stateContest,
            isFetching: false,
          })
          Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Document has been uploaded</span>', 'success')
            .then(() => {
              history.push(`/contest-menu/${res.id}/winner`)
            })
        })
        .catch((err) => {
          Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error')
          props.setStateContest({
            ...stateContest,
            isFetching: false,
          })
        })
    },
  }),
)(UploadContest)
