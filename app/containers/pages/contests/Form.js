/* eslint-disable no-const-assign */
/* eslint-disable func-names */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import {
  fetchDetailContest, contestDetail,
  createContest, updateContest,
} from 'actions/Contest'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import { message } from 'antd'
import _, { isEmpty, pickBy, identity } from 'lodash'
import FormContest from 'components/pages/contests/Form'
import history from 'utils/history'
import Helper from 'utils/Helper'
import Swal from 'sweetalert2'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
    detailContest,
  } = state.root.contest

  return {
    isFetching,
    detailContest,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailContest: bindActionCreators(fetchDetailContest, dispatch),
  contestDetail: bindActionCreators(contestDetail, dispatch),
  createContest: bindActionCreators(createContest, dispatch),
  updateContest: bindActionCreators(updateContest, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formContest' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('banner', 'setBanner', ''),
    withState('stateRewards', 'setStateRewards', {
      list: [],
      visible: false,
      field: {},
      error: '',
    }),
    withState('participants', 'setParticipants', {
      load: true,
      list: [],
    }),
    withHandlers({
      handleUpload: props => (info) => {
        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/webp' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'
        const imageWidth = 1440
        const imageHeight = 590
        const fr = new FileReader()

        fr.readAsDataURL(info.file)
        fr.addEventListener('load', (event) => {
          const loadedImageUrl = event.target.result
          const image = document.createElement('img')
          image.src = loadedImageUrl
          image.addEventListener('load', () => {
            const { width, height } = image

            if (!isFileType) {
              message.error('You can only upload JPG/PNG file!')
              props.setBanner('')
              return props.form.setFieldsValue({ banner_file: undefined })
            }

            if ((imageWidth !== width) && (imageHeight !== height)) {
              message.error('You can only upload images that are 1440 wide and 590 high')
              props.setBanner('')
              return props.form.setFieldsValue({ banner_file: undefined })
            }

            return (
              Helper.getBase64(info.file, file => props.setBanner(file))
            )
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { params } = props.match
        props.form.validateFields((err, values) => {
          if (!err) {
            if (isEmpty(props.stateRewards.list)) {
              props.setStateRewards({ ...props.stateRewards, error: '*Wajib diisi' })
            } else {
              const payload = {
                ...values,
                name: values.name ? (values.name).toUpperCase() : '',
                description: values.description ? (values.description).toUpperCase() : '',
                end_date: values.end_date ? moment(values.end_date).format('YYYY-MM-DD') : '',
                start_date: values.start_date ? moment(values.start_date).format('YYYY-MM-DD') : '',
                rewards: (
                  !isEmpty(props.stateRewards.list)
                    ? (props.stateRewards.list.map(item => (
                      pickBy(({ id: isEmpty(item.id) ? '' : item.id, name: item.name, description: item.description }), identity)
                    )))
                    : []
                ),
              }
              delete payload.reward_name
              delete payload.reward_description

              const formData = new FormData()
              Object.keys(pickBy(payload, identity)).map((item) => {
                if ((item === 'banner_file') && payload[item]) return formData.append(item, payload[item].file)
                if ((((item === 'rewards') || (item === 'participant_level_ids'))) && payload[item]) return formData.append(item, JSON.stringify(payload[item]))
                return formData.append(item, payload[item])
              })

              props[`${params.id ? 'upd' : 'cre'}ateContest`](formData, (params.id || null))
                .then(() => (
                  Swal.fire(`Kontes telah di${params.id ? 'perbarui' : 'tambahkan'}`, '<span style="color:#2b57b7;font-weight:bold">Kembali ke list Kontes</span>', 'success')
                    .then(() => history.push('/contest-menu'))
                ))
                .catch(error => (
                  Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
                ))
            }
          }
        })
      },

      removeItem: props => (idx) => {
        props.setStateRewards({
          ...props.stateRewards,
          list: _.remove(props.stateRewards.list, e => (isEmpty(e.id) ? e.idx : e.id) !== idx),
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          match, setParticipants, detailContest, setStateRewards, stateRewards,
        } = this.props

        if (match.params.id) {
          this.props.fetchDetailContest(match.params.id)

          const list = (detailContest.rewards || []).map(item => ({
            id: item.id, reward_id: item.reward_id, name: item.name, description: item.description,
          }))

          setStateRewards({
            ...stateRewards,
            list,
          })
        } else {
          this.props.contestDetail({})
        }

        this.props.getDatas(
          { base: 'apiUser', url: '/levels', method: 'get' },
        ).then(res => (
          setParticipants({
            load: false,
            list: res.data,
          })
        ))
      },
      componentWillReceiveProps(nextProps) {
        const {
          detailContest, stateRewards, setStateRewards, setBanner,
        } = this.props
        if (detailContest !== nextProps.detailContest) {
          const { banner_url: bannerUrl, rewards } = nextProps.detailContest
          const list = (rewards || []).map(item => ({
            id: item.id, reward_id: item.reward_id, name: item.name, description: item.description,
          }))
          setBanner(bannerUrl)
          setStateRewards({
            ...stateRewards,
            list,
          })
        }
      },
    }),
  )(FormContest),
)
