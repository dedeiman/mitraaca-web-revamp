import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import { fetchDetailContest, deleteContest, cancelWinner } from 'actions/Contest'
import DetailContest from 'components/pages/contests/Detail'
import history from 'utils/history'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  const {
    isFetching,
    detailContest,
  } = state.root.contest

  return {
    isFetching,
    detailContest,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailContest: bindActionCreators(fetchDetailContest, dispatch),
  deleteContest: bindActionCreators(deleteContest, dispatch),
  cancelWinner: bindActionCreators(cancelWinner, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateModal', 'setStateModal', {
    idRewards: '',
    idContest: '',
    visible: false,
  }),
  withHandlers({
    handleDelete: props => () => {
      const { params } = props.match
      props.deleteContest(params.id)
        .then(() => (
          Swal.fire('Kontes telah dihapus', '<span style="color:#2b57b7;font-weight:bold">Kembali ke list Kontes</span>', 'success')
            .then(() => history.push('/contest-menu'))
        ))
        .catch(err => (
          Swal.fire(err, '', 'error')
        ))
    },
    handleCancelWinner: props => (payload) => {
      const { agentId, rewardId, uuid } = payload
      props.cancelWinner(agentId, rewardId, uuid)
        .then(() => props.fetchDetailContest(props.match.params.id))
        .catch(err => Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${err}</span>`, 'error'))
    },
  }),
  lifecycle({
    componentDidMount() {
      const { match } = this.props
      this.props.fetchDetailContest(match.params.id)
    },
  }),
)(DetailContest)
