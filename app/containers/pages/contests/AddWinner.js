import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { fetchDetailContest } from 'actions/Contest'
import AddWinner from 'components/pages/contests/AddWinner'

export function mapStateToProps(state) {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchDetailContest: bindActionCreators(fetchDetailContest, dispatch),
})

export default Form.create({ name: 'formAddWinner' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('cancellationTypes', 'setCancellationTypes', {}),
    withState('stateSelect', 'setStateSelect', {}),
    withHandlers({
      handleSearch: props => (val) => {
        props.setStateSelect({ ...props.stateSelect, agentLoad: true })
        props.getDatas(
          { base: 'apiUser', url: `/search-agents?keyword=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch(() => {
          props.setStateSelect({
            ...props.stateSelect,
            agentLoad: false,
            agentList: [],
          })
        })
      },
      onSubmit: props => (event, stateModal) => {
        event.preventDefault()

        const { form } = props
        form.validateFields((err, values) => {
          if (!err) {
            props.getDatas(
              { base: 'apiContent', url: `/contests/${stateModal.idContest}/add-winner`, method: 'put' },
              { reward_id: stateModal.idRewards, agent_id: values.agent_id },
            ).then(() => {
              props.toggle(true)
              setTimeout(() => props.fetchDetailContest(stateModal.idContest))
            }).catch((error) => {
              if (error.errors && Object.keys(error.errors).length) {
                let objError = {}
                Object.keys(error.errors).forEach((item) => {
                  objError = {
                    ...objError,
                    [item]: {
                      validateStatus: 'error',
                      errors: [new Error('Agent ID  is required')],
                    },
                  }

                  props.form.setFields(objError)
                })
              }
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        [
          { url: '/branches', name: 'branch' },
          { url: '/cancellation-types', name: 'cancel' },
        ].map(item => this.loadMasterData(item.url, item.name))
      },
      loadMasterData(url, name) {
        this.props.getDatas(
          { base: 'apiUser', url, method: 'get' },
        ).then((res) => {
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err.message)
          this.props.setStateSelect({
            ...this.props.stateSelect,
            [`${name}Load`]: false,
            [`${name}List`]: [],
          })
        })
      },
    }),
  )(AddWinner),
)
