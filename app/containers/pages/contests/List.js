/* eslint-disable no-shadow */
/* eslint-disable func-names */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pickBy, identity } from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import ListContest from 'components/pages/contests/List'
import { fetchContest } from 'actions/Contest'
import history from 'utils/history'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const {
    currentUser,
  } = state.root.auth

  const {
    isFetching,
    errorMessage,
    dataContest,
    metaContest,
  } = state.root.contest

  return {
    isFetching,
    errorMessage,
    dataContest,
    metaContest,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchContest: bindActionCreators(fetchContest, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateContest', 'setStateContest', props => ({
    load: true,
    list: [],
    search: '',
    filter: 'name',
    page: props.metaContest.current_page,
    perPage: props.metaContest.per_page,
  })),
  withHandlers({
    loadContest: props => (isSearch) => {
      const {
        filter, search,
        page, perPage,
      } = props.stateContest
      const payload = {
        search_by: search ? filter : '',
        keyword: search,
        page: isSearch ? '' : page,
        per_page: isSearch ? '' : perPage,
      }
      history.push(`/contest-menu?${qs.stringify(pickBy(payload, identity))}`)
    },
  }),
  withHandlers({
    handleFilter: props => (val) => {
      const { stateContest, setStateContest } = props
      setStateContest({
        ...stateContest,
        filter: val,
      })
    },
    handlePage: props => (value) => {
      const { stateContest, setStateContest, loadContest } = props
      setStateContest({
        ...stateContest,
        page: value,
      })
      setTimeout(() => {
        loadContest()
      }, 300)
    },
    updatePerPage: props => (current, value) => {
      const { stateContest, setStateContest } = props
      setStateContest({
        ...stateContest,
        perPage: value,
      })
      setTimeout(() => {
        const payload = {
          per_page: value,
          search_by: stateContest.search ? stateContest.filter : '',
          keyword: stateContest.search,
        }
        props.fetchContest(`?${qs.stringify(pickBy(payload, identity))}`)
      }, 300)
    },
  }),
  lifecycle({
    componentDidMount() {
      if (window.location.pathname !== window.localStorage.getItem('prevPath')) {
        window.localStorage.setItem('isTrueSecondary', false)
        window.localStorage.setItem('prevPath', window.location.pathname)
      }
      const { location, setStateContest, stateContest } = this.props
      const newState = (qs.parse(location.search) || {})

      this.props.fetchContest(location.search)

      setStateContest({
        ...stateContest,
        load: false,
        search: newState.keyword,
        filter: newState.search_by || 'name',
        page: newState.page,
      })
    },
  }),
)(ListContest)
