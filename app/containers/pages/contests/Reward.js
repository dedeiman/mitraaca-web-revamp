import { connect } from 'react-redux'
import {
  compose, withHandlers,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import FormReward from 'components/pages/contests/Reward'
import moment from 'moment'

export default Form.create({ name: 'formReward' })(
  compose(
    connect(
      null,
      null,
    ),
    withHandlers({
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          const payload = {
            ...values,
            name: values.name ? (values.name).toUpperCase() : '',
            description: values.description ? (values.description).toUpperCase() : '',
          }

          if (!err) {
            const {
              list, submit,
              isEdit, data,
            } = props
            if (isEdit !== false) {
              list[isEdit] = { ...payload, id: data.id, reward_id: (data.reward_id || '') }
            } else {
              list.push({ ...payload, idx: moment().milliseconds().toString() })
            }
            submit(list)
            props.form.resetFields()
          }
        })
      },
    }),
  )(FormReward),
)
