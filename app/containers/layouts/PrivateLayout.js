import { connect } from 'react-redux'
import {
  compose, withHandlers, withState, lifecycle,
} from 'recompose'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { clearCurrentUser } from 'actions/Auth'
import { updateSiteConfiguration } from 'actions/Site'
import { first, isEmpty, last } from 'lodash'
import Swal from 'sweetalert2'
import PrivateLayoutView from 'components/layouts/PrivateLayout'

export function mapStateToProps(state) {
  const { site } = state.root
  const { currentUser } = state.root.auth
  return {
    site,
    currentUser,
    groupRole: state.root.role,
  }
}

const mapDispatchToProps = dispatch => ({
  clearCurrentUser: bindActionCreators(clearCurrentUser, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('isCollapsed', 'toggleCollapsed', false),
  withState('imgAvatar', 'setAvatar', props => (
    props.currentUser ? props.currentUser.profile_pic : '/assets/avatar-on-error.jpg'
  )),
  withHandlers({
    doLogout: props => () => {
      window.localStorage.setItem('isTrueSecondary',false)
      props.clearCurrentUser()
    },
  }),
  withHandlers({
    avatarError: props => () => {
      props.setAvatar('/assets/avatar-on-error.jpg')
    },
    handleKeys: props => (param, isOpenSub) => {
      if (isOpenSub) {
        props.updateSiteConfiguration('activeSubPage', last(param))
      } else {
        props.updateSiteConfiguration('activePage', first(param.keyPath))
        localStorage.setItem('isPath', param.keyPath)
      }
    },
  }),
  lifecycle({
    componentWillMount() {
      this.props.updateSiteConfiguration('isActiveSidebar', true)

      if (this.props.location.pathname === '/dashboard') {
        window.localStorage.removeItem('isPath')
      }
    },
    componentDidMount() {
      const { validations } = this.props.currentUser

      const menuRefs = document.querySelector(
        `.restore-${this.props.location.state}`,
      )

      if (menuRefs) {
        menuRefs.scrollIntoView({ behavior: 'smooth', block: 'center' })
      }

      if (isEmpty(validations)) {
        this.props.clearCurrentUser()
      }

      validations.forEach((validate) => {
        if ((validate.name === 'email_verify' || validate.name === 'phone_verify') && !validate.is_validate) {
          Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Mohon Aktivasi Agent Terlebih Dahulu!<br>Silahkan cek email anda.</span>', 'error')
            .then(() => {
              this.props.clearCurrentUser()
            })
        }
      })
    },
  }),
)(PrivateLayoutView)
