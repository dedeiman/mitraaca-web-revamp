import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { Router, Route, Switch } from 'react-router-dom'
import history from 'utils/history'
import privateComponent from 'utils/hocs/privateComponent'
import publicComponent from 'utils/hocs/publicComponent'
import PublicLayout from 'containers/layouts/PublicLayout'
import PrivateLayout from 'containers/layouts/PrivateLayout'

import Login from 'containers/login/Login'
import Forgot from 'containers/login/Forgot'
import Verify from 'containers/login/Verify'
import NotFound from 'containers/NotFound'
import Dashboard from 'containers/pages/dashboard'
import FAQ from 'containers/pages/faq'
import Announcement from 'containers/pages/announcement'
import Materi from 'containers/pages/contents'
import Profile from 'containers/pages/profiles'
import Contest from 'containers/pages/contests'
import Customer from 'containers/pages/customers'
import Policy from 'containers/pages/policies'
import DetailPolicy from 'containers/pages/policies/Detail'
import Agent from 'containers/pages/agents'
import PrintPolicy from 'containers/pages/printPolicies'
import Production from 'containers/pages/production'
import Report from 'containers/pages/report'
import TrainingClass from 'containers/pages/trainings'
import Cart from 'containers/pages/payment'
import ApprovalList from 'containers/pages/approval'
import StatusPage from 'containers/pages/payment/Checkout/StatusPages'
import LoginPayment from 'containers/pages/payment/Checkout/LoginPayment'
import SecondaryPassword from 'containers/pages/secondaryPassword/index'
import SettingMenu from 'containers/pages/settingMenu'

const PublicRoute = (props) => {
  const {
    component: Component,
    redirect,
    redirectPath,
    ...rest
  } = props

  const PublicLayoutView = publicComponent(PublicLayout, redirect, redirectPath)

  return (
    <Route
      {...rest}
      render={matchProps => (
        <PublicLayoutView>
          <Component {...matchProps} />
        </PublicLayoutView>
      )}
    />
  )
}

const PrivateRoute = ({ component: Component, ...rest }) => {
  const PrivateLayoutView = privateComponent(PrivateLayout)

  return (
    <Route
      {...rest}
      render={matchProps => (
        <PrivateLayoutView>
          <Component {...matchProps} />
        </PrivateLayoutView>
      )}
    />
  )
}

const Root = ({ store, persistor }) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>

      <Router history={history}>
        <Switch>
          <PublicRoute redirect exact path="/" component={Login} />
          <PublicRoute redirect exact path="/password/:type/:tkn" component={Forgot} />
          <PublicRoute redirect exact path="/password/:type" component={Forgot} />
          <PublicRoute redirect exact path="/verify" component={Verify} />
          <PrivateRoute path="/payment-checkout-mitraca" component={LoginPayment} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/profile/:type" component={Profile} />
          <PrivateRoute exact path="/production-create-io" component={Production} />
          <PrivateRoute exact path="/production-create-sppa" component={Production} />
          <PrivateRoute exact path="/production-create-io/create" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/create" component={Production} />
          <PrivateRoute exact path="/production-create-io/detail/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/detail/:id" component={Production} />
          <PrivateRoute exact path="/production-create-io/edit/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/document/detail/:id" component={Production} />
          {/* Sementawis */}
          <PrivateRoute exact path="/production-create-sppa/confirm-travel-international/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/confirm-asri/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/confirm-cargo/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/confirm-offer/:status/:id" component={Production} />
          <PrivateRoute exact path="/production-create-io/confirm-offer/:id" component={Production} />
          {/* Sementawis */}
          <PrivateRoute exact path="/production-create-sppa/confirm/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/upload/:id" component={Production} />
          <PrivateRoute exact path="/production-create-sppa/edit/:id" component={Production} />
          <PrivateRoute exact path="/production-search-sppa-menu" component={Production} />
          <PrivateRoute exact path="/production-search-io-menu" component={Production} />
          <PrivateRoute path="/faq-menu/:id/edit" component={FAQ} />
          <PrivateRoute path="/faq-menu/add" component={FAQ} />
          <PrivateRoute path="/faq-menu/:id" component={FAQ} />
          <PrivateRoute path="/faq-menu" component={FAQ} />
          <PrivateRoute path="/announcement" component={Announcement} />
          <PrivateRoute path="/materi-menu/:id/edit" component={Materi} />
          <PrivateRoute path="/materi-menu/add" component={Materi} />
          <PrivateRoute path="/materi-menu/:id" component={Materi} />
          <PrivateRoute path="/materi-menu" component={Materi} />
          <PrivateRoute path="/contest-menu/:id/edit" component={Contest} />
          <PrivateRoute path="/contest-menu/add" component={Contest} />
          <PrivateRoute path="/contest-menu/:id" component={Contest} />
          <PrivateRoute path="/contest-menu" component={Contest} />
          <PrivateRoute path="/customer-menu/:id/edit" component={Customer} />
          <PrivateRoute path="/customer-menu/add" component={Customer} />
          <PrivateRoute path="/customer-menu/:id" component={Customer} />
          <PrivateRoute path="/customer-menu" component={Customer} />
          <PrivateRoute path="/training-class-menu/:id/edit" component={TrainingClass} />
          <PrivateRoute path="/training-class-menu/:id/history" component={TrainingClass} />
          <PrivateRoute path="/training-class-menu/add" component={TrainingClass} />
          <PrivateRoute path="/training-class-menu/:id" component={TrainingClass} />
          <PrivateRoute path="/training-class-menu" component={TrainingClass} />
          <PrivateRoute path="/agent-search-menu/:id/edit" component={Agent} />
          <PrivateRoute path="/agent-search-menu/add" component={Agent} />
          <PrivateRoute path="/agent-search-menu/:id" component={Agent} />
          <PrivateRoute path="/agent-search-menu" component={Agent} />
          <PrivateRoute path="/policy-search-menu/:id" component={DetailPolicy} />
          <PrivateRoute path="/policy-search-menu" component={Policy} />
          <PrivateRoute path="/pending-policy-list" component={PrintPolicy} />
          <PrivateRoute path="/payment-filter" component={Cart} />
          <PrivateRoute path="/payment-history" component={Cart} />
          <PrivateRoute path="/detail-statement/:id" component={Cart} />
          <PrivateRoute path="/checkout/:id" component={Cart} />
          <PrivateRoute path="/status/:id/:slug" component={StatusPage} />

          {/* <PrivateRoute path="/production/:type/create-liability" component={Production} /> */}
          {/* <PrivateRoute path="/production/:type/create-cargo" component={Production} /> */}

          <PrivateRoute path="/report-administrator-agent-list" component={Report} />
          <PrivateRoute path="/report-administrator-agent-contest-winner" component={Report} />
          <PrivateRoute path="/report-administrator-agent-restricted-product" component={Report} />
          <PrivateRoute path="/report-training-list" component={Report} />
          <PrivateRoute path="/report-agent-commission-detail" component={Report} />
          <PrivateRoute path="/report-agent-expired-policy" component={Report} />
          <PrivateRoute path="/report-administrator-approval" component={Report} />
          <PrivateRoute path="/report-production-sppa" component={Report} />
          <PrivateRoute path="/report-production-loss-business" component={Report} />
          <PrivateRoute path="/report-production-list" component={Report} />
          <PrivateRoute path="/report-production-claim-settle" component={Report} />
          <PrivateRoute path="/report-production-outstanding-claim" component={Report} />
          <PrivateRoute path="/report-finance-career" component={Report} />
          <PrivateRoute path="/report-finance-benefit-yearly" component={Report} />
          <PrivateRoute path="/report-finance-benefit-monthly" component={Report} />
          <PrivateRoute path="/report-finance-commission" component={Report} />
          <PrivateRoute exact path="/approval-list-menu" component={ApprovalList} />
          <PrivateRoute exact path="/approval-list-menu/:id/history" component={ApprovalList} />
          <PrivateRoute exact path="/approval-list-menu/:id/approved" component={ApprovalList} />
          <PrivateRoute exact path="/check-secondary-password/:type" component={SecondaryPassword} />
          <PrivateRoute exact path="/check-secondary-password/:type/:type" component={SecondaryPassword} />
          <PrivateRoute path="/setting-menu" component={SettingMenu} />
          {/* <PrivateRoute exact path="/user/add" component={FormUser} />
            <PrivateRoute exact path="/user/:id/edit" component={FormUser} />
            <PrivateRoute exact path="/users" component={User} />
            <PrivateRoute exact path="/product/add" component={FormProduct} />
            <PrivateRoute exact path="/product/:id/edit" component={FormProduct} />
            <PrivateRoute exact path="/products" component={Product} />
            <PrivateRoute exact path="/reports/training/:type" component={ReportTraining} />
            <PrivateRoute exact path="/reports/report-administrator-agent-restricted-product" component={ReportProduct} />
            <PrivateRoute exact path="/reports/contest-winner" component={ReportContest} /> */}

          <PublicRoute component={NotFound} />
        </Switch>
      </Router>

    </PersistGate>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.shape().isRequired,
  persistor: PropTypes.shape().isRequired,
}

PrivateRoute.propTypes = {
  component: PropTypes.any,
}

PublicRoute.propTypes = {
  component: PropTypes.any,
  redirect: PropTypes.bool,
  redirectPath: PropTypes.string,
}

export default Root
