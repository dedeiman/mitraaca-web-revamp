import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, withHandlers, lifecycle, withState,
} from 'recompose'
import {
  forgotPassword, otpPassword,
  authenticateUserFailure, resetPassword,
} from 'actions/Auth'
import {
  startCase, capitalize,
} from 'lodash'
import history from 'utils/history'
import Swal from 'sweetalert2'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import qs from 'query-string'
import ForgotView from 'components/login/Forgot'

const mapDispatchToProps = dispatch => ({
  forgotPassword: bindActionCreators(forgotPassword, dispatch),
  otpPassword: bindActionCreators(otpPassword, dispatch),
  resetPassword: bindActionCreators(resetPassword, dispatch),
  authenticateUserFailure: bindActionCreators(authenticateUserFailure, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'loginForm' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('isAuthenticating', 'setAuthenticating', false),
    withState('isPassword', 'setPassword', 'reset'),
    withState('errorMessage', 'setErrorMessage', ''),
    withState('tokenExpired', 'setTokenExpired', ''),
    withState('isVerifyOtp', 'setVerifyOtp', {
      isLoading: false,
      time: {},
      seconds: 120,
      count: 0,
    }),
    withHandlers({
      secondsToTime: () => (secs) => {
        const hours = Math.floor(secs / (60 * 60))

        const divisorForMinutes = secs % (60 * 60)
        const minutes = Math.floor(divisorForMinutes / 60)

        const divisorForSeconds = divisorForMinutes % 60
        const seconds = Math.ceil(divisorForSeconds)

        const obj = {
          h: hours,
          m: minutes,
          s: seconds,
        }
        return obj
      },
    }),
    withHandlers({
      countDown: props => () => {
        // Remove one second, set state so a re-render happens.
        const seconds = props.isVerifyOtp.seconds - 1
        props.setVerifyOtp({
          ...props.isVerifyOtp,
          time: props.secondsToTime(seconds),
          seconds,
        })

        // Check if we're at zero.
        if (seconds === 0) {
          clearInterval(props.isVerifyOtp.count)
          props.setVerifyOtp({
            isLoading: false,
            time: {
              h: 0,
              m: 0,
              s: 120,
            },
            seconds: 120,
            count: 0,
          })
        }
      },
    }),
    withHandlers({
      onCloseError: props => () => {
        props.setErrorMessage('')
      },
      onChange: props => (e) => {
        const event = e.target || e
        const { name, value } = event

        props.form.setFieldsValue({
          [name]: { value },
        })
      },
      handleResendOTP: props => () => {
        props.getDatas(
          { base: 'apiUser', url: `/passwords/reset/resend-otp/${window.localStorage.getItem('isToken')}`, method: 'post' },
        ).then((res) => {
          if (props.isVerifyOtp.count === 0 && props.isVerifyOtp.seconds > 0) {
            props.setVerifyOtp({ ...props.isVerifyOtp, isLoading: true, count: setInterval(props.countDown, 1000) })
          }
          message.success(res.meta.message)
        }).catch((err) => {
          props.setVerifyOtp({ ...props.isVerifyOtp, isLoading: false })
          message.error(err.message)
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const {
          setAuthenticating, form, isPassword,
          setErrorMessage, location,
        } = props

        form.validateFields((err, values) => {
          if (!err) {
            setAuthenticating(true)
            let payload = values
            if (isPassword === 'reset') {
              payload = {
                ...payload,
                reset_token: qs.parse(location.search).tkn,
                change_password: !qs.parse(location.search).is_secondary || false,
                change_alternative_password: qs.parse(location.search).is_secondary === 'true' || false,
              }
            }
            if (isPassword === 'otp') {
              payload = {
                ...payload,
                token: window.localStorage.getItem('isToken'),
              }
            }
            props[`${isPassword}Password`](payload, values.key)
              .then((res) => {
                setAuthenticating(false)

                if (isPassword === 'forgot') {
                  if (res.is_otp_verification) {
                    window.localStorage.setItem('isToken', res.token)
                    props.getDatas(
                      { base: 'apiUser', url: '/passwords/reset/send-otp', method: 'post' }, {
                        token: res.token,
                        send_via: 'sms',
                      },
                    ).then(() => {
                      Swal.fire(
                        'Link to Reset Password has been sent',
                        `<span style="color:#2b57b7;font-weight:bold">Check your ${res.is_otp_verification ? 'handphone' : 'email'}</span>`,
                        'success',
                      ).then(() => {
                        if (res.is_otp_verification) {
                          history.push('/password/otp')
                        } else {
                          history.push('/')
                        }
                      })
                    }).catch((errors) => {
                      setAuthenticating(false)
                      if (errors.errors && Object.keys(errors.errors).length) {
                        let objError = {}
                        Object.keys(errors.errors).forEach((item) => {
                          objError = {
                            ...objError,
                            [item]: {
                              validateStatus: 'error',
                              help: startCase((errors.errors[item]).replace('_id', '').replace('_', ' ')),
                            },
                          }
                          props.setErrorsField(objError)
                        })
                      } else {
                        Swal.fire(
                          'Error!',
                          `<span style="color:#2b57b7;font-weight:bold">Check your ${errors.message}</span>`,
                          'error',
                        )
                      }
                    })
                  }
                }
              })
              .catch((error) => {
                setAuthenticating(false)
                if (error.errors && Object.keys(error.errors).length) {
                  Object.keys(error.errors).forEach((item) => {
                    setErrorMessage(capitalize((error.errors[item]).replace('_id', '').replace('_', ' ')))
                  })
                } else {
                  setErrorMessage(capitalize(error))
                }
              })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          match, setPassword,
          location, setTokenExpired,
        } = this.props
        this.props.authenticateUserFailure('')
        setPassword(match.params.type)
        if (match.params.type === 'reset') {
          this.props.getDatas(
            { base: 'apiUser', url: '/passwords/token-validation', method: 'post' },
            { token: qs.parse(location.search).tkn },
          ).catch((err) => {
            setTokenExpired(err.message)
          })
        }

        const timeLeftVar = this.props.secondsToTime(this.props.isVerifyOtp.seconds)
        this.props.setVerifyOtp({ ...this.props.isVerifyOtp, time: timeLeftVar })
      },
    }),
  )(ForgotView),
)
