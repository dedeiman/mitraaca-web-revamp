import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Cookies from 'js-cookie'
import config from 'app/config'
import { removeToken } from 'actions/Auth'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { isEmpty } from 'lodash'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import moment from 'moment'
import qs from 'query-string'
import Swal from 'sweetalert2'
import Browser from 'utils/Browser'
import VerifyView from 'components/login/Verify'

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'verifyForm' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('stateLoad', 'setStateLoad', {
      authenticating: false,
      loading: false,
      loadingResend: false,
      otp: false,
    }),
    withState('currentState', 'setCurrentState', {
      agent_id: '',
      birth_date: '',
      phone_number: '',
      secret_key: '',
    }),
    withHandlers({
      getOTP: props => () => {
        props.form.validateFields((err, values) => {
          if (!err) {
            const payload = {
              ...values,
              birth_date: moment(values.birth_date).format('YYYY-MM-DD'),
            }
            delete payload.recaptcha

            props.setStateLoad({ ...props.stateLoad, loading: true })
            props.getDatas(
              { base: 'apiUser', url: '/agents-verify', method: 'post' },
              payload,
            ).then(() => {
              props.setStateLoad({
                ...props.stateLoad,
                loading: false,
                otp: true,
              })
              window.localStorage.setItem('isOTP', false)
              Swal.fire(
                'Link to Agent Verification has been sent',
                '<span style="color:#2b57b7;font-weight:bold">Check your handphone</span>',
                'success',
              )
            }).catch((error) => {
              message.error(error.message)
              props.setStateLoad({ ...props.stateLoad, loading: false })
            })
          }
        })
      },
      resendOTP: props => (agentId, phoneNumber) => {
        props.setStateLoad({ ...props.stateLoad, loadingResend: true })
        props.getDatas(
          { base: 'apiUser', url: '/agents-verify/resend-otp', method: 'post' }, {
            agent_id: agentId,
            phone_number: phoneNumber,
          },
        ).then(() => {
          Swal.fire(
            'Link to Agent Verification has been sent',
            '<span style="color:#2b57b7;font-weight:bold">Check your handphone</span>',
            'success',
          )
          props.setStateLoad({ ...props.stateLoad, loadingResend: false })
        }).catch((error) => {
          message.error(error.message)
          props.setStateLoad({ ...props.stateLoad, loadingResend: false })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { form, setStateLoad, stateLoad } = props

        form.validateFields((err, values) => {
          if (!err) {
            setStateLoad({ ...stateLoad, authenticating: true })
            const payload = {
              phone_number: values.phone_number,
              otp: values.otp,
            }

            props.getDatas(
              { base: 'apiUser', url: '/otp/verify', method: 'post' },
              payload,
            ).then(() => {
              props.setStateLoad({
                ...props.stateLoad,
                authenticating: false,
              })
              Swal.fire(
                'Your account is verified',
                '<span style="color:#2b57b7;font-weight:bold">go to login page</span>',
                'success',
              )
                .then(() => Browser.setWindowHref('/'))
            }).catch((error) => {
              message.error(error.message)
              setStateLoad({ ...stateLoad, authenticating: false })
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const { search } = this.props.location
        const token = qs.parse(search).tkn || {}
        removeToken()
        window.localStorage.clear()
        if (!isEmpty(Cookies.get(config.auth_cookie_name))) {
          removeToken()
          window.localStorage.setItem('isOTP', false)
        }

        if (!isEmpty(token)) {
          this.props.getDatas(
            { base: 'apiUser', url: `/agents-verify/${token}`, method: 'get' },
          ).then((res) => {
            this.props.setCurrentState({ ...res.data })
          }).catch((error) => {
            message.error(error.message)
          })
        }
      },
    }),
  )(VerifyView),
)
