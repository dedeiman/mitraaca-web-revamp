import { connect } from 'react-redux'
import { isEmpty } from 'lodash'
import { bindActionCreators } from 'redux'
import { compose, withHandlers, lifecycle } from 'recompose'
import { authenticateByCredentials, authenticateUserFailure } from 'actions/Auth'
import { Form } from '@ant-design/compatible'
import LoginView from 'components/login/Login'

export function mapStateToProps(state) {
  const {
    isAuthenticating,
    errorMessage,
  } = isEmpty(state.root.auth)
    ? { isAuthenticating: false, errorMessage: null }
    : state.root.auth

  return {
    isAuthenticating,
    errorMessage,
  }
}

const mapDispatchToProps = dispatch => ({
  loginWithCredentials: bindActionCreators(authenticateByCredentials, dispatch),
  authenticateUserFailure: bindActionCreators(authenticateUserFailure, dispatch),
})

export default Form.create({ name: 'loginForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withHandlers({
      onCloseError: props => () => {
        props.authenticateUserFailure('')
      },
      onChange: props => (e) => {
        const event = e.target || e
        const { name, value } = event

        props.form.setFieldsValue({
          [name]: { value },
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        const { form, loginWithCredentials } = props

        form.validateFields((err, values) => {
          if (!err) {
            loginWithCredentials(values)
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.authenticateUserFailure('')
      },
    }),
  )(LoginView),
)
