import PropTypes from 'prop-types'
import {
  Layout, Result,
  Card, Button,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Sidebar } from '../elements'
import HelpCenter from '../../containers/pages/helpCenter'

const { Content } = Layout

const PrivateLayout = ({
  children, doLogout,
  handleKeys, site, currentUser,
  isCollapsed, toggleCollapsed,
  avatarError, imgAvatar, groupRole,
}) => (
  <div className="app main">
    <div id="yield" className="app-main-body">
      <Layout>
        <Layout>
          { site.isActiveSidebar && (
            <Sidebar
              site={site}
              role={groupRole}
              user={currentUser || {}}
              logout={doLogout}
              isCollapsed={isCollapsed}
              toggleCollapsed={toggleCollapsed}
              handleKeys={(param, isOpenSub) => handleKeys(param, isOpenSub)}
              avatarError={avatarError}
              imgAvatar={imgAvatar}
            />
          )}

          <Layout className="main-content" style={site.isActiveSidebar ? { padding: '0 24px 24px' } : { padding: '0' }}>
            <Content
              className="site-layout-background"
              style={site.isActiveSidebar ? {
                padding: '24px 0',
                margin: 0,
                minHeight: 280,
                background: '/assets/city-bg.svg',
              } : {
                padding: '0',
                margin: 0,
                minHeight: 280,
                background: '/assets/city-bg.svg',
              }}
            >
              {(!window.location.href.includes('/profile/change#first') && Helper.loginValidations(currentUser.validations))
                ? (
                  <Card>
                    <Result
                      status="403"
                      title="Tidak Memiliki Akses"
                      subTitle="Maaf, anda harus mengubah password terlebih dahulu."
                      extra={<Button type="primary" onClick={() => history.replace('/profile/change#first')}>Ubah Password</Button>}
                    />
                  </Card>
                )
                : (
                  <>
                    {children}
                    <HelpCenter />
                  </>
                )
              }
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </div>
  </div>
)

PrivateLayout.propTypes = {
  children: PropTypes.node,
  doLogout: PropTypes.func,
  handleKeys: PropTypes.func,
  site: PropTypes.object,
  currentUser: PropTypes.object,
  isCollapsed: PropTypes.bool,
  toggleCollapsed: PropTypes.func,
  imgAvatar: PropTypes.string,
  avatarError: PropTypes.func,
  groupRole: PropTypes.object,
}

export default PrivateLayout
