/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _ from 'lodash'
import moment from 'moment'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'

const PreviewTravelDomestic = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  let travelDestinationText = '-'
  const travelDestinationList = data.stateSelects ? data.stateSelects.travelDestinationList : []

  if (data.travel_destination_id && data.stateSelects.travelDestinationList.length) {
    const travelDestination = travelDestinationList.find(o => o.id === data.travel_destination_id)

    if (travelDestination) {
      travelDestinationText = travelDestination.name
    }
  }

  let relationshipText = '-'
  const relationshipList = data.stateSelects ? data.stateSelects.relationshipList : []

  if (data.relationship_id && data.stateSelects.relationshipList.length) {
    const relationship = relationshipList.find(o => o.id === data.relationship_id)

    if (relationship) {
      relationshipText = relationship.name
    }
  }

  let domestikCityText = '-'
  const domestikCityList = data.stateSelects ? data.stateSelects.domestikCityList : []

  if (data.destination_city_id && data.stateSelects.domestikCityList.length) {
    const domestikCity = domestikCityList.find(o => o.id === data.destination_city_id)

    if (domestikCity) {
      domestikCityText = domestikCity.name
    }
  }

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      wrapClassName="wrap-modal-preview"
      className="modal-add-customer"
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">SPPA TRAVEL SAFE DOMESTIC</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nomor Penawaran</b></p>
                    <p className="mb-1"><i>Auto Generate</i></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nama</b></p>
                    <p className="mb-1">{_.get(data, 'name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Email</b></p>
                    <p className="mb-1">{_.get(data, 'email', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>No Handphone</b></p>
                    <p className="mb-1">{_.get(data, 'phone_number', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Tanggal Lahir</b></p>
                    <p className="mb-1">{_.get(data, 'insured_dob') ? moment(data.insured_dob).format('DD MMM YYYY') : '-'}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Produk</b></p>
                    <p>
                      {_.get(data, 'productDetail.type.name')}
                      &nbsp;-&nbsp;
                      {_.get(data, 'productDetail.class_of_business.name')}
                      &nbsp;-&nbsp;
                      {_.get(data, 'productDetail.display_name')}
                    </p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Tujuan dan Ahli Waris</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Tujuan Perjalanan</b></p>
                    <p>{travelDestinationText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kota Asal (Domestic)</b></p>
                    <p className="mb-1">{domestikCityText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Ahli Waris</b></p>
                    <p className="mb-1">{_.get(data, 'heir_name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1">Hubungan</p>
                    <p>{relationshipText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1">Tanggal keberangkatan</p>
                    <p>{data.departure_date ? moment(data.departure_date).format('DD MMM YYYY') : ''}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1">Tanggal Kembali</p>
                    <p>{data.return_date ? moment(data.return_date).format('DD MMM YYYY') : ''}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Perhitungan Premi / Kontribusi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Jenis Plan</p>
                    <p>{data.plan_type}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Jumlah Hari yang Dipertanggungkan ({data.premi.total_days_insured})</p>
                    <p>
                      <b>
                        {Helper.currency(data.premi.price_days_insured, 'Rp. ', ',-')}
                      </b>
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Tambahan Per Minggu ({data.premi.total_additional_week})</p>
                    <p>
                      <b>
                        {Helper.currency(data.premi.price_additional_week, 'Rp. ', ',-')}
                      </b>
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Total Premi</p>
                    <p>
                      <b>
                        {Helper.currency(data.premi.value, 'Rp. ', ',-')}
                      </b>
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1">Diskon</p>
                    <p>{`${data.premi.discount_percentage}%`}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1">&nbsp;</p>
                    <p>{Helper.currency(data.premi.discount_amount, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Biaya Administrasi</p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item style={{ marginBottom: '0' }}>
                      <Checkbox checked={data.print_policy_book === true}>
                          Buku Polis
                      </Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={6}>
                      <p className="mt-2 mb-1">Rp 50.000,-</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Materai</p>
                  </Col>
                  <Col xs={24} md={8} />
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2">
                      <b>{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</b>
                    </p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Total Pembayaran</p>
                  </Col>
                  <Col xs={24} md={8} />
                  <Col xs={24} md={6}>
                    <p
                      className="mb-1 mt-2"
                      style={{ fontSize: '18px' }}
                    >
                      <b>
                        {Helper.currency(data.paymentTotal, 'Rp. ', ',-')}
                      </b>
                    </p>
                  </Col>
                </Row>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Form.Item>
                      <Checkbox checked={data.aggree}>
                        Saya telah membaca dan setuju dengan
                        &nbsp;
                        <b>Syarat & Ketentuan Mitraca</b>
                        &nbsp;
                        yang berlaku
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 border-lg"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewTravelDomestic.propTypes = {
  data: PropTypes.any,
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
}

export default PreviewTravelDomestic
