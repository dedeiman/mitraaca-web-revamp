import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Checkbox,
  Button, Tag,
  Descriptions,
} from 'antd'
import moment from 'moment'
import { Form } from '@ant-design/compatible'
import history from 'utils/history'
import Helper from 'utils/Helper'
import _, { isEmpty } from 'lodash'
import { LeftOutlined } from '@ant-design/icons'

const DetailTravel = ({
  detailData, documentData,
}) => {
  // eslint-disable-next-line no-unused-vars
  const data = detailData.list

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail SPPA ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tipe SPPA" className="px-1 profile-detail pb-0">
                        {data.sppa_type.toUpperCase()}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Indicative Offer/Renewal" className="px-1 profile-detail pb-0">
                        {!isEmpty(data.indicative_offer) ? data.indicative_offer.io_number : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Pemegang Polis" className="px-1 profile-detail pb-0">
                        {!isEmpty(data.policy_holder) ? data.policy_holder.name.toUpperCase() : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                        {!isEmpty(data.insured) ? data.insured.name.toUpperCase() : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                        {data.name_on_policy}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Dokumen Pendukung</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    {!documentData.loading && documentData.list.map(item => (
                      <p className="mb-1">
                        <a
                          className="link-download ml-1"
                          href={item.file}
                          download
                        >
                          { item.description || item.file }
                        </a>
                      </p>
                    ))}
                    {!documentData.loading && !documentData.list.length > 0 && (
                      <p className="mb-1">Tidak ada dokumen</p>
                    )}
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Tujuan dan Ahli Waris</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tujuan Perjalanan" className="px-1 profile-detail pb-0">
                        {data.travel_information.travel_destination.name.toUpperCase() || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kota Tujuan (Domestic)" className="px-1 profile-detail pb-0">
                        {data.travel_information.destination_city.name.toUpperCase() || ''}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Ahli Waris" className="px-1 profile-detail pb-0">
                        {data.travel_information.heir_name || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Hubungan" className="px-1 profile-detail pb-0">
                        {data.travel_information.relationship.name.toUpperCase() || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tanggal keberangkatan" className="px-1 profile-detail pb-0">
                        {moment(data.travel_information.departure_date).format('DD MMM YYYY')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  {/* <Col xs={24} md={12}>
                    <p className="mb-1">&nbsp;</p>
                    <Checkbox
                      checked={data.travel_information.is_annual}
                    >
                      Annual
                    </Checkbox>
                  </Col> */}
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tanggal Kembali" className="px-1 profile-detail pb-0">
                        {moment(data.travel_information.return_date).format('DD MMM YYYY')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Jenis Plan" className="px-1 profile-detail pb-0">
                            {(data.travel_information.plan_type || '-').toUpperCase()}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <div className="px-1">
                          <p className="mb-2 ant-descriptions-item-label">
                            {`Jumlah Hari yang Dipertanggungkan (${0})`}
                          </p>
                          <p className="text-dark fw-bold">
                            {`${data.travel_premi_calculation.total_days_insured} Days` || '0 Days'}
                          </p>
                        </div>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <div className="px-1">
                          <p className="mb-2 ant-descriptions-item-label">
                            {`Tambahan Per Minggu (${_.get(data, 'travel_premi_calculation.total_additional_week', 0)})`}
                          </p>
                          <p className="text-dark fw-bold">
                            {Helper.currency(data.travel_premi_calculation.price_additional_week, 'Rp. ', ',-')}
                          </p>
                        </div>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Total Premi" className="px-1 profile-detail pb-0">
                            {Helper.currency(data.travel_premi_calculation.premi, 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                            {data.travel_premi_calculation.discount_percentage}
                            {' '}
                            %
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                            {Helper.currency(data.travel_premi_calculation.discount_amount, 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${data.status}/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" disabled={data.status === 'complete' || data.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailTravel.propTypes = {
  detailData: PropTypes.object,
  documentData: PropTypes.object,
}

export default DetailTravel
