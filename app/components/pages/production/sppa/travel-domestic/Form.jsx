/* eslint-disable no-shadow */
/* eslint-disable react/jsx-props-no-multi-spaces */
/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Input,
  Checkbox,
  Button,
  DatePicker,
  Select,
  Radio,
  AutoComplete,
  InputNumber,
} from 'antd'
import _, { isEmpty } from 'lodash'
import moment from 'moment'
import Hotkeys from 'react-hot-keys'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'
import queryString from 'query-string'
import NumberFormat from 'react-number-format'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import PreviewTravel from './Preview'
import config from 'app/config'

const FormTravelDomestic = ({
  form,
  detailData,
  submitted,
  productDetail,
  stateSelects,
  submitPreview,
  submitData,
  visiblePreview,
  setVisiblePreview,
  visibleDiscount,
  setVisibleDiscount,
  premi,
  getPremi,
  appendCustomer,
  bookPrice,
  handleRenewal,
  listIO,
  listCustomer,
  match,
  setSelectedHolder,
  setSelectedInsured,
  setSelectedIO,
  selectedHolder,
  selectedInsured,
  fetchDetailCustomer,
  calcDiscountByPercentage,
  calcDiscountByCurrency,
  calcPaymentTotal,
  paymentTotal,
  setFieldByIO,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
  } = form
  const data = detailData.list
  const parsed = queryString.parse(window.location.search)
  let sppaType

  if (!isEmpty(data)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = data.sppa_type
    }
  }

  return (
    <>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <PreviewTravel
        show={visiblePreview}
        setVisible={setVisiblePreview}
        submitted={submitted}
        data={{
          ...getFieldsValue(),
          stateSelects,
          productDetail,
          premi,
          paymentTotal,
        }}
        handleCancel={() => setVisiblePreview(false)}
        handleSubmit={submitData}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Create'} SPPA Travel Safe Domestic`}</div>
        </Col>
      </Row>
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">SPPA Travel Safe Domestic</p>
                  <Row gutter={24}>
                    <Col span={8}>
                      <p className="mb-1">Tipe SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tipe SPPA'),
                          ],
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Type SPPA"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={16}>
                      <p className="mb-1">Indicative Offer/Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_number', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            placeholder="IO Number"
                            className="field-lg w-100"
                            dataSource={(listIO.data).map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}

                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listIO.data).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listIO.data).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }
                            disabled={getFieldValue('sppa_type') === undefined}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Pemegang Polis</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('policy_holder_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.policy_holder.name : undefined || undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Pemegang polis'),
                            {
                              validator: (rule, value) => {
                                const arrayPolicy = []
                                listCustomer.options.map(item => arrayPolicy.push(item.name))

                                if (!value) return Promise.resolve()

                                if (!arrayPolicy.includes(value)) return Promise.reject('*Nama Pemegang Polis tidak terdaftar')

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <AutoComplete
                            placeholder="Pemegang Polis"
                            className="field-lg w-100"
                            dataSource={(listCustomer.options).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              // eslint-disable-next-line no-shadow
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName}`,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_name: data.name,
                                })
                              }

                              getPremi()
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Tertanggung</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('insured_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.insured.name.toUpperCase() : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Nama Tertanggung'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Nama Tertanggung"
                            dataSource={(listCustomer.options || []).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              // eslint-disable-next-line no-shadow
                              const data = (listCustomer.options).find(item => item.id === val)
                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_name').toUpperCase(),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${getFieldValue('policy_holder_name')} QQ ${data.name.toUpperCase()}`,
                                  })
                                }

                                setFieldsValue({
                                  insured_name: data.name.toUpperCase(),
                                })
                              }
                              getPremi()

                              fetchDetailCustomer(val)

                              setTimeout(() => {
                                setFieldsValue({
                                  insured_dob: moment(data.dob),
                                })
                              }, 1000)
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Pada Polis</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: _.get(detailData, 'list.name_on_policy', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nama pada polis'),
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled
                            placeholder="Nama Pada Polis"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Produk</p>
                      <p>
                        <b>
                          {_.get(productDetail, 'type.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'class_of_business.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'display_name')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tanggal Lahir</p>
                      <Form.Item>
                        {getFieldDecorator('insured_dob', {
                          initialValue: !isEmpty(data) ? moment(data.insured.dob) : undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tanggal lahir'),
                            {
                              type: 'number', max: 70, transform: value => moment().diff(moment(value), 'years'), message: 'Umur maximal 70 tahun.',
                            },
                          ],
                        })(
                          <DatePicker
                            disabled
                            placeholder="- Tanggal Lahir -"
                            size="large"
                            className="w-100"
                            defaultPickerValue={moment().subtract(17, 'year')}
                            disabledDate={current => current && current > moment().endOf('day').subtract(1, 'days')}
                            format="DD MMMM YYYY"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Tujuan dan Ahli Waris</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tujuan Perjalanan</p>
                      <Form.Item>
                        {getFieldDecorator('travel_destination_id',
                          {
                            initialValue: !isEmpty(data) ? data.travel_information.travel_destination.id : undefined,
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tanggal perjalanan'),
                            ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Tujuan Perjalanan"
                              loading={stateSelects.travelDestinationLoad}
                            >
                              {(stateSelects.travelDestinationList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kota Asal (Domestic)</p>
                      <Form.Item>
                        {getFieldDecorator('destination_city_id',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.destination_city.id', undefined),
                            // rules: [
                            //   ...Helper.fieldRules(['required'], 'Tujuan'),
                            // ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Kota Asal"
                              loading={stateSelects.domestikCityLoad}
                            >
                              {(stateSelects.domestikCityList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Ahli Waris</p>
                      <Form.Item>
                        {getFieldDecorator('heir_name',
                          {
                            initialValue: !isEmpty(data) ? data.travel_information.heir_name.toUpperCase() : '',
                            rules: [
                              ...Helper.fieldRules(['required'], 'Ahli waris'),
                            ],
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              placeholder="Input Ahli Waris"
                              className="field-lg uppercase"
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Hubungan</p>
                      <Form.Item>
                        {getFieldDecorator('relationship_id',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.relationship.id', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tujuan'),
                            ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Hubungan"
                              loading={stateSelects.relationshipLoad}
                            >
                              {(stateSelects.relationshipList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Tanggal Keberangkatan</p>
                      <Form.Item>
                        {getFieldDecorator('departure_date',
                          {
                            if(){

                            },
                            initialValue: !isEmpty(data) ? moment(data.travel_information.departure_date) : undefined,
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tanggal keberangkatan'),
                              {
                                validator: (rule, value) => {
                                  const insuredDob = getFieldValue('insured_dob')

                                  if ((moment().diff(moment(insuredDob), 'years') - moment().diff(moment(value), 'years')) < 1) {
                                    return Promise.reject('*Minimal umur 1 Tahun dari Tanggal keberangkatan')
                                  }

                                  if ((moment().diff(moment(insuredDob), 'years') - moment().diff(moment(value), 'years')) > 70) {
                                    return Promise.reject('*Maximal umur 70 Tahun dari Tanggal keberangkatan')
                                  }
                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                            <DatePicker
                              placeholder="Input Tanggal keberangkatan"
                              size="large"
                              className="w-100"
                              format="DD MMMM YYYY"
                              disabledDate={current => current && current < moment().endOf('day').subtract(1, 'days')}
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Tanggal Kembali</p>
                      <Form.Item>
                        {getFieldDecorator('return_date',
                          {
                            initialValue: !isEmpty(data) ? moment(data.travel_information.return_date) : undefined,
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tanggal kembali'),
                            ],
                          })(
                            <DatePicker
                              placeholder="Input Tanggal Kembali"
                              size="large"
                              className="w-100"
                              format="DD MMMM YYYY"
                              disabledDate={current =>
                                // current && getFieldValue('departure_date') && current < getFieldValue('departure_date')
                                current && current < getFieldValue('departure_date') || current.diff(moment().add(88), 'day') > 88
                              }
                              onChange={() => getPremi()}
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Premi / Kontribusi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Jenis Plan</p>
                      <Form.Item>
                        {getFieldDecorator('plan_type', {
                          initialValue: !isEmpty(data) ? data.travel_information.plan_type : '',
                          rules: [
                            ...Helper.fieldRules(['required'], 'Jenis plan'),
                          ],
                        })(
                          <Radio.Group
                            options={[
                              { label: 'Nusantara 1', value: 'nusantara-1' },
                              { label: 'Nusantara 2', value: 'nusantara-2' },
                            ]}
                            optionType="button"
                            onChange={() => getPremi()}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Jumlah Hari yang Dipertanggungkan ({premi.total_days_insured})</p>
                      <p>
                        <b>
                          {Helper.currency(premi.price_days_insured, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tambahan Per Minggu ({premi.total_additional_week})</p>
                      <p>
                        <b>
                          {Helper.currency(premi.price_additional_week, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Total Premi</p>
                      <p>
                        <b>
                          {Helper.currency(premi.premi, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(data, 'travel_premi_calculation.discount_percentage', 0),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            suffix="%"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_amount', {
                          initialValue: parseFloat(premi.discount_amount).toFixed(2) || 0,

                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book',
                          {
                            initialValue: !isEmpty(data) ? data.print_policy_book : false,
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                print_policy_book: e.target.value,
                              })

                              return e.target.checked
                            },
                          })(
                            <Checkbox
                              checked={getFieldValue('print_policy_book')}
                              onChange={() => setTimeout(() => getPremi())}
                            >
                              Buku Polis
                            </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Materai</p>
                    </Col>
                    <Col xs={24} md={8} />
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={6} />
                    <Col xs={24} md={8}>
                      <p
                        className="mb-1 mt-2"
                        style={{ fontSize: '18px' }}
                      >
                        <b>
                          {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <br />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: Helper.fieldRules(['required'], 'Syarat & ketentuan'),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              aggree: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox checked={getFieldValue('aggree')}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <u>
                              <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                <b>Syarat & Ketentuan Mitraca</b>
                              </a>
                            </u>
                            &nbsp;
                            yang berlaku
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24}>
                <Button
                  ghost
                  type="primary"
                  disabled={submitted}
                  className="button-lg w-50 border-lg"
                  onClick={() => submitData()}
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </>
  )
}

FormTravelDomestic.propTypes = {
  form: PropTypes.object,
  productDetail: PropTypes.object,
  stateSelects: PropTypes.object,
  submitPreview: PropTypes.func,
  submitData: PropTypes.func,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.bool,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  premi: PropTypes.object,
  getPremi: PropTypes.func,
  bookPrice: PropTypes.number,
  handleRenewal: PropTypes.func,
  match: PropTypes.object,
  listIO: PropTypes.object,
  listCustomer: PropTypes.object,
  setSelectedHolder: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  appendCustomer: PropTypes.func,
  setSelectedIO: PropTypes.func,
  selectedHolder: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  selectedInsured: PropTypes.func,
  detailData: PropTypes.object,
  fetchDetailCustomer: PropTypes.func,
  calcPaymentTotal: PropTypes.func,
  paymentTotal: PropTypes.any,
  setFieldByIO: PropTypes.func,
}

export default FormTravelDomestic
