import React from 'react'
import PropTypes from 'prop-types'
import {
  Descriptions,
} from 'antd'
import Helper from 'utils/Helper'
import { handleProduct } from 'constants/ActionTypes'
import SignatureCanvas from 'react-signature-canvas'

const ConfirmTravelDomestic = ({
  detailSPPA,
  signature,
}) => {
  // eslint-disable-next-line no-unused-vars
  const isMobile = window.innerWidth < 768
  return (
    <Descriptions layout="vertical" colon={false} column={4}>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Tipe SPPA" className="px-1 profile-detail pb-0">
        {detailSPPA.sppa_type.toUpperCase() || '-'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Pemegang Polis" className="px-1 profile-detail pb-0">
        {detailSPPA.policy_holder.name.toUpperCase() || '-'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Nama tertanggung" className="px-1 profile-detail pb-0">
        {detailSPPA.insured.name.toUpperCase() || '-'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Nama pada Polis" className="px-1 profile-detail pb-0">
        {detailSPPA.name_on_policy.toUpperCase() || '-'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Produk" className="px-1 profile-detail pb-0">
        {detailSPPA.product.type.name || '-'}
        {' '}
        -
        {handleProduct(detailSPPA.product.code) || '-'}
        {' '}
        -
        {detailSPPA.product.display_name || '-'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label={`Jumlah Hari yang Dipertanggungkan (${detailSPPA.travel_premi_calculation.total_days_insured})`} className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.travel_premi_calculation.price_days_insured, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label={`Tambahan per minggu (${detailSPPA.travel_premi_calculation.total_additional_week})`} className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.travel_premi_calculation.price_additional_week, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      {detailSPPA.product.type.name === 'Konvensional'
      && (
        <Descriptions.Item span={isMobile ? 1 : 2} label="Premi" className="px-1 profile-detail pb-0">
          {Helper.currency(detailSPPA.travel_premi_calculation.premi, 'Rp. ', ',-') || '0'}
        </Descriptions.Item>
      )
      }
      {detailSPPA.product.type.name === 'Syariah'
      && (
        <Descriptions.Item span={isMobile ? 1 : 2} label="Kontribusi" className="px-1 profile-detail pb-0">
          {Helper.currency(detailSPPA.travel_premi_calculation.premi, 'Rp. ', ',-') || '0'}
        </Descriptions.Item>
      )
      }
      <Descriptions.Item span={isMobile ? 1 : 2} label="Discount Percentage" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.travel_premi_calculation.discount_percentage) || '0'}%
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Diskon Currency" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.travel_premi_calculation.discount_amount, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Buku Polis" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.policy_printing_fee, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      <Descriptions.Item span={isMobile ? 1 : 2} label="Materai" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.stamp_fee, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      <Descriptions.Item span={4} label="Total Pembayaran" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.total_payment, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
      {detailSPPA.product.type.name === 'Konvensional'
        && (
        <Descriptions.Item span={4} label="Pernyataan Tertanggung" className="px-1 profile-detail pb-0">
          <ol>
            <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
            <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh penanggung</li>
            <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari penanggung.</li>
            <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT. Asuransi Central Asia melalui Virtual Account yang tercantum pada nota premi.</li>
          </ol>
        </Descriptions.Item>
        )
      }
      {detailSPPA.product.type.name === 'Syariah'
        && (
          <Descriptions.Item span={4} label="Pernyataan Peserta" className="px-1 profile-detail pb-0">
            <ol>
              <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
              <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh pengelola.</li>
              <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari pengelola.</li>
              <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT. Asuransi Central Asia melalui Virtual Account yang tercantum pada nota premi.</li>
            </ol>
          </Descriptions.Item>
        )
      }
      <Descriptions.Item span={4} label="Tanda Tangan Tertanggung" className="px-1 profile-detail pb-0 text-center">
        <SignatureCanvas
          penColor="black"
          canvasProps={{ width: 300, height: 300, className: 'sigCanvas' }}
          /*eslint-disable */
          ref={ref => signature.data = ref}
          /* eslint-enable */
        />
      </Descriptions.Item>
    </Descriptions>
  )
}

ConfirmTravelDomestic.propTypes = {
  detailSPPA: PropTypes.object,
  signature: PropTypes.object,
  handleSignatureSave: PropTypes.func,
  handleBack: PropTypes.func,
}

export default ConfirmTravelDomestic
