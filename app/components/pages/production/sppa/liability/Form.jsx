/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Input,
  Select,
  Checkbox,
  Button,
  DatePicker,
  AutoComplete,
  Radio,
  InputNumber,
} from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import Helper from 'utils/Helper'
import Hotkeys from 'react-hot-keys'
import { Form } from '@ant-design/compatible'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import moment from 'moment'
import _, { isEmpty, capitalize } from 'lodash'
import NumberFormat from 'react-number-format'
import { handleProduct } from 'constants/ActionTypes'
import queryString from 'query-string'
import Preview from './Preview'
import config from 'app/config'

const FormLiability = ({
  form, onSubmit,
  setPremi,
  premi,
  visibleDiscount,
  setVisibleDiscount,
  listIO,
  handleRenewal,
  detailLiability,
  listCustomer,
  setSelectedHolder,
  selectedInsured,
  setSelectedInsured,
  selectedHolder,
  appendCustomer,
  loadCreateSppa,
  match,
  getPremi,
  setFieldByIO,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)
  const data = detailLiability.list
  let sppaType

  if (!isEmpty(data)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = data.sppa_type
    }
  }

  return (
    <React.Fragment>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">
            {`${match.params.id ? 'Edit' : 'Create'} SPPA Liability`}
          </div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Informasi Polis</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tipe Polis</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: Helper.fieldRules(['required'], 'Tipe polis'),
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Pilih Tipe"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Indicative Offer / Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_renewal', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            placeholder="Input Indicative Offer / Renewal"
                            className="field-lg w-100"
                            dataSource={listIO.options.map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}
                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listIO.options).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listIO.options).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }                        
                            disabled={getFieldValue('sppa_type') === undefined}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">NO SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_number')(
                          <Input
                            disabled
                            placeholder="Auto Generated By System"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Pemegang Polis</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('policy_holder_id', {
                          initialValue: !isEmpty(data) ? data.policy_holder.name.toUpperCase() : '',
                          rules: [
                            ...Helper.fieldRules(['required'], 'Pemegang polis'),
                            {
                              validator: (rule, value) => {
                                const arrayPolicy = []
                                listCustomer.options.map(item => arrayPolicy.push(item.name))

                                if (!value) return Promise.resolve()

                                if (!arrayPolicy.includes(value)) return Promise.reject('*Nama Pemegang Polis tidak terdaftar')

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <AutoComplete
                            className="field-lg w-100 uppercase-auto-complite"
                            placeholder="Pemegang Polis"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              // eslint-disable-next-line no-shadow
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_id')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name.toUpperCase(),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName}`,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_id: data.name.toUpperCase(),
                                })
                              }
                            }}
                            onChange={() => setTimeout(() => getPremi())}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Tertanggung</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('insured_id', {
                          initialValue: !isEmpty(data) ? data.insured.name.toUpperCase() : '' || '',
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nama tertanggung'),
                            {
                              validator: (rule, value) => {
                                const arrayPolicy = []
                                listCustomer.options.map(item => arrayPolicy.push(item.name))

                                if (!value) return Promise.resolve()

                                if (!arrayPolicy.includes(value)) return Promise.reject('*Nama Tertanggung tidak terdaftar')

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <AutoComplete
                            className="field-lg w-100 uppercase-auto-complite"
                            placeholder="Nama Tertanggung"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              // eslint-disable-next-line no-shadow
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_id'),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${getFieldValue('policy_holder_id')} QQ ${data.name}`,
                                  })
                                }

                                setFieldsValue({
                                  insured_id: data.name.toUpperCase(),
                                })
                              }
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Pada Polis</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: !isEmpty(data) ? data.name_on_policy.toUpperCase() : '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Insured Name"
                            className="field-lg uppercase"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Product</p>
                      <p>
                        {match.params.id
                          ? <p>{`${!isEmpty(detailLiability.list) ? detailLiability.list.product.type.name : '-'} - Liability - ${!isEmpty(detailLiability.list) ? detailLiability.list.product.display_name : '-'}`}</p>
                          : <p>{`${capitalize(parsed.type)} - ${handleProduct(parsed.product)} - Director Personal Liability (D & O)`}</p>
                        }
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Awal</p>
                      <Form.Item>
                        {getFieldDecorator('start_period', {
                          initialValue: !isEmpty(data) ? moment(new Date(data.liability_calculation.start_period), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['required'], 'Periode awal'),
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            allowClear={false}
                            format="DD MMMM YYYY"
                            disabledDate={current =>
                              // eslint-disable-next-line no-mixed-operators,implicit-arrow-linebreak
                              current && current < moment().endOf('day') || current.diff(moment().add(7), 'day') > 6
                            }
                            onChange={(date) => {
                              setFieldsValue({
                                end_period: moment(new Date(date), 'DD MMMM YYYY').add(1, 'year'),
                              })
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Akhir</p>
                      <Form.Item>
                        {getFieldDecorator('end_period', {
                          initialValue: !isEmpty(data) ? moment(new Date(data.liability_calculation.end_period), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['required'], 'Periode akhir'),
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            allowClear={false}
                            disabledDate={current => (current && (current < moment(getFieldValue('start_period')).subtract(0, 'day').add(1, 'year')))}
                            format="DD MMMM YYYY"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Premi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Limit Liability Polis Pertahun</p>
                      <Form.Item>
                        {getFieldDecorator('coverage_limit', {
                          initialValue: premi.limit_policy_amount,
                          rules: Helper.fieldRules(['required'], 'Limit liability polis pertahun'),
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Premi</p>
                      <Form.Item>
                        {getFieldDecorator('premi', {
                          initialValue: premi.premi,
                          rules: Helper.fieldRules(['required'], 'Premi'),
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-0">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(data, 'liability_calculation.discount_percentage', 0),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            suffix="%"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_currency', {
                          initialValue: parseFloat(premi.discount_currency).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book', {
                          initialValue: _.get(data, 'liability_calculation.print_policy_book', undefined),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              print_policy_book: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                            onChange={() => setTimeout(() => getPremi())}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={7}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9} />
                    <Col xs={24} md={8}>
                      <p>Materai</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={17}>
                      <p>Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>
                        {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                      </p>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Pernyataan Tertanggung</p>
                  <Row gutter={24} />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakah saat ini Anda sedang ada proses hukum yang sedang berjalan ?</p>
                      <div className="mb-1">
                        <Form.Item>
                          {getFieldDecorator('is_legal_processdings', {
                            // eslint-disable-next-line no-nested-ternary
                            initialValue: isEmpty(data) ? false : data.liability_statement_insured.is_legal_processdings,
                            rules: [],
                          })(
                            <Radio.Group
                              onChange={(e) => {
                                if (!e.target.value) {
                                  setFieldsValue({
                                    legal_proceedings_date: undefined,
                                    legal_proceedings_description: '',
                                  })
                                }
                              }}
                            >
                              <Radio value>Ya</Radio>
                              <Radio value={false}>Tidak</Radio>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </div>
                      <p className="mb-1">Jika ya, tanggal proses hukum dimulai.</p>
                      <Form.Item>
                        {getFieldDecorator('legal_proceedings_date', {
                          // eslint-disable-next-line no-nested-ternary
                          initialValue: !isEmpty(data) ? !isEmpty(data.liability_statement_insured.legal_proceedings_date) ? moment(data.liability_statement_insured.legal_proceedings_date) : data.liability_statement_insured.legal_proceedings_date : undefined,
                          rules: [
                            ...Helper.fieldRules(getFieldValue('is_legal_processdings') === false ? [] : ['required']),
                          ],
                        })(
                          <DatePicker
                            size="large"
                            className="w-100"
                            format="DD MMMM YYYY"
                            onChange={date => date}
                            placeholder="Tanggal Mulai Proses Hukum"
                            disabled={getFieldValue('is_legal_processdings') === false}
                          />,
                        )}
                      </Form.Item>
                      <p className="mb-1">Jika ada, jelaskan proses hukum apa.</p>
                      <Form.Item>
                        {getFieldDecorator('legal_proceedings_description', {
                          // eslint-disable-next-line no-nested-ternary
                          initialValue: !isEmpty(data) ? data.liability_statement_insured.legal_proceedings_description.toUpperCase() : '-',
                          rules: getFieldValue('healthy_question_1_answer') ? Helper.fieldRules(['required']) : [],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input.TextArea
                            disabled={getFieldValue('is_legal_processdings') === false}
                            placeholder="Jawaban"
                            className="field-lg uppercase"
                            rows={5}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: Helper.fieldRules(['required'], 'Syarat & ketentuan'),
                        })(
                          <Checkbox.Group>
                            <Checkbox value="aggree">
                              Saya telah membaca dan setuju dengan
                              &nbsp;
                              <u>
                                <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                  <b>Syarat & Ketentuan Mitraca</b>
                                </a>
                              </u>
                              &nbsp;
                              yang berlaku
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24}>
                <Button
                  ghost
                  type="primary"
                  htmlType="submit"
                  disabled={loadCreateSppa}
                  className="button-lg w-50 border-lg"
                >
                  {loadCreateSppa && <LoadingOutlined className="mr-2" />}
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormLiability.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  setPremi: PropTypes.func,
  premi: PropTypes.object,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  // eslint-disable-next-line react/no-unused-prop-types
  handleProduct: PropTypes.func,
  listIO: PropTypes.object,
  handleRenewal: PropTypes.func,
  detailLiability: PropTypes.object,
  listCustomer: PropTypes.object,
  setSelectedHolder: PropTypes.func,
  selectedInsured: PropTypes.object,
  setSelectedInsured: PropTypes.func,
  selectedHolder: PropTypes.object,
  visiblePreview: PropTypes.bool,
  loadCreateSppa: PropTypes.object,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.bool,
  submitPreview: PropTypes.func,
  paymentTotal: PropTypes.any,
  calcPaymentTotal: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
  match: PropTypes.object,
  setSelectedIO: PropTypes.func,
  appendCustomer: PropTypes.func,
  bookPrice: PropTypes.any,
  getPremi: PropTypes.func,
  setFieldByIO: PropTypes.func,
}

export default FormLiability
