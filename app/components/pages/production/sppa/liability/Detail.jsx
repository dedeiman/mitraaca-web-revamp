/* eslint-disable no-nested-ternary */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button,
  Checkbox,
  Descriptions, Tag,
} from 'antd'
import moment from 'moment'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import { LeftOutlined } from '@ant-design/icons'

const DetailLiability = ({
  detailData,
}) => {
  const data = detailData.list

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Detail SPPA Liability</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailData.loading}>
                {
                  !detailData.loading && (
                    <>
                      <p className="title-card">Informasi Polis</p>
                      <Row gutter={24}>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="Tipe Polis" className="px-1 profile-detail pb-0">
                              {data.sppa_type.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="No Indicative Offer / Renewal" className="px-1 profile-detail pb-0">
                              {!isEmpty(data.indicative_offer) ? data.indicative_offer.io_number : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="No SPPA" className="px-1 profile-detail pb-0">
                              {data.sppa_number || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="Pemegang Polis" className="px-1 profile-detail pb-0">
                              {data.policy_holder.name.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                              {data.insured.name.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                              {data.name_on_policy.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={24}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={2} label="Product" className="px-1 profile-detail pb-0">
                              {data.product.type.name || ' '}
                              {' '}
                              -
                              {data.product.class_of_business.name || ' '}
                              {' '}
                              -
                              {data.product.display_name || ' '}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={1} label="Periode Awal" className="px-1 profile-detail pb-0">
                              {data.liability_calculation ? moment(data.liability_calculation.start_period).format('DD-MM-YYYY') : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item span={1} label="Periode Akhir" className="px-1 profile-detail pb-0">
                              {data.liability_calculation ? moment(data.liability_calculation.end_period).format('DD-MM-YYYY') : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailData.loading}>
                {
                  !detailData.loading && (
                    <>
                      <p className="title-card">Perhitungan Premi</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} label="Perhitungan Premi" className="px-1 profile-detail pb-0">
                          {Helper.currency(data.liability_calculation ? data.liability_calculation.limit_policy_amount : '0', 'Rp. ', ',-')}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Premi" className="px-1 profile-detail pb-0">
                          {Helper.currency(data.liability_calculation ? data.liability_calculation.premi : '0', 'Rp. ', ',-')}
                        </Descriptions.Item>
                      </Descriptions>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={1} label="Diskon" className="px-1 profile-detail pb-0">
                          {!isEmpty(data.liability_calculation) ? data.liability_calculation.discount_percentage : '0'}
                          {' '}
                          %
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="" className="px-1 profile-detail pb-0">
                          {Helper.currency(Number(!isEmpty(data.liability_calculation) ? data.liability_calculation.discount_currency : '0'), 'Rp. ', ',-')}
                        </Descriptions.Item>
                      </Descriptions>
                      <Row gutter={24}>
                        <Col xs={24} md={10}>
                          <p className="mb-1 mt-2">Biaya Administrasi</p>
                        </Col>
                        <Col xs={24} md={7}>
                          <Form.Item style={{ marginBottom: '0' }}>
                            <Checkbox checked={data.liability_calculation.print_policy_book} className="cursor-default" disabled>Buku Polis</Checkbox>
                          </Form.Item>
                        </Col>
                        <Col xs={24} md={7}>
                          <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.liability_calculation.print_policy_book ? '50000' : '0', 'Rp. ', ',-')}</b></p>
                        </Col>
                      </Row>
                      <Row gutter={24}>
                        <Col xs={24} md={10} />
                        <Col xs={24} md={7}>
                          <p className="mb-1 mt-2">Materai</p>
                        </Col>
                        <Col xs={24} md={7}>
                          <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                        </Col>
                      </Row>
                      <hr />
                      <Row gutter={24}>
                        <Col xs={24} md={10}>
                          <p className="mb-1 mt-2">Total Pembayaran</p>
                        </Col>
                        <Col xs={24} md={6} />
                        <Col xs={24} md={8}>
                          <p
                            className="mb-1 mt-2 text-dark fw-bold"
                            style={{ fontSize: '18px' }}
                          >
                            {Helper.currency(data.total_payment, 'Rp. ', ',-')}
                          </p>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
            <Col span={24}>
              <Card className="h-100" loading={detailData.loading}>
                {
                  !detailData.loading && (
                    <>
                      <p className="title-card">Pernyataan Tertanggung</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          Apakah saat ini Anda sedang ada proses hukum yang sedang berjalan ?
                        </Descriptions.Item>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          {data.liability_statement_insured.is_legal_processdings ? 'YES' : 'NO'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          Jika ya, tanggal proses hukum dimulai.
                        </Descriptions.Item>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          {data.liability_statement_insured ? !isEmpty(data.liability_statement_insured.legal_proceedings_date) ? moment(data.liability_statement_insured.legal_proceedings_date).format('DD-MM-YYYY') : '-' : '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          Jika ada, jelaskan proses hukum apa.
                        </Descriptions.Item>
                        <Descriptions.Item span={2} className="px-1 profile-detail pb-0">
                          {data.liability_statement_insured.legal_proceedings_description || ''}
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${data.status}/${data.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" disabled={data.status === 'complete' || data.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailLiability.propTypes = {
  detailData: PropTypes.object,
}

export default DetailLiability
