import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _, { isEmpty, capitalize } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import moment from 'moment'

import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'

const PreviewLiability = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  const parsed = queryString.parse(window.location.search)
  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      className="modal-add-customer"
      style={{ width: '80% !important' }}
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Tipe Polis</b></p>
                    <p className="mb-1">{(_.get(data, 'sppa_type') || '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Indicative Offer / Renewal </b></p>
                    <p className="mb-1">{_.get(data, 'io_renewal') || '-'}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>NO SPPA </b></p>
                    <p className="mb-1">Auto Genrated By System</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Pemegang Polis</b></p>
                    <p className="mb-1">{(_.get(data, 'policy_holder_id') || '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nama Tertanggung</b></p>
                    <p className="mb-1">{(_.get(data, 'insured_id') || '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nama Pada Polis</b></p>
                    <p className="mb-1">{(_.get(data, 'name_on_policy') || '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Product</b></p>
                    <p>
                      {data.match.params.id
                        ? <p>{`${!isEmpty(data.data) ? data.data.product.type.name : '-'} - Liability - ${!isEmpty(data.data) ? data.data.product.display_name : '-'}`}</p>
                        : <p>{`${capitalize(parsed.type)} - Liability - ${handleProduct(parsed.product)}`}</p>
                      }
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} md={12}>
                    <p className="mb-0"><b>Periode Awal</b></p>
                    <p className="mb-1">{moment(_.get(data, 'start_period')).format('DD MMMM YYYY')}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-0"><b>Periode Akhir</b></p>
                    <p className="mb-1">{moment(_.get(data, 'end_period')).format('DD MMMM YYYY')}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Limit Liability Polis Pertahun</b></p>
                        <p className="mb-1">{Helper.currency(_.get(data, 'coverage_limit'), 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Premi</b></p>
                        <p className="mb-1">{Helper.currency(_.get(data, 'premi'), 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-1">Diskon</p>
                        <p className="mb-1">{`${_.get(data, 'discount_percentage') || '0'}%`}</p>
                      </Col>
                      <Col xs={12} md={12}>
                        <p className="mb-1">&nbsp;</p>
                        <p className="mb-1">{Helper.currency(_.get(data, 'discount_currency'), 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={8}>
                        <p className="mt-2"><b>Biaya Administrasi</b></p>
                      </Col>
                      <Col xs={24} md={7}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true}>
                              Buku Polis
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mt-2 mb-1">Rp 50.000,-</p>
                        {/* <p className="mt-2">{_.get(data, 'print_policy_book') ? 'Rp 50.000,-' : 'Rp 0,-'}</p> */}
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={7}>
                        <p className="mb-1">Materai</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{!isEmpty(data) ? Helper.currency(data.data.stamp_fee, 'Rp. ', ',-') : ''}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={15}>
                        <p className="mb-1">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{Helper.currency(data.paymentTotal + (data.print_policy_book ? 50000 : 0), 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Pernyataan Tertanggung</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          Apakah saat ini Anda sedang ada proses hukum yang sedang berjalan ?
                          {' '}
                          <br />
                          {_.get(data, 'is_legal_processdings', false) ? (<b>Ya</b>) : (<b>Tidak</b>) }
                        </p>
                        <p className="mb-1">
                          Tanggal Proses Hukum Dimulai ?
                          {' '}
                          <br />
                          { _.get(data, 'legal_proceedings_date', false) ? (<b>{data.legal_proceedings_date.format('DD MMMM YYYY')}</b>) : (<b>-</b>) }
                        </p>
                        <p className="mb-1">
                          Jika ada, jelaskan proses hukum apa.
                          {' '}
                          <br />
                          <b>{_.get(data, 'legal_proceedings_description', '-')}</b>
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 mr-3"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewLiability.propTypes = {
  show: PropTypes.any,
  handleCancel: PropTypes.func,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
  data: PropTypes.object,
}

export default PreviewLiability
