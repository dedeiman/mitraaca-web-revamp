import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Descriptions,
  Checkbox, Tag,
} from 'antd'
import history from 'utils/history'
import Helper from 'utils/Helper'
import moment from 'moment'
import { isEmpty } from 'lodash'
import { LeftOutlined } from '@ant-design/icons'

const DetailWellwomen = ({
  detailWellwomen,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Detail SPPA Wellwoman</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailWellwomen.loading}>
                {
                  !detailWellwomen.loading
                  && (
                    <>
                      <p className="title-card">Informasi Polis</p>
                      <Row gutter={24}>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Tipe Polis" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.sppa_type.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Indicative Offer / Renewal" className="px-1 profile-detail pb-0">
                              {!isEmpty(detailWellwomen.list.indicative_offer) ? (detailWellwomen.list.indicative_offer.io_number || '').toUpperCase() : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="NO SPPA" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.sppa_number.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Pemegang Polis" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.insured.name.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.policy_holder.name.toUpperCase() || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.name_on_policy || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Product" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list.product.type.name || ''}
                              {' - '}
                              Health
                              {' - '}
                              {detailWellwomen.list.product.display_name || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Periode Awal" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list ? moment(detailWellwomen.list.start_period).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Periode Akhir" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list ? moment(detailWellwomen.list.end_period).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                        <Col xs={24} md={12}>
                          <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Tanggal Lahir Tertanggung" className="px-1 profile-detail pb-0">
                              {detailWellwomen.list ? moment(detailWellwomen.list.dob).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                          </Descriptions>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailWellwomen.loading}>
                {
                  !detailWellwomen.loading
                  && (
                    <>
                      <p className="title-card">Informasi Ahli Waris</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} label="Nama Ahli Waris" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_beneficiary_information.name || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Hubungan" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_beneficiary_information.relationship.name || '-'}
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
            </Col>
            <Col span={24}>
              <Card className="h-100" loading={detailWellwomen.loading}>
                {
                  !detailWellwomen.loading
                  && (
                    <>
                      <p className="title-card">Perhitungan Premi</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} label="Premi" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailWellwomen.list.wellwoman_premi_calculation.premi, 'Rp. ', ',-') || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Plan" className="px-1 profile-detail pb-0">
                          Plan
                          {' '}
                          {detailWellwomen.list.wellwoman_premi_calculation.plan || '-'}
                        </Descriptions.Item>
                      </Descriptions>
                      <Row gutter={24}>
                        <Col xs={24} md={10}>
                          <p className="mt-2"><b>Biaya Administrasi</b></p>
                        </Col>
                        <Col xs={24} md={7}>
                          <Checkbox className="mt-2" checked={detailWellwomen.list.print_policy_book} disabled>
                            Buku Polis
                          </Checkbox>
                        </Col>
                        <Col xs={24} md={7}>
                          <p className="mt-2 text-dark fw-bold">{Helper.currency(detailWellwomen.list.policy_printing_fee, 'Rp. ', ',-')}</p>
                        </Col>
                      </Row>
                      <Row gutter={24}>
                        <Col xs={24} md={10} />
                        <Col xs={24} md={7}>
                          <p className="mb-1">Materai</p>
                        </Col>
                        <Col xs={24} md={7}>
                          <strong className="mb-1 text-dark fw-bold">{Helper.currency(detailWellwomen.list.stamp_fee || '0', 'Rp. ', ',-')}</strong>
                        </Col>
                      </Row>
                      <hr />
                      <Row gutter={24}>
                        <Col xs={24} md={10}>
                          <p className="mb-1 mt-2">Total Pembayaran</p>
                        </Col>
                        <Col xs={24} md={6} />
                        <Col xs={24} md={8}>
                          <p
                            className="mb-1 mt-2 text-dark fw-bold"
                            style={{ fontSize: '18px' }}
                          >
                            <b>
                              {Helper.currency(detailWellwomen.list.total_payment, 'Rp. ', ',-') || '-'}
                            </b>
                          </p>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailWellwomen.loading}>
                {
                  !detailWellwomen.loading
                  && (
                    <>
                      <p className="title-card">Pernyataan Kesehatan Tertanggung</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} label="Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.first_answer ? 'Iya' : 'Tidak'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Jika ya, mohon diuraikan secara terinci." className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.first_answer_description || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Apakan calon peserta asuransi pernah menjalani test pap smear (cervical smear) dengan hasil abnormal atau disarankan doker untuk mengulangi test smear dalam kurun waktu 6 bulan sejak terakhir ?" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.second_answer ? 'Iya' : 'Tidak'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Jika ya, mohon diuraikan secara terinci." className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.second_answer_description || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Apakah calon peserta asuransi pernah menjalani atau disarankan untuk menjalani pemeriksaan, berkonsultasi dengan dokter spesialis untuk suatu penyakit atau kelainan seputar payudara ? (termasuk didalamnya benjolan pada payudara, fibroadenoma, kista, kelainan pada puting, radang payudara, perubahan sel yang abnormal, carcinoma in situ, kangker atau pertumbuhan) ?" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.third_answer ? 'Iya' : 'Tidak'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Jika ya, mohon diuraikan secara terinci." className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.third_answer_description || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Apakah calon peserta asuransi pernah menjalani atau disarankan untuk menjalani pemeriksaan, berkosultasi dengan dokter spesialis untuk suatu penyakit atau kelainan seputar mulut rahim, kandungan atau indung telur ? (termasuk didalamnya kista ovarium pendarahan kandungan atau kelamin yang tidak biasa, pembesaran tidak normal pada rongga perut, tumor jaringan fibrous, polip, carcinoma in situ, kangker atau pertumbuhan) ?" className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.fourth_answer ? 'Iya' : 'Tidak'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Jika ya, mohon diuraikan secara terinci." className="px-1 profile-detail pb-0">
                          {detailWellwomen.list.wellwoman_health_statement.fourth_answer_description || '-'}
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${detailWellwomen.list.status}/${detailWellwomen.list.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" disabled={detailWellwomen.list.status === 'complete' || detailWellwomen.list.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${detailWellwomen.list.id}?product=${detailWellwomen.list.product.code}&product_id=${detailWellwomen.list.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailWellwomen.propTypes = {
  detailWellwomen: PropTypes.object,
}

export default DetailWellwomen
