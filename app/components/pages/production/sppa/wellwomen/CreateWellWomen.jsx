/* eslint-disable no-unused-vars */
/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Input,
  Select,
  Checkbox,
  Button,
  DatePicker,
  AutoComplete,
  Radio,
  InputNumber,
} from 'antd'
import Helper from 'utils/Helper'
import Hotkeys from 'react-hot-keys'
import { Form } from '@ant-design/compatible'
import queryString from 'query-string'
import { handleProduct } from 'constants/ActionTypes'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import moment from 'moment'
import { isEmpty, capitalize } from 'lodash'
import NumberFormat from 'react-number-format'
import PreviewWellwoman from './PreviewWellwoman'
import config from 'app/config'

const CreateWellWomen = ({
  form, onSubmit,
  premi,
  visibleDiscount,
  setVisibleDiscount,
  listIO,
  handleRenewal,
  detailWellwomen,
  listCustomer,
  setSelectedHolder,
  selectedInsured,
  setSelectedInsured,
  selectedHolder,
  listRelationship,
  visiblePreview,
  setVisiblePreview,
  submitted,
  submitData,
  submitPreview,
  setSelectedRelationship,
  selectedRelationship,
  appendCustomer,
  getPremi, match,
  setSelectedIO,
  paymentTotal,
  calcDiscountByCurrency,
  calcDiscountByPercentage,
  calcPaymentTotal,
  loadSubmitBtn,
  setFieldByIO,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    setFieldsValue,
    getFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)

  const data = detailWellwomen.list

  let sppaType

  if (!isEmpty(detailWellwomen.list)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailWellwomen.list.sppa_type
    }
  }

  return (
    <React.Fragment>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <PreviewWellwoman
        show={visiblePreview}
        setVisible={setVisiblePreview}
        submitted={submitted}
        data={{
          ...getFieldsValue(),
          premi,
          selectedRelationship,
          paymentTotal,
        }}
        handleCancel={() => setVisiblePreview(false)}
        handleSubmit={submitData}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Create'}`} SPPA Wellwoman</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Informasi Polis</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tipe Polis</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: Helper.fieldRules(['required'], 'Tipe polis'),
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Pilih Tipe"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Indicative Offer / Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_renewal', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Indicative Offer / Renewal"
                            dataSource={listIO.options.map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.id : item.id}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}
                            disabled={getFieldValue('sppa_type') === undefined}
                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listIO.options).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listIO.options).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">NO SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_number', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.sppa_number : '',
                        })(
                          <Input
                            disabled
                            placeholder="Auto Generated By System"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Pemegang Polis</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>

                        {getFieldDecorator('policy_holder_id', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.policy_holder.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Pemegang polis'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Pemegang Polis"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)
                                setSelectedInsured(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                    
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName || ''}`,
                                  })
                                }
                                setFieldsValue({
                                  insured_id: data.name,
                                  policy_holder_id: data.name,
                                  birth_date: !isEmpty(data.dob) ? moment(data.dob) : undefined,
                                })
                              }
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Tertanggung</p>
                      <Form.Item>
                          {getFieldDecorator('insured_id', {
                            initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.insured.name : undefined || undefined,
                            rules: Helper.fieldRules(['required'], 'Nama tertanggung'),
                          })(
                          <Input
                            placeholder="Nama Tertanggung"
                            className="field-lg uppercase"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Pada Polis</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.name_on_policy : '',
                          rules: Helper.fieldRules(['required'], 'Nama pada polis'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Insured Name"
                            className="field-lg uppercase"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Product</p>
                      {match.params.id
                        ? <p>{`${!isEmpty(detailWellwomen.list) ? detailWellwomen.list.product.type.name : '-'} - Health - ${!isEmpty(detailWellwomen.list) ? detailWellwomen.list.product.display_name : '-'}`}</p>
                        : <p>{`${capitalize(parsed.type)} - Health - ${handleProduct(parsed.product)}`}</p>
                      }
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Awal</p>
                      <Form.Item>
                        {getFieldDecorator('start_period', {
                          initialValue: !isEmpty(detailWellwomen.list) ? moment(new Date(detailWellwomen.list.start_period), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['required'], 'Periode awal'),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              end_period: moment(new Date(e), 'DD MMMM YYYY').add(1, 'year'),
                            })

                            return moment(new Date(e), 'DD MMMM YYYY')
                          },
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            format="DD MMMM YYYY"
                            disabledDate={current =>
                              // eslint-disable-next-line no-mixed-operators,implicit-arrow-linebreak
                              current && current < moment().endOf('day') || current.diff(moment().add(6), 'day') > 6
                            }
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Akhir</p>
                      <Form.Item>
                        {getFieldDecorator('end_period', {
                          initialValue: !isEmpty(detailWellwomen.list) ? moment(new Date(detailWellwomen.list.end_period), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['required'], 'Periode akhir'),
                          getValueFromEvent: (e) => {
                            const difference = moment(new Date(e)).diff(getFieldValue('start_period'), 'year')
                            const endDate = moment(getFieldValue('start_period')).add(difference, 'year').add(0, 'day')

                            return endDate
                          },
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            disabledDate={current => (current && (current < moment(getFieldValue('start_period')).subtract(0, 'day').add(1, 'year')))}
                            format="DD MMMM YYYY"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tanggal Lahir Tertanggung</p>
                      <Form.Item>
                        {getFieldDecorator('birth_date', {
                          initialValue: !isEmpty(detailWellwomen.list) ? moment(new Date(detailWellwomen.list.insured_dob), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['required'], 'Tanggal lahir tertanggung'),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              birth_date: moment(new Date(e), 'DD MMMM YYYY'),
                            })
                            return moment(new Date(e), 'DD MMMM YYYY')
                          },
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            defaultPickerValue={moment().subtract(18, 'years')}
                            disabled
                            format="DD MMMM YYYY"
                            onChange={(date) => {
                              setFieldsValue({
                                birth_date: date,
                              })
                              getPremi()
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Informasi Ahli Waris</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Ahli Waris</p>
                      <Form.Item>
                        {getFieldDecorator('name_of_heir', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_beneficiary_information.name : undefined,
                          rules: Helper.fieldRules(['required'], 'Nama ahli waris'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Insured Name"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Hubungan</p>
                      <Form.Item>
                        {getFieldDecorator('heir_relation', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_beneficiary_information.relationship.id : undefined,
                          rules: Helper.fieldRules(['required'], 'Hubungan'),
                          onChange: (value, selectedOption) => {
                            setSelectedRelationship(selectedOption)
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Pilih Hungungan"
                            loading={false}
                            disabled={false}
                          >
                            {(listRelationship.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Premi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Plan</p>
                      <Form.Item>
                        {getFieldDecorator('plan', {
                          initialValue: Number(!isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_premi_calculation.plan : 0),
                          rules: [],
                          onChange: (e) => {
                            setFieldsValue({
                              plan: e.target.value,
                            })
                            getPremi()
                          },
                        })(
                          <Radio.Group>
                            <Radio value={1}>Plan 1</Radio>
                            <Radio value={2}>Plan 2</Radio>
                            <Radio value={3}>Plan 3</Radio>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Premi</p>
                      <Form.Item>
                        {getFieldDecorator('premi', {
                          initialValue: premi ? premi.premi : 0,
                          rules: Helper.fieldRules(['required'], 'Premi'),
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(data, 'wellwoman_premi_calculation.discount_percentage', 0),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            suffix="%"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_currency', {
                          initialValue: parseFloat(premi.discount_currency).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book', {
                          initialValue: _.get(detailWellwomen, 'list.print_policy_book', false),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              print_policy_book: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                            onChange={() => setTimeout(() => getPremi())}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={7}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9} />
                    <Col xs={24} md={8}>
                      <p>Materai</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={17}>
                      <p>Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>
                        {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                      </p>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Pernyataan Kesehatan Tertanggung</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?</p>
                      <div className="mb-1">
                        <Form.Item>
                          {getFieldDecorator('healthy_question_1_answer', {
                            // eslint-disable-next-line no-nested-ternary
                            initialValue: isEmpty(detailWellwomen.list) ? 0 : detailWellwomen.list.wellwoman_health_statement.first_answer ? 1 : 0,
                            rules: [],
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                healthy_question_1_esay: '',
                              })

                              return e.target.value
                            },
                          })(
                            <Radio.Group>
                              <Radio value={1}>Ya</Radio>
                              <Radio value={0}>Tidak</Radio>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </div>
                      <p className="mb-1">Jika ya, mohon diuraikan secara terinci.</p>
                      <Form.Item>
                        {getFieldDecorator('healthy_question_1_esay', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_health_statement.first_answer_description : undefined,
                          rules: getFieldValue('healthy_question_1_answer') ? Helper.fieldRules(['required', 'notSpecial']) : [],
                        })(
                          <Input.TextArea
                            placeholder="Jawaban"
                            className="field-lg"
                            rows={5}
                            disabled={!getFieldValue('healthy_question_1_answer')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakan calon peserta asuransi pernah menjalani test pap smear (cervical smear) dengan hasil abnormal atau disarankan doker untuk mengulangi test smear dalam kurun waktu 6 bulan sejak terakhir ?</p>
                      <div className="mb-1">
                        <Form.Item>
                          {getFieldDecorator('healthy_question_2_answer', {
                            // eslint-disable-next-line no-nested-ternary
                            initialValue: isEmpty(detailWellwomen.list) ? 0 : detailWellwomen.list.wellwoman_health_statement.second_answer ? 1 : 0,
                            rules: [],
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                healthy_question_2_esay: '',
                              })

                              return e.target.value
                            },
                          })(
                            <Radio.Group>
                              <Radio value={1}>Ya</Radio>
                              <Radio value={0}>Tidak</Radio>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </div>
                      <p className="mb-1">Jika ya, mohon diuraikan secara terinci.</p>
                      <Form.Item>
                        {getFieldDecorator('healthy_question_2_esay', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_health_statement.second_answer_description : undefined,
                          rules: getFieldValue('healthy_question_2_answer') ? Helper.fieldRules(['required', 'notSpecial']) : [],
                        })(
                          <Input.TextArea
                            placeholder="Jawaban"
                            className="field-lg"
                            rows={5}
                            disabled={!getFieldValue('healthy_question_2_answer')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakah calon peserta asuransi pernah menjalani atau disarankan untuk menjalani pemeriksaan, berkonsultasi dengan dokter spesialis untuk suatu penyakit atau kelainan seputar payudara ? (termasuk didalamnya benjolan pada payudara, fibroadenoma, kista, kelainan pada puting, radang payudara, perubahan sel yang abnormal, carcinoma in situ, kangker atau pertumbuhan) ?</p>
                      <div className="mb-1">
                        <Form.Item>
                          {getFieldDecorator('healthy_question_3_answer', {
                            // eslint-disable-next-line no-nested-ternary
                            initialValue: isEmpty(detailWellwomen.list) ? 0 : detailWellwomen.list.wellwoman_health_statement.third_answer ? 1 : 0,
                            rules: [],
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                healthy_question_3_esay: '',
                              })

                              return e.target.value
                            },
                          })(
                            <Radio.Group>
                              <Radio value={1}>Ya</Radio>
                              <Radio value={0}>Tidak</Radio>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </div>
                      <p className="mb-1">Jika ya, mohon diuraikan secara terinci.</p>
                      <Form.Item>
                        {getFieldDecorator('healthy_question_3_esay', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_health_statement.third_answer_description : undefined,
                          rules: getFieldValue('healthy_question_3_answer') ? Helper.fieldRules(['required', 'notSpecial']) : [],
                        })(
                          <Input.TextArea
                            placeholder="Jawaban"
                            className="field-lg"
                            rows={5}
                            disabled={!getFieldValue('healthy_question_3_answer')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakah calon peserta asuransi pernah menjalani atau disarankan untuk menjalani pemeriksaan, berkosultasi dengan dokter spesialis untuk suatu penyakit atau kelainan seputar mulut rahim, kandungan atau indung telur ? (termasuk didalamnya kista ovarium pendarahan kandungan atau kelamin yang tidak biasa, pembesaran tidak normal pada rongga perut, tumor jaringan fibrous, polip, carcinoma  in situ, kangker atau pertumbuhan) ? </p>
                      <div className="mb-1">
                        <Form.Item>
                          {getFieldDecorator('healthy_question_4_answer', {
                            // eslint-disable-next-line no-nested-ternary
                            initialValue: isEmpty(detailWellwomen.list) ? 0 : detailWellwomen.list.wellwoman_health_statement.fourth_answer ? 1 : 0,
                            rules: [],
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                healthy_question_4_esay: '',
                              })

                              return e.target.value
                            },
                          })(
                            <Radio.Group>
                              <Radio value={1}>Ya</Radio>
                              <Radio value={0}>Tidak</Radio>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </div>
                      <p className="mb-1">Jika ya, mohon diuraikan secara terinci.</p>
                      <Form.Item>
                        {getFieldDecorator('healthy_question_4_esay', {
                          initialValue: !isEmpty(detailWellwomen.list) ? detailWellwomen.list.wellwoman_health_statement.fourth_answer_description : undefined,
                          rules: getFieldValue('healthy_question_4_answer') ? Helper.fieldRules(['required', 'notSpecial']) : [],
                        })(
                          <Input.TextArea
                            placeholder="Jawaban"
                            className="field-lg"
                            rows={5}
                            disabled={!getFieldValue('healthy_question_4_answer')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: Helper.fieldRules(['required'], 'Syarat & ketentuan'),
                        })(
                          <Checkbox.Group>
                            <Checkbox value="aggree">
                              Saya telah membaca dan setuju dengan
                              &nbsp;
                              <u>
                                <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                  <b>Syarat & Ketentuan Mitraca</b>
                                </a>
                              </u>
                              &nbsp;
                              yang berlaku
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="button-lg w-50 border-lg"
                  disabled={loadSubmitBtn}
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

CreateWellWomen.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  premi: PropTypes.object,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  // eslint-disable-next-line react/no-unused-prop-types
  handleProduct: PropTypes.func,
  listIO: PropTypes.object,
  handleRenewal: PropTypes.func,
  detailWellwomen: PropTypes.object,
  listCustomer: PropTypes.object,
  setSelectedHolder: PropTypes.func,
  selectedInsured: PropTypes.object,
  setSelectedInsured: PropTypes.func,
  selectedHolder: PropTypes.object,
  listRelationship: PropTypes.object,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.bool,
  submitData: PropTypes.func,
  submitPreview: PropTypes.func,
  setSelectedRelationship: PropTypes.func,
  selectedRelationship: PropTypes.object,
  appendCustomer: PropTypes.func,
  getPremi: PropTypes.func,
  match: PropTypes.object,
  setSelectedIO: PropTypes.func,
  paymentTotal: PropTypes.any,
  calcDiscountByCurrency: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
  calcPaymentTotal: PropTypes.func,
  loadSubmitBtn: PropTypes.bool,
  setFieldByIO: PropTypes.func,
}

export default CreateWellWomen
