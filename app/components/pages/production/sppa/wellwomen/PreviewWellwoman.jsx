import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _, { isEmpty, capitalize } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'

import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'

const PreviewWellwoman = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  const parsed = queryString.parse(window.location.search)
  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      className="modal-add-customer"
      style={{ width: '80% !important' }}
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Tipe Polis</b></p>
                    <p className="mb-1">{_.get(data, 'sppa_type', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Indicative Offer / Renewal </b></p>
                    <p className="mb-1">{_.get(data, 'sppa_type', null) === 'sppa_number' ? _.get(data, 'io_number', '-') : _.get(data, 'io_number', '-') }</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>NO SPPA </b></p>
                    <p className="mb-1">Auto Genrated By System</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Pemegang Polis</b></p>
                    <p className="mb-1">{_.get(data, 'insured_id', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nama Tertanggung</b></p>
                    <p className="mb-1">{_.get(data, 'policy_holder_id', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nama Pada Polis</b></p>
                    <p className="mb-1">{_.get(data, 'name_on_policy', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Product</b></p>
                    <p>
                      <b>{`${capitalize(parsed.type)} - Healthy - ${handleProduct(parsed.product)}`}</b>
                    </p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-0"><b>Insurance Period</b></p>
                    <p className="mb-1">
                      1 Tahun
                      {' '}
                      (
                      { data.start_period ? data.start_period.format('YYYY-MM-DD') : '-' }
                      {' '}
                      -
                      {' '}
                      { data.end_period ? data.end_period.format('YYYY-MM-DD') : '-' }
                      )
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Tanggal Lahir Tertanggung</b></p>
                    <p className="mb-1">{ data.birth_date ? data.birth_date.format('YYYY-MM-DD') : '-' }</p>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col xs={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Informasi Ahli Waris</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Nama Ahli Waris</b></p>
                        <p className="mb-1">{_.get(data, 'name_of_heir', '-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Hubungan</b></p>
                        <p className="mb-1">{data.selectedRelationship.children ? data.selectedRelationship.children : '-'}</p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Premi</b></p>
                        <p className="mb-1">{Helper.currency(data.premi.premi, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col xs={6} md={6}>
                            <p className="mb-1"><b>Diskon</b></p>
                            <p className="mb-1">{_.get(data, 'discount_percentage')}%</p>
                        </Col>
                        <Col xs={6} md={6}>
                            <p className="mb-1">&nbsp;</p>
                            <p>Rp. {_.get(data, 'discount_currency')},-</p>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={8}>
                        <p className="mt-2"><b>Biaya Administrasi</b></p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={data.print_policy_book === true}>
                            Buku Polis
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mt-2">Rp 50.000,-</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={8}>
                        <p className="mb-1">Materai</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={16}>
                        <p className="mb-1">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{Helper.currency(data.paymentTotal + (data.print_policy_book ? 50000 : 0), 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Form.Item>
                          <Checkbox checked={!isEmpty(data.aggree) ? _.get(data, 'aggree', '-')[0] : false}>
                            Saya telah membaca dan setuju dengan
                            {' '}
                            <b>Syarat & Ketentuan Mitraca</b>
                            {' '}
                            yang berlaku
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Pernyataan Tertanggung</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?
                          {_.get(data, 'healthy_question_1_answer', false) ? (<b>Ya</b>) : (<b>Tidak</b>) }
                        </p>
                        <p className="mb-1"><b>{_.get(data, 'healthy_question_1_esay', '-')}</b></p>
                        <hr />
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?
                          {_.get(data, 'healthy_question_2_answer', false) ? (<b>Ya</b>) : (<b>Tidak</b>) }
                        </p>
                        <p className="mb-1"><b>{_.get(data, 'healthy_question_2_esay', '-')}</b></p>
                        <hr />
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?
                          {_.get(data, 'healthy_question_3_answer', false) ? (<b>Ya</b>) : (<b>Tidak</b>) }
                        </p>
                        <p className="mb-1"><b>{_.get(data, 'healthy_question_3_esay', '-')}</b></p>
                        <hr />
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          Apakah saat ini perserta asuransi mengalami gejala-gejala yang dapat menyebabkan calon peserta asuransi untuk meminta saran/berkonsultasi dengan dokter, menjalani investigasi secara medis atau pengobatan dalam waktu dekat ?
                          {_.get(data, 'healthy_question_4_answer', false) ? (<b>Ya</b>) : (<b>Tidak</b>) }
                        </p>
                        <p className="mb-1"><b>{_.get(data, 'healthy_question_4_esay', '-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 mr-3"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewWellwoman.propTypes = {
  show: PropTypes.any,
  handleCancel: PropTypes.func,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
  data: PropTypes.object,
}

export default PreviewWellwoman
