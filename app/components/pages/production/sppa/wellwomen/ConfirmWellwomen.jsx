import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Descriptions,
} from 'antd'
import moment from 'moment'
import Helper from 'utils/Helper'
import SignatureCanvas from 'react-signature-canvas'

const ConfirmWellwomen = ({
  detailSPPA, signature,
  handleSignatureSave,
  handleBack, errorSigPad, setErrorSigPad
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Confirm SPPA Wellwomen</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={24}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailSPPA.loading}>
                {
                  !detailSPPA.loading
                  && (
                    <>
                      <p className="title-card">Informasi SPPA</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Tipe SPPA" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.sppa_type.toUpperCase() || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Pemegang Polis" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.policy_holder.name.toUpperCase() || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Nama tertanggung" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.insured.name.toUpperCase() || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Nama pada Polis" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.name_on_policy.toUpperCase() || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="QQ" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.qq.toUpperCase() || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Produk" className="px-1 profile-detail pb-0">
                          {detailSPPA.list.product.type.name || '-'}
                          {' '}
                          {detailSPPA.list.product.display_name || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Awal" className="px-1 profile-detail pb-0">
                          {detailSPPA.list ? moment(detailSPPA.list.start_period).format('DD MMMM YYYY') : '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Akhir" className="px-1 profile-detail pb-0">
                          {detailSPPA.list ? moment(detailSPPA.list.end_period).format('DD MMMM YYYY') : '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Premi" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.wellwoman_premi_calculation.premi, 'Rp. ', ',-') || '0'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Diskon Percentage" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.wellwoman_premi_calculation.discount_percentage) || '0'}%
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Diskon Currency" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.wellwoman_premi_calculation.discount_currency, 'Rp. ', ',-') || '0'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Buku Polis" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.policy_printing_fee, 'Rp. ', ',-') || '0'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Materai" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.stamp_fee, 'Rp. ', ',-') || '0'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Total Pembayaran" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailSPPA.list.total_payment, 'Rp. ', ',-') || '0'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Pernyataan Tertanggung" className="px-1 profile-detail pb-0">
                          <ol>
                            <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
                            <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh penanggung</li>
                            <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari penanggung.</li>
                            <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT.</li>
                            <li style={{ textAlign: 'justify' }}>Dengan ini menyatakan bahwa saat ini saya dalam keadaan sehat jasmani dan rohani, serta tidak terdapat riwayat kangker pada dirisaya sebelumnya. Dan juga meberikan kuasa penuh pada PT Asuransi Central Asia untuk menjalankan analisis mendalam (termasuk pengumpulan informasi) terhadap claim yang di ajukan. Jika keterangan yang di ajukan tidak benar, maka akan mengakibatkan batalnya pertanggungan  dan di tolaknya pengajuan claim.</li>
                          </ol>
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Tanda Tangan Tertanggung" className="px-1 profile-detail pb-0 text-center">
                          <SignatureCanvas
                            penColor="black"
                            canvasProps={{ width: 300, height: 300, className: 'sigCanvas' }}
                            /*eslint-disable */
                            ref={ref => signature.data = ref}
                            /* eslint-enable */
                          />
                          <p style={{ color: 'red' }}>{errorSigPad ? 'Wajib Diisi!' : ''}</p>
                        </Descriptions.Item>
                      </Descriptions>
                      <Row gutter={[24, 24]} justify="space-between" className="mb-3">
                        <Col span={24} className="d-flex justify-content-between align-items-center">
                          <Button type="primary" htmlType="button" onClick={() => handleBack(detailSPPA.list.id, detailSPPA.list.product.code, detailSPPA.list.product.id)} className="button-lg w-25 ml-2 mr-2 border-lg">
                            Back
                          </Button>
                          <Button type="primary" htmlType="button" onClick={() => signature.data.clear()} className="button-lg w-25 ml-2 mr-2 border-lg">
                            Clear
                          </Button>
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="button-lg w-25 ml-2 mr-2 border-lg"
                            onClick={() => {
                              if (!signature.data.isEmpty()) {
                                setErrorSigPad(false)
                                handleSignatureSave(
                                  signature.data.getCanvas().toDataURL('image/png'),
                                  'save',
                                )
                              } else {
                                setErrorSigPad(true)
                              }
                            }}
                          >
                            Save
                          </Button>
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="button-lg w-25 ml-2 mr-2 border-lg"
                            onClick={() => {
                              if (!signature.data.isEmpty()) {
                                setErrorSigPad(false)
                                handleSignatureSave(
                                  signature.data.getCanvas().toDataURL('image/png'),
                                  'next',
                                )
                              } else {
                                setErrorSigPad(true)
                              }
                            }}
                          >
                            Next
                          </Button>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

ConfirmWellwomen.propTypes = {
  detailSPPA: PropTypes.object,
  signature: PropTypes.object,
  handleSignatureSave: PropTypes.func,
  handleBack: PropTypes.func,
  errorSigPad: PropTypes.bool,
  setErrorSigPad: PropTypes.func,
}

export default ConfirmWellwomen
