import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
} from 'antd'
import history from 'utils/history'

const DetailDocument = ({
  detailData,
}) => (
  <React.Fragment>
    <Row gutter={[24, 24]} className="mb-3">
      <Col span={24} className="d-flex justify-content-between align-items-center">
        <div className="title-page">Confirm SPPA</div>
      </Col>
    </Row>
    <Row gutter={24}>
      <Col xs={24} md={24}>
        <Row gutter={[24, 24]}>
          <Col span={24}>
            <Card className="h-100" loading={detailData.loading}>
              {
                  !detailData.loading
                  && (
                    <>
                      <Row gutter={[24, 24]} justify="space-between" className="mb-3">
                        {detailData.list.map(data => (
                          <Col span={12}>
                            <img src={data.file} alt={data.description} className="w-100" />
                            <p className="text-center title-card">{data.description}</p>
                          </Col>
                        ))}
                      </Row>
                    </>
                  )
                }
            </Card>
            <Button type="primary" onClick={() => history.goBack()} className="mt-3 mb-5 pl-5 pr-5">Kembali</Button>
          </Col>
        </Row>
      </Col>
    </Row>
  </React.Fragment>
)

DetailDocument.propTypes = {
  detailData: PropTypes.object,
}

export default DetailDocument
