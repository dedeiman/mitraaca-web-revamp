import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _, { isEmpty, capitalize } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import moment from 'moment'
import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'

const PreviewOtomate = ({
  data,
  show,
  premi,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  const parsed = queryString.parse(window.location.search)
  const seriesName = data.listSeries.options.filter(item => item.value === data.series)
  const brandName = data.listBrand.options.filter(item => item.value === data.brand)
  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      className="modal-add-customer"
      style={{ width: '80% !important' }}
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">SPPA Motorcar</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Tipe Polis</b></p>
                    <p className="mb-1">{_.get(data, 'sppa_type', '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Indicative Offer / Renewal</b></p>
                    <p className="mb-1">{_.get(data, 'io_renewal', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Policy Holder</b></p>
                    <p className="mb-1">{_.get(data, 'policy_holder_id', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Insured</b></p>
                    <p className="mb-1">{_.get(data, 'insured_id', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Name on Policy</b></p>
                    <p className="mb-1">{_.get(data, 'name_on_policy', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>QQ</b></p>
                    <p className="mb-1">{_.get(data, 'qq', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Periode</b></p>
                    <p className="mb-1">{`${_.get(data, 'period', '-')} Tahun`}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Product</b></p>
                    {data.match.params.id
                      ? <p>{`${!isEmpty(data.detailOtomate.list) ? data.detailOtomate.list.product.type.name : '-'} - Motorcar - ${!isEmpty(data.detailOtomate.list) ? data.detailOtomate.list.product.display_name : '-'}`}</p>
                      : <p>{`${capitalize(parsed.type)} - Motorcar - ${handleProduct(parsed.product)}`}</p>
                    }
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Periode Awal</b></p>
                    <p className="mb-1">{!isEmpty(_.get(data, 'start_period')) ? moment(_.get(data, 'start_period')).format('DD MMMM YYYY') : '-'}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Periode Akhir</b></p>
                    <p className="mb-1">{!isEmpty(_.get(data, 'end_period')) ? moment(_.get(data, 'end_period')).format('DD MMMM YYYY') : '-'}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col xs={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Informasi Kendaraan</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Brand</b></p>
                        <p className="mb-1">{brandName.map(brand => brand.label)}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <p className="mb-0"><b>Series</b></p>
                        <p className="mb-1">{seriesName.map(series => series.label)}</p>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mb-0"><b>Desc</b></p>
                        <p className="mb-1">{_.get(data, 'desc_vehicle', '-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Vehicle Category</b></p>
                        <p className="mb-1">{_.get(data, 'vehicle_category', '-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <p className="mb-0"><b>Production Year</b></p>
                        <p className="mb-1">{_.get(data, 'production_year', '-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-0"><b>Plate Code</b></p>
                        <p className="mb-1">
                          {_.get(data, 'license_plate_code', '-')}
                          {' '}
                          {_.get(data, 'middle_policy_number')}
                          {' '}
                          {_.get(data, 'rear_vehicle_year')}
                        </p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-0"><b>Warna</b></p>
                        <p className="mb-1">{_.get(data, 'color', '-').toUpperCase()}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-0"><b>Nomer Rangka</b></p>
                        <p className="mb-1">{_.get(data, 'chassis_number', '-').toUpperCase()}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-0"><b>Nomor Mesin</b></p>
                        <p className="mb-1">{_.get(data, 'machine_number', '-').toUpperCase()}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-0"><b>Vehicle Use</b></p>
                        <p className="mb-1">{_.get(data, 'vehicle_use', '-').toUpperCase()}</p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Perhitungan Premi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Alamat Pertanggungan</p>
                    <p className="mb-0"><b>Nilai Pertanggungan</b></p>
                    <p className="mb-1">
                      {typeof (data.coverage_value) === 'string'
                        ? Helper.currency(Helper.replaceNumber(data.coverage_value), 'Rp. ', ',-') : Helper.currency(_.get(data, 'coverage_value', '-'), 'Rp. ', ',-')
                      }
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nilai Pertanggungan Perlengkapan</b></p>
                    <p className="mb-1">
                      {typeof (data.additional_accessories_costs) === 'string'
                        ? Helper.currency(Helper.replaceNumber(data.additional_accessories_costs), 'Rp. ', ',-') : Helper.currency(_.get(data, 'additional_accessories_costs', '-'), 'Rp. ', ',-')
                      }
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nilai Total Pertanggungan</b></p>
                    <p className="mb-1">{Helper.currency(_.get(data, 'coverage_total_value', '-'), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Nilai Resiko Sendiri</b></p>
                    <p className="mb-1">{Helper.currency(_.get(data, 'own_risk_value', '-'), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Loading Rate</b></p>
                    <p className="mb-1">{_.get(data, 'loading_rate_type', '-').toUpperCase()}</p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Jenis Rate</b></p>
                    <p className="mb-1">{_.get(data, 'rate_type', '-').toUpperCase()}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perluasan Jaminan</p>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.flood) ? _.get(data, 'flood', '-')[0] : false}>
                            Banjir
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{`${data.flood_percentage || 0}%`}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.eq) ? _.get(data, 'eq', '-')[0] : false}>
                            Gempa
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{`${data.eq_percentage || 0}%`}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.rscc) ? _.get(data, 'rscc', '-')[0] : false}>
                            Huru Hara
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={14}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.ts) ? _.get(data, 'ts', '-')[0] : false}>
                            Terorisme dan Sabotase
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.atpm) ? _.get(data, 'atpm', '-')[0] : false}>
                            Bengkel Authorize
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{`${_.get(data, 'atpm_percentage', '0')}%`}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.tjh) ? _.get(data, 'tjh', '-')[0] : false}>
                            TJH Pihak Ketiga
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{!isEmpty(data.tjh) ? data.tjh_amount : 'Rp. 0,-'}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={6}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.pap) ? _.get(data, 'pap', '-')[0] : false}>
                            PAP
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mt-2">
                          {_.get(data, 'pap_passenger', '-')}
                          {' '}
                          Orang
                        </p>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{!isEmpty(data.tjh) ? data.pap_amount : 'Rp. 0,-'}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.pll) ? _.get(data, 'pll', '-')[0] : false}>
                            PLL
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{!isEmpty(data.tjh) ? data.pll_amount : 'Rp. 0,-'}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={!isEmpty(data.pad) ? _.get(data, 'pad', '-')[0] : false}>
                            PAD
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <p className="mt-2">{!isEmpty(data.tjh) ? data.pad_amount : 'Rp. 0,-'}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0"><b>Rate</b></p>
                        <p className="mb-1">{premi.rate}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">
                          <b>
                            {(() => {
                              switch (parsed.product) {
                                case 'POS01':
                                case 'POS02':
                                case 'POS03':
                                case 'POS04':
                                case 'POS05':
                                  return 'Kontribusi'
                                default:
                                  return 'Premi'
                              }
                            })()}
                          </b>
                        </p>
                        <p className="mb-1">{Helper.currency(premi.premi, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    {(!isEmpty(data.discount_percentage) || !isEmpty(data.discount_currency)) && (
                      <Row gutter={24}>
                        <Col xs={12} md={12}>
                          <b className="mb-0">Diskon Percentage</b>
                          <p>{`${data.discount_percentage}%`}</p>
                        </Col>
                        <Col xs={12} md={12}>
                          <b className="mb-0">Diskon Currency</b>
                          <p>{Helper.currency(data.discount_currency, 'Rp. ', ',-')}</p>
                        </Col>
                      </Row>
                    )}
                    <Row gutter={24}>
                      <Col xs={24} md={8}>
                        <p className="mt-2"><b>Biaya Administrasi</b></p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item className="mb-0">
                          <Checkbox checked={data.print_policy_book} className="cursor-default">
                            Buku Polis
                          </Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mt-2 mb-1">Rp 50.000,-</p>
                        {/* <p className="mt-2">{Helper.currency(premi.admin_fee, 'Rp. ', ',-')}</p> */}
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={8}>
                        <p className="mb-1">Materai</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{Helper.currency(premi.materai, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={16}>
                        <p className="mb-1">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <p className="mb-1">{Helper.currency(data.paymentTotal + (data.print_policy_book ? 50000 : 0), 'Rp. ', ',-')}</p>
                        {/* <p className="mb-1">{Helper.currency(premi.totalPayment, 'Rp. ', ',-')}</p> */}
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Form.Item>
                          <Checkbox checked={!isEmpty(data.aggree) ? _.get(data, 'aggree', '-')[0] : false}>
                            Saya telah membaca dan setuju dengan
                            {' '}
                            <b>Syarat & Ketentuan Mitraca</b>
                            {' '}
                            yang berlaku
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 mr-3"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewOtomate.propTypes = {
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
  data: PropTypes.object,
  premi: PropTypes.object,
}

export default PreviewOtomate
