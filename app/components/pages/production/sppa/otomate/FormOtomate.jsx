/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable default-case */
/* eslint-disable react/jsx-props-no-multi-spaces */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import {
  Row, Col,
  Card, Input,
  Select, Radio,
  Checkbox, Button,
  DatePicker, AutoComplete,
  InputNumber,
} from 'antd'
import _, { isEmpty, capitalize } from 'lodash'
import queryString from 'query-string'
import Hotkeys from 'react-hot-keys'
import Helper from 'utils/Helper'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'
import NumberFormat from 'react-number-format'
import PreviewOtomate from './PreviewOtomate'
import 'styles/app.scss'
import config from 'app/config'

const CreateOtomate = ({
  form, onSubmit, initPremi,
  listBrand, handleSelect,
  listSeries, listPlateCode,
  listYear, floodMotorCar,
  handleRenewal, listIO,
  listCustomer, eqMotorCar, searchCustomer,
  tjhMotorCar, papMotorCar, pllMotorCar,
  padMotorCar, bengkelAuthMotorCar,
  huraMotorCar, terrorismMotorCar,
  setSelectedHolder, selectedHolder,
  setSelectedInsured, selectedInsured,
  setRateTypeLoadingDisabled, rateTypeLoadingDisabled,
  handleSeriesPrice, setVisibleDiscount,
  visibleDiscount, premiPayload,
  setPremiPayload, setSelectedIO,
  match, detailOtomate, visiblePreview,
  setVisiblePreview, submitted, submitData,
  submitPreview, setPremi, hiddenJenisRate,
  rateType, commercial, submitLoading,
  premi, loadAdditional, appendCustomer,
  calcDiscountByCurrency,
  paymentTotal,
  calcDiscountByPercentage, 
}) => {
  const {
    getFieldDecorator, getFieldValue, setFieldsValue, getFieldsValue,
  } = form

  const data = detailOtomate.list

  const parsed = queryString.parse(window.location.search)
  const [maxSeat, setMaxSeat] = useState(4)
  const isMobile = window.innerWidth < 768
  let insuredPeriode = []
  if (getFieldValue('production_year') !== (new Date()).getFullYear()) {
    insuredPeriode = [1, 2]
  } else {
    insuredPeriode = [1, 2, 3]
  }

  let sppaType
  if (!isEmpty(detailOtomate.list)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailOtomate.list.sppa_type
    }
  }

  return (
    <React.Fragment>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />

      <PreviewOtomate
        show={visiblePreview}
        setVisible={setVisiblePreview}
        submitted={submitted}
        data={{
          ...getFieldsValue(),
          match,
          detailOtomate,
          listSeries,
          listBrand,
          paymentTotal,
        }}
        premi={premi}
        handleCancel={() => setVisiblePreview(false)}
        handleSubmit={submitData}
      />

      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">
            {`${match.params.id ? 'Edit' : 'Create'} SPPA Motorcar`}
          </div>
        </Col>
      </Row>
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100" loading={detailOtomate.loading}>
                  <p className="title-card">Informasi Polis</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tipe Polis</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: Helper.fieldRules(['requiredDropdown']),
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Pilih Tipe"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Indicative Offer / Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_renewal', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Search Polis"
                            dataSource={listIO.options.map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}

                            onSelect={(val) => {
                              if (getFieldValue('sppa_type') === 'new') {
                                const data = (listIO.options).find(item => item.io_number === val)
                                setSelectedIO(data)
                                handleSelect('series-car', data.ottomate_vehicle_information.brand_id, 'Series')
                                setTimeout(() => {
                                  initPremi()
                                  loadAdditional(data.ottomate_additional_limit.limits)
                                  setFieldsValue({
                                    middle_policy_number: data.ottomate_vehicle_information.middle_police_number,
                                    rear_vehicle_year: data.ottomate_vehicle_information.end_police_number,
                                    vehicle_use: data.ottomate_vehicle_information.vehicle_use,
                                    license_plate_code: data.ottomate_vehicle_information.license_plate_code,
                                    brand: data.ottomate_vehicle_information.brand_id,
                                    series: data.ottomate_vehicle_information.series_id,
                                    vehicle_category: data.ottomate_vehicle_information.series.category,
                                    production_year: data.ottomate_vehicle_information.production_year,
                                    coverage_value: data.ottomate_premi_calculation.coverage,
                                    additional_accessories_costs: data.ottomate_premi_calculation.additional_accessories_costs,
                                    coverage_total_value: data.ottomate_premi_calculation.additional_accessories_costs + data.ottomate_premi_calculation.coverage,
                                    own_risk_value: data.ottomate_premi_calculation.own_risk_value,
                                    rate_type: data.ottomate_premi_calculation.rate_type,
                                    pap: (data.ottomate_additional_limit.limits || []).includes('pap') ? ['pap'] : [],
                                    flood: (data.ottomate_additional_limit.limits || []).includes('flood') ? ['flood'] : [],
                                    eq: (data.ottomate_additional_limit.limits || []).includes('eq') ? ['eq'] : [],
                                    atpm: (data.ottomate_additional_limit.limits || []).includes('atpm') ? ['atpm'] : [],
                                    ts: (data.ottomate_additional_limit.limits || []).includes('ts') ? ['ts'] : [],
                                    rscc: (data.ottomate_additional_limit.limits || []).includes('rscc') ? ['rscc'] : [],
                                    tjh: (data.ottomate_additional_limit.limits || []).includes('tjh') ? ['tjh'] : [],
                                    pll: (data.ottomate_additional_limit.limits || []).includes('pll') ? ['pll'] : [],
                                    pad: (data.ottomate_additional_limit.limits || []).includes('pad') ? ['pad'] : [],
                                    tjh_amount: data.ottomate_additional_limit.tjh,
                                    eq_percentage: (data.ottomate_additional_limit.limits || []).includes('eq') ? '100%' : '',
                                    flood_percentage: (data.ottomate_additional_limit.limits || []).includes('flood') ? '100%' : '',
                                    pap_amount: data.ottomate_additional_limit.pap_amount,
                                    pad_amount: data.ottomate_additional_limit.pad_amount,
                                    pll_amount: data.ottomate_additional_limit.pll_amount,
                                    atpm_percentage: data.ottomate_additional_limit.atpm_percentage,
                                    pap_passenger: data.ottomate_additional_limit.pap_total,
                                  })

                                  setPremi({
                                    ...premi,
                                    premi: data.ottomate_premi_calculation.premi,
                                    policy_printing_fee: data.ottomate_premi_calculation.policy_printing_fee,
                                    rate: data.ottomate_premi_calculation.rate,
                                    stamp_fee: data.stamp_fee,
                                    total_payment: data.total_payment,
                                    print_policy_book: data.print_policy_book,
                                  })
                                }, 200)
                              } else if (getFieldValue('sppa_type') === 'renewal') {
                                const data = (listIO.options).find(item => item.sppa_number === val)
                                setSelectedIO(data)
                                handleSelect('series-car', data.ottomate_vehicle.brand_id, 'Series')
                                setTimeout(() => {
                                  setFieldsValue({
                                    name_on_policy: data.name_on_policy,
                                    qq: data.qq,
                                    insured_id: data.insured.name,
                                    policy_holder_id: data.policy_holder.name,
                                    start_period: moment(new Date(data.start_period), 'DD MMMM YYYY'),
                                    end_period: moment(new Date(data.end_period), 'DD MMMM YYYY'),
                                    middle_policy_number: data.ottomate_vehicle.middle_police_number,
                                    rear_vehicle_year: data.ottomate_vehicle.end_police_number,
                                    vehicle_use: data.ottomate_vehicle.vehicle_use,
                                    license_plate_code: data.ottomate_vehicle.license_plate_code,
                                    brand: data.ottomate_vehicle.brand_id,
                                    series: data.ottomate_vehicle.series_id,
                                    desc_vehicle: data.ottomate_vehicle.description_series,
                                    vehicle_category: data.ottomate_vehicle.series.category,
                                    production_year: data.ottomate_vehicle.production_year,
                                    color: data.ottomate_vehicle.color,
                                    chassis_number: data.ottomate_vehicle.chassis_number,
                                    machine_number: data.ottomate_vehicle.machine_number,
                                    vehicle_damage_type: data.ottomate_vehicle.vehicle_damage_type,
                                    vehicle_damage_description: data.ottomate_vehicle.vehicle_damage_description,
                                    equipment_type: data.ottomate_vehicle.equipment_type,
                                    equipment_description: data.ottomate_vehicle.equipment_description,
                                    coverage_value: data.ottomate_calculation.coverage,
                                    additional_accessories_costs: data.ottomate_calculation.additional_accessories_costs,
                                    coverage_total_value: data.ottomate_calculation.additional_accessories_costs + data.ottomate_calculation.coverage,
                                    own_risk_value: data.ottomate_calculation.own_risk_value,
                                    rate_type: data.ottomate_calculation.rate_type,
                                  })

                                  setSelectedInsured(data.insured)
                                  setSelectedHolder(data.policy_holder)
                                  setPremi({
                                    ...premi,
                                    coverage: data.ottomate_premi_calculation.coverage,
                                  })

                                  setPremiPayload({
                                    ...premiPayload,
                                    brand_id: data.ottomate_vehicle.brand_id || 0,
                                    series_id: data.ottomate_vehicle.series_id || 0,
                                    rate_type: data.ottomate_calculation.rate_type || '',
                                    production_year: data.ottomate_vehicle.production_year || 0,
                                    license_plate_code: data.ottomate_vehicle.license_plate_code || 0,
                                    insurance_period: moment(data.end_period).format('YYYY') - moment(data.start_period).format('YYYY') || 0,
                                    coverage: data.ottomate_premi_calculation.coverage,
                                  })
                                  initPremi()
                                }, 200)
                              }
                            }}
                            disabled={getFieldValue('sppa_type') === undefined}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Policy Holder</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                          
                        {getFieldDecorator('policy_holder_id', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.policy_holder.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Policy Holder'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Input Policy Holder"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_id')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName}`,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_id: data.name,
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">
                        {(() => {
                          switch (parsed.product) {
                            case 'POS01':
                            case 'POS02':
                            case 'POS03':
                            case 'POS04':
                            case 'POS05':
                              return 'Nama Peserta'
                            default:
                              return 'Insured Name'
                          }
                        })()}
                      </p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer searchCustomer={searchCustomer} onSuccess={data => appendCustomer(data)} />
                        </div>
                        {getFieldDecorator('insured_id', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.insured.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Name'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder={
                              (() => {
                                switch (parsed.product) {
                                  case 'POS01':
                                  case 'POS02':
                                  case 'POS03':
                                  case 'POS04':
                                  case 'POS05':
                                    return 'Input Nama Peserta'
                                  default:
                                    return 'Input Insured Name'
                                }
                              })()
                            }
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_id'),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${getFieldValue('policy_holder_id') || ''} QQ ${data.name}`,
                                  })
                                }

                                setFieldsValue({
                                  insured_id: data.name,
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Name on Policy</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: (detailOtomate.list.name_on_policy || '').toUpperCase(),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Nama"
                            className="field-lg uppercase"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">QQ</p>
                      <Form.Item>
                        {getFieldDecorator('qq', {
                          initialValue: (detailOtomate.list.qq || '').toUpperCase(),
                          rules: Helper.fieldRules(['notSpecial']),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="QQ"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Periode</p>
                      <Form.Item>
                        {getFieldDecorator('period', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.period_in_year : undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Periode"
                            loading={listBrand.loading}
                            disabled={false}
                            onSelect={(value) => {
                              if (getFieldValue('start_period')) {
                                setTimeout(() => initPremi())
                                setFieldsValue({
                                  end_period: moment(getFieldValue('start_period'), 'DD MMMM YYYY').add(value, 'year'),
                                })
                              }
                            }}
                          >
                            {(insuredPeriode || []).map(year => (
                              <Select.Option value={year}>{`${year} Tahun`}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Product</p>
                      {match.params.id
                        ? <p>{`${!isEmpty(detailOtomate.list) ? detailOtomate.list.product.type.name : '-'} - Motorcar - ${!isEmpty(detailOtomate.list) ? detailOtomate.list.product.display_name : '-'}`}</p>
                        : <p>{`${capitalize(parsed.type)} - Motorcar - ${handleProduct(parsed.product)}`}</p>
                      }
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Awal</p>
                      <Form.Item>
                        {getFieldDecorator('start_period', {
                          initialValue: !isEmpty(detailOtomate.list) ? moment(new Date(detailOtomate.list.start_period), 'DD MMMM YYYY') : undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())
                            setFieldsValue({
                              end_period: moment(new Date(e), 'DD MMMM YYYY').add(getFieldValue('period'), 'year'),
                            })

                            return moment(new Date(e), 'DD MMMM YYYY')
                          },
                        })(
                          <DatePicker
                            disabled={getFieldValue('period') === undefined}
                            className="field-lg w-100"
                            placeholder="Periode"
                            disabledDate={current => (current && (current < moment().subtract(0, 'day')))}
                            format="DD MMMM YYYY"
                            allowClear={false}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Akhir</p>
                      <Form.Item>
                        {getFieldDecorator('end_period', {
                          initialValue: !isEmpty(detailOtomate.list) ? moment(new Date(detailOtomate.list.end_period), 'DD MMMM YYYY') : undefined,
                          getValueFromEvent: (e) => {
                            const difference = moment(new Date(e)).diff(getFieldValue('start_period'), 'year')
                            const endDate = moment(getFieldValue('start_period')).add(difference, 'year').add(0, 'day')

                            return endDate
                          },
                        })(
                          <DatePicker
                            disabled
                            className="field-lg w-100"
                            placeholder="Periode"
                            disabledDate={current => (current && (current < moment(getFieldValue('start_period')).subtract(0, 'day').add(1, 'year')))}
                            format="DD MMMM YYYY"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col span={24}>
                <Card className="h-100" loading={detailOtomate.loading}>
                  <p className="title-card">Informasi Kendaraan</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Brand</p>
                      <Form.Item>
                        {getFieldDecorator('brand', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.brand_id : undefined || undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                          getValueFromEvent: (e) => {
                            handleSelect('series-car', e, 'Series')
                            setTimeout(() => initPremi())
                            return e
                          },
                        })(
                          <Select
                            showSearch
                            optionFilterProp="children"
                            className="field-lg"
                            placeholder="Brand"
                            loading={listBrand.loading}
                            disabled={false}
                            onSelect={() => {
                              setFieldsValue({
                                series: '',
                              })
                            }}
                          >
                            {(listBrand.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Series</p>
                      <Form.Item>
                        {getFieldDecorator('series', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.series_id : undefined || undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())
                            setMaxSeat(listSeries.options.find(item => item.value === e).seat - 1)
                            setFieldsValue({
                              coverage_value: '',
                              additional_accessories_costs: '',
                              coverage_total_value: '',
                              vehicle_category: listSeries.options.find(item => item.value === e).category,
                            })

                            return e
                          },
                        })(
                          <Select
                            showSearch
                            optionFilterProp="children"
                            className="field-lg"
                            placeholder="Series"
                            loading={listSeries.loading}
                            disabled={!getFieldValue('brand')}
                            onSelect={(val) => {
                              const data = listSeries.options.find(item => item.value === val)
                              handleSeriesPrice(data.value, getFieldValue('production_year'))
                            }}
                          >
                            {(listSeries.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Desc</p>
                      <Form.Item>
                        {getFieldDecorator('desc_vehicle', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_vehicle.description_series).toUpperCase() : undefined || undefined,
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            className="field-lg uppercase"
                            placeholder="Input Vehicle Description"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Vehicle Category</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_category', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.series.category : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Vehicle Category'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled
                            className="field-lg uppercase"
                            placeholder="Input Vehicle Category"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Production Year</p>
                      <Form.Item>
                        {getFieldDecorator('production_year', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.production_year : undefined || undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                          getValueFromEvent: (e) => {
                            const date = new Date()
                            const getAgeCar = date.getFullYear() - e
                            switch (parsed.product) {
                              case 'PO001':
                              case 'PO002':
                              case 'PO003':
                              case 'PO005':
                              case 'POS01':
                              case 'POS02':
                              case 'POS03':
                              case 'POS05':
                                if (getAgeCar === 6 || getAgeCar === 7) {
                                  setRateTypeLoadingDisabled({
                                    tidak: true,
                                    rate: false,
                                    risiko: false,
                                  })
                                  setFieldsValue({
                                    loading_rate_type: '',
                                    own_risk_value: 0,
                                  })
                                } else if (getAgeCar > 7) {
                                  setRateTypeLoadingDisabled({
                                    tidak: true,
                                    rate: false,
                                    risiko: true,
                                  })
                                  setFieldsValue({
                                    loading_rate_type: 'rate',
                                    own_risk_value: 300000,
                                  })
                                } else {
                                  setRateTypeLoadingDisabled({
                                    tidak: false,
                                    rate: true,
                                    risiko: true,
                                  })
                                  setFieldsValue({
                                    loading_rate_type: 'tidak',
                                    own_risk_value: 300000,
                                  })
                                }
                                break

                              case 'PO004':
                              case 'POS04':
                                if (getAgeCar >= 10) {
                                  setRateTypeLoadingDisabled({
                                    risiko: true,
                                    tidak: true,
                                  })
                                  setFieldsValue({
                                    loading_rate_type: 'rate',
                                    own_risk_value: 300000,
                                  })
                                } else {
                                  setRateTypeLoadingDisabled({
                                    rate: false,
                                    risiko: true,
                                    tidak: false,
                                  })
                                  setFieldsValue({
                                    loading_rate_type: 'tidak',
                                    own_risk_value: 300000,
                                  })
                                }
                                break

                              default:
                                break
                            }

                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Production Year"
                            loading={false}
                            disabled={false}
                            onSelect={(val) => {
                              const data = listYear.options.find(item => item.value === val)
                              handleSeriesPrice(getFieldValue('series'), data.value)
                            }}
                          >
                            {(listYear.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={8}>
                      <p className="mb-0">Plate Code</p>
                      <Form.Item>
                        {getFieldDecorator('license_plate_code', {
                          rules: Helper.fieldRules(['requiredDropdown']),
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.license_plate_code : undefined || undefined,

                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Police Number"
                            loading={false}
                            disabled={false}
                          >
                            {(listPlateCode.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={8}>
                      <p className="mb-0">.</p>
                      <Form.Item>
                        {getFieldDecorator('middle_policy_number', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.middle_police_number : undefined || undefined,
                          rules: Helper.fieldRules(['required', 'number', 'positiveNumber'], 'Nomor tengah'),
                        })(
                          <Input
                            maxLength={4}
                            className="field-lg"
                            placeholder=""
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={8}>
                      <p className="mb-0">.</p>
                      <Form.Item>
                        {getFieldDecorator('rear_vehicle_year', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.end_police_number : undefined || undefined,
                          rules: Helper.fieldRules(['alphabet'], 'Nomor Belakang'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            maxLength={4}
                            className="field-lg uppercase"
                            placeholder=""
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Warna</p>
                      <Form.Item>
                        {getFieldDecorator('color', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_vehicle.color).toUpperCase() : undefined || undefined,
                          rules: Helper.fieldRules(['required', 'notSpecial'], 'Warna'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Pilih Warna"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nomor Rangka</p>
                      <Form.Item>
                        {getFieldDecorator('chassis_number', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_vehicle.chassis_number).toUpperCase() : undefined || undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nomor Rangka'),
                            { pattern: /^[a-zA-Z0-9]+$/, message: '*Tidak boleh special karakter' },
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Pilih Nomor Rangka"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nomor Mesin</p>
                      <Form.Item>
                        {getFieldDecorator('machine_number', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_vehicle.machine_number).toUpperCase() : undefined || undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nomor Mesin'),
                            { pattern: /^[a-zA-Z0-9]+$/, message: '*Tidak boleh special karakter' },
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Pilih Nomor Mesin"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Vehicle Use</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_use', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.vehicle_use : undefined || undefined,
                          rules: Helper.fieldRules(['requiredDropdown']),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              pll_amount: '',
                            })

                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Vehicle Use"
                            loading={false}
                            disabled={false}
                          >
                            <Select.Option value="dinas">DINAS</Select.Option>
                            <Select.Option value="pribadi">PRIBADI</Select.Option>
                            {commercial
                              ? <Select.Option value="komersil">KOMERSIL</Select.Option> : null
                            }
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Perlengkapan</p>
                      <Form.Item>
                        {getFieldDecorator('equipment_type', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.equipment_type : 'standar' || 'standar',
                          rules: Helper.fieldRules(['required']),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              equipment_description: e.target.value === 'standar' ? 'STANDAR' : '',
                            })

                            return e.target.value
                          },
                        })(
                          <Radio.Group
                            size="large"
                            className="w-100"
                          >
                            <Row>
                              <Col xs={24} md={8}>
                                <Radio value="standar">Standar</Radio>
                              </Col>
                              <Col xs={24} md={12}>
                                <Radio value="tidak">Tidak</Radio>
                              </Col>
                            </Row>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Keterangan Perlengkapan</p>
                      <Form.Item>
                        {getFieldDecorator('equipment_description', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.equipment_description : 'STANDAR' || undefined,
                          rules: [...Helper.fieldRules(getFieldValue('equipment_type') === 'standar' ? ['notSpecial'] : ['required', 'notSpecial'])],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Deskripsi"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Kerusakan Kendaraan saat survey penutupan</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_damage_type', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.vehicle_damage_type : 'tidak_ada' || 'tidak_ada',
                          rules: Helper.fieldRules(['required']),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              vehicle_damage_description: e.target.value === 'tidak_ada' ? 'TIDAK ADA' : '',
                            })

                            return e.target.value
                          },
                        })(
                          <Radio.Group
                            size="large"
                            className="w-100"
                          >
                            <Row>
                              <Col xs={24} md={8}>
                                <Radio value="tidak_ada">Tidak Ada</Radio>
                              </Col>
                              <Col xs={24} md={12}>
                                <Radio value="ada">Ada</Radio>
                              </Col>
                            </Row>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Keterangan Kerusakan</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_damage_description', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_vehicle.vehicle_damage_description : 'TIDAK ADA' || undefined,
                          rules: [...Helper.fieldRules(getFieldValue('vehicle_damage_type') === 'tidak_ada' ? ['notSpecial'] : ['required', 'notSpecial'])],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Deskripsi"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100" loading={detailOtomate.loading}>
                  <p className="title-card">Perhitungan Premi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nilai Pertanggungan</p>
                      <Form.Item>
                        {getFieldDecorator('coverage_value', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.coverage : 0 || 0,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nilai Pertanggungan'),
                            {
                              type: 'number',
                              min: premi.sum_insured == 50000000 ? 50000000 : 100000000,
                              transform: value => Helper.replaceNumber(value),
                              message: premi.sum_insured == 50000000 ? '*Besar Nilai Pertanggungan Minimum Rp. 50.000.000,-' : '*Besar Nilai Pertanggungan Minimum Rp. 100.000.000,-',
                            },
                          ],
                        })(
                          <NumberFormat
                            className="ant-input field-lg currency"
                            thousandSeparator
                            prefix="Rp. "
                            readOnly={getFieldValue('coverage_value') === undefined}
                            placeholder="- IDR -"
                            onValueChange={(e) => {
                              if (Number(e.floatValue) || (e.floatValue === '')) {
                                const getCoverage = Number(e.floatValue)
                                const getAdditional = Helper.replaceNumber(getFieldValue('additional_accessories_costs')) || 0

                                setFieldsValue({
                                  coverage_value: getCoverage,
                                  coverage_total_value: getAdditional + getCoverage,
                                })

                                setTimeout(() => initPremi())
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nilai Pertanggungan Perlengkapan</p>
                      <Form.Item>
                        {getFieldDecorator('additional_accessories_costs', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.additional_accessories_costs || 0 : 0,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Pertanggungan Perlengkapan'),
                            {
                              type: 'number',
                              max: Helper.replaceNumber(getFieldValue('coverage_value')) * 10 / 100,
                              transform: value => Helper.replaceNumber(value),
                            },
                          ],
                        })(
                          <NumberFormat
                            className="ant-input field-lg currency"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            onValueChange={(e) => {
                              if (Number(e.floatValue) > Helper.replaceNumber(getFieldValue('coverage_value')) * 10 / 100) {
                                setTimeout(() => {
                                  setFieldsValue({
                                    additional_accessories_costs: Helper.replaceNumber(getFieldValue('coverage_value')) * 10 / 100,
                                  })
                                }, 1000)
                              }
                              if (Number(e.floatValue) || (e.floatValue === '')) {
                                const getCoverageValueEquip = Number(e.floatValue)
                                const getCoverageValue = Helper.replaceNumber(getFieldValue('coverage_value')) || 0

                                setFieldsValue({
                                  coverage_total_value: getCoverageValue + getCoverageValueEquip,
                                  additional_accessories_costs: getCoverageValueEquip,
                                })

                                setTimeout(() => initPremi())
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nilai Total Pertanggungan</p>
                      <Form.Item>
                        {getFieldDecorator('coverage_total_value', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.additional_accessories_costs + detailOtomate.list.ottomate_calculation.coverage : 0 || 0,
                          rules: [
                            {
                              validator: (rule, value) => {
                                switch (parsed.product) {
                                  case 'PO001':
                                  case 'PO002':
                                  case 'PO003':
                                  case 'POS01':
                                  case 'POS02':
                                  case 'POS03':
                                    // eslint-disable-next-line prefer-promise-reject-errors
                                    if (Helper.replaceNumber(value) < 100000000) return Promise.reject('*Besar Nilai Coverage Minimum Rp. 100.000.000,-')
                                    break
                                  case 'PO004':
                                  case 'POS04':
                                  case 'PO005':
                                  case 'POS05':
                                    // eslint-disable-next-line prefer-promise-reject-errors
                                    if (Helper.replaceNumber(value) < 50000000) return Promise.reject('*Besar Nilai Coverage Minimum Rp. 50.000.000,-')
                                    break
                                  default:
                                    return Promise.resolve()
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <NumberFormat
                            className="ant-input field-lg currency"
                            thousandSeparator
                            prefix="Rp. "
                            readOnly
                            placeholder="- IDR -"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nilai Resiko Sendiri</p>
                      <Form.Item>
                        {getFieldDecorator('own_risk_value', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.own_risk_value : 300000 || 0,
                          rules: [
                            ...Helper.fieldRules(['required']),
                          ],
                        })(
                          <NumberFormat
                            readOnly
                            className="ant-input field-lg currency"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  {!rateTypeLoadingDisabled.hiddenType && (
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">Loading Rate</p>
                        <Form.Item>
                          {getFieldDecorator('loading_rate_type', {
                            initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.loading_rate_type : '',
                            rules: Helper.fieldRules(['required'], 'Loading Rate'),

                            getValueFromEvent: (e) => {
                              const date = new Date()
                              const getAgeCar = date.getFullYear() - getFieldValue('production_year')
                              switch (parsed.product) {
                                case 'PO001':
                                case 'PO002':
                                case 'PO003':
                                case 'PO005':
                                case 'POS01':
                                case 'POS02':
                                case 'POS03':
                                case 'POS05':
                                  if (getAgeCar === 6 || getAgeCar === 7) {
                                    if (e.target.value === 'rate') {
                                      setFieldsValue({
                                        own_risk_value: 300000,
                                      })
                                    } else if (e.target.value === 'resiko_sendiri') {
                                      setFieldsValue({
                                        own_risk_value: 500000,
                                      })
                                    }
                                  }
                                  break

                                default:
                                  break
                              }

                              if (e.target.value === 'rate' || e.target.value === 'tidak') {
                                setFieldsValue({
                                  own_risk_value: 300000,
                                })
                              } else if (e.target.value === 'resiko_sendiri') {
                                setFieldsValue({
                                  own_risk_value: 500000,
                                })
                              }

                              setTimeout(() => initPremi())

                              return e.target.value
                            },
                          })(
                            <Radio.Group
                              size="large"
                              className="w-100"
                            >
                              <Row>
                                <Col xs={24} md={8}>
                                  <Radio value="tidak" disabled={rateTypeLoadingDisabled.tidak}>Tidak</Radio>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Radio value="rate" disabled={rateTypeLoadingDisabled.rate}>Rate</Radio>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Radio value="resiko_sendiri" disabled={rateTypeLoadingDisabled.risiko}>Resiko Sendiri</Radio>
                                </Col>
                              </Row>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  )}
                  {!hiddenJenisRate && (
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-0">Jenis Rate</p>
                        <Form.Item>
                          {getFieldDecorator('rate_type', {
                            initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_calculation.rate_type : rateType || undefined,
                            rules: Helper.fieldRules(['required']),
                            getValueFromEvent: (e) => {
                              setTimeout(() => initPremi())

                              return e.target.value
                            },
                          })(
                            <Radio.Group
                              size="large"
                              className="w-100"
                            >
                              <Row>
                                <Col xs={24} md={8}>
                                  <Radio value="bottom">Batas Bawah</Radio>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Radio value="middle">Batas Tengah</Radio>
                                </Col>
                                <Col xs={24} md={8}>
                                  <Radio value="top">Batas Atas</Radio>
                                </Col>
                              </Row>
                            </Radio.Group>,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  )}
                </Card>
              </Col>
              <Col span={24}>
                <Card className="h-100" loading={detailOtomate.loading}>
                  <p className="title-card">Perluasan Jaminan</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('flood', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('flood') ? ['flood'] : [] : floodMotorCar.floodInit,
                          getValueFromEvent: (e) => {
                            if (!(getFieldValue('flood').length === 0)) {
                              switch (parsed.product) {
                                case 'PO002':
                                case 'POS02':
                                case 'PO004':
                                case 'POS04':
                                case 'PO005':
                                case 'POS05':
                                  setFieldsValue({
                                    flood_percentage: 0,
                                  })
                                  break
                                default:
                                  setFieldsValue({
                                    flood_percentage: 10,
                                  })
                              }
                            } else {
                              switch (parsed.product) {
                                case 'PO002':
                                case 'POS02':
                                case 'PO004':
                                case 'POS04':
                                case 'PO005':
                                case 'POS05':
                                  setFieldsValue({
                                    flood_percentage: 100,
                                  })
                                  break
                                default:
                                  setFieldsValue({
                                    flood_percentage: 10,
                                  })
                              }
                            }

                            setTimeout(() => initPremi('flood', ...e))

                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="flood"
                              disabled={floodMotorCar.disabled}
                            >
                              Banjir
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('flood_percentage', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_additional_limit.flood_percentage : floodMotorCar.initPercent,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Banjir"
                            loading={floodMotorCar.loading}
                            disabled={getFieldValue('flood').length === 0 || floodMotorCar.disablePercantage}
                          >
                            {(floodMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('eq', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('eq') ? ['eq'] : [] : eqMotorCar.eqInit,
                          getValueFromEvent: (e) => {
                            if (!(getFieldValue('eq').length === 0)) {
                              switch (parsed.product) {
                                case 'PO002':
                                case 'POS02':
                                case 'PO004':
                                case 'POS04':
                                case 'PO005':
                                case 'POS05':
                                  setFieldsValue({
                                    eq_percentage: 0,
                                  })
                                  break
                                default:
                                  setFieldsValue({
                                    eq_percentage: 10,
                                  })
                              }
                            } else {
                              switch (parsed.product) {
                                case 'PO002':
                                case 'POS02':
                                case 'PO004':
                                case 'POS04':
                                case 'PO005':
                                case 'POS05':
                                  setFieldsValue({
                                    eq_percentage: 100,
                                  })
                                  break
                                default:
                                  setFieldsValue({
                                    eq_percentage: 10,
                                  })
                              }
                            }

                            setTimeout(() => initPremi('eq', ...e))

                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="eq"
                              disabled={eqMotorCar.disabled}
                            >
                              Gempa
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('eq_percentage', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_additional_limit.eq_percentage : eqMotorCar.initPercent,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Gempa"
                            loading={eqMotorCar.loading}
                            disabled={getFieldValue('eq').length === 0 || eqMotorCar.disablePercantage}
                          >
                            {(eqMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={11}>
                      <Form.Item className="m-0">
                        {getFieldDecorator('rscc', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('rscc') ? ['rscc'] : [] : huraMotorCar.rsccInit,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi('rscc', ...e))
                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="rscc"
                              disabled={huraMotorCar.disabled}
                              loading={huraMotorCar.loading}
                            >
                              Huru Hara
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={13}>
                      <Form.Item>
                        {getFieldDecorator('ts', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('ts') ? ['ts'] : [] : terrorismMotorCar.tsInit,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi('ts', ...e))

                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="ts"
                              disabled={terrorismMotorCar.disabled}
                              loading={terrorismMotorCar.loading}
                            >
                              Terorisme dan Sabotase
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  {bengkelAuthMotorCar.bengkelNotHide ? (
                    <Row gutter={24}>
                      <Col xs={24} md={12}>
                        <Form.Item>
                          {getFieldDecorator('atpm', {
                            initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('atpm') ? ['atpm'] : [] : bengkelAuthMotorCar.bengkelInit,
                            getValueFromEvent: (e) => {
                              setTimeout(() => initPremi('atpm', ...e))
                              if (isEmpty(e)) {
                                setFieldsValue({
                                  atpm_percentage: 0,
                                })
                              }

                              return e
                            },
                          })(
                            <Checkbox.Group>
                              <Checkbox
                                value="atpm"
                                disabled={bengkelAuthMotorCar.disabled}
                              >
                                Bengkel Authorize
                              </Checkbox>
                            </Checkbox.Group>,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={12}>
                        <Form.Item>
                          {getFieldDecorator('atpm_percentage', {
                            initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_additional_limit.atpm_percentage : bengkelAuthMotorCar.atpmPercentage,
                            rules: [
                              ...Helper.fieldRules(['number'], 'Nilai'),
                              {
                                type: 'number', max: 1, transform: value => (value ? Number(value) : ''), message: 'Percentage maximal 1%',
                              },
                            ],
                            getValueFromEvent: (e) => {
                              setTimeout(() => initPremi())

                              return e.target.value
                            },
                          })(
                            <Input
                              placeholder="- Dalam % -"
                              className="field-lg"
                              suffix="%"
                              maxLength={4}
                              disabled={bengkelAuthMotorCar.disabled || isEmpty(getFieldValue('atpm'))}
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  ) : null}
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('tjh', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('tjh') ? ['tjh'] : [] : tjhMotorCar.tjhInit,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi('tjh', ...e))
                            if (!(getFieldValue('tjh').length === 0)) {
                              setFieldsValue({
                                tjh_amount: 0,
                              })
                            } else {
                              setFieldsValue({
                                tjh_amount: tjhMotorCar.options[0].value,
                              })
                            }
                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="tjh"
                              disabled={tjhMotorCar.disabled}
                            >
                              TJH Pihak Ketiga
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('tjh_amount', {
                          initialValue: !isEmpty(detailOtomate.list) ? Helper.currency(detailOtomate.list.ottomate_additional_limit.tjh, 'Rp. ', ',-') : Helper.currency(tjhMotorCar.initAmount, 'Rp. ', ',-'),
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            loading={tjhMotorCar.loading}
                            disabled={getFieldValue('tjh').length === 0}
                          >
                            {(tjhMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{Helper.currency(item.label, 'Rp. ', ',-')}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={6}>
                      <Form.Item>
                        {getFieldDecorator('pap', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('pap') ? ['pap'] : [] : papMotorCar.papInit,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi('pap', ...e))
                            setTimeout(() => initPremi('pad', 'pad'))
                            if (!(getFieldValue('pap').length === 0)) {
                              setFieldsValue({
                                pap_amount: 0,
                                pap_passenger: 1,
                              })
                            } else {
                              setFieldsValue({
                                pap_amount: papMotorCar.options[0].value,
                                pad_amount: padMotorCar.options[0].value,
                                pad: 'pad',
                              })
                            }

                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="pap"
                              disabled={papMotorCar.disabled}
                            >
                              PAP
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <Form.Item>
                        {getFieldDecorator('pap_passenger', {
                          initialValue: !isEmpty(detailOtomate.list) ? detailOtomate.list.ottomate_additional_limit.pap_total : papMotorCar.papPassenger,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            if (Number(e.target.value) > maxSeat) {
                              return maxSeat
                            }

                            if (Number(e.target.value) < 0) {
                              return 0
                            }

                            return e.target.value
                          },
                        })(
                          <Input
                            type="number"
                            min={(() => {
                              switch (parsed.product) {
                                case 'PO004':
                                case 'PO005':
                                case 'POS04':
                                case 'POS05':
                                  return 1
                                default:
                                  return 4
                              }
                            })()}
                            max={maxSeat}
                            maxLength={1}
                            placeholder=""
                            className="field-lg"
                            disabled={getFieldValue('pap').length === 0}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('pap_amount', {
                          initialValue: !isEmpty(detailOtomate.list) ? Helper.currency(detailOtomate.list.ottomate_additional_limit.pap_amount, 'Rp. ', ',-') : Helper.currency(papMotorCar.initAmount, 'Rp. ', ',-'),
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            loading={papMotorCar.loading}
                            disabled={getFieldValue('pap').length === 0}
                          >
                            {(papMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{Helper.currency(item.label, 'Rp. ', ',-')}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row><p style={{ color: '#D60403' }}>{`Maks. Penumpang ${maxSeat}`}</p></Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('pll', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('pll') ? ['pll'] : [] : pllMotorCar.pllInit,
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="pll"
                              className={!pllMotorCar.pllInit ? 'checkedDisabled' : ''}
                              disabled={pllMotorCar.disabled || (getFieldValue('vehicle_use') !== 'komersil')}
                              onChange={(e) => {
                                setTimeout(() => initPremi('pll', ...e))
                                if (!(getFieldValue('pll').length === 0)) {
                                  setFieldsValue({
                                    pll_amount: 0,
                                  })
                                } else if (!isEmpty(pllMotorCar.options)) {
                                  setFieldsValue({
                                    pll_amount: pllMotorCar.options[0].value,
                                  })
                                }
                              }}
                            >
                              PLL
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('pll_amount', {
                          initialValue: !isEmpty(detailOtomate.list) ? Helper.currency(detailOtomate.list.ottomate_additional_limit.pll_amount, 'Rp. ', ',-') : Helper.currency(pllMotorCar.initAmount, 'Rp. ', ',-'),
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            loading={pllMotorCar.loading}
                            disabled={pllMotorCar.disabled || (getFieldValue('vehicle_use') !== 'komersil') || getFieldValue('pll').length === 0}
                          >
                            {(pllMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{Helper.currency(item.label, 'Rp. ', ',-')}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('pad', {
                          initialValue: !isEmpty(detailOtomate.list) ? (detailOtomate.list.ottomate_additional_limit.limits || []).includes('pad') ? ['pad'] : [] : padMotorCar.padInit,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi('pad', ...e))
                            setTimeout(() => initPremi('pad', 'pad'))
                            if (!(getFieldValue('pad').length === 0)) {
                              setFieldsValue({
                                pad_amount: 0,
                                pap: '',
                              })
                            } else {
                              setFieldsValue({
                                pad_amount: padMotorCar.options[0].value,
                              })
                            }
                            return e
                          },
                        })(
                          <Checkbox.Group>
                            <Checkbox
                              value="pad"
                              disabled={padMotorCar.disabled}
                            >
                              PAD
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <Form.Item>
                        {getFieldDecorator('pad_amount', {
                          initialValue: !isEmpty(detailOtomate.list) ? Helper.currency(detailOtomate.list.ottomate_additional_limit.pad_amount, 'Rp. ', ',-') : Helper.currency(padMotorCar.initAmount, 'Rp. ', ',-'),
                          rules: !isEmpty(getFieldValue('pad')) ? Helper.fieldRules(['required']) : null,
                          getValueFromEvent: (e) => {
                            setTimeout(() => initPremi())

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            loading={padMotorCar.loading}
                            disabled={getFieldValue('pad').length === 0}
                          >
                            {(padMotorCar.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{Helper.currency(item.label, 'Rp. ', ',-')}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24} className="mb-3">
                    <Col xs={24} md={24}>
                      <p className="mb-1">Rate</p>
                      <p className="mb-0"><strong>{premi.rate}%</strong></p>
                    </Col>
                  </Row>
                  <Row gutter={24} className="mb-2">
                    <Col xs={24} md={24}>
                      <p className="mb-1">
                        {(() => {
                          switch (parsed.product) {
                            case 'POS01':
                            case 'POS02':
                            case 'POS03':
                            case 'POS04':
                            case 'POS05':
                              return 'Kontribusi'
                            default:
                              return 'Premi'
                          }
                        })()}
                      </p>
                      <p className="mb-0"><strong>{Helper.currency(premi.premi, 'Rp. ', ',-')}</strong></p>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-0">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                        initialValue: _.get(detailOtomate, 'list.ottomate_calculation.discount_percentage'),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => initPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_currency', {
                          initialValue: parseFloat(premi.discount_currency).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => initPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                      {getFieldDecorator('print_policy_book', {
                          initialValue: _.get(detailOtomate, 'list.print_policy_book', undefined),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              print_policy_book: e.target.checked,
                            })

                            return e.target.checked
                          },
                        })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                            onChange={() => setTimeout(() => initPremi())}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={7}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9} />
                    <Col xs={24} md={8}>
                      <p>Materai</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={17}>
                      <p>Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={7}>
                      <p>
                        {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: Helper.fieldRules(['required'], 'Aggree'),
                        })(
                          <Checkbox.Group>
                            <Checkbox value="aggree">
                              Saya telah membaca dan setuju dengan
                              &nbsp;
                              <u>
                                <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                  <b>Syarat & Ketentuan Mitraca</b>
                                </a>
                              </u>
                              &nbsp;
                              yang berlaku
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]} className="mb-5">
              <Col xs={24} md={24}>
                <Button
                  ghost
                  type="primary"
                  loading={submitLoading}
                  disabled={submitLoading}
                  onClick={onSubmit}
                  className="button-lg w-50"
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

CreateOtomate.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  listYear: PropTypes.object,
  listBrand: PropTypes.object,
  listSeries: PropTypes.object,
  handleSelect: PropTypes.func,
  eqMotorCar: PropTypes.object,
  tjhMotorCar: PropTypes.object,
  padMotorCar: PropTypes.object,
  pllMotorCar: PropTypes.object,
  papMotorCar: PropTypes.object,
  floodMotorCar: PropTypes.object,
  listPlateCode: PropTypes.object,
  handleRenewal: PropTypes.func,
  listIO: PropTypes.object,
  listCustomer: PropTypes.object,
  rateTypeLoadingDisabled: PropTypes.object,
  setRateTypeLoadingDisabled: PropTypes.func,
  bengkelAuthMotorCar: PropTypes.object,
  premi: PropTypes.object,
  initPremi: PropTypes.func,
  setSelectedHolder: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  selectedHolder: PropTypes.object,
  selectedInsured: PropTypes.object,
  huraMotorCar: PropTypes.object,
  terrorismMotorCar: PropTypes.object,
  handleSeriesPrice: PropTypes.func,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  premiPayload: PropTypes.object,
  setPremiPayload: PropTypes.func,
  setSelectedIO: PropTypes.func,
  match: PropTypes.object,
  detailOtomate: PropTypes.object,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.func,
  submitData: PropTypes.func,
  submitPreview: PropTypes.func,
  setPremi: PropTypes.func,
  rateType: PropTypes.string,
  submitLoading: PropTypes.bool,
  commercial: PropTypes.bool,
  hiddenJenisRate: PropTypes.bool,
  loadAdditional: PropTypes.func,
  searchCustomer: PropTypes.func,
  appendCustomer: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
  paymentTotal: PropTypes.any,
}

export default CreateOtomate
