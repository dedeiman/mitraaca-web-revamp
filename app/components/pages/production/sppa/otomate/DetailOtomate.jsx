import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Descriptions,
  Checkbox, Tag,
} from 'antd'
import history from 'utils/history'
import moment from 'moment'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import { isEmpty } from 'lodash'
import { LeftOutlined } from '@ant-design/icons'

const DetailOtomate = ({
  detailOtomate,
}) => {
  const parsed = queryString.parse(window.location.search)
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Detail SPPA Motorcar</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailOtomate.loading}>
                {
                  !detailOtomate.loading
                  && (
                  <>
                    <p className="title-card">Informasi Polis</p>
                    <Descriptions layout="vertical" colon={false} column={2}>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Indicative Offer" className="px-1 profile-detail pb-0">
                        {!isEmpty(detailOtomate.list.indicative_offer) ? detailOtomate.list.indicative_offer.io_number : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Pemegang Polis" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.policy_holder.name.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Nama tertanggung" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.insured.name.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Nama pada Polis" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.name_on_policy.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="QQ" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.qq.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Produk" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.product.display_name || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Awal" className="px-1 profile-detail pb-0">
                        {detailOtomate.list ? moment(detailOtomate.list.start_period).format('DD MMMM YYYY') : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Akhir" className="px-1 profile-detail pb-0">
                        {detailOtomate.list ? moment(detailOtomate.list.end_period).format('DD MMMM YYYY') : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </>
                  )
                }
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailOtomate.loading}>
                {
                  !detailOtomate.loading
                  && (
                  <>
                    <p className="title-card">Informasi Kendaraan</p>
                    <Descriptions layout="vertical" colon={false} column={2}>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Merk" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.brand.name || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Model Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.series.name || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Deskripsi Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.description_series || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Kategori Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.series.category || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Tahun Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.production_year || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Nomor Polisi Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.license_plate_code || ''}
                        {' '}
                        {detailOtomate.list.ottomate_vehicle.middle_police_number || ''}
                        {' '}
                        {detailOtomate.list.ottomate_vehicle.end_police_number || ''}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Warna" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.color || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Nomor Rangka" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.chassis_number || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Nomor Mesin" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.machine_number || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Penggunaan Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.vehicle_use.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Perlengkapan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.equipment_type.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Keterangan Perlengkapan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.equipment_description || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Kerusakan Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.vehicle_damage_type.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Keterangan Kerusakan Kendaraan" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_vehicle.vehicle_damage_description || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailOtomate.loading}>
                {
                  !detailOtomate.loading
                  && (
                  <>
                    <p className="title-card">Perhitungan Premi</p>
                    <Descriptions layout="vertical" colon={false} column={2}>
                      <Descriptions.Item span={2} label="Nilai Pertanggungan" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_calculation.coverage, 'Rp. ', ',-') || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={2} label="Nilai Pertanggungan Perlengkapan" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_calculation.additional_accessories_costs, 'Rp. ', ',-') || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={2} label="Nilai Total Pertanggungan" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_calculation.coverage + detailOtomate.list.ottomate_calculation.additional_accessories_costs, 'Rp. ', ',-') || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={2} label="Nilai Resiko Sendiri" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_calculation.own_risk_value, 'Rp. ', ',-') || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Tipe Loading" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_calculation.loading_rate_type.toUpperCase() || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Jenis Rate" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_calculation.rate_type.toUpperCase() || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </>
                  )
                }
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="" loading={detailOtomate.loading}>
                {
                  !detailOtomate.loading
                  && (
                  <>
                    <p className="title-card">Perluasan Jaminan</p>
                    <Descriptions layout="vertical" colon={false} column={2}>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('flood')}>Banjir</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {`${detailOtomate.list.ottomate_additional_limit.flood_percentage}%`}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('eq')}>Gempa</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {`${detailOtomate.list.ottomate_additional_limit.eq_percentage}%`}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('rscc')}>Hura Hura</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('ts')}>Terorisme dan Sabotase</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('atpm')}>Bengkel Authorize</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {`${detailOtomate.list.ottomate_additional_limit.atpm_percentage}%`}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('tjh')}>TJH Pihak Ketiga</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_additional_limit.tjh, 'Rp. ', ',-')}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('pap')}>PAP</Checkbox>
                        {' '}
                        {`${detailOtomate.list.ottomate_additional_limit.pap_total} Orang`}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_additional_limit.pap_amount, 'Rp. ', ',-')}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('pll')}>PLL</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_additional_limit.pll_amount, 'Rp. ', ',-')}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        <Checkbox disabled checked={(detailOtomate.list.ottomate_additional_limit.limits || []).includes('pad')}>PAD</Checkbox>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_additional_limit.pad_amount, 'Rp. ', ',-')}
                      </Descriptions.Item>
                      <Descriptions.Item span={2} label="Rate" className="px-1 profile-detail pb-0">
                        {detailOtomate.list.ottomate_calculation.rate || '0'}
                      </Descriptions.Item>
                      {(() => {
                        switch (parsed.product) {
                          case 'POS01':
                          case 'POS02':
                          case 'POS03':
                          case 'POS04':
                          case 'POS05':
                            return (
                              <Descriptions.Item span={2} label="Kontribusi" className="px-1 profile-detail pb-0">
                                {Helper.currency(detailOtomate.list.ottomate_calculation.premi, 'Rp. ', ',-') || '-'}
                              </Descriptions.Item>
                            )
                          default:
                            return (
                              <Descriptions.Item span={2} label="Premi" className="px-1 profile-detail pb-0">
                                {Helper.currency(detailOtomate.list.ottomate_calculation.premi, 'Rp. ', ',-') || '-'}
                              </Descriptions.Item>
                            )
                        }
                      })()}
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Diskon Currency" className="px-1 profile-detail pb-0">
                        {Helper.currency(detailOtomate.list.ottomate_calculation.discount_currency || '0', 'Rp. ', ',-')}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Diskon Percentage" className="px-1 profile-detail pb-0">
                        {`${detailOtomate.list.ottomate_calculation.discount_percentage || 0}%`}
                      </Descriptions.Item>
                    </Descriptions>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mt-2"><b>Biaya Administrasi</b></p>
                      </Col>
                      <Col xs={24} md={7}>
                        <Checkbox disabled className="mt-2 cursor-default" checked={detailOtomate.list.print_policy_book} disabled>
                          Buku Polis
                        </Checkbox>
                      </Col>
                      <Col xs={24} md={7}>
                        <p className="mt-2 text-dark fw-bold">{Helper.currency(detailOtomate.list.policy_printing_fee, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10} />
                      <Col xs={24} md={7}>
                        <p className="mb-1">Materai</p>
                      </Col>
                      <Col xs={24} md={7}>
                        <strong className="mb-1 text-dark fw-bold">{Helper.currency(detailOtomate.list.stamp_fee || '0', 'Rp. ', ',-')}</strong>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(detailOtomate.list.total_payment, 'Rp. ', ',-') || '-'}</b></p>
                      </Col>
                    </Row>
                  </>
                  )
                }
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${detailOtomate.list.status}/${detailOtomate.list.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" disabled={detailOtomate.list.status === 'complete' || detailOtomate.list.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${detailOtomate.list.id}?product=${detailOtomate.list.product.code}&product_id=${detailOtomate.list.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailOtomate.propTypes = {
  detailOtomate: PropTypes.object,
}

export default DetailOtomate
