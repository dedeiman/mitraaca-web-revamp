import React from 'react'
import { Descriptions } from 'antd'
import PropTypes from 'prop-types'
import moment from 'moment'
import Helper from 'utils/Helper'
import SignatureCanvas from 'react-signature-canvas'

const ConfirmOtomate = ({
  detailSPPA,
  isMobile,
  signature,
}) => (
  <Descriptions layout="vertical" colon={false} column={2}>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Tipe SPPA" className="px-1 profile-detail pb-0">
      {detailSPPA.sppa_type.toUpperCase() || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Pemegang Polis" className="px-1 profile-detail pb-0">
      {detailSPPA.policy_holder.name.toUpperCase() || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Nama tertanggung" className="px-1 profile-detail pb-0">
      {detailSPPA.insured.name.toUpperCase() || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Nama pada Polis" className="px-1 profile-detail pb-0">
      {detailSPPA.name_on_policy.toUpperCase() || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="QQ" className="px-1 profile-detail pb-0">
      {detailSPPA.qq.toUpperCase() || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Produk" className="px-1 profile-detail pb-0">
      {detailSPPA.product.type.name || '-'}
      {' - Motorcar - '}
      {detailSPPA.product.display_name || '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Awal" className="px-1 profile-detail pb-0">
      {detailSPPA ? moment(detailSPPA.start_period).format('DD MMMM YYYY') : '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Akhir" className="px-1 profile-detail pb-0">
      {detailSPPA ? moment(detailSPPA.end_period).format('DD MMMM YYYY') : '-'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Rate" className="px-1 profile-detail pb-0">
      {detailSPPA.ottomate_calculation.rate || '-'}
    </Descriptions.Item>
    {detailSPPA.product.type.name === 'Konvensional'
    && (
      <Descriptions.Item span={isMobile ? 2 : 1} label="Premi" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.ottomate_calculation.premi, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
    )
    }
    {detailSPPA.product.type.name === 'Syariah'
    && (
      <Descriptions.Item span={isMobile ? 2 : 1} label="Kontribusi" className="px-1 profile-detail pb-0">
        {Helper.currency(detailSPPA.ottomate_calculation.premi, 'Rp. ', ',-') || '0'}
      </Descriptions.Item>
    )
    }
    <Descriptions.Item span={isMobile ? 2 : 1} label="Diskon Currency" className="px-1 profile-detail pb-0">
      {Helper.currency(detailSPPA.ottomate_calculation.discount_currency, 'Rp. ', ',-') || '0'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Buku Polis" className="px-1 profile-detail pb-0">
      {Helper.currency(detailSPPA.policy_printing_fee, 'Rp. ', ',-') || '0'}
    </Descriptions.Item>
    <Descriptions.Item span={isMobile ? 2 : 1} label="Materai" className="px-1 profile-detail pb-0">
      {Helper.currency(detailSPPA.stamp_fee, 'Rp. ', ',-') || '0'}
    </Descriptions.Item>
    <Descriptions.Item span={2} label="Total Pembayaran" className="px-1 profile-detail pb-0">
      {Helper.currency(detailSPPA.total_payment, 'Rp. ', ',-') || '0'}
    </Descriptions.Item>
    {detailSPPA.product.type.name === 'Konvensional'
      && (
      <Descriptions.Item span={2} label="Pernyataan Tertanggung" className="px-1 profile-detail pb-0">
        <ol>
          <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
          <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh penanggung</li>
          <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari penanggung.</li>
          <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT.</li>
          <li style={{ textAlign: 'justify' }}>Mengerti bahwa polis akan diterbitkan setelah SPPA di isi dan ditandatangani serta melampirkan Photocopy STNK</li>
        </ol>
      </Descriptions.Item>
      )
    }
    {detailSPPA.product.type.name === 'Syariah'
      && (
      <Descriptions.Item span={2} label="Pernyataan Peserta" className="px-1 profile-detail pb-0">
        <ol>
          <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
          <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh pengelola.</li>
          <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari pengelola.</li>
          <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT. Asuransi Central Asia melalui Virtual Account yang tercantum pada nota premi.</li>
        </ol>
      </Descriptions.Item>
      )
    }
    <Descriptions.Item span={2} label="Tanda Tangan Tertanggung" className="px-1 profile-detail pb-0 text-center">
      <SignatureCanvas
        penColor="black"
        canvasProps={{ width: 300, height: 300, className: 'sigCanvas' }}
        /*eslint-disable */
        ref={ref => signature.data = ref}
        /* eslint-enable */
      />
    </Descriptions.Item>
  </Descriptions>
)


ConfirmOtomate.propTypes = {
  detailSPPA: PropTypes.object,
  isMobile: PropTypes.bool,
  signature: PropTypes.object,
}

export default ConfirmOtomate
