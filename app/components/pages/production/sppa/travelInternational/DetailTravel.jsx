/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button,
  Checkbox, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import history from 'utils/history'
import moment from 'moment'
import Helper from 'utils/Helper'
import { LeftOutlined } from '@ant-design/icons'

const DetailTravel = ({
  detailData, documentData,
}) => {
  const data = detailData.list

  let typeSPPAText = '-'
  switch (data.sppa_type) {
    case 'new':
      typeSPPAText = 'New'
      break
    case 'renewal':
      typeSPPAText = 'Renewal'
      break
    default:
      break
  }

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail SPPA ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tipe SPPA" className="px-1 profile-detail pb-0">
                        {typeSPPAText}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Indicative Offer/Renewal" className="px-1 profile-detail pb-0">
                        {_.get(data, 'indicative_offer.io_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Pemegang Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'policy_holder.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                        {_.get(data, 'insured.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'name_on_policy', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Anggota</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Paket" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.package_type') === 'family' ? 'Family' : 'Individu'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {_.get(data, 'travel_information.package_type') === 'family' && _.get(data, 'travel_information.family_members', []).map(field => (
                  <>
                    <br />
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Card>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1"><b>Hubungan</b></p>
                              <p className="mb-1">{(() => {
                                switch (field.relationship) {
                                  case 'spouse':
                                    return 'Istri'
                                  case 'first-child':
                                    return 'Anak ke 1'
                                  case 'second-child':
                                    return 'Anak ke 2'
                                  default:
                                    return '-'
                                }
                              })()}
                              </p>
                            </Col>
                          </Row>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1"><b>Nama Lengkap</b></p>
                              <p className="mb-1">{field.full_name}</p>
                            </Col>
                          </Row>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1"><b>Tanggal Lahir</b></p>
                              <p className="mb-1">{field.date_of_birth ? moment(field.date_of_birth).format('DD MMM YYYY') : '-'}</p>
                            </Col>
                          </Row>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1"><b>No Passport</b></p>
                              <p className="mb-1">{field.passport_number}</p>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                  </>
                ))}
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Dokumen Pendukung</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    {!documentData.loading && documentData.list.map(item => (
                      <p className="mb-1">
                        <a
                          className="link-download ml-1"
                          href={item.file}
                          download
                        >
                          { item.description || item.file }
                        </a>
                      </p>
                    ))}
                    {!documentData.loading && !documentData.list.length > 0 && (
                      <p className="mb-1">Tidak ada dokumen</p>
                    )}
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Tujuan dan Ahli Waris</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tujuan Perjalanan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.travel_destination.name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="No Passport" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.passport_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Negara Tujuan (Internasional)" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.destination_countries', []).map(item => `${item.name}, `)}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kota Asal (Domestic)" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.hometown.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Ahli Waris" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.heir_name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Hubungan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'travel_information.relationship.name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tanggal keberangkatan" className="px-1 profile-detail pb-0">
                        {data.travel_information.departure_date ? moment(data.travel_information.departure_date).format('DD MMM YYYY') : ''}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                        <Checkbox
                          checked={data.travel_information.is_annual}
                        >
                          Annual
                        </Checkbox>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Tanggal Kembali" className="px-1 profile-detail pb-0">
                        {data.travel_information.return_date ? moment(data.travel_information.return_date).format('DD MMM YYYY') : ''}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="">
                <p className="title-card">Perhitungan Premi / Kontribusi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Jenis Plan" className="px-1 profile-detail pb-0">
                        {data.travel_information.plan_type}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <div className="px-1">
                      <p className="mb-2 ant-descriptions-item-label">
                        Jumlah Hari yang Dipertanggungkan ({data.travel_premi_calculation.total_days_insured})
                      </p>
                      <p className="text-dark fw-bold">
                        {Helper.currency(data.travel_premi_calculation.price_days_insured, 'Rp. ', ',-')}
                      </p>
                    </div>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <div className="px-1">
                      <p className="mb-2 ant-descriptions-item-label">
                        Tambahan Per Minggu ({data.travel_premi_calculation.total_additional_week})
                      </p>
                      <p className="text-dark fw-bold">
                        {Helper.currency(data.travel_premi_calculation.price_additional_week, 'Rp. ', ',-')}
                      </p>
                    </div>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Loading Premi (Di atas 70 Thn) (Internasional)" className="px-1 profile-detail pb-0">
                        {Helper.currency(data.travel_premi_calculation.loading_premi, 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Total Premi" className="px-1 profile-detail pb-0">
                        {Helper.currency(data.travel_premi_calculation.premi, 'Rp.', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                        {`${data.travel_premi_calculation.discount_percentage}%`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                        {Helper.currency(data.travel_premi_calculation.discount_amount, 'Rp.', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Biaya Administrasi</p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item style={{ marginBottom: '0' }}>
                      <Checkbox checked={data.print_policy_book} disabled>
                        Buku Polis
                      </Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2 text-dark fw-bold">
                      <b>{Helper.currency(data.policy_printing_fee, 'Rp.', ',-')}</b>
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Materai</p>
                  </Col>
                  <Col xs={24} md={8} />
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2 text-dark fw-bold">
                      <b>{Helper.currency(data.stamp_fee, 'Rp.', ',-')}</b>
                    </p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2">Total Pembayaran</p>
                  </Col>
                  <Col xs={24} md={6} />
                  <Col xs={24} md={8}>
                    <p
                      className="mb-1 mt-2 text-dark fw-bold"
                      style={{ fontSize: '18px' }}
                    >
                      <b>
                        {Helper.currency(data.total_payment, 'Rp.', ',-')}
                      </b>
                    </p>
                  </Col>
                </Row>
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${data.status}/${data.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" disabled={data.status === 'complete' || data.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailTravel.propTypes = {
  detailData: PropTypes.object,
  documentData: PropTypes.object,
}

export default DetailTravel
