/* eslint-disable react/jsx-props-no-multi-spaces */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Input,
  Checkbox,
  Button,
  DatePicker,
  Select,
  Radio,
  AutoComplete,
  InputNumber,
} from 'antd'
import _, { isEmpty } from 'lodash'
import moment from 'moment'
import Hotkeys from 'react-hot-keys'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'
import queryString from 'query-string'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import PreviewTravel from './PreviewTravel'
import NumberFormat from 'react-number-format'
import config from 'app/config'

const FormTravel = ({
  form,
  detailData,
  submitted,
  productDetail,
  stateSelects,
  submitData,
  visiblePreview,
  setVisiblePreview,
  visibleDiscount,
  setVisibleDiscount,
  premi,
  getPremi,
  handleRenewal,
  listIO,
  listCustomer,
  setSelectedHolder,
  setSelectedInsured,
  setSelectedIO,
  selectedHolder,
  appendCustomer,
  selectedInsured,
  calcDiscountByPercentage,
  calcDiscountByCurrency,
  fetchDetailCustomer,
  paymentTotal,
  match,
  setFieldByIO,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)
  let sppaType

  if (!isEmpty(detailData.list)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailData.list.sppa_type
    }
  }

  const defaultFieldFamily = [
    {
      relationship: 'spouse',
      relationshipText: 'Istri',
      full_name: undefined,
      date_of_birth: undefined,
      passport_number: undefined,
      relationship_id: undefined,
    },
    {
      relationship: 'first-child',
      relationshipText: 'Anak 1',
      full_name: undefined,
      date_of_birth: undefined,
      passport_number: undefined,
      relationship_id: undefined,
    },
    {
      relationship: 'second-child',
      relationshipText: 'Anak 2',
      full_name: undefined,
      date_of_birth: undefined,
      passport_number: undefined,
      relationship_id: undefined,
    },
    {
      relationship: 'third-child',
      relationshipText: 'Anak 3',
      full_name: undefined,
      date_of_birth: undefined,
      passport_number: undefined,
      relationship_id: undefined,
    },
  ]

  const [fieldFamily, setFieldFamily] = React.useState(defaultFieldFamily)

  React.useEffect(() => {
    _.get(detailData, 'list.travel_information.family_members', []).forEach((item) => {
      const indexItem = fieldFamily.findIndex(o => o.relationship === item.relationship)

      if (indexItem > -1) {
        fieldFamily[indexItem].full_name = item.full_name
        fieldFamily[indexItem].date_of_birth = item.date_of_birth ? moment(item.date_of_birth) : undefined
        fieldFamily[indexItem].passport_number = item.passport_number
      }

      setFieldFamily(fieldFamily)
    })
  })

  return (
    <>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <PreviewTravel
        show={visiblePreview}
        setVisible={setVisiblePreview}
        submitted={submitted}
        data={{
          ...getFieldsValue(),
          stateSelects,
          fieldFamily,
          productDetail,
          premi,
          paymentTotal,
        }}
        handleCancel={() => setVisiblePreview(false)}
        handleSubmit={submitData}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Create'} SPPA Travel Safe International`}</div>
        </Col>
      </Row>
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">SPPA Travel Safe International</p>
                  <Row gutter={24}>
                    <Col span={8}>
                      <p className="mb-1">Tipe SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tipe SPPA'),
                          ],
                        })(
                          <Select
                            className="field-lg"
                            placeholder="- TYPE SPPA -"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={16}>
                      <p className="mb-1">Indicative Offer/Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_number', {
                          initialValue: match.params.id ? _.get(detailData, 'list.indicative_offer.io_number', '') : parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            placeholder="- NO IO -"
                            className="field-lg w-100"
                            dataSource={(listIO.data).map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}

                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listIO.data).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listIO.data).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }
                            disabled={getFieldValue('sppa_type') === undefined}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Pemegang Polis</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('policy_holder_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.policy_holder.name : undefined || undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Pemegang polis'),
                            {
                              validator: (rule, value) => {
                                const arrayPolicy = []
                                listCustomer.options.map(item => arrayPolicy.push(item.name))

                                if (!value) return Promise.resolve()

                                if (!arrayPolicy.includes(value)) return Promise.reject('*Nama Pemegang Polis tidak terdaftar')

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <AutoComplete
                            placeholder="Pemegang Polis"
                            className="field-lg w-100"
                            dataSource={(listCustomer.options).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName}`,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_name: data.name,
                                })
                              }

                              getPremi()
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Tertanggung</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('insured_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.insured.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Policy holder'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Nama Tertanggung"
                            dataSource={(listCustomer.options || []).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)
                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_name'),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_name') + ' QQ ' + data.name,
                                  })
                                }

                                setFieldsValue({
                                  insured_name: data.name,
                                })
                              }

                              fetchDetailCustomer(val)

                              setTimeout(() => {
                                setFieldsValue({
                                  insured_dob: moment(data.dob),
                                })
                              }, 1000)
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Nama Pada Polis</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: _.get(detailData, 'list.name_on_policy', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nama Pada Polis'),
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled
                            placeholder="Nama pada polis"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Produk</p>
                      <p>
                        <b>
                          {_.get(productDetail, 'type.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'class_of_business.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'display_name')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tanggal Lahir</p>
                      <Form.Item>
                        {getFieldDecorator('insured_dob', {
                          initialValue: !isEmpty(detailData.list) ? moment(detailData.list.insured.dob) : undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tanggal lahir'),
                            {
                              type: 'number', max: 70, transform: value => moment().diff(moment(value), 'years'), message: 'Umur maximal 70 tahun.',
                            },
                            {
                              validator: () => {
                                const departureDate = getFieldValue('departure_date')
                                const value = getFieldValue('insured_dob')
                                if (!value || !departureDate) return Promise.resolve()

                                const diffYear = departureDate.diff(value, 'years')
                                if (diffYear < 1) {
                                  // eslint-disable-next-line prefer-promise-reject-errors
                                  return Promise.reject('*Usia tertanggung harus lebih besar dari 1 tahun dari tanggal keberangkatan')
                                }
                                if (diffYear > 85) {
                                  return Promise.reject('*Usia tertanggung tidak dapat lebih besar dari 85 tahun dari tanggal keberangkatan')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                          // getValueFromEvent: (e) => {
                          //   setFieldsValue({
                          //     departure_date: null,
                          //     return_date: null,
                          //   })

                          //   return e
                          // },
                        })(
                          <DatePicker
                            placeholder="- Tanggal Lahir -"
                            size="large"
                            className="w-100"
                            disabled
                            disabledDate={current => current && current > moment().endOf('day').subtract(1, 'days')}
                            format="DD MMMM YYYY"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Anggota</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Paket</p>
                      <Form.Item>
                        {getFieldDecorator('package_type', {
                          initialValue: _.get(detailData, 'list.travel_information.package_type', 'individu'),
                          rules: [
                            ...Helper.fieldRules(['required'], ''),
                          ],
                        })(
                          <Radio.Group
                            options={[
                              { label: 'Individu', value: 'individu' },
                              { label: 'Keluarga', value: 'family' },
                            ]}
                            optionType="button"
                            onChange={() => getPremi()}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  {getFieldValue('package_type') === 'family' && fieldFamily.map((field, idx) => (
                    <>
                      <br />
                      <Row gutter={24}>
                        <Col xs={24} md={24}>
                          <Card>
                            <Row gutter={24}>
                              <Col xs={24} md={12}>
                                <p className="mb-1">Hubungan</p>
                                <Form.Item>
                                  {getFieldDecorator(`family_members[${idx}].relationship_id`,
                                    {
                                      // initialValue: _.get(detailData, 'list.travel_information.relationship.id', undefined),
                                    })(
                                      <Select
                                        size="large"
                                        className="w-100"
                                        placeholder="Pilih Hubungan"
                                        loading={stateSelects.relationshipLoad}
                                      >
                                        {(stateSelects.relationshipList || []).map(item => (
                                          <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                        ))}
                                      </Select>,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">Nama Lengkap</p>
                                <Form.Item>
                                  {getFieldDecorator(`family_members[${idx}].relationship`, {
                                    initialValue: field.relationship,
                                    // rules: [
                                    //   ...Helper.fieldRules(['required'], 'Nama Lengkap'),
                                    // ],
                                  })(
                                    <Input
                                      type="hidden"
                                      className="field-lg"
                                    />,
                                  )}
                                  {getFieldDecorator(`family_members[${idx}].full_name`, {
                                    initialValue: field.full_name,
                                    getValueFromEvent: e => e.target.value,
                                    // rules: [
                                    //   ...Helper.fieldRules(['required'], 'Nama Lengkap'),
                                    // ],
                                  })(
                                    <Input
                                      className="field-lg uppercase"
                                    />,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">Tanggal Lahir</p>
                                <Form.Item>
                                  {getFieldDecorator(`family_members[${idx}].date_of_birth`, {
                                    initialValue: field.date_of_birth,
                                    rules: [
                                      {
                                        validator: (rule, value) => {
                                          const departureDate = getFieldValue('departure_date')
                                          if (!value || !departureDate) return Promise.resolve()

                                          const diffYear = departureDate.diff(value, 'years')

                                          if (diffYear < 1) {
                                            return Promise.reject('*Usia tertanggung harus lebih besar dari 1 tahun dari tanggal keberangkatan')
                                          }

                                          if (diffYear > 85) {
                                            return Promise.reject('*Usia tertanggung tidak dapat lebih besar dari 85 tahun dari tanggal keberangkatan')
                                          }

                                          return Promise.resolve()
                                        },
                                      },
                                    ],
                                  })(
                                    <DatePicker
                                      placeholder="- Tanggal Lahir -"
                                      size="large"
                                      className="w-100"
                                      format="DD MMMM YYYY"
                                      disabledDate={current => current && current > moment().endOf('day').subtract(1, 'days')}
                                    />,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">No Passport</p>
                                <Form.Item>
                                  {getFieldDecorator(`family_members[${idx}].passport_number`, {
                                    initialValue: field.passport_number,
                                    rules: [
                                      {
                                        pattern: /^[A-Za-z0-9 ]+$/, message: '*Tidak boleh ada spesial karakter',
                                      },
                                    ],
                                    getValueFromEvent: e => e.target.value,
                                  })(
                                    <Input
                                      className="field-lg uppercase"
                                    />,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                    </>
                  ))}
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Tujuan dan Ahli Waris</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tujuan Perjalanan</p>
                      <Form.Item>
                        {getFieldDecorator('travel_destination_id',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.travel_destination.id', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tujuan perjalanan'),
                            ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Tujuan Perjalanan"
                              loading={stateSelects.travelDestinationLoad}
                            >
                              {(stateSelects.travelDestinationList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">No Passport</p>
                      <Form.Item>
                        {getFieldDecorator('passport_number',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.passport_number', undefined),
                            rules: [
                              {
                                pattern: /^[A-Za-z0-9 ]+$/, message: '*Tidak boleh ada spesial karakter',
                              },
                            ],
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              placeholder="Input No Passport"
                              className="field-lg uppercase"
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Negara Tujuan (Internasional)</p>
                      <Form.Item>
                        {getFieldDecorator('destination_country_ids',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.destination_countries', undefined) ? detailData.list.travel_information.destination_countries.map(o => (o.id)) : [],
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tujuan'),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (value && Array.isArray(value) && value.length) {
                                    if (value.length > 6) {
                                      return Promise.reject('*Maksimal 6 negara')
                                    }
                                  }
                                  return Promise.resolve()
                                },
                              },
                              {
                                validator: (rule, value) => {
                                  const planType = getFieldValue('plan_type')
                                  const schengen = stateSelects.countryList.filter(o => o.is_schengen_area === true).map(o => (o.id))

                                  if (planType && value && Array.isArray(value) && value.length) {
                                    // eslint-disable-next-line consistent-return
                                    if (schengen.filter(o => value.includes(o)).length !== 0 && !['vip', 'executive'].includes(planType)) {
                                      return Promise.reject('*Negara bagian Schengen country hanya bisa pilih pada plan VIP dan Executive')
                                    }
                                  }

                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                            <Select
                              mode="multiple"
                              size="large"
                              className="w-100"
                              placeholder="Pilih Negara Tujuan"
                              loading={stateSelects.countryLoad}
                              filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                              onChange={() => getPremi()}
                            >
                              {(stateSelects.countryList ? stateSelects.countryList.filter(o => !o.is_closed_country) : []).map(item => (
                                <Select.Option key={item.id} value={item.id} disabled={item.id === '5694a3b2-99f0-4d22-9f04-668cc5dd5661'}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kota Asal (Domestic)</p>
                      <Form.Item>
                        {getFieldDecorator('hometown_id',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.hometown.id', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Kota asal'),
                            ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Kota Asal"
                              loading={stateSelects.domestikCityLoad}
                            >
                              {(stateSelects.domestikCityList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Ahli Waris</p>
                      <Form.Item>
                        {getFieldDecorator('heir_name',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.heir_name', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Ahli waris'),
                              {
                                pattern: /^[A-Za-z ]+$/, message: '*Hanya menggunakan huruf alphabet',
                              },
                            ],
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              placeholder="Input Ahli Waris"
                              className="field-lg uppercase"
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Hubungan</p>
                      <Form.Item>
                        {getFieldDecorator('relationship_id',
                          {
                            initialValue: _.get(detailData, 'list.travel_information.relationship.id', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tujuan'),
                            ],
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              placeholder="Pilih Hubungan"
                              loading={stateSelects.relationshipLoad}
                            >
                              {(stateSelects.relationshipList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Tanggal Keberangkatan</p>
                      <Form.Item>
                        {getFieldDecorator('departure_date',
                          {
                            initialValue: !isEmpty(detailData.list) ? moment(detailData.list.travel_information.departure_date) : undefined,
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tanggal keberangkatan'),
                              {
                                validator: (rule, value) => {
                                  if (getFieldValue('insured_dob')) {
                                    form.validateFields(['insured_dob'])
                                  }

                                  if (getFieldValue('package_type') === 'family') {
                                    fieldFamily.map((field, idx) => setTimeout(() => form.validateFields([`family_members[${idx}].date_of_birth`])))
                                  }
                                  if (getFieldValue('return_date')) {
                                    setTimeout(() => form.validateFields(['return_date']))
                                  }

                                  if (getFieldValue('is_annual')) {
                                    setFieldsValue({
                                      return_date: value ? moment(value).add(1, 'years') : undefined,
                                    })
                                  }
                                  if (getFieldValue('return_date')) {
                                    setTimeout(() => form.validateFields(['return_date']))
                                  }

                                  if (getFieldValue('is_annual')) {
                                    setFieldsValue({
                                      return_date: value ? moment(value).add(1, 'years') : undefined,
                                    })
                                  }

                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                            <DatePicker
                              placeholder="Input Tanggal keberangkatan"
                              size="large"
                              className="w-100"
                              format="DD MMMM YYYY"
                              disabledDate={current => current && current < moment().endOf('day').subtract(1, 'days')}
                              onChange={(value) => {
                                if (getFieldValue('package_type') === 'family') {
                                  fieldFamily.map((field, idx) => setTimeout(() => form.validateFields([`family_members[${idx}].date_of_birth`])))
                                }

                                if (getFieldValue('return_date')) {
                                  setTimeout(() => form.validateFields(['return_date']))
                                }

                                if (getFieldValue('is_annual')) {
                                  setFieldsValue({
                                    return_date: value ? moment(value).add(1, 'years') : undefined,
                                  })
                                }
                              }}
                            />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('is_annual', {
                          initialValue: _.get(detailData, 'list.travel_information.is_annual', false),
                          getValueFromEvent: (e) => {
                            const departureDate = getFieldValue('departure_date')
                            setFieldsValue({
                              return_date: departureDate ? moment(departureDate).add(1, 'years') : undefined,
                              is_annual: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox
                            checked={getFieldValue('is_annual')}
                            onChange={() => getPremi()}
                          >
                            Annual
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Tanggal Kembali</p>
                      <Form.Item>
                        {getFieldDecorator('return_date',
                          {
                            initialValue: !isEmpty(detailData.list) ? moment(detailData.list.travel_information.return_date) : undefined,
                            rules: [
                              ...Helper.fieldRules(['required'], 'Tanggal kembali'),
                              {
                                validator: (rule, value) => {
                                  const departureDate = getFieldValue('departure_date')
                                  if (value && departureDate) {
                                    const sameAfter = moment(value).isSameOrAfter(departureDate)
                                    // eslint-disable-next-line prefer-promise-reject-errors
                                    if (!sameAfter) {
                                      return Promise.reject('*Tanggal kembali harus sama atau lebih dari tanggal keberangkatan')
                                    }
                                  }
                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                            <DatePicker
                              // disabled={!!getFieldValue('is_annual')}
                              placeholder="Input Tanggal Kembali"
                              size="large"
                              className="w-100"
                              format="DD MMMM YYYY"
                              disabledDate={current => 
                                // current && getFieldValue('departure_date') && current < getFieldValue('departure_date')
                                current && current < getFieldValue('departure_date') || current.diff(moment().add(178), 'day') > 178
                              }
                              onChange={() => getPremi()}
                            />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Premi / Kontribusi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Jenis Plan</p>
                      <Form.Item>
                        {getFieldDecorator('plan_type', {
                          initialValue: _.get(detailData, 'list.travel_information.plan_type', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Jenis plan'),
                          ],
                        })(
                          <Radio.Group
                            options={[
                              { label: 'VIP', value: 'vip' },
                              { label: 'Executive', value: 'executive' },
                              { label: 'Deluxe', value: 'deluxe', disabled: getFieldValue('is_annual') },
                              { label: 'Superior', value: 'superior', disabled: getFieldValue('is_annual') },
                            ]}
                            optionType="button"
                            onChange={() => {
                              setTimeout(() => form.validateFields(['destination_country_ids']))
                              getPremi()
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Currency</p>
                      <Form.Item>
                        {getFieldDecorator('currency_id', {
                          initialValue: _.get(detailData, 'list.travel_information.currency.id', stateSelects.currencyList ? stateSelects.currencyList[1].id : undefined),
                          rules: Helper.fieldRules(['required'], 'Currency'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Currency"
                            loading={stateSelects.currencyLoad}
                            onChange={() => getPremi()}
                          >
                            {(stateSelects.currencyList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Premi</p>
                      <Form.Item className="mb-0">
                        <NumberFormat
                          className="ant-input field-lg"
                          thousandSeparator
                          placeholder="0"
                          value={Helper.currency(premi.other_currency_premi)}
                          disabled
                        />
                      </Form.Item>
                      <p className="mb-3 mt-1"><b>{Helper.currency(premi.premi, 'Rp. ', ',-')}</b></p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Jumlah Hari yang Dipertanggungkan ({premi.total_days_insured})</p>
                      <p>
                        <b>
                          {Helper.currency(premi.price_days_insured, 'IDR. ', ',-')}
                        </b>
                        <div>
                          <small className="mr-1">
                            {/* {premi.premi_travel_calculation.currency_code} */}
                          </small>
                          <small>
                            {Helper.currency(premi.premi_travel_calculation.premi)}
                          </small>
                        </div>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Tambahan Per Minggu ({premi.total_additional_week})</p>
                      <p>
                        <b>
                          {Helper.currency(premi.price_additional_week, 'IDR ', ',-')}
                        </b>
                        <div>
                          <small className="mr-1">
                            {/* {premi.premi_travel_calculation.currency_code} */}
                          </small>
                          <small>
                            {Helper.currency(premi.premi_travel_calculation.price_additional_week)}
                          </small>
                        </div>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Loading Premi (Di atas 70 Thn) (Internasional)</p>
                      <p>
                        <b>
                          {Helper.currency(premi.loading_premi, 'IDR ', ',-')}
                        </b>
                        <div>
                          <small className="mr-1">
                            {/* {premi.premi_travel_calculation.currency_code} */}
                          </small>
                          <small>
                            {Helper.currency(premi.premi_travel_calculation.loading_premi)}
                          </small>
                        </div>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(detailData, 'list.travel_premi_calculation.discount_percentage', 0),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            suffix="%"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_amount', {
                          initialValue: parseFloat(premi.discount_amount).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book',
                          {
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                print_policy_book: e.target.checked,
                              })

                              return e.target.checked
                            },
                          })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                            onChange={() => setTimeout(() => getPremi())}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Materai</p>
                    </Col>
                    <Col xs={24} md={8} />
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={5} />
                    <Col xs={24} md={9}>
                      <p
                        className="mb-1 mt-2"
                        style={{ fontSize: '18px' }}
                      >
                        <b>
                          {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <br />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: [
                            {
                              validator: (rule, value) => {
                                if (!value || value === false) return Promise.reject('*Wajib centang pernyataan ')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              aggree: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox checked={getFieldValue('aggree')}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <u>
                              <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                <b>Syarat & Ketentuan Mitraca</b>
                              </a>
                            </u>
                            &nbsp;
                            yang berlaku
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24}>
                <Button
                  ghost
                  type="primary"
                  disabled={submitted}
                  className="button-lg w-50 border-lg"
                  onClick={() => submitData()}
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </>
  )
}

FormTravel.propTypes = {
  form: PropTypes.object,
  productDetail: PropTypes.object,
  countryList: PropTypes.object,
  stateSelects: PropTypes.object,
  submitData: PropTypes.func,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.bool,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  premi: PropTypes.object,
  getPremi: PropTypes.func,
  bookPrice: PropTypes.number,
  handleRenewal: PropTypes.func,
  listIO: PropTypes.object,
  listCustomer: PropTypes.object,
  setSelectedHolder: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  setSelectedIO: PropTypes.func,
  selectedHolder: PropTypes.func,
  selectedInsured: PropTypes.func,
  detailData: PropTypes.object,
  calcDiscountByPercentage: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  fetchDetailCustomer: PropTypes.func,
  customerDetail: PropTypes.object,
  appendCustomer: PropTypes.func,
  paymentTotal: PropTypes.any,
  calcPaymentTotal: PropTypes.func,
  match: PropTypes.object,
  setFieldByIO: PropTypes.func,
}

export default FormTravel
