import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Upload, Avatar,
  Input,
} from 'antd'
import { InboxOutlined } from '@ant-design/icons'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import Helper from 'utils/Helper'

const DetailOtomate = ({
  form, handleUpload, detailSPPA,
  docList, onSubmit,
  handleInput,
}) => {
  const { getFieldDecorator } = form

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center" />
      </Row>
      <Row gutter={24} type="flex" justify="center" align="middle">
        <Col xs={24} md={18}>
          <div className="title-page mb-2">{`Upload SPPA ${_.get(detailSPPA, 'data.product.display_name', 'ASRI')}`}</div>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi SPPA</p>
                <Form onSubmit={onSubmit}>
                  {docList.map((item, id) => (
                    <Row key={item.id}>
                      <Col md={8}>
                        <Form.Item>
                          {getFieldDecorator(`file-${id}`, {
                            rules: Helper.fieldRules(['required'], 'Upload Document'),
                          })(
                            <Upload
                              accept="image/*"
                              name={`file-${id}`}
                              listType="picture-card"
                              className="avatar-uploader banner-content"
                              showUploadList={false}
                              beforeUpload={() => false}
                              onChange={info => handleUpload(info, id)}
                            >
                              {item.value
                                ? (
                                  <Avatar src={item.value} shape="square" className="banner-preview" />
                                )
                                : (
                                  <div>
                                    <p className="ant-upload-drag-icon">
                                      <InboxOutlined />
                                    </p>
                                    <p className="ant-upload-text">Upload Document</p>
                                  </div>
                                )
                              }
                            </Upload>,
                          )}
                        </Form.Item>
                      </Col>
                      <Col md={14}>
                        <p className="mb-2">Deskripsi Dokumen</p>
                        <Form.Item>
                          {getFieldDecorator(`deskripsi-${id}`, {
                            rules: [
                              ...Helper.fieldRules(['required'], 'Deskripsi Dokumen'),
                            ],
                            getValueFromEvent: e => (e.target.value),
                          })(
                            <Input
                              size="large"
                              placeholder="Deskripsi"
                              onChange={e => handleInput(e, id)}
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  ))}
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="button-lg mr-3"
                  >
                    Save Document
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailOtomate.propTypes = {
  form: PropTypes.any,
  handleUpload: PropTypes.func,
  handleInput: PropTypes.func,
  docList: PropTypes.object,
  detailSPPA: PropTypes.object,
  onSubmit: PropTypes.func,
}

export default DetailOtomate
