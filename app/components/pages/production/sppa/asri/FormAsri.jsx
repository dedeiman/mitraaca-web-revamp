/* eslint-disable react/jsx-props-no-multi-spaces */
/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Checkbox,
  Button,
  AutoComplete,
  DatePicker,
  Radio,
  InputNumber,
} from 'antd'
import {
  AsriSyariahCode,
} from 'constants/ActionTypes'
import _, { isEmpty } from 'lodash'
import NumberFormat from 'react-number-format'
import Helper from 'utils/Helper'
import Hotkeys from 'react-hot-keys'
import { Form } from '@ant-design/compatible'
import moment from 'moment'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import queryString from 'query-string'
import PreviewAsri from './PreviewAsri'
import config from 'app/config'

const FormAsri = ({
  form,
  stateSelects,
  detailData,
  productDetail,
  submitPreview,
  submitData,
  premi,
  getPremi,
  visiblePreview,
  setVisiblePreview,
  onChangeStateLocation,
  setGeoLocation,
  geoLocation,
  visibleDiscount,
  setVisibleDiscount,
  submitted,
  bookPrice,
  handleRenewal,
  coverage,
  listIO,
  listCustomer,
  appendCustomer,
  setSelectedHolder,
  setSelectedInsured,
  setSameAddress,
  clearAddress,
  selectedHolder,
  selectedInsured,
  setFieldByIO,
  calcDiscountByPercentage,
  calcDiscountByCurrency,
  paymentTotal,
  calcPaymentTotal,
  match,
  loadSubmitBtn,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)

  const data = detailData.list

  let sppaType

  if (!isEmpty(detailData.list)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailData.list.sppa_type
    }
  }

  const defaultOptionYear = [{
    label: '2017',
    value: '2017',
  }, {
    label: '2016',
    value: '2016',
  }]

  const defaultFieldPropertyLoss = [{
    year_of_incident: undefined,
    cause_of_loss_code: '',
    amount_of_loss_code: '',
  }]

  const [fieldPropertyLoss, setFieldPropertyLoss] = React.useState(defaultFieldPropertyLoss)
  const [optionYear, setOptionYear] = React.useState(defaultOptionYear)

  const addFieldPropertyLoss = () => {
    fieldPropertyLoss.push(defaultFieldPropertyLoss)
    setFieldPropertyLoss([...fieldPropertyLoss])
  }

  const removeFieldPropertyLoss = (key) => {
    if (!_.isEmpty(fieldPropertyLoss[key])) {
      fieldPropertyLoss.splice(key, 1)
      setFieldPropertyLoss([...fieldPropertyLoss])
    }
  }

  React.useEffect(() => {
    const arr = []
    // eslint-disable-next-line no-plusplus
    for (let index = Number(moment().format('YYYY')); index > Number(moment().format('YYYY')) - 5; index--) {
      arr.push({
        label: index,
        value: index,
      })
    }

    const fieldData = []
    _.forEach(_.get(detailData, 'list.asri_property_loss', []), (item) => {
      fieldData.push({
        year_of_incident: item.year_of_incident,
        cause_of_loss_code: item.cause_of_loss_code,
        amount_of_loss_code: item.amount_of_loss_code,
      })
    })

    setFieldPropertyLoss(fieldData)

    setOptionYear(arr)
  }, [detailData])

  return (
    <>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <PreviewAsri
        show={visiblePreview}
        setVisible={setVisiblePreview}
        submitted={submitted}
        data={{
          ...getFieldsValue(),
          stateSelects,
          productDetail,
          fieldPropertyLoss,
          geoLocation,
          bookPrice,
          coverage,
          premi,
          paymentTotal,
        }}
        handleCancel={() => setVisiblePreview(false)}
        handleSubmit={submitData}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">
            {`${match.params.id ? 'Edit' : 'Create'} SPPA Asri`}
          </div>
        </Col>
      </Row>
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100" loading={detailData.loading}>
                  <p className="title-card">Informasi Polis</p>
                  <Row gutter={24}>
                    <Col span={8}>
                      <p className="mb-1">Tipe SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tipe SPPA'),
                          ],
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Tipe SPPA"
                            loading={false}
                            disabled={false}
                            onChange={e => handleRenewal(e)}
                            onSelect={() => setFieldsValue({ io_renewal: '' })}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={16}>
                      <p className="mb-1">Indicative Offer/Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_number', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : _.get(detailData, 'list.indicative_offer.io_number'),
                        })(
                          <AutoComplete
                            placeholder="IO Number"
                            className="field-lg w-100"
                            dataSource={(listIO.data).map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}

                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listIO.data).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listIO.data).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }
                            disabled={getFieldValue('sppa_type') === undefined}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Pemegang Polis</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('policy_holder_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.policy_holder.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Pemegang polis'),
                        })(
                          <AutoComplete
                            placeholder="Pemegang Polis"
                            className="field-lg w-100"
                            dataSource={(listCustomer.options).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: `${data.name} QQ ${insuredName}`,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_name: data.name,
                                })

                                if (getFieldValue('same_with_insured_address') === true) {
                                  setTimeout(async () => {
                                    await setSameAddress()
                                    setTimeout(() => getPremi())
                                  })
                                }
                              }

                              getPremi()
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">{productDetail.code === AsriSyariahCode ? 'Nama Peserta' : 'Nama Tertanggung'}</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('insured_name', {
                          initialValue: !_.isEmpty(detailData.list) ? detailData.list.insured.name : undefined || undefined,
                          rules: Helper.fieldRules(['required'], 'Policy holder'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder={productDetail.code === AsriSyariahCode ? 'Nama Peserta' : 'Nama Tertanggung'}
                            dataSource={(listCustomer.options || []).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)
                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_name'),
                                  })
                                } else {
                                  setFieldsValue({
                                    // eslint-disable-next-line prefer-template
                                    name_on_policy: getFieldValue('policy_holder_name') + ' QQ ' + data.name,
                                  })
                                }

                                setFieldsValue({
                                  insured_name: data.name,
                                })
                              }

                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Nama Pada Polis</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: _.get(detailData, 'list.name_on_policy', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Nama Pada Polis'),
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled
                            placeholder="Nama Pada Polis"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">QQ</p>
                      <Form.Item>
                        {getFieldDecorator('qq', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.qq', undefined),
                          rules: [
                            {
                              pattern: /^[A-Za-z0-9 ]+$/, message: '*Tidak boleh ada spesial karakter',
                            },
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="- QQ -"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Produk</p>
                      <p>
                        <b>
                          {_.get(productDetail, 'type.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'class_of_business.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'display_name')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1">Periode Pertanggungan</p>
                      <Form.Item>
                        {getFieldDecorator('start_period', {
                          initialValue: !_.isEmpty(detailData.list) ? moment(new Date(detailData.list.start_period), 'DD MMMM YYYY') : undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Periode pertanggungan'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                const diffDay = value.diff(moment(), 'days')

                                if (diffDay > 7) {
                                  return Promise.reject('*Maksimal 7 hari dari sekarang')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <DatePicker
                            placeholder="- START DATE -"
                            size="large"
                            className="w-100"
                            format="DD MMMM YYYY"
                            disabledDate={current => current && current < moment().endOf('day')}
                            onChange={(value) => {
                              setFieldsValue({
                                end_period: value ? moment(value).add(1, 'years') : undefined,
                              })
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={2}>
                      <p className="mb-1">&nbsp;</p>
                      <p className="mb-2">s/d</p>
                    </Col>
                    <Col xs={24} md={10}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('end_period', {
                          initialValue: !_.isEmpty(detailData.list) ? moment(new Date(detailData.list.end_period), 'DD MMMM YYYY') : undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Periode akhir'),
                          ],
                        })(
                          <DatePicker
                            disabled
                            placeholder="- END DATE -"
                            size="large"
                            className="w-100"
                            format="DD MMMM YYYY"
                            disabledDate={current => current && current < moment().endOf('day').subtract(1, 'days')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100" loading={detailData.loading}>
                  <p className="title-card">Nilai Objek Pertanggungan</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Luas Bangunan</p>
                      <Form.Item>
                        {getFieldDecorator('building_area', {
                          initialValue: _.get(detailData, 'list.asri_calculation.building_area', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Luas bangunan'),
                            {
                              validator: (rule, value) => {
                                if (value === '' || value === undefined) return Promise.resolve()

                                if (!(/^-?[0-9]\d*(\.\d+)?$/).test(value)) {
                                  return Promise.reject('*Harus menggunakan angka')
                                }

                                if (Number(value) <= 0) {
                                  return Promise.reject('*Nilai harus lebih dari 0')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <Input
                            placeholder={'- Luas dalam m\u00B2 -'}
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Nilai Bangunan</p>
                      <Form.Item>
                        {getFieldDecorator('property_price', {
                          initialValue: _.get(detailData, 'list.asri_calculation.property_price', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'Nilai Bangunan'),
                            {
                              validator: (rule, value) => {
                                if (!value || value === '') return Promise.reject('*Nilai harus lebih dari 0')

                                if (Number(value) <= 0) return Promise.reject('*Nilai harus lebih dari 0')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
                        })(
                          <NumberFormat
                            autoComplete="off"
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- Nilai dalam Rupiah -"
                            onChange={() => getPremi()}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Nilai Perabot</p>
                      <Form.Item>
                        {getFieldDecorator('furniture_price', {
                          initialValue: _.get(detailData, 'list.asri_calculation.furniture_price', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'Nilai Bangunan'),
                            {
                              validator: (rule, value) => {
                                if (!value || value === '') return Promise.reject('*Nilai harus lebih dari 0')

                                if (Number(value) <= 0) return Promise.reject('*Nilai harus lebih dari 0')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
                        })(
                          <NumberFormat
                            autoComplete="off"
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- Nilai dalam Rupiah -"
                            onChange={() => getPremi()}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Total</p>
                      <Form.Item>
                        <Input
                          disabled
                          placeholder="- Nilai dalam Rupiah -"
                          className="field-lg"
                          value={Helper.currency(coverage, 'Rp. ', ',-')}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Perluasan Jaminan</p>
                      <div className="d-flex align-items-center">
                        <Form.Item>
                          {getFieldDecorator('additional_limits', {
                            initialValue: _.get(detailData, 'list.asri_calculation.additional_limits', undefined),
                          })(
                            <Checkbox.Group
                              options={[
                                { label: 'Banjir', value: 'tsfwd' },
                                { label: 'Gempa', value: 'eqvet', disabled: getFieldValue('earth_quake_area') !== 'YA' },
                              ]}
                              onChange={() => getPremi()}
                            />,
                          )}
                        </Form.Item>
                      </div>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Kerugian yang pernah dialami</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Apakah ada kerugian yang pernah dialami dalam 5 tahun terakhir</p>
                      <Form.Item>
                        {getFieldDecorator('have_property_loss', {
                          initialValue: _.get(detailData, 'list.asri_property_loss', []).length > 0 ? 1 : 0,
                          rules: [
                            ...Helper.fieldRules(['required'], ''),
                          ],
                        })(
                          <Radio.Group
                            options={[
                              { label: 'Tidak Pernah', value: 0 },
                              { label: 'Pernah', value: 1 },
                            ]}
                            optionType="button"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  {getFieldValue('have_property_loss') === 1 && fieldPropertyLoss.map((field, idx) => (
                    <>
                      <br />
                      <Row gutter={24}>
                        <Col xs={24} md={24}>
                          <Card>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">Tahun Kejadian</p>
                                <Form.Item>
                                  {getFieldDecorator(`property_loss[${idx}].year_of_incident`, {
                                    initialValue: field.year_of_incident || undefined,
                                    rules: [
                                      ...Helper.fieldRules(['required'], 'Tahun kejadian'),
                                    ],
                                  })(
                                    <Select
                                      size="large"
                                      className="w-100"
                                      placeholder="- Tahun Kejadian -"
                                    >
                                      {(optionYear || []).map(item => (
                                        <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                                      ))}
                                    </Select>,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">Penyebab Kerugian</p>
                                <Form.Item>
                                  {getFieldDecorator(`property_loss[${idx}].cause_of_loss`, {
                                  initialValue: field.cause_of_loss_code || undefined,
                                    rules: [
                                      ...Helper.fieldRules(['required'], 'Penyebab kerugian'),
                                    ],
                                  })(
                                    <Select
                                      size="large"
                                      className="w-100"
                                      placeholder="Pilih Tujuan Perjalanan"
                                      loading={stateSelects.causeOfLossLoad}
                                    >
                                      {(stateSelects.causeOfLossList || []).map(item => (
                                        <Select.Option key={item.slug} value={item.slug}>{item.display_name}</Select.Option>
                                      ))}
                                    </Select>,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            <Row gutter={24}>
                              <Col xs={24} md={24}>
                                <p className="mb-1">Besar Kerugian</p>
                                <Form.Item>
                                  {getFieldDecorator(`property_loss[${idx}].amount_of_loss`, {
                                    initialValue: field.amount_of_loss_code || undefined,
                                    rules: [
                                      ...Helper.fieldRules(['required'], 'Besar kerugian'),
                                    ],
                                  })(
                                    <Select
                                      size="large"
                                      className="w-100"
                                      placeholder="Pilih Tujuan Perjalanan"
                                      loading={stateSelects.amountOfLossLoad}
                                    >
                                      {(stateSelects.amountOfLossList || []).map(item => (
                                        <Select.Option key={item.slug} value={item.slug}>{item.display_name}</Select.Option>
                                      ))}
                                    </Select>,
                                  )}
                                </Form.Item>
                              </Col>
                            </Row>
                            {(fieldPropertyLoss).length > 1 && (
                              <Row gutter={24}>
                                <Col xs={24} md={24}>
                                  <Button
                                    type="link"
                                    onClick={() => removeFieldPropertyLoss(idx)}
                                    className="d-flex align-items-center float-right"
                                  >
                                    Hapus
                                  </Button>
                                </Col>
                              </Row>
                            )}
                          </Card>
                        </Col>
                      </Row>
                    </>
                  ))}
                  {getFieldValue('have_property_loss') === 1 && (fieldPropertyLoss.length <= 5) && (
                    <Button
                      type="link"
                      onClick={() => addFieldPropertyLoss()}
                      className="d-flex align-items-center float-right"
                    >
                      Tambah
                    </Button>
                  )}
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Object Pertanggungan</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Alamat Pertanggungan</p>
                      <Form.Item>
                        {getFieldDecorator('same_with_insured_address', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.same_with_insured_address', true),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Alamat pertanggungan'),
                          ],
                        })(
                          <Radio.Group
                            options={[
                              { label: `Sama dengan Alamat ${productDetail.code === AsriSyariahCode ? 'Peserta' : 'Tertanggung'}`, value: true },
                              { label: 'Berbeda, Sebutkan', value: false },
                            ]}
                            optionType="button"
                            onChange={(event) => {
                              if (event.target.value === true) {
                                setTimeout(() => setSameAddress())
                              } else {
                                setTimeout(() => clearAddress())
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Alamat</p>
                      <Form.Item>
                        {getFieldDecorator('address',
                          {
                            initialValue: _.get(detailData, 'list.asri_insured_object.address', undefined),
                            rules: [
                              ...Helper.fieldRules(['required'], 'Alamat'),
                            ],
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              style={{ display: 'none' }}
                              className="uppercase"
                            />,
                        )}
                        <Input
                          rows="5"
                          id="convert-geolocation"
                          className="field-lg"
                          placeholder="Input Address"
                          value={geoLocation.address}
                          disabled={getFieldValue('same_with_insured_address')}
                          style={{ 'text-transform': 'uppercase' }}
                          onChange={(e) => {
                            setGeoLocation({ ...geoLocation, address: e.target.value })
                            setFieldsValue({
                              address: e.target.value,
                            })
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">RT</p>
                      <Form.Item>
                        {getFieldDecorator('rt', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.rt', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'RT'),
                            { pattern: /^[A-Za-z0-9]{0,3}$/, message: '*Maksimal 3 Digit' },
                          ],
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- RT -"
                            className="field-lg"
                            disabled={getFieldValue('same_with_insured_address')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">RW</p>
                      <Form.Item>
                        {getFieldDecorator('rw', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.rw', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'RW'),
                            { pattern: /^[A-Za-z0-9]{0,3}$/, message: '*Maksimal 3 Digit' },
                          ],
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- RW -"
                            className="field-lg"
                            disabled={getFieldValue('same_with_insured_address')}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Provinsi</p>
                      <Form.Item>
                        {getFieldDecorator('province_id', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.province_id', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Provinsi'),
                          ],
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Pilih Provinsi"
                            loading={stateSelects.provinceLoad}
                            onChange={value => onChangeStateLocation('province', value)}
                            disabled={getFieldValue('same_with_insured_address')}
                          >
                            {(stateSelects.provinceList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kota</p>
                      <Form.Item>
                        {getFieldDecorator('city_id', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.city_id', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Kota'),
                          ],
                        })(
                          <Select
                            disabled={!getFieldValue('province_id') || getFieldValue('same_with_insured_address')}
                            size="large"
                            className="w-100"
                            placeholder="Pilih Kota"
                            loading={stateSelects.cityLoad}
                            onChange={value => onChangeStateLocation('city', value)}
                          >
                            {(stateSelects.cityList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kecamatan</p>
                      <Form.Item>
                        {getFieldDecorator('sub_district_id')(
                          <Select
                            disabled={!getFieldValue('city_id') || getFieldValue('same_with_insured_address')}
                            size="large"
                            className="w-100"
                            placeholder="Pilih Kecamatan"
                            loading={stateSelects.subDistrictLoad}
                            onChange={value => onChangeStateLocation('subDistrict', value)}
                          >
                            {(stateSelects.subDistrictList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kelurahan</p>
                      <Form.Item>
                        {getFieldDecorator('village_id')(
                          <Select
                            disabled={!getFieldValue('sub_district_id') || getFieldValue('same_with_insured_address')}
                            size="large"
                            className="w-100"
                            placeholder="Pilih Kelurahan"
                            loading={stateSelects.villageLoad}
                          >
                            {(stateSelects.villageList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kode POS</p>
                      <Form.Item>
                        {getFieldDecorator('postal_code',
                          {
                            initialValue: _.get(detailData, 'list.asri_insured_object.postal_code', undefined),
                            rules: [
                              ...Helper.fieldRules(['required', 'number'], 'Kode POS'),
                              { pattern: /^[A-Za-z0-9]{0,5}$/, message: '*Maksimal 5 digit' },
                            ],
                          })(
                            <Input
                              disabled={getFieldValue('same_with_insured_address')}
                              placeholder="Input KODE POS"
                              className="field-lg"
                            />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Latitude</p>
                      <Form.Item>
                        {getFieldDecorator('lat', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.lat', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'Latitude'),
                          ],
                        })(
                          <Input
                            disabled={getFieldValue('same_with_insured_address')}
                            placeholder="Input Latitude"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Longitude</p>
                      <Form.Item>
                        {getFieldDecorator('lng', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.lng', undefined),
                          rules: [
                            ...Helper.fieldRules(['number'], 'Longitude'),
                          ],
                        })(
                          <Input
                            disabled={getFieldValue('same_with_insured_address')}
                            placeholder="Longitude"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Area Gempa</p>
                      <Form.Item>
                        {getFieldDecorator('earth_quake_area', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.city.earthquake_area', undefined) === true ? 'YA' : 'TIDAK',
                        })(
                          <Input
                            disabled
                            placeholder="Area Gempa"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Area Banjir</p>
                      <Form.Item>
                        {getFieldDecorator('float_area', {
                          initialValue: (_.get(detailData, 'list.asri_insured_object.city.flood_area', undefined) === true ? 'YA' : 'TIDAK'),
                        })(
                          <Input
                            disabled
                            placeholder="Area Banjir"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Penggunaan</p>
                      <Form.Item>
                        {getFieldDecorator('used_by', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.used_by', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Penggunaan'),
                          ],
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="- Penggunaan -"
                            loading={false}
                            style={{ 'text-transform': 'uppercase' }}
                          >
                            <Select.Option key="sendiri" value="sendiri">Sendiri</Select.Option>
                            <Select.Option key="sewa" value="sewa">Disewakan</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Jumlah Lantai</p>
                      <Form.Item>
                        {getFieldDecorator('total_floor', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.total_floor', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Jumlah lantai'),
                          ],
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="- JUMLAH LANTAI -"
                            style={{ 'text-transform': 'uppercase' }}
                            loading={false}
                          >
                            {(stateSelects.floorList || []).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Kelas Kontruksi</p>
                      <Form.Item>
                        {getFieldDecorator('class_construction', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.class_construction', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Kelas kontruksi'),
                          ],
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder=" - KELAS KONTRUKSI -"
                            loading={false}
                            style={{ 'text-transform': 'uppercase' }}
                            onChange={() => getPremi()}
                          >
                            {(stateSelects.constructList || []).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-1">Deskripsi</p>
                      <Form.Item>
                        {getFieldDecorator('class_construction_description', {
                          initialValue: _.get(detailData, 'list.asri_insured_object.class_construction_description', undefined),
                        })(
                          <Input.TextArea rows={4} style={{ 'text-transform': 'uppercase' }} />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">{productDetail.code === AsriSyariahCode ? 'Perhitungan Kontribusi' : 'Perhitungan Premi / Perhitungan Kontribusi'}</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Rate</p>
                      <p>
                        <b>
                          {premi.rate}%
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">{productDetail.code === AsriSyariahCode ? 'Kontribusi' : 'Premi'}</p>
                      <p>
                        <b>
                          {Helper.currency(premi.premi, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(detailData, 'list.asri_calculation.discount_percentage', 0),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            suffix="%"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_currency', {
                          initialValue: parseFloat(premi.discount_currency).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book',
                          {
                            initialValue: _.get(detailData, 'list.print_policy_book', false),
                            getValueFromEvent: (e) => {
                              setFieldsValue({
                                print_policy_book: e.target.checked,
                              })

                              return e.target.checked
                            },
                          })(
                            <Checkbox
                              checked={getFieldValue('print_policy_book')}
                              onChange={() => getPremi()}
                            >
                              Buku Polis
                            </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Materai</p>
                    </Col>
                    <Col xs={24} md={8} />
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={8} />
                    <Col xs={24} md={6}>
                      <p
                        className="mb-1 mt-2"
                        style={{ fontSize: '18px' }}
                      >
                        <b>
                          {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <br />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: [
                            {
                              validator: (rule, value) => {
                                if (!value || value === false) return Promise.reject('*Wajib centang pernyataan ')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              aggree: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox checked={getFieldValue('aggree')}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <u>
                              <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                <b>Syarat & Ketentuan Mitraca</b>
                              </a>
                            </u>
                            &nbsp;
                            yang berlaku
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24}>
                <Button
                  ghost
                  type="primary"
                  className="button-lg w-50 border-lg"
                  disabled={loadSubmitBtn}
                  onClick={() => submitData()}
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </>
  )
}

FormAsri.propTypes = {
  form: PropTypes.any,
  stateSelects: PropTypes.any,
  listIO: PropTypes.any,
  productDetail: PropTypes.object,
  submitPreview: PropTypes.func,
  submitData: PropTypes.func,
  premi: PropTypes.object,
  getPremi: PropTypes.func,
  detailData: PropTypes.object,
  onChangeStateLocation: PropTypes.any,
  setGeoLocation: PropTypes.func,
  geoLocation: PropTypes.object,
  setSameAddress: PropTypes.func,
  clearAddress: PropTypes.func,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  handleRenewal: PropTypes.func,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  submitted: PropTypes.bool,
  bookPrice: PropTypes.number,
  listCustomer: PropTypes.object,
  appendCustomer: PropTypes.func,
  setSelectedHolder: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  selectedHolder: PropTypes.object,
  selectedInsured: PropTypes.object,
  coverage: PropTypes.number,
  setFieldByIO: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  paymentTotal: PropTypes.any,
  calcPaymentTotal: PropTypes.func,
  match: PropTypes.object,
  loadSubmitBtn: PropTypes.bool,
}

export default FormAsri
