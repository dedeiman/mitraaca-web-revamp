import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Checkbox,
  Button, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import history from 'utils/history'
import Helper from 'utils/Helper'
import moment from 'moment'
import { LeftOutlined } from '@ant-design/icons'

const DetailAsri = ({
  detailData,
  documentData,
}) => {
  const data = detailData.list

  let typeSPPAText = '-'
  switch (data.sppa_type) {
    case 'new':
      typeSPPAText = 'New'
      break
    case 'renewal':
      typeSPPAText = 'Renewal'
      break
    default:
      break
  }

  let periodeText = ''
  if (data.start_period) {
    periodeText = moment(data.start_period).format('DD MMM YYYY')

    if (data.end_period) {
      periodeText += ` s/d ${moment(data.end_period).format('DD MMM YYYY')}`
    }
  }

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail SPPA ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Tipe SPPA" className="px-1 profile-detail pb-0">
                        {typeSPPAText}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Indicative Offer/Renewal" className="px-1 profile-detail pb-0">
                        {_.get(data, 'indicative_offer.io_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Pemegang Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'policy_holder.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                        {_.get(data, 'insured.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'name_on_policy', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="QQ" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.qq', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Periode Pertanggungan" className="px-1 profile-detail pb-0">
                        {periodeText}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Nilai Objek Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Luas Bangunan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_calculation.building_area', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nilai Bangunan" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_calculation.property_price', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nilai Perabot" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_calculation.furniture_price', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Total" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_calculation.total_coverage', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Perluasan Jaminan" className="px-1 profile-detail pb-0">
                        <Form.Item>
                          <Checkbox checked={(data.asri_calculation.additional_limits || []).includes('tsfwd')}>Banjir</Checkbox>
                          <Checkbox checked={(data.asri_calculation.additional_limits || []).includes('eqvet')}>Gempa</Checkbox>
                        </Form.Item>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Kerugian yang pernah dialami</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Kerugian yang pernah dialami dalam 5 tahun terakhir</b></p>
                    {!data.asri_property_loss.length > 0 && <p className="mb-1"><i>Tidak Pernah</i></p>}
                  </Col>
                </Row>
                {data.asri_property_loss.length > 0 && data.asri_property_loss.map(item => (
                  <>
                    <br />
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Card>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <Descriptions layout="vertical" colon={false} column={1}>
                                <Descriptions.Item span={2} label="Tahun Kejadian" className="px-1 profile-detail pb-0">
                                  {item.year_of_incident}
                                </Descriptions.Item>
                              </Descriptions>
                            </Col>
                          </Row>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <Descriptions layout="vertical" colon={false} column={1}>
                                <Descriptions.Item span={2} label="Penyebab Kerugian" className="px-1 profile-detail pb-0">
                                  {item.cause_of_loss}
                                </Descriptions.Item>
                              </Descriptions>
                            </Col>
                          </Row>
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <Descriptions layout="vertical" colon={false} column={1}>
                                <Descriptions.Item span={2} label="Besar Kerugian" className="px-1 profile-detail pb-0">
                                  {item.amount_of_loss_name}
                                </Descriptions.Item>
                              </Descriptions>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>
                  </>
                ))}
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Dokumen Pendukung</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    {!documentData.loading && documentData.list.map(item => (
                      <p className="mb-1">
                        <a
                          className="link-download ml-1"
                          href={item.file}
                          download
                        >
                          { item.description || item.file }
                        </a>
                      </p>
                    ))}
                    {!documentData.loading && !documentData.list.length > 0 && (
                      <p className="mb-1">Tidak ada dokumen</p>
                    )}
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Object Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Alamat" className="px-1 profile-detail pb-0">
                        {data.asri_insured_object.address}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="RT" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.rt', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="RW" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.rw', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Provinsi" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.province.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Kota" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.city.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Kecamatan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.sub_district.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Kelurahan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.village.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Kode POS" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.postal_code', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Latitude" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.lat', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Longitude" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.lng', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Area Gempa" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.city.earthquake_area', false) ? 'TIDAK' : 'YA'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Area Banjir" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.city.flood_area', false) ? 'TIDAK' : 'YA'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Penggunaan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.used_by', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Jumlah Lantai" className="px-1 profile-detail pb-0">
                        {`${_.get(data, 'asri_insured_object.total_floor', '-')} Lantai`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Kelas Kontruksi" className="px-1 profile-detail pb-0">
                        {`Kelas ${_.get(data, 'asri_insured_object.class_construction', '-')}`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Deskripsi" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_insured_object.class_construction_description', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Premi" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'asri_calculation.premi'), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Diskon" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'asri_calculation.discount_percentage'))}%
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Currency" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'asri_calculation.discount_currency'), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${data.status}/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" disabled={data.status === 'complete' || data.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailAsri.propTypes = {
  detailData: PropTypes.object,
  documentData: PropTypes.object,
}

export default DetailAsri
