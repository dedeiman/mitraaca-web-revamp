import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Spin } from 'antd'
import {
  OttomateCode, OttomateSmartCode,
  OttomateSolitareCode, OttomateTLOCode,
  OttomateComprehensiveCode, AsriCode,
  CargoMarineCode, CargoInlandTransitCode,
  LiabilityCode, OttomateSyariahCode,
  OttomateSyariahSmartCode, OttomateSyariahSolitareCode,
  OttomateSyariahTLOCode, OttomateSyariahComprehensiveCode,
  AsriSyariahCode, WellWomenCode, Amanah, TravelInternationalCode, TravelDomesticCode,
} from 'constants/ActionTypes'
import DetailOtomate from './otomate/DetailOtomate'
import DetailAsri from './asri/DetailAsri'
import DetailLiability from './liability/Detail'
import DetailCargo from './cargo/DetailCargo'
import DetailAmanah from './amanah/DetailAmanah'
import DetailTravelInternational from './travelInternational/DetailTravel'
import DetailTravelDomestic from './travel-domestic/Detail'
import DetailWellwomen from './wellwomen/DetailWellwomen'

const DetailSPPA = ({
  detailData, documentData,
}) => (
  <React.Fragment>
    {(() => {
      const productCode = _.get(detailData.list, 'product.code', undefined)

      switch (productCode) {
        case OttomateCode:
        case OttomateSmartCode:
        case OttomateSolitareCode:
        case OttomateTLOCode:
        case OttomateComprehensiveCode:
        case OttomateSyariahCode:
        case OttomateSyariahSmartCode:
        case OttomateSyariahSolitareCode:
        case OttomateSyariahTLOCode:
        case OttomateSyariahComprehensiveCode:
          return <DetailOtomate detailOtomate={detailData} loading={false} />
        case WellWomenCode:
          return <DetailWellwomen detailWellwomen={detailData} loading={false} />
        case AsriCode:
        case AsriSyariahCode:
          return <DetailAsri detailData={detailData} documentData={documentData} loading={false} />
        case CargoMarineCode:
        case CargoInlandTransitCode:
          return <DetailCargo detailData={detailData} documentData={documentData} loading={false} />
        case LiabilityCode:
          return <DetailLiability detailData={detailData} loading={false} />
        case TravelInternationalCode:
          return <DetailTravelInternational detailData={detailData} documentData={documentData} loading={false} />
        case TravelDomesticCode:
          return <DetailTravelDomestic detailData={detailData} documentData={documentData} loading={false} />
        case Amanah:
          return <DetailAmanah detailData={detailData} loading={false} />
        default:
          return (
            <div
              style={{
                height: '100vh',
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Spin tip="Loading..." />
            </div>
          )
      }
    })()}
  </React.Fragment>
)
DetailSPPA.propTypes = {
  detailData: PropTypes.object,
  documentData: PropTypes.object,
}

export default DetailSPPA
