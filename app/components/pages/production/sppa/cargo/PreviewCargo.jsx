import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _ from 'lodash'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'

const PreviewCargo = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  let typeSPPAText = '-'
  switch (data.sppa_type) {
    case 'new':
      typeSPPAText = 'New'
      break
    case 'renewal':
      typeSPPAText = 'Renewal'
      break
    default:
      break
  }

  let currencyText = '-'
  const currencyList = data.stateSelects ? data.stateSelects.currencyList : []

  if (data.currency_id && (data.stateSelects.currencyList || []).length) {
    const currency = currencyList.find(o => o.id === data.currency_id)

    if (currency) {
      currencyText = currency.name
    }
  }

  let cargoCategoryText = '-'
  const cargoCategoryList = data.stateSelects ? data.stateSelects.cargoCategoryList : []

  if (data.cargo_category_id && data.stateSelects.cargoCategoryList.length) {
    const cargoCategory = cargoCategoryList.find(o => o.id === data.cargo_category_id)

    if (cargoCategory) {
      cargoCategoryText = cargoCategory.name
    }
  }

  let cargoTypeText = '-'
  const cargoTypeList = data.stateSelects ? data.stateSelects.cargoTypeList : []

  if (data.cargo_type_id && data.stateSelects.cargoTypeList.length) {
    const cargoType = cargoTypeList.find(o => o.id === data.cargo_type_id)

    if (cargoType) {
      cargoTypeText = cargoType.name
    }
  }

  let vehicleTypeText = '-'
  const vehicleTypeList = data.stateSelects ? data.stateSelects.vehicleTypeList : []

  if (data.vehicle_type_id && data.stateSelects.vehicleTypeList.length) {
    const vehicleType = vehicleTypeList.find(o => o.id === data.vehicle_type_id)

    if (vehicleType) {
      vehicleTypeText = vehicleType.name
    }
  }

  let vehicleText = '-'
  const vehicleList = data.stateSelects ? data.stateSelects.vehicleList : []

  if (data.vehicle_id && data.stateSelects.vehicleList.length) {
    const vehicle = vehicleList.find(o => o.id === data.vehicle_id)

    if (vehicle) {
      vehicleText = vehicle.name
    }
  }

  let destinationCountryText = '-'
  const countryList = data.stateSelects ? data.stateSelects.countryList : []

  if (data.destination_country_id && data.stateSelects.countryList.length) {
    const destinationCountry = countryList.find(o => o.id === data.destination_country_id)

    if (destinationCountry) {
      destinationCountryText = destinationCountry.name
    }
  }

  const portList = data.stateSelects ? data.stateSelects.portList : []

  let portOriginText = '-'
  if (data.origin_port_id && data.stateSelects.portList.length) {
    const portOrigin = portList.find(o => o.id === data.origin_port_id)
    if (portOrigin) {
      portOriginText = portOrigin.name
    }
  }

  let portDestinationText = '-'
  if (data.destination_port_id && data.stateSelects.portList.length) {
    const portDestination = portList.find(o => o.id === data.destination_port_id)
    if (portDestination) {
      portDestinationText = portDestination.name
    }
  }

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      wrapClassName="wrap-modal-preview"
      className="modal-add-customer"
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={8}>
                    <p className="mb-1"><b>Tipe SPPA</b></p>
                    <p className="mb-1"><i>{typeSPPAText}</i></p>
                  </Col>
                  <Col xs={24} md={16}>
                    <p className="mb-1"><b>Indicative Offer/Renewal</b></p>
                    <p className="mb-1"><i>{data.io_number}</i></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Policy Holder</b></p>
                    <p className="mb-1">{_.get(data, 'policy_holder_name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Insured</b></p>
                    <p className="mb-1">{_.get(data, 'insured_name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Long Insured Name</b></p>
                    <p className="mb-1">{_.get(data, 'name_on_policy', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Product</b></p>
                    <p>
                      <b>
                        {_.get(data, 'productDetail.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'productDetail.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'productDetail.display_name')}
                      </b>
                    </p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Cargo Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Currency</b></p>
                    <p className="mb-1">{currencyText}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Sum Insured</b></p>
                    <p className="mb-1">{Helper.currency(data.sum_insured, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <p className="mb-0"><i>Deductible 5% of total sum insured</i></p>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Main Coverage</b></p>
                    <p className="mb-1">ICC &lsquo;A&lsquo;</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Main Coverage, Conditions, Waranties, Etc.</b></p>
                    <p className="mb-1">{data.other_information}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Voyage From</b></p>
                    <p className="mb-1">{data.voyage_from}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Voyage To</b></p>
                    <p className="mb-1">{data.voyage_to}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">*( Within indonesian terrorities)</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Transhipment</b></p>
                    <p className="mb-1">{data.transhipment}</p>
                  </Col>
                  <Col xs={24} md={12} style={{ display: !['marine-export', 'marine-import'].includes(_.get(data.selectedPolicyType, 'slug')) ? 'none' : '' }}>
                    <p className="mb-1"><b>Destination Country</b></p>
                    <p className="mb-1">{destinationCountryText}</p>
                  </Col>
                </Row>
                <Row gutter={24} style={{ display: !['marine-export', 'marine-import'].includes(_.get(data.selectedPolicyType, 'slug')) ? 'none' : '' }}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Port of Origin</b></p>
                    <p className="mb-1">{portOriginText}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Destination Port</b></p>
                    <p className="mb-1">{portDestinationText}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Vesel Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Category Cargo</b></p>
                    <p className="mb-1">{cargoCategoryText}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Type Cargo</b></p>
                    <p className="mb-1">{cargoTypeText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Vehicle Type</b></p>
                    <p className="mb-1">{vehicleTypeText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Vehicle</b></p>
                    <p className="mb-1">{vehicleText}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">Rate</p>
                        <p className="mb-1">
                          {data.rate}
                          %
                        </p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">Premi</p>
                        <p><b>{Helper.currency(data.premi.premi, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-1">Diskon</p>
                        <p className="mb-1">
                          {_.get(data, 'discount_percentage', 0)}
                          %
                        </p>
                      </Col>
                      <Col xs={12} md={12}>
                        <p className="mb-1">&nbsp;</p>
                        <p className="mb-1">
                          {Helper.currency(data.discount_amount, 'Rp. ', ',-')}
                        </p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true}>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.print_policy_book ? data.bookPrice : 0, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2" style={{ fontSize: '18px' }}><b>{Helper.currency(data.paymentTotal, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <br />
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Form.Item>
                          <Checkbox checked={data.aggree === true}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <b>Syarat & Ketentuan Mitraca</b>
                            &nbsp;
                            yang berlaku
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            type="primary"
            className="button-lg w-25 border-lg"
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 ml-3 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewCargo.propTypes = {
  data: PropTypes.any,
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
  selectedPolicyType: PropTypes.any,
}

export default PreviewCargo
