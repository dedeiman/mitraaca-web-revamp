import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Checkbox,
  Button, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import history from 'utils/history'
import Helper from 'utils/Helper'
import moment from 'moment'
import { LeftOutlined } from '@ant-design/icons'

const DetailCargo = ({
  detailData, documentData,
}) => {
  const data = detailData.list
  const htmlDecode = (input) => {
    const e = document.createElement('div')
    e.innerHTML = input
    return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue
  }

  const renderHTML = rawHTML => React.createElement('div', { dangerouslySetInnerHTML: { __html: rawHTML } })

  let typeSPPAText = '-'
  switch (data.sppa_type) {
    case 'new':
      typeSPPAText = 'New'
      break
    case 'renewal':
      typeSPPAText = 'Renewal'
      break
    default:
      break
  }

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-sppa-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail SPPA ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Tipe SPPA" className="px-1 profile-detail pb-0">
                        {typeSPPAText}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Indicative Offer/Renewal" className="px-1 profile-detail pb-0">
                        {_.get(data, 'indicative_offer.io_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Pemegang Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'policy_holder.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nama Tertanggung" className="px-1 profile-detail pb-0">
                        {_.get(data, 'insured.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Nama Pada Polis" className="px-1 profile-detail pb-0">
                        {_.get(data, 'name_on_policy', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Cargo Information</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="BL Number / AWB Number" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.bl_or_awb_number', '-') || '-' }
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="L/C Number" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.lc_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Inv Number" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.invoice_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Intereset Detail" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.interest_detail', '-') || '-' }
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Qty Interest" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.interest_quantity', '-') || '-' }
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Currency" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.currency.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Sum Insured" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'cargo_information.sum_insured', 0), `${_.get(data, 'cargo_information.currency.code', '-')} `, ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <p className="mb-0"><i>Deductible 5% of total sum insured</i></p>
                <br />
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Voyage No" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.voyage_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Est. Time Depature" className="px-1 profile-detail pb-0">
                        { _.get(data, 'cargo_information.est_time_departure', undefined) ? moment(data.cargo_information.est_time_departure).format('DD MMMM YYYY') : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>

                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Main Coverage" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.main_coverage', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Main Coverage, Conditions, Waranties, Etc." className="px-1 profile-detail pb-0">
                        {renderHTML(htmlDecode(_.get(data, 'cargo_information.other_information')))}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Voyage From" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.voyage_from')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Voyage To" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.voyage_to', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={24} className="mb-3">
                    <p className="mb-1">*( Within indonesian terrorities)</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Policy Type" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.policy_type.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Transhipment" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.transhipment', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Destination Country" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.destination_country.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Port of Origin" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.origin_port.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Destination Port" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.destination_port.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Dokumen Pendukung</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    {!documentData.loading && documentData.list.map(item => (
                      <p className="mb-1">
                        <a
                          className="link-download ml-1"
                          href={item.file}
                          download
                        >
                          { item.description || item.file }
                        </a>
                      </p>
                    ))}
                    {!documentData.loading && !documentData.list.length > 0 && (
                      <p className="mb-1">Tidak ada dokumen</p>
                    )}
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Vesel Information</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Category Cargo" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.cargo_type.category.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Type Cargo" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.cargo_type.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Vehicle Type" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.vehicle_type.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={2} label="Vehicle" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.vehicle.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Rate" className="px-1 profile-detail pb-0">
                            {_.get(data, 'cargo_premi_calculation.rate', 0)}
                            %
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Premi" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'cargo_premi_calculation.premi', 0), 'Rp. ', ',-')}%
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="Diskon" className="px-1 profile-detail pb-0">
                            {_.get(data, 'cargo_premi_calculation.discount_percentage', 0)}
                            %
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item span={2} label="&nbsp;" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'cargo_premi_calculation.discount_amount', 0), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.cargo_premi_calculation.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <br />
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-sppa/confirm-offer/${data.status}/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" disabled={data.status === 'complete' || data.status === 'approved'} onClick={() => history.push(`/production-create-sppa/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailCargo.propTypes = {
  detailData: PropTypes.object,
  documentData: PropTypes.object,
}

export default DetailCargo
