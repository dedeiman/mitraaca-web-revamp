/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Checkbox,
  Button,
  AutoComplete,
  DatePicker,
  InputNumber,
} from 'antd'
import Helper from 'utils/Helper'
import Hotkeys from 'react-hot-keys'
import { Form } from '@ant-design/compatible'
import _, { isEmpty } from 'lodash'
import moment from 'moment'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import queryString from 'query-string'
import PreviewCargo from './PreviewCargo'
import config from 'app/config'

const FormCargo = ({
  form,
  stateSelects,
  cargoTypeList,
  setCargoTypeList,
  detailData,
  productDetail,
  submitPreview,
  submitData,
  visiblePreview,
  setVisiblePreview,
  visibleDiscount,
  setVisibleDiscount,
  submitted,
  bookPrice,
  customers,
  appendCustomer,
  getPreviousPolicy,
  listPolis,
  premi,
  getPremi,
  optionsSPPAType,
  setSelectedHolder,
  setSelectedInsured,
  setFieldByIO,
  selectedHolder,
  selectedInsured,
  calcDiscountByPercentage,
  calcDiscountByCurrency,
  selectedPolicyType,
  setSelectedPolicyType,
  paymentTotal,
  calcPaymentTotal,
  exchangeRate,
  match,
  loadSubmitBtn,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    setFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)
  let sppaType
  const dataCargoTypes = []

  if (!isEmpty(detailData.list)) {
    const cargoTypes = stateSelects.cargoTypeList.filter(o => o.category.id === detailData.list.vessel_information.cargo_type.category.id)
    dataCargoTypes.push(...cargoTypes)
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailData.list.sppa_type
    }
  }

  return (
    <>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">
            {`${match.params.id ? 'Edit' : 'Create'} SPPA Cargo`}
          </div>
        </Col>
      </Row>

      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Informasi Polis</p>
                  <Row gutter={24}>
                    <Col span={8}>
                      <p className="mb-1">Tipe SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          initialValue: sppaType,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Tipe SPPA'),
                          ],
                        })(
                          <Select
                            className="field-lg w-100"
                            placeholder="Tipe SPPA"
                            onChange={(value) => {
                              getPreviousPolicy(null, value)
                            }}
                          >
                            {(optionsSPPAType).map(item => (
                              <Select.Option value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={16}>
                      <p className="mb-1">Indicative Offer/Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_number', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : _.get(detailData, 'list.indicative_offer.io_number'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="IO Number"
                            dataSource={(listPolis.data || []).map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}
                            // filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={
                              (val) => {
                                if (getFieldValue('sppa_type') === 'new') {
                                  const selectedData = (listPolis.data).find(item => item.io_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                } else {
                                  const selectedData = (listPolis.data).find(item => item.sppa_number === val)
                                  setFieldByIO(getFieldValue('sppa_type'), selectedData)
                                }
                              }
                            }
                            disabled={getFieldValue('sppa_type') === undefined}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Policy Holder</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={(data) => {
                            setFieldsValue({
                              policy_holder_name: data.name,
                            })
                            appendCustomer(data)
                            setSelectedHolder(data)

                            const insuredId = selectedInsured.id
                            const insuredName = getFieldValue('insured_name')

                            if (insuredId === data.id || !insuredId) {
                              setFieldsValue({
                                name_on_policy: data.name,
                              })
                            } else {
                              setFieldsValue({
                                // eslint-disable-next-line prefer-template
                                name_on_policy: data.name + ' QQ ' + insuredName,
                              })
                            }

                            setFieldsValue({
                              policy_holder_name: data.name,
                            })
                          }}
                          />
                        </div>
                        {getFieldDecorator('policy_holder_name', {
                          initialValue: _.get(detailData, 'list.policy_holder.name', undefined),
                          rules: Helper.fieldRules(['required'], 'Policy holder'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Search Customer"
                            dataSource={(customers.data || []).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (customers.data).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name,
                                  })
                                } else {
                                  setFieldsValue({
                                    // eslint-disable-next-line prefer-template
                                    name_on_policy: data.name + ' QQ ' + insuredName,
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_name: data.name,
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Insured</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={(data) => {
                            appendCustomer(data)
                            setSelectedInsured(data)

                            if (selectedHolder.id === data.id) {
                              setFieldsValue({
                                name_on_policy: getFieldValue('policy_holder_name'),
                              })
                            } else {
                              setFieldsValue({
                                // eslint-disable-next-line prefer-template
                                name_on_policy: getFieldValue('policy_holder_name') + ' QQ ' + data.name,
                              })
                            }

                            setFieldsValue({
                              insured_name: data.name,
                            })
                          }}
                          />
                        </div>
                        {getFieldDecorator('insured_name', {
                          initialValue: _.get(detailData, 'list.insured.name', undefined),
                          rules: Helper.fieldRules(['required'], 'Policy holder'),
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Search Customer"
                            dataSource={(customers.data || []).map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (customers.data).find(item => item.id === val)
                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_name'),
                                  })
                                } else {
                                  setFieldsValue({
                                    // eslint-disable-next-line prefer-template
                                    name_on_policy: getFieldValue('policy_holder_name') + ' QQ ' + data.name,
                                  })
                                }

                                setFieldsValue({
                                  insured_name: data.name,
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Long Insured Name</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          initialValue: _.get(detailData, 'list.name_on_policy', undefined),
                          rules: Helper.fieldRules(['required'], 'Long insured name'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled
                            className="field-lg w-100"
                            placeholder="Long Insured Name"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Product</p>
                      <p>
                        <b>
                          {_.get(productDetail, 'type.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'class_of_business.name')}
                          &nbsp;-&nbsp;
                          {_.get(productDetail, 'display_name')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Cargo Information</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">BL Number / AWB Number</p>
                      <Form.Item>
                        {getFieldDecorator('bl_or_awb_number', {
                          rules: Helper.fieldRules(['number'], 'BL / AWB Number'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input BL/AWB Number"
                            className="field-lg uppercase"
                            disabled={getFieldValue('policy_type_id') === 'ac8dcd09-2e8c-4dc9-8319-cd2c8fbd84c6'}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">L/C Number</p>
                      <Form.Item>
                        {getFieldDecorator('lc_number', {
                          initialValue: _.get(detailData, 'list.cargo_information.lc_number', undefined),
                          rules: Helper.fieldRules(['number'], 'L/C Number'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input L/C Number"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Inv Number</p>
                      <Form.Item>
                        {getFieldDecorator('invoice_number', {
                          initialValue: _.get(detailData, 'list.cargo_information.invoice_number', undefined),
                          rules: Helper.fieldRules(['number'], 'Inv Number'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Inv Number"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Interest Detail</p>
                      <Form.Item>
                        {getFieldDecorator('interest_detail', {
                          initialValue: _.get(detailData, 'list.cargo_information.interest_detail', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Interest detail'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                if (/^[a-zA-Z0-9-/\n/ ]*$/.test(value) === false) return Promise.reject('*Tidak boleh menggunakan spesial karakter')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input.TextArea
                            placeholder="Input Interest Detail"
                            rows={5}
                            className="uppercase"
                            style={{ resize: 'none' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">QTY Interest</p>
                      <Form.Item>
                        {getFieldDecorator('interest_quantity', {
                          initialValue: _.get(detailData, 'list.cargo_information.interest_quantity', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'QTY interest'),
                            { pattern: /^[1-9][0-9]*$/, message: '*Nilai harus lebih dari 0' },
                          ],
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="Input QTY"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Currency</p>
                      <Form.Item>
                        {getFieldDecorator('currency_id', {
                          initialValue: _.get(detailData, 'list.cargo_information.currency.id', stateSelects.currencyList ? stateSelects.currencyList[1].id : undefined),
                          rules: Helper.fieldRules(['required'], 'Currency'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Currency"
                            loading={stateSelects.currencyLoad}
                            onChange={() => getPremi()}
                          >
                            {(stateSelects.currencyList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Sum Insured</p>
                      <Form.Item>
                        {getFieldDecorator('sum_insured', {
                          initialValue: _.get(detailData, 'list.cargo_information.sum_insured', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Sum insured'),
                            { pattern: /^[1-9][0-9]*$/, message: '*Nilai harus lebih dari 0' },
                          ],
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            placeholder="Amount"
                            className="field-lg"
                            style={{ width: '100%' }}
                            onChange={() => getPremi()}
                          />,
                        )}
                        {(form.getFieldsValue().currency_id !== 'a3fa5781-5018-4c11-8dae-142fb60b48c0') && (
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1 "><b>{Helper.currency(exchangeRate.idr_exchange_rate, 'Rp. ', ',-')}</b></p>
                            </Col>
                          </Row>
                        )}
                        {(exchangeRate.idr_exchange_rate > 3000000000) && (
                          <Row gutter={24}>
                            <Col xs={24} md={24}>
                              <p className="mb-1 text-danger">*Maximum Rp.3,000,000,000</p>
                            </Col>
                          </Row>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <p className="mb-0">Deductible 5% of total sum insured</p>

                  <br />
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Voyage No</p>
                      <Form.Item>
                        {getFieldDecorator('voyage_number', {
                          initialValue: _.get(detailData, 'list.cargo_information.voyage_number', undefined),
                          rules: Helper.fieldRules(['required'], 'Voyage no'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Voyage No"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Est. Time Depature</p>
                      <Form.Item>
                        {getFieldDecorator('est_time_departure', {
                          initialValue: _.get(detailData, 'list.cargo_information.est_time_departure', undefined) ? moment(detailData.list.cargo_information.est_time_departure) : undefined,
                          rules: [
                            ...Helper.fieldRules(['required'], 'Time Depature'),
                          ],
                        })(
                          <DatePicker
                            size="large"
                            className="w-100"
                            format="DD MMMM YYYY"
                            disabledDate={current => current && (current < moment().endOf('day').subtract(1, 'days') || current > moment().endOf('day').add(60, 'days'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Main Coverage</p>
                      <p className="mb-1">ICC &lsquo;A&lsquo;</p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Main Coverage, Conditions, Waranties, Etc.</p>
                      <Form.Item>
                        {getFieldDecorator('other_information', {
                          initialValue: `- Institute Cargo Clauses (AIR), (Excluding sendings by Post)
- Institute Extended Radioactive Contamination Exclusive Clause
- Electronic Date Recognition Exclusion Clause (for Marine Business)
- Seepage and Pollution Exclusion Clause (01.01.89)
- Information Technology Hazards Clause
- Institute Chemical, Biological, Bio-Chemical, Electro Magnetic, Weapons and Cyber Attack Exclusion Clause (01/11/02)
- Terrorism Exclusion Clause
- Sanction limitation exclusion clause
- For shipment by sea, vessel must be maximum 35 years old and minimum 100 GRT and  subject to excluding Tug and Barge, LCT/LST and wooden vessel
- Cargo must be packed in standard packing
- For sending by open truck, cargo must be sufficiently covered by waterproof tarpaulin
- For Non-containerized cargo, it must be loaded under deck, if on deck it will be covered under ICC 'C' 1/1/82
- Cargo must be in safety lashing (for cargoes in form of Heavy equipment and Car)
- Land Conveyance must be approved by transportation authority
- Excluding back-dated cover
- Coverage will start as per time and date of issuance policy/certificate
- Sanction limitation exclusion clause
- The condition of cargo must be in brand new product for machineries, Spare-parts and Equipments
- Including loading and unloading
- Excluding Freight Forwarder Liability
- Excluding denting, bending, and scratching unless due to insured perils
- Excluding Rusting, Oxidation, Discoloration unless due to insured perils
- Excluding Mechanical and Electrical Derangement
- Excluding pre-existing Damages
- Excluding mysterious loss, shortage and loss of volume and loss of weight
- Excluding loss of cargo due to Fraud by own driver of the cargo owner and/or carrier company
- Los notification clause (30 days)`,
                          rules: Helper.fieldRules(['required']),
                        })(
                          <Input.TextArea disabled rows={15} style={{ resize: 'none' }} />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Voyage From</p>
                      <Form.Item>
                        {getFieldDecorator('voyage_from', {
                          initialValue: _.get(detailData, 'list.cargo_information.voyage_from', undefined),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Voyage From"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Voyage To</p>
                      <Form.Item>
                        {getFieldDecorator('voyage_to', {
                          initialValue: _.get(detailData, 'list.cargo_information.voyage_to', undefined),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Input Voyage To"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <p className="mb-0">*(Within indonesian terrorities)</p>
                  <br />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Policy Type</p>
                      <Form.Item>
                        {getFieldDecorator('policy_type_id', {
                          initialValue: _.get(detailData, 'list.cargo_information.policy_type.id'),
                          rules: Helper.fieldRules(['required'], 'Policy type'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Policy Type"
                            loading={stateSelects.policyTypeLoad}
                            onChange={(value) => {
                              const selected = stateSelects.policyTypeList.find(o => o.id === value)
                              setTimeout(() => setSelectedPolicyType(selected))
                            }}
                          >
                            {(stateSelects.policyTypeList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Transhipment</p>
                      <Form.Item>
                        {getFieldDecorator('transhipment', {
                          initialValue: _.get(detailData, 'list.cargo_information.transhipment', undefined),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            placeholder="Transhipment"
                            className="field-lg uppercase"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    {['marine-export', 'marine-import'].includes(_.get(selectedPolicyType, 'slug')) && (
                      <Col xs={12} md={12}>
                        <p className="mb-1">Destination Country</p>
                        <Form.Item>
                          {getFieldDecorator('destination_country_id', {
                            initialValue: _.get(detailData, 'list.cargo_information.destination_country.id', undefined),
                            rules: ['marine-export', 'marine-import'].includes(_.get(selectedPolicyType, 'slug')) ? Helper.fieldRules(['required'], 'Destination country') : null,
                          })(
                            <Select
                              size="large"
                              className="w-100"
                              allowClear
                              placeholder="Select Country"
                              loading={stateSelects.countryLoad}
                            >
                              {(stateSelects.countryList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                  </Row>

                  {['marine-export', 'marine-import'].includes(_.get(selectedPolicyType, 'slug')) && (
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-1">Port of Origin</p>
                        <Form.Item>
                          {getFieldDecorator('origin_port_id', {
                            initialValue: _.get(detailData, 'list.cargo_information.origin_port.id', undefined),
                            rules: ['marine-export', 'marine-import'].includes(_.get(selectedPolicyType, 'slug')) ? [
                              ...Helper.fieldRules(['required'], 'Port of origin'),
                            ] : null,
                          })(
                            <Select
                              size="large"
                              allowClear
                              className="w-100"
                              placeholder="Select Port Origin"
                              loading={stateSelects.portLoad}
                            >
                              {(stateSelects.portList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={12} md={12}>
                        <p className="mb-1">Destination Port</p>
                        <Form.Item>
                          {getFieldDecorator('destination_port_id', {
                            initialValue: _.get(detailData, 'list.cargo_information.destination_port.id', undefined),
                            rules: ['marine-export', 'marine-import'].includes(_.get(selectedPolicyType, 'slug')) ? [
                              ...Helper.fieldRules(['required'], 'Destination port'),
                              {
                                validator: (rule, value) => {
                                  if (value && getFieldValue('origin_port_id') === value) {
                                    // eslint-disable-next-line prefer-promise-reject-errors
                                    return Promise.reject('Destination Port tidak boleh sama dengan Port of Origin')
                                  }
                                  return Promise.resolve()
                                },
                              },
                            ] : null,
                          })(
                            <Select
                              size="large"
                              allowClear
                              className="w-100"
                              placeholder="Select Port Origin"
                              loading={stateSelects.portLoad}
                            >
                              {(stateSelects.portList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  )}

                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Vesel Information</p>
                  <Row gutter={24}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Category Cargo</p>
                      <Form.Item>
                        {getFieldDecorator('cargo_category_id', {
                          initialValue: _.get(detailData, 'list.vessel_information.cargo_type.category.id', undefined),
                          rules: Helper.fieldRules(['required'], 'Category cargo'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Category"
                            loading={stateSelects.cargoCategoryLoad}
                            onChange={(value) => {
                              const cargoTypes = stateSelects.cargoTypeList.filter(o => o.category.id === value)
                              setTimeout(() => setFieldsValue({ cargo_type_id: undefined }))
                              setCargoTypeList(cargoTypes)
                            }}
                          >
                            {(stateSelects.cargoCategoryList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Type Cargo</p>
                      <Form.Item>
                        {getFieldDecorator('cargo_type_id', {
                          initialValue: _.get(detailData, 'list.vessel_information.cargo_type.id', undefined),
                          rules: Helper.fieldRules(['required'], 'Type cargo'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Type"
                            disabled={!cargoTypeList.length > 0}
                          >
                            {(!isEmpty(detailData.list) ? dataCargoTypes : cargoTypeList).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Vehicle Type</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_type_id', {
                          initialValue: _.get(detailData, 'list.vessel_information.vehicle_type.id', undefined),
                          rules: Helper.fieldRules(['required'], 'Vehicle type'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Vehicle Type"
                            loading={stateSelects.vehicleTypeLoad}
                          >
                            {(stateSelects.vehicleTypeList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Vehicle</p>
                      <Form.Item>
                        {getFieldDecorator('vehicle_id', {
                          initialValue: _.get(detailData, 'list.vessel_information.vehicle.id', undefined),
                          rules: Helper.fieldRules(['required'], 'Vehicle'),
                        })(
                          <Select
                            size="large"
                            className="w-100"
                            placeholder="Select Vehicle"
                            loading={stateSelects.vehicleLoad}
                          >
                            {(stateSelects.vehicleList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Premi / Kontribusi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Rate</p>
                      <Form.Item>
                        {getFieldDecorator('rate', {
                          initialValue: _.get(detailData, 'list.cargo_premi_calculation.rate', undefined),
                          rules: [
                            ...Helper.fieldRules(['required'], 'Rate'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                // eslint-disable-next-line no-restricted-globals
                                if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                                if (Number(value) < 0.08) return Promise.reject('*Nilai minimal 0.08')

                                if (Number(value) > 10) {
                                  return Promise.reject('*Nilai maksimal 10')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                        })(
                          <Input
                            placeholder="- Dalam % -"
                            className="field-lg"
                            onChange={() => getPremi()}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Premi</p>
                      <p className="mb-1"><b>{Helper.currency(premi.premi, 'Rp. ', ',-')}</b></p>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-1">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: _.get(detailData, 'list.cargo_premi_calculation.discount_percentage'),
                          rules: Helper.fieldRules(['number', 'positiveNumber', 'maxDiscount'], 'Diskon'),
                        })(
                          <Input
                            autoComplete="off"
                            placeholder="- Dalam % -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_amount', {
                          initialValue: parseFloat(premi.discount_amount).toFixed(2) || 0,
                          rules: Helper.fieldRules(['number', 'positiveNumber'], 'Diskon'),
                        })(
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            autoComplete="off"
                            placeholder="- Dalam Currency -"
                            className="field-lg"
                            onChange={() => {
                              setTimeout(() => getPremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book', {
                          initialValue: _.get(detailData, 'list.print_policy_book', false),
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              print_policy_book: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                            onChange={() => setTimeout(() => getPremi())}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={6}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Materai</p>
                    </Col>
                    <Col xs={24} md={8} />
                    <Col xs={24} md={6}>
                      <p className="mb-1 mt-2">
                        <b>{Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}</b>
                      </p>
                    </Col>
                  </Row>
                  <hr />
                  <Row gutter={24}>
                    <Col xs={24} md={10}>
                      <p className="mb-1 mt-2">Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={6} />
                    <Col xs={24} md={8}>
                      <p
                        className="mb-1 mt-2"
                        style={{ fontSize: '18px' }}
                      >
                        <b>
                          {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                        </b>
                      </p>
                    </Col>
                  </Row>
                  <br />
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: [
                            {
                              validator: (rule, value) => {
                                if (!value || value === false) return Promise.reject('*Wajib centang pernyataan ')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              aggree: e.target.checked,
                            })
                            return e.target.checked
                          },
                        })(
                          <Checkbox checked={getFieldValue('aggree')}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <u>
                              <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                <b>Syarat & Ketentuan Mitraca</b>
                              </a>
                            </u>
                            &nbsp;
                            yang berlaku
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 pb-3">
              <Col span={24}>
                <Button
                  ghost
                  type="primary"
                  className="button-lg w-50 border-lg"
                  disabled={loadSubmitBtn}
                  onClick={() => submitData()}
                >
                  {`${match.params.id ? 'Update' : 'Save'}`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </>
  )
}

FormCargo.propTypes = {
  form: PropTypes.any,
  stateSelects: PropTypes.any,
  cargoTypeList: PropTypes.any,
  setCargoTypeList: PropTypes.func,
  productDetail: PropTypes.any,
  submitPreview: PropTypes.any,
  submitData: PropTypes.any,
  visiblePreview: PropTypes.any,
  setVisiblePreview: PropTypes.any,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  detailData: PropTypes.object,
  submitted: PropTypes.any,
  bookPrice: PropTypes.any,
  getPremi: PropTypes.any,
  premi: PropTypes.object,
  customers: PropTypes.object,
  selectedPolicyType: PropTypes.object,
  setSelectedPolicyType: PropTypes.func,
  appendCustomer: PropTypes.func,
  getPreviousPolicy: PropTypes.any,
  listPolis: PropTypes.any,
  optionsSPPAType: PropTypes.any,
  setSelectedHolder: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  setFieldByIO: PropTypes.func,
  selectedHolder: PropTypes.any,
  selectedInsured: PropTypes.any,
  calcDiscountByPercentage: PropTypes.func,
  calcDiscountByCurrency: PropTypes.func,
  paymentTotal: PropTypes.any,
  calcPaymentTotal: PropTypes.func,
  exchangeRate: PropTypes.object,
  match: PropTypes.object,
  loadSubmitBtn: PropTypes.bool,
}

export default FormCargo
