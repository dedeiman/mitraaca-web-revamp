import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Popconfirm,
} from 'antd'
import { Loader } from 'components/elements'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const PolicyList = ({
  handleFilter, loadPolicy,
  setStateFilter, stateFilter,
  handlePage, statePolicy,
  updatePerPage,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={21}>
          <Input
            allowClear
            placeholder="Search SPPA / Policy Number..."
            className="field-lg"
            value={stateFilter.search}
            onChange={(e) => {
              setStateFilter({
                ...stateFilter,
                search: e.target.value,
              })
            }}
            onPressEnter={() => loadPolicy(true)}
          />
        </Col>
        <Col span={3} align="end">
          <Button type="primary" className="button-lg px-4" onClick={() => loadPolicy(true)}>
            Search
          </Button>
        </Col>
        <Col span={21}>
          <Form.Item label="Status SPPA">
            <Select
              onChange={handleFilter}
              allowClear
              value={stateFilter.filter || undefined}
              style={{ width: '350px' }}
              loading={stateFilter.load}
              placeholder="Select Status"
              className="field-lg"
            >
              {(stateFilter.list || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>

      <Card>
        {statePolicy.load && (
          <Loader />
        )}
        {(!statePolicy.load && isEmpty(statePolicy.list)) && (
          <Empty description="Tidak ada data." />
        )}
        {!isEmpty(statePolicy.list)
          ? (
            statePolicy.list.map((item, idx) => (
              <Row gutter={24}>
                <Col xs={24} md={4} xl={5}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA Number / Policy Number" className="px-1">
                      {item.indicative_offer && (
                        <React.Fragment>
                          <p className="mb-0">{`Quotation No: ${item.indicative_offer || '-'}`}</p>
                          <p>
                            {`(${
                              item.created_at ? moment(item.created_at).utc().format('DD MMM YYYY') : '-'
                            } - ${
                              item.created_at ? moment(item.created_at).utc().format('HH:mm') : '-'
                            })`}
                          </p>
                        </React.Fragment>
                      )}
                      <p className="mb-0">{`SPPA No: ${item.sppa_number || '-'}`}</p>
                      {item.created_at ? (
                        <p>
                          {`(${
                            moment(item.created_at).utc().format('DD MMM YYYY') || '-'
                          } - ${
                            moment(item.created_at).utc().format('HH:mm') || '-'
                          })`}
                        </p>
                      ) : '-'}
                      <p className="mb-0">{`Policy No: ${item.policy_number || '-'}`}</p>
                      {item.policied_at ? (
                        <p>
                          {`(${
                            moment(item.policied_at).utc().format('DD MMM YYYY') || '-'
                          } - ${
                            moment(item.policied_at).utc().format('HH:mm') || '-'
                          })`}
                        </p>
                      ) : '-'}
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={4}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item span={isMobile ? 4 : 1} label="Agent" className="px-1">
                      <p className="mb-0">
                        {item.creator ? Helper.getValue(item.creator.name) : '-'}
                      </p>
                      <p>
                        {`(${
                          item.creator ? Helper.getValue(item.creator.agent_id) : '-'
                        })`}
                      </p>
                      <p className="mb-0">Production in:</p>
                      <p>
                        {`${
                          item.creator.branch ? Helper.getValue(item.creator.branch.name) : '-'
                        } - ${
                          item.creator.branch_perwakilan ? Helper.getValue(item.creator.branch_perwakilan.name) : '-'
                        }`}
                      </p>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={3}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Valid Until" className="px-1">
                      <p className="mb-0">
                        {`${item.expired_at ? moment(item.expired_at).format('D MMM YYYY') : '-'}`}
                      </p>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={5}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item span={isMobile ? 4 : 1} label="Insured" className="px-1">
                      <p>
                        {`${
                          item.product.type ? item.product.type.name : '-'
                        } - ${
                          item.product.class_of_business ? item.product.class_of_business.name : '-'
                        } - ${
                          item.product.display_name || '-'
                        }`}
                      </p>
                      <p>{`Policy Holder: ${item.policy_holder.name || '-'}`}</p>
                      <p>{`Insured: ${item.insured.name || '-'}`}</p>
                      <p className="mb-0">Insured Period:</p>
                      <p>
                        {'('}
                        {item.start_period ? moment(item.start_period).format('D MMM YYYY') : '-'}
                        {' - '}
                        {item.end_period ? moment(item.end_period).format('D MMM YYYY') : '-'}
                        {')'}
                      </p>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={2}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Status" className="px-1">
                      <span className="tag" style={{ textTransform: 'capitalize'}}>
                        {item.status}
                      </span>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={5}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA / Policy Info" className="px-1">
                      {(item.sppa_infos || []).map(info => (
                        <p className="d-flex align-items-center mb-2" key={Math.random()}>
                          {info.is_check
                            ? <img src="/assets/ic-check.png" alt="check" className="mr-2" style={{ width: '20px', height: '20px' }} />
                            : <img src="/assets/ic-times.svg" alt="times" className="mr-2" />
                          }
                          {info.display_name || '-'}
                        </p>
                      ))}
                    </Descriptions.Item>
                  </Descriptions>
                  {(item.status != 'expired') && (
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item className="px-1">
                        <Button
                          type="primary"
                          htmlType="button"
                          className="w-100"
                          onClick={() => {
                            history.push(`/production-create-sppa/detail/${item.id}`)
                          }}
                        >
                          Detail SPPA
                        </Button>
                      </Descriptions.Item>
                    </Descriptions>
                  )}
                  {(item.status == 'expired') && (
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item className="px-1">
                        <Button
                          type="primary"
                          htmlType="button"
                          className="w-100"
                          disabled
                        >
                          Detail SPPA
                        </Button>
                      </Descriptions.Item>
                    </Descriptions>
                  )}
                </Col>

                {(idx !== (statePolicy.list.length - 1)) && (
                  <Divider />
                )}
              </Row>
            ))
          )
            : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={stateFilter.page ? Number(stateFilter.page) : 1}
        onChange={handlePage}
        total={statePolicy.page.total_count}
        pageSize={statePolicy.page.per_page || 10}
        onShowSizeChange={updatePerPage}
      />
    </React.Fragment>
  )
}

PolicyList.propTypes = {
  handlePage: PropTypes.func,
  handleFilter: PropTypes.func,
  loadPolicy: PropTypes.func,
  statePolicy: PropTypes.object,
  setStateFilter: PropTypes.func,
  stateFilter: PropTypes.object,
  updatePerPage: PropTypes.func,
}

export default PolicyList