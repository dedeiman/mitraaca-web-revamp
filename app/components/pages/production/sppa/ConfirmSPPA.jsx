import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
} from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import ConfirmAmanah from './amanah/ConfrimAmanah'
import ConfirmTravelFomestic from './travel-domestic/Confirm'
import ConfirmOtomate from './otomate/ConfirmOtomate'

const ConfirmSPPA = ({
  detailSPPA, signature,
  handleSignatureSave,
  handleBack, page,
  loadConfirmSppa, errorSigPad, setErrorSigPad,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Confirm SPPA</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={24}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailSPPA.loading}>
                {
                  !detailSPPA.loading
                  && (
                    <>
                      <p className="title-card">Informasi SPPA</p>
                      {(() => {
                        switch (page) {
                          case 'Otomate':
                            return (
                              <ConfirmOtomate signature={signature} isMobile={isMobile} detailSPPA={detailSPPA.list} />
                            )

                          case 'Amanah':
                            return (
                              <ConfirmAmanah signature={signature} isMobile={isMobile} detailSPPA={detailSPPA.list} />
                            )

                          case 'TravelDomestic':
                            return (
                              <ConfirmTravelFomestic signature={signature} isMobile={isMobile} detailSPPA={detailSPPA.list} />
                            )

                          default:
                            return null
                        }
                      })()}
                      <p style={{ color: 'red' }}>{ errorSigPad ? 'Tanda Tangan Wajib Diisi!' : '' }</p>
                      <Row gutter={[24, 24]} justify="space-between" className="mb-3">
                        <Col span={24} className="d-flex justify-content-between align-items-center">
                          <Button type="primary" htmlType="button" onClick={() => handleBack(detailSPPA.list.id, detailSPPA.list.product.code, detailSPPA.list.product.id)} className="button-lg w-25 ml-2 mr-2 border-lg">
                            Back
                          </Button>
                          <Button type="primary" htmlType="button" onClick={() => signature.data.clear()} className="button-lg w-25 ml-2 mr-2 border-lg">
                            Clear
                          </Button>
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="button-lg w-25 ml-2 mr-2 border-lg"
                            disabled={loadConfirmSppa}
                            onClick={() => {
                              if (!signature.data.isEmpty()) {
                                setErrorSigPad(false)
                                handleSignatureSave(
                                  signature.data.getCanvas().toDataURL('image/png'), 'save',
                                )
                              } else {
                                setErrorSigPad(true)
                              }
                            }}
                          >
                            {loadConfirmSppa && <LoadingOutlined className="mr-2" />}
                            {'Save'}
                          </Button>
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="button-lg w-25 ml-2 mr-2 border-lg"
                            disabled={loadConfirmSppa}
                            onClick={() => {
                              if (!signature.data.isEmpty()) {
                                setErrorSigPad(false)
                                handleSignatureSave(
                                  signature.data.getCanvas().toDataURL('image/png'), 'next',
                                )
                              } else {
                                setErrorSigPad(true)
                              }
                            }}
                          >
                            {loadConfirmSppa && <LoadingOutlined className="mr-2" />}
                            {'Next'}
                          </Button>
                        </Col>
                      </Row>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

ConfirmSPPA.propTypes = {
  detailSPPA: PropTypes.object,
  signature: PropTypes.object,
  handleSignatureSave: PropTypes.func,
  handleBack: PropTypes.func,
  errorSigPad: PropTypes.bool,
  setErrorSigPad: PropTypes.func,
  page: PropTypes.string,
  loadConfirmSppa: PropTypes.bool,
}

export default ConfirmSPPA
