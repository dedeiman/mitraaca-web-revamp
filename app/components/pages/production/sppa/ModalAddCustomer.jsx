import React, { Fragment } from 'react'
import {
  Modal, Col, Row,
  Card, Select, Input,
  DatePicker, Button,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import moment from 'moment'
import NumberFormat from 'react-number-format'
import { isEmpty } from 'lodash'

const AddNewCustomer = ({
  showModal, form,
  visibleAddCust,
  setVisibleAddCust, listVillages,
  submitAddCustomer, listSubDistrict,
  listStatuses, listEmployes,
  listRangeOfIncomes, listProvincy,
  handleSelectResidence, listCities,
  stateCountry, stateCountryCode,
  listFundSources, listInstitutionTypes, listBusinessPurposes,
  listBusinessTypes, handleProfileId,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    setFieldsValue,
    validateFields,
  } = form

  return (
    <Fragment>
      {/* eslint-disable */}
      <a
        className="mr-3"
        onClick={() => showModal()}
      >
        Add New Customer
      </a>
      {/* eslint-enable */}
      <AddModalCust
        submitAddCustomer={submitAddCustomer}
        visibleAddCust={visibleAddCust}
        setVisibleAddCust={setVisibleAddCust}
        getFieldDecorator={getFieldDecorator}
        getFieldValue={getFieldValue}
        setFieldsValue={setFieldsValue}
        validateFields={validateFields}
        listStatuses={listStatuses}
        listEmployes={listEmployes}
        listRangeOfIncomes={listRangeOfIncomes}
        listProvincy={listProvincy}
        handleSelectResidence={handleSelectResidence}
        listCities={listCities}
        stateCountry={stateCountry}
        stateCountryCode={stateCountryCode}
        listSubDistrict={listSubDistrict}
        listVillages={listVillages}
        listFundSources={listFundSources}
        listInstitutionTypes={listInstitutionTypes}
        listBusinessPurposes={listBusinessPurposes}
        listBusinessTypes={listBusinessTypes}
        handleProfileId={handleProfileId}
      />
    </Fragment>
  )
}

const AddModalCust = ({
  visibleAddCust, setVisibleAddCust, listCities,
  submitAddCustomer, listStatuses, listEmployes,
  getFieldDecorator, getFieldValue, setFieldsValue,
  listRangeOfIncomes, listProvincy, handleSelectResidence,
  listSubDistrict, listVillages, stateCountry, stateCountryCode,
  listFundSources, listInstitutionTypes, listBusinessPurposes,
  listBusinessTypes, handleProfileId,
}) => {
  let rulesID = []
  if (getFieldValue('citizenship') === 'wna') {
    rulesID = [{ pattern: /^[A-Z0-9]+$/, message: '*wajib menggunakan huruf kapital atau angka' }]
  } else {
    rulesID = [{ pattern: /^(?=.{16,}$).*/, message: '*Wajib 16 karakter' }]
  }
  const isIndividu = getFieldValue('customer_type') === 'individu'
  const isCorporate = getFieldValue('customer_type') === 'corporate'
  const isMobile = window.innerWidth < 768

  const prefixSelector = getFieldDecorator('phone_code_id', {
    initialValue: '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountry.list || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  const prefixSelectorCode = getFieldDecorator('pic_phone_country_code', {
    initialValue: '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountryCode.list || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  return (
    <Modal
      title="Data Customer"
      visible={visibleAddCust}
      onCancel={() => setVisibleAddCust(false)}
      wrapClassName="wrap-modal-preview"
      className="modal-add-customer"
      footer={null}
    >
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">General Info Calon Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Type Customer</p>
                      <Form.Item>
                        {getFieldDecorator('customer_type', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: 'individu',
                          getValueFromEvent: (val) => {
                            if (val === 'individu') {
                              setFieldsValue({
                                pic_name: '',
                                pic_phone_number: '',
                                gender: undefined,
                                type_id: undefined,
                                martial_status: undefined,
                                employment: undefined,
                                number_id: '',
                                dob: undefined,
                                birthplace: '',
                              })
                            } else {
                              setFieldsValue({
                                pic_name: '',
                                pic_phone_number: '',
                                gender: undefined,
                                type_id: undefined,
                                martial_status: undefined,
                                employment: undefined,
                                number_id: '',
                                dob: undefined,
                                birthplace: '',
                              })
                            }

                            return val
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Type Customer"
                            loading={false}
                            disabled={false}
                          >
                            {[{ id: 'individu', name: 'INDIVIDU' }, { id: 'corporate', name: 'CORPORATE' }].map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Kewarganegaraan</p>
                      <Form.Item>
                        {getFieldDecorator('citizenship', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: 'wni',
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              expiry_date_identity_id: '',
                            })

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Kewarganegaraan"
                            loading={false}
                            disabled={false}
                            onChange={() => {
                              setFieldsValue({
                                type_id: undefined,
                                number_id: undefined,
                              })
                            }}
                          >
                            <Select.Option value="wni">WNI</Select.Option>
                            <Select.Option value="wna">WNA</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Nomor Customer</p>
                      <Form.Item>
                        {getFieldDecorator('customer_number', {
                          initialValue: '',
                        })(
                          <Input
                            disabled
                            placeholder="Auto Generated By System"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">No Izin Usaha</p>
                      <Form.Item>
                        {getFieldDecorator('business_license_no', {
                          rules: isCorporate ? Helper.fieldRules(['required', 'number'], 'Institution Types') : null,
                          initialValue: '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled={!isCorporate}
                            className="field-lg uppercase"
                            placeholder="No Izin Usaha"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Sumber Dana</p>
                        <Form.Item>
                          {getFieldDecorator('fund_source_id', {
                            rules: [
                              ...Helper.fieldRules(['required'], 'Sumber Dana'),
                            ],
                            initialValue: undefined,
                          })(
                            <Select
                              className="field-lg"
                              placeholder="Select Sumber Dana"
                              loading={false}
                            >
                              {(listFundSources.options || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Institution Types</p>
                        <Form.Item>
                          {getFieldDecorator('institution_type_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Institution Types') : null,
                            initialValue: undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Institution Types"
                              loading={false}
                            >
                              {(listInstitutionTypes.options || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Maksud dan Tujuan Usaha</p>
                        <Form.Item>
                          {getFieldDecorator('business_purpose_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Maksud dan Tujuan Usaha') : null,
                            initialValue: undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Sumber Dana"
                              loading={false}
                            >
                              {(listBusinessPurposes.options || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Bidang Usaha</p>
                        <Form.Item>
                          {getFieldDecorator('business_type_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Bidang Usaha') : null,
                            initialValue: undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Sumber Dana"
                              loading={false}
                            >
                              {(listBusinessTypes.options || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Type ID</p>
                      <Form.Item>
                        {getFieldDecorator('type_id', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Type ID') : null,
                          getValueFromEvent: e => e,
                        })(
                          <Select
                            disabled={!isIndividu}
                            placeholder="Select Type ID"
                            className="field-lg"
                          >
                            {[
                              { id: 'ktp', name: 'KTP' },
                              { id: 'kitas', name: 'KITAS' },
                              { id: 'kitap', name: 'KITAP' },
                              { id: 'passport', name: 'Passport' },
                            ].map(item => (
                              <Select.Option
                                key={item.id}
                                value={item.id}
                                disabled={(getFieldValue('citizenship') === 'wna') ? (item.id === 'ktp') : (item.id !== 'ktp')}
                              >
                                {item.name}
                              </Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Nomor ID</p>
                      <Form.Item>
                        {getFieldDecorator('number_id', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('customer_type') === 'corporate' ? [] : ['required'], 'Nomor ID'),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (!(/^[0-9+]+$/).test(value)) {
                                    return Promise.reject('*Nomor ID harus menggunakan angka')
                                  }

                                  if (!value) return Promise.resolve()
                                  if (/^.{16,30}$/.test(value) === false) return Promise.reject('*Wajib 16 Character')

                                  return Promise.resolve()
                                },
                              },
                            ],
                          initialValue: undefined,
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled={getFieldValue('customer_type') === 'corporate'}
                            className="field-lg uppercase"
                            maxLength={getFieldValue('citizenship') === 'wni' ? 16 : 30}
                            placeholder="Number ID"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Nomor NPWP</p>
                      <Form.Item>
                        {getFieldDecorator('npwp', {
                          rules: [
                            ...Helper.fieldRules(isIndividu ? [] : ['required'], 'Nomor NPWP'),
                            {
                              validator: (rule, value) => {
                                let npwp = isEmpty(value) ? '' : value
                                npwp = npwp.split('-').join('')
                                npwp = npwp.split('.').join('')
                                npwp = npwp.split(' ').join('')

                                if (!npwp) return Promise.resolve()
                                // eslint-disable-next-line prefer-promise-reject-errors
                                if (/^.{15,30}$/.test(npwp) === false) return Promise.reject('*Minimal 15 Character')

                                return Promise.resolve()
                              },
                            },
                          ],
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') {
                              handleProfileId(value)
                              return value
                            }
                            return value
                          },
                        })(
                          <NumberFormat
                            className="field-lg ant-input"
                            placeholder="Input Nomor NPWP"
                            format="##.###.###.#-###.########"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Tanggal Habis Berlaku</p>
                      <Form.Item>
                        {getFieldDecorator('expiry_date_identity_id', {
                          rules: getFieldValue('citizenship') !== 'wni' ? Helper.fieldRules(['required'], 'Tanggal Habis Berlaku') : null,
                          initialValue: undefined,
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Tanggal Habis Berlaku"
                            disabled={getFieldValue('citizenship') !== 'wna'}
                            disabledDate={current => (current && (current <= moment().subtract(1, 'day')))}
                            format="DD MMMM YYYY"
                            allowClear
                            onChange={() => {
                              if (getFieldValue('citizenship') === 'wni') {
                                setFieldsValue({
                                  expiry_date_identity_id: '',
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      {(!isCorporate) && (
                        <div>
                          <p className="mb-0">Nama</p>
                          <Form.Item>
                            {getFieldDecorator('name', {
                              rules: Helper.fieldRules(['required', 'notSpecial'], 'Nama'),
                              initialValue: undefined,
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                placeholder="Input Nama"
                                type="text"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                      {(isCorporate) && (
                        <div>
                          <p className="mb-0">Nama Perusahaan</p>
                          <Form.Item>
                            {getFieldDecorator('name', {
                              rules: Helper.fieldRules(['required', 'notSpecial'], 'Nama Perusahaan'),
                              initialValue: undefined,
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                placeholder="Input Nama Perusahaan"
                                type="text"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Marital Status</p>
                      <Form.Item>
                        {getFieldDecorator('martial_status', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Marital Status') : null,
                          getValueFromEvent: e => e,
                        })(
                          <Select
                            disabled={!isIndividu}
                            className="field-lg"
                            placeholder="Select Status"
                            loading={false}
                          >
                            {(listStatuses.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Gender</p>
                      <Form.Item>
                        {getFieldDecorator('gender', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Gender') : null,
                          initialValue: undefined,
                          getValueFromEvent: e => e,
                        })(
                          <Select
                            disabled={!isIndividu}
                            placeholder="Select Gender"
                            className="field-lg"
                          >
                            {[{ id: 'laki-laki', name: 'MALE' }, { id: 'perempuan', name: 'FEMALE' }].map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      {(isCorporate) && (
                        <div>
                          <p className="mb-0">Tanggal Pendirian</p>
                          <Form.Item>
                            {getFieldDecorator('dob', {
                              rules: Helper.fieldRules(['required'], 'Tanggal Pendirian'),
                              initialValue: undefined,
                            })(
                              <DatePicker
                                className="field-lg w-100"
                                placeholder="Input Tanggal Pendirian"
                                disabledDate={current => (current && (current > moment().subtract(0, 'day')))}
                                format="DD MMMM YYYY"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                      {(!isCorporate) && (
                        <div>
                          <p className="mb-0">Tanggal Lahir</p>
                          <Form.Item>
                            {getFieldDecorator('dob', {
                              rules: Helper.fieldRules(['required'], 'Tanggal Lahir'),
                              initialValue: undefined,
                            })(
                              <DatePicker
                                className="field-lg w-100"
                                placeholder="Input Tanggal Lahir"
                                defaultPickerValue={moment().subtract(17, 'year')}
                                disabledDate={current => (current && (current > moment().subtract(0, 'day')))}
                                format="DD MMMM YYYY"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      {(isCorporate) && (
                        <div>
                          <p className="mb-0">Tempat Pendirian</p>
                          <Form.Item>
                            {getFieldDecorator('birthplace', {
                              rules: Helper.fieldRules(['required'], 'Tempat Pendirian'),
                              initialValue: undefined,
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                type="text"
                                placeholder="Input Tempat Pendirian"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                      {(!isCorporate) && (
                        <div>
                          <p className="mb-0">Tempat Lahir</p>
                          <Form.Item>
                            {getFieldDecorator('birthplace', {
                              rules: Helper.fieldRules(['required'], 'Tempat Lahir'),
                              initialValue: undefined,
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                type="text"
                                placeholder="Input Tempat Lahir"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Employment</p>
                      <Form.Item>
                        {getFieldDecorator('employment', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Employment') : null,
                          initialValue: undefined,
                          getValueFromEvent: (e) => {
                            if (e !== 'lainnya') {
                              setFieldsValue({
                                other_employment: '',
                              })
                            }

                            return e
                          },
                        })(
                          <Select
                            disabled={!isIndividu}
                            className="field-lg"
                            placeholder="Type Customer"
                            loading={false}
                          >
                            {(listEmployes.options).map(item => (
                              <Select.Option key={item.value} value={item.value} disabled={getFieldValue('citizenship') === 'wna' && item.value === 'pegawai_negeri_sipil'}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    {getFieldValue('employment') === 'lainnya' && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Other Employment</p>
                        <Form.Item>
                          {getFieldDecorator('other_employment', {
                            rules: Helper.fieldRules(['required'], 'Other Employment'),
                            initialValue: undefined,
                            getValueFromEvent: (e) => {
                              const { value } = e.target
                              if (value !== '') return value
                              return value
                            },
                          })(
                            <Input
                              size="large"
                              placeholder="Other Employment"
                              style={{ textTransform: 'uppercase' }}
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    <Col xs={24} md={12}>
                      <p className="mb-0">Range Penghasilan</p>
                      <Form.Item>
                        {getFieldDecorator('income_range_id', {
                          getValueFromEvent: e => e,
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Range Penghasilan"
                            loading={false}
                            disabled={false}
                          >
                            {(listRangeOfIncomes.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">PIC Name</p>
                      <Form.Item>
                        {getFieldDecorator('pic_name', {
                          rules: [
                            ...Helper.fieldRules(getFieldValue('customer_type') === 'individu' ? [] : ['required', 'notSpecial'], 'PIC Name'),
                          ],
                          initialValue: undefined,
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') return value
                            return value
                          },
                        })(
                          <Input
                            placeholder="Input PIC Name"
                            className="field-lg uppercase"
                            disabled={getFieldValue('customer_type') === 'individu'}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">PIC Phone</p>
                      <Form.Item>
                        {getFieldDecorator('pic_phone_number', {
                          rules: [
                            ...Helper.fieldRules(getFieldValue('customer_type') === 'individu' ? [] : ['required'], 'PIC Phone'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                if (!(/^[0-9+]+$/).test(value)) {
                                  return Promise.reject('*PIC phone harus menggunakan angka')
                                }

                                if (value.charAt(0) == 0) {
                                  return Promise.reject('*Cannot input first number with "0"')
                                }

                                if (value.length < 8) {
                                  return Promise.reject('*Minimal 8 digit')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                          initialValue: undefined ,
                        })(
                          <Input
                            className="field-lg"
                            maxLength={16}
                            size="large"
                            addonBefore={prefixSelectorCode}
                            placeholder="Input PIC Phone Number"
                            disabled={getFieldValue('customer_type') === 'individu'}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Alamat Calon Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-2">Alamat</p>
                      <Form.Item>
                        {getFieldDecorator('address', {
                          rules: Helper.fieldRules(['required'], 'Alamat'),
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            rows={3}
                            size="large"
                            className="uppercase"
                            placeholder="Input Address"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-2">RT</p>
                      <Form.Item>
                        {getFieldDecorator('rt', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'RT'),
                            { pattern: /^[A-Za-z0-9]{0,3}$/, message: '*Maksimal 3 Digit' },
                          ],
                        })(
                          <Input
                            rows={3}
                            size="large"
                            maxLength={5}
                            placeholder="RT"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-2">RW</p>
                      <Form.Item>
                        {getFieldDecorator('rw', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'RW'),
                            { pattern: /^[A-Za-z0-9]{0,3}$/, message: '*Maksimal 3 Digit' },
                          ],
                        })(
                          <Input
                            rows={3}
                            size="large"
                            maxLength={5}
                            placeholder="RW"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Provinsi</p>
                      <Form.Item>
                        {getFieldDecorator('province_id', {
                          rules: Helper.fieldRules(['required'], 'Provinsi'),
                          getValueFromEvent: (e) => {
                            handleSelectResidence('province_id', e)
                            setFieldsValue({
                              city_id: undefined,
                              sub_district_id: undefined,
                              urban_village_id: undefined,
                            })

                            return Number(e)
                          },
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            placeholder="Pilih Provinsi"
                            optionFilterProp="children"
                            loading={false}
                            disabled={false}
                          >
                            {(listProvincy.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kota</p>
                      <Form.Item>
                        {getFieldDecorator('city_id', {
                          rules: Helper.fieldRules(['required'], 'Kota'),
                          getValueFromEvent: (e) => {
                            handleSelectResidence('city', e)
                            setFieldsValue({
                              sub_district_id: undefined,
                              urban_village_id: undefined,
                            })

                            return Number(e)
                          },
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            placeholder="Pilih Kota"
                            optionFilterProp="children"
                            loading={false}
                            disabled={!getFieldValue('province_id')}
                          >
                            {(listCities.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kecamatan</p>
                      <Form.Item>
                        {getFieldDecorator('sub_district_id', {
                          getValueFromEvent: (e) => {
                            handleSelectResidence('sub_district_id', e)
                            setFieldsValue({
                              urban_village_id: undefined,
                            })

                            return Number(e)
                          },
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            optionFilterProp="children"
                            placeholder="Pilih Kecamatan"
                            loading={false}
                            disabled={!getFieldValue('city_id')}
                          >
                            {(listSubDistrict.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kelurahan</p>
                      <Form.Item>
                        {getFieldDecorator('urban_village_id', {
                          getValueFromEvent: e => Number(e),
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            optionFilterProp="children"
                            placeholder="Pilih Kelurahan"
                            loading={false}
                            disabled={!getFieldValue('sub_district_id')}
                          >
                            {(listVillages.options).map(item => (
                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Kode Pos</p>
                      <Form.Item>
                        {getFieldDecorator('postal_code', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Kode POS'),
                            { pattern: /^[A-Za-z0-9]{0,5}$/, message: '*Maksimal 5 digit' },
                          ],
                        })(
                          <Input
                            rows={3}
                            size="large"
                            maxLength={8}
                            placeholder="Kode Pos"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Latitude</p>
                      <Form.Item>
                        {getFieldDecorator('lat', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Latitude'),
                          ],
                        })(
                          <Input
                            rows={3}
                            size="large"
                            placeholder="Latitude"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Longitude</p>
                      <Form.Item>
                        {getFieldDecorator('lng', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Longitude'),
                          ],
                        })(
                          <Input
                            rows={3}
                            size="large"
                            placeholder="Longitude"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">General Info Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Phone Number</p>
                      <Form.Item>
                        {getFieldDecorator('phone_number', {
                          rules: [
                            ...Helper.fieldRules(['required'], 'Phone Number'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                if (!(/^[0-9+]+$/).test(value)) {
                                  return Promise.reject('*Phone number harus menggunakan angka')
                                }

                                if (value.charAt(0) == 0) {
                                  return Promise.reject('*Cannot input first number with "0"')
                                }

                                if (value.length < 8) {
                                  return Promise.reject('*Minimal 8 digit')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                          initialValue: undefined,
                        })(
                          <Input
                            className="field-lg"
                            maxLength={16}
                            size="large"
                            addonBefore={prefixSelector}
                            placeholder="Input Phone Number"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Email</p>
                      <Form.Item>
                        {getFieldDecorator('email', {
                          rules: Helper.fieldRules(['required', 'email'], 'Email'),
                          initialValue: undefined,
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            className="field-lg uppercase"
                            placeholder="Input Email Address"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col span={24}>
                <Button
                  type="primary"
                  className="button-lg w-25 mr-3"
                  onClick={submitAddCustomer}
                >
                  Save
                </Button>
                <Button
                  ghost
                  type="primary"
                  className="button-lg w-25 border-lg"
                  onClick={() => setVisibleAddCust(false)}
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </Modal>
  )
}

AddModalCust.propTypes = {
  getFieldDecorator: PropTypes.any,
  getFieldValue: PropTypes.any,
  setFieldsValue: PropTypes.func,
  visibleAddCust: PropTypes.bool,
  setVisibleAddCust: PropTypes.func,
  // validateFields: PropTypes.func,
  listVillages: PropTypes.object,
  submitAddCustomer: PropTypes.func,
  listSubDistrict: PropTypes.object,
  listStatuses: PropTypes.object,
  listEmployes: PropTypes.object,
  listRangeOfIncomes: PropTypes.object,
  listProvincy: PropTypes.object,
  handleSelectResidence: PropTypes.func,
  listCities: PropTypes.object,
  stateCountry: PropTypes.object,
  stateCountryCode: PropTypes.object,
  listFundSources: PropTypes.object,
  listInstitutionTypes: PropTypes.object,
  listBusinessPurposes: PropTypes.object,
  listBusinessTypes: PropTypes.object,
  handleProfileId: PropTypes.func,

}

AddNewCustomer.propTypes = {
  form: PropTypes.any,
  showModal: PropTypes.func,
  visibleAddCust: PropTypes.bool,
  setVisibleAddCust: PropTypes.func,
  listVillages: PropTypes.object,
  submitAddCustomer: PropTypes.func,
  listSubDistrict: PropTypes.object,
  listStatuses: PropTypes.object,
  stateCountry: PropTypes.object,
  stateCountryCode: PropTypes.object,
  listEmployes: PropTypes.object,
  listRangeOfIncomes: PropTypes.object,
  listProvincy: PropTypes.object,
  handleSelectResidence: PropTypes.func,
  listCities: PropTypes.object,
  listFundSources: PropTypes.object,
  listInstitutionTypes: PropTypes.object,
  listBusinessPurposes: PropTypes.object,
  listBusinessTypes: PropTypes.object,
  handleProfileId: PropTypes.func,
}

export default AddNewCustomer
