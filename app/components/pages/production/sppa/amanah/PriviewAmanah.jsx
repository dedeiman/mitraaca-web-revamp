import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import moment from 'moment'
import { Form } from '@ant-design/compatible'
import { LoadingOutlined } from '@ant-design/icons'
import _, { isEmpty, capitalize } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import { handleProduct } from 'constants/ActionTypes'

const PreviewAmanah = ({
  data,
  show,
  premi,
  handleCancel,
  handleSubmit,
  submitted,
  heirsState,
}) => {
  const parsed = queryString.parse(window.location.search)

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      className="modal-add-customer"
      style={{ width: '80% !important' }}
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">PA Amanah Syariah</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Base SPPA</b></p>
                    <p className="mb-1">{(_.get(data, 'sppa_type', '-')).toUpperCase()}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Indicative Offer / Renewal</b></p>
                    <p className="mb-1">{_.get(data, 'io_renewal', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>No SPPA</b></p>
                    <p className="mb-1"><i>Auto Generate</i></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Policy Holder</b></p>
                    <p className="mb-1">{_.get(data, 'policy_holder_id', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Insured</b></p>
                    <p className="mb-1">{_.get(data, 'insured_id', '-')}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-0"><b>Long Insured Name</b></p>
                    <p className="mb-1">{_.get(data, 'name_on_policy', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Product</b></p>
                    <p><b>{`${capitalize(parsed.type)} - Personal Accident - ${handleProduct(parsed.product)}`}</b></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Periode Awal</b></p>
                    <p className="mb-1">{!isEmpty(_.get(data, 'start_period')) ? moment(_.get(data, 'start_period')).format('DD MMMM YYYY') : '-'}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-0"><b>Periode Akhir</b></p>
                    <p className="mb-1">{!isEmpty(_.get(data, 'end_period')) ? moment(_.get(data, 'end_period')).format('DD MMMM YYYY') : '-'}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col xs={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Ahli Waris</p>
                    {heirsState.map(item => (
                      <div key={item.id}>
                        <Row gutter={24}>
                          <Col xs={24} md={24}>
                            <p className="mb-0"><b>Nama Lengkap</b></p>
                            <p className="mb-1">{_.get(data, `nama_${item.id}`, '-')}</p>
                          </Col>
                        </Row>
                        <Row gutter={24}>
                          <Col xs={24} md={24}>
                            <p className="mb-0"><b>Hubungan</b></p>
                            <p className="mb-1">{!isEmpty(data.relationships.options.filter(itemRealtion => itemRealtion.value === data[`hubungan_${item.id}`])) ? data.relationships.options.filter(itemRealtion => itemRealtion.value === data[`hubungan_${item.id}`])[0].label : ''}</p>
                          </Col>
                        </Row>
                        <Row gutter={24} className="mb-3">
                          <Col xs={24} md={24}>
                            <p className="mb-0"><b>Phone Number</b></p>
                            <p className="mb-1">{_.get(data, `telp_${item.id}`, '-')}</p>
                          </Col>
                        </Row>
                      </div>
                    ))}
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Perhitungan Kontribusi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Kelas Pekerjaan</b></p>
                    <p className="mb-1">{_.get(data, 'job_class', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Besar Nilai Pertanggungan</b></p>
                    <p className="mb-1">{Helper.currency(_.get(data, 'coverage', '-'), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Kontribusi</b></p>
                    <p className="mb-1">{Helper.currency(_.get(data, 'premi', '-'), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={6} md={6}>
                    <p className="mb-1"><b>Diskon</b></p>
                    <p>
                      {_.get(data, 'discount_percentage', 0)}
                      %
                    </p>
                  </Col>
                  <Col xs={6} md={6}>
                    <p className="mb-1">&nbsp;</p>
                    <p>{Helper.currency(_.get(data, 'discount_currency', 0), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={8}>
                    <p className="mt-2"><b>Biaya Administrasi</b></p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item className="mb-0">
                      <Checkbox checked={data.print_policy_book === true}>
                        Buku Polis
                      </Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={8}>
                    <p className="mt-2">Rp 50.000,-</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={8} />
                  <Col xs={24} md={8}>
                    <p className="mb-1">Materai</p>
                  </Col>
                  <Col xs={24} md={8}>
                    <p className="mb-1">{Helper.currency(premi.materai, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={16}>
                    <p className="mb-1">Total Pembayaran</p>
                  </Col>
                  <Col xs={24} md={8}>
                    <p className="mb-1">{Helper.currency((premi.value + premi.materai + (data.print_policy_book === true ? 50000 : 0)), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Form.Item>
                      <Checkbox checked={!isEmpty(data.aggree) ? _.get(data, 'aggree', '-')[0] : false}>
                        Saya telah membaca dan setuju dengan
                        <b> Syarat & Ketentuan Mitraca</b>
                        {' '}
                        yang berlaku
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 mr-3"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {submitted && <LoadingOutlined className="mr-2" />}
            {'Save'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewAmanah.propTypes = {
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
  data: PropTypes.object,
  premi: PropTypes.object,
  heirsState: PropTypes.any,
}

export default PreviewAmanah
