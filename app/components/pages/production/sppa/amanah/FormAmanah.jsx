/* eslint-disable react/no-unused-prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import {
  Row, Col, Divider,
  Card, Input, Select,
  Radio, Checkbox,
  Button, DatePicker,
  AutoComplete,
  InputNumber,
} from 'antd'
import NumberFormat from 'react-number-format'
import _, { capitalize, isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import Hotkeys from 'react-hot-keys'
import queryString from 'query-string'
import { Form } from '@ant-design/compatible'
import { LoadingOutlined } from '@ant-design/icons'
import { handleProduct } from 'constants/ActionTypes'
import AddNewCustomer from 'containers/pages/production/sppa/ModalAddCustomer'
import PreviewAmanah from './PriviewAmanah'
import config from 'app/config'

const CreateIO = ({
  form, handlePremi, premi, heirsState,
  visibleDiscount, setVisibleDiscount,
  onSubmit, relationships, handleHeirs,
  listIO, handleRenewal, listCustomer,
  selectedInsured, setSelectedInsured,
  selectedHolder, setSelectedHolder,
  setSelectedIO, loadCreateSppa, visiblePreview,
  setVisiblePreview, submitted, submitData,
  submitPreview, match, detailAmanah, removeHeirs,
  fetchIdCustomer, appendCustomer, paymentTotal,
  stateCountry, calcDiscountByPercentage, calcDiscountByCurrency,
}) => {
  const {
    getFieldDecorator, getFieldValue, setFieldsValue, getFieldsValue,
  } = form
  const parsed = queryString.parse(window.location.search)
  let sppaType

  if (!isEmpty(detailAmanah.list)) {
    if (!isEmpty(parsed.sppa_type)) {
      sppaType = parsed.sppa_type
    } else {
      sppaType = detailAmanah.list.sppa_type
    }
  }

  return (
    <React.Fragment>
      <Hotkeys
        keyName="shift+`"
        onKeyUp={() => setVisibleDiscount((visibleDiscount === false))}
      />

      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Create'} SPPA`}</div>
        </Col>
      </Row>
      <Form>
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">PA Amanah Syariah</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Base SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('sppa_type', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: sppaType,
                          getValueFromEvent: (e) => {
                            handleRenewal(e)
                            setFieldsValue({ io_renewal: '' })

                            return e
                          },
                        })(
                          <Select
                            className="field-lg"
                            placeholder="Pilih Base"
                            loading={false}
                            disabled={false}
                          >
                            <Select.Option key="new" value="new">NEW</Select.Option>
                            <Select.Option key="renewal" value="renewal">RENEWAL</Select.Option>
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Indicative Offer / Renewal</p>
                      <Form.Item>
                        {getFieldDecorator('io_renewal', {
                          initialValue: parsed.indicative_offer ? parsed.indicative_offer : '',
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Input Indicative Offer / Renewal"
                            dataSource={listIO.options.map(item => (
                              <AutoComplete.Option
                                key={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                                value={getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              >
                                {getFieldValue('sppa_type') === 'new' ? item.io_number : item.sppa_number}
                              </AutoComplete.Option>
                            ))}
                            disabled={getFieldValue('sppa_type') === undefined}
                            onSelect={(val) => {
                              if (getFieldValue('sppa_type') === 'new') {
                                const data = (listIO.options).find(item => item.io_number === val)
                                setSelectedIO(data)
                                setTimeout(() => {
                                  setFieldsValue({
                                    job_class: data.pa_amanah_premi_calculation.job_class,
                                    coverage: data.pa_amanah_premi_calculation.coverage,
                                    premi: data.pa_amanah_premi_calculation.premi,
                                    print_policy_book: data.print_policy_book,
                                    discount_percentage: parseFloat(data.pa_amanah_premi_calculation.discount_percentage),
                                    discount_currency: parseFloat(data.pa_amanah_premi_calculation.discount_currency),
                                  })

                                  handlePremi('job_class', data.pa_amanah_premi_calculation.job_class)
                                  handlePremi('coverage', data.pa_amanah_premi_calculation.coverage)
                                  handlePremi('discount_percentage', data.pa_amanah_premi_calculation.discount_percentage)
                                  handlePremi('discount_currency', data.pa_amanah_premi_calculation.discount_currency)
                                }, 200)
                              } else {
                                const data = (listIO.options).find(item => item.sppa_number === val)
                                setSelectedIO(data)
                                setTimeout(() => {
                                  setFieldsValue({
                                    job_class: data.pa_amanah_premi_calculation.job_class,
                                    coverage: data.pa_amanah_premi_calculation.coverage,
                                    premi: data.pa_amanah_premi_calculation.premi,
                                    print_policy_book: data.print_policy_book,
                                    discount_percentage: parseFloat(data.pa_amanah_premi_calculation.discount_percentage),
                                    discount_currency: parseFloat(data.pa_amanah_premi_calculation.discount_currency),
                                  })

                                  handlePremi('job_class', data.pa_amanah_premi_calculation.job_class)
                                  handlePremi('coverage', data.pa_amanah_premi_calculation.coverage)
                                  handlePremi('discount_percentage', parseFloat(data.pa_amanah_premi_calculation.discount_percentage))
                                  handlePremi('discount_currency', parseFloat(data.pa_amanah_premi_calculation.discount_currency))
                                }, 200)
                              }
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">No SPPA</p>
                      <Form.Item>
                        {getFieldDecorator('indicative_offer', {
                          initialValue: detailAmanah.list.sppa_number || undefined,
                        })(
                          <Input
                            disabled
                            placeholder="Auto Generated By System"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Policy Holder</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={appendCustomer} />
                        </div>
                        {getFieldDecorator('policy_holder_id', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.policy_holder.name : undefined || undefined,
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Policy Holder"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              if (!data) {
                                setSelectedHolder({})
                              } else {
                                setSelectedHolder(data)

                                const insuredId = selectedInsured.id
                                const insuredName = getFieldValue('insured_name')

                                if (insuredId === val || !insuredId) {
                                  setFieldsValue({
                                    name_on_policy: data.name.toUpperCase(),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: (`${data.name} QQ ${insuredName || ''}`).toUpperCase(),
                                  })
                                }

                                setFieldsValue({
                                  policy_holder_id: data.name.toUpperCase(),
                                })
                              }
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Insured</p>
                      <Form.Item>
                        <div className="input-button-transparan">
                          <AddNewCustomer onSuccess={data => appendCustomer(data)} />
                        </div>
                        {getFieldDecorator('insured_id', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.insured.name : undefined || undefined,
                        })(
                          <AutoComplete
                            className="field-lg w-100"
                            placeholder="Insured"
                            dataSource={listCustomer.options.map(item => (
                              <AutoComplete.Option
                                key={item.id}
                                value={item.id}
                              >
                                {item.name.toUpperCase()}
                              </AutoComplete.Option>
                            ))}
                            filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            onSelect={(val) => {
                              const data = (listCustomer.options).find(item => item.id === val)

                              fetchIdCustomer(val)

                              if (!data) {
                                setSelectedInsured({})
                              } else {
                                setSelectedInsured(data)

                                if (selectedHolder.id === val) {
                                  setFieldsValue({
                                    name_on_policy: getFieldValue('policy_holder_id'),
                                  })
                                } else {
                                  setFieldsValue({
                                    name_on_policy: (`${getFieldValue('policy_holder_id') || ''} QQ ${data.name}`).toUpperCase(),
                                  })
                                }

                                setFieldsValue({
                                  insured_id: data.name.toUpperCase(),
                                })
                              }
                            }}
                            style={{ width: 200 }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Long Insured Name</p>
                      <Form.Item>
                        {getFieldDecorator('name_on_policy', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.name_on_policy : undefined || undefined,
                          getValueFromEvent: e => (e.target.value).toUpperCase(),
                        })(
                          <Input
                            disabled
                            placeholder="Input Insured Name"
                            className="field-lg"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Product</p>
                      {match.params.id
                        ? <p>{`${!isEmpty(detailAmanah.list) ? detailAmanah.list.product.type.name : '-'} - Personal Accident - ${!isEmpty(detailAmanah.list) ? detailAmanah.list.product.display_name : '-'}`}</p>
                        : <p>{`${capitalize(parsed.type)} - Personal Accident - ${handleProduct(parsed.product)}`}</p>
                      }
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Awal</p>
                      <Form.Item>
                        {getFieldDecorator('start_period', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? moment(new Date(detailAmanah.list.start_period), 'DD MMMM YYYY') : undefined,
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              end_period: moment(new Date(e), 'DD MMMM YYYY').add(1, 'year'),
                            })

                            return moment(new Date(e), 'DD MMMM YYYY')
                          },
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            disabledDate={current => (current && (current < moment().subtract(0, 'day')))}
                            format="DD MMMM YYYY"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Periode Akhir</p>
                      <Form.Item>
                        {getFieldDecorator('end_period', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? moment(new Date(detailAmanah.list.end_period), 'DD MMMM YYYY') : undefined,
                          getValueFromEvent: (e) => {
                            const difference = moment(new Date(e)).diff(getFieldValue('start_period'), 'year')
                            const endDate = moment(getFieldValue('start_period')).add(difference, 'year').add(0, 'day')

                            return endDate
                          },
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Periode"
                            disabledDate={current => (current && (current < moment(getFieldValue('start_period')).subtract(0, 'day').add(1, 'year')))}
                            format="DD MMMM YYYY"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <Row gutter={[24, 24]}>
                    <Col span={12}>
                      <p className="title-card">Ahli Waris</p>
                    </Col>
                    <Col span={12}>
                      <Button
                        ghost
                        type="primary"
                        disabled={heirsState.disabled}
                        onClick={handleHeirs}
                        className="border-lg d-flex align-items-center ml-3"
                      >
                        <img src="/assets/plus.svg" alt="plus" className="mr-1" />
                        Tambah Ahli Waris
                      </Button>
                    </Col>
                  </Row>
                  { heirsState.heirsCount.map((item, index) => (
                    <>
                      {index !== 0 && <Divider />}
                      <Heirs form={form} relationships={relationships} stateCountry={stateCountry} heirsState={heirsState} removeHeirs={removeHeirs} items={item} index={item.id} />
                    </>
                  ))}
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Perhitungan Kontribusi</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Kelas Pekerjaan</p>
                      <Form.Item>
                        {getFieldDecorator('job_class', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.pa_amanah_premi_calculation.job_class : undefined || undefined,
                        })(
                          <Radio.Group
                            size="large"
                            className="w-100"
                            onChange={(e) => {
                              handlePremi('job_class', e.target.value)
                            }}
                          >
                            <Row>
                              <Col xs={24} md={8}>
                                <Radio value="A">Kelas A</Radio>
                              </Col>
                              <Col xs={24} md={8}>
                                <Radio value="B">Kelas B</Radio>
                              </Col>
                              <Col xs={24} md={8}>
                                <Radio value="C">Kelas C</Radio>
                              </Col>
                            </Row>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Besar Nilai Pertanggungan</p>
                      <Form.Item>
                        {getFieldDecorator('coverage', {
                          rules: [
                            ...Helper.fieldRules(['required', 'number'], 'Nilai'),
                            {
                              type: 'number', min: 10000000, transform: value => Number(value), message: '*Besar Nilai Resiko Sendiri Minimum Rp.10.000.000',
                            },
                          ],
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.pa_amanah_premi_calculation.coverage : undefined,
                          getValueFromEvent: (e) => {
                            handlePremi('coverage', parseFloat(e.target.value.replace('Rp. ', '').replace(/,/g, '')))

                            return e.target.value.replace('Rp. ', '').replace(/,/g, '')
                          },
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            maxLength={25}
                          />,
                        )}
                      </Form.Item>
                      <ul>
                        <li>Meninggal Dunia dan Cacat Tetap (akibat kecelakaan)</li>
                        <li>Manfaat Biaya Pengobatan akibat kecelakaan (10% dari Nilai Pertanggungan)</li>
                        <li>Santunan Duka (10% dari Nilai Pertanggungan)</li>
                      </ul>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-1">Kontribusi</p>
                      <Form.Item>
                        {getFieldDecorator('premi', {
                          initialValue: parseFloat(premi.premi < 0 ? 0 : premi.premi).toFixed(2) || 0,
                          getValueFromEvent: (e) => {
                            handlePremi('coverage', parseFloat(e.target.value.replace('Rp. ', '').replace(/,/g, '')))

                            return e.target.value.replace('Rp. ', '').replace(/,/g, '')
                          },
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            disabled
                            placeholder="- IDR -"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24} style={{ display: visibleDiscount ? '' : 'none' }}>
                    <Col xs={12} md={12}>
                      <p className="mb-0">Diskon</p>
                      <Form.Item>
                        {getFieldDecorator('discount_percentage', {
                          initialValue: !isEmpty(detailAmanah.list) ? detailAmanah.list.pa_amanah_premi_calculation.discount_percentage : undefined,
                          rules: [
                            ...Helper.fieldRules(['number'], 'Nilai'),
                            {
                              type: 'number', max: 100, transform: value => (value ? Number(value) : ''), message: 'Diskon maximal 100%',
                            },
                            {
                              type: 'number', min: 0, transform: value => Helper.replaceNumber(value), message: '*Nilai harus positif',
                            },
                          ],
                        })(
                          <Input
                            placeholder="- Dalam % -"
                            className="field-lg"
                            onChange={(e) => {
                              let discountCurrency = 0
                              const discountPercentage = e.target.value || '0'

                              if (discountPercentage && !_.isNaN(discountPercentage)) {
                                discountCurrency = (premi.premi / 100) * parseFloat(discountPercentage)

                                setTimeout(() => {
                                  handlePremi('discount_currency', parseFloat(discountCurrency))
                                  handlePremi('discount_percentage', parseFloat(discountPercentage))
                                })
                              }

                              // setFieldsValue({
                              //   discount_currency: discountCurrency < 0 ? 0 : parseFloat(discountCurrency),
                              // })
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-1">&nbsp;</p>
                      <Form.Item>
                        {getFieldDecorator('discount_currency', {
                          initialValue: parseFloat(premi.discount_currency).toFixed(2) || 0,
                          rules: [
                            {
                              type: 'number', min: 0, transform: value => Helper.replaceNumber(value), message: '*Nilai harus positif',
                            },
                          ],
                          getValueFromEvent: (e) => {
                            let discountPercentage = 0
                            const discountCurrency = e.target.value || '0'

                            if (discountCurrency && (premi.premi !== '0')) {
                              let value = discountCurrency.replace('Rp. ', '')
                              value = value.replace(/,/g, '')
                              discountPercentage = (parseFloat(value) / premi.premi) * 100

                              setTimeout(() => {
                                handlePremi('discount_currency', parseFloat(value))
                                handlePremi('discount_percentage', parseFloat(discountPercentage))
                              })

                              setFieldsValue({
                                discount_percentage: value >= premi.premi ? 0 : parseFloat(discountPercentage),
                              })
                            }

                            return discountCurrency.replace('Rp. ', '').replace(/,/g, '')
                          },
                        })(
                          <NumberFormat
                            className="ant-input field-lg"
                            thousandSeparator
                            prefix="Rp. "
                            placeholder="- IDR -"
                            onChange={() => {
                              setTimeout(() => handlePremi())
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9}>
                      <p className="mb-1 mt-2">Biaya Administrasi</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <Form.Item style={{ marginBottom: '0' }}>
                        {getFieldDecorator('print_policy_book', {
                          getValueFromEvent: (e) => {
                            setFieldsValue({
                              print_policy_book: e.target.checked,
                            })

                            setTimeout(() => handlePremi('print_policy_book', e.target.checked))

                            return e.target.checked
                          },
                          initialValue: detailAmanah.list.print_policy_book,
                        })(
                          <Checkbox
                            checked={getFieldValue('print_policy_book')}
                          >
                            Buku Polis
                          </Checkbox>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={7}>
                      <b>{Helper.currency(getFieldValue('print_policy_book') ? premi.policy_printing_fee : 0, 'Rp. ', ',-')}</b>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={9} />
                    <Col xs={24} md={8}>
                      <p>Materai</p>
                    </Col>
                    <Col xs={24} md={7}>
                      {Helper.currency(premi.stamp_fee, 'Rp. ', ',-')}
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={16}>
                      <p>Total Pembayaran</p>
                    </Col>
                    <Col xs={24} md={8}>
                      <p
                        className="mb-2"
                        style={{ fontSize: '18px' }}
                      >
                        {Helper.currency(premi.total_payment, 'Rp. ', ',-')}
                      </p>
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('aggree', {
                          rules: Helper.fieldRules(['required']),
                        })(
                          <Checkbox.Group>
                            <Checkbox value="aggree">
                              Saya telah membaca dan setuju dengan
                              &nbsp;
                              <u>
                                <a href={`${config.api_url}/insurance-letters/blonde/term-conditions`} target="_blank">
                                  <b>Syarat & Ketentuan Mitraca</b>
                                </a>
                              </u>
                              &nbsp;
                              yang berlaku
                            </Checkbox>
                          </Checkbox.Group>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row className="mt-3 mb-3">
              <Col span={24} className="d-flex justify-content-end align-items-center">
                <Button
                  ghost
                  type="primary"
                  htmlType="submit"
                  onClick={onSubmit}
                  disabled={loadCreateSppa}
                  className="button-lg w-25 border-lg"
                >
                  {loadCreateSppa && <LoadingOutlined className="mr-2" />}
                  {'Save'}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

const Heirs = ({
  form,
  index,
  items,
  removeHeirs,
  relationships,
  stateCountry,
}) => {
  const {
    getFieldValue,
    getFieldDecorator,
  } = form

  let prefix = '+62'
  if (items.phone_number) {
    prefix = (items.phone_number).charAt(0) === '0' ? '+62' : (items.phone_number).substring(0, 3)
  }
  const prefixSelector = getFieldDecorator('phone_country_code', {
    initialValue: prefix,
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountry.list || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  return (
    <Card>
      <p style={{ textAlign: 'right' }}>
        <span
          style={{
            color: '#2b57b7', paddingTop: 3, paddingBottom: 3, paddingRight: 10, paddingLeft: 10, border: '2px solid #2b57b7', borderRadius: 8,
          }}
          onClick={() => removeHeirs(index)}
        >
          Remove
        </span>
      </p>
      <Row gutter={24}>
        <Col xs={24} md={24}>
          <p className="mb-1">Nama Lengkap</p>
          <Form.Item className="mb-2">
            {getFieldDecorator(`nama_${index}`, {
              initialValue: !isEmpty(items) ? items.name : undefined,
              rules: [
                {
                  validator: (rule, value) => {
                    if (value && !isEmpty(getFieldValue(`hubungan_${index}`)) && !isEmpty(getFieldValue(`telp_${index}`))) return Promise.resolve()
                    // eslint-disable-next-line prefer-promise-reject-errors
                    if (!value && (!isEmpty(getFieldValue(`hubungan_${index}`)) || !isEmpty(getFieldValue(`telp_${index}`)))) return Promise.reject('*Nama Lengkap wajib diisi.')
                    // eslint-disable-next-line prefer-promise-reject-errors
                    if (/^[a-zA-Z0-9- ]*$/.test(value) === false) return Promise.reject('*Tidak boleh menggunakan spesial karakter')

                    return Promise.resolve()
                  },
                },
              ],
              getValueFromEvent: e => (e.target.value).toUpperCase(),
            })(
              <Input
                className="field-lg"
                placeholder="Input Nama Lengkap"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} md={24}>
          <p className="mb-1">Hubungan</p>
          <Form.Item className="mb-2">
            {getFieldDecorator(`hubungan_${index}`, {
              rules: [
                {
                  validator: (rule, value) => {
                    // eslint-disable-next-line prefer-promise-reject-errors
                    if (value && !isEmpty(getFieldValue(`nama_${index}`)) && !isEmpty(getFieldValue(`telp_${index}`))) return Promise.resolve()
                    // eslint-disable-next-line prefer-promise-reject-errors
                    if (!value && (!isEmpty(getFieldValue(`nama_${index}`)) || !isEmpty(getFieldValue(`telp_${index}`)))) return Promise.reject('*Hubungan wajib diisi.')

                    return Promise.resolve()
                  },
                },
              ],
              initialValue: !isEmpty(items) ? items.relationship.name : undefined,
            })(
              <Select
                allowClear
                className="field-lg"
                placeholder="Pilih Tipe"
                loading={relationships.loading}
                disabled={relationships.disabled}
              >
                {(relationships.options).map(item => (
                  <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} md={24}>
          <p className="mb-1">Phone Number</p>
            <Form.Item>
              {getFieldDecorator(`telp_${index}`, {
                initialValue: !isEmpty(items) ? items.phone_number : undefined,
                rules: [
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()
                      if (value && !isEmpty(getFieldValue(`nama_${index}`)) && !isEmpty(getFieldValue(`hubungan_${index}`))) return Promise.resolve()
                      if (!value && (!isEmpty(getFieldValue(`nama_${index}`)) || !isEmpty(getFieldValue(`hubungan_${index}`)))) return Promise.reject('*Phone Number wajib diisi.')

                      if (!(/^[0-9+]+$/).test(value)) {
                        return Promise.reject('*Phone number harus menggunakan angka')
                      }

                      if (value.charAt(0) == 0) {
                        return Promise.reject('*Cannot input first number with "0"')
                      }

                      if (value.length < 8) {
                        return Promise.reject('*Minimal 8 digit')
                      }

                      return Promise.resolve()
                    },
                  },
                ],
              })(
                <Input
                  className="field-lg"
                  maxLength={16}
                  size="large"
                  addonBefore={prefixSelector}
                  placeholder="Input Phone Number"
                />,
              )}
            </Form.Item>
        </Col>
      </Row>
    </Card>
  )
}

Heirs.propTypes = {
  form: PropTypes.any,
  index: PropTypes.number,
  items: PropTypes.object,
  heirsState: PropTypes.object,
  removeHeirs: PropTypes.func,
  relationships: PropTypes.object,
  stateCountry: PropTypes.object,
}

CreateIO.propTypes = {
  form: PropTypes.any,
  premi: PropTypes.object,
  listIO: PropTypes.object,
  handlePremi: PropTypes.func,
  visibleDiscount: PropTypes.bool,
  setVisibleDiscount: PropTypes.func,
  onSubmit: PropTypes.func,
  heirsState: PropTypes.object,
  relationships: PropTypes.object,
  handleHeirs: PropTypes.func,
  handleRenewal: PropTypes.func,
  setSelectedInsured: PropTypes.func,
  setSelectedHolder: PropTypes.func,
  selectedInsured: PropTypes.object,
  selectedHolder: PropTypes.object,
  listCustomer: PropTypes.object,
  appendCustomer: PropTypes.func,
  setSelectedIO: PropTypes.func,
  loadCreateSppa: PropTypes.object,
  visiblePreview: PropTypes.bool,
  setVisiblePreview: PropTypes.func,
  submitted: PropTypes.func,
  submitData: PropTypes.func,
  submitPreview: PropTypes.func,
  match: PropTypes.object,
  removeHeirs: PropTypes.func,
  detailAmanah: PropTypes.object,
  fetchIdCustomer: PropTypes.func,
  costumerDetail: PropTypes.object,
  paymentTotal: PropTypes.any,
  stateCountry: PropTypes.object,
  calcDiscountByCurrency: PropTypes.func,
  calcDiscountByPercentage: PropTypes.func,
}

export default CreateIO
