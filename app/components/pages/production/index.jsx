import React from 'react'
import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import { Result, Button } from 'antd'
import {
  OttomateCode, OttomateSmartCode,
  OttomateSolitareCode, OttomateTLOCode,
  OttomateComprehensiveCode, AsriCode,
  CargoMarineCode, CargoInlandTransitCode,
  LiabilityCode, OttomateSyariahCode,
  OttomateSyariahSmartCode, OttomateSyariahSolitareCode,
  OttomateSyariahTLOCode, OttomateSyariahComprehensiveCode,
  AsriSyariahCode, WellWomenCode, Amanah, TravelInternationalCode,
  TravelDomesticCode,
} from 'constants/ActionTypes'
import CreateIO from 'containers/pages/production/io/otomate/CreateOtomate'
import DetailIO from 'containers/pages/production/io/DetailIO'
import DetailAmanah from 'containers/pages/production/io/amanah/DetailAmanah'
import SearchIO from 'containers/pages/production/io/Search'
import SearchSPPA from 'containers/pages/production/sppa/Search'
import CreateIOLiability from 'containers/pages/production/io/liability/FormLiability'
import CreateIOTravelInternational from 'containers/pages/production/io/travelInternational/FormTravel'
import CreateSPPATravelInternational from 'containers/pages/production/sppa/travelInternational/FormTravel'
import CreateIOTravelDomestic from 'containers/pages/production/io/travel-domestic/Form'
import CreateSPPATravelDomestic from 'containers/pages/production/sppa/travel-domestic/Form'
import CreateIOCargo from 'containers/pages/production/io/cargo/FormCargo'
import CreateIOAsri from 'containers/pages/production/io/asri/FormAsri'
import CreateCobIO from 'containers/pages/production/io/CreateCOB'
import FormOtomate from 'containers/pages/production/sppa/otomate/FormOtomate'
import DetailSPPA from 'containers/pages/production/sppa/DetailSPPA'
import DetailDocument from 'containers/pages/production/sppa/DetailDocument'
import ConfirmSPPAAsri from 'containers/pages/production/sppa/asri/ConfirmAsri'
import ConfirmSPPACargo from 'containers/pages/production/sppa/cargo/ConfirmCargo'
import ConfirmSPPA from 'containers/pages/production/sppa/ConfirmSPPA'
import ConfirmSPPATravelInternational from 'containers/pages/production/sppa/travelInternational/ConfirmTravel'
import ConfirmSPPATravelDomestic from 'containers/pages/production/sppa/travel-domestic/Confirm'
import ConfirmSPPALiability from 'containers/pages/production/sppa/liability/Confirm'
import UploadCargo from 'containers/pages/production/sppa/cargo/UploadCargo'
import UploadOtomate from 'containers/pages/production/sppa/otomate/UploadOtomate'
import UploadSPPAAsri from 'containers/pages/production/sppa/asri/UploadAsri'
import UploadAmanah from 'containers/pages/production/sppa/amanah/UploadAmanah'
import UploadSPPALiability from 'containers/pages/production/sppa/liability/UploadLiability'
import UploadSPPAWellwomen from 'containers/pages/production/sppa/wellwomen/UploadWellwomen'
import UploadSPPATravelInternational from 'containers/pages/production/sppa/travelInternational/UploadTravel'
import UploadSPPATravelDomestic from 'containers/pages/production/sppa/travel-domestic/Upload'
import CreateCobSPPA from 'containers/pages/production/sppa/CreateCOB'
import CreateSPPALiability from 'containers/pages/production/sppa/liability/Form'
import CreateSPPACargo from 'containers/pages/production/sppa/cargo/FormCargo'
import CreateSPPAAsri from 'containers/pages/production/sppa/asri/FormAsri'
import CreateIOWellWomen from 'containers/pages/production/io/wellwomen/CreateWellWomen'
import CreateSPPAWellWomen from 'containers/pages/production/sppa/wellwomen/CreateWellWomen'
import CreateIOAmanah from 'containers/pages/production/io/amanah/FormAmanah'
import FormAmanah from 'containers/pages/production/sppa/amanah/FormAmanah'
import ConfirmSPPAWellWomen from 'containers/pages/production/sppa/wellwomen/ConfirmWellwomen'
import ConfirmOfferIO from 'containers/pages/production/io/ConfirmOfferIO'
import ConfirmOfferSPPA from 'containers/pages/production/sppa/ConfirmOfferSPPA'
import qs from 'query-string'
import history from 'utils/history'

const ProductRoutes = ({ currentUser, match, location, stateCheck }) => {
  const productCode = qs.parse(location.search).product

  // if (PermissionPage('productionAll', groupRole.code)) {
  //   return <Forbidden />
  // }

  if (location.pathname === '/production-create-io') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if ((currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1)) {
      return <CreateCobIO match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/production-create-sppa') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if ((currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1)) {
      return <CreateCobSPPA match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/production-search-io-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io') > -1) {
      return <SearchIO match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/production-search-sppa-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if ((currentUser.permissions && currentUser.permissions.indexOf('production-search-sppa') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-sppa') > -1)) {
      return <SearchSPPA match={match} location={location} />
    }
    return <Forbidden />
  }

  if (location.pathname.includes('/production-create-sppa/document/detail')) {
    return <DetailDocument match={match} location={location} />
  }

  if (location.pathname.includes('/production-create-sppa/confirm-offer')) {
    return <ConfirmOfferSPPA match={match} location={location} />
  }

  if (location.pathname.includes('/production-create-io/confirm-offer')) {
    return <ConfirmOfferIO match={match} location={location} />
  }

  if (match.params.id && !location.pathname.includes('/edit') && !location.pathname.includes('/create') && !location.pathname.includes('/production-create-io') && !location.pathname.includes('/confirm') && !location.pathname.includes('/upload') && !location.pathname.includes('/edit')) {
    if ((currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)) {
      return <DetailSPPA match={match} location={location} />
    }
    return <Forbidden />
  }

  if (match.params.id && !location.pathname.includes('/edit') && !location.pathname.includes('/create') && !location.pathname.includes('/production-create-sppa') && !location.pathname.includes('/confirm') && !location.pathname.includes('/upload') && !location.pathname.includes('/edit')) {
    if ((currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)
        || (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1)) {
      return <DetailIO match={match} location={location} />
    }
    return <Forbidden />
  }

  if (match.params.id && location.pathname.includes('/confirm-cargo')) {
    return <ConfirmSPPACargo match={match} location={location} />
  }

  if (match.params.id && location.pathname.includes('/confirm-asri')) {
    return <ConfirmSPPAAsri match={match} location={location} />
  }

  if (match.params.id && location.pathname.includes('/confirm-travel-international')) {
    return <ConfirmSPPATravelInternational match={match} location={location} />
  }

  if (qs.parse(location.search).product) {
    switch (productCode) {
      case OttomateCode:
      case OttomateSmartCode:
      case OttomateSolitareCode:
      case OttomateTLOCode:
      case OttomateComprehensiveCode:
      case OttomateSyariahCode:
      case OttomateSyariahSmartCode:
      case OttomateSyariahSolitareCode:
      case OttomateSyariahTLOCode:
      case OttomateSyariahComprehensiveCode:
        if (location.pathname === '/production-create-sppa/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1) {
            return <FormOtomate match={match} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-sppa-edit') > -1) {
            return <FormOtomate match={match} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          return <CreateIO match={match} />
        }

        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIO match={match} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPA match={match} page="Otomate" />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadOtomate match={match} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1) {
            return <DetailIO match={match} location={location} />
          }
          return <Forbidden />
        }
        break
      case WellWomenCode:
        if (location.pathname === '/production-create-io/create') {
          return <CreateIOWellWomen match={match} />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          return <CreateSPPAWellWomen match={match} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadSPPAWellwomen match={match} location={location} />
        }

        if (location.pathname === '/production-create-sppa/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1) {
            return <CreateSPPAWellWomen match={match} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/confirm')) {
          return <ConfirmSPPAWellWomen match={match} />
        }

        if (location.pathname.includes('/production-create-sppa/detail')) {
          return <DetailSPPA match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          return <DetailIO match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          return <CreateIOWellWomen match={match} />
        }
        break
      case AsriCode:
      case AsriSyariahCode:
        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIOAsri match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-edit') > -1) {
            return <CreateIOAsri match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname === '/production-create-sppa/create') {
          return <CreateSPPAAsri match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          return <CreateSPPAAsri match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPAAsri match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadSPPAAsri match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1) {
            return <DetailIO match={match} location={location} />
          }
          return <Forbidden />
        }
        break
      case CargoMarineCode:
      case CargoInlandTransitCode:
        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIOCargo match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-edit') > -1) {
            return <CreateIOCargo match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname === '/production-create-sppa/create') {
          return <CreateSPPACargo match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          return <CreateSPPACargo match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPACargo match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadCargo match={match} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1) {
            return <DetailIO match={match} location={location} />
          }
          return <Forbidden />
        }
        break
      case LiabilityCode:
        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIOLiability match={match} location={location} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadSPPALiability match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-edit') > -1) {
            return <CreateIOLiability match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          return <CreateSPPALiability match={match} location={location} />
        }

        if (location.pathname === '/production-create-sppa/create') {
          return <CreateSPPALiability match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/confirm')) {
          return <ConfirmSPPALiability match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-io-detail') > -1) {
            return <DetailIO match={match} location={location} />
          }
          return <Forbidden />
        }
        break
      case Amanah:
        if (location.pathname.includes('/production-create-sppa/create')) {
          return <FormAmanah match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          return <FormAmanah match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          return <CreateIOAmanah match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPA match={match} page="Amanah" />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          return <DetailAmanah match={match} location={location} />
        }

        if (location.pathname === '/production-create-io/create') {
          return <CreateIOAmanah match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadAmanah match={match} />
        }
        break
      case TravelInternationalCode:
        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIOTravelInternational match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          return <CreateIOTravelInternational match={match} />
        }

        if (location.pathname === '/production-create-sppa/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1) {
            return <CreateSPPATravelInternational match={match} location={location} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/edit')) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-sppa-edit') > -1) {
            return <CreateSPPATravelInternational match={match} location={location} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPATravelInternational match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          return <DetailIO match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadSPPATravelInternational match={match} />
        }
        return true
      case TravelDomesticCode:
        if (location.pathname === '/production-create-io/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-io') > -1) {
            return <CreateIOTravelDomestic match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-io/edit') && match.params.id) {
          return <CreateIOTravelDomestic match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPA match={match} page="TravelDomestic" />
        }

        if (location.pathname === '/production-create-sppa/create') {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-create-sppa') > -1) {
            return <CreateSPPATravelDomestic match={match} location={location} />
          }
          return <Forbidden />
        }

        if (location.pathname.includes('/production-create-sppa/edit') && match.params.id) {
          if (currentUser.permissions && currentUser.permissions.indexOf('production-search-sppa-edit') > -1) {
            return <CreateSPPATravelDomestic match={match} location={location} />
          }
          return <Forbidden />
        }

        if (match.params.id && location.pathname.includes('/confirm')) {
          return <ConfirmSPPATravelDomestic match={match} location={location} />
        }

        if (location.pathname.includes('/production-create-io/detail')) {
          return <DetailIO match={match} location={location} />
        }

        if (match.params.id && location.pathname.includes('/production-create-sppa/upload/')) {
          return <UploadSPPATravelDomestic match={match} />
        }

        return true
      default:
        return (
          <Result
            status="404"
            title="404"
            subTitle="Sorry, the page you visited does not exist."
            extra={<Button type="primary" onClick={() => history.push('/dashboard')}>Back Home</Button>}
          />
        )
    }
  }

  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={<Button type="primary" onClick={() => history.push('/dashboard')}>Back Home</Button>}
    />
  )
}

ProductRoutes.propTypes = {
  match: PropTypes.object,
  currentUser: PropTypes.object,
  location: PropTypes.any,
  stateCheck: PropTypes.object,
}

export default ProductRoutes
