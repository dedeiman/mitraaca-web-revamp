import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Descriptions, Tag,
} from 'antd'
import history from 'utils/history'
import Helper from 'utils/Helper'
import { LeftOutlined } from '@ant-design/icons'

const DetailOtomate = ({
  detailData,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-io-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Detail IO Amanah</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailData.loading}>
                {
                  !detailData.loading
                  && (
                    <>
                      <p className="title-card">Informasi Polis</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Indicative Offer" className="px-1 profile-detail pb-0">
                          AUTO GENERATED
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Nama tertanggung" className="px-1 profile-detail pb-0">
                          {detailData.list.name || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="No Handphone" className="px-1 profile-detail pb-0">
                          {detailData.list.phone_country_code.phone_code || '-'} {detailData.list.phone_number || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Email" className="px-1 profile-detail pb-0 uppercase">
                          {detailData.list.email || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Produk" className="px-1 profile-detail pb-0">
                          {detailData.list.product.type.name || '-'}
                          {' - Personal Accident - '}
                          {detailData.list.product.display_name || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Pertanggungan" className="px-1 profile-detail pb-0">
                          1 Tahun
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="" loading={detailData.loading}>
                {
                  !detailData.loading
                  && (
                    <>
                      <p className="title-card">Perhitungan Premi</p>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={2} label="Kelas Pekerjaan" className="px-1 profile-detail pb-0">
                          Kelas
                          {' '}
                          {detailData.list.pa_amanah_premi_calculation.job_class || '-'}
                        </Descriptions.Item>
                        <Descriptions.Item span={2} label="Nilai Pertanggungan" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.pa_amanah_premi_calculation.coverage, 'Rp. ', ',-')}
                          <ul>
                            <li>Meninggal Dunia dan Cacat Tetap (akibat kecelakaan)</li>
                            <li>Manfaat Biaya Pengobatan akibat kecelakaan (10% dari Nilai Pertanggungan)</li>
                            <li>Santunan Duka (10% dari Nilai Pertanggungan)</li>
                          </ul>
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Discount Currency" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.pa_amanah_premi_calculation.discount_currency, 'Rp. ', ',-')}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Discount Percentage" className="px-1 profile-detail pb-0">
                          {detailData.list.pa_amanah_premi_calculation.discount_percentage}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Kontribusi" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.pa_amanah_premi_calculation.premi, 'Rp. ', ',-')}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Buku Polis" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.policy_printing_fee, 'Rp. ', ',-')}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Materai" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.stamp_fee, 'Rp. ', ',-')}
                        </Descriptions.Item>
                        <Descriptions.Item span={isMobile ? 2 : 1} label="Total Pembayaran" className="px-1 profile-detail pb-0">
                          {Helper.currency(detailData.list.total_payment, 'Rp. ', ',-')}
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${detailData.list.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${detailData.list.id}?product=${detailData.list.product.code}&product_id=${detailData.list.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailOtomate.propTypes = {
  detailData: PropTypes.object,
}

export default DetailOtomate
