import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button, Tag,
} from 'antd'
import {LeftOutlined, LoadingOutlined} from '@ant-design/icons'
import history from 'utils/history'
import config from 'app/config'

const ConfirmOfferIO = ({
  detailData, handleConfirmOffer, stateLoad,
  stateLoadIframe, handleLoadIframe,
}) => {
  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Konfirmasi Penawaran IO</div>
        </Col>
      </Row>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="primary"
            className="ml-2"
            onClick={handleConfirmOffer}
            disabled={(stateLoad.loading)}
          >
            {stateLoad.loading && <LoadingOutlined className="mr-2" />}
            Kirim Penawaran
          </Button>
          <Button
            type="primary"
            className="ml-2"
          >
            <a href={`${config.api_url}/indicative-offers/${detailData.id_io}/preview`}>Download</a>
          </Button>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={24}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card>
                {stateLoadIframe.loadingIframe ? (
                  <LoadingOutlined className="mr-2" />) : null }
                <iframe onLoad={handleLoadIframe} src={`${config.api_url}/indicative-offers/${detailData.id_io}/previewpdf#toolbar=0`} width="100%" height="950px"></iframe>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

ConfirmOfferIO.propTypes = {
  detailData: PropTypes.object,
  handleConfirmOffer: PropTypes.any,
  handleLoadIframe: PropTypes.any,
  stateLoad: PropTypes.object,
  stateLoadIframe: PropTypes.object,
}

export default ConfirmOfferIO
