import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

// import { Result, Button } from 'antd'
// import history from 'utils/history'
import {
  OttomateCode, OttomateSmartCode,
  OttomateSolitareCode, OttomateTLOCode,
  OttomateComprehensiveCode, AsriCode,
  CargoMarineCode, CargoInlandTransitCode,
  LiabilityCode, OttomateSyariahCode,
  OttomateSyariahSmartCode, OttomateSyariahSolitareCode,
  OttomateSyariahTLOCode, OttomateSyariahComprehensiveCode,
  AsriSyariahCode, WellWomenCode, Amanah,
  TravelInternationalCode, TravelDomesticCode,
} from 'constants/ActionTypes'
import DetailOtomate from './otomate/DetailOtomate'
import DetailAmanah from './amanah/DetailAmanah'
import DetailLiability from './liability/DetailLiability'
import DetailWellWomen from './wellwomen/DetailWellWomen'
import DetailAsri from './asri/DetailAsri'
import DetailCargo from './cargo/DetailCargo'
import DetailTravelInternational from './travelInternational/DetailTravel'
import DetailTravelDomestic from './travel-domestic/Detail'

const DetailIO = ({
  detailData,
}) => (
  <React.Fragment>
    {(() => {
      const productCode = _.get(detailData.list, 'product.code', undefined)
      switch (productCode) {
        case OttomateCode:
        case OttomateSmartCode:
        case OttomateSolitareCode:
        case OttomateTLOCode:
        case OttomateComprehensiveCode:
        case OttomateSyariahCode:
        case OttomateSyariahSmartCode:
        case OttomateSyariahSolitareCode:
        case OttomateSyariahTLOCode:
        case OttomateSyariahComprehensiveCode:
          return <DetailOtomate detailOtomate={detailData} loading={false} />
        case WellWomenCode:
          return <DetailWellWomen detailData={detailData} loading={false} />
        case AsriCode:
        case AsriSyariahCode:
          return <DetailAsri detailData={detailData} loading={false} />
        case CargoMarineCode:
        case CargoInlandTransitCode:
          return <DetailCargo detailData={detailData} loading={false} />
        case LiabilityCode:
          return <DetailLiability detailData={detailData} loading={false} />
        case TravelInternationalCode:
          return <DetailTravelInternational detailData={detailData} loading={false} />
        case TravelDomesticCode:
          return <DetailTravelDomestic detailData={detailData} loading={false} />
        case Amanah:
          return <DetailAmanah detailData={detailData} loading={false} />

        default:
          // return (
          //   <Result
          //     status="404"
          //     title="404"
          //     subTitle="Sorry, the page you visited does not exist."
          //     extra={<Button type="primary" onClick={() => history.push('/dashboard')}>Back Home</Button>}
          //   />
          // )
      }

      return false
    })()}
  </React.Fragment>
)
DetailIO.propTypes = {
  detailData: PropTypes.object,
}

export default DetailIO
