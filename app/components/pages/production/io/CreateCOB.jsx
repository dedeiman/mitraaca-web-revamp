import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card,
  Button, Select,
  Radio, Input,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

const CreateCOB = ({
  stateCategory, handleProduct,
  handleProductType, getProduct,
  onSubmit, form, stateCategorySyariah,
}) => {
  const { getFieldDecorator, setFieldsValue, getFieldValue } = form
  const isMobile = window.innerWidth < 768

  return (
    <>
      <Row className="pt-5 mt-5" gutter={24} type="flex" justify="center">
        <Col xs={24} md={16}>
          <Card className="m-0 auto">
            <Row>
              <Col md={24}>
                <div className="title-page">Indicative Offer</div>
                <p>Masa berlaku 30 hari</p>
              </Col>
              <Col md={24}>
                <Form
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 16 }}
                  onSubmit={onSubmit}
                >
                  <Form.Item>
                    {getFieldDecorator('type_product', {
                      initialValue: 'konvensional',
                      rules: Helper.fieldRules(['required']),
                    })(
                      <Radio.Group
                        size="large"
                        className="w-100"
                        onChange={(e) => {
                          handleProductType(e.target.value)
                          setFieldsValue({
                            cob: undefined,
                            product: undefined,
                          })
                        }}
                      >
                        <Radio value="konvensional">Konvensional</Radio>
                        <Radio value="syariah">Syariah</Radio>
                      </Radio.Group>,
                    )}
                  </Form.Item>
                  <Row gutter={[24, 24]}>
                    <Col xs={24} md={6}>
                      <b>
                        <span style={{ color: 'red' }}>*</span>
                        Class Bussiness
                      </b>
                    </Col>
                    <Col xs={24} md={14} className="pt-0">
                      {getFieldDecorator('cob', {
                        rules: Helper.fieldRules(['required'], 'Class Bussiness'),
                      })(
                        <Select
                          onChange={(e) => {
                            handleProduct(e)
                          }}
                          loading={stateCategory.loading}
                          placeholder="Select COB"
                          className="field-lg w-100"
                          disabled={getProduct.type === ''}
                          onSelect={() => {
                            setFieldsValue({
                              product_id: '',
                              product: undefined,
                            })
                          }}
                        >
                          {getFieldValue('type_product') === 'konvensional'
                            && (stateCategory.list || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))
                          }
                          {getFieldValue('type_product') === 'syariah'
                            && (stateCategorySyariah.list || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))
                          }
                        </Select>,
                      )}
                    </Col>
                    <Col xs={24} md={6}>
                      <b>
                        <span style={{ color: 'red' }}>*</span>
                        Product
                      </b>
                    </Col>
                    <Col xs={24} md={14} className="pt-0">
                      {getFieldDecorator('product', {
                        rules: Helper.fieldRules(['required'], 'Product'),
                      })(
                        <Select
                          loading={getProduct.loading}
                          placeholder="Select Product"
                          className={`field-lg w-100`}
                          disabled={!getFieldValue('cob')}
                          onSelect={(val) => {
                            const data = getProduct.list.find(item => item.code === val)
                            setFieldsValue({
                              product_id: data.id,
                              product: val,
                            })
                          }}
                        >
                          {(getProduct.list || []).map(item => (
                            <Select.Option key={item.id} value={item.code}>{item.display_name}</Select.Option>
                          ))}
                        </Select>,
                      )}
                    </Col>
                  </Row>
                  <Form.Item label="Product ID" className="d-none">
                    {getFieldDecorator('product_id', {
                      rules: Helper.fieldRules(['required']),
                    })(
                      <Input
                        className="field-lg"
                        placeholder=""
                        disabled
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary button-lg mt-4" htmlType="submit" disabled={false}>
                      Create IO
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  )
}

CreateCOB.propTypes = {
  stateCategory: PropTypes.object,
  stateCategorySyariah: PropTypes.object,
  handleProduct: PropTypes.func,
  handleProductType: PropTypes.func,
  getProduct: PropTypes.object,
  onSubmit: PropTypes.func,
  form: PropTypes.any,
}

export default CreateCOB
