import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _ from 'lodash'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'

const PreviewCargo = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  let currencyText = '-'
  const currencyList = data.stateSelects ? data.stateSelects.currencyList : []

  if (data.currency_id && (data.stateSelects.currencyList || []).length) {
    const currency = currencyList.find(o => o.id === data.currency_id)

    if (currency) {
      currencyText = currency.name
    }
  }

  let cargoCategoryText = '-'
  const cargoCategoryList = data.stateSelects ? data.stateSelects.cargoCategoryList : []

  if (data.cargo_category_id && data.stateSelects.cargoCategoryList.length) {
    const cargoCategory = cargoCategoryList.find(o => o.id === data.cargo_category_id)

    if (cargoCategory) {
      cargoCategoryText = cargoCategory.name
    }
  }

  let cargoTypeText = '-'
  const cargoTypeList = data.stateSelects ? data.stateSelects.cargoTypeList : []

  if (data.cargo_type_id && data.stateSelects.cargoTypeList.length) {
    const cargoType = cargoTypeList.find(o => o.id === data.cargo_type_id)

    if (cargoType) {
      cargoTypeText = cargoType.name
    }
  }

  let vehicleTypeText = '-'
  const vehicleTypeList = data.stateSelects ? data.stateSelects.vehicleTypeList : []

  if (data.vehicle_type_id && data.stateSelects.vehicleTypeList.length) {
    const vehicleType = vehicleTypeList.find(o => o.id === data.vehicle_type_id)

    if (vehicleType) {
      vehicleTypeText = vehicleType.name
    }
  }

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      wrapClassName="wrap-modal-preview"
      className="modal-preview"
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">IO Cargo</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nomor Penawaran</b></p>
                    <p className="mb-1"><i>Auto Generate</i></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nama</b></p>
                    <p className="mb-1">{_.get(data, 'name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Email</b></p>
                    <p className="mb-1">{_.get(data, 'email', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Phone</b></p>
                    <p className="mb-1">{_.get(data, 'phone_number', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Product</b></p>
                    <p>
                      <b>
                        {_.get(data, 'productDetail.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'productDetail.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'productDetail.display_name')}
                      </b>
                    </p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Cargo Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Currency</b></p>
                    <p className="mb-1">{currencyText}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Sum Insured</b></p>
                    <p className="mb-1">{Helper.currency(data.sum_insured, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <p className="mb-0"><i>Deductible 5% of total sum insured</i></p>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1">Main Coverage, Conditions, Waranties, Etc.</p>
                    <p className="mb-1">{data.other_information}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Vesel Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Category Cargo</b></p>
                    <p className="mb-1">{cargoCategoryText}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Type Cargo</b></p>
                    <p className="mb-1">{cargoTypeText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Vehicle Type</b></p>
                    <p className="mb-1">{vehicleTypeText}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">Rate</p>
                        <p className="mb-1">
                          {data.rate}
                          %
                        </p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">Premi</p>
                        <p><b>{Helper.currency(data.premi.premi, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <p className="mb-1">Diskon</p>
                        <p className="mb-1">
                          {_.get(data, 'discount_percentage', 0)}
                          %
                        </p>
                      </Col>
                      <Col xs={12} md={12}>
                        <p className="mb-1">&nbsp;</p>
                        <p className="mb-1">
                          {Helper.currency(data.discount_amount, 'Rp. ', ',-')}
                        </p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true}>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.print_policy_book ? data.bookPrice : 0, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2" style={{ fontSize: '18px' }}><b>{Helper.currency(data.paymentTotal, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <br />
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Form.Item>
                          <Checkbox checked={data.aggree === true}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <b>Syarat & Ketentuan Mitraca</b>
                            &nbsp;
                            yang berlaku
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 border-lg"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewCargo.propTypes = {
  data: PropTypes.any,
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
}

export default PreviewCargo
