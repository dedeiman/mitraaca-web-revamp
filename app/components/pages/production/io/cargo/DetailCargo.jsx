import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Checkbox,
  Button, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { LeftOutlined } from '@ant-design/icons'

const DetailCargo = ({
  detailData,
}) => {
  const data = detailData.list
  const htmlDecode = (input) => {
    const e = document.createElement('div')
    e.innerHTML = input
    return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue
  }

  const renderHTML = rawHTML => React.createElement('div', { dangerouslySetInnerHTML: { __html: rawHTML } })

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-io-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail IO ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nomor Penawaran" className="px-1 profile-detail pb-0">
                        {_.get(data, 'io_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama" className="px-1 profile-detail pb-0 uppercase">
                        {_.get(data, 'name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Email" className="px-1 profile-detail pb-0 uppercase">
                        {_.get(data, 'email', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="No Handphone" className="px-1 profile-detail pb-0">
                        {_.get(data, 'phone_country_code.phone_code')} {_.get(data, 'phone_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {/* <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Periode Pertanggungan</b></p>
                    <p>1 Tahun</p>
                  </Col>
                </Row> */}
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Cargo Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Currency" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.currency.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Sum Insured" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'cargo_information.sum_insured', 0), `${_.get(data, 'cargo_information.currency.code', '-')} `, ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <p className="mb-0"><i>Deductible 5% of total sum insured</i></p>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Main Coverage" className="px-1 profile-detail pb-0">
                        {_.get(data, 'cargo_information.main_coverage', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Main Coverage, Conditions, Waranties, Etc.</b></p>
                    <div
                      className="mb-1"
                      style={{
                        border: '2px solid #EAEAEA',
                        height: '400px',
                        overflow: 'scroll',
                        padding: '10px 0',
                        borderTopLeftRadius: '10px',
                        borderBottomLeftRadius: '10px',
                      }}
                    >
                      {renderHTML(htmlDecode(_.get(data, 'cargo_information.other_information')))}
                    </div>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Vesel Information</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Category Cargo" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.cargo_type.category.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Type Cargo" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.cargo_type.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Vehicle Type" className="px-1 profile-detail pb-0">
                        {_.get(data, 'vessel_information.vehicle_type.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Rate" className="px-1 profile-detail pb-0">
                            {_.get(data, 'cargo_premi_calculation.rate', 0).toFixed(2)}
                            %
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Premi" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'cargo_premi_calculation.premi', 0), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                            {_.get(data, 'cargo_premi_calculation.discount_percentage', 0)}
                            %
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'cargo_premi_calculation.discount_amount', 0), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailCargo.propTypes = {
  detailData: PropTypes.object,
}

export default DetailCargo
