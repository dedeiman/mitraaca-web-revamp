import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button,
  Checkbox, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import moment from 'moment'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { LeftOutlined } from '@ant-design/icons'

const DetailOtomate = ({
  detailData,
}) => {
  const data = detailData.list

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-io-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail IO ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Nomor Penawaran" className="px-1 profile-detail pb-0">
                          {_.get(data, 'io_number', '-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Nama" className="px-1 profile-detail pb-0">
                          <span className="uppercase">
                            {_.get(data, 'name', '-')}
                          </span>
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Email" className="px-1 profile-detail pb-0">
                          <span className="uppercase">
                            {_.get(data, 'email', '-')}
                          </span>
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="No Handphone" className="px-1 profile-detail pb-0">
                          <span className="uppercase">
                            {_.get(data, 'phone_country_code.phone_code')} {_.get(data, 'phone_number', '-')}
                          </span>
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Produk" className="px-1 profile-detail pb-0">
                          {_.get(data, 'product.type.name')}
                          &nbsp;-&nbsp;
                          {_.get(data, 'product.class_of_business.name')}
                          &nbsp;-&nbsp;
                          {_.get(data, 'product.display_name')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {/* <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Periode Pertanggungan</b></p>
                    <p>1 Tahun</p>
                  </Col>
                </Row> */}
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Anggota</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Paket" className="px-1 profile-detail pb-0">
                          {data.travel_information.package_type === 'family' ? 'Family' : 'Individu'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Tujuan dan Ahli Waris</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Tujuan Perjalanan" className="px-1 profile-detail pb-0">
                          {data.travel_information.travel_destination.name || '-'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="No Passport" className="px-1 profile-detail pb-0">
                          {data.travel_information.passport_number || '-'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Negara Tujuan (Internasional)" className="px-1 profile-detail pb-0">
                          {data.travel_information.destination_countries.map(countrie => (
                            <p>{countrie.name}</p>
                          ))}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Negara Kota Asal (Domestic)" className="px-1 profile-detail pb-0">
                          {data.travel_information.hometown.name || '-'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Ahli Waris" className="px-1 profile-detail pb-0">
                          {data.travel_information.heir_name || '-'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Hubungan" className="px-1 profile-detail pb-0">
                          {data.travel_information.relationship.name || '-'}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Tanggal keberangkatan" className="px-1 profile-detail pb-0">
                          {moment(data.travel_information.departure_date).format('DD MMM YYYY')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1">&nbsp;</p>
                    <Checkbox
                      checked={data.travel_information.is_annual}
                    >
                      Annual
                    </Checkbox>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Tanggal Kembali" className="px-1 profile-detail pb-0">
                          {moment(data.travel_information.return_date).format('DD MMM YYYY')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Jenis Plan" className="px-1 profile-detail pb-0">
                              {(data.travel_information.plan_type || '-').toUpperCase()}
                            </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <div className="px-1">
                          <p className="mb-2 ant-descriptions-item-label">
                            Jumlah Hari yang Dipertanggungkan (
                            {data.travel_premi_calculation.total_days_insured}
                            )
                          </p>
                          <p className="text-dark fw-bold">
                            {Helper.currency(data.travel_premi_calculation.price_days_insured, 'Rp. ', ',-')}
                          </p>
                        </div>
                      </Col>
                    </Row>
                    <Row gutter={24} className="mt-2">
                      <Col xs={24} md={24}>
                        <div className="px-1">
                          <p className="mb-2 ant-descriptions-item-label">
                            Tambahan Per Minggu (
                            {data.travel_premi_calculation.total_additional_week}
                            )
                          </p>
                          <p className="text-dark fw-bold">
                              {Helper.currency(data.travel_premi_calculation.price_additional_week, 'Rp. ', ',-')}
                          </p>
                        </div>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Loading Premi (Di atas 70 Thn) (Internasional)" className="px-1 profile-detail pb-0">
                              {Helper.currency(data.travel_premi_calculation.loading_premi, 'Rp. ', ',-')}
                            </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Total Premi" className="px-1 profile-detail pb-0">
                              {Helper.currency(data.travel_premi_calculation.premi, 'Rp. ', ',-')}
                            </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                              {data.travel_premi_calculation.discount_percentage}
                              {' '}
                              %
                            </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={12} md={12}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                            <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                              {Helper.currency(data.travel_premi_calculation.discount_amount, 'Rp. ', ',-')}
                            </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailOtomate.propTypes = {
  detailData: PropTypes.object,
}

export default DetailOtomate
