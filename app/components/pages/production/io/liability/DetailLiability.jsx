import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button,
  Checkbox,
  Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _, { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { LeftOutlined } from '@ant-design/icons'

const DetailLiability = ({
  detailData,
}) => {
  const data = detailData.list

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-io-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail IO ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailData.loading}>
                <p className="title-card">IO Liability</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Nomor Penawaran" className="px-1 profile-detail pb-0">
                          {_.get(data, 'io_number', '-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Nama" className="px-1 profile-detail pb-0 uppercase">
                          {_.get(data, 'name', '-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Email" className="px-1 profile-detail pb-0 uppercase">
                          {_.get(data, 'email', '-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Phone Number" className="px-1 profile-detail pb-0">
                          {_.get(data, 'phone_country_code.phone_code', '-')} {_.get(data, 'phone_number', '-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Product" className="px-1 profile-detail pb-0">
                        {`${!isEmpty(data) ? data.product.type.name : '-'} - Liability - ${!isEmpty(data) ? data.product.display_name : '-'}`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Periode Pertanggungan" className="px-1 profile-detail pb-0">
                        1 Tahun
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="" loading={detailData.loading}>
                <p className="title-card">Perhitungan Premi / Kontribusi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Nilai Pertanggungan Perlengkapan" className="px-1 profile-detail pb-0">
                          {Helper.currency(data.liability_premi_calculation.coverage_limit, 'Rp. ', ',-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Premi" className="px-1 profile-detail pb-0">
                          {Helper.currency(data.liability_premi_calculation.premi, 'Rp. ', ',-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={6} md={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                            {!isEmpty(data.liability_premi_calculation) ? data.liability_premi_calculation.discount_percentage : '0'}
                            %
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={6} md={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                          {Helper.currency(Number(!isEmpty(data.liability_premi_calculation) ? data.liability_premi_calculation.discount_currency : '0'), 'Rp. ', ',-')}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Biaya Administrasi</b></p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item style={{ marginBottom: '0' }}>
                      <Checkbox checked={data.liability_premi_calculation.print_policy_book} className="cursor-default" disabled>Buku Polis</Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2 text-dark fw-bold">{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10} />
                  <Col xs={24} md={8}>
                    <p>Materai</p>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="text-dark fw-bold">{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Total Pembayaran</b></p>
                  </Col>
                  <Col xs={24} md={6} />
                  <Col xs={24} md={8}>
                    <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                  </Col>
                </Row>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Form.Item>
                      <Checkbox checked className="cursor-default">
                        Saya telah membaca dan setuju dengan
                        <b>Syarat & Ketentuan Mitraca</b>
                        yang berlaku
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${data.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailLiability.propTypes = {
  detailData: PropTypes.object,
}

export default DetailLiability
