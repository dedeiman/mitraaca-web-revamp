import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _, { capitalize, isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'
import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'

const PreviewLiability = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  const parsed = queryString.parse(window.location.search)

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      wrapClassName="wrap-modal-preview"
      className="modal-preview"
      style={{ width: '80% !important' }}
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Card className="h-100">
            <p className="title-card">IO Liability</p>
            <Row gutter={24}>
              <Col xs={24} md={24}>
                <p className="mb-1"><b>Nomor Penawaran</b></p>
                <p className="mb-1"><i>Auto Generate</i></p>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} md={24}>
                <p className="mb-1"><b>Nama</b></p>
                <p className="mb-1">{_.get(data, 'name', '-')}</p>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} md={24}>
                <p className="mb-1"><b>Email</b></p>
                <p className="mb-1">{_.get(data, 'email', '-')}</p>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} md={24}>
                <p className="mb-1"><b>Phone</b></p>
                <p className="mb-1">{_.get(data, 'phone_number', '-')}</p>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} md={24}>
                <p className="mb-0"><b>Product</b></p>
                <p>
                  {data.match.params.id
                    ? <p>{`${!isEmpty(data.data) ? data.data.product.type.name : '-'} - Liability - ${!isEmpty(data.data) ? data.data.product.display_name : '-'}`}</p>
                    : <p>{`${capitalize(parsed.type)} - Liability - ${handleProduct(parsed.product)}`}</p>
                  }
                </p>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={12} md={12}>
                <p className="mb-1"><b>Periode Pertanggungan</b></p>
                <p className="mb-1">1 Tahun</p>
              </Col>
            </Row>
          </Card>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Perhitungan Premi / Kontribusi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nilai Pertanggungan Perlengkapan</b></p>
                    <p>{Helper.currency(data.amountCoverage, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Premi</b></p>
                    <p>{Helper.currency(data.amountPremi, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={6} md={6}>
                    <p className="mb-1"><b>Diskon</b></p>
                    <p>
                      {_.get(data, 'discount_percentage', 0)}
                      %
                    </p>
                  </Col>
                  <Col xs={6} md={6}>
                    <p className="mb-1">&nbsp;</p>
                    <p>{Helper.currency(_.get(data, 'discount_currency', 0), 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Biaya Administrasi</b></p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item style={{ marginBottom: '0' }}>
                      <Checkbox checked={data.print_policy_book} className="cursor-default">Buku Polis</Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2">{Helper.currency(50000, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10} />
                  <Col xs={24} md={8}>
                    <p>Materai</p>
                  </Col>
                  <Col xs={24} md={6}>
                    <p>{Helper.currency(6000, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Total Pembayaran</b></p>
                  </Col>
                  <Col xs={24} md={5} />
                  <Col xs={24} md={9}>
                    <p className="mb-1 mt-2" style={{ fontSize: '18px' }}><b>{Helper.currency(data.paymentTotal, 'Rp. ', ',-')}</b></p>
                  </Col>
                </Row>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Form.Item>
                      <Checkbox checked={data.aggree === true} className="cursor-default">
                        Saya telah membaca dan setuju dengan
                        <b>Syarat & Ketentuan Mitraca</b>
                        yang berlaku
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 border-lg"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewLiability.propTypes = {
  data: PropTypes.any,
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
}

export default PreviewLiability
