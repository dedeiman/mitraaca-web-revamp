import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import _, { isEmpty, capitalize } from 'lodash'
import Helper from 'utils/Helper'
import queryString from 'query-string'

import { Form } from '@ant-design/compatible'
import { handleProduct } from 'constants/ActionTypes'

const PreviewWellwoman = ({
    data,
    show,
    handleCancel,
    handleSubmit,
    submitted,
}) => {
    const parsed = queryString.parse(window.location.search)

    return (
        <Modal
            title="Preview"
            visible={show}
            onCancel={handleCancel}
            className="modal-add-customer"
            style={{ width: '80% !important' }}
            footer={null}
        >
            <Row gutter={24}>
                <Col xs={24} md={12}>
                    <Row gutter={[24, 24]}>
                        <Col span={24}>
                            <Card className="h-100">
                                <p className="title-card">Informasi Polis</p>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Indicative Offer</b></p>
                                        <p className="mb-1"><i>Auto Generate</i></p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Insured Name</b></p>
                                        <p className="mb-1">{_.get(data, 'insured_name', '-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Phone Number</b></p>
                                        <p className="mb-1">{_.get(data, 'phone_number', '-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Email</b></p>
                                        <p className="mb-1">{_.get(data, 'email', '-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Product</b></p>
                                        <b>{`${capitalize(parsed.type)} - Healthy - ${handleProduct(parsed.product)}`}</b>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={12} md={12}>
                                        <p className="mb-1"><b>Periode Pertanggungan</b></p>
                                        <p className="mb-1">
                                            1 Tahun
                                        </p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={12} md={12}>
                                        <p className="mb-1"><b>Tanggal Lahir Tertanggung</b></p>
                                        <p className="mb-1">{ data.insured_dob ? data.insured_dob.format('YYYY-MM-DD') : '-' }</p>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
                <Col xs={24} md={12}>
                    <Row gutter={[24, 24]}>
                        <Col span={24}>
                            <Card className="h-100">
                                <p className="title-card">Perhitungan Premi</p>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <p className="mb-1"><b>Premi</b></p>
                                        <p>{Helper.currency(data.premi.premi, 'Rp. ', ',-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={6} md={6}>
                                        <p className="mb-1"><b>Diskon</b></p>
                                        <p className="mb-1">{_.get(data, 'discount_percentage')}%</p>
                                    </Col>
                                    <Col xs={6} md={6}>
                                        <p className="mb-1">&nbsp;</p>
                                        <p>Rp. {_.get(data, 'discount_currency')},-</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={8}>
                                        <p className="mt-2"><b>Biaya Administrasi</b></p>
                                    </Col>
                                    <Col xs={24} md={8}>
                                        <Form.Item className="mb-0">
                                            <Checkbox checked={data.print_policy_book === true}>
                                                Buku Polis
                                            </Checkbox>
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} md={8}>
                                        <p className="mt-2">Rp 50.000,-</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={8} />
                                    <Col xs={24} md={8}>
                                        <p className="mb-1">Materai</p>
                                    </Col>
                                    <Col xs={24} md={8}>
                                        <p className="mb-1">{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={16}>
                                        <p className="mb-1">Total Pembayaran</p>
                                    </Col>
                                    <Col xs={24} md={8}>
                                        <p className="mb-1">{Helper.currency(data.paymentTotal + (data.print_policy_book ? 50000 : 0), 'Rp. ', ',-')}</p>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col xs={24} md={24}>
                                        <Form.Item>
                                        <Checkbox checked={!isEmpty(data.aggree) ? _.get(data, 'aggree', '-')[0] : false}>
                                            Saya telah membaca dan setuju dengan Syarat & Ketentuan Mitraca yang berlaku
                                        </Checkbox>
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row className="mt-3 mb-3">
                                    <Col span={24} className="d-flex justify-content-end align-items-center">
                                        <Button
                                            ghost
                                            className="button-lg w-25 mr-3"
                                            isBorderDark
                                            onClick={() => handleCancel()}
                                            disabled={false}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            className="button-lg w-25 border-lg"
                                            type="primary"
                                            onClick={() => handleSubmit()}
                                            disabled={submitted}
                                        >
                                            {!submitted ? 'Save' : 'Loading ...'}
                                        </Button>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Modal>
    )
}

PreviewWellwoman.propTypes = {
    show: PropTypes.any,
    handleCancel: PropTypes.func,
    handleSubmit: PropTypes.any,
    submitted: PropTypes.any,
    data: PropTypes.object,
}

export default PreviewWellwoman
