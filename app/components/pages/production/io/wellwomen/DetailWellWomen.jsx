import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Button,
  Checkbox, Tag,
  Descriptions,
} from 'antd'
import { Form } from '@ant-design/compatible'
import _, { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import moment from 'moment'
import history from 'utils/history'
import queryString from 'query-string'
import { handleProduct } from 'constants/ActionTypes'
import { LeftOutlined } from '@ant-design/icons'

const DetailWellwoman = ({
  detailData,
}) => {
  const data = detailData.list
  const parsed = queryString.parse(window.location.search)

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail IO ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">{`IO ${_.get(data, 'product.display_name')}`}</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Indicative Offer" className="px-1 profile-detail pb-0">
                          {data.io_number || ''}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Insured Name" className="px-1 profile-detail pb-0 uppercase">
                          {data.name || ''}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Phone Number" className="px-1 profile-detail pb-0">
                          {data.phone_country_code.phone_code} {data.phone_number || ''}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Email" className="px-1 profile-detail pb-0 uppercase">
                          {data.email || ''}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Periode Pertanggungan" className="px-1 profile-detail pb-0">
                          1 Tahun
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Tanggal Lahir Tertanggung" className="px-1 profile-detail pb-0">
                          {moment(data.insured_dob).format('DD MMMM YYYY') || ''}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                        <Descriptions.Item label="Product" className="px-1 profile-detail pb-0">
                          {`${data.product.type.name} - Health - ${handleProduct(parsed.product)}`}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Perhitungan Premi</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Premi" className="px-1 profile-detail pb-0">
                        {Helper.currency(data.wellwoman_premi_calculation.premi, 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={6} md={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                        {!isEmpty(data.wellwoman_premi_calculation) ? data.wellwoman_premi_calculation.discount_percentage : '0'}
                        %
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={6} md={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                        {Helper.currency(Number(!isEmpty(data.wellwoman_premi_calculation) ? data.wellwoman_premi_calculation.discount_currency : '0'), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Biaya Administrasi</b></p>
                  </Col>
                  <Col xs={24} md={8}>
                    <Form.Item style={{ marginBottom: '0' }}>
                      <Checkbox checked={data.print_policy_book} disabled>Buku Polis</Checkbox>
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="mb-1 mt-2 text-dark fw-bold">{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={10} />
                  <Col xs={24} md={8}>
                    <p>Materai</p>
                  </Col>
                  <Col xs={24} md={6}>
                    <p className="text-dark fw-bold">{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={10}>
                    <p className="mb-1 mt-2"><b>Total Pembayaran</b></p>
                  </Col>
                  <Col xs={24} md={5} />
                  <Col xs={24} md={9}>
                    <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                  </Col>
                </Row>
                <br />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Form.Item>
                      <Checkbox checked>
                        Saya telah membaca dan setuju dengan
                        <b>Syarat & Ketentuan Mitraca</b>
                        yang berlaku
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
              <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${data.id}`)}>
                Kirim Penawaran
              </Button>
              <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailWellwoman.propTypes = {
  detailData: PropTypes.object,
}

export default DetailWellwoman
