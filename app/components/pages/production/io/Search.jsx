/* eslint-disable consistent-return */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Descriptions,
  Button,
  Pagination, Empty,
  Divider, Select,
} from 'antd'
import Helper from 'utils/Helper'
import moment from 'moment'
import { Loader } from 'components/elements'
import { Form } from '@ant-design/compatible'
import history from 'utils/history'
import { isEmpty } from 'lodash'

export default function List({
  stateIO, handlePage,
  stateFilter, setStateFilter,
  handleFilter, loadIO,
}) {
  const isMobile = window.innerWidth < 768
  return (
    <>
      <Row gutter={[24, 24]}>
        <Col xs={24} md={21} xl={21}>
          <Input
            allowClear
            placeholder="Search IO / Policy Number..."
            className="field-lg"
            value={stateFilter.search}
            onChange={(e) => {
              setStateFilter({
                ...stateFilter,
                search: e.target.value,
              })
            }}
            onPressEnter={() => loadIO()}
          />
        </Col>
        <Col xs={24} md={3} xl={3}>
          <Button type="primary" className="button-lg w-100" onClick={() => loadIO()}>
            Search
          </Button>
        </Col>
        <Col span={21}>
          <Form.Item label="Status IO">
            <Select
              onChange={handleFilter}
              allowClear
              value={stateFilter.filter || undefined}
              style={{ width: '350px' }}
              loading={stateFilter.loading}
              placeholder="Select Status"
              className="field-lg"
            >
              {(stateFilter.list || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>

      <Card>
        {stateIO.loading && (
          <Loader />
        )}
        {(!stateIO.loading && isEmpty(stateIO.list)) && (
          <Empty description="Tidak ada data." />
        )}
        {!isEmpty(stateIO.list)
          ? (
            stateIO.list.map((item, idx) => (
              <Row gutter={24}>
                <Col xs={24} md={4} xl={4}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="IO Number / Policy Number" className="px-1">
                      {item.indicative_offer && (
                        <React.Fragment>
                          <p className="mb-0">{`Quotation No: ${item.indicative_offer || '-'}`}</p>
                          <p>
                            {`(${
                              item.created_at ? moment(item.created_at).utc().format('DD MMM YYYY') : '-'
                            } - ${
                              item.created_at ? moment(item.created_at).utc().format('HH:mm') : '-'
                            })`}
                          </p>
                        </React.Fragment>
                      )}
                      <p className="mb-0">{`IO No: ${item.io_number || '-'}`}</p>
                      {item.created_at ? (
                        <p>
                          {`(${
                            moment(item.created_at).utc().format('DD MMM YYYY') || '-'
                          } - ${
                            moment(item.created_at).utc().format('HH:mm') || '-'
                          })`}
                        </p>
                      ) : '-'}
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={4}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Agent" className="px-1">
                      <p className="mb-0">
                        {item.creator ? Helper.getValue(item.creator.name) : '-'}
                      </p>
                      <p>
                        {`(${
                          item.creator ? Helper.getValue(item.creator.agent_id) : '-'
                        })`}
                      </p>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={3}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Valid Until" className="px-1">
                      <p className="mb-0">
                        {`${item.expired_at ? moment(item.expired_at).format('D MMM YYYY') : '-'}`}
                      </p>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={7}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Insured" className="px-1">
                      <p>
                        {`${
                          item.product.type ? item.product.type.name : '-'
                        } - ${
                          item.product.class_of_business ? item.product.class_of_business.name : '-'
                        } - ${
                          item.product.display_name || '-'
                        }`}
                      </p>
                      {/* <p>{`Policy Holder: ${item.policy_holder.name || '-'}`}</p> */}
                      {/* <p>{`Insured: ${item.insured.name || '-'}`}</p> */}
                      <p className="mb-0">Insured Period:</p>
                      {(() => {
                        if ((item.period_in_year === 0) && (item.period_in_days === 0)) {
                          return (
                            <p>
                              {`(${item.created_at ? moment(item.created_at).format('D MMM YYYY') : '-'}) (-)`}
                            </p>
                          )
                        }
                        if ((item.period_in_year === 0) && (item.period_in_days !== 0)) {
                          return (
                            <p>
                              {`(${item.created_at ? `${moment(item.created_at).format('D MMM YYYY')} - ${moment(item.created_at).add(item.period_in_days, 'day').format('D MMM YYYY')}` : '-'}) (${item.period_in_days} Days)`}
                            </p>
                          )
                        }
                        if ((item.period_in_year !== 0) && (item.period_in_days === 0)) {
                          return (
                            <p>
                              {`(${item.created_at ? `${moment(item.created_at).format('D MMM YYYY')} - ${moment(item.created_at).add(item.period_in_year, 'year').format('D MMM YYYY')}` : '-'}) (${item.period_in_year} Year)`}
                            </p>
                          )
                        }
                      })()}
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={3}>
                  <Descriptions layout="vertical" colon={false} column={1}>
                    <Descriptions.Item label="Status" className="px-1">
                      <span className="tag" style={{ textTransform: 'capitalize'}}>
                        {item.status}
                      </span>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={4} xl={3}>
                  {(item.status != 'expired') && (
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Action" className="px-1">
                        <Button
                          type="primary"
                          htmlType="button"
                          onClick={() => {
                            history.push(`/production-create-io/detail/${item.id}?product=${item.product.code}`)
                          }}
                        >
                          Detail IO
                        </Button>
                      </Descriptions.Item>
                    </Descriptions>
                  )}
                  {(item.status == 'expired') && (
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Action" className="px-1">
                        <Button
                          type="primary"
                          htmlType="button"
                          disabled
                        >
                          Detail IO
                        </Button>
                      </Descriptions.Item>
                    </Descriptions>
                  )}
                </Col>
                
                {(idx !== (stateIO.list.length - 1)) && (
                  <Divider />
                )}
              </Row>
            ))
        )
        : <div className="py-5" />
        }
      </Card>
      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={stateIO.page ? Number(stateIO.page.current_page) : 1}
        onChange={handlePage}
        total={stateIO.page.total_count}
        pageSize={stateIO.page.per_page || 10}
      />
    </>
  )
}

List.propTypes = {
  stateIO: PropTypes.object,
  handlePage: PropTypes.func,
  stateFilter: PropTypes.object,
  setStateFilter: PropTypes.func,
  handleFilter: PropTypes.func,
  loadIO: PropTypes.func,
}