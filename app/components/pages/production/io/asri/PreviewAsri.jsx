import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Col,
  Row,
  Card,
  Checkbox,
  Button,
} from 'antd'
import {
  AsriSyariahCode,
} from 'constants/ActionTypes'
import _ from 'lodash'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'

const PreviewIOCargo = ({
  data,
  show,
  handleCancel,
  handleSubmit,
  submitted,
}) => {
  let provinceText = '-'
  const provinceList = data.stateSelects ? data.stateSelects.provinceList : []

  if (data.province_id && data.stateSelects.provinceList.length) {
    const province = provinceList.find(o => o.id === data.province_id)

    if (province) {
      provinceText = province.name
    }
  }

  let cityText = '-'
  const cityList = data.stateSelects ? data.stateSelects.cityList : []

  if (data.city_id && data.stateSelects.cityList.length) {
    const city = cityList.find(o => o.id === data.city_id)

    if (city) {
      cityText = city.name
    }
  }

  let subDistrictText = '-'
  const subDistrictList = data.stateSelects ? data.stateSelects.subDistrictList : []

  if (data.sub_district_id && data.stateSelects.subDistrictList.length) {
    const subDistrict = subDistrictList.find(o => o.id === data.sub_district_id)

    if (subDistrict) {
      subDistrictText = subDistrict.name
    }
  }

  let villageText = '-'
  const villageList = data.stateSelects ? data.stateSelects.villageList : []

  if (data.village_id && data.stateSelects.villageList.length) {
    const village = villageList.find(o => o.id === data.village_id)

    if (village) {
      villageText = village.name
    }
  }

  return (
    <Modal
      title="Preview"
      visible={show}
      onCancel={handleCancel}
      wrapClassName="wrap-modal-preview"
      className="modal-add-customer"
      footer={null}
    >
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">IO Asri</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Nomor Penawaran</b></p>
                    <p className="mb-1"><i>Auto Generate</i></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>{data.productDetail.code === AsriSyariahCode ? 'Nama Peserta' : 'Tertanggung'}</b></p>
                    <p className="mb-1">{_.get(data, 'name', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Email</b></p>
                    <p className="mb-1">{_.get(data, 'email', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>No Handphone</b></p>
                    <p className="mb-1">{_.get(data, 'phone_number', '-')}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-0"><b>Produk</b></p>
                    <p>
                      {_.get(data, 'productDetail.type.name')}
                      &nbsp;-&nbsp;
                      {_.get(data, 'productDetail.class_of_business.name')}
                      &nbsp;-&nbsp;
                      {_.get(data, 'productDetail.display_name')}
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-0"><b>Periode Pertanggungan</b></p>
                    <p>1 Tahun</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Object Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <p className="mb-1"><b>Alamat Pertanggungan</b></p>
                    <p className="mb-1">{data.address}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Provinsi</b></p>
                    <p className="mb-1">{provinceText}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kota</b></p>
                    <p className="mb-1">{cityText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kecamatan</b></p>
                    <p className="mb-1">{subDistrictText}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kelurahan</b></p>
                    <p className="mb-1">{villageText}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kode POS</b></p>
                    <p className="mb-1">{data.postal_code}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Latitude</b></p>
                    <p className="mb-1">{data.lat}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Longitude</b></p>
                    <p className="mb-1">{data.lng}</p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Jumlah Lantai</b></p>
                    <p className="mb-1">{`${data.total_floor} Lantai`}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Kelas Kontruksi</b></p>
                    <p className="mb-1">{`Kelas ${data.class_construction}`}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Area Gempa</b></p>
                    <p className="mb-1">{data.earth_quake_area}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Area Banjir</b></p>
                    <p className="mb-1">{data.float_area}</p>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Nilai Objek Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Luas Bangunan</b></p>
                    <p className="mb-1">{data.building_area}</p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Nilai Bangunan</b></p>
                    <p className="mb-1">{Helper.currency(data.building_price, 'Rp. ', ',-')}</p>
                  </Col>
                  <Col xs={12} md={12}>
                    <p className="mb-1"><b>Nilai Perabot</b></p>
                    <p className="mb-1">{Helper.currency(data.furniture_price, 'Rp. ', ',-')}</p>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Total</b></p>
                    <p><b>{Helper.currency(data.coverage, 'Rp. ', ',-')}</b></p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>Perluasan Jaminan</b></p>
                    <Form.Item>
                      <Checkbox checked={(data.additional_limits || []).includes('tsfwd')}>Banjir</Checkbox>
                      <Checkbox checked={(data.additional_limits || []).includes('eqvet')}>Gempa</Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">{data.productDetail.code === AsriSyariahCode ? 'Perhitungan Kontribusi' : 'Perhitungan Premi / Perhitungan Kontribusi'}</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">Rate</p>
                        <p><b>{data.premi.rate} %</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <p className="mb-1">{data.productDetail.code === AsriSyariahCode ? 'Kontribusi' : 'Premi'}</p>
                        <p><b>{Helper.currency(data.premi.value, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={6} md={6}>
                        <p className="mb-1">Diskon</p>
                        <p>{`${data.discount_percentage || 0}%`}</p>
                      </Col>
                      <Col xs={6} md={6}>
                        <p className="mb-1">&nbsp;</p>
                        <p>{Helper.currency(data.discount_currency || 0, 'Rp. ', ',-')}</p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true}>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.bookPrice, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2"><b>{Helper.currency(data.premi.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2" style={{ fontSize: '18px' }}><b>{Helper.currency(data.paymentTotal, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <br />
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Form.Item>
                          <Checkbox checked={data.aggree === true}>
                            Saya telah membaca dan setuju dengan
                            &nbsp;
                            <b>Syarat & Ketentuan Mitraca</b>
                            &nbsp;
                            yang berlaku
                          </Checkbox>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            ghost
            className="button-lg w-25 border-lg"
            isBorderDark
            onClick={() => handleCancel()}
            disabled={false}
          >
            Cancel
          </Button>
          <Button
            className="button-lg w-25 border-lg"
            type="primary"
            onClick={() => handleSubmit()}
            disabled={submitted}
          >
            {!submitted ? 'Save' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

PreviewIOCargo.propTypes = {
  data: PropTypes.any,
  show: PropTypes.any,
  handleCancel: PropTypes.object,
  handleSubmit: PropTypes.any,
  submitted: PropTypes.any,
}

export default PreviewIOCargo
