import React from 'react'
import PropTypes from 'prop-types'
import {
  Row,
  Col,
  Card,
  Checkbox,
  Button, Tag,
  Descriptions,
} from 'antd'
import history from 'utils/history'
import { Form } from '@ant-design/compatible'
import _ from 'lodash'
import Helper from 'utils/Helper'
import { LeftOutlined } from '@ant-design/icons'

const DetailOtomate = ({
  detailData,
}) => {
  const data = detailData.list
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/production-search-io-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Detail IO ${_.get(data, 'product.display_name')}`}</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Informasi Polis</p>
                <Row gutter={24}>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nomor Penawaran" className="px-1 profile-detail pb-0">
                        {_.get(data, 'io_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nama" className="px-1 profile-detail pb-0">
                        {_.get(data, 'name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Email" className="px-1 profile-detail pb-0">
                        {_.get(data, 'email', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="No Handphone" className="px-1 profile-detail pb-0">
                        {_.get(data, 'phone_country_code.phone_code')} {_.get(data, 'phone_number', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Produk" className="px-1 profile-detail pb-0">
                        {_.get(data, 'product.type.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.class_of_business.name')}
                        &nbsp;-&nbsp;
                        {_.get(data, 'product.display_name')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={12} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Periode Pertanggungan" className="px-1 profile-detail pb-0">
                        1 Tahun
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Object Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Alamat" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.address', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {/* <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>RT</b></p>
                    <p>{_.get(data, 'asri_building_address.rt', '-')}</p>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-1"><b>RW</b></p>
                    <p>{_.get(data, 'asri_building_address.rw', '-')}</p>
                  </Col>
                </Row> */}
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Provinsi" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.province.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kota" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.city.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kecamatan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.sub_district.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kelurahan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.village.name', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kode POS" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.postal_code', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Latitude" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.lat', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Longitude" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.lng', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Area Gempa" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.city.earthquake_area', false) === true ? 'YA' : 'TIDAK'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Area Banjir" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_address.city.flood_area', false) === true ? 'YA' : 'TIDAK'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Jumlah Lantai" className="px-1 profile-detail pb-0">
                        {`${_.get(data, 'asri_building_address.total_floor', '-')} Lantai`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Kelas Kontruksi" className="px-1 profile-detail pb-0">
                        {`Kelas ${_.get(data, 'asri_building_information.class_construction', '-')}`}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100">
                <p className="title-card">Nilai Objek Pertanggungan</p>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Luas Bangunan" className="px-1 profile-detail pb-0">
                        {_.get(data, 'asri_building_information.building_area', '-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nilai Bangunan" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_premi_calculation.building_price', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={12}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Nilai Perabot" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_premi_calculation.furniture_price', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <hr />
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Total" className="px-1 profile-detail pb-0">
                        {Helper.currency(_.get(data, 'asri_premi_calculation.building_price', 0) + _.get(data, 'asri_premi_calculation.furniture_price', 0), 'Rp. ', ',-')}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xs={24} md={24}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Perluasan Jaminan" className="px-1 profile-detail pb-0">
                        <Form.Item>
                          <Checkbox checked={(data.asri_premi_calculation.additional_limits || []).includes('tsfwd')}>Banjir</Checkbox>
                          <Checkbox checked={(data.asri_premi_calculation.additional_limits || []).includes('eqvet')}>Gempa</Checkbox>
                        </Form.Item>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Card className="h-100">
                    <p className="title-card">Perhitungan Premi / Kontribusi</p>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Rate" className="px-1 profile-detail pb-0">
                            {_.get(data, 'asri_premi_calculation.rate').toFixed(2)}%
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={24}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Premi" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'asri_premi_calculation.premi'), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={6} md={6}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="Diskon" className="px-1 profile-detail pb-0">
                            {`${_.get(data, 'asri_premi_calculation.discount_percentage', 0)}%`}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                      <Col xs={6} md={6}>
                        <Descriptions layout="vertical" colon={false} column={1}>
                          <Descriptions.Item label="&nbsp;" className="px-1 profile-detail pb-0">
                            {Helper.currency(_.get(data, 'asri_premi_calculation.discount_currency', 0), 'Rp. ', ',-')}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Biaya Administrasi</p>
                      </Col>
                      <Col xs={24} md={8}>
                        <Form.Item style={{ marginBottom: '0' }}>
                          <Checkbox checked={data.print_policy_book === true} disabled>Buku Polis</Checkbox>
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.policy_printing_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Materai</p>
                      </Col>
                      <Col xs={24} md={8} />
                      <Col xs={24} md={6}>
                        <p className="mb-1 mt-2 text-dark fw-bold"><b>{Helper.currency(data.stamp_fee, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                    <hr />
                    <Row gutter={24}>
                      <Col xs={24} md={10}>
                        <p className="mb-1 mt-2">Total Pembayaran</p>
                      </Col>
                      <Col xs={24} md={6} />
                      <Col xs={24} md={8}>
                        <p className="mb-1 mt-2 text-dark fw-bold" style={{ fontSize: '18px' }}><b>{Helper.currency(data.total_payment, 'Rp. ', ',-')}</b></p>
                      </Col>
                    </Row>
                  </Card>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/confirm-offer/${data.id}`)}>
                    Kirim Penawaran
                  </Button>
                  <Button type="primary" onClick={() => history.push(`/production-create-io/edit/${data.id}?product=${data.product.code}&product_id=${data.product.id}`)} className="mt-3 ml-2 mb-5 pl-5 pr-5">Edit</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailOtomate.propTypes = {
  detailData: PropTypes.object,
}

export default DetailOtomate
