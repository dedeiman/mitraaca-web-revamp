import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions, Tooltip,
  Pagination, DatePicker,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const PolicyList = ({
  handleFilter, loadPolicy,
  stateFilter, currentUser,
  handlePage, statePolicy,
  updatePerPage,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      {(currentUser.permissions && currentUser.permissions.indexOf('policy-search-filter') > -1) && (
      <Row gutter={[24, 24]}>
        <Col sm={24} md={17} lg={19} xl={21} className={`${isMobile ? 'mt-3' : ''}`}>
          <Input
            allowClear
            placeholder="Search SPPA / Policy Number..."
            className="field-lg"
            value={stateFilter.search}
            onChange={e => handleFilter('search', e.target.value)}
            onPressEnter={() => loadPolicy(true)}
          />
        </Col>
        <Col sm={8} md={3} lg={3} align="end">
          <Button type="primary" className="button-lg px-4" onClick={() => loadPolicy(true)}>
            Search
          </Button>
        </Col>
        <Col sm={24} md={24} lg={10}>
          <Form.Item label="Status SPPA" className="mb-0">
            <Select
              onChange={e => handleFilter('status', e)}
              allowClear
              value={stateFilter.status || undefined}
              loading={stateFilter.load}
              placeholder="Select Status"
              className="field-lg w-100"
            >
              {(stateFilter.list || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col sm={24} md={24} lg={10}>
          <Form.Item label="SPPA Date">
            <DatePicker
              placeholder="End Date"
              loading={stateFilter.load}
              className="field-lg w-100"
              style={{ width: '350px' }}
              format="DD MMMM YYYY"
              value={!isEmpty(stateFilter.date) ? moment(stateFilter.date) : undefined}
              onChange={e => handleFilter('date', !isEmpty(e) ? e.format('YYYY-MM-DD') : undefined)}
            />
          </Form.Item>
        </Col>
      </Row>
      )}

      <Card>
        {statePolicy.load && (
          <Loader />
        )}
        {(!statePolicy.load && isEmpty(statePolicy.list)) && (
          <Empty description="Tidak ada data." />
        )}
        {!isEmpty(statePolicy.list)
          ? (
            statePolicy.list.map((item, idx) => (
              <>
                <Row gutter={24} key={item.id}>
                  <Col xs={24} md={6} xl={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA Number / Policy Number" className="px-1">
                        {item.indicative_offer && (
                          <React.Fragment>
                            <p className="mb-0">{`Quotation No: ${item.indicative_offer || '-'}`}</p>
                            <p>
                              {`(${
                                item.created_at ? moment(item.created_at).format('D MMM YYYY') : '-'
                              } - ${
                                item.created_at ? moment(item.created_at).format('hh.mm a') : '-'
                              })`}
                            </p>
                          </React.Fragment>
                        )}
                        <p className="mb-0">{`SPPA No: ${item.sppa_number || '-'}`}</p>
                        {item.created_at ? (
                          <p>
                            {`(${
                              moment(item.created_at).format('D MMM YYYY') || '-'
                            } - ${
                              moment(item.created_at).format('hh.mm a') || '-'
                            })`}
                          </p>
                        ) : '-'}
                        <p className="mb-0">{`Policy No: ${item.policy_number || '-'}`}</p>
                        {item.policied_at ? (
                          <p>
                            {`(${
                              moment(item.policied_at).format('D MMM YYYY') || '-'
                            } - ${
                              moment(item.policied_at).format('hh.mm a') || '-'
                            })`}
                          </p>
                        ) : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 4 : 1} label="Agent" className="px-1">
                        <p className="mb-0">
                          {item.creator ? Helper.getValue(item.creator.name) : '-'}
                        </p>
                        <p>
                          {`(${
                            item.creator ? Helper.getValue(item.creator.agent_id) : '-'
                          } - ${
                            item.creator ? Helper.getValue(item.creator.birth_place) : '-'
                          })`}
                        </p>
                        <p className="mb-0">Production in:</p>
                        <p>
                          {`${
                            item.creator.branch ? Helper.getValue(item.creator.branch.name) : '-'
                          } - ${
                            item.creator.branch_perwakilan ? Helper.getValue(item.creator.branch_perwakilan.name) : '-'
                          }`}
                        </p>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 4 : 1} label="Insured" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-1">
                          <Tooltip
                            title={`${
                              item.product.type ? item.product.type.name : '-'
                            } - ${
                              item.product.class_of_business ? item.product.class_of_business.name : '-'
                            } - ${
                              item.product.display_name || '-'
                            }`}
                          >
                            {`${
                              item.product.type ? item.product.type.name : '-'
                            } - ${
                              item.product.class_of_business ? item.product.class_of_business.name : '-'
                            } - ${
                              item.product.display_name || '-'
                            }`}
                          </Tooltip>
                        </Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-1">
                          <Tooltip
                            title={item.policy_holder.name}
                          >
                            {`Policy Holder: ${item.policy_holder.name || '-'}`}
                          </Tooltip>
                        </Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-1">
                          <Tooltip
                            title={item.insured.name}
                          >
                            {`Policy Holder: ${item.insured.name || '-'}`}
                          </Tooltip>
                        </Typography.Paragraph>
                        <Typography.Paragraph className="mb-0">
                          Insured Period:
                        </Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-1">
                          <Tooltip
                            title={`(${item.start_period ? moment(item.start_period).format('D MMM YYYY') : '-'}) - (${item.end_period ? moment(item.end_period).format('D MMM YYYY') : '-'})`}
                          >
                            {'('}
                            {item.start_period ? moment(item.start_period).format('D MMM YYYY') : '-'}
                            {' - '}
                            {item.end_period ? moment(item.end_period).format('D MMM YYYY') : '-'}
                            {')'}
                          </Tooltip>
                        </Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-1 text-capitalize">
                          <Tooltip
                            title={item.status ? item.status.replace('-', ' ') : ''}
                          >
                            {`Status: ${item.status ? item.status.replace('-', ' ') : '-'}`}
                          </Tooltip>
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA / Policy Info" className="px-1">
                        {(item.sppa_infos || []).map(info => (
                          <p className="d-flex align-items-center mb-2" key={Math.random()}>
                            {info.is_check
                              ? <img src="/assets/ic-check.png" alt="check" className="mr-2" style={{ width: '20px', height: '20px' }} />
                              : <img src="/assets/ic-times.svg" alt="times" className="mr-2" />
                            }
                            {info.display_name || '-'}
                          </p>
                        ))}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 4 : 1} className="px-1">
                        <Button
                          type="primary"
                          className="d-flex justify-content-center align-items-center w-100"
                          onClick={() => history.push(`/policy-search-menu/${item.id}`)}
                        >
                          Detail
                          {' '}
                        </Button>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {(idx !== (statePolicy.list.length - 1)) && (
                  <Divider />
                )}
              </>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={stateFilter.page ? Number(stateFilter.page) : 1}
        onChange={handlePage}
        total={statePolicy.page.total_count}
        pageSize={statePolicy.page.per_page || 10}
        onShowSizeChange={updatePerPage}
        showSizeChanger={false}
      />
    </React.Fragment>
  )
}

PolicyList.propTypes = {
  currentUser: PropTypes.object,
  handlePage: PropTypes.func,
  handleFilter: PropTypes.func,
  loadPolicy: PropTypes.func,
  statePolicy: PropTypes.object,
  stateFilter: PropTypes.object,
  updatePerPage: PropTypes.func,
}

export default PolicyList
