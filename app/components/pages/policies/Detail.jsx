import React from 'react'
import PropTypes from 'prop-types'
import {
  Tag, Button, Empty,
  Row, Col, Card, Modal,
  Descriptions,
  Checkbox, Avatar,
} from 'antd'
import { Loader } from 'components/elements'
import { Form } from '@ant-design/compatible'
import { LeftOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const PolicyDetail = ({
  form, statePolicy, handleDelete,
  stateModal, setStateModal, handleDownload,
}) => {
  const { getFieldDecorator } = form
  const isMobile = window.innerWidth <= 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
      </Row>

      <Card className="mb-4">
        {statePolicy.load && (
          <Loader />
        )}
        {(!statePolicy.load && isEmpty(statePolicy.detail)) && (
          <Empty description="Tidak ada data." />
        )}
        {!isEmpty(statePolicy.detail)
          ? (
            <Descriptions layout="vertical" colon={false} column={4}>
              <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA Number / Policy Number" className="px-1">
                {statePolicy.detail.indicative_offer && (
                  <React.Fragment>
                    <p className="mb-0">{`Quotation No: ${statePolicy.detail.indicative_offer || '-'}`}</p>
                    <p>
                      {`(${
                        statePolicy.detail.created_at ? moment(statePolicy.detail.created_at).format('D MMM YYYY') : '-'
                      } - ${
                        statePolicy.detail.created_at ? moment(statePolicy.detail.created_at).format('hh.mm a') : '-'
                      })`}
                    </p>
                  </React.Fragment>
                )}
                <p className="mb-0">{`SPPA No: ${statePolicy.detail.sppa_number || '-'}`}</p>
                {statePolicy.detail.created_at ? (
                  <p>
                    {`(${
                      moment(statePolicy.detail.created_at).format('D MMM YYYY') || '-'
                    } - ${
                      moment(statePolicy.detail.created_at).format('hh.mm a') || '-'
                    })`}
                  </p>
                ) : '-'}
                <p className="mb-0">{`Policy No: ${statePolicy.detail.policy_number || '-'}`}</p>
                {statePolicy.detail.policied_at ? (
                  <p>
                    {`(${
                      moment(statePolicy.detail.policied_at).format('D MMM YYYY') || '-'
                    } - ${
                      moment(statePolicy.detail.policied_at).format('hh.mm a') || '-'
                    })`}
                  </p>
                ) : '-'}
              </Descriptions.Item>
              <Descriptions.Item span={isMobile ? 4 : 1} label="Agent" className="px-1">
                <p className="mb-0">
                  {statePolicy.detail.creator ? Helper.getValue(statePolicy.detail.creator.name) : '-'}
                </p>
                <p>
                  {`(${
                    statePolicy.detail.creator ? Helper.getValue(statePolicy.detail.creator.agent_id) : '-'
                  } - ${
                    statePolicy.detail.creator ? Helper.getValue(statePolicy.detail.creator.birth_place) : '-'
                  })`}
                </p>
                <p className="mb-0">Production in:</p>
                <p>
                  {`${
                    statePolicy.detail.creator.branch ? Helper.getValue(statePolicy.detail.creator.branch.name) : '-'
                  } - ${
                    statePolicy.detail.creator.branch_perwakilan ? Helper.getValue(statePolicy.detail.creator.branch_perwakilan.name) : '-'
                  }`}
                </p>
              </Descriptions.Item>
              <Descriptions.Item span={isMobile ? 4 : 1} label="Insured" className="px-1">
                <p>
                  {`${
                    statePolicy.detail.product.type ? statePolicy.detail.product.type.name : '-'
                  } - ${
                    statePolicy.detail.product.class_of_business ? statePolicy.detail.product.class_of_business.name : '-'
                  } - ${
                    statePolicy.detail.product.display_name || '-'
                  }`}
                </p>
                <p>{`Policy Holder: ${statePolicy.detail.policy_holder.name || '-'}`}</p>
                <p>{`Insured: ${statePolicy.detail.insured.name || '-'}`}</p>
                <p className="mb-0">Insured Period:</p>
                <p>
                  {'('}
                  {statePolicy.detail.start_period ? moment(statePolicy.detail.start_period).format('D MMM YYYY') : '-'}
                  {' - '}
                  {statePolicy.detail.end_period ? moment(statePolicy.detail.end_period).format('D MMM YYYY') : '-'}
                  {')'}
                </p>
              </Descriptions.Item>
              <Descriptions.Item span={isMobile ? 4 : 1} label="SPPA / Policy Info" className="px-1">
                {(statePolicy.detail.sppa_infos || []).map(info => (
                  <p className="d-flex align-items-center mb-2" key={Math.random()}>
                    {info.is_check
                      ? <img src="/assets/ic-check.png" alt="check" className="mr-2" style={{ width: '20px', height: '20px' }} />
                      : <img src="/assets/ic-times.svg" alt="times" className="mr-2" />
                    }
                    {info.display_name || '-'}
                  </p>
                ))}
              </Descriptions.Item>
            </Descriptions>
          )
          : <div className="py-5" />
        }
      </Card>

      <Form className="mb-4">
        <Row>
          <Col span={5}>
            <Button
              ghost
              type="primary"
              className="align-items-center btn-border-mitra mb-3"
              onClick={handleDelete}
            >
              Delete Document
            </Button>
          </Col>
          <Col span={5}>
            <Button
              ghost
              type="primary"
              className="align-items-center btn-border-mitra mb-3 ml-3"
              onClick={handleDownload}
            >
              Download Document
            </Button>
          </Col>
        </Row>
        <Card>
          <p className="title-card">Dokumen</p>
          {statePolicy.load && (
            <Loader />
          )}
          {(!statePolicy.load && isEmpty(statePolicy.list)) && (
            <Empty description="Tidak ada data." />
          )}
          {!isEmpty(statePolicy.list) && (
            <>
              <Form.Item>
                {getFieldDecorator('documents')(
                  <Checkbox.Group>
                    <Row gutter={24}>
                      {(statePolicy.list || []).map(item => (
                        <Col>
                          <Checkbox key={`doc-${item.id}`} value={item.id} className="text-center">
                            <Avatar
                              shape="square"
                              size={isMobile ? 75 : 150}
                              className="border-blue img-contain"
                              src={item.file || '/assets/avatar-on-error.jpg'}
                              onClick={() => setStateModal({ file: item.file, visible: true })}
                            />
                            <p className="mb-0 ml-3 text-capitalize">{item.description}</p>
                          </Checkbox>
                        </Col>
                      ))}
                    </Row>
                  </Checkbox.Group>,
                )}
              </Form.Item>
            </>
          )}
        </Card>
      </Form>
      <Modal
        visible={stateModal.visible}
        title={null}
        footer={null}
        closable={false}
        onCancel={() => setStateModal({ file: '', visible: false })}
      >
        <Avatar src={stateModal.file} shape="square" className="border-blue h-100 w-100" />
      </Modal>
    </React.Fragment>
  )
}

PolicyDetail.propTypes = {
  form: PropTypes.any,
  handleDelete: PropTypes.func,
  handleDownload: PropTypes.func,
  stateModal: PropTypes.object,
  setStateModal: PropTypes.func,
  statePolicy: PropTypes.object,
}

export default PolicyDetail
