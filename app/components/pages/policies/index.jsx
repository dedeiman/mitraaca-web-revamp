import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/policies/List'
import Detail from 'containers/pages/printPolicies/Detail'
import history from 'utils/history'

const Policy = ({ currentUser, location, stateCheck }) => {
  if (location.pathname === '/policy-search-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('policy-search-menu') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }

  if (location.pathname.includes('/detail')) {
    return <Detail location={location} />
  }

  return ''
}

Policy.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Policy
