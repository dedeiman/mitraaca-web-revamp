import React from 'react'
import PropTypes from 'prop-types'
import { AGENCY_CODE, AGENT_CODE, PO_CODE } from 'constants/ActionTypes'
import {
  Row, Col, Skeleton, Card, Empty,
} from 'antd'
import CardIO from './CardIO'
import CardSPPA from './CardSPPA'
import CardContest from './CardContest'
import CardBirthday from './CardBirthday'
import CardPolis from './CardPolis'
import CardAgent from './CardAgent'

const Dashboard = ({
  group, dataContest, loadContest, announcementsCount,
  announcements, agentsToday,
  birthdays, greeting, sppa,
  approvals, policies, iOffer,
  detailNotif, currentUser,
}) => {
  const order = {
    io: (currentUser.permissions && currentUser.permissions.indexOf('dashboard-indicative-offer-index') > -1) ? 1 : 0,
    sppa: (currentUser.permissions && currentUser.permissions.indexOf('dashboard-sppa-list') > -1) ? 2 : 0,
    contest: 3,
    birthday: ((currentUser.permissions && currentUser.role.group.name !== 'agent' && currentUser.permissions.indexOf('dashboard-birthday-partner') > -1)) ? 2 : 4,
    polis: (currentUser.permissions && currentUser.permissions.indexOf('dashboard-expiry-policy') > -1) ? 5 : 1,
    agent: ((currentUser.permissions && currentUser.role.group.name === 'agency') || (currentUser.permissions && currentUser.role.group.name === 'branch') || (currentUser.permissions && currentUser.role.group.name === 'cabang') || (currentUser.permissions && currentUser.role.group.name === 'product-owner')) ? 4 : 0,
  }

  return (
    <Row gutter={[24, 24]}>
      {((!order.agent) && (!order.io) && (!order.sppa) && (!order.birthday))
        ? (
          <Col xs={24} md={24}>
            <Card />
          </Col>
        )
        : null
      }
      {((!order.agent) && (!order.io) && (!order.sppa) && (!order.birthday))
        ? (
          <Col xs={24} md={24}>
            <Card />
          </Col>
        )
        : null
      }
      {order.io
        ? (
          <Col xs={24} md={12} order={order.io}>
            <CardIO
              data={iOffer.list}
              loading={iOffer.load}
            />
          </Col>
        )
        : null
      }

      {order.sppa
        ? (
          <Col xs={24} md={12} order={order.sppa}>
            <CardSPPA
              data={sppa.list}
              loading={sppa.load}
            />
          </Col>
        )
        : null
      }
      <Col xs={24} style={{ padding: '6px' }} order={order.contest}>
        {dataContest.length !== 0
          ? <CardContest data={dataContest} isPO={group.code === PO_CODE} />
          : (
            <div className="d-flex w-200 ml-2">
              <Card>
                <Empty
                  description="Tidak ada data."
                />
              </Card>
            </div>
          )
        }
      </Col>
      {order.birthday
        ? (
          <Col xs={24} md={group.code === AGENT_CODE ? 12 : 16} order={order.birthday}>
            <CardBirthday
              data={birthdays.list}
              greeting={greeting}
              isAgent={group.code === AGENT_CODE}
              loading={birthdays.load}
            />
          </Col>
        )
        : null
      }
      {order.polis
        ? (
          <Col xs={24} md={group.code === AGENT_CODE ? 12 : 8} order={order.polis}>
            <CardPolis
              announcementsCount={announcementsCount.list}
              isAgent={group.code === AGENT_CODE}
              data={(group.code === AGENT_CODE) ? policies.list : announcements.list}
              loading={(group.code === AGENT_CODE) ? policies.load : announcements.load}
              stateModal={(group.code === AGENT_CODE) ? false : { modal: announcements.modal, data: announcements.data }}
              openDetail={(id) => {
                if (group.code !== AGENT_CODE) {
                  detailNotif(id)
                }
              }}
            />
          </Col>
        )
        : null
      }

      {order.agent
        ? (
          <Col xs={24} md={24} order={order.agent}>
            <CardAgent
              isAgency={group.code === AGENCY_CODE}
              data={(group.code === AGENCY_CODE) ? agentsToday.list : approvals.list}
              loading={(group.code === AGENCY_CODE) ? agentsToday.load : approvals.load}
            />
          </Col>
        )
        : null
      }
    </Row>
  )
}

Dashboard.propTypes = {
  announcementsCount: PropTypes.object,
  currentUser: PropTypes.object,
  group: PropTypes.object,
  dataContest: PropTypes.array,
  loadContest: PropTypes.bool,
  announcements: PropTypes.object,
  agentsToday: PropTypes.object,
  birthdays: PropTypes.object,
  greeting: PropTypes.func,
  approvals: PropTypes.object,
  policies: PropTypes.object,
  iOffer: PropTypes.object,
  sppa: PropTypes.object,
  detailNotif: PropTypes.func,
}
export default Dashboard
