import React from 'react'
import Slider from 'react-slick'
import PropTypes from 'prop-types'
import { Button, Avatar, Popconfirm } from 'antd'
import history from 'utils/history'

const CustomArrow = ({ className, style, onClick }) => (
  <Button
    type="link"
    style={style}
    className={className}
    onClick={onClick}
  >
    <i
      className={(
        `las la-angle-${
          className.includes('next') ? 'right' : 'left'
        } font-weight-bold`
      )}
    />
  </Button>
)

const CardContest = ({ data, isPO }) => {
  const settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: <CustomArrow />,
    prevArrow: <CustomArrow />,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      },
    }, {
      breakpoint: 766,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
      },
    }],
  }
  return (
    <Slider {...settings}>
      {(data || []).map((item) => {
        // if (isPO) {
        //   return (
        //     <Avatar
        //       key={Math.random()}
        //       shape="square"
        //       className="h-100 w-100 icon-error"
        //       src={item.banner_url || '/assets/avatar-on-error.jpg'}
        //     >
        //       <img src="/assets/avatar-on-error.jpg" alt="icon-error" />
        //     </Avatar>
        //   )
        // }
        return (
          <Popconfirm
            key={Math.random()}
            title="Open this contest detail?"
            okText="Yes"
            cancelText="No"
            onConfirm={() => history.push(`/contest-menu/${item.id}`)}
          >
            <Avatar
              shape="square"
              className="h-100 w-100 icon-error"
              src={item.banner_url || '/assets/avatar-on-error.jpg'}
            >
              <img src="/assets/avatar-on-error.jpg" alt="icon-error" />
            </Avatar>
          </Popconfirm>
        )
      })}
    </Slider>
  )
}

CustomArrow.propTypes = {
  onClick: PropTypes.func,
  style: PropTypes.object,
  className: PropTypes.string,
}

CardContest.propTypes = {
  data: PropTypes.array,
  isPO: PropTypes.bool,
}

export default CardContest
