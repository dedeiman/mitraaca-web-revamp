import React from 'react'
import PropTypes from 'prop-types'
import {
  Divider, Button,
  Card, Badge, Avatar,
  Descriptions, Typography,
  Empty,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'

const CardIO = ({ data, loading }) => (
  <Card className="card-product" loading={loading}>
    <div className="d-flex justify-content-between">
      <h5 className="font-weight-bold mb-4">Indicative Offer</h5>
      <Badge count={null}>
        <Avatar size="large" src="/assets/ic-file.png" className="img-contain" />
      </Badge>
    </div>
    <div className="item">
      {data.length
        ? (
          data.map((item, idx) => {
            let tsi = 0
            let premi = 0
            if (item.ottomate_premi_calculation) {
              const { premi: premiVal, coverage, additional_accessories_costs: additional } = item.ottomate_premi_calculation
              tsi = (coverage + additional)
              premi = premiVal
            }
            if (item.asri_premi_calculation) {
              const { premi: premiVal, building_price: property, furniture_price: furniture } = item.asri_premi_calculation
              tsi = (Number(property || 0) + Number(furniture || 0))
              premi = premiVal
            }
            if (item.cargo_premi_calculation) {
              const { premi: premiVal, total_payment: totalVal } = item.cargo_premi_calculation
              premi = premiVal
              tsi = totalVal
            }
            if (item.travel_premi_calculation) {
              const { premi: premiVal } = item.travel_premi_calculation
              premi = premiVal
            }
            if (item.pa_amanah_premi_calculation) {
              const { premi: premiVal, coverage: coverageVal } = item.pa_amanah_premi_calculation
              tsi = (Number(premiVal || 0) + Number(coverageVal || 0))
              premi = premiVal
            }
            if (item.wellwoman_premi_calculation) {
              const { premi: premiVal } = item.wellwoman_premi_calculation
              premi = premiVal
              tsi = premiVal
            }
            if (item.liability_premi_calculation) {
              const { premi: premiVal, coverage_limit: coverageLimit } = item.liability_premi_calculation
              premi = premiVal
              tsi = coverageLimit
            }
            return (
              <React.Fragment key={Math.random()}>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item span={1} label="IO" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p className="mb-0">{item.io_number || '-'}</p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="TSI" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p className="mb-0">{Helper.currency(tsi, 'Rp. ')}</p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Product" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p
                        className="mb-0"
                        style={{
                          textOverflow: 'ellipsis',
                          overflow: 'hidden',
                          whiteSpace: 'break-spaces',
                        }}
                      >
                        {item.product ? Helper.getValue(item.product.display_name) : '-'}
                      </p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Premi" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p className="mb-0">{Helper.currency(premi, 'Rp. ')}</p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Nama" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p className="mb-0">{item.name || '-'}</p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Status IO" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                      <p className="mb-0">{item.status || '-'}</p>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                </Descriptions>
                <Button
                  htmlType="button"
                  onClick={() => {
                    history.push(`/production-create-io/detail/${item.id}?product=${item.product.code}`)
                  }}
                >
                  Detail IO
                </Button>
                {(data.length !== (idx + 1)) && (
                <Divider />
                )}
              </React.Fragment>
            )
          })
        )
        : <Empty description="Tidak ada Data." />
      }
    </div>

    <div className="text-center">
      <Button
        type="primary"
        className="w-75"
        onClick={() => history.push('/production-create-io')}
      >
        Create Indicative Offer
      </Button>
    </div>

  </Card>
)

CardIO.propTypes = {
  data: PropTypes.array,
  loading: PropTypes.bool,
}

export default CardIO
