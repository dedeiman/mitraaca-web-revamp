/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Card,
  Badge, Avatar,
  Popconfirm, Empty,
} from 'antd'
import moment from 'moment'
import history from 'utils/history'
import { capitalize } from 'lodash'

const CardBirthday = ({
  data, isAgent, greeting, loading,
}) => {
  const dataItem = data.filter(item => item.is_birthday_month)
  const dataCust = data.filter(item => item.say_birthday !== null)
  const findBirthdayMonth = data.find(item => (item.is_birthday_month !== false) || (item.is_birthday_today !== false))
  const isMobile = window.innerWidth < 768

  return (
    <Card className={`card-notif ${isMobile ? '' : ''}`} loading={loading}>
      <div className="d-flex justify-content-between">
        <h5 className="font-weight-bold mb-4">
          {isAgent ? 'Birthdays This Month' : "Partner's Birthday"}
        </h5>
        <Badge count={null}>
          <Avatar size="large" src="/assets/ic-kado.png" className="img-contain" />
        </Badge>
      </div>
      <div className="item" style={{ overflowX: 'hidden' }}>
        {((!isAgent && (dataItem.length !== 0)) || (isAgent && (findBirthdayMonth !== undefined)))
          ? (
            <>
              {dataItem.map(item => (
                <div className="d-flex justify-content-between align-items-center mb-2" key={Math.random()}>
                  <p
                    className="mb-0"
                    style={{
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      whiteSpace: 'nowrap',
                    }}
                  >
                    {item.is_birthday_today ? 'Today Is Agent' : `${moment(item.birthdate).utc().format('MMM DD')} Is Agent`}
                    {' '}
                    <a className="text-primary font-weight-bold" onClick={() => history.push(`/agent-search-menu/${item.id}`)}>
                      {capitalize(item.name || '-')}
                    </a>
                    &apos;s BIRTHDAY!
                  </p>
                  {(() => {
                    if (item.is_birthday_today) {
                      if (!item.user.flag_say_birthday) {
                        return (
                          <Popconfirm
                            title="Send Birthday Wishes?"
                            okText="Yes"
                            cancelText="No"
                            onConfirm={() => setTimeout(() => greeting(item.id))}
                          >
                            <Button ghost size="small" type="primary" className="border-blue rounded">
                              Say Happy Birthday
                            </Button>
                          </Popconfirm>
                        )
                      }
                      return <p style={{ color: '#B0B0B0' }} className="mb-0">Already sent messages</p>
                    }
                    return ''
                  })()}
                </div>
              ))}

              {dataCust.map((item) => {
                const custBirth = item.say_birthday.filter(birthdate => birthdate.is_birthday_month)

                return (
                  custBirth.map(birthCust => (
                    <div className="d-flex justify-content-between align-items-center mb-3" key={Math.random()}>
                      <p
                        className="mb-0"
                        style={{
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                          whiteSpace: 'nowrap',
                        }}
                      >
                        {birthCust.is_birthday_today ? 'Today Is Customer' : `${moment(birthCust.dob).utc().format('MMM DD')} Is Customer`}
                        {' '}
                        <a className="text-primary font-weight-bold" onClick={() => history.push(`/customer-menu/${birthCust.id}`)}>
                          {capitalize(birthCust.name || '-')}
                        </a>
                        &apos;s BIRTHDAY!
                      </p>
                      {(() => {
                        if (birthCust.is_birthday_today) {
                          if (!birthCust.flag_say_birthday) {
                            return (
                              <Popconfirm
                                title="Send Birthday Wishes?"
                                okText="Yes"
                                cancelText="No"
                                onConfirm={() => setTimeout(() => greeting(birthCust.id))}
                              >
                                <Button ghost size="small" type="primary" className="border-blue rounded">
                                  Say Happy Birthday
                                </Button>
                              </Popconfirm>
                            )
                          }
                          return <p style={{ color: '#B0B0B0' }} className="mb-0">Already sent messages</p>
                        }
                        return ''
                      })()}
                    </div>
                  ))
                )
              })}
            </>
          )
          : <Empty description="Tidak ada Data." />
        }
      </div>
    </Card>
  )
}
CardBirthday.propTypes = {
  data: PropTypes.array,
  loading: PropTypes.bool,
  isAgent: PropTypes.bool,
  greeting: PropTypes.func,
}

export default CardBirthday
