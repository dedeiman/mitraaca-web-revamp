import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Descriptions, Typography,
  Row, Col, Card, Avatar,
  Badge, Button, Divider,
  Empty, Tooltip,
} from 'antd'
import { capitalize } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const CardPolis = ({ data, isAgency, loading }) => (
  <Card className="card-notif" loading={loading}>
    <div className="d-flex justify-content-between">
      <h5 className="font-weight-bold mb-4">
        {isAgency ? 'New Agents Today' : 'Approval List'}
      </h5>
      {!isAgency && (
      <Badge count={null}>
        <Avatar size="large" src="/assets/ic-list.png" className="img-contain" />
      </Badge>
      )}
    </div>
    <div className="item">
      <Row gutter={[24, 24]} type="flex" align="bottom">
        {!data.length && (
          <Col span={24}>
            <Empty description="Tidak ada Data." />
          </Col>
        )}
        {isAgency
          ? (
            (data || []).map(item => (
              <Col xs={24} md={12} key={Math.random()} className="d-flex">
                <Avatar size={60} className="mr-4" style={{ background: '#2b57b7', fontSize: '24px' }} src={item.profile_pic}>
                  {capitalize(item.name.replace(' ', '')[0])}
                </Avatar>
                <div>
                  <Link to={`/agent-search-menu/${item.id}`} className="font-weight-bold text-primary mb-0">{item.name || '-'}</Link>
                  <p className="mb-2">{item.agent_id || '-'}</p>
                  <p className="mb-0">
                    {`Joining since ${item.created_at ? moment(item.created_at).format('DD MMMM YYYY') : '-'}`}
                  </p>
                  <p className="m-0">
                    {`Branch: ${item.branch ? Helper.getValue(item.branch.name) : '-'}`}
                  </p>
                </div>
              </Col>
            ))
          )
          : (
            (data || []).map((item, idx) => {
              let tsi = 0
              let premi = 0
              if (item.ottomate_calculation) {
                const { premi: premiVal, coverage, additional_accessories_costs: additional } = item.ottomate_calculation
                tsi = (coverage + additional)
                premi = premiVal
              }
              if (item.asri_calculation) {
                const { premi: premiVal, property_price: property, furniture_price: furniture } = item.asri_calculation
                tsi = (property + furniture)
                premi = premiVal
              }
              if (item.cargo_premi_calculation) {
                const { premi: premiVal } = item.cargo_premi_calculation
                premi = premiVal
              }
              return (
                <Col xs={24} md={12} key={Math.random()}>
                  <Row gutter={24} type="flex" align="bottom">
                    <Col xs={24} md={19}>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        <Descriptions.Item span={1} label="Nomor SPPA" className="px-1">
                          <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <p className="mb-0">{item.sppa_number || '-'}</p>
                          </Typography.Paragraph>
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="TSI" className="px-1">
                          <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <p className="mb-0">{Helper.currency(tsi, 'Rp. ')}</p>
                          </Typography.Paragraph>
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="Product" className="px-1">
                          <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <Tooltip
                              title={item.product ? Helper.getValue(item.product.display_name) : '-'}
                            >
                              <p
                                className="mb-0"
                                style={{
                                  textOverflow: 'ellipsis',
                                  overflow: 'hidden',
                                }}
                              >
                                {item.product ? Helper.getValue(item.product.display_name) : '-'}
                              </p>
                            </Tooltip>
                          </Typography.Paragraph>
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="Premi" className="px-1">
                          <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <p className="mb-0">{Helper.currency(premi, 'Rp. ')}</p>
                          </Typography.Paragraph>
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="Nama" className="px-1">
                          <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <p className="mb-0">{item.name_on_policy || '-'}</p>
                          </Typography.Paragraph>
                        </Descriptions.Item>
                      </Descriptions>
                    </Col>
                    <Col xs={24} md={5}>
                      <Button ghost type="primary" size="small" className="rounded" disabled={item.status === 'approved' || item.status === 'rejected' || item.status === 'expired'} onClick={() => history.push(`/approval-list-menu/${item.id}/approved`)}>
                        {(item.status === 'need-approval' ? 'Approve' : capitalize(item.status))}
                      </Button>
                    </Col>
                  </Row>
                  {(data.length !== (idx + 1)) && (
                  <Divider />
                  )}
                </Col>
              )
            })
          )
        }
      </Row>
    </div>
  </Card>
)

CardPolis.propTypes = {
  data: PropTypes.array,
  loading: PropTypes.bool,
  isAgency: PropTypes.bool,
}

export default CardPolis
