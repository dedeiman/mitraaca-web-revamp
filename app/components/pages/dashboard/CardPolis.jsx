import React from 'react'
import PropTypes from 'prop-types'
import {
  Divider, Button, Row,
  Col, Card, Badge, Avatar,
  Descriptions, Typography,
  Empty, Modal,
} from 'antd'
import history from 'utils/history'
import moment from 'moment'
import { isEmpty } from 'lodash'

const CardPolis = ({
  data, isAgent, loading, announcementsCount,
  stateModal, openDetail,
}) => (
  <Card className="card-notif" loading={loading}>
    <div className="d-flex justify-content-between">
      <h5 className="font-weight-bold mb-4">
        {isAgent ? 'Expiry Polis' : 'Notifications'}
      </h5>
      <Badge count={announcementsCount.count}>
        <Avatar size="large" src="/assets/ic-notif.png" className="img-contain" />
      </Badge>
    </div>
    <div className="item" style={{ overflowX: 'hidden' }}>
      {(isEmpty(data)) && (
        <Empty
          description="Tidak ada data."
        />
      )}
      {isAgent
        ? (
          (data || []).map((item, idx) => (
            <div
              key={Math.random()}
              style={{
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}
            >
              <Row gutter={24} type="flex" align="bottom">
                <Col xs={24} md={19}>
                  <Descriptions layout="vertical" colon={false} column={2}>
                    <Descriptions.Item span={1} label="SPPA Number" className="px-1">
                      <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                        <p className="mb-0">{item.sppa_number || '-'}</p>
                      </Typography.Paragraph>
                    </Descriptions.Item>
                    <Descriptions.Item span={1} label="Nama Tertanggung" className="px-1">
                      <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                        <p className="mb-0">{item.insured.name || '-'}</p>
                      </Typography.Paragraph>
                    </Descriptions.Item>
                    <Descriptions.Item span={1} label="Product" className="px-1">
                      <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                        <p
                          className="mb-0"
                          style={{
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            whiteSpace: 'break-spaces',
                          }}
                        >
                          {item.product.display_name || '-'}
                        </p>
                      </Typography.Paragraph>
                    </Descriptions.Item>
                    <Descriptions.Item span={1} label="Periode Polis" className="px-1">
                      <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                        <p
                          className="mb-0"
                          style={{
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            width: '90%',
                          }}
                        >
                          {item.start_period ? moment(item.start_period).format('DD MMM YYYY') : '-'}
                          {item.end_period ? ' - ' : ''}
                          {item.end_period ? moment(item.end_period).format('DD MMM YYYY') : ''}
                        </p>
                      </Typography.Paragraph>
                    </Descriptions.Item>
                  </Descriptions>
                </Col>
                <Col xs={24} md={5}>
                  <Button
                    ghost
                    type="primary"
                    size="small"
                    className="rounded mb-3"
                    onClick={() => {
                      history.push(`/production-create-sppa/edit/${item.id}?product=${item.product.code}&product_id=${item.product.id}&sppa_type=renewal${item.indicative_offer ? `&indicative_offer=${item.indicative_offer.io_number}` : ''}`)
                    }}
                  >
                    Renewal
                  </Button>
                </Col>
              </Row>
              {(data.length !== (idx + 1)) && (
              <Divider />
              )}
            </div>
          ))
        )
        : (
          (data || []).map(item => (
            <div className="d-flex justify-content-between align-items-center mb-3" key={Math.random()}>
              <Button
                type="link"
                className="text-muted px-0 mb-0"
                onClick={() => openDetail(item.id)}
                style={{
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap',
                }}
              >
                <Typography.Paragraph strong={item.is_read === false} ellipsis={{ rows: 1 }}>
                  {`(${item.end_date ? item.end_date : '-'}) ${item.subject}`}
                </Typography.Paragraph>
              </Button>
            </div>
          ))
        )
      }
    </div>
    <Modal
      title={<p className="mb-0 title-card">Detail Announcement</p>}
      visible={stateModal.modal}
      footer={null}
      closable={false}
      onCancel={() => openDetail(null)}
    >
      <Row gutter={24}>
        <Col xs={24}>
          {
            stateModal.data && (
              <>
                <p className="font-weight-light mb-0">
                  {stateModal.data.created_at}
                </p>
                <br />
                <p className="font-weight-bold mb-3">
                  Pengirim : {stateModal.data.sender ? stateModal.data.sender.name : '-'}
                </p>
                <p className="font-weight-bold mb-3">
                  Subject : {stateModal.data.subject}
                </p>
                <p className="font-weight-light mb-1">
                  Description : {stateModal.data.announcement}
                </p>
                <br /><br />
                <a className="text-primary font-weight-bold" onClick={() => history.push(`/announcement`)}>
                   Lihat semua notifikasi
                </a>
              </>
            )
          }
        </Col>
      </Row>
    </Modal>
  </Card>
)

CardPolis.propTypes = {
  announcementsCount: PropTypes.object,
  data: PropTypes.array,
  isAgent: PropTypes.bool,
  loading: PropTypes.bool,
  stateModal: PropTypes.object,
  openDetail: PropTypes.func,
}

export default CardPolis
