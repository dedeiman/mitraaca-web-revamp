import PropTypes from 'prop-types'
import List from 'containers/pages/announcement/List'
import history from 'utils/history'

const Announcement = ({ location, stateCheck }) => {
  if (location.pathname === '/announcement') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    }
    return <List location={location} />
  }

  return ''
}

Announcement.propTypes = {
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Announcement
