import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Divider,
  Descriptions,
  Typography,
  Pagination,
  Button,
  Result, Modal, Popconfirm,
} from 'antd'
import { Loader } from 'components/elements'
import { InboxOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import moment from 'moment'

const Announcement = ({
  detailNotif, dataAnnouncement,
  handlePage, metaAnnouncement,
  stateAnnouncement, announcementsList,
  isFetching, handleDelete,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Notifications</div>
        </Col>
      </Row>
      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataAnnouncement)) && (
          <Result
            icon={<InboxOutlined />}
            subTitle="Tidak ada data."
          />
        )}
        {!isEmpty(dataAnnouncement)
          ? (
            dataAnnouncement.map((item, idx) => (
              <div>
                <Descriptions layout="vertical" colon={false} column={7}>
                  <Descriptions.Item span={isMobile ? 12 : 1} label="Date" className="px-1">{item.created_at}</Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 12 : 2} label="Subject" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                      {item.subject}
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 12 : 1} label="Sender" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                      {item.sender.name}
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 15 : 3} className="px-1 text-center desc-action">
                    <Row gutter={12}>
                      <React.Fragment>
                        <Col xs={24} md={12}>
                          <Button
                            type="primary"
                            className="w-100"
                            onClick={() => detailNotif(item.id)}
                          >
                            Detail
                          </Button>
                          <Popconfirm
                            title="Anda akan menghapus data ini?"
                            okText="Yes"
                            cancelText="No"
                            onConfirm={() => handleDelete(item.id)}
                          >
                            <Button
                              type="primary"
                              className="w-100 mt-2"
                            >
                              Hapus
                            </Button>
                          </Popconfirm>
                        </Col>
                      </React.Fragment>
                    </Row>
                  </Descriptions.Item>
                </Descriptions>
                {(idx !== (dataAnnouncement.length - 1)) && (<Divider />)}
                <Modal
                  title={<p className="mb-0 title-card">Detail Announcement</p>}
                  visible={announcementsList.modal}
                  footer={null}
                  onCancel={() => detailNotif(null)}
                >
                  <Row gutter={24}>
                    <Col xs={24}>
                      <>
                        <p className="font-weight-light mb-0">
                          {announcementsList.data ? announcementsList.data.created_at : ''}
                        </p>
                        <br />
                        <p className="font-weight-bold mb-3">
                          Pengirim : {announcementsList.data ? announcementsList.data.sender ? announcementsList.data.sender.name : '' : ''}
                        </p>
                        <p className="font-weight-bold mb-3">
                          Subject : {announcementsList.data ? announcementsList.data.subject : ''}
                        </p>
                        <p className="font-weight-light mb-1">
                          Description : {announcementsList.data ? announcementsList.data.announcement : ''}
                        </p>
                      </>
                    </Col>
                  </Row>
                </Modal>
              </div>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={stateAnnouncement.page ? Number(stateAnnouncement.page) : 1}
        onChange={handlePage}
        total={metaAnnouncement.total_count}
        showSizeChanger={false}
        simple={isMobile}
      />
    </React.Fragment>
  )
}

Announcement.propTypes = {
  handleDelete: PropTypes.func,
  detailNotif: PropTypes.func,
  dataAnnouncement: PropTypes.array,
  metaAnnouncement: PropTypes.object,
  stateAnnouncement: PropTypes.object,
  handlePage: PropTypes.func,
  isFetching: PropTypes.bool,
  announcementsList: PropTypes.object,
}

export default Announcement
