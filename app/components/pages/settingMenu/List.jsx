import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Form, Checkbox, Button,
} from 'antd'
import { capitalize } from 'lodash'

const SettingMenu = ({
  detailSettingMenu, detailSettingMenuBefore, onSubmit, form, onChange,
}) => {
  const {
    getFieldDecorator,
  } = form

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Setting Menu</div>
        </Col>
      </Row>
      <Card>
        <Form onSubmit={onSubmit}>
          <Form.Item label="Setting Menu : ">
            {getFieldDecorator('permissions', {
              initialValue: detailSettingMenu.listDetailSettingMenu ? detailSettingMenu.listDetailSettingMenu.filter(item => item.is_locked === true).map(o => o.name) : detailSettingMenu.listDetailSettingMenu.filter(item => item.is_locked === true).map(o => o.name),
            })(
              <Checkbox.Group onChange={onChange}>
                <Row gutter={[12, 12]}>
                  {detailSettingMenu.listDetailSettingMenu.map(item => (
                    <Col xs={24} md={12} sm={24} key={`chkb_${item.name}`}>
                      <Checkbox value={item.name}>{capitalize((item.name).split('-').join(' '))}</Checkbox>
                    </Col>
                  ))}
                </Row>
              </Checkbox.Group>,
            )}
          </Form.Item>
          <Row className="my-3">
            <Col span={24} className="d-flex justify-content-end align-items-center mb-3">
              <Button
                type="primary"
                htmlType="submit"
                className="button-lg mr-3"
                onClick={() => onSubmit(form)}
              >
                Submit Change
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    </React.Fragment>
  )
}

SettingMenu.propTypes = {
  onSubmit: PropTypes.func,
  form: PropTypes.any,
  onChange: PropTypes.func,
  detailSettingMenu: PropTypes.object,
  detailSettingMenuBefore: PropTypes.object,
}

export default SettingMenu
