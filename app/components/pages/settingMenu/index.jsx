import PropTypes from 'prop-types'
import List from 'containers/pages/settingMenu/List'
import SecondaryPassword from 'containers/pages/secondaryPassword/index'
import history from 'utils/history'

const settingMenu = ({ location, stateCheck }) => {
  if (location.pathname.includes('/setting-menu')) {
    if (window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    }
    return <List />
  }

  return ''
}

settingMenu.propTypes = {
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default settingMenu
