/* eslint-disable react/jsx-closing-tag-location */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Card,
  Avatar, Tree as TreeList,
  Typography, Empty,
  Row, Col,
} from 'antd'
import { Tree } from 'react-organizational-chart'
import { capitalize, isEmpty, first } from 'lodash'
import Helper from 'utils/Helper'

const CareerPath = ({
  currentUser, stateCareer, setStateCareer,
}) => {
  const element = []
  const treeData = []

  stateCareer.list.map(item => (
    treeData.push({
      title: <Card
        style={{
          marginBottom: '10px',
          width: '350px',
        }}
      >
        <React.Fragment key={`list_tree_${Math.random()}`}>
          <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex justify-content-between align-items-center">
              <Avatar
                size="large"
                className="mr-3"
                style={{
                  fontSize: '18px',
                  backgroundColor: '#2b57b7',
                  fontFamily: 'Poppins-Bold',
                }}
              >
                {capitalize(item.agent_name.replace(' ', '')[0])}
              </Avatar>
              <div>
                <p
                  className="title-card mb-0"
                  style={{ textAlign: 'left' }}
                >
                  {(item.agent_name || '').toUpperCase()}
                </p>
                <p
                  className="mb-0"
                  style={{ textAlign: 'left' }}
                >
                  {item.level_name}
                </p>
              </div>
            </div>
          </div>
        </React.Fragment>
      </Card>,
      key: `${Math.random()}`,
      children: !isEmpty(item) ? Helper.renderChildList(item.partners) : null,
    })
  ))

  if (stateCareer.isTree) {
    element.push(
      <Col xs={24} md={24}>
        <Card className="wrapper-career h-100" loading={stateCareer.load}>
          <Tree
            lineWidth="2px"
            lineColor="#2b57b7"
            label={(
              <Card className="card-career">
                <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                  Partner
                </Typography.Paragraph>
                <Typography.Paragraph className="name" ellipsis={{ rows: 1 }}>
                  <Avatar className="initial">{first(capitalize(currentUser.name.replace(' ', '')))}</Avatar>
                  {currentUser.name}
                </Typography.Paragraph>
                <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                  {isEmpty(currentUser.branch_perwakilan) ? '-' : first(currentUser.branch_perwakilan).name}
                </Typography.Paragraph>
              </Card>
            )}
          >
            {Helper.renderChild(stateCareer.list, true)}
          </Tree>
        </Card>
      </Col>,
    )
  } else {
    element.push(
      <Col xs={8} md={8} key={Math.random()}>
        {isEmpty(stateCareer.list)
          ? <Card><Empty description="Tidak ada Data." /></Card>
          : (
            <TreeList
              treeData={treeData}
            />
          )}
      </Col>,
    )
  }

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Jenjang Karir</div>
        </Col>
        <Col span={24} className="text-center">
          {(currentUser.permissions && currentUser.permissions.indexOf('profile-agent-tree') > -1) && (
            <Button
              className="w-25 mr-md-3"
              onClick={() => setStateCareer({ ...stateCareer, isTree: true })}
              type={stateCareer.isTree ? 'primary' : ''}
            >
              Agent Tree
            </Button>
          )}
          {(currentUser.permissions && currentUser.permissions.indexOf('profile-agent-list') > -1) && (
            <Button
              className="w-25"
              onClick={() => setStateCareer({ ...stateCareer, isTree: false })}
              type={!stateCareer.isTree ? 'primary' : ''}
            >
              Agent List
            </Button>
          )}
        </Col>
      </Row>
      <Row gutter={24} className={`mb-5 ${stateCareer.isTree ? '' : 'overflow-scroll'}`}>
        {element}
      </Row>
    </React.Fragment>
  )
}

CareerPath.propTypes = {
  currentUser: PropTypes.object,
  stateCareer: PropTypes.object,
  setStateCareer: PropTypes.func,
}

export default CareerPath
