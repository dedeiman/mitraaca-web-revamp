import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'antd'
import PrimaryPassword from 'containers/pages/profiles/PrimaryPassword'
import SecondaryPassword from 'containers/pages/profiles/SecondaryPassword'

const Password = ({ isSecondary, toggleSecondary }) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Change Password</div>
        </Col>
      </Row>
      <Row gutter={24} justify="center" className="mb-5">
        <Col xs={24} md={12} className={isMobile ? 'mb-3' : ''}>
          <PrimaryPassword isSecondary={isSecondary} toggleSecondary={toggleSecondary} />
        </Col>

        {isSecondary && (
        <Col xs={24} md={12} className={isMobile ? 'mb-3' : ''}>
          <SecondaryPassword />
        </Col>
        )}
      </Row>
    </React.Fragment>
  )
}
Password.propTypes = {
  isSecondary: PropTypes.bool,
  toggleSecondary: PropTypes.func,
}

export default Password
