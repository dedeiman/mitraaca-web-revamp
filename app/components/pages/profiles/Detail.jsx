import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Empty,
  Button, Card,
  Avatar, Descriptions,
  Divider, Select,
  Pagination,
} from 'antd'
import history from 'utils/history'
import { DoubleRightOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import moment from 'moment'

const DetailProfile = ({
  isFetching, detailProfile,
  stateButton, setStateButton,
  handleDetail, profilePic, setProfilePic,
  handlePage,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mb-3">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">My Profile</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={isFetching}>
                <div className="d-block d-md-flex justify-content-between align-items-end">
                  <div>
                    <p className="title-card">{(detailProfile.name || '-').toUpperCase()}</p>
                    <Avatar
                      src={profilePic || '/assets/avatar-on-error.jpg'}
                      shape="square"
                      size={150}
                      className="img-contain"
                      onError={() => setProfilePic('')}
                    />
                  </div>
                  <div className="text-right">
                    <p className="mb-0">Join Date</p>
                    <p className="mb-0">{detailProfile.created_at ? moment(detailProfile.created_at).format('DD MMM YYYY') : '-'}</p>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={24}>
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Personal Info Agent</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item span={2} label="Agent Name" className="px-1 profile-detail pb-0">
                    {(detailProfile.name || '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Nomor KTP" className="px-1 profile-detail pb-0">
                    {detailProfile.no_ktp || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Birth Place" className="px-1 profile-detail pb-0">
                    {(detailProfile.birth_place || '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Day of Birth" className="px-1 profile-detail pb-0">
                    {(detailProfile.birthdate ? moment.utc(detailProfile.birthdate).format('DD MMM YYYY') : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Gender" className="px-1 profile-detail pb-0">
                    {(!isEmpty(detailProfile.gender) ? detailProfile.gender.name : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Religion" className="px-1 profile-detail pb-0">
                    {(!isEmpty(detailProfile.religion) ? detailProfile.religion.name : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Marital Status" className="px-1 profile-detail pb-0">
                    {(!isEmpty(detailProfile.marital_status) ? detailProfile.marital_status.display_name : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Employment" className="px-1 profile-detail pb-0">
                    {(!isEmpty(detailProfile.employment) ? detailProfile.employment.display_name : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Email" className="px-1 profile-detail pb-0">
                    {(detailProfile.user ? Helper.getValue(detailProfile.user.email) : '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Phone Number" className="px-1 profile-detail pb-0">
                    {detailProfile.user ? Helper.getValue(detailProfile.user.phone_number) : '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Alamat" className="px-1 profile-detail pb-0">
                    <p className="mb-0">
                      {detailProfile.province ? detailProfile.province.name : ''}
                      {' '}
                      -
                      {' '}
                      {detailProfile.zip_code}
                    </p>
                    <p>{detailProfile.address || '-'}</p>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Alamat Alternative" className="px-1 profile-detail pb-0">
                    <p className="mb-0">
                      {detailProfile.residence_province ? detailProfile.residence_province.name : ''}
                      {' '}
                      -
                      {' '}
                      {detailProfile.residence_zip_code}
                    </p>
                    <p>{detailProfile.residence_address || '-'}</p>
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Agent ID</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent Type" className="px-1 profile-detail pb-0">
                    {(detailProfile.type || '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent Source" className="px-1 profile-detail pb-0">
                    {(detailProfile.source || '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent ID" className="px-1 profile-detail pb-0">
                    {(detailProfile.agent_id || '-').toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Profile ID" className="px-1 profile-detail pb-0">
                    {detailProfile.profile_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="ID Syariah" className="px-1 profile-detail pb-0">
                    {detailProfile.syariah_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Reference ID" className="px-1 profile-detail pb-0">
                    {detailProfile.reference_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Upline Agent ID" className="px-1 profile-detail pb-0">
                    {detailProfile.upline_id || '-'}
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
            <Col span={24} className="h-100">
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Work Area</p>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={1} label="Agent Level" className="px-1 profile-detail pb-0 uppercase">
                    {(detailProfile.level ? detailProfile.level.name : '-')}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Branch Perwakilan" className="px-1 profile-detail pb-0 uppercase">
                    {(detailProfile.branch_perwakilan ? detailProfile.branch_perwakilan.name : '-')}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Branch" className="px-1 profile-detail pb-0 uppercase">
                    {(detailProfile.branch ? detailProfile.branch.name : '-')}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Kode Ulas" className="px-1 profile-detail pb-0 uppercase">
                    {(detailProfile.review_code ? detailProfile.review_code.ulas_code : '-')}
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Card>
            <Row gutter={24}>
              <Col xs={24} md={4}>
                {isMobile && (
                  <Select
                    onChange={(val) => {
                      setStateButton({ ...stateButton, active: val })
                      handleDetail(val)
                    }}
                    value={stateButton.active}
                    className="field-lg w-100"
                  >
                    {(stateButton.list || []).map(item => (
                      <Select.Option key={`select_${item}`} value={item}>{item}</Select.Option>
                    ))}
                  </Select>
                )}
                {!isMobile && stateButton.list.map(item => (
                  <Button
                    className="button-profile shadow"
                    key={`button_${item}`}
                    type={stateButton.active === item ? 'primary' : ''}
                    onClick={() => {
                      setStateButton({ ...stateButton, active: item })
                      handleDetail(item)
                    }}
                  >
                    {item}
                    {stateButton.active === item ? <DoubleRightOutlined /> : null}
                  </Button>
                ))}
              </Col>
              <Col xs={24} md={20} className="px-0 px-md-5">
                <p className="title-card pt-3 pt-md-0">
                  {((stateButton.active === 'Training') || (stateButton.active === 'Contest'))
                    ? `History ${stateButton.active}`
                    : stateButton.active
                  }
                </p>
                <Card className="border-0 card-description shadow-none" loading={stateButton.isFetching}>
                  {(
                    ((stateButton.active === 'Product') && isEmpty(stateButton.products))
                    || ((stateButton.active === 'Contract') && isEmpty(stateButton.contracts))
                    || ((stateButton.active === 'License') && isEmpty(stateButton.licenses))
                    || ((stateButton.active === 'Bank') && isEmpty(stateButton.banks))
                    || ((stateButton.active === 'Level') && isEmpty(stateButton.levels))
                    || ((stateButton.active === 'Taxation') && isEmpty(stateButton.taxation))
                    || ((stateButton.active === 'Document') && isEmpty(stateButton.documents))
                    || ((stateButton.active === 'Training') && isEmpty(stateButton['training-histories']))
                    || ((stateButton.active === 'Contest') && isEmpty(stateButton['contest-histories']))
                  ) && (
                    <Empty description="Tidak ada data" />
                  )}
                  {stateButton.active === 'Product' && (
                    (stateButton.products || []).map((item, i) => (
                      <React.Fragment key={`product_${item.id}`}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={isMobile ? 4 : 2} label="Product" className="px-1 profile-detail pb-0">
                            {item.product ? Helper.getValue(item.product.display_name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Tanggal Efektif" className="px-1 profile-detail pb-0">
                            {item.effective_date ? moment(item.effective_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Tanggal Expiry" className="px-1 profile-detail pb-0">
                            {item.expiry_date ? moment(item.expiry_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.products.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Contract' && (
                    (stateButton.contracts || []).map((item, i) => (
                      <React.Fragment key={`contract_${item.id}`}>
                        <Descriptions layout="vertical" colon={false} column={5}>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Contract" className="px-1 profile-detail pb-0">
                            {item.contract_type ? Helper.getValue(item.contract_type.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nomor Contract" className="px-1 profile-detail pb-0">
                            {item.contract_number || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Efektif" className="px-1 profile-detail pb-0">
                            {item.start_date ? moment(item.start_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Expiry" className="px-1 profile-detail pb-0">
                            {item.end_date ? moment(item.end_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Pemutusan" className="px-1 profile-detail pb-0">
                            {item.termination_date ? moment(item.termination_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.contracts.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'License' && (
                    (stateButton.licenses || []).map((item, i) => (
                      <React.Fragment key={`lecense_${item.id}`}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="License Type" className="px-1 profile-detail pb-0">
                            {item.license_type ? Helper.getValue(item.license_type.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="License No" className="px-1 profile-detail pb-0">
                            {item.license_number || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Tanggal Awal" className="px-1 profile-detail pb-0">
                            {item.start_date ? moment(item.start_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Tanggal Akhir" className="px-1 profile-detail pb-0">
                            {item.end_date ? moment(item.end_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.licenses.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Bank' && (
                    (stateButton.banks || []).map((item, i) => (
                      <React.Fragment key={`kontes_${item.contest_id}`}>
                        <Descriptions layout="vertical" colon={false} column={5}>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nama Bank" className="px-1 text-uppercase profile-detail pb-0">
                            {item.bank ? Helper.getValue(item.bank.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nama" className="px-1 text-uppercase profile-detail pb-0">
                            {item.name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nomor Rekening" className="px-1 text-uppercase profile-detail pb-0">
                            {item.account_number || '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.banks.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Taxation' && (
                    (stateButton.taxation || []).map((item, i) => (
                      <React.Fragment key={`tax_${item.id}`}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Tax Type" className="px-1 profile-detail pb-0">
                            {item.type ? Helper.getValue(item.type.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Nama NPWP" className="px-1 profile-detail pb-0">
                            {item.name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Nomor NPWP" className="px-1 profile-detail pb-0">
                            {item.npwp_number || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Status PTKP" className="px-1 profile-detail pb-0">
                            {item.ptkp_status ? Helper.getValue(item.ptkp_status.name) : '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.taxation.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Document' && (
                    (stateButton.documents || []).map((item, i) => (
                      <React.Fragment key={`document_${item.id}`}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={isMobile ? 4 : 1} className="px-1 profile-detail pb-0">
                            <Avatar
                              shape="square"
                              size={150}
                              src={item.file_url}
                              className="img-contain"
                            />
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Kategori" className="px-1 profile-detail pb-0">
                            {item.document_category ? Helper.getValue(item.document_category.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Deskripsi" className="px-1 profile-detail pb-0">
                            {item.description || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Upload By" className="px-1 profile-detail pb-0">
                            {item.uploader ? Helper.getValue(item.uploader.name) : '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.documents.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Training' && (
                    (stateButton['training-histories'] || []).map((item, i) => (
                      <React.Fragment key={`training_${Math.random()}`}>
                        <Descriptions layout="vertical" colon={false} column={3}>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Subject" className="px-1 profile-detail pb-0">
                            {item.subject_name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Tanggal Training" className="px-1 profile-detail pb-0">
                            {item.training_date ? moment(item.training_date).format('DD MMMM YYYY') : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Status" className="px-1 profile-detail pb-0">
                            {item.is_present ? 'Ya' : 'Tidak'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton['training-histories'].length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Level' && (
                    (stateButton.levels || []).map((item, i) => (
                      <React.Fragment key={`kontes_${item.contest_id}`}>
                        <Descriptions layout="vertical" colon={false} column={3}>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Agent ID" className="px-1 text-uppercase profile-detail pb-0">
                            {item.agent_id || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="From Level" className="px-1 text-uppercase profile-detail pb-0">
                            {!isEmpty(item.from_level) ? item.from_level.name : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="To Level" className="px-1 text-uppercase profile-detail pb-0">
                            {!isEmpty(item.to_level) ? item.to_level.name : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Reformer" className="px-1 text-uppercase profile-detail pb-0">
                            {!isEmpty(item.reformer) ? item.reformer.name : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 3 : 1} label="Reason" className="px-1 text-uppercase profile-detail pb-0">
                            {item.reason || '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.levels.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                  {stateButton.active === 'Contest' && (
                    (stateButton['contest-histories'] || []).map((item, i) => (
                      <React.Fragment key={`kontes_${item.contest_id}`}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Kontes ID" className="px-1 profile-detail pb-0">
                            {item.contest_id || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Nama Kontes" className="px-1 profile-detail pb-0">
                            {item.contest_name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Periode Kontes" className="px-1 profile-detail pb-0">
                            {item.contest_period || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 4 : 1} label="Reward" className="px-1 profile-detail pb-0">
                            {item.reward_name || '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton['contest-histories'].length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              simple={isMobile}
                              pageSize={stateButton.meta_page.per_page || 10}
                              current={stateButton.meta_page.page ? Number(stateButton.meta_page.page) : 1}
                              onChange={handlePage}
                              total={stateButton.meta_page.total_count ? stateButton.meta_page.total_count : 1}
                              showSizeChanger={false}
                            />
                          )
                        }
                      </React.Fragment>
                    ))
                  )}
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailProfile.propTypes = {
  detailProfile: PropTypes.object,
  isFetching: PropTypes.bool,
  handleDetail: PropTypes.func,
  setStateButton: PropTypes.func,
  stateButton: PropTypes.object,
  profilePic: PropTypes.string,
  setProfilePic: PropTypes.func,
  handlePage: PropTypes.func,
}

export default DetailProfile
