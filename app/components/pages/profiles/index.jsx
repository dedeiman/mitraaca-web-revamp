import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import PasswordChange from 'containers/pages/profiles/Password'
import FirstPassword from 'containers/pages/profiles/FirstPassword'
import Detail from 'containers/pages/profiles/Detail'
import Career from 'containers/pages/profiles/Career'
import Helper from 'utils/Helper'

const ProfileRoutes = ({
  currentUser, match,
  location, user,
}) => {
  // if (location.pathname === '/materi') {
  //   if (groupRole.code === BRANCH_CODE) {
  //     return <Forbidden />
  //   }
  //   return <List location={location} />
  // }
  if (match.params.type === 'change') {
    if (location.hash === '#first' && Helper.loginValidations(user.validations)) {
      return <FirstPassword match={match} location={location} />
    }
    return <PasswordChange match={match} location={location} />
  }
  if (match.params.type === 'profile-index') {
    if (currentUser.permissions && currentUser.permissions.indexOf('profile-menu') > -1) {
      return <Detail match={match} />
    }
    return <Forbidden />
  }
  if (match.params.type === 'profile-level-tree-menu') {
    if (currentUser.permissions && currentUser.permissions.indexOf('profile-level-tree-menu') > -1) {
      return <Career match={match} />
    }
    return <Forbidden />
  }
  // if (location.pathname.includes('/edit') || (location.pathname === '/materi/add')) {
  //   if (groupRole.code === BRANCH_CODE || groupRole.code === AGENT_CODE) {
  //     return <Forbidden />
  //   }
  //   return <Form match={match} />
  // }


  return ''
}

ProfileRoutes.propTypes = {
  location: PropTypes.object,
  currentUser: PropTypes.object,
  match: PropTypes.object,
  user: PropTypes.object,
}

export default ProfileRoutes
