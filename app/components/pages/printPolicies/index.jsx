import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/printPolicies/List'
import Detail from 'containers/pages/printPolicies/Detail'
import history from 'utils/history'

const printPolicy = ({ currentUser, location, stateCheck }) => {
  if (location.pathname === '/pending-policy-list') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('pending-policy-list') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }

  if (location.pathname.includes('/detail')) {
    return <Detail location={location} />
  }

  return ''
}

printPolicy.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default printPolicy
