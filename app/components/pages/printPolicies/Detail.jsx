import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Row, Col, Card,
  Descriptions,
} from 'antd'
import { isEmpty } from 'lodash'
import history from 'utils/history'
import Helper from 'utils/Helper'

export default function Detail({
  handlePrintPolicies,
  dataPolicy,
}) {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Card className="mt-5 mr-5 ml-5">
        <p className="title-card">Policy</p>
        <Descriptions layout="vertical" colon={false} column={5}>
          <Descriptions.Item span={1} label="SPPA / Policy No" className="px-1 profile-detail pb-0">
            {dataPolicy && (dataPolicy.sppa_number || '-')}
          </Descriptions.Item>
          <Descriptions.Item span={1} label="Nama Tertanggung" className="px-1 profile-detail pb-0">
            {dataPolicy && (dataPolicy.name_on_policy || '-')}
          </Descriptions.Item>
          <Descriptions.Item span={1} label="Print By" className="px-1">
            {dataPolicy && (!isEmpty(dataPolicy.sppa_print_history) ? dataPolicy.sppa_print_history.printed_by : '-')}
          </Descriptions.Item>
          <Descriptions.Item span={1} label="Print Date" className="px-1 profile-detail pb-0">
            {dataPolicy && (!isEmpty(dataPolicy.sppa_print_history) ? dataPolicy.sppa_print_history.print_date : '-')}
          </Descriptions.Item>
          <Descriptions.Item span={1} label="Total Payment" className="px-1 profile-detail pb-0">
            {dataPolicy && (Helper.currency(dataPolicy.total_payment, 'Rp. ', ',-') || '-')}
          </Descriptions.Item>
          <Descriptions.Item span={2} label="Alamat" className="px-1 profile-detail pb-0">
            {dataPolicy && (dataPolicy.insured.address || '-')}
          </Descriptions.Item>
        </Descriptions>
      </Card>
      <Row className="my-4">
        <Col span={24} className={isMobile ? 'mb-3 d-flex justify-content-end align-items-center' : 'd-flex justify-content-end align-items-center'}>
          <Button
            ghost
            type="primary"
            className={isMobile ? 'button-lg mr-3' : 'button-lg w-15 mr-3'}
            htmlType="button"
            onClick={() => history.goBack()}
          >
            Cancel
          </Button>
          <Button
            type="primary"
            className={`mr-5 ${isMobile ? 'button-lg border-lg' : 'button-lg w-15 border-lg'}`}
            htmlType="button"
            onClick={() => {
              window.print()
              handlePrintPolicies(dataPolicy.id)
            }}
          >
            {dataPolicy && (isEmpty(dataPolicy.sppa_print_history) ? 'Print Policy' : 'Reprint')}
          </Button>
        </Col>
      </Row>
    </React.Fragment>
  )
}

Detail.propTypes = {
  dataPolicy: PropTypes.object,
  handlePrintPolicies: PropTypes.func,
}
