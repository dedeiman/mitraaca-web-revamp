import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Descriptions,
  Button, Select,
  Pagination, Empty,
  Typography, Divider,
} from 'antd'
import { Form } from '@ant-design/compatible'

export default function List({
  statePrint, currentUser,
  getPrintPolicies, setStatePrint,
  handlePage, openDetailPage,
}) {
  const isMobile = window.innerWidth < 768
  return (
    <>
      <Row className="mt-2" gutter={[24, 24]}>
        <Col sm={24} md={17} lg={19} xl={21}>
          <Form.Item className="mb-0" label="Status SPPA">
            <Select
              onChange={(e) => {
                getPrintPolicies(e)
              }}
              allowClear
              value={undefined}
              loading={false}
              placeholder="Select Status"
              className={`field-lg ${isMobile ? 'w-100' : 'w-50'}`}
            >
              <Select.Option key="0" value="">SEMUA</Select.Option>
              <Select.Option key="1" value="not-printed">BELUM CETAK</Select.Option>
              <Select.Option key="2" value="printed">SUDAH CETAK</Select.Option>
            </Select>
          </Form.Item>
        </Col>
        <Col sm={24} md={24} lg={10}>
          <Input
            allowClear
            placeholder="Search SPPA / Policy Number..."
            className="field-lg"
            value={statePrint.searchSPPA}
            onChange={(e) => {
              setStatePrint({
                ...statePrint,
                searchSPPA: (e.target.value || '').toUpperCase(),
              })
            }}
            onPressEnter={() => getPrintPolicies('')}
          />
        </Col>
        <Col sm={24} md={24} lg={10}>
          <Input
            allowClear
            placeholder="Search Name..."
            className="field-lg"
            value={statePrint.searchName}
            onChange={(e) => {
              setStatePrint({
                ...statePrint,
                searchName: (e.target.value || '').toUpperCase(),
              })
            }}
            onPressEnter={() => getPrintPolicies('')}
          />
        </Col>
        <Col sm={24} md={8} xl={4}>
          <Button type="primary w-100" className="button-lg" onClick={() => getPrintPolicies('')}>
            Search
          </Button>
        </Col>
      </Row>
      
      <Card loading={statePrint.loading}>
        {statePrint.list.map((item, idx) => (
          <>
            <Row gutter={24} key={item.id}>
              <Col xs={24} md={12} xl={6}>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={isMobile ? 1 : 1} label="SPPA / Policy No" className="px-1">{item.sppa_number}</Descriptions.Item>
                </Descriptions>
              </Col>
              <Col xs={24} md={6} xl={6}>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={isMobile ? 4 : 1} label="Nama Tertanggung" className="px-1">{item.insured.name || ''}</Descriptions.Item>
                </Descriptions>
              </Col>
              <Col xs={24} md={6} xl={6}>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={isMobile ? 4 : 1} label="Alamat" className="px-1">{item.insured.address}</Descriptions.Item>
                </Descriptions>
              </Col>
              <Col xs={24} md={12} xl={6}>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={isMobile ? 4 : 1} label="Tanggal Cetak" className="px-1">
                    {item.sppa_print_history
                      && (
                      <>
                        <Typography.Paragraph className="label mb-0" ellipsis={{ rows: 1 }}>
                          Print By :
                          {' '}
                          {' '}
                          {item.sppa_print_history.printed_by}
                        </Typography.Paragraph>
                        <Typography.Paragraph className="label mb-0" ellipsis={{ rows: 1 }}>
                          Print Date :
                          {' '}
                          {' '}
                          {item.sppa_print_history.print_date}
                        </Typography.Paragraph>
                        <Typography.Paragraph className="label mb-3" ellipsis={{ rows: 1 }}>
                          Reprint :
                          {' '}
                          {' '}
                          {item.sppa_print_history.reprint}
                        </Typography.Paragraph>
                      </>
                      )
                    }
                    {(currentUser.permissions && currentUser.permissions.indexOf('pending-policy-generate') > -1) && (
                      <Button
                        type="primary"
                        className={`text-center ${isMobile ? 'w-100' : ''}`}
                        htmlType="button"
                        onClick={() => openDetailPage(item.id)}
                      >
                        Show Detail
                      </Button>
                      // <Button
                      //   type="primary"
                      //   className="w-50"
                      //   htmlType="button"
                      //   onClick={() => {
                      //     window.print()
                      //     handlePrintPolicies(item.id)
                      //     window.location.reload()
                      //   }
                      // }
                      // >
                      //   {isEmpty(item.sppa_print_history) ? 'Print Policy' : 'Reprint'}
                      // </Button>
                    )}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
            </Row>
            {(idx !== (statePrint.list.length - 1)) && (
              <Divider />
            )}
          </>
        ))}
        {statePrint.list.length === 0
          && (
            <Empty
              description="Tidak ada data."
            />
          )
        }
      </Card>
      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={statePrint.page ? Number(statePrint.page.current_page) : 1}
        onChange={handlePage}
        total={statePrint.page.total_count}
        pageSize={statePrint.page.per_page || 10}
      />
    </>
  )
}

List.propTypes = {
  currentUser: PropTypes.object,
  statePrint: PropTypes.object,
  getPrintPolicies: PropTypes.func,
  setStatePrint: PropTypes.func,
  openDetailPage: PropTypes.func,
  handlePage: PropTypes.func,
}
