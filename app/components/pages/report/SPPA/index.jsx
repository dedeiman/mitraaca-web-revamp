import React from 'react'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select

const ApprovalReport = ({
  sppaReport, state, changeState,
  handleSearch, handleTableChange,
  handleReport, handleRepresentative,
  handleProduct,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: 'no',
      render: (text, record, idx) => (sppaReport.list.pagination.current_page > 1 ? (sppaReport.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
    },
    {
      title: 'Agent ID',
      dataIndex: ['agent_id'],
    },
    {
      title: 'Agent Profile ID',
      dataIndex: 'profile_id',
    },
    {
      title: 'Agent Name',
      dataIndex: ['agent_name'],
    },
    {
      title: 'Agent Level',
      dataIndex: ['agent_level'],
    },
    {
      title: 'Agent Branch',
      dataIndex: 'agent_branch',
    },
    {
      title: 'Agent Representative',
      dataIndex: ['agent_representative'],
    },
    {
      title: 'Class of Bussines',
      dataIndex: 'class_of_business',
    },
    {
      title: 'Product',
      dataIndex: 'product',
    },
    {
      title: 'Tipe SPPA',
      dataIndex: 'sppa_type',
    },
    {
      title: 'SPPA No.',
      dataIndex: 'sppa_number',
    },
    {
      title: 'Insured Name',
      dataIndex: 'insured_name',
    },
    {
      title: 'Premi (IDR)',
      dataIndex: 'premi',
    },
    {
      title: 'SPPA Create Date',
      dataIndex: 'sppa_create_date',
    },
    {
      title: 'SPPA Status',
      dataIndex: 'sppa_status',
    },
  ]

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Report - SPPA</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24}>
          <Card className="card-box-filter">
            <form onSubmit={handleSearch}>
              <Row gutter={24}>
                <Col xs={12} md={12}>
                  <Form.Item className="mb-2">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.keyword}
                      placeholder="Search Agent ID / Profile ID / Name Agent..."
                      onChange={e => changeState({ ...state, keyword: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={12} xxl={9}>
                  <Form.Item className="mb-2" label="Periode">
                    <DatePicker.RangePicker
                      format="DD MMM YYYY"
                      value={!isEmpty(state.join_from_date) ? [moment(state.join_from_date), moment(state.join_to_date)] : ''}
                      onChange={(date) => {
                        changeState({
                          ...state,
                          join_from_date: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                          join_to_date: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Branch -"
                      className="w-100"
                      value={state.branch || undefined}
                      onChange={val => handleRepresentative(val)}
                    >
                      <Option value="">All Branch</Option>
                      {(state.branchList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Representative -"
                      className="w-100"
                      value={state.agent_representative || undefined}
                      disabled={state.branch === '' || state.branch === undefined}
                      onChange={val => changeState({ ...state, agent_representative: val })}
                    >
                      <Option value="">All Representative</Option>
                      {(state.agentRepresentList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Class of Business -"
                      className="w-100"
                      value={state.cob || undefined}
                      onChange={val => handleProduct(val)}
                    >
                      <Option value="">All COB</Option>
                      {(state.cobList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Product -"
                      className="w-100"
                      disabled={state.cob === '' || state.cob === undefined}
                      value={state.product || undefined}
                      onChange={val => changeState({ ...state, product: val })}
                    >
                      <Option value="">All Product</Option>
                      {(state.productList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{(key.display_name || '').toUpperCase()}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24} align="end">
                  <Button type="primary" className="button-lg px-4" htmlType="submit">
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Card>
        </Col>
      </Row>
      <Row gutter={24} className="mt-5">
        <Col md={{ span: 5, offset: 19 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={handleReport}
          >
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          dataSource={sppaReport.list.data}
          loading={sppaReport.list.isFetching}
          columns={columns}
          pagination={{
            position: ['bottomRight'],
            current: sppaReport.list.pagination.current_page,
            total: sppaReport.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </React.Fragment>
  )
}

ApprovalReport.propTypes = {
  sppaReport: PropTypes.any,
  state: PropTypes.object,
  changeState: PropTypes.func,
  handleTableChange: PropTypes.func,
  handleSearch: PropTypes.func,
  handleReport: PropTypes.func,
  handleRepresentative: PropTypes.func,
  handleProduct: PropTypes.func,
}

export default ApprovalReport
