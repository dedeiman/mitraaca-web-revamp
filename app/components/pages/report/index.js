import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import AgentList from 'containers/pages/report/AgentList'
import ContestWinner from 'containers/pages/report/ContestWinner'
import RestrictedProduct from 'containers/pages/report/RestrictedProduct'
import TrainingReport from 'containers/pages/report/Training'
import CommisionReport from 'containers/pages/report/DetailCommision'
import ExpiredPolicy from 'containers/pages/report/ExpiredPolicy'
import LossBusinessReport from 'containers/pages/report/Production/LossBusiness'
import ProductionReport from 'containers/pages/report/Production/Production'
import ClaimSettle from 'containers/pages/report/Production/ClaimSettle'
import OutstandingClaim from 'containers/pages/report/Production/OutstandingClaim'
import CareerReport from 'containers/pages/report/Finance/Career'
import BenefitYearly from 'containers/pages/report/Finance/BenefitYearly'
import BenefitMonthly from 'containers/pages/report/Finance/BenefitMonthly'
import FinanceCommission from 'containers/pages/report/Finance/FinanceCommission'
import Approval from 'containers/pages/report/Approval'
import SPPAReport from 'containers/pages/report/SPPA'
import history from 'utils/history'

const ReportRoutes = ({ currentUser, match, location, stateCheck }) => {
  if (location.pathname.includes('/report-administrator-agent-list')) {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-administrator-agent-list') > -1) {
      return <AgentList />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-administrator-agent-contest-winner') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-administrator-agent-contest-winner') > -1) {
      return <ContestWinner match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-administrator-agent-restricted-product') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-administrator-agent-restricted-product') > -1) {
      return <RestrictedProduct match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-training-list') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-training-list') > -1) {
      return <TrainingReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-administrator-approval') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-administrator-approval') > -1) {
      return <Approval match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-production-sppa') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-production-sppa') > -1) {
      return <SPPAReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-agent-commission-detail') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-agent') > -1) {
      return <CommisionReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-production-loss-business') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-production-loss-business') > -1) {
      return <LossBusinessReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-finance-career') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-finance-career') > -1) {
      return <CareerReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-finance-benefit-yearly') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-finance-benefit-yearly') > -1) {
      return <BenefitYearly match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-finance-benefit-monthly') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-finance-benefit-monthly') > -1) {
      return <BenefitMonthly match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-finance-commission') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-finance-commission') > -1) {
      return <FinanceCommission match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-production-list') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-production-list') > -1) {
      return <ProductionReport match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-production-claim-settle') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-production-claim-settle') > -1) {
      return <ClaimSettle match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-production-outstanding-claim') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-production-outstanding-claim') > -1) {
      return <OutstandingClaim match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/report-agent-expired-policy') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('report-agent-expired-policy') > -1) {
      return <ExpiredPolicy match={match} />
    }
    return <Forbidden />
  }

  return ''
}

ReportRoutes.propTypes = {
  location: PropTypes.object,
  currentUser: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default ReportRoutes
