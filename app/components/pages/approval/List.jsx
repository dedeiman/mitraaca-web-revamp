import React from 'react'
import PropTypes from 'prop-types'
import {
    Row, Col, Input,
    Card, Descriptions,
    Button, Select,
    Pagination, Empty,
    Divider, DatePicker,
} from 'antd'
import { Form } from '@ant-design/compatible'
import moment from 'moment'
import { isEmpty } from 'lodash'
import history from 'utils/history'

export default function List({
                                 stateApproval, handleFilter, currentUser,
                                 handlePage, downloadApproval, loadPolicy,
                             }) {
    const isMobile = window.innerWidth < 768
    return (
        <>
            <Row gutter={[24, 24]}>
                <Col sm={24} md={17} lg={19} xl={21}>
                    {(currentUser.permissions && currentUser.permissions.indexOf('approval-search') > -1) && (
                        <Input
                            allowClear
                            placeholder="Search SPPA / Policy Number..."
                            className="field-lg w-100"
                            value={stateApproval.search}
                            onChange={e => handleFilter('search', (e.target.value || '').toUpperCase())}
                            onPressEnter={() => loadPolicy(true)}
                        />
                    )}
                </Col>
                <Col sm={24} md={24} lg={10}>
                    {(currentUser.permissions && currentUser.permissions.indexOf('approval-filter') > -1) && (
                        <Form.Item label="Status SPPA" className="mb-0">
                            <Select
                                onChange={e => handleFilter('status', e)}
                                defaultValue="need-approval"
                                value={stateApproval.status || undefined}
                                loading={false}
                                placeholder="Select Status"
                                className="field-lg w-100"
                            >
                                <Select.Option key="0" value="all">All</Select.Option>
                                <Select.Option key="1" value="need-approval">Need Approval</Select.Option>
                                <Select.Option key="2" value="complete">Approved</Select.Option>
                                <Select.Option key="3" value="pending">Pending</Select.Option>
                                <Select.Option key="4" value="rejected">Reject</Select.Option>
                            </Select>
                        </Form.Item>
                    )}
                </Col>
                <Col sm={24} md={24} lg={10}>
                    {(currentUser.permissions && currentUser.permissions.indexOf('approval-filter') > -1) && (
                        <Form.Item label="SPPA Date" className="mb-0">
                            <DatePicker
                                placeholder="SPPA Date"
                                loading={stateApproval.load}
                                className="field-lg w-100"
                                format="DD MMMM YYYY"
                                value={!isEmpty(stateApproval.date) ? moment(stateApproval.date) : ''}
                                onChange={e => handleFilter('date', !isEmpty(e) ? e.format('YYYY-MM-DD') : undefined)}
                            />
                        </Form.Item>
                    )}
                </Col>
                {(currentUser.permissions && currentUser.permissions.indexOf('approval-search') > -1) && (
                    <Col sm={24} md={8} xl={4}>
                        <Button type="primary" className="button-lg px-4" onClick={() => loadPolicy(true)}>
                            Search
                        </Button>
                    </Col>
                )}
            </Row>
            <Row gutter={24} justify="end" className="my-4">
                <Col align="end">
                    {(currentUser.permissions && currentUser.permissions.indexOf('approval-export-excel') > -1) && (
                    <Button
                        ghost
                        type="primary"
                        className="align-items-center btn-border-mitra"
                        onClick={downloadApproval}
                    >
                        Download Approval
                    </Button>
                    )}
                </Col>
            </Row>
            <Card loading={stateApproval.loading}>
                {stateApproval.list.map(item => (
                    <>
                        <Row gutter={24} key={`list-approval-${item.id}`}>
                            <Col xs={24} md={18} xl={12}>
                                <Descriptions layout="vertical" colon={false} column={2}>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="SPPA Number" className="px-1">{item.sppa_number || '-'}</Descriptions.Item>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="Product" className="px-1">{item.product.display_name || ''}</Descriptions.Item>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="Agent" className="px-1">{item.creator.name.toUpperCase() || '-'}</Descriptions.Item>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="Periode Polis" className="px-1">
                                        {item.start_period ? moment(item.start_period).format('DD MMM YYYY') : '-'}
                                        {' '}
                                        -
                                        {' '}
                                        {item.end_period ? moment(item.end_period).format('DD MMM YYYY') : '-'}
                                    </Descriptions.Item>
                                </Descriptions>
                            </Col>
                            <Col xs={24} md={6} xl={6}>
                                <Descriptions layout="vertical" colon={false} column={1}>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="Insured Name" className="px-1">{item.insured.name.toUpperCase() || '-'}</Descriptions.Item>
                                    <Descriptions.Item span={isMobile ? 2 : 1} label="Status SPPA" className="px-1">
                                        { item.status == 'complete' ? 'APPROVED' : item.status.toUpperCase()}
                                    </Descriptions.Item>
                                </Descriptions>
                            </Col>
                            <Col xs={24} md={12} xl={6}>
                                <Button
                                    type="primary"
                                    className={`d-flex align-items-center mt-2 ${isMobile ? 'w-100' : ''}`}
                                    style={{
                                        paddingLeft: 24,
                                        paddingRight: 24,
                                    }}
                                    disabled={false}
                                    onClick={() => history.push(`/approval-list-menu/${item.id}/history`)}
                                >
                                    History
                                    <i style={{ fontSize: '18px' }} className="ml-2 las la-sync" />
                                </Button>
                                <Button
                                    type="primary"
                                    className={`d-flex align-items-center mt-2 ${isMobile ? 'w-100' : ''}`}
                                    disabled={(item.status === 'rejected' || item.status === 'approved' || item.status === 'expired') || item.flag_approval}
                                    onClick={() => history.push(`/approval-list-menu/${item.id}/approved`)}
                                >
                                    Approval
                                    <i style={{ fontSize: '18px' }} className="ml-2 las la-thumbs-up" />
                                </Button>
                            </Col>
                        </Row>
                        <Divider />
                    </>
                ))}
                {stateApproval.list.length === 0
                && (
                    <Empty
                        description="Tidak ada data."
                    />
                )
                }
            </Card>
            {stateApproval.list.length !== 0
            && (
                <Pagination
                    className="py-3"
                    showTotal={() => 'Page'}
                    current={stateApproval.page ? Number(stateApproval.page.current_page) : 1}
                    onChange={handlePage}
                    total={stateApproval.page.total_count}
                    pageSize={stateApproval.page.per_page || 10}
                />
            )
            }
        </>
    )
}

List.propTypes = {
    currentUser: PropTypes.object,
    stateApproval: PropTypes.object,
    handlePage: PropTypes.func,
    loadPolicy: PropTypes.func,
    handleFilter: PropTypes.func,
    downloadApproval: PropTypes.func,
}
