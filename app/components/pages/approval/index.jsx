import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/approval/List'
import History from 'containers/pages/approval/History'
import ApprovalPolicy from 'containers/pages/approval/Approval'
import history from 'utils/history'

const Approval = ({ currentUser, location, match, stateCheck }) => {
  if (location.pathname === '/approval-list-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('approval-list-menu') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }

  if (match.params.id && location.pathname.includes('/history')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('approval-list') > -1) {
      return <History location={location} match={match} />
    }
    return <Forbidden />
  }

  if (match.params.id && location.pathname.includes('/approved')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('approval-approve') > -1) {
      return <ApprovalPolicy location={location} match={match} />
    }
    return <Forbidden />
  }
  // if (match.params.id && !location.pathname.includes('/edit')) {
  // if (groupRole.code === BRANCH_CODE) {
  //   return <Forbidden />
  // }
  //   return <Detail match={match} />
  // }
  // if (location.pathname.includes('/edit') || (location.pathname === '/materi/add')) {
  //   if (groupRole.code === AGENT_CODE) {
  //     return <Forbidden />
  //   }
  //   return <Form match={match} />
  // }

  return ''
}

Approval.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Approval
