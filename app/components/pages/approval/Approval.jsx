import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Tag,
  Card, Descriptions,
  Button, Radio, Input,
} from 'antd'
import { LoadingOutlined, LeftOutlined } from '@ant-design/icons'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'

import history from 'utils/history'

export default function Approvals({
  form,
  onSubmit,
  detailHistory,
  loadingApprove,
}) {
  const {
    getFieldDecorator,
  } = form

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Approval</div>
        </Col>
      </Row>
      <Row gutter={24} className="mb-5">
        <Col xs={24}>
          <Card className="h-100" loading={detailHistory.loading}>
            <div style={{
              color: '#2b57b7',
              fontSize: '18px',
              fontWeight: '600',
            }}
            >
              Detail
            </div>
            { !detailHistory.loading && (
              <Descriptions layout="vertical" colon={false} column={5}>
                <Descriptions.Item label="Agent" className="px-1">
                  {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.creator.name.toUpperCase() : ''}
                </Descriptions.Item>
                <Descriptions.Item label="Cabang" className="px-1">
                  {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.creator.branch.name.toUpperCase() : ''}
                </Descriptions.Item>
                <Descriptions.Item label="Nomor SPPA" className="px-1">
                  {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.sppa_number : ''}
                </Descriptions.Item>
                <Descriptions.Item label="Produk" className="px-1">
                  {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.product.name.toUpperCase() : ''}
                </Descriptions.Item>
                <Descriptions.Item label="Approval For" className="px-1">
                  {!isEmpty(detailHistory.listDetailHistory.approval_for_display) ? (detailHistory.listDetailHistory.approval_for_display.map((item, idx) => (
                    <p>
                      {idx + 1}
                      .
                      {' '}
                      {item.toUpperCase()}
                    </p>
                  ))) : '' }
                </Descriptions.Item>
              </Descriptions>
            )}
          </Card>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24}>
          <Form onSubmit={onSubmit}>
            <Card className="h-100 mb-4">
              <div style={{
                color: '#2b57b7',
                fontSize: '18px',
                fontWeight: '600',
              }}
              >
                Approval
              </div>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <p className="mb-0 mt-1">Persetujuan</p>
                  <Form.Item>
                    {getFieldDecorator('approval', {
                      rules: Helper.fieldRules(['required'], 'Persetujuan'),
                    })(
                      <Radio.Group
                        size="large"
                        className="w-100"
                      >
                        <Radio value="approve">Approve</Radio>
                        <Radio value="pending">Pending</Radio>
                        <Radio value="rejected">Reject</Radio>
                      </Radio.Group>,
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} md={12}>
                  <p className="mb-1">Reason</p>
                  <Form.Item>
                    {getFieldDecorator('reason', {
                      rules: [
                        ...Helper.fieldRules(['required'], 'Reason'),
                        { pattern: /^[a-zA-Z1-9., ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                        { pattern: /^.{10,}$/, message: '*Minimal 10 karakter' },
                      ],
                    })(
                      <Input
                        style={{ textTransform: 'uppercase' }}
                        placeholder="Reason"
                        className="field-lg uppercase"
                      />,
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Card>
            <Row className="mt-3 mb-3">
              <Col span={24} className="d-flex justify-content-end align-items-center">
                <Button
                  type="primary"
                  htmlType="submit"
                  className="button-lg w-25 mr-3"
                >
                  {'Save'}
                  {loadingApprove && <LoadingOutlined className="mr-2" />}
                </Button>
                <Button
                  ghost
                  type="primary"
                  onClick={() => history.goBack()}
                  className="button-lg w-25 border-lg"
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </React.Fragment>
  )
}

Approvals.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  loadingApprove: PropTypes.bool,
  detailHistory: PropTypes.object,
}
