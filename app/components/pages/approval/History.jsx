import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Tag,
  Card, Descriptions,
  Divider, Empty, Button,
} from 'antd'
import moment from 'moment'
import history from 'utils/history'
import { LeftOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'

export default function History({
  dataHistory,
  detailHistory,
}) {
  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">History</div>
        </Col>
      </Row>
      <Row gutter={24} className="mb-5">
        <Col xs={24}>
          <Card className="h-100" loading={detailHistory.loading}>
            { !detailHistory.loading && (
            <Descriptions layout="vertical" colon={false} column={5}>
              <Descriptions.Item label="Agent" className="px-1">
                {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.creator.name.toUpperCase() : ''}
              </Descriptions.Item>
              <Descriptions.Item label="Cabang" className="px-1">
                {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.creator.branch.name.toUpperCase() : ''}
              </Descriptions.Item>
              <Descriptions.Item label="Nomor SPPA" className="px-1">
                {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.sppa_number : ''}
              </Descriptions.Item>
              <Descriptions.Item label="Produk" className="px-1">
                {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.product.name.toUpperCase() : ''}
              </Descriptions.Item>
              <Descriptions.Item label="Policy Number" className="px-1">
                {!isEmpty(detailHistory.listDetailHistory) ? detailHistory.listDetailHistory.policy_number.toUpperCase() : ''}
              </Descriptions.Item>
            </Descriptions>
            )}
          </Card>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24}>
          <Card className="h-100" loading={detailHistory.loading}>
            { !dataHistory.loading && (
              dataHistory.listHistory.map(item => (
                <>
                  <Descriptions layout="vertical" colon={false} column={4}>
                    <Descriptions.Item label="Approval By" className="px-1">
                      {!isEmpty(item.creator) ? item.creator.name.toUpperCase() : ''}
                    </Descriptions.Item>
                    <Descriptions.Item label="Time" className="px-1">
                      {moment.utc(item.created_at).format('HH : mm')}
                    </Descriptions.Item>
                    <Descriptions.Item label="Approval For" className="px-1">
                      {!isEmpty(item.approval_for_display) ? (item.approval_for_display.map((approveFor, idx) => (
                        <p>
                          {idx + 1}
                          .
                          {' '}
                          {approveFor.toUpperCase()}
                        </p>
                      ))) : '' }
                    </Descriptions.Item>
                    <Descriptions.Item label="Reason" className="px-1">
                      {item.reason.toUpperCase() || '-'}
                    </Descriptions.Item>
                    <Descriptions.Item label="Date" className="px-1">
                      {moment(item.created_at).format('DD MMM YYYY')}
                    </Descriptions.Item>
                    <Descriptions.Item label="Result" className="px-1">
                      {item.approval.toUpperCase() || '-'}
                    </Descriptions.Item>
                  </Descriptions>
                  <Divider />
                </>
              ))
            )}

            {dataHistory.listHistory.length === 0
              && (
                <Empty
                  description="Tidak ada data."
                />
              )
            }
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  )
}

History.propTypes = {
  dataHistory: PropTypes.object,
  detailHistory: PropTypes.object,
}
