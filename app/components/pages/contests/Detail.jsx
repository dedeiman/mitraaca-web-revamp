import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Avatar, Modal,
  Card, Popconfirm, Tag,
  Descriptions, Typography,
  Row, Col, Divider, Empty,
} from 'antd'
import {
  StopOutlined, DownCircleOutlined, LeftOutlined, PlusCircleOutlined,
} from '@ant-design/icons'
import AddWinner from 'containers/pages/contests/AddWinner'
import moment from 'moment'
import history from 'utils/history'

const DetailContest = ({
  isFetching, detailContest,
  location, handleCancelWinner,
  stateModal, setStateModal,
  currentUser,
}) => {
  const isWinner = location.pathname.includes('/winner')
  const element = []

  if (isWinner) {
    if (currentUser.permissions && !(currentUser.permissions.indexOf('contest-detail') > -1)) {
      element.push(
        <Col span={24}>
          <Card>
            <Empty description="Tidak ada Data" />
          </Card>
        </Col>,
      )
    } else {
      (detailContest.rewards || []).map(item => element.push(
        <Col xs={24} md={12} key={`reward_${item.id}`}>
          <Card className="h-100" loading={isFetching}>
            <Typography.Paragraph ellipsis={{ rows: 1 }} className="title-card">
              {`${detailContest.contest_id} - ${detailContest.name}`}
              <Button
                type="primary"
                className="d-flex align-items-center float-right"
                onClick={() => setStateModal({
                  ...stateModal, visibleAdd: true, idRewards: item.reward_id, idContest: detailContest.id,
                })}
              >
                Add
                <PlusCircleOutlined />
              </Button>
            </Typography.Paragraph>
            <p className="mb-0">Reward</p>
            <p className="font-weight-bold mb-5">{`${item.reward_id} - ${item.name}`}</p>
            {(item.winners || []).map((winner, idx) => (
              <React.Fragment key={`winner_${winner.id}`}>
                <Row gutter={12} type="flex">
                  <Col xs={1} md={1}>
                    {`${idx + 1}.`}
                  </Col>
                  <Col xs={23} md={17}>
                    <Descriptions layout="vertical" colon={false} column={3}>
                      <Descriptions.Item span={2} label="Agent" className="px-1">
                        {`${winner.agent_id} - ${winner.agent.name}`}
                      </Descriptions.Item>
                      <Descriptions.Item span={1} label="Qty" className="px-1">
                        {winner.quantity || 0}
                      </Descriptions.Item>
                      <Descriptions.Item span={3} label="Remarks" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                          {winner.remarks}
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} className="d-flex align-items-end">
                    {(currentUser.permissions && currentUser.permissions.indexOf('contest-winner-cancel') > -1) && (
                      <Popconfirm
                        title="Anda akan menghapus data ini?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={() => handleCancelWinner({ agentId: winner.agent_id, rewardId: item.reward_id, uuid: detailContest.id })}
                      >
                        <Button type="primary">
                          Cancel
                          <StopOutlined />
                        </Button>
                      </Popconfirm>
                    )}
                  </Col>
                </Row>
                <Divider />
              </React.Fragment>
            ))}
          </Card>
        </Col>,
      ))
    }
  } else {
    element.push(
      <React.Fragment>
        <Col span={24} className="mb-4">
          <Avatar
            size={250}
            shape="square"
            className="w-100 img-contain shadow"
            src={detailContest.banner_url || '/assets/avatar-on-error.jpg'}
          />
        </Col>
        <Col xs={24} md={12}>
          <Card className="h-100">
            <p className="title-card mb-4">Rewards</p>
            {(detailContest.rewards || []).map(item => (
              <React.Fragment key={`descRew_${Math.random()}`}>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item label="Reward ID" className="px-1">{item.reward_id || '(auto generate)'}</Descriptions.Item>
                  <Descriptions.Item label="Reward Name" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                      {item.name || '-'}
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item label="Reward Description" className="px-1" span={2}>
                    <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                      {item.description || '-'}
                    </Typography.Paragraph>
                  </Descriptions.Item>
                </Descriptions>
                <Divider />
              </React.Fragment>
            ))}
          </Card>
        </Col>
        <Col xs={24} md={12}>
          <Card className="h-100">
            <p className="title-card mb-5">Contest Details</p>
            <Descriptions layout="vertical" colon={false} column={1}>
              <Descriptions.Item label="Contest ID" className="px-1">
                <p className="mb-3">{detailContest.contest_id || '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Contest Name" className="px-1">
                <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-3">
                  {detailContest.name || '-'}
                </Typography.Paragraph>
              </Descriptions.Item>
              <Descriptions.Item label="Periode" className="px-1">
                <p className="mb-3">
                  {`${
                    detailContest.start_date ? moment(detailContest.start_date).format('DD MMM YYYY') : '-'
                  } - ${
                    detailContest.end_date ? moment(detailContest.end_date).format('DD MMM YYYY') : '-'
                  }`}
                </p>
              </Descriptions.Item>
              <Descriptions.Item label="Descriptions" className="px-1">
                <Typography.Paragraph
                  ellipsis={{
                    rows: 5,
                    expandable: true,
                    symbol: (
                      <p className="text-center text-primary pt-2">
                        Lihat Selengkapnya
                        {' '}
                        <DownCircleOutlined />
                      </p>
                    ),
                  }}
                  className="mb-3"
                >
                  {detailContest.description || '-'}
                </Typography.Paragraph>
              </Descriptions.Item>
            </Descriptions>
          </Card>
        </Col>
      </React.Fragment>,
    )
  }
  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{isWinner ? 'View Winners' : detailContest.name}</div>
        </Col>
      </Row>
      <Row gutter={[24, 24]} className="mb-5">
        {element}

        <Modal
          title={<p className="mb-0 title-card">Add Winner</p>}
          visible={stateModal.visibleAdd}
          footer={null}
          closable={false}
          wrapClassName="bg-transparent"
          onCancel={() => setStateModal({ ...stateModal, visibleAdd: false })}
        >
          <AddWinner
            stateModal={stateModal}
            toggle={() => {
              setStateModal({ ...stateModal, visibleAdd: false })
            }}
          />
        </Modal>
      </Row>
    </React.Fragment>
  )
}

DetailContest.propTypes = {
  detailContest: PropTypes.object,
  isFetching: PropTypes.bool,
  location: PropTypes.object,
  handleCancelWinner: PropTypes.func,
  stateModal: PropTypes.object,
  setStateModal: PropTypes.func,
  currentUser: PropTypes.object,
}

export default DetailContest
