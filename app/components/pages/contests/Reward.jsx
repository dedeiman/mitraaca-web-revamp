import PropTypes from 'prop-types'
import {
  Row, Col,
  Input, Button,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

const FormReward = ({
  data, toggle,
  form, onSubmit,
}) => {
  const { getFieldDecorator } = form
  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <p className="mb-0">Contest</p>
          <p className="font-weight-bold">{data.contest || '-'}</p>
        </Col>
        <Col span={24}>
          <p className="mb-0">Reward ID</p>
          <p className="font-weight-bold">{data.reward_id || '-'}</p>
        </Col>
        <Col span={24}>
          <p className="mb-0">Reward Name</p>
          <Form.Item>
            {getFieldDecorator('name', {
              rules: [
                ...Helper.fieldRules(['required', 'Name']),
                { min: 3, message: '*Minimal 3 karakter\n' },
                { pattern: /^[a-zA-Z0-9 ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
              ],
              initialValue: data.name ? (data.name).toUpperCase() : '',
              getValueFromEvent: e => e.target.value,
            })(
              <Input
                placeholder="Input Name uppercase"
                className="field-lg uppercase"
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={24}>
          <p className="mb-0">Reward Description</p>
          <Form.Item>
            {getFieldDecorator('description', {
              rules: [
                ...Helper.fieldRules(['required'], 'Description'),
                { min: 3, message: '*Minimal 3 karakter\n' },
                { max: 200, message: '*Maksimal 200 karakter\n' },
                { pattern: /^[a-zA-Z0-9,?\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
              ],
              initialValue: data.description ? (data.description).toUpperCase() : '',
              getValueFromEvent: e => e.target.value,
            })(
              <Input.TextArea
                rows={4}
                className="field-lg uppercase"
                placeholder="Input Description"
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            type="primary"
            className="mr-2"
            htmlType="submit"
          >
            Submit
          </Button>
          <Button
            ghost
            type="primary"
            className="border-lg"
            onClick={() => toggle()}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}
FormReward.propTypes = {
  data: PropTypes.object,
  toggle: PropTypes.func,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
}

export default FormReward
