import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Modal,
  Button, Avatar,
  Card, Input, Upload,
  DatePicker, Checkbox,
  Descriptions, Divider,
  Typography, Tag,
} from 'antd'
import { Form} from '@ant-design/compatible'
import { InboxOutlined, LeftOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import FormReward from 'containers/pages/contests/Reward'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const FormFAQ = ({
  isFetching, detailContest,
  onSubmit, participants,
  form, match, banner,
  stateRewards, setStateRewards,
  handleUpload, removeItem,
}) => {
  const isMobile = window.innerWidth < 768
  const { getFieldDecorator, getFieldValue } = form

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center mt-3">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Add'} Contest`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24}>
          <Col xs={24} md={13}>
            <Row gutter={0} className="h-100">
              <Col span={24} className="pb-4">
                <Card className="h-100">
                  <p className="title-card">Contest Banner</p>
                  <Form.Item>
                    {getFieldDecorator('banner_file', {
                      rules: Helper.fieldRules(match.params.id ? [] : ['required'], 'Contest Banner'),
                      getValueFromEvent: (info) => {
                        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'
                        if (!isFileType) return undefined
                        return info
                      },
                    })(
                      <Upload
                        accept="image/*"
                        name="icon"
                        listType="picture-card"
                        className="avatar-uploader banner-content contest-upload"
                        showUploadList={false}
                        beforeUpload={() => false}
                        onChange={info => handleUpload(info)}
                      >
                        {banner
                          ? (
                            <Avatar src={banner} shape="square" className="banner-preview" />
                          )
                          : (
                            <div>
                              <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                              </p>
                              <p className="ant-upload-text">Upload Banner</p>
                            </div>
                          )
                        }
                      </Upload>,
                    )}
                  </Form.Item>
                </Card>
              </Col>
              <Col span={24}>
                <Card className="h-100">
                  <div className="d-flex justify-content-between mb-5">
                    <p className="title-card mb-0">Rewards</p>
                    <Button
                      ghost
                      type="primary"
                      className="border-lg d-flex align-items-center"
                      onClick={() => (
                        setStateRewards({
                          ...stateRewards,
                          visible: true,
                          field: { contest: getFieldValue('name') },
                          isEdit: false,
                        })
                      )}
                    >
                      <img src="/assets/plus.svg" alt="plus" className="mr-1" />
                      Add Rewards
                    </Button>
                  </div>
                  {stateRewards.error && <p className="text-danger">{stateRewards.error}</p>}
                  {stateRewards.list.length
                    ? stateRewards.list.map((item, idx) => (
                      <React.Fragment key={`descRew_${Math.random()}`}>
                        <Descriptions layout="vertical" colon={false} column={2}>
                          <Descriptions.Item label="Reward ID" className="px-1">{item.reward_id || '(auto generate)'}</Descriptions.Item>
                          <Descriptions.Item label="Reward Name" className="px-1">
                            <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                              {item.name || '-'}
                            </Typography.Paragraph>
                          </Descriptions.Item>
                          <Descriptions.Item label="Reward Description" className="px-1" span={2}>
                            <Typography.Paragraph
                              ellipsis={{ rows: 3 }}
                              className="mb-0"
                            >
                              {item.description || '-'}
                            </Typography.Paragraph>
                          </Descriptions.Item>
                        </Descriptions>
                        <Button
                          type="link"
                          className="d-flex align-items-center px-0 float-right"
                          onClick={() => (
                            setStateRewards({
                              ...stateRewards,
                              visible: true,
                              field: { ...item, contest: getFieldValue('name') },
                              isEdit: idx,
                            })
                          )}
                        >
                          <i className="las la-edit mr-2" style={{ fontSize: '20px' }} />
                          Edit
                        </Button>
                        <Button
                          type="link"
                          className="d-flex align-items-center px-0 float-right mr-3"
                          onClick={() => removeItem(isEmpty(item.id) ? item.idx : item.id)}
                        >
                          <i className="las la-trash-alt mr-2" style={{ fontSize: '20px' }} />
                          Delete
                        </Button>
                        <Divider />
                      </React.Fragment>
                    ))
                    : 'Tidak ada Data'
                  }
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={11}>
            <Card className="h-100">
              <p className="title-card">Contest Detail</p>
              <p className="font-weight-bold">Contest ID</p>
              <Form.Item>
                {getFieldDecorator('contest_id', {
                  initialValue: detailContest.contest_id || '',
                })(
                  <Input
                    className="field-lg"
                    placeholder="Auto Generate"
                    disabled
                  />,
                )}
              </Form.Item>
              <p className="font-weight-bold">Contest Name</p>
              <Form.Item>
                {getFieldDecorator('name', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Contest Name'),
                    { pattern: /^[a-zA-Z0-9 ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                  ],
                  initialValue: detailContest.name || '',
                  getValueFromEvent: e => e.target.value,
                })(
                  <Input
                    className="field-lg uppercase"
                    placeholder="Input Contest Name"
                  />,
                )}
              </Form.Item>
              <p className="font-weight-bold">Periode</p>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <Form.Item>
                    {getFieldDecorator('start_date', {
                      rules: Helper.fieldRules(['required'], 'Start Date'),
                      initialValue: detailContest.start_date ? moment(detailContest.start_date) : undefined,
                    })(
                      <DatePicker
                        className="field-lg w-100"
                        format="DD MMMM YYYY"
                        placeholder="Start Date"
                        disabledDate={current => (current && (current < moment().subtract(1, 'day').endOf('day')))}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} md={12}>
                  <Form.Item>
                    {getFieldDecorator('end_date', {
                      rules: Helper.fieldRules(['required'], 'End Date'),
                      initialValue: detailContest.end_date ? moment(detailContest.end_date) : undefined,
                    })(
                      <DatePicker
                        className="field-lg w-100"
                        format="DD MMMM YYYY"
                        placeholder="End Date"
                        disabled={!getFieldValue('start_date')}
                        disabledDate={current => (current && (current < moment(getFieldValue('start_date'))))}
                      />,
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <p className="font-weight-bold">Participant</p>
              <Form.Item>
                {getFieldDecorator('participant_level_ids', {
                  rules: Helper.fieldRules(['required'], 'Participant'),
                  initialValue: !isEmpty(detailContest.participant_levels) ? detailContest.participant_levels.map(item => item.id) : [],
                })(
                  <Checkbox.Group>
                    <Row gutter={[12, 12]}>
                      {participants.list.map(item => (
                        <Col xs={24} md={12} key={`chkb_${item.id}`}>
                          <Checkbox value={item.id}>{item.name}</Checkbox>
                        </Col>
                      ))}
                    </Row>
                  </Checkbox.Group>,
                )}
              </Form.Item>
              <p className="font-weight-bold">Description</p>
              <Form.Item>
                {getFieldDecorator('description', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Description'),
                    { max: 200, message: '*Maksimal 200 karakter\n' },
                    { min: 3, message: '*Minimal 3 karakter\n' },
                    { pattern: /^[a-zA-Z0-9,?\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
                  ],
                  initialValue: detailContest.description || '',
                  getValueFromEvent: e => e.target.value,
                })(
                  <Input.TextArea
                    rows={3}
                    className="field-lg uppercase"
                    placeholder="Input Description"
                  />,
                )}
              </Form.Item>
            </Card>
          </Col>
        </Row>
        <Row className="my-3">
          <Col span={24} className="d-flex justify-content-end align-items-center mb-3">
            <Button
              type="primary"
              htmlType="submit"
              className={`button-lg mr-3 ${isMobile ? 'w-100' : ''}`}
            >
              {match.params.id ? 'Save' : 'Submit'}
            </Button>
            <Button
              ghost
              type="primary"
              className={`button-lg ${isMobile ? 'w-100' : ''}`}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>
      <Modal
        title={<p className="mb-0 title-card">Add Reward</p>}
        visible={stateRewards.visible}
        footer={null}
        closable={false}
        onCancel={() => setStateRewards({ ...stateRewards, visible: false, field: {} })}
        wrapClassName="bg-transparent"
      >
        <FormReward
          list={stateRewards.list}
          data={stateRewards.field}
          isEdit={stateRewards.isEdit}
          toggle={() => (
            setStateRewards({
              ...stateRewards,
              visible: false,
              field: {},
              isEdit: false,
            })
          )}
          submit={list => (
            setStateRewards({
              ...stateRewards,
              list,
              visible: false,
              field: {},
              isEdit: false,
            })
          )}
        />
      </Modal>
    </React.Fragment>
  )
}

FormFAQ.propTypes = {
  detailContest: PropTypes.object,
  isFetching: PropTypes.bool,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  participants: PropTypes.object,
  match: PropTypes.object,
  banner: PropTypes.string,
  stateRewards: PropTypes.object,
  setStateRewards: PropTypes.func,
  handleUpload: PropTypes.func,
  removeItem: PropTypes.func,
}

export default FormFAQ
