import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Input, Upload,
  AutoComplete, DatePicker,
  Select,
} from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import moment from 'moment'
import config from 'app/config'

const UploadContest = ({
  isFetching, handleSearch,
  stateContest, handleSelect,
  handleSheetChange, handleUpload,
  setStateContest,
}) => (
  <React.Fragment>
    <Row gutter={[24, 24]}>
      <Col span={24} className="d-flex justify-content-between align-items-center">
        <div className="title-page">Upload Winners</div>
      </Col>
    </Row>
    <Row gutter={[24, 24]}>
      <Col xs={24} md={24}>
        <Card className="h-100" loading={isFetching}>
          <p className="title-card">Contest ID</p>
          <AutoComplete
            options={(stateContest.list || []).map(item => ({ label: item.name, value: item.contest_id }))}
            onSearch={handleSearch}
            onSelect={handleSelect}
          >
            <Input.Search size="large" placeholder="Search Contest By ID..." />
          </AutoComplete>
        </Card>
      </Col>
      <Col xs={24} md={12}>
        <Card className="h-100" loading={isFetching}>
          <p className="title-card mb-4">Contest Details</p>
          <p className="font-weight-bold">Contest Name</p>
          <Input
            disabled
            value={stateContest.pick.name || ''}
            className="field-lg mb-4"
            placeholder="Contest Name"
          />
          <p className="font-weight-bold">Periode</p>
          <Row gutter={12}>
            <Col xs={24} md={11}>
              <DatePicker
                disabled
                placeholder="Start Date"
                className="field-lg w-100"
                value={stateContest.pick.start_date ? moment(stateContest.pick.start_date) : ''}
                format="DD MMMM YYYY"
              />
            </Col>
            <Col xs={1} md={2} className="d-flex align-items-center justify-content-center">to</Col>
            <Col xs={23} md={11}>
              <DatePicker
                disabled
                placeholder="End Date"
                className="field-lg w-100"
                value={stateContest.pick.end_date ? moment(stateContest.pick.end_date) : ''}
                format="DD MMMM YYYY"
              />
            </Col>
          </Row>
        </Card>
      </Col>
      <Col xs={24} md={12}>
        <Card className="h-100" loading={isFetching}>
          <Row gutter={24}>
            <Col xs={24} md={24} className="mb-4">
              <p className="title-card">File</p>
              <Upload
                accept=".xlsx, .xls"
                name="icon"
                listType="picture-card"
                className="avatar-uploader banner-content"
                beforeUpload={() => false}
                fileList={stateContest.fileList}
                showUploadList={{ showRemoveIcon: false, showPreviewIcon: false }}
                onChange={handleSheetChange}
                disabled={isEmpty(stateContest.pick)}
              >
                <div>
                  <p className="ant-upload-text mb-1">Taruh file di area ini</p>
                  <p className="ant-upload-text mb-1">atau</p>
                  <Button
                    ghost
                    size="small"
                    type="primary"
                    disabled={isEmpty(stateContest.pick)}
                  >
                    Upload Document
                  </Button>
                </div>
              </Upload>
            </Col>
            <Col xs={24} className="mb-4">
              Need a Template Document?
              <a
                className="link-download ml-1"
                href={`${config.api_url}/contests/download-template`}
                download
              >
                Download Here
              </a>
            </Col>
            <Col xs={24} md={24}>
              <p className="font-weight-bold">Work Sheets</p>
              <Select
                disabled={isEmpty(stateContest.file)}
                value={stateContest.selected}
                placeholder="Select Type"
                className="field-lg w-100"
                onSelect={(val) => {
                  setStateContest({
                    ...stateContest,
                    selected: val,
                  })
                }}
              >
                {(stateContest.sheets || []).map(item => (
                  <Select.Option key={item} value={item}>{item}</Select.Option>
                ))}
              </Select>
            </Col>
          </Row>
        </Card>
      </Col>

      <Col span={24} className="d-flex justify-content-end align-items-center">
        <Button
          type="primary"
          className="mr-2"
          disabled={isEmpty(stateContest.selected) || stateContest.isFetching}
          onClick={handleUpload}
        >
          {stateContest.isFetching && <LoadingOutlined className="mr-2" />}
          Save
        </Button>
      </Col>
    </Row>
  </React.Fragment>
)

UploadContest.propTypes = {
  isFetching: PropTypes.bool,
  handleSearch: PropTypes.func,
  stateContest: PropTypes.object,
  handleSelect: PropTypes.func,
  handleSheetChange: PropTypes.func,
  handleUpload: PropTypes.func,
  setStateContest: PropTypes.func,
}

export default UploadContest
