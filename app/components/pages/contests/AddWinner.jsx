import PropTypes from 'prop-types'
import {
  Row, Col, Input, Button,
  AutoComplete,
} from 'antd'
import { Form } from '@ant-design/compatible'

const AddWinner = ({
  form, onSubmit,
  stateSelect, toggle,
  handleSearch, list,
  stateModal,
}) => {
  const { getFieldDecorator, setFieldsValue } = form

  return (
    <Form colon={false}>
      <Row gutter={[24, 24]}>
        <Col xs={24}>
          <p className="mb-0">Agent ID</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('agent_id', {
              rules: [
                { pattern: /^.{3,}$/, message: 'Minimum length is 3' },
              ],
            })(
              <AutoComplete
                allowClear
                onSelect={val => setFieldsValue({
                  agent_id: val,
                  name: ((stateSelect.agentList || []).find(item => item.agent_id === val).name).toUpperCase(),
                  branch: (stateSelect.agentList || []).find(item => item.agent_id === val).branch.name,
                })}
                onSearch={handleSearch}
                loading={stateSelect.agentLoad}
                options={(stateSelect.agentList || []).map(item => ({ value: item.agent_id, label: `${(item.agent_id || '').toUpperCase()} - ${(item.name || '').toUpperCase()}`, disabled: ((list || []).map(agent => agent.agent_id)).includes(item.agent_id) }))}
              >
                <Input size="large" placeholder="Search Agent" className="uppercase" maxLength={10} />
              </AutoComplete>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Agent Name</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('name')(
              <Input
                disabled
                size="large"
                placeholder="Agent Name"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Branch</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('branch')(
              <Input
                disabled
                size="large"
                placeholder="Branch"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} className="pb-0">
          <div className="d-flex justify-content-end">
            <Button
              type="primary"
              htmlType="submit"
              onClick={(e) => {
                onSubmit(e, stateModal)
                setFieldsValue({ agent_id: '', name: '', branch: '' })
              }}
            >
              Add Winners
            </Button>
            <Button
              ghost
              type="primary"
              className="ml-2"
              onClick={() => {
                toggle()
                form.resetFields()
                setFieldsValue({ agent_id: '', name: '', branch: '' })
              }}
            >
              Cancel
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  )
}

AddWinner.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  stateSelect: PropTypes.object,
  toggle: PropTypes.func,
  handleSearch: PropTypes.func,
  list: PropTypes.array,
  stateModal: PropTypes.object,
}

export default AddWinner
