import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Result, Typography, Empty,
} from 'antd'
import { Loader } from 'components/elements'
import { InboxOutlined } from '@ant-design/icons'
import { AGENT_CODE, AGENCY_CODE } from 'constants/ActionTypes'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'
import moment from 'moment'

const ContestList = ({
  groupRole, dataContest,
  handlePage, metaContest,
  stateContest, setStateContest,
  handleFilter, loadContest,
  isFetching, updatePerPage,
  currentUser,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col xs={24} md={3}>
          <Select
            onChange={handleFilter}
            value={stateContest.filter}
            loading={stateContest.load}
            className="field-lg w-100"
          >
            {['name', 'year'].map(item => (
              <Select.Option key={item} value={item}>{capitalize(item)}</Select.Option>
            ))}
          </Select>
        </Col>
        <Col xs={24} md={18}>
          <Input
            allowClear
            placeholder={`Search by Contest ${capitalize(stateContest.filter)}...`}
            className="field-lg"
            value={stateContest.search}
            onChange={(e) => {
              setStateContest({
                ...stateContest,
                search: e.target.value,
              })
            }}
            onPressEnter={() => loadContest(true)}
          />
        </Col>
        <Col xs={24} md={3} align="end">
          <Button type="primary" className="button-lg px-4" onClick={() => loadContest(true)}>
            Search
          </Button>
        </Col>
      </Row>
      {(groupRole.code !== AGENT_CODE) && (
        <Row gutter={24} justify="end" className="my-4">
          <Col align="end">
            <Button
              ghost
              type="link"
              onClick={() => history.push('/contest-menu/upload')}
            >
              Upload Winners
            </Button>
          </Col>
          <Col align="end">
            <Button
              ghost
              type="primary"
              className="ml-auto border-lg d-flex align-items-center"
              onClick={() => history.push('/contest-menu/add')}
            >
              <img src="/assets/plus.svg" alt="plus" className="mr-1" />
              Add New Contest
            </Button>
          </Col>
        </Row>
      )}

      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataContest)) && (
          <Empty
            description="Tidak ada data."
          />
        )}
        {!isEmpty(dataContest)
          ? (
            dataContest.map((item, idx) => (
              <React.Fragment key={`list-faq-${item.id}`}>
                <Row gutter={24} key={item.id}>
                  <Col xs={24} md={6} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 1 : 1} label="Contest ID" className="px-1">{item.contest_id || '-'}</Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 1 : 1} label="Contest Name" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                            {item.name}
                          </Typography.Paragraph>
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={6}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 1 : 1} label="Periode" className="px-1">
                        {item.start_date ? moment(item.start_date).format('DD MMM YYYY') : '-'}
                        {' - '}
                        {item.end_date ? moment(item.end_date).format('DD MMM YYYY') : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 1 : 1} label="Level" className="px-1">
                        {item.participant_levels.map(level => <p className="m-0" key={`lvl_${level.id}`}>{level.name}</p>)}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 1 : 1} className="px-1 text-center desc-action">
                        <Row gutter={[24, 12]} className="pl-0 mr-0">
                          {(currentUser.permissions && currentUser.permissions.indexOf('contest-detail') > -1) && (
                            <React.Fragment>
                              <Col xs={24} xxl={12} className="p-0 mt-2">
                                <Button
                                  type="primary"
                                  className="button-lg d-flex justify-content-between align-items-center w-100"
                                  onClick={() => history.push(`/contest-menu/${item.id}`)}
                                >
                                  Details
                                  {' '}
                                  <i className="las la-share" />
                                </Button>
                              </Col>
                            </React.Fragment>
                          )}
                          {groupRole.code === AGENCY_CODE && (
                            <React.Fragment>
                              <Col xs={24} xxl={12} className="p-0 mt-2">
                                {(currentUser.permissions && currentUser.permissions.indexOf('contest-edit') > -1) && (
                                  <Button
                                    type="primary"
                                    className="button-lg d-flex justify-content-between align-items-center w-100"
                                    onClick={() => history.push(`/contest-menu/${item.id}/edit`)}
                                  >
                                    Edit
                                    {' '}
                                    <i className="las la-edit p-0" />
                                  </Button>
                                )}
                              </Col>
                              <Col xs={24} xxl={12} className="p-0 mt-2">
                                <Button
                                  type="primary"
                                  className="button-lg d-flex justify-content-between align-items-center w-100"
                                  onClick={() => history.push(`/contest-menu/${item.id}/winner`)}
                                >
                                  Winners
                                  {' '}
                                  <i className="las la-certificate p-0" />
                                </Button>
                              </Col>
                            </React.Fragment>
                          )}
                        </Row>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {(idx !== (dataContest.length - 1)) && (
                  <Divider />
                )}
              </React.Fragment>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      {
        !isEmpty(dataContest) && (
          <Pagination
            className="py-3"
            showTotal={() => 'Page'}
            simple={isMobile}
            pageSize={metaContest.per_page || 10}
            current={stateContest.page ? Number(stateContest.page) : 1}
            onChange={handlePage}
            total={metaContest.total_count}
            onShowSizeChange={updatePerPage}
            showSizeChanger={false}
          />
        )
      }
    </React.Fragment>
  )
}

ContestList.propTypes = {
  groupRole: PropTypes.object,
  dataContest: PropTypes.array,
  metaContest: PropTypes.object,
  stateContest: PropTypes.object,
  handlePage: PropTypes.func,
  handleFilter: PropTypes.func,
  loadContest: PropTypes.func,
  setStateContest: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  currentUser: PropTypes.object,
}

export default ContestList
