import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/contests/List'
import Detail from 'containers/pages/contests/Detail'
import Form from 'containers/pages/contests/Form'
import Upload from 'containers/pages/contests/Upload'
import history from 'utils/history'

const Contest = ({ currentUser, location, match, stateCheck }) => {
  if (location.pathname === '/contest-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('contest-list') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (match.params.id && !location.pathname.includes('/edit') && !location.pathname.includes('/upload')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('contest-detail') > -1) {
      return <Detail match={match} location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/contest-menu/add')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('contest-create') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/upload')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('contest-upload') > -1) {
      return <Upload match={match} />
    }
    return <Forbidden />
  }

  return ''
}

Contest.propTypes = {
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Contest
