import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Pagination,
  Button, Avatar,
  Select, Empty,
} from 'antd'
import { Loader } from 'components/elements'
import { EditOutlined } from '@ant-design/icons'
import { AGENT_CODE } from 'constants/ActionTypes'
import { Link } from 'react-router-dom'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const ListMateri = ({
  groupRole, dataMateri, currentUser,
  handlePage, metaMateri,
  stateMateri, setStateMateri,
  /* handleFilter, */loadMateri,
  isFetching, meterialType,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={isMobile ? 24 : 16} className={isMobile ? 'mt-4' : ''}>
          {(currentUser.permissions && currentUser.permissions.indexOf('materi-search') > -1) && (
            <Input
              allowClear
              placeholder="Search Materi by Subject..."
              className="field-lg"
              value={stateMateri.search}
              onChange={(e) => {
                setStateMateri({
                  ...stateMateri,
                  search: (e.target.value || '').toUpperCase(),
                })
              }}
              onPressEnter={() => loadMateri(true)}
            />
          )}
        </Col>
        <Col span={isMobile ? 14 : 5}>
          {(currentUser.permissions && currentUser.permissions.indexOf('materi-search') > -1) && (
            <Select
              onChange={(val) => {
                setStateMateri({ ...stateMateri, type: val })
                setTimeout(() => {
                  loadMateri(true)
                }, 300)
              }}
              allowClear
              value={meterialType.isLoad ? '' : stateMateri.type || `${(groupRole.code === AGENT_CODE) ? 'for_agent' : ''}`}
              placeholder="Select Type"
              className="field-lg w-100"
              loading={meterialType.isLoad}
              disabled={groupRole.code === AGENT_CODE}
            >
              {(meterialType.list || []).map(item => (
                <Select.Option key={item.value} value={item.value}>{(item.name || '').toUpperCase()}</Select.Option>
              ))}
            </Select>
          )}
        </Col>
        <Col span={2} align="end">
          {(currentUser.permissions && currentUser.permissions.indexOf('materi-search') > -1) && (
            <Button type="primary" className="button-lg px-4" onClick={() => loadMateri(true)}>
              Search
            </Button>
          )}
        </Col>
        <Col span={19} />
        {(currentUser.permissions && currentUser.permissions.indexOf('materi-create') > -1) && (
          <Col span={5} className="d-flex justify-content-end">
            <Button
              ghost
              type="primary"
              className="px-4 border-lg d-flex align-items-center"
              onClick={() => history.push('/materi-menu/add')}
            >
              <img src="/assets/plus.svg" alt="plus" className="mr-1" />
              Add New Materi
            </Button>
          </Col>
        )}
      </Row>

      <Row gutter={[24, 24]}>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataMateri)) && (
          <Col span={24}>
            <Card>
              <Empty
                description="Tidak ada Data."
              />
            </Card>
          </Col>
        )}
        {!isEmpty(dataMateri) && dataMateri.map(item => (
          <Col xs={24} md={8} lg={6} key={`materi_${item.id}`}>
            <Card className="text-center" loading={isFetching}>
              <Avatar
                src={item.icon_url || '/assets/avatar-on-error.jpg'}
                shape="square"
                size={230}
                className="border-blue w-100 icon-error material-img"
              >
                <img
                  src="/assets/avatar-on-error.jpg"
                  alt="on-error"
                />
              </Avatar>
              <div className="mt-5 text-truncate">
                <Link to={`/materi-menu/${item.id}`} className="title-card">{item.subject}</Link>
              </div>
              {(currentUser.permissions && currentUser.permissions.indexOf('materi-edit') > -1) && (
                <Button type="link" onClick={() => history.push(`/materi-menu/${item.id}/edit`)} className="float-right p-0">
                  <EditOutlined />
                  Edit
                </Button>
              )}
            </Card>
          </Col>
        ))}
      </Row>
      {!isFetching && (!isEmpty(dataMateri) && !(metaMateri.total_count < 12)) && (
        <Pagination
          className="py-3"
          showTotal={() => 'Page'}
          current={stateMateri.page ? Number(stateMateri.page) : 1}
          onChange={handlePage}
          simple={isMobile}
          pageSize={12}
          total={metaMateri.total_count}
          showSizeChanger={false}
        />
      )}
    </React.Fragment>
  )
}

ListMateri.propTypes = {
  groupRole: PropTypes.object,
  currentUser: PropTypes.object,
  dataMateri: PropTypes.array,
  metaMateri: PropTypes.object,
  stateMateri: PropTypes.object,
  handlePage: PropTypes.func,
  // handleFilter: PropTypes.func,
  meterialType: PropTypes.object,
  loadMateri: PropTypes.func,
  setStateMateri: PropTypes.func,
  isFetching: PropTypes.bool,
}

export default ListMateri
