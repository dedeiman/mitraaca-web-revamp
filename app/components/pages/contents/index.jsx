import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/contents/List'
import Detail from 'containers/pages/contents/Detail'
import Form from 'containers/pages/contents/Form'
import history from 'utils/history'

const FAQ = ({ currentUser, location, match, stateCheck }) => {
  if (location.pathname === '/materi-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('materi-menu') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (match.params.id && !location.pathname.includes('/edit')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('materi-detail') > -1) {
      return <Detail match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/materi-menu/add')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('materi-create') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }

  return ''
}

FAQ.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default FAQ
