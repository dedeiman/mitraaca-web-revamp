import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Tag,
  Card, Popconfirm,
  Avatar,
} from 'antd'
import {
  LeftOutlined, DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons'
import { AGENT_CODE } from 'constants/ActionTypes'
import ReactPlayer from 'react-player'
import history from 'utils/history'
import { isEmpty, capitalize } from 'lodash'

const DetailMateri = ({
  isFetching, detailMateri,
  groupRole, handleDelete,
  handleDownloadMultiple,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24} className={isMobile ? 'mt-4' : ''}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24}>
          <Row className="d-flex align-items-center">
            <Col span={isMobile ? 24 : 12}>
              <div className="title-page">Detail Materi</div>
            </Col>
            <Col span={isMobile ? 24 : 12} className={isMobile ? 'd-flex justify-content-start' : 'd-flex justify-content-end'}>
              {(groupRole.code !== AGENT_CODE) && (
                <div className="button-action">
                  <Popconfirm
                    title="Anda akan menghapus data ini?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => handleDelete()}
                  >
                    <Button type="link" className={isMobile ? 'pl-0' : ''}>
                      <DeleteOutlined />
                      Hapus
                    </Button>
                  </Popconfirm>
                  <Button type="link" onClick={() => history.push(`/materi-menu/${detailMateri.id}/edit`)}>
                    <EditOutlined />
                    Edit
                  </Button>
                </div>
              )}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={12} className={isMobile ? 'mb-4' : ''}>
          <Card className="h-100" loading={isFetching}>
            <p className="title-card">Materi</p>
            <p className="mb-5">{!isEmpty(detailMateri.type_of_material) ? ((detailMateri.type_of_material).split('_').join(' ')).toUpperCase() : '-'}</p>
            <p className="title-card">Subject</p>
            <p className="mb-5">{(detailMateri.subject || '-').toUpperCase()}</p>
            <p className="title-card">Language</p>
            <p className="mb-5">{(detailMateri.language || '-').toUpperCase()}</p>
            <p className="title-card">Description</p>
            <p>{(detailMateri.description || '-').toUpperCase()}</p>
          </Card>
        </Col>
        <Col xs={24} md={12} className={isMobile ? 'mb-3' : ''}>
          <Card className="h-100" loading={isFetching}>
            <Row gutter={24}>
              <Col xs={24} md={24} lg={24} xl={12}>
                <p className="title-card">Icon</p>
                <Avatar
                  src={detailMateri.icon_url || '/assets/avatar-on-error.jpg'}
                  shape="square"
                  size={150}
                  className="img-contain"
                />
              </Col>
              <Col xs={24} md={24} lg={24} xl={12}>
                <p className="title-card">Link</p>
                <p>{detailMateri.link_embed_video || '-'}</p>
                {detailMateri.link_embed_video && (
                  <ReactPlayer
                    className="player-video"
                    controls
                    url={detailMateri.link_embed_video}
                  />
                )}
              </Col>
              <Col xs={24} className="mt-3">
                <p className="title-card">Download File</p>
                {/* eslint-disable */}
                <Row>
                  {!isEmpty(detailMateri) && detailMateri.files.map((dataFile, idx) => {
                    let fileUrl = !isEmpty(dataFile.file_url) ? dataFile.file_url :  '/assets/avatar-on-error.jpg'

                    return (
                      <Col lg={11} xs={12} className="m-2">
                        {(() => {
                          switch (dataFile.file_url.split('.').pop()) {
                            case 'jpg':
                            case 'jpeg':
                            case 'bpm':
                            case 'gif':
                            case 'png':
                              return (
                                <>
                                  <Avatar
                                    id={dataFile.id}
                                    src={fileUrl}
                                    shape="square"
                                    size={150}
                                    className="img-contain"
                                  />
                                  <p
                                    style={{
                                      bottom: 0,
                                      paddingTop: 5,
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                    }}
                                  >
                                    {`${capitalize((dataFile.title || '').split('-')[0])} - File ${idx+1}`}
                                  </p>
                                </>
                              )
                            default:
                              return (
                                <>
                                  <div style={{ height: 150, width: 150, display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#F3F6F9', borderRadius: 10 }}>
                                    <Avatar
                                      id={dataFile.id}
                                      src="/assets/document.svg"
                                      shape="square"
                                      size={50}
                                      className="img-contain"
                                      style={{
                                        display: "block",
                                        marginRight: "auto",
                                        marginLeft: "auto",
                                      }}
                                    />
                                  </div>
                                  <p
                                    style={{
                                      bottom: 0,
                                      paddingTop: 5,
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                    }}
                                  >
                                    {`${capitalize((dataFile.title || '').split('-')[0])} - File ${idx+1}`}
                                  </p>
                                </>
                              )
                          }
                        })()}
                      </Col>
                    )
                  })}
                </Row>
                <p>
                  <a onClick={(e) => handleDownloadMultiple(e, detailMateri.files)} style={{ color: '#0F6ECD' }}><b>Click here</b></a>
                  {' '}
                  to download file
                </p>
                {/* eslint-enable */}
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailMateri.propTypes = {
  groupRole: PropTypes.object,
  detailMateri: PropTypes.object,
  isFetching: PropTypes.bool,
  handleDelete: PropTypes.func,
  handleDownloadMultiple: PropTypes.func,
}

export default DetailMateri
