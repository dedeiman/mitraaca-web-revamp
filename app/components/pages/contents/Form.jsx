/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable consistent-return */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Avatar,
  Card, Select,
  Input, Upload,
  Tag,
} from 'antd'
import Helper from 'utils/Helper'
import { Form } from '@ant-design/compatible'
import { InboxOutlined, LoadingOutlined, LeftOutlined } from '@ant-design/icons'
import ReactPlayer from 'react-player'
import history from 'utils/history'

const { Dragger } = Upload

const FormMateri = ({
  isFetching,
  onSubmit, handleUploadIcon,
  form, match, stateFile,
  languageOptions, handleUploadFiles,
}) => {
  const isMobile = window.innerWidth < 768
  const { getFieldDecorator, getFieldValue } = form

  return (
    <React.Fragment>
      <Row gutter={24} className={isMobile ? 'pt-3 mb-3' : 'mb-5'}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center mt-3">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Add'} Materi`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24}>
          <Col xs={24} md={12} className={isMobile ? 'pb-4' : 'pb-0'}>
            <Card className="h-100">
              <p className="title-card mb-4">Details</p>
              <p className="font-weight-bold">Type</p>
              <Form.Item>
                {getFieldDecorator('type_of_material', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Type'),
                  ],
                  initialValue: stateFile.type_of_material || undefined,
                })(
                  <Select
                    placeholder="Select Type"
                    className="field-lg"
                  >
                    {[{ id: 'for_agent', name: 'Agent' }, { id: 'for_non_agent', name: 'Non Agent' }].map(item => (
                      <Select.Option key={item.id} value={item.id}>{item.name.toUpperCase()}</Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
              <p className="font-weight-bold">Subject</p>
              <Form.Item>
                {getFieldDecorator('subject', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Subject'),
                    { max: 100, message: '*Maksimal 100 karakter\n' },
                    { min: 10, message: '*Minimal 10 karakter\n' },
                    { pattern: /^[a-zA-Z0-9 ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
                  ],
                  initialValue: stateFile.subject || undefined,
                })(
                  <Input
                    placeholder="Input Subject"
                    className="field-lg uppercase"
                    style={{ 'text-transform': 'uppercase' }}
                  />,
                )}
              </Form.Item>
              <p className="font-weight-bold">Language</p>
              <Form.Item>
                {getFieldDecorator('language', {
                  initialValue: stateFile.language || 'Indonesia',
                })(
                  <Select
                    showSearch
                    className="field-lg"
                    loading={languageOptions.load}
                    placeholder="Select Language"
                  >
                    {(languageOptions.list || []).map(item => (
                      <Select.Option key={item.id} value={item.name}>{item.name.toUpperCase()}</Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
              <p className="font-weight-bold">Description</p>
              <Form.Item>
                {getFieldDecorator('description', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Description'),
                    { max: 50, message: '*Maksimal 50 karakter\n' },
                    { min: 10, message: '*Minimal 10 karakter\n' },
                    { pattern: /^[a-zA-Z0-9,?\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },

                  ],
                  initialValue: stateFile.description || '',
                })(
                  <Input.TextArea
                    rows={4}
                    className="field-lg uppercase"
                    placeholder="Input Description"
                    style={{ 'text-transform': 'uppercase' }}
                  />,
                )}
              </Form.Item>
            </Card>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={0} className="h-100">
              <Col span={24} className="pb-4">
                <Card className="h-100">
                  <p className="title-card">Icon</p>
                  <Form.Item>
                    {getFieldDecorator('icon', {
                      getValueFromEvent: (info) => {
                        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'
                        if (!isFileType) return undefined
                        return info
                      },
                    })(
                      <Upload
                        accept="image/*"
                        name="icon"
                        listType="picture-card"
                        className="avatar-uploader banner-content"
                        showUploadList={false}
                        beforeUpload={() => false}
                        onChange={info => handleUploadIcon(info)}
                      >
                        {stateFile.icon
                          ? (
                            <Avatar src={stateFile.icon} shape="square" className="banner-preview" />
                          )
                          : (
                            <div>
                              <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                              </p>
                              <p className="ant-upload-text">Upload Icon</p>
                            </div>
                          )
                        }
                      </Upload>,
                    )}
                  </Form.Item>
                </Card>
              </Col>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">File</p>
                  <Form.Item className="m-0">
                    {getFieldDecorator('files', {
                      initialValue: stateFile.file || [],
                      rules: [
                        ...Helper.fieldRules(['required'], 'File'),
                      ],
                    })(
                      <Dragger
                        name="file"
                        multiple="true"
                        listType="picture-card"
                        beforeUpload={() => false}
                        onChange={(info) => {
                          if (info.fileList.length > 5) {
                            return false
                          }

                          handleUploadFiles(info)
                        }}
                        fileList={stateFile.file}
                        showUploadList={{ showRemoveIcon: true, showPreviewIcon: false }}
                        style={{ 'marginBottom': '10px', display: (stateFile.file.length >= 5 ? 'none' : '') }}
                      >
                        { stateFile.file.length < 5 && (
                          <div>
                            <p className="ant-upload-drag-icon">
                              <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                              Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                              band files
                            </p>
                          </div>
                        )}
                      </Dragger>,
                    )}
                  </Form.Item>
                  <p className="font-weight-bold">Link Embed Video</p>
                  <Form.Item>
                    {getFieldDecorator('link_embed_video', {
                      initialValue: stateFile.link_embed_video || '',
                    })(
                      <Input
                        className="field-lg"
                        placeholder="Input Link Video"
                      />,
                    )}
                  </Form.Item>
                  <ReactPlayer
                    controls
                    width="250px"
                    height="250px"
                    url={getFieldValue('link_embed_video')}
                  />
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col span={24} className="d-flex justify-content-end align-items-center">
            <Button
              type="primary"
              htmlType="submit"
              className={isMobile ? 'button-lg mb-3 mr-3' : 'button-lg w-15 mr-3'}
              disabled={isFetching}
            >
              {match.params.id ? 'Save' : 'Submit'}
              {isFetching && <LoadingOutlined />}
            </Button>
            <Button
              ghost
              type="primary"
              className={isMobile ? 'button-lg border-lg mb-3 mr-3' : 'button-lg w-15 border-lg'}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormMateri.propTypes = {
  isFetching: PropTypes.bool,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  match: PropTypes.object,
  stateFile: PropTypes.object,
  languageOptions: PropTypes.object,
  handleUploadFiles: PropTypes.func,
  handleUploadIcon: PropTypes.func,
}

export default FormMateri
