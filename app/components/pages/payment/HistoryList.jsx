import React from 'react'
import {
  Button, Empty,
  Card, Row, Col,
  Typography, Divider,
  Descriptions, Select,
  Pagination,
} from 'antd'
import { isEmpty } from 'lodash'
import moment from 'moment'
import Helper from 'utils/Helper'
import PropTypes from 'prop-types'

function getStatusFromApi(status) {
  switch (status) {
    case 'need-approval':
      return 'Need Approval'
    case 'approved':
      return 'Pending Payment'
    case 'waiting-payment':
      return 'Waiting Payment'
    case 'complete':
      return 'Complete'
    default:
      return '-'
  }
}

const HistoryList = ({ state, changeState, handlePage, loadMasterData }) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">History Payment</div>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col xs={24} md={6}>
          <Card className="card-box-filter">
            <div className="title-box">
              <h5>Filter</h5>
            </div>
            <div className="body-box">
              <div className="input-col-filter">
                <span>
                  Status Pembayaran
                </span>
                <Select
                  className="mitra-select-filter-sm"
                  defaultValue=""
                  value={state.status}
                  onChange={val => changeState({ ...state, status: val })}
                >
                  <Select.Option value="">All</Select.Option>
                  {['waiting-payment', 'cancelled', 'paid', 'expired'].map(key => (
                    <Select.Option value={key} className="text-capitalize">{key.replace('-', ' ')}</Select.Option>
                  ))}
                </Select>
              </div>
              <div className="input-col-filter">
                <span>
                  Product
                </span>
                <Select
                  className="mitra-select-filter-sm"
                  defaultValue=""
                  value={state.product}
                  onChange={val => changeState({ ...state, product: val })}
                >
                  <Select.Option value="">All</Select.Option>
                  {(state.productList || []).map(key => (
                    <Select.Option value={key.id} key={Math.random()}>{key.display_name}</Select.Option>
                  ))}
                </Select>
              </div>

              <div className="mt-4 mb-5 d-flex float-right">
                <Button
                  type="primary"
                  className="mitra-submit-filter-sm float-right"
                  onClick={loadMasterData}
                >
                  Terapkan
                </Button>
              </div>
            </div>
          </Card>
        </Col>

        <Col xs={24} md={18}>
          {state.load
            ? <Card loading />
            : (
              <Row gutter={[24, 24]}>
                {!isEmpty(state.list)
                  ? (
                    (state.list || []).map(item => (
                      <Col xs={24}>
                        <Card>
                          <div className="d-flex justify-content-between align-items-center">
                            <p className="text-primary font-weight-bold">{`Invoice No: ${item.transaction_number}`}</p>
                            <p className={`font-weight-bold text-${getStatusFromApi(item.status) === 'Complete' ? 'success' : 'danger'}`}>{getStatusFromApi(item.status)}</p>
                          </div>
                          {(item.sppa_items || []).map(sppa => (
                            <React.Fragment key={Math.random()}>
                              <Row gutter={24}>
                                <Col xs={24} md={6} xl={6}>
                                  <Descriptions layout="vertical" colon={false} column={1}>
                                    <Descriptions.Item
                                        span={2}
                                        className="px-1"
                                        label={(
                                          <p
                                            className="m-0 font-weight-bold"
                                            style={{ color: 'black' }}
                                          >
                                            {`${sppa.product ? Helper.getValue(sppa.product.class_of_business.name) : '-'} - ${sppa.product ? Helper.getValue(sppa.product.display_name) : '-'}`}
                                          </p>
                                        )}
                                      >
                                        <p className="mb-0 text-muted">{sppa.sppa_number || '-'}</p>
                                    </Descriptions.Item>
                                  </Descriptions>
                                </Col>
                                <Col xs={24} md={6} xl={6}>
                                  <Descriptions layout="vertical" colon={false} column={1}>
                                    <Descriptions.Item span={2} label="Pemegang Polis" className="px-1">
                                      <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                                        <p className="mb-0">{sppa.name_on_policy || '-'}</p>
                                      </Typography.Paragraph>
                                    </Descriptions.Item>
                                  </Descriptions>
                                </Col>
                                <Col xs={24} md={6} xl={6}>
                                  <Descriptions layout="vertical" colon={false} column={1}>
                                    <Descriptions.Item span={2} label="Tertanggung" className="px-1">
                                      <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                                        <p className="mb-0">{sppa.insured ? Helper.getValue(sppa.insured.name) : '-'}</p>
                                      </Typography.Paragraph>
                                    </Descriptions.Item>
                                  </Descriptions>
                                </Col>
                                <Col xs={24} md={6} xl={6}>
                                  <Descriptions layout="vertical" colon={false} column={1}>
                                    <Descriptions.Item span={2} label="Periode" className="px-1">
                                      <p className="mb-0">
                                        {sppa.start_period ? moment(sppa.start_period).format('DD/MM/YYYY') : '-'}
                                        {sppa.end_period ? ' - ' : ''}
                                        {sppa.end_period ? moment(sppa.end_period).format('DD/MM/YYYY') : ''}
                                      </p>
                                    </Descriptions.Item>
                                  </Descriptions>
                                </Col>
                              </Row>
                              <Divider />
                            </React.Fragment>
                          ))}
                          <div className="d-flex justify-content-end align-items-center">
                            <p className="text-primary font-weight-bold m-0">{Helper.currency(item.gross_amount, 'Rp. ', ',00')}</p>
                            {/*
                              <Button ghost type="primary" className="mx-3" size="large">Batalkan</Button>
                              <Button type="primary" size="large">Bayar</Button>
                            */}
                          </div>
                        </Card>
                      </Col>
                    ))
                  ) : (
                    <Col xs={24}>
                      <Card>
                        <Empty description="No Data" />
                      </Card>
                    </Col>
                  )
                }
              </Row>
            )
          }
          {
            !isEmpty(state.list) && (
              <Pagination
                className="py-3"
                showTotal={() => 'Page'}
                simple={isMobile}
                pageSize={state.meta_page.per_page || 10}
                current={state.meta_page.page ? Number(state.meta_page.page) : 1}
                onChange={handlePage}
                total={state.meta_page.total_count}
                showSizeChanger={false}
              />
            )
          }
        </Col>
      </Row>
    </React.Fragment>
  )
}
HistoryList.propTypes = {
  state: PropTypes.object,
  changeState: PropTypes.func,
  handlePage: PropTypes.func,
  loadMasterData: PropTypes.func,
}

export default HistoryList