import {
  Card, Select, Button,
} from 'antd'
import PropTypes from 'prop-types'

const { Option } = Select

const Form = ({
  state, changeState, handleSearch,
  handleProduct,
}) => (
  <Card className="card-box-filter">
    <div className="title-box">
      <h5>Filter</h5>
    </div>
    <div className="body-box">
      <form onSubmit={handleSearch}>
        <div className="input-col-filter">
          <span>
            COB
          </span>
          <Select
            className="mitra-select-filter-sm"
            value={state.cob}
            onChange={(val) => {
              handleProduct(val)
            }}
          >
            <Option value="">All</Option>
            {
              state.cobList && state.cobList.map(key => (
                <Option value={key.id} key={key.id}>{key.name}</Option>
              ))
            }
          </Select>
        </div>

        <div className="input-col-filter">
          <span>
            Product
          </span>
          <Select
            className="mitra-select-filter-sm"
            defaultValue=""
            value={state.product}
            loading={state.productLoad}
            onChange={val => changeState({
              ...state,
              product: val,
            })}
          >
            <Option value="">All</Option>
            {
              state.productList && state.productList.map(key => (
                <Option value={key.id} key={key.id}>{key.display_name}</Option>
              ))
            }
          </Select>
        </div>

        <div className="input-col-filter">
          <span>
            Nomor SPPA
          </span>
          <input
            className="mitra-input-filter-sm"
            placeholder="- Nomor SPPA -"
            value={state.number_sppa}
            onChange={e => changeState({
              ...state,
              number_sppa: e.target.value,
            })}
          />
        </div>

        <div className="input-col-filter">
          <span>
            Pemegang Polis
          </span>
          <input
            className="mitra-input-filter-sm"
            placeholder="- Nama Pemegang Polis -"
            value={state.policy_holder}
            onChange={e => changeState({
              ...state,
              policy_holder: e.target.value,
            })}
          />
        </div>

        <div className="input-col-filter">
          <span>
            Tertanggung
          </span>
          <input
            className="mitra-input-filter-sm"
            placeholder="- Nama Tertanggung -"
            value={state.insured}
            onChange={e => changeState({
              ...state,
              insured: e.target.value,
            })}
          />
        </div>
        <div className="input-col-filter">
          <span>
            Status
          </span>
          <Select
            className="mitra-select-filter-sm"
            defaultValue=""
            value={state.status}
            onChange={val => changeState({
              ...state,
              status: val,
            })}
          >
            <Option value="">All</Option>
            {
              state.statusList && state.statusList.map(key => (
                <Option value={key.code} key={key.id}>{key.name}</Option>
              ))
            }
          </Select>
        </div>

        <div className="mt-4 mb-5 d-flex float-right">
          <Button
            type="primary"
            className="mitra-submit-filter-sm float-right"
            htmlType="submit"
          >
            Terapkan
          </Button>
        </div>
      </form>
    </div>
  </Card>
)

Form.propTypes = {
  state: PropTypes.any,
  changeState: PropTypes.func,
  handleSearch: PropTypes.func,
  handleProduct: PropTypes.func,
}


export default Form
