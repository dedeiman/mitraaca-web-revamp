/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-array-index-key */

import React from 'react'
import {
  Card, Row, Col,
  Checkbox, Collapse, Button,
  Skeleton, Pagination
} from 'antd'
import moment from 'moment'
import Helper from 'utils/Helper'
import PropTypes from 'prop-types'
import history from 'utils/history'

const { Panel } = Collapse

const isMobile = window.innerWidth < 768

function getStatusFromApi(status) {
  switch (status) {
    case 'need-approval':
      return 'Need Approval'
    case 'approved':
      return 'Pending Payment'
    case 'waiting-payment':
      return 'Waiting Payment'
    case 'pending':
      return 'Pending'
    case 'complete':
      return 'Complete'
    case 'rejected':
      return 'Rejected'
    case 'expired':
      return 'Expired'
    default:
      return '-'
  }
}

const List = ({
  cart, handleCheckbox, currentUser,
  state, handleSubmit,
  handleIO, handleComingSoon,
  handleDetailDocument,
  handlePage, metaCart,
  stateCart, updatePerPage,
}) => (
  <React.Fragment>
    {
      cart.isFetching && (
        <Card>
          <Skeleton active />
          <Skeleton active />
        </Card>
      )
    }
    {!cart.isFetching && (
    <Card className="card-box-list">
      {!cart.isFetching && cart.listCart.length !== 0 ? cart.listCart.map((item, id) => (
        <div key={id}>
          <div className="cart-item-list">
            <Row>
              <Col md={{ span: 1 }} xs={3}>
                <Checkbox
                  value={1}
                  onChange={() => handleCheckbox(item)}
                  checked={state.listCheck.includes(item.id)}
                />
              </Col>
              <Col md={{ span: 23 }} xs={21}>
                <Row gutter={24}>
                  <Col md={12} xs={24}>
                    <div className="info-item">
                      <label>
                        {item.product.display_name}
                      </label>
                      <p className="id-item">
                        {item.sppa_number}
                      </p>
                    </div>
                  </Col>
                  <Col md={12} xs={24}>
                    <div className="info-item">
                      <label>
                        Pemegang Polis
                      </label>
                      <p>
                        {item.policy_holder.name}
                      </p>
                    </div>
                  </Col>
                  <Col md={12} xs={24}>
                    <div className="info-item">
                      <label>
                        Periode
                      </label>
                      <p>
                        {moment(item.start_period).format('DD/MM/YYYY')}
                        {' '}
                        -
                        {' '}
                        {moment(item.end_period).format('DD/MM/YYYY')}
                      </p>
                    </div>
                  </Col>
                  <Col md={12} xs={24}>
                    <div className="info-item">
                      <label>
                        Tertanggung
                      </label>
                      <p>
                        {item.insured ? item.insured.name : ''}
                      </p>
                    </div>
                  </Col>
                  <Col md={16} xs={24}>
                    <div className="info-item">
                      <label>
                        Status SPPA
                      </label>
                      <p>
                        {(item.status_info ? item.status_info.name : '')}
                      </p>
                    </div>
                  </Col>
                  <Col md={8} xs={24}>
                    <div className="info-item">
                      <p className="price">
                        {Helper.currency(item.total_payment, 'Rp. ', '')}
                      </p>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          {(currentUser.permissions && currentUser.permissions.indexOf('payment-sppa') > -1) && (
          <Row>
            <Col md={24} xs={24}>
              <Collapse defaultActiveKey={['100']} className="collapse-detail">
                <Panel header="Detail Produk" key={id}>
                  <Row gutter={24}>
                    <Col md={6} xs={24} className="text-center">
                      <div className="button-detail ">
                        <Button
                          type="primary"
                          disabled={false}
                          className="mitra-submit-filter-sm"
                          onClick={() => handleIO(item.io_id, item.product)}
                        >
                          Detail Penawaran
                        </Button>
                      </div>
                    </Col>
                    <Col md={6} xs={24} className="text-center">
                      <div className="button-detail ">
                        <Button
                          type="primary"
                          disabled={false}
                          className="mitra-submit-filter-sm"
                          onClick={() => history.push(`/production-create-sppa/detail/${item.id}`)}
                        >
                          Detail SPPA
                        </Button>
                      </div>
                    </Col>
                    <Col md={6} xs={24} className="text-center">
                      <div className="button-detail ">
                        <Button
                          type="primary"
                          disabled={false}
                          className="mitra-submit-filter-sm"
                          onClick={() => history.push(`/detail-statement/${item.id}`)}
                        >
                          Detail Pernyataan
                        </Button>
                      </div>
                    </Col>
                    <Col md={6} xs={24} className="text-center">
                      <div className="button-detail ">
                        <Button
                          type="primary"
                          disabled={false}
                          className="mitra-submit-filter-sm"
                          onClick={() => handleDetailDocument(item)}
                        >
                          Detail Dokumen
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Panel>
              </Collapse>
            </Col>
          </Row>
          )}
          <hr />
        </div>
      )) : <p className="placeholder">Data yang anda cari tidak ditemukan.</p>}
      <Row>
          <Col md={{ span: 23 }} xs={6}>
            <Pagination
              className="py-3"
              showTotal={() => 'Page'}
              current={stateCart.page ? Number(stateCart.page) : 1}
              onChange={handlePage}
              simple={isMobile}
              pageSize={12}
              total={metaCart.total_count}
              showSizeChanger={false}
            />
          </Col>
      </Row>
      <div className="card-footer-result">
        <Row>
          <Col md={12} xs={12}>
            <p className="text-pay">
              Total Pembayaran:
              {state.listPrice.length}
              {' '}
              Items
            </p>
            <div>
              <p className="text-price">{Helper.currency(state.total, 'Rp. ', '')}</p>
            </div>
          </Col>
          <Col md={12} xs={12}>
            <Button
              type="primary"
              className="mitra-submit-filter-sm btn-block"
              htmlType="submit"
              onClick={handleSubmit}
            >
              Checkout
            </Button>
          </Col>
        </Row>
      </div>
    </Card>
    )}
  </React.Fragment>
)

List.propTypes = {
  currentUser: PropTypes.object,
  cart: PropTypes.any,
  handleCheckbox: PropTypes.func,
  state: PropTypes.any,
  handleSubmit: PropTypes.func,
  handleIO: PropTypes.func,
  handleComingSoon: PropTypes.func,
  handleDetailDocument: PropTypes.func,
  metaCart: PropTypes.object,
  stateCart: PropTypes.object,
  updatePerPage: PropTypes.func,
}

export default List
