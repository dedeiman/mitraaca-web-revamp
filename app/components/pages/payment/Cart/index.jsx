import React from 'react'
import {
  Row, Col,
} from 'antd'

import FormFilter from 'containers/pages/payment/Cart/Form'
import List from 'containers/pages/payment/Cart/List'

const Cart = () => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Keranjang</div>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col xs={24} md={6}>
          <FormFilter />
        </Col>
        <Col xs={24} md={18}>
          <List />
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default Cart
