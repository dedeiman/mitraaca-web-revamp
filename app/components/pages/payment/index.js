import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import Cart from 'containers/pages/payment/Cart'
import HistoryCart from 'containers/pages/payment/HistoryList'
import Pernyataan from 'containers/pages/payment/Pernyataan'
import Checkout from 'containers/pages/payment/Checkout'
import StatusPages from './Checkout/StatusPages'
import LoginPayment from './Checkout/LoginPayment'
import history from '../../../utils/history'

const Routes = ({ currentUser, match, location, stateCheck }) => {
  if (location.pathname === '/payment-filter') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('payment-filter') > -1) {
      return <Cart match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/payment-checkout-mitraca') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('payment-checkout-mitraca') > -1) {
      return <LoginPayment match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname === '/payment-history') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('payment-history') > -1) {
      return <HistoryCart match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname.includes('/checkout')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('payment-menu') > -1) {
      return <Checkout match={match} />
    }
    return <Forbidden />
  }

  if (location.pathname.includes('/detail-statement')) {
    return <Pernyataan match={match} />
  }

  if (location.pathname.includes('/status')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('payment-menu') > -1) {
      return <StatusPages match={match} />
    }
    return <Forbidden />
  }

  return ''
}

Routes.propTypes = {
  location: PropTypes.object,
  currentUser: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Routes
