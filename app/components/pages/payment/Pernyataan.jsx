import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Card, Descriptions, Button, Tag,
} from 'antd'
import { LeftOutlined } from '@ant-design/icons'
import history from 'utils/history'

const Pernyataan = ({
  detailSPPA,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Detail Pernyataan</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={24}>
          <Row gutter={[24, 24]}>
            <Col span={24}>
              <Card className="h-100" loading={detailSPPA.loading}>
                {
                  !detailSPPA.loading
                  && (
                    <>
                      <Descriptions layout="vertical" colon={false} column={2}>
                        {detailSPPA.list.product.type.name === 'Konvensional'
                        && (
                          <Descriptions.Item span={2} label="Pernyataan Tertanggung" className="px-1 profile-detail pb-0">
                            <ol>
                              <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
                              <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh penanggung</li>
                              <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari penanggung.</li>
                              <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT.</li>
                              <li style={{ textAlign: 'justify' }}>Mengerti bahwa polis akan diterbitkan setelah SPPA di isi dan ditandatangani serta melampirkan Photocopy STNK</li>
                            </ol>
                          </Descriptions.Item>
                        )
                        }
                        {detailSPPA.list.product.type.name === 'Syariah'
                        && (
                          <Descriptions.Item span={2} label="Pernyataan Peserta" className="px-1 profile-detail pb-0">
                            <ol>
                              <li style={{ textAlign: 'justify' }}>Menyatakan bahwa keterangan tersebut diatas dibuat dengan sejujurnya dan sesuai dengan keadaan yang sebenarnya menurut pengetahuan saya atau yang seharusnya saya ketahui.</li>
                              <li style={{ textAlign: 'justify' }}>Menyadari bahwa keterangan tersebut akan digunakan sebagai dasar dan merupakan bagian yang tidak terpisahkan dari polis yang akan diterbitkan, oleh karenanya ketidakbenarannya dapat mengakibatkan batalnya pertanggungan dan ditolak setiap tuntutan ganti rugi oleh pengelola.</li>
                              <li style={{ textAlign: 'justify' }}>Memahami bahwa pertanggungan yang diminta ini baru berlaku setelah mendapat persetujuan tertulis dari pengelola.</li>
                              <li style={{ textAlign: 'justify' }}>Menyetujui untuk bertanggung jawab atas premi asuransi yang harus dibayar sesuai dengan jadwal pembayaran yang dinyatakan dalam SPPA asuransi. Pembayaran premi akan di transfer ke rekening PT. Asuransi Central Asia melalui Virtual Account yang tercantum pada nota premi.</li>
                            </ol>
                          </Descriptions.Item>
                        )
                        }
                        <Descriptions.Item span={2} label="Tanda Tangan Tertanggung" className="px-1 profile-detail pb-0 text-center">
                          <img src={detailSPPA.list.signature} alt="logo" />
                        </Descriptions.Item>
                      </Descriptions>
                    </>
                  )
                }
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  )
}

Pernyataan.propTypes = {
  detailSPPA: PropTypes.object,
}

export default Pernyataan
