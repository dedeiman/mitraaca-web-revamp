import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Form } from '@ant-design/compatible'
import {
  Input, Divider, Alert,
} from 'antd'
import { Button, Col } from 'react-bootstrap'
import Helper from 'utils/Helper'
import '@ant-design/compatible/assets/index.css'

const LoginPayment = ({
  onChange,
  onSubmit,
  form,
  errorMessage,
  onCloseError,
}) => {
  const { getFieldDecorator } = form
  return (
    <div className="form-container row m-0">
      <Col md={7} className="d-none d-md-block" />
      <Col>
        <Form className="form-signin" onSubmit={onSubmit} hideRequiredMark layout="vertical">
          <h3>Silakan masuk dengan kode Pembayaran</h3>
          <Form.Item label="Nomor Kode Pembayaran">
            {getFieldDecorator('user_id', {
              rules: Helper.fieldRules(['required'], 'User'),
            })(
              <Input onChange={onChange} name="user_id" placeholder="User ID" />,
            )}
          </Form.Item>
          <Form.Item label="Password">
            {getFieldDecorator('password', {
              rules: Helper.fieldRules(['required'], 'Password'),
            })(
              <Input.Password
                name="password"
                onChange={onChange}
                placeholder="Password"
                iconRender={visible => (
                  visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                )}
              />,
            )}
          </Form.Item>

          {errorMessage && (
            <Alert
              message=""
              description={errorMessage}
              type="error"
              closable
              onClose={onCloseError}
              className="mb-3 text-left"
            />
          )}

          <div className="d-flex align-items-center justify-content-end">
            <Button block size="lg" variant="primary" style={{ width: '50%', textAlign: 'center', float: 'right' }} type="submit">
              {/* {isAuthenticating && <LoadingOutlined className="mr-2" />} */}
              {'Login'}
            </Button>
          </div>

          <div className="pb-3" />

          <Divider />
          <div className="text-left">
            Butuh bantuan?
            <a href="tel:+39708999" className="ml-1">(021) 39708999</a>
          </div>
        </Form>
      </Col>
    </div>
  )
}

LoginPayment.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCloseError: PropTypes.func,
  form: PropTypes.any,
  errorMessage: PropTypes.string,
}

export default LoginPayment
