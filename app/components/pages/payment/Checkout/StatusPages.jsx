import React from 'react'
import PropTypes from 'prop-types'
import { Typography } from 'antd'
import moment from 'moment'

const { Paragraph } = Typography
const StatusPages = ({
  dataPayment,
}) => (
  <div className="container-status mt-5">
    {!dataPayment.loadPayment && dataPayment.getDataPayment.payment_method.slug === 'virtual-account' && (
      <div className="wrapped-status">
        <div className="title-page align-items-center">Virtual Account</div>
        <h6>Berikut nomor Virtual Account untuk pembayaran</h6>
        <Paragraph className="h1 text-center mt-4 mb-4" style={{ color: '#2b57b7' }} copyable>{dataPayment.getDataPayment.payment_response.va_number}</Paragraph>
        <p className="mb-2 "><strong>Nomor virtual Account hanya berlaku untuk 2 jam, terhitung dari proses pembayaran dilakukan</strong></p>
        <p className="mb-2"><strong>Petunjuk Transfer mBanking / m-BCA:</strong></p>

        <ol>
          <li><strong>Pilih m-Transfer &gt; BCA Virtual Account</strong></li>
          <li>
            <strong>
              Masukkan nomor Virtual Account
              {' '}
              <span className="trigger-blue">{dataPayment.getDataPayment.payment_response.va_number}</span>
              {' '}
              dan pilih
              {' '}
              <span className="trigger-blue">send</span>
            </strong>
          </li>
          <li>
            <strong>
              Periksa nominal yang akan dibayarkan. Jika benar, pilih
              {' '}
              <span className="trigger-blue">OK</span>
            </strong>
          </li>
          <li>
            <strong>
              Masukkan PIN m-BCA Anda dan pilih
              {' '}
              <span className="trigger-blue">OK</span>
            </strong>
          </li>
        </ol>

        <p className="back">
          <a href="/payment-filter">Kembali ke Keranjang</a>
        </p>
      </div>
    )}
    {!dataPayment.loadPayment && dataPayment.getDataPayment.payment_method.slug === 'klik-mitraca' && (
      <div className="wrapped-status">
        <div className="title-page align-items-center">Nomor Invoice Klik-Mitraca</div>
        <h6>Berikut nomor invoice untuk pembayaran</h6>
        <Paragraph className="h1 text-center mt-4 mb-4" style={{ color: '#2b57b7' }} copyable>{dataPayment.getDataPayment.payment_response.va_number}</Paragraph>
        <h6 className="mb-2 "><strong>Nomor invoice berlaku hingga {' '}
          <span className="trigger-blue">{moment(dataPayment.getDataPayment.payment_deadline).format('YYYY-MM-DD HH:MM:SS')}</span></strong></h6>
        <h6 className="mb-2"><strong>Petunjuk pembayaran menggunakan KlikACA :</strong></h6>

        <ol>
          <li><strong>Masuk ke aplikasi KlikACA <a className="trigger-blue" target="_blank" href="http://www.klik-mitraca.com">(www.klik-mitraca.com)</a></strong></li>
          <li>
            <strong>
              Login dengan menggunakan Kode dan Password yang sudah dikirimkan
            </strong>
          </li>
          <li>
            <strong>
              Cek kembali Nomor SPPA, nomor invoice dan nominal yang ditampilkan
            </strong>
          </li>
          <li>
            <strong>
              Checklist pada bagian "Saya setuju untuk melakukan pembayaran sesuai jumlah diatas"
            </strong>
          </li>
          <li>
            <strong>
              Klik button Pay
            </strong>
          </li>
        </ol>
        <p className="back">
          <a href="/payment-filter">Kembali ke Keranjang</a>
        </p>
      </div>
    )}
  </div>
)

StatusPages.propTypes = {
  dataPayment: PropTypes.object,
}

export default StatusPages
