import React from 'react'
import {
  Row, Col, Card,
  Descriptions, Divider,
  Radio, Skeleton, Button,
} from 'antd'
import moment from 'moment'
import history from 'utils/history'
import PropTypes from 'prop-types'
import Helper from 'utils/Helper'
import config from 'app/config'

const Checkout = ({
  listCheckout,
  onChangeRadio,
  statePayment,
  selectPayment,
  handlePayment,
  detailPayment,
  loading,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Check Out</div>
        </Col>
      </Row>

      {listCheckout.isFetching && (
        <Card>
          <Skeleton active />
          <Skeleton active />
        </Card>
      )}

      {!listCheckout.isFetching && (
        <Card>
          {!listCheckout.isFetching && (listCheckout.listCart.sppa_items.map(item => (
            <Row gutter={[24, 24]} key={item.id}>
              <Col xs={24} md={24}>
                <Descriptions layout="vertical" colon={false} column={4}>
                  <Descriptions.Item label="Motorcar - Motorcar TLO" className="px-1">
                    {item.sppa_number}
                  </Descriptions.Item>
                  <Descriptions.Item label="Pemegang Polis">
                    {item.name_on_policy.toUpperCase()}
                  </Descriptions.Item>
                </Descriptions>
                <Descriptions layout="vertical" colon={false} column={4}>
                  <Descriptions.Item label="Periode">
                    {moment(item.created_at).format('MM/DD/YYYY')}
                    {' '}
                    -
                    {moment(item.end_period).format('MM/DD/YYYY')}
                  </Descriptions.Item>
                  <Descriptions.Item label="Tertanggung">
                    {item.insured.name.toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item label="Status SPPA">
                    {item.status.toUpperCase()}
                  </Descriptions.Item>
                  <Descriptions.Item>
                    <span className="price-payment">{Helper.currency(item.total_payment, 'Rp. ', '')}</span>
                  </Descriptions.Item>
                </Descriptions>
              </Col>
            </Row>
          )))}
          <Divider />
          <Row>
            <Descriptions layout="vertical" colon={false} column={4}>
              <Descriptions.Item span={3}>
                <span className="total-payment">Total</span>
              </Descriptions.Item>
              <Descriptions.Item>
                <span className="total-payment">{Helper.currency(listCheckout.listCart.gross_amount, 'Rp. ', '')}</span>
              </Descriptions.Item>
            </Descriptions>
          </Row>
        </Card>
      )}
      <Card className="mt-5 mb-4">
        <Row gutter={[24, 24]} className="mt-2 ml-2">
          <span>Tipe Pembayaran yang akan di gunakan</span>
        </Row>
        <Radio.Group
          size="large"
          className="w-100"
        >
          {!statePayment.paymentLoad && (statePayment.paymentList.map(item => (
            <Row className="mb-3 mt-2">
              <Col xl={2} className="mt-5">
                <Radio
                  value={item.id}
                  id={item.slug}
                  name={item.slug}
                  checked={selectPayment === item.id}
                  onChange={() => onChangeRadio(item.id)}
                />
              </Col>
              <Col xl={6}>
                <img src="/assets/icon-payment.svg" width="150px" alt="" />
              </Col>
              <Col xl={16} className="mt-5">
                <p>
                  Pembayaran menggunakan
                  {' '}
                  {item.name}
                </p>
                <img src={item.logo_url} width="80px" alt="Logo" />
              </Col>
            </Row>
          )))}
        </Radio.Group>
        <form
          method="post"
          style={{ display: 'none' }}
          action={config.URL_PAYMENT_BCA}
        >
          <input
            type="text"
            id="merchantID"
            name="merchantID"
            value={detailPayment.merchant_id || ''}
          />
          <input
            type="text"
            id="invoiceNumber"
            name="invoiceNumber"
            value={detailPayment.invoice_number || ''}
          />
          <input
            type="text"
            id="productDescription"
            name="productDescription"
            value={detailPayment.product_description || ''}
          />
          <input
            type="text"
            id="cardToken"
            name="cardToken"
            value={detailPayment.card_token || ''}
          />
          <input
            type="text"
            id="storedCard"
            name="storedCard"
            value={detailPayment.stored_card || ''}
          />
          <input
            type="text"
            id="amount"
            name="amount"
            value={detailPayment.amount || ''}
          />
          <input
            type="text"
            id="specialFare"
            name="specialFare"
            value={detailPayment.special_fare || ''}
          />
          <input
            type="text"
            id="paymentChannel"
            name="paymentChannel"
            value={detailPayment.payment_channel || ''}
          />
          <input
            type="text"
            id="nonSecure"
            name="nonSecure"
            value={detailPayment.nonsecure || ''}
          />
          <input
            type="text"
            id="promotionCode"
            name="promotionCode"
            value={detailPayment.promotion_code || ''}
          />
          <input
            type="text"
            id="promotionOrigin"
            name="promotionOrigin"
            value={detailPayment.promotion_origin || ''}
          />
          <input
            type="text"
            id="installmentCode"
            name="installmentCode"
            value={detailPayment.installment_code || ''}
          />
          <input
            type="text"
            id="installmentTenor"
            name="installmentTenor"
            value={detailPayment.installment_tenor || ''}
          />
          <input
            type="text"
            id="hashValue"
            name="hashValue"
            value={detailPayment.hash_value || ''}
          />
          <input
            type="submit"
            id="btnSubmitValue"
            name="btnSubmit"
            value="Submit"
          />
        </form>
        <Row className="mt-3 mb-3">
          <Col span={24} className="d-flex justify-content-end align-items-center">
            <Button
              ghost
              type="primary"
              onClick={() => history.goBack()}
              className="button-lg w-25 border-lg mr-3"
            >
              Cancel
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              className="button-lg w-25"
              onClick={handlePayment}
            >
              {loading ? 'Loading...' : 'Bayar'}
            </Button>
          </Col>
        </Row>
      </Card>
    </React.Fragment>
  )
}

Checkout.propTypes = {
  listCheckout: PropTypes.object,
  statePayment: PropTypes.object,
  onChangeRadio: PropTypes.func,
  selectPayment: PropTypes.func,
  handlePayment: PropTypes.func,
  detailPayment: PropTypes.object,
  loading: PropTypes.bool,
}

export default Checkout
