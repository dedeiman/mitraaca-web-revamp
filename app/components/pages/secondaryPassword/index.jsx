import React from 'react'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import {
  Divider,
  Input, Button,
  Card, Alert, Row, Col,
} from 'antd'
import Helper from 'utils/Helper'

const SecondaryPasswordCheck = ({
  onSubmit, form, isLoading, stateError,
}) => {
  const { getFieldDecorator } = form
  const isTablet = window.innerWidth < 1100

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Check Secondary Password</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24} justify="center" className="mb-5">
          <Col xs={24} md={12}>
            <Card className="h-100">
              <p className="title-card mb-4">Input Secondary Password</p>
              <Form.Item {...stateError.password}>
                {getFieldDecorator('password', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Password'),
                  ],
                  initialValue: undefined,
                })(
                  <Input.Password
                    placeholder="Masukan Password..."
                    className="field-lg"
                    maxLength={50}
                    iconRender={visible => (
                      visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                    )}
                  />,
                )}
              </Form.Item>
              {stateError.message && (
                <Alert
                  message=""
                  description={stateError.message}
                  type="error"
                  closable
                  className="mb-3 text-left"
                />
              )}
              <div className={`${!isTablet ? 'd-flex justify-content-between align-items-center' : 'float-right'}`}>
                <Form.Item>
                  <Button type={`${!isTablet ? 'primary button-lg float-right mt-4' : 'primary button-lg float-right mt-2'}`} htmlType="submit" disabled={isLoading}>
                    Submit Second Password
                  </Button>
                </Form.Item>
              </div>
            </Card>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

SecondaryPasswordCheck.propTypes = {
  form: PropTypes.any,
  isLoading: PropTypes.bool,
  onSubmit: PropTypes.func,
  stateError: PropTypes.object,
  setStateError: PropTypes.func,
  onVerify: PropTypes.func,
  currentUser: PropTypes.object,
  sendEmail: PropTypes.func,
  isVisible: PropTypes.bool,
  toggleVisible: PropTypes.func,
  fetchForgot: PropTypes.bool,
  groupRole: PropTypes.object,
}

export default SecondaryPasswordCheck
