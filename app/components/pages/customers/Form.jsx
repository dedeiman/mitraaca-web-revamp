/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-promise-reject-errors */

import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Row, Col,
  Card, Input,
  DatePicker, Select,
  Tag,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { LeftOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'
import NumberFormat from 'react-number-format'

const FormCustomer = ({
  detailCustomer, stateCountry, stateCountryCode,
  stateSelects, onSubmit, handleProfileId,
  form, match, handleLocation,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  const incomeRange = !isEmpty(detailCustomer.income_range) ? detailCustomer.income_range.name : ''
  let rulesID = []
  if (getFieldValue('citizenship') === 'wna') {
    rulesID = [{ pattern: /^[A-Z0-9]+$/, message: '*wajib menggunakan huruf kapital atau angka' }]
  } else {
    rulesID = [{ pattern: /^(?=.{16,}$).*/, message: '*Wajib 16 karakter' }]
  }
  const isIndividu = getFieldValue('customer_type') === 'individu'
  const isCorporate = getFieldValue('customer_type') === 'corporate'
  const isMobile = window.innerWidth < 768

  const prefixSelector = getFieldDecorator('phone_code_id', {
    initialValue: !isEmpty(detailCustomer) ? (detailCustomer.phone_code) ? (detailCustomer.phone_code.phone_code) : '+62' : '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountry.list || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  const prefixSelectorCode = getFieldDecorator('pic_phone_country_code', {
    initialValue: !isEmpty(detailCustomer) ? (detailCustomer.pic_phone_code) ? (detailCustomer.pic_phone_code.phone_code) : '+62' : '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountryCode.list || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center mt-3">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Registrasi'} Customer`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={[24, 24]} className="mb-5">
          <Col span={isMobile ? 24 : 12} className="mb-4">
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card>
                  <p className="title-card">General Info Calon Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Type Customer</p>
                      <Form.Item>
                        {getFieldDecorator('customer_type', {
                          rules: Helper.fieldRules(['required'], 'Type Customer'),
                          initialValue: detailCustomer.customer_type || 'individu',
                          getValueFromEvent: (val) => {
                            if (val === 'individu') {
                              setFieldsValue({
                                pic_name: detailCustomer.pic_name || '',
                                pic_phone_number: detailCustomer.pic_phone_number || '',
                                gender: detailCustomer.gender || undefined,
                                type_id: detailCustomer.type_id || undefined,
                                martial_status: detailCustomer.martial_status || undefined,
                                employment: detailCustomer.employment || undefined,
                                number_id: detailCustomer.number_id || '',
                                dob: !isEmpty(detailCustomer) ? moment(detailCustomer.dob) : undefined,
                                birthplace: detailCustomer.birthplace || '',
                              })
                            } else {
                              setFieldsValue({
                                pic_name: '',
                                pic_phone_number: '',
                                gender: undefined,
                                type_id: undefined,
                                martial_status: undefined,
                                employment: undefined,
                                number_id: '',
                                dob: undefined,
                                birthplace: '',
                              })
                            }

                            return val
                          },
                        })(
                          <Select
                            placeholder="Select Type"
                            className="field-lg"
                          >
                            {[{ id: 'individu', name: 'INDIVIDU' }, { id: 'corporate', name: 'CORPORATE' }].map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kewarganegaraan</p>
                      <Form.Item>
                        {getFieldDecorator('citizenship', {
                          rules: Helper.fieldRules(['required'], 'Kewarganegaraan'),
                          initialValue: detailCustomer.citizenship || 'wni',
                          getValueFromEvent: (val) => {
                            setFieldsValue({
                              type_id: (val === 'wni') ? 'ktp' : 'passport',
                              number_id: (val !== 'wni') ? detailCustomer.number_id : '',
                              expiry_date_identity_id: (val !== 'wni') ? (!isEmpty(detailCustomer.expiry_date_identity_id) ? moment(detailCustomer.expiry_date_identity_id, 'YYYY-MM-DD') : '') : '',
                            })

                            return val
                          },
                        })(
                          <Select
                            disabled={!isIndividu}
                            placeholder="Select Kewarganegaraan"
                            className="field-lg"
                          >
                            {[{ id: 'wni', name: 'WNI' }, { id: 'wna', name: 'WNA' }].map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Nomor Customer</p>
                      <Form.Item>
                        {getFieldDecorator('customer_number', {
                          initialValue: detailCustomer.customer_number || '',
                        })(
                          <Input
                            disabled
                            className="field-lg"
                            placeholder="Auto Generate"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">No Izin Usaha</p>
                      <Form.Item>
                        {getFieldDecorator('business_license_no', {
                          rules: isCorporate ? Helper.fieldRules(['required', 'number'], 'Institution Types') : null,
                          initialValue: detailCustomer.business_license_no || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled={!isCorporate}
                            className="field-lg uppercase"
                            placeholder="No Izin Usaha"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Sumber Dana</p>
                        <Form.Item>
                          {getFieldDecorator('fund_source_id', {
                            rules: [
                              ...Helper.fieldRules(['required'], 'Sumber Dana'),
                            ],
                            initialValue: match.params.id && detailCustomer.fund_source ? detailCustomer.fund_source.id : detailCustomer.fund_source_id || undefined,
                          })(
                            <Select
                              className="field-lg"
                              placeholder="Select Sumber Dana"
                              loading={stateSelects.fund}
                            >
                              {(stateSelects.fundList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Institution Types</p>
                        <Form.Item>
                          {getFieldDecorator('institution_type_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Institution Types') : null,
                            initialValue: match.params.id && detailCustomer.institution_type ? detailCustomer.institution_type.id : detailCustomer.institution_type_id || undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Institution Types"
                              loading={stateSelects.institution}
                            >
                              {(stateSelects.institutionList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Maksud dan Tujuan Usaha</p>
                        <Form.Item>
                          {getFieldDecorator('business_purpose_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Maksud dan Tujuan Usaha') : null,
                            initialValue: match.params.id && detailCustomer.business_purpose ? detailCustomer.business_purpose.id : detailCustomer.business_purpose_id || undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Maksud dan Tujuan Usaha"
                              loading={stateSelects.purpose}
                            >
                              {(stateSelects.businessPurposesList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Bidang Usaha</p>
                        <Form.Item>
                          {getFieldDecorator('business_type_id', {
                            rules: isCorporate ? Helper.fieldRules(['required'], 'Bidang Usaha') : null,
                            initialValue: match.params.id && detailCustomer.business_type ? detailCustomer.business_type.id : detailCustomer.business_type_id || undefined,
                          })(
                            <Select
                              disabled={!isCorporate}
                              className="field-lg"
                              placeholder="Select Bidang Usaha"
                              loading={stateSelects.business}
                            >
                              {(stateSelects.businessList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Type ID</p>
                      <Form.Item>
                        {getFieldDecorator('type_id', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Type ID') : null,
                          initialValue: detailCustomer.type_id || 'ktp',
                        })(
                          <Select
                            disabled={!isIndividu}
                            placeholder="Select Type ID"
                            className="field-lg"
                          >
                            {[
                              { id: 'ktp', name: 'KTP' },
                              { id: 'kitas', name: 'KITAS' },
                              { id: 'kitap', name: 'KITAP' },
                              { id: 'passport', name: 'Passport' },
                            ].map(item => (
                              <Select.Option
                                key={item.id}
                                value={item.id}
                                disabled={(getFieldValue('citizenship') === 'wna') ? (item.id === 'ktp') : (item.id !== 'ktp')}
                              >
                                {item.name}
                              </Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Nomor ID</p>
                      <Form.Item>
                        {getFieldDecorator('number_id', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('customer_type') === 'corporate' ? [] : ['required'], 'Nomor ID'),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (!(/^[0-9+]+$/).test(value)) {
                                    return Promise.reject('*Nomor ID harus menggunakan angka')
                                  }

                                  if (!value) return Promise.resolve()
                                  if (/^.{16,30}$/.test(value) === false) return Promise.reject('*Wajib 16 Character')

                                  return Promise.resolve()
                                },
                              },
                            ],
                          initialValue: detailCustomer.number_id || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            disabled={getFieldValue('customer_type') === 'corporate'}
                            className="field-lg uppercase"
                            maxLength={getFieldValue('citizenship') === 'wni' ? 16 : 30}
                            placeholder="Number ID"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Nomor NPWP</p>
                      <Form.Item>
                        {getFieldDecorator('npwp', {
                          rules: [
                            ...Helper.fieldRules(isIndividu ? [] : (['required']), 'Nomor NPWP'),
                            {
                              validator: (rule, value) => {
                                let npwp = value
                                npwp = npwp.split('-').join('')
                                npwp = npwp.split('.').join('')
                                npwp = npwp.split(' ').join('')

                                if (!npwp) return Promise.resolve()
                                if (/^.{15,30}$/.test(npwp) === false) return Promise.reject('*Minimal 15 Character')

                                return Promise.resolve()
                              },
                            },
                          ],
                          initialValue: detailCustomer.npwp || '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') {
                              handleProfileId(value)
                              return value
                            }
                            return value
                          },
                        })(
                          <NumberFormat
                            className="field-lg ant-input"
                            placeholder="Input Nomor NPWP"
                            format="##.###.###.#-###.########"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    {/* {(getFieldValue('citizenship') === 'wni') && (
                      <Col xs={24} md={6} />
                    )} */}
                    <Col xs={24} md={12}>
                      <p className="mb-0">Tanggal Habis Berlaku</p>
                      <Form.Item>
                        {getFieldDecorator('expiry_date_identity_id', {
                          rules: getFieldValue('citizenship') !== 'wni' ? Helper.fieldRules(['required'], 'Tanggal Habis Berlaku') : null,
                          initialValue: !isEmpty(detailCustomer.expiry_date_identity_id) ? moment(detailCustomer.expiry_date_identity_id, 'YYYY-MM-DD') : undefined,
                        })(
                          <DatePicker
                            className="field-lg w-100"
                            placeholder="Tanggal Habis Berlaku"
                            disabled={getFieldValue('citizenship') !== 'wna'}
                            disabledDate={current => (current && (current <= moment().subtract(1, 'day')))}
                            format="DD MMMM YYYY"
                            allowClear
                            onChange={() => {
                              if (getFieldValue('citizenship') === 'wni') {
                                setFieldsValue({
                                  expiry_date_identity_id: '',
                                })
                              }
                            }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      {(!isCorporate) && (
                        <div>
                          <p className="mb-0">Nama</p>
                          <Form.Item>
                            {getFieldDecorator('name', {
                              rules: Helper.fieldRules(['required', 'notSpecial'], 'Nama'),
                              initialValue: (detailCustomer.name || ''),
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                placeholder="Input Nama"
                                type="text"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                      {(isCorporate) && (
                        <div>
                          <p className="mb-0">Nama Perusahaan</p>
                          <Form.Item>
                            {getFieldDecorator('name', {
                              rules: Helper.fieldRules(['required', 'notSpecial'], 'Nama Perusahaan'),
                              initialValue: (detailCustomer.name || ''),
                              getValueFromEvent: e => e.target.value,
                            })(
                              <Input
                                className="field-lg uppercase"
                                placeholder="Input Nama Perusahaan"
                                type="text"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Marital Status</p>
                      <Form.Item>
                        {getFieldDecorator('martial_status', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Marital Status') : null,
                          initialValue: detailCustomer.martial_status || undefined,
                        })(
                          <Select
                            disabled={!isIndividu}
                            className="field-lg"
                            placeholder="Select Status"
                            loading={stateSelects.maritalLoad}
                          >
                            {(stateSelects.maritalList || []).map(item => (
                              <Select.Option key={item.id} value={item.name}>{(item.display_name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Gender</p>
                      <Form.Item>
                        {getFieldDecorator('gender', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Gender') : null,
                          initialValue: (detailCustomer.gender || '').toUpperCase() || undefined,
                          getValueFromEvent: e => e,
                        })(
                          <Select
                            disabled={!isIndividu}
                            placeholder="Select Gender"
                            className="field-lg"
                          >
                            {[{ id: 'laki-laki', name: 'MALE' }, { id: 'perempuan', name: 'FEMALE' }].map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      {(isCorporate) && (
                        <div>
                          <p className="mb-0">Tanggal Pendirian</p>
                          <Form.Item>
                            {getFieldDecorator('dob', {
                              rules: Helper.fieldRules(['required'], 'Tanggal Pendirian'),
                              initialValue: !isEmpty(detailCustomer.dob) ? moment(detailCustomer.dob) : undefined,
                            })(
                              <DatePicker
                                className="field-lg w-100"
                                placeholder="Input Tanggal Pendirian"
                                disabledDate={current => (current && (current > moment().subtract(0, 'day')))}
                                format="DD MMMM YYYY"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                      {(!isCorporate) && (
                        <div>
                          <p className="mb-0">Tanggal Lahir</p>
                          <Form.Item>
                            {getFieldDecorator('dob', {
                              rules: Helper.fieldRules(['required'], 'Tanggal Lahir'),
                              initialValue: !isEmpty(detailCustomer.dob) ? moment(detailCustomer.dob) : undefined,
                            })(
                              <DatePicker
                                className="field-lg w-100"
                                placeholder="Input Tanggal Lahir"
                                defaultPickerValue={moment().subtract(17, 'year')}
                                disabledDate={current => (current && (current > moment().subtract(0, 'day')))}
                                format="DD MMMM YYYY"
                              />,
                            )}
                          </Form.Item>
                        </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      {(isCorporate) && (
                          <div>
                            <p className="mb-0">Tempat Pendirian</p>
                            <Form.Item>
                              {getFieldDecorator('birthplace', {
                                rules: Helper.fieldRules(['required'], 'Tempat Pendirian'),
                                initialValue: detailCustomer.birthplace || undefined,
                                getValueFromEvent: e => e.target.value,
                              })(
                                <Input
                                  className="field-lg uppercase"
                                  type="text"
                                  placeholder="Input Tempat Pendirian"
                                />,
                              )}
                            </Form.Item>
                          </div>
                      )}
                      {(!isCorporate) && (
                          <div>
                            <p className="mb-0">Tempat Lahir</p>
                            <Form.Item>
                              {getFieldDecorator('birthplace', {
                                rules: Helper.fieldRules(['required'], 'Tempat Lahir'),
                                initialValue: detailCustomer.birthplace || undefined,
                                getValueFromEvent: e => e.target.value,
                              })(
                                <Input
                                  className="field-lg uppercase"
                                  type="text"
                                  placeholder="Input Tempat Lahir"
                                />,
                              )}
                            </Form.Item>
                          </div>
                      )}
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Employment</p>
                      <Form.Item>
                        {getFieldDecorator('employment', {
                          rules: isIndividu ? Helper.fieldRules(['required'], 'Employment') : null,
                          initialValue: detailCustomer.employment || undefined,
                          getValueFromEvent: (e) => {
                            if (e !== 'lainnya') {
                              setFieldsValue({
                                other_employment: '',
                              })
                            }

                            return e
                          },
                        })(
                          <Select
                            disabled={!isIndividu}
                            className="field-lg"
                            placeholder="Select Employment"
                            loading={stateSelects.employmentLoad}
                          >
                            {(stateSelects.employmentList || []).map(item => (
                              <Select.Option key={item.id} value={item.name} disabled={getFieldValue('citizenship') === 'wna' && item.name === 'pegawai_negeri_sipil'}>{(item.display_name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    {getFieldValue('employment') === 'lainnya' && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Other Employment</p>
                        <Form.Item>
                          {getFieldDecorator('other_employment', {
                            rules: Helper.fieldRules(['required'], 'Other Employment'),
                            initialValue: detailCustomer.other_employment ? detailCustomer.other_employment : '',
                            getValueFromEvent: (e) => {
                              const { value } = e.target
                              if (value !== '') return value
                              return value
                            },
                          })(
                            <Input
                              size="large"
                              placeholder="Other Employment"
                              style={{ textTransform: 'uppercase' }}
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    <Col xs={24} md={12}>
                      <p className="mb-0">Range Penghasilan</p>
                      <Form.Item>
                        {getFieldDecorator('income_range_id', {
                          initialValue: !isEmpty(incomeRange) ? (detailCustomer.income_range_id || undefined) : undefined,
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            optionFilterProp="children"
                            placeholder="Select Range Income"
                            loading={stateSelects.incomeLoad}
                          >
                            {(stateSelects.incomeList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.display_name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">PIC Name</p>
                      <Form.Item>
                        {getFieldDecorator('pic_name', {
                          rules: [
                            ...Helper.fieldRules(getFieldValue('customer_type') === 'individu' ? [] : ['required', 'notSpecial'], 'PIC Name'),
                          ],
                          initialValue: detailCustomer.pic_name || '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') return value
                            return value
                          },
                        })(
                          <Input
                            placeholder="Input PIC Name"
                            className="field-lg uppercase"
                            disabled={getFieldValue('customer_type') === 'individu'}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">PIC Phone</p>
                      <Form.Item>
                        {getFieldDecorator('pic_phone_number', {
                          rules: [
                            ...Helper.fieldRules(getFieldValue('customer_type') === 'individu' ? [] : ['required'], 'PIC Phone'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                if (!(/^[0-9+]+$/).test(value)) {
                                  return Promise.reject('*PIC phone harus menggunakan angka')
                                }

                                if (value.charAt(0) == 0) {
                                  return Promise.reject('*Cannot input first number with "0"')
                                }

                                if (value.length < 8) {
                                  return Promise.reject('*Minimal 8 digit')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                          initialValue: (detailCustomer.pic_phone_number || ''),
                        })(
                          <Input
                            className="field-lg"
                            maxLength={16}
                            size="large"
                            addonBefore={prefixSelectorCode}
                            placeholder="Input PIC Phone Number"
                            disabled={getFieldValue('customer_type') === 'individu'}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Card className="h-100">
                  <p className="title-card">Alamat Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-2">Alamat</p>
                      <Form.Item>
                        {getFieldDecorator('address', {
                          rules: Helper.fieldRules(['required'], 'Alamat'),
                          initialValue: detailCustomer.address || '',
                        })(
                          <Input
                            rows={3}
                            size="large"
                            placeholder="Input Address"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-2">RT</p>
                      <Form.Item>
                        {getFieldDecorator('rt', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'RT'),
                            { pattern: /^.{0,3}$/, message: '*Maksimal 3 karakter' },
                          ],
                          initialValue: detailCustomer.rt || '',
                        })(
                          <Input
                            className="field-lg"
                            placeholder="RT"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={12} md={12}>
                      <p className="mb-2">RW</p>
                      <Form.Item>
                        {getFieldDecorator('rw', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'RW'),
                            { pattern: /^.{0,3}$/, message: '*Maksimal 3 karakter' },
                          ],
                          initialValue: detailCustomer.rw || '',
                        })(
                          <Input
                            className="field-lg"
                            placeholder="RW"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Provinsi</p>
                      <Form.Item>
                        {getFieldDecorator('province_id', {
                          rules: Helper.fieldRules(['required'], 'Provinsi'),
                          getValueFromEvent: (val) => {
                            setFieldsValue({
                              city_id: undefined,
                              sub_district_id: undefined,
                              urban_village_id: undefined,
                            })
                            handleLocation(`/cities/${val}`, 'city')
                            return val
                          },
                          initialValue: detailCustomer.province_id || undefined,
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            placeholder="Pilih Provinsi"
                            optionFilterProp="children"
                            loading={stateSelects.provincyLoad}
                          >
                            {(stateSelects.provincyList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kota</p>
                      <Form.Item>
                        {getFieldDecorator('city_id', {
                          rules: Helper.fieldRules(['required'], 'Kota'),
                          getValueFromEvent: (val) => {
                            setFieldsValue({
                              sub_district_id: undefined,
                              urban_village_id: undefined,
                            })
                            handleLocation(`/sub-district/${val}`, 'subDistrict')
                            return val
                          },
                          initialValue: detailCustomer.city_id || undefined,
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            placeholder="Pilih Kota"
                            optionFilterProp="children"
                            loading={stateSelects.cityLoad}
                            disabled={!getFieldValue('province_id')}
                          >
                            {(stateSelects.cityList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kecamatan</p>
                      <Form.Item>
                        {getFieldDecorator('sub_district_id', {
                          getValueFromEvent: (val) => {
                            setFieldsValue({ urban_village_id: undefined })
                            handleLocation(`/villages/${val}`, 'villages')
                            return val
                          },
                          initialValue: detailCustomer.sub_district_id || undefined,
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            optionFilterProp="children"
                            placeholder="Pilih Kecamatan"
                            loading={stateSelects.subDistrictLoad}
                            disabled={!getFieldValue('city_id')}
                          >
                            {(stateSelects.subDistrictList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kelurahan</p>
                      <Form.Item>
                        {getFieldDecorator('urban_village_id', {
                          initialValue: detailCustomer.urban_village_id || undefined,
                        })(
                          <Select
                            showSearch
                            className="field-lg"
                            optionFilterProp="children"
                            placeholder="Pilih Kelurahan"
                            loading={stateSelects.villagesLoad}
                            disabled={!getFieldValue('sub_district_id')}
                          >
                            {(stateSelects.villagesList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Kode Pos</p>
                      <Form.Item>
                        {getFieldDecorator('postal_code', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Kode Pos'),
                            { pattern: /^.{0,5}$/, message: '*Maksimal 5 karakter' },
                          ],
                          initialValue: detailCustomer.postal_code || '',
                        })(
                          <Input
                            className="field-lg"
                            maxLength="5"
                            placeholder="Input Kode Pos"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Latitude</p>
                      <Form.Item>
                        {getFieldDecorator('lat', {
                          initialValue: detailCustomer.lat || '',
                        })(
                          <Input
                            className="field-lg"
                            placeholder="Input Latitude"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-2">Longitude</p>
                      <Form.Item>
                        {getFieldDecorator('lng', {
                          initialValue: detailCustomer.lng || '',
                        })(
                          <Input
                            className="field-lg"
                            placeholder="Input Longitude"
                            disabled={isEmpty(getFieldValue('address'))}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col xs={24}>
                <Card className="h-100">
                  <p className="title-card">General Info Customer</p>
                  <Row gutter={24}>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Phone Number</p>
                      <Form.Item>
                        {getFieldDecorator('phone_number', {
                          rules: [
                            ...Helper.fieldRules(['required'], 'Phone Number'),
                            {
                              validator: (rule, value) => {
                                if (!value) return Promise.resolve()

                                if (!(/^[0-9+]+$/).test(value)) {
                                  return Promise.reject('*Phone number harus menggunakan angka')
                                }

                                if (value.charAt(0) == 0) {
                                  return Promise.reject('*Cannot input first number with "0"')
                                }

                                if (value.length < 8) {
                                  return Promise.reject('*Minimal 8 digit')
                                }

                                return Promise.resolve()
                              },
                            },
                          ],
                          initialValue: (detailCustomer.phone_number || ''),
                        })(
                          <Input
                            className="field-lg"
                            maxLength={16}
                            size="large"
                            addonBefore={prefixSelector}
                            placeholder="Input Phone Number"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Email</p>
                      <Form.Item>
                        {getFieldDecorator('email', {
                          rules: Helper.fieldRules(['required', 'email'], 'Email'),
                          initialValue: detailCustomer.email || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            className="field-lg uppercase"
                            placeholder="Input Email Address"
                            style={{ textTransform: 'uppercase' }}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col span={24}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="button-lg w-25 mr-3"
                >
                  {match.params.id ? 'Save' : 'Submit'}
                </Button>
                <Button
                  ghost
                  type="primary"
                  className="button-lg w-25 border-lg"
                  onClick={() => history.goBack()}
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormCustomer.propTypes = {
  form: PropTypes.any,
  match: PropTypes.object,
  onSubmit: PropTypes.func,
  stateCountry: PropTypes.object,
  stateCountryCode: PropTypes.object,
  stateSelects: PropTypes.object,
  handleLocation: PropTypes.func,
  detailCustomer: PropTypes.object,
  handleProfileId: PropTypes.func,
}

export default FormCustomer
