import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions,
  Pagination, Tooltip,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const ContestList = ({
  currentUser, dataCustomer,
  handlePage, metaCustomer,
  stateCustomer, setStateCustomer,
  loadCustomer, handleFilter,
  isFetching, updatePerPage,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="pt-3">
        <Col xs={24} md={5} xl={5}>
          <Select
            className="field-lg w-100"
            defaultValue=""
            onChange={handleFilter}
            value={stateCustomer.filter}
            loading={stateCustomer.loading}
          >
            {[{
              id: '',
              display_name: '- ALL -',
            }, {
              id: 'name',
              display_name: 'Name',
            }, {
              id: 'number_id',
              display_name: 'Number ID',
            }, {
              id: 'customer_number',
              display_name: 'No Nasabah',
            }, {
              id: 'email',
              display_name: 'Email',
            }, {
              id: 'phone_number',
              display_name: 'Phone Number',
            }].map(item => (
              <Select.Option key={item.id} value={item.id}>{item.display_name}</Select.Option>
            ))}
          </Select>
        </Col>
        <Col xs={24} md={5} xl={16}>
          {(currentUser.permissions && currentUser.permissions.indexOf('customer-search') > -1) && (
            <Input
              allowClear
              placeholder={`Search Customer${!isEmpty(stateCustomer.filter) ? ' by' : ''} ${capitalize((stateCustomer.filter).split('_').join(' '))}...`}
              className="field-lg"
              value={stateCustomer.search}
              onChange={(e) => {
                setStateCustomer({
                  ...stateCustomer,
                  search: e.target.value.toUpperCase(),
                })
              }}
              onPressEnter={() => loadCustomer(true)}
            />
          )}
        </Col>
        <Col xs={24} md={5} xl={3}>
          {(currentUser.permissions && currentUser.permissions.indexOf('customer-search') > -1) && (
            <Button type="primary" className="button-lg w-100" onClick={() => loadCustomer(true)}>
              Search
            </Button>
          )}
        </Col>
        <Col span={14}>
          {/* <Form.Item label="Filter">
            <Select
              onChange={handleFilter}
              allowClear
              value={stateCustomer.filter || undefined}
              style={{ width: '350px' }}
              placeholder="Select Customer Type"
              className="field-lg"
            >
              {[{ id: 'candidate_customer', name: 'Calon Customer' }, { id: 'customer', name: 'Customer' }].map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Form.Item> */}
        </Col>
        {(currentUser.permissions && currentUser.permissions.indexOf('customer-register') > -1) && (
        <Col span={10} align="end" className="d-flex justify-content-end">
          <Button
            ghost
            type="primary"
            className="px-4 border-lg d-flex align-items-center"
            onClick={() => history.push('/customer-menu/add')}
          >
            <img src="/assets/plus.svg" alt="plus" className="mr-1" />
            Register Customer
          </Button>
          {/* <Button
            ghost
            type="primary"
            className="px-4 border-lg d-flex align-items-center ml-4"
            onClick={() => history.push('/customer-menu/add')}
          >
            <img src="/assets/plus.svg" alt="plus" className="mr-1" />
            Register Calon Customer
          </Button> */}
        </Col>
        )}
      </Row>

      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataCustomer)) && (
          <Empty
            description="Tidak ada data."
          />
        )}
        {!isEmpty(dataCustomer)
          ? (
            dataCustomer.map((item, idx) => (
              <React.Fragment key={`list-faq-${item.id}`}>
                <Row gutter={24}>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="No Nasabah" className="px-1">{item.customer_number || '-'}</Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Name" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                          {item.name.toUpperCase() || '-'}
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={2} xl={2}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={isMobile ? 15 : 1} label="Type ID" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                          <Tooltip
                            title={item.type_id.toUpperCase() || '-'}
                            >
                            {item.type_id.toUpperCase() || '-'}
                          </Tooltip>
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Number ID" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                          <Tooltip
                            title={item.number_id || '-'}
                            >
                            {item.number_id || '-'}
                          </Tooltip>
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={3} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Phone Number" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                          <Tooltip
                            title={item.phone_number || '-'}
                            >
                            {item.phone_number || '-'}
                          </Tooltip>
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={3} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item label="Email" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                            <Tooltip
                              title={item.email || '-'}
                              >
                              {item.email || '-'}
                            </Tooltip>
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={4} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item zspan={isMobile ? 15 : 3} className="px-1 text-center desc-action">
                            {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
                                <Button
                                type="primary"
                                className="d-flex justify-content-between align-items-center w-100"
                                onClick={() => history.push(`/customer-menu/${item.id}`)}
                                >
                                View Profile
                                {' '}
                                <i className="las la-user" style={{ fontSize: '20px' }} />
                                </Button>
                            )}
                            {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
                            <React.Fragment>
                                <Button
                                    type="primary"
                                    className="w-100 mt-2"
                                    onClick={() => history.push(`/customer-menu/${item.id}/edit`)}
                                >
                                    Edit
                                </Button>
                                <Button
                                    type="primary"
                                    className="w-100 mt-2"
                                    onClick={() => history.push(`/customer-menu/${item.id}`)}
                                >
                                    Detail
                                </Button>
                            </React.Fragment>
                            )}
                        </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
                {(idx !== (dataCustomer.length - 1)) && (
                  <Divider />
                )}
              </React.Fragment>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        simple={isMobile}
        pageSize={metaCustomer.per_page || 10}
        current={stateCustomer.page ? Number(stateCustomer.page) : 1}
        onChange={handlePage}
        total={metaCustomer.total_count}
        onShowSizeChange={updatePerPage}
        showSizeChanger={false}
      />
    </React.Fragment>
  )
}

ContestList.propTypes = {
  currentUser: PropTypes.object,
  dataCustomer: PropTypes.array,
  metaCustomer: PropTypes.object,
  stateCustomer: PropTypes.object,
  handlePage: PropTypes.func,
  loadCustomer: PropTypes.func,
  setStateCustomer: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  handleFilter: PropTypes.func,
}

export default ContestList
