import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/customers/List'
import Detail from 'containers/pages/customers/Detail'
import Form from 'containers/pages/customers/Form'
import history from 'utils/history'

const Customer = ({
  currentUser, location, match,
  stateCheck,
}) => {
  if (location.pathname === '/customer-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('customer-menu') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (match.params.id && !location.pathname.includes('/edit') && !location.pathname.includes('/upload')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) {
      return <Detail match={match} location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/customer-menu/add')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('customer-edit') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }

  return ''
}

Customer.propTypes = {
  currentUser: PropTypes.object,
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Customer
