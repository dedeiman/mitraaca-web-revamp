import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions,
  Typography,
  Pagination,
  Button, Select,
  Result, Empty,
} from 'antd'
import { Loader } from 'components/elements'
import { Form } from '@ant-design/compatible'
import { InboxOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const FAQ = ({
  currentUser, dataFaq,
  handlePage, metaFaq,
  stateFaq, setStateFaq,
  handleFilter, loadFaq,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const isTablet = window.innerWidth < 1100

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className="mt-4">
        <Col xs={24} md={18} xl={21}>
          {(currentUser.permissions && currentUser.permissions.indexOf('faq-search') > -1) && (
            <Input
              allowClear
              placeholder="Search by Keywords..."
              className="field-lg"
              value={stateFaq.search}
              onChange={(e) => {
                setStateFaq({
                  ...stateFaq,
                  search: (e.target.value || '').toUpperCase(),
                })
              }}
              onPressEnter={() => loadFaq(true)}
            />
          )}
        </Col>
        <Col md={6} xl={3} align="end">
          {(currentUser.permissions && currentUser.permissions.indexOf('faq-search') > -1) && (
            <Button type="primary" className="button-lg px-4 w-100" onClick={() => loadFaq(true)}>
              Search
            </Button>
          )}
        </Col>
        <Col xs={24} md={24} xl={20}>
          {(currentUser.permissions && currentUser.permissions.indexOf('faq-search') > -1) && (
            <Form.Item label="Category" style={isTablet ? { marginBottom: 0 } : ''}>
              <Select
                onChange={handleFilter}
                allowClear
                value={stateFaq.filter || undefined}
                style={isMobile ? { width: '100%' } : { width: '350px' }}
                loading={stateFaq.load}
                placeholder="Select Category"
                className={`field-lg ${isMobile ? 'w-100' : ''}`}
              >
                {(stateFaq.list || []).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          )}
        </Col>
        {(currentUser.permissions && currentUser.permissions.indexOf('faq-create') > -1) && (
        <Col align="end" xl={4}>
          <Button
            ghost
            type="primary"
            className="px-4 border-lg d-flex align-items-center w-100 justify-content-center"
            onClick={() => history.push('/faq-menu/add')}
          >
            <img src="/assets/plus.svg" alt="plus" className="mr-1" />
            Add FAQ
          </Button>
        </Col>
        )}
      </Row>

      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataFaq)) && (
          <Empty
            description="Tidak ada data."
          />
        )}
        {!isEmpty(dataFaq)
          ? (
            dataFaq.map((item, idx) => (
              <div>
                <Descriptions layout="vertical" colon={false} column={7}>
                  <Descriptions.Item span={isMobile ? 12 : 1} label="Category" className="px-1">{item.category.name}</Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 12 : 3} label="Question" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                      {item.question}
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 12 : 3} label="Answer" className="px-1">
                    <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                      <Link to={`/faq-menu/${item.id}`}>{item.answer}</Link>
                    </Typography.Paragraph>
                  </Descriptions.Item>
                  {(currentUser.permissions && currentUser.permissions.indexOf('faq-detail') > -1) && (
                    <Descriptions.Item span={isMobile ? 12 : 1} className="px-1">
                      <Button
                        type="primary"
                        className="d-flex justify-content-center align-items-center w-100"
                        onClick={() => history.push(`/faq-menu/${item.id}`)}
                      >
                        Detail
                        {' '}
                      </Button>
                    </Descriptions.Item>
                  )}
                </Descriptions>
                {(idx !== (dataFaq.length - 1)) && (<Divider />)}
              </div>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        current={stateFaq.page ? Number(stateFaq.page) : 1}
        onChange={handlePage}
        total={metaFaq.total_count}
        showSizeChanger={false}
        simple={isMobile}
      />
    </React.Fragment>
  )
}

FAQ.propTypes = {
  currentUser: PropTypes.object,
  dataFaq: PropTypes.array,
  metaFaq: PropTypes.object,
  stateFaq: PropTypes.object,
  handlePage: PropTypes.func,
  handleFilter: PropTypes.func,
  loadFaq: PropTypes.func,
  setStateFaq: PropTypes.func,
  isFetching: PropTypes.bool,
}

export default FAQ
