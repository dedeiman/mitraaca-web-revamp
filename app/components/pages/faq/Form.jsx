import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button,
  Card, Select,
  Input, Tag,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { LeftOutlined } from '@ant-design/icons'

const FormFAQ = ({
  isFetching, detailFaq,
  onSubmit, categories,
  form, match,
}) => {
  const { getFieldDecorator } = form
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={24} className={isMobile ? 'mb-3 mt-3' : 'mb-5'}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center mt-3">
          <div className="title-page">{`${match.params.id ? 'Edit' : 'Add'} FAQ`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24}>
          <Col xs={24} md={8}>
            <Row gutter={[24, 0]}>
              <Col span={24} className="mb-4">
                <Card className="h-100" loading={isFetching}>
                  <p className="title-card">Category</p>
                  <Form.Item>
                    {getFieldDecorator('category_id', {
                      rules: Helper.fieldRules(['required'], 'Category'),
                      initialValue: detailFaq.category ? detailFaq.category.id : undefined,
                    })(
                      <Select
                        allowClear
                        loading={categories.load}
                        placeholder="Select Category"
                        className="field-lg"
                      >
                        {(categories.list || []).map(item => (
                          <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                        ))}
                      </Select>,
                    )}
                  </Form.Item>
                </Card>
              </Col>
              <Col span={24} className={isMobile ? 'mb-4' : ''}>
                <Card className="h-100" loading={isFetching}>
                  <p className="title-card">Question</p>
                  <Form.Item>
                    {getFieldDecorator('question', {
                      rules: [
                        ...Helper.fieldRules(['required'], 'Question'),
                        { min: 10, message: '*Minimal 10 karakter\n' },
                        { max: 100, message: '*Maksimal 100 karakter\n' },
                        { pattern: /^[a-zA-Z0-9,?\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
                      ],
                      initialValue: detailFaq.question || '',
                    })(
                      <Input.TextArea
                        rows={4}
                        className="field-lg uppercase"
                        onBlur={e => (e.target.value).toUpperCase()}
                        placeholder="Input Question"
                      />,
                    )}
                  </Form.Item>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={16}>
            <Card className="h-100" loading={isFetching}>
              <p className="title-card">Answer</p>
              <Form.Item>
                {getFieldDecorator('answer', {
                  rules: [
                    ...Helper.fieldRules(['required'], 'Answer'),
                    { min: 10, message: '*Minimal 10 karakter\n' },
                    { max: 100, message: '*Maksimal 100 karakter\n' },
                    { pattern: /^[a-zA-Z0-9,?\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
                  ],
                  initialValue: detailFaq.answer || '',
                })(
                  <Input.TextArea
                    rows={12}
                    className="field-lg uppercase"
                    placeholder="Input Answer"
                  />,
                )}
              </Form.Item>
            </Card>
          </Col>
        </Row>
        <Row className="my-4">
          <Col span={24} className={isMobile ? 'mb-3 d-flex justify-content-end align-items-center' : 'd-flex justify-content-end align-items-center'}>
            <Button
              type="primary"
              htmlType="submit"
              className={isMobile ? 'button-lg mr-3' : 'button-lg w-15 mr-3'}
            >
              {match.params.id ? 'Save' : 'Submit'}
            </Button>
            <Button
              ghost
              type="primary"
              className={isMobile ? 'button-lg border-lg' : 'button-lg w-15 border-lg'}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormFAQ.propTypes = {
  detailFaq: PropTypes.object,
  isFetching: PropTypes.bool,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  categories: PropTypes.object,
  match: PropTypes.object,
}

export default FormFAQ
