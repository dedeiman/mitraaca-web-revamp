import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/faq/List'
import Detail from 'containers/pages/faq/Detail'
import Form from 'containers/pages/faq/Form'
import history from 'utils/history'

const FAQ = ({ currentUser, location, match, stateCheck }) => {
  if (location.pathname === '/faq-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('faq-list') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (match.params.id && !location.pathname.includes('/edit')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('faq-detail') > -1) {
      return <Detail match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('faq-edit') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname === '/faq-menu/add') {
    if (currentUser.permissions && currentUser.permissions.indexOf('faq-create') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }


  return ''
}

FAQ.propTypes = {
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default FAQ
