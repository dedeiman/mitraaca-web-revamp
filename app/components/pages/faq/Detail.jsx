import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Tag,
  Card, Popconfirm,
} from 'antd'
import {
  LeftOutlined, DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons'
import history from 'utils/history'

const DetailFAQ = ({
  isFetching, detailFaq,
  handleDelete, currentUser,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24} className={isMobile ? 'mt-4' : ''}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24}>
          <Row className="d-flex align-items-center">
            <Col span={isMobile ? 24 : 12}>
              <div className="title-page">Detail FAQ</div>
            </Col>
            <Col span={isMobile ? 24 : 12} className={isMobile ? 'd-flex justify-content-start' : 'd-flex justify-content-end'}>
              {(currentUser.permissions && currentUser.permissions.indexOf('faq-edit') > -1) && (
                <div className="button-action">
                  <Popconfirm
                    title="Anda akan menghapus data ini?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => handleDelete()}
                  >
                    <Button type="link">
                      <DeleteOutlined />
                      Hapus
                    </Button>
                  </Popconfirm>
                  <Button type="link" onClick={() => history.push(`/faq-menu/${detailFaq.id}/edit`)}>
                    <EditOutlined />
                    Edit
                  </Button>
                </div>
              )}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} md={8} className={isMobile ? 'mb-4' : ''}>
          <Card className="h-100" loading={isFetching}>
            <p className="title-card">Category</p>
            <p className="mb-5">{detailFaq.category ? detailFaq.category.name : '-'}</p>
            <p className="title-card">Question</p>
            <p>{detailFaq.question || '-'}</p>
          </Card>
        </Col>
        <Col xs={24} md={16}>
          <Card className="h-100" loading={isFetching}>
            <p className="title-card">Answer</p>
            <p>{detailFaq.answer || '-'}</p>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailFAQ.propTypes = {
  currentUser: PropTypes.object,
  detailFaq: PropTypes.object,
  isFetching: PropTypes.bool,
  handleDelete: PropTypes.func,
}

export default DetailFAQ
