import React, { useState } from 'react'
import {Modal, Button, Input, Col} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

export default function index({ form, onSubmit, currentUser }) {
  const [visible, setVisible] = useState(false)
  const [loading, setLoading] = useState(false)
  const { TextArea } = Input
  const { getFieldDecorator } = form
  return (
    <>
      {/* eslint-disable */}
      {(currentUser.role.group.name === 'agent') && (
      <img
        src="/assets/help.png"
        style={{
          position: 'absolute',
          bottom: '70px',
          right: '30px',
          cursor: 'pointer',
        }}
        alt="help"
        onClick={() => setVisible(true)}
      />
      )}
      {/* eslint-enable */}
      <Modal
        visible={visible}
        title="Pusat Bantuan"
        onOk={() => {
          setLoading(true)
          setTimeout(() => {
            setLoading(false)
            setVisible(false)
          }, 3000)
        }}
        onCancel={() => setVisible(false)}
        footer={null}
      >
        <Form onSubmit={onSubmit}>
          <p className="m-0">Recipient</p>
          <p className="font-weight-bold">{currentUser.branch_perwakilan ? currentUser.branch_perwakilan[0].name : '-'}</p>
          <p className="mb-0">Text Subject</p>
          <Form.Item>
            {getFieldDecorator('subject', {
              rules: Helper.fieldRules(['required'], 'Text Subject'),
            })(
              <Input
                maxLength={30}
                placeholder="Subject..."
                className="field-lg uppercase"
              />,
            )}
          </Form.Item>
          <p className="mb-0">Description</p>
          <Form.Item>
            {getFieldDecorator('announcement', {
              rules: Helper.fieldRules(['required'],'Description'),
            })(
              <TextArea
                placeholder="Description..."
                className="field-lg uppercase"
                maxLength={100}
                rows={4}
              />,
            )}
          </Form.Item>
          <Button
            key="submit"
            type="primary"
            loading={loading}
            htmlType="submit"
            onClick={() => {
              setLoading(true)
              setTimeout(() => {
                setLoading(false)
              }, 3000)
            }}
          >
            Kirim
          </Button>
          <Button key="back" className="ml-3" onClick={() => setVisible(false)}>
            Cancel
          </Button>
        </Form>
      </Modal>
    </>
  )
}
