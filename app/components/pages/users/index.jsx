import PropTypes from 'prop-types'
import {
  Card, Table, Popconfirm,
  Button, Avatar, Tag,
  Alert, Input, Row, Col,
} from 'antd'
import history from 'utils/history'
import moment from 'moment'

const UserPage = ({
  handleDelete, handleSearch,
  metaUser, dataUser,
  isFetching, handlePage,
  errorMessage, closeError,
  keyword, currentPage, setKeyword,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'profile_pic',
      key: 'profile_pic',
      // width: 100,
      render: text => (
        text
          ? <Avatar shape="square" size={50} src={text} className="img-contain" />
          : <Avatar shape="square" size={50} src="/assets/default.jpg" className="img-contain" />
      ),
    },
    {
      title: 'NIK',
      dataIndex: 'nik',
      key: 'nik',
      render: text => text || '-',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: text => text || '-',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      render: text => text || '-',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone_number',
      key: 'phone_number',
      render: text => text || '-',
    },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
      render: text => text.name || '-',
    },
    {
      title: 'Created at',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => (text ? moment(text).format('DD-MMM-YYYY, HH:mm') : '-'),
    },
    {
      title: 'Last Login',
      dataIndex: 'last_login',
      key: 'last_login',
      render: text => (text ? moment(text).format('DD-MMM-YYYY, HH:mm') : '-'),
    },
    {
      title: 'Token Reset Exp',
      dataIndex: 'token_reset_expired',
      key: 'token_reset_expired',
      render: text => (text ? moment(text).format('DD-MMM-YYYY, HH:mm') : '-'),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: text => <Tag>{text || '-'}</Tag>,
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'action',
      className: '',
      width: 110,
      render: (text, record) => (
        <Button.Group>
          <Button onClick={() => history.push(`/user/${record.id}/edit`)} className="px-2">
            <i className="lar la-edit text-primary" style={{ fontSize: '20px' }} />
          </Button>
          <Popconfirm
            title="Are you sure delete this user?"
            okText="Yes"
            cancelText="No"
            placement="topLeft"
            onConfirm={() => handleDelete(record.id)}
          >
            <Button className="px-2">
              <i className="las la-trash text-danger" style={{ fontSize: '20px' }} />
            </Button>
          </Popconfirm>
        </Button.Group>
      ),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h3 className="mb-3">User List</h3>
            <Input.Search
              allowClear
              placeholder="Search..."
              value={keyword}
              onChange={e => setKeyword(e.target.value)}
              onSearch={handleSearch}
              className="w-md-50"
            />
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="rounded-pill float-md-right mt-3 mt-md-0"
              onClick={() => history.push('/user/add')}
            >
              Add User
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message=""
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}
      <Table
        bordered
        rowKey="id"
        columns={columns}
        dataSource={dataUser}
        loading={isFetching}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaUser ? metaUser.total_count : dataUser.length,
          current: currentPage || 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

UserPage.propTypes = {
  isFetching: PropTypes.bool,
  dataUser: PropTypes.array,
  metaUser: PropTypes.object,
  handlePage: PropTypes.func,
  handleDelete: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleSearch: PropTypes.func,
  keyword: PropTypes.string,
  currentPage: PropTypes.string,
  setKeyword: PropTypes.func,
}

export default UserPage
