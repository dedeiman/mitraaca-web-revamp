import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input, Button,
  Upload, Icon, Avatar, Divider,
  Select, Alert, Row, Col,
} from 'antd'
import { isEmpty } from 'lodash'
import { Loader } from '../../elements'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 5,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 12,
    },
  },
}

let id = 0

const FormUser = ({
  detailUser, stateBranches, setStateBranches,
  stateProducts, setStateProducts,
  handleSubmit, form, handleUpload,
  avatarFileList, handleSelect,
  selectOption, loadRole,
  selectGroupOption, loadGroupRole,
  selectCOBOption, loadCOB,
  selectBranchesOption, loadBranches,
  isEdit, isFetching, errorMessage,
  errorsField, setErrorsField,
  currentState,
}) => {
  const {
    getFieldDecorator, validateFields, setFieldsValue,
    setFields, getFieldsValue, getFieldValue,
  } = form

  const remove = (k) => {
    const {
      keys, class_of_business: cob, product_ids: productIds,
      branch_id: branchIds, branch_perwakilan_ids: perwakilanIds,
      role_group_id: group,
    } = getFieldsValue()
    if (keys.length === 1) return

    let newState
    if (group === 9) {
      newState = {
        branch_id: branchIds.filter((item, idx) => idx !== k),
        branch_perwakilan_ids: perwakilanIds.filter((item, idx) => idx !== k),
      }
    } else {
      newState = {
        class_of_business: cob.filter((item, idx) => idx !== k),
        product_ids: productIds.filter((item, idx) => idx !== k),
      }
    }
    setFieldsValue({
      ...newState,
      keys: keys.filter((key, idx) => idx !== k),
    })
    setTimeout(() => {
      if (group === 9) {
        setStateBranches(stateBranches.filter((item, idx) => idx !== k))
      } else {
        setStateProducts(stateProducts.filter((item, idx) => idx !== k))
      }
    }, 300)
  }

  const add = () => {
    const { keys, role_group_id: group } = getFieldsValue()
    const nextKeys = keys.concat(id += 1)
    let newState
    if (group === 9) {
      newState = stateBranches.concat({ load: false, list: [] })
      setStateBranches(newState)
    } else {
      newState = stateProducts.concat({ load: false, list: [] })
      setStateProducts(newState)
    }
    setTimeout(() => {
      setFieldsValue({
        keys: nextKeys,
      })
    }, 300)
  }

  getFieldDecorator('keys', { initialValue: currentState.keys })

  const keys = getFieldValue('keys')

  let fieldItems
  if (getFieldValue('role_group_id') === 8 || getFieldValue('role_group_id') === 9) {
    fieldItems = keys.map((k, index) => {
      const { role_group_id: group } = getFieldsValue()
      const elementTags = {}
      if (group === 9) {
        elementTags.parent = !isEmpty(currentState.branch) ? currentState.branch[index] : undefined
        elementTags.child = !isEmpty(currentState.perwakilan) ? currentState.perwakilan[index] : undefined
        elementTags.loading = !isEmpty(stateBranches[index]) ? stateBranches[index].load : false
        elementTags.list = !isEmpty(stateBranches[index]) ? stateBranches[index].list : []
      } else {
        elementTags.parent = !isEmpty(currentState.cob) ? currentState.cob[index] : undefined
        elementTags.child = !isEmpty(currentState.productIds) ? currentState.productIds[index] : undefined
        elementTags.loading = !isEmpty(stateProducts[index]) ? stateProducts[index].load : false
        elementTags.list = !isEmpty(stateProducts[index]) ? stateProducts[index].list : []
      }

      return (
        <React.Fragment key={`field_${k}`}>
          <b>{`${group === 9 ? 'Branch Perwakilan' : 'Product'} ${(keys.length > 1) ? (index + 1) : ''}`}</b>
          <Form.Item
            label={group === 9 ? 'Branch' : 'Class of Business'}
            key={`${group === 9 ? 'branch_id_' : 'class_of_business_'}${k}`}
            wrapperCol={{ sm: 24 }}
            labelCol={{ sm: 24 }}
            labelAlign="left"
          >
            {getFieldDecorator(`${group === 9 ? 'branch_id' : 'class_of_business'}[${index}]`, {
              initialValue: elementTags.parent || undefined,
              rules: [{ required: true, message: `${group === 9 ? 'Branch' : 'Class of Business'} Is Required` }],
            })(
              <Select
                onChange={e => handleSelect(e, `${group === 9 ? 'branch_id_' : 'class_of_business_'}${index}`)}
                loading={group === 9 ? loadBranches : loadCOB}
                disabled={group === 9 ? isEmpty(selectBranchesOption) : isEmpty(selectCOBOption)}
                placeholder={`Select ${group === 9 ? 'Branch' : 'Class of Business'}`}
              >
                {(group === 9 ? selectBranchesOption : selectCOBOption).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
          <Form.Item
            label={group === 9 ? 'Branch Perwakilan' : 'Product'}
            key={`${group === 9 ? 'branch_perwakilan_ids_' : 'product_ids_'}${k}`}
            labelCol={{ sm: 24 }}
            wrapperCol={{ sm: 24 }}
            labelAlign="left"
          >
            {getFieldDecorator(`${group === 9 ? 'branch_perwakilan_ids' : 'product_ids'}[${index}]`, {
              initialValue: elementTags.child || undefined,
              rules: [{ required: true, message: `${group === 9 ? 'Branch Perwakilan' : 'Product'} Is Required` }],
            })(
              <Select
                // onChange={e => handleSelect(e, 'role')}
                loading={elementTags.load}
                disabled={isEmpty(elementTags.list)}
                placeholder={`Select ${group === 9 ? 'Branch Perwakilan' : 'Product'}`}
              >
                {(elementTags.list).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item[group === 9 ? 'name' : 'display_name']}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
          {keys.length > 1 ? (
            <React.Fragment>
              <Icon
                type="delete"
                onClick={() => remove(index)}
              />
              <Divider />
            </React.Fragment>
          ) : null}
        </React.Fragment>
      )
    })
  }

  return (
    <Card title={`Form ${isEdit ? 'Edit' : 'Add'} User`}>
      {isFetching && <Loader />}
      {errorMessage && (
        <Alert
          message=""
          description={errorMessage}
          type="error"
          className="mb-3"
        />
      )}
      <Form
        {...formItemLayout}
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              if (!(values.phone_number.length < 16 && values.phone_number.length > 7)) {
                setFields({ phone_number: { value: values.phone_number, errors: [new Error('Phone Number must be between 8 and 15')] } })
              } else {
                handleSubmit(values)
              }
            }
          })
        }}
      >
        <Form.Item label="Profile Picture">
          {getFieldDecorator('profile_pic_file', {
            rules: [{ required: !isEdit, message: 'Profile Picture Is Required' }],
          })(
            <Upload
              name="profile_pic_file"
              listType="picture-card"
              className="avatar-uploader banner-content"
              showUploadList={false}
              beforeUpload={() => false}
              onChange={info => handleUpload(info)}
            >
              {avatarFileList
                ? (
                  <Avatar src={avatarFileList} shape="square" className="banner-preview" />
                )
                : (
                  <div>
                    <p className="ant-upload-drag-icon">
                      <Icon type="inbox" style={{ fontSize: '24px' }} />
                    </p>
                    <p className="ant-upload-text">Upload Profile Picture</p>
                  </div>
                )
              }
            </Upload>,
          )}
        </Form.Item>
        <Form.Item label="Role Group">
          {getFieldDecorator('role_group_id', {
            initialValue: detailUser.role ? detailUser.role.group.id : undefined,
            rules: [{ required: true, message: 'Role Group Is Required' }],
          })(
            <Select
              onChange={(e) => {
                setFields({ role_group_id: { value: e } })
                setFields({ role_id: { value: undefined } })
                setFields({ keys: { value: [0] } })
                handleSelect(e, 'role-group')
              }}
              loading={loadGroupRole}
              disabled={isEmpty(selectGroupOption)}
              placeholder="Select Role Group"
            >
              {selectGroupOption.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
            </Select>,
          )}
        </Form.Item>
        <Form.Item label="Role">
          {getFieldDecorator('role_id', {
            initialValue: detailUser.role ? detailUser.role.id : undefined,
            rules: [{ required: true, message: 'Role Is Required' }],
          })(
            <Select
              onChange={e => handleSelect(e, 'role')}
              loading={loadRole}
              disabled={isEmpty(selectOption)}
              placeholder="Select Role"
            >
              {selectOption.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
            </Select>,
          )}
        </Form.Item>
        {(getFieldValue('role_group_id') === 8 || getFieldValue('role_group_id') === 9) && (
          <Row style={{ marginBottom: '24px' }}>
            <Col xs={24} sm={5} />
            <Col xs={24} sm={12}>
              <Card className="child-item">
                {fieldItems}
                {(getFieldValue('role_id') === 32 || getFieldValue('role_id') === 33) && (
                  <Form.Item style={{ marginTop: '24px' }}>
                    <Button shape="round" onClick={add}>
                      <Icon type="plus" />
                      {` Add ${getFieldValue('role_id') === 33 ? 'Product' : 'Branch'}`}
                    </Button>
                  </Form.Item>
                )}
              </Card>
            </Col>
          </Row>
        )}
        <Form.Item label="NIK" {...errorsField.nik}>
          {getFieldDecorator('nik', {
            initialValue: detailUser.nik,
            rules: [
              { pattern: new RegExp('^[a-zA-Z0-9]{1,20}$'), message: 'Cannot input with special character and longer than 20 character' },
              { required: true, message: 'NIK Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                nik: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input NIK" />,
          )}
        </Form.Item>
        <Form.Item label="Name" {...errorsField.name}>
          {getFieldDecorator('name', {
            initialValue: detailUser.name,
            rules: [{ required: true, message: 'Name Is Required' }],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                name: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input Name" />,
          )}
        </Form.Item>
        <Form.Item label="Email" {...errorsField.email}>
          {getFieldDecorator('email', {
            initialValue: detailUser.email,
            rules: [
              { type: 'email', message: 'Email Is Not Valid' },
              { required: true, message: 'Email Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                email: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input Email" />,
          )}
        </Form.Item>
        <Form.Item label="Phone Number" {...errorsField.phone_number}>
          {getFieldDecorator('phone_number', {
            initialValue: detailUser.phone_number,
            rules: [
              { type: 'number', transform: value => (value ? Number(value) : ''), message: 'Phone Number Is Not A Number' },
              { required: true, message: 'Phone Number Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                phone_number: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input Phone Number" />,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">{!isEdit ? 'Add User' : 'Submit Change'}</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormUser.propTypes = {
  detailUser: PropTypes.object,
  handleSubmit: PropTypes.func,
  handleUpload: PropTypes.func,
  avatarFileList: PropTypes.any,
  handleSelect: PropTypes.func,
  selectOption: PropTypes.array,
  loadRole: PropTypes.bool,
  selectGroupOption: PropTypes.array,
  loadGroupRole: PropTypes.bool,
  selectCOBOption: PropTypes.array,
  loadCOB: PropTypes.bool,
  selectBranchesOption: PropTypes.array,
  loadBranches: PropTypes.bool,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  isFetching: PropTypes.bool,
  errorMessage: PropTypes.string,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  stateBranches: PropTypes.array,
  setStateBranches: PropTypes.func,
  stateProducts: PropTypes.array,
  setStateProducts: PropTypes.func,
  currentState: PropTypes.object,
}

export default Form.create({ name: 'userForm' })(FormUser)
