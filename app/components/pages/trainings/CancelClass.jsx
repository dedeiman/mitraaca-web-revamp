import PropTypes from 'prop-types'
import {
  Row, Col, Input, Button,
  Select, Radio, Divider,
  DatePicker, TimePicker,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import moment from 'moment'

const CancelClass = ({
  data, form, onSubmit,
  stateSelect, toggle,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form

  return (
    <Form onSubmit={onSubmit} colon={false}>
      <Row gutter={[24, 24]}>
        <Col xs={24}>
          <p className="mb-0">Training ID</p>
          <Input
            disabled
            size="large"
            value={data.training_id || ''}
          />
        </Col>
        <Col xs={24} md={8}>
          <p className="mb-0">Date</p>
          <Input
            disabled
            size="large"
            value={data.date || ''}
          />
        </Col>
        <Col xs={24} md={8}>
          <p className="mb-0">Start Time</p>
          <Input
            disabled
            size="large"
            value={data.start_time || ''}
          />
        </Col>
        <Col xs={24} md={8}>
          <p className="mb-0">End Time</p>
          <Input
            disabled
            size="large"
            value={data.end_time || ''}
          />
        </Col>
        <Col xs={24}>
          <p className="mb-0">Location</p>
          <Input
            disabled
            size="large"
            value={data.location || ''}
          />
        </Col>
        <Col xs={24}>
          <p className="mb-0">Description</p>
          <Input.TextArea
            disabled
            rows={3}
            size="large"
            value={data.description || ''}
          />
        </Col>
        <Col xs={24}>
          <p className="mb-0">Subject</p>
          <Input
            disabled
            size="large"
            value={data.subject ? data.subject.name : ''}
          />
        </Col>
        <Divider />
        <Col xs={24}>
          <p className="mb-0">Cancellation Type</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('cancellation_type_id', {
              rules: [...Helper.fieldRules(['required'])],
            })(
              <Select
                size="large"
                className="w-100"
                placeholder="Select Type"
                loading={stateSelect.cancelLoad}
              >
                {(stateSelect.cancelList || []).map(item => (
                  <Select.Option key={Math.random()} value={item.id}>{item.description}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Cancellation Description</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('cancellation_description', {
              rules: [
                ...Helper.fieldRules(['required']),
                { pattern: /^.{10,}$/, message: '*Minimal 10 karakter' },
              ],
              getValueFromEvent: e => e.target.value,
            })(
              <Input.TextArea
                rows={3}
                className="uppercase"
                size="large"
                placeholder="Insert Description"
              />,
            )}
          </Form.Item>
        </Col>
        <Divider />
        <Col xs={24} md={24} className="pb-0">
          <Form.Item className="mb-0" label="Reschedule?">
            {getFieldDecorator('is_class_reschedule', {
              initialValue: true,
              getValueFromEvent: (e) => {
                setFieldsValue({
                  date: undefined,
                  time: undefined,
                  location: undefined,
                  branch_organizer_id: undefined,
                })
                return e.target.value
              },
            })(
              <Radio.Group>
                <Radio value>Yes</Radio>
                <Radio value={false}>No</Radio>
              </Radio.Group>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} md={10}>
          <p className="mb-0">Date</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('date', {
              rules: [...Helper.fieldRules(getFieldValue('is_class_reschedule') ? ['required'] : [])],
            })(
              <DatePicker
                size="large"
                placeholder="Input Date"
                disabled={!getFieldValue('is_class_reschedule')}
                disabledDate={current => (current && (current < moment().endOf('day')))}
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} md={14}>
          <p className="mb-0">Time</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('time', {
              rules: [...Helper.fieldRules(getFieldValue('is_class_reschedule') ? ['required'] : [])],
            })(
              <TimePicker.RangePicker
                size="large"
                disabled={!getFieldValue('is_class_reschedule')}
                format="HH:mm"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Branch</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('branch_organizer_id', {
              rules: [...Helper.fieldRules(getFieldValue('is_class_reschedule') ? ['required'] : [])],
            })(
              <Select
                size="large"
                className="w-100"
                placeholder="Select Branch"
                loading={stateSelect.branchLoad}
                disabled={!getFieldValue('is_class_reschedule')}
              >
                {(stateSelect.branchList || []).map(item => (
                  <Select.Option key={Math.random()} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Location</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('location', {
              rules: [...Helper.fieldRules(getFieldValue('is_class_reschedule') ? ['required'] : [])],
              getValueFromEvent: e => e.target.value,
            })(
              <Input.TextArea
                rows={3}
                size="large"
                className="w-100 uppercase"
                placeholder="Input Location"
                disabled={!getFieldValue('is_class_reschedule')}
              />,
            )}
          </Form.Item>
        </Col>
        <Col hidden xs={24}>
          <p className="mb-0">Location</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('location_url', {
              rules: [...Helper.fieldRules(getFieldValue('is_class_reschedule') ? ['required'] : [])],
              getValueFromEvent: e => e.target.value,
            })(
              <Input.TextArea
                rows={3}
                size="large"
                className="w-100 uppercase"
                placeholder="Input Location"
                disabled={!getFieldValue('is_class_reschedule')}
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} className="pb-0">
          <div className="d-flex justify-content-end">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button
              ghost
              type="primary"
              className="ml-2"
              onClick={() => {
                toggle()
                form.resetField()
              }}
            >
              Cancel
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  )
}

CancelClass.propTypes = {
  form: PropTypes.any,
  data: PropTypes.object,
  onSubmit: PropTypes.func,
  stateSelect: PropTypes.object,
  toggle: PropTypes.func,
}

export default CancelClass
