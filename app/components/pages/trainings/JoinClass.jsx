import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Avatar,
  Descriptions,
  Typography,
} from 'antd'
import config from 'app/config'

const JoinClass = ({ data, handleJoin, toggle }) => (
  <Row gutter={[24, 24]}>
    <Col xs={24}>
      <Descriptions layout="vertical" colon={false} column={2}>
        <Descriptions.Item span={1} label="Class Subject" className="px-1">
          {data.subject || '-'}
        </Descriptions.Item>
        <Descriptions.Item span={1} label="Jam" className="px-1">
          <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
            {data.time || '-'}
            {data.end_time ? ' - ' : ''}
            {data.end_time || ''}
          </Typography.Paragraph>
        </Descriptions.Item>
        <Descriptions.Item span={1} label="Hari" className="px-1">
          <span className="text-capitalize">
            {data.date || '-'}
          </span>
        </Descriptions.Item>
        <Descriptions.Item span={1} label="Lokasi" className="px-1">
          {data.location || '-'}
        </Descriptions.Item>
      </Descriptions>
    </Col>
    <Col xs={24} className="text-center">
      <p className="text-muted mb-0">Scan to join</p>
      <Avatar size={200} shape="square" src={`${config.api_url}/training_classes/${data.id}/qr-code`} className="border" />
    </Col>
    <Col xs={24} className="text-right mt-4">
      <Button
        type="primary"
        className="px-4"
        onClick={() => handleJoin(data.id)}
      >
        Join
      </Button>
      <Button
        ghost
        type="primary"
        className="ml-3"
        onClick={() => toggle()}
      >
        Cancel
      </Button>
    </Col>
  </Row>
)

JoinClass.propTypes = {
  data: PropTypes.object,
  handleJoin: PropTypes.func,
  toggle: PropTypes.func,
}

export default JoinClass
