import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/trainings/List'
import Detail from 'containers/pages/trainings/Detail'
import Form from 'containers/pages/trainings/Form'
import DetailHistory from 'containers/pages/trainings/DetailHistory'
import Upload from 'containers/pages/trainings/Upload'
import history from 'utils/history'

const Customer = ({ currentUser, location, match, stateCheck }) => {
  if (location.pathname === '/training-class-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('training-class-list') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/open')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('training-class-open') > -1) {
      return <Detail match={match} location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/training-class-menu/add')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('training-class-register') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/history')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('training-class-detail') > -1) {
      return <DetailHistory match={match} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/upload')) {
    return <Upload match={match} />
  }

  return ''
}

Customer.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
}

export default Customer
