import PropTypes from 'prop-types'
import {
  Row, Col, Input, Button,
  AutoComplete,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

const AddParticipant = ({
  form, onSubmit,
  stateSelect, toggle,
  handleSearch, list,
}) => {
  const { getFieldDecorator, setFieldsValue } = form

  return (
    <Form colon={false}>
      <Row gutter={[24, 24]}>
        <Col xs={24}>
          <p className="mb-0">ID Agent</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('id', {
              rules: [...Helper.fieldRules(['required'])],
            })(
              <AutoComplete
                allowClear
                onSelect={val => setFieldsValue({
                  id: val,
                  name: ((stateSelect.agentList || []).find(item => item.agent_id === val).name).toUpperCase(),
                  branch: (stateSelect.agentList || []).find(item => item.agent_id === val).branch.name,
                })}
                onSearch={handleSearch}
                loading={stateSelect.agentLoad}
                options={(stateSelect.agentList || []).map(item => ({ value: item.agent_id, label: `${item.agent_id} - ${item.name}`, disabled: ((list || []).map(agent => agent.agent_id)).includes(item.agent_id) }))}
              >
                <Input size="large" placeholder="Search Agent" />
              </AutoComplete>,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Agent Name</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('name')(
              <Input
                disabled
                size="large"
                placeholder="Agent Name"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24}>
          <p className="mb-0">Branch</p>
          <Form.Item className="mb-0">
            {getFieldDecorator('branch')(
              <Input
                disabled
                size="large"
                placeholder="Branch"
              />,
            )}
          </Form.Item>
        </Col>
        <Col xs={24} className="pb-0">
          <div className="d-flex justify-content-end">
            <Button
              type="primary"
              htmlType="submit"
              onClick={(e) => {
                onSubmit(e)
                setFieldsValue({ id: '', name: '', branch: '' })
              }}
            >
              Add
            </Button>
            <Button
              ghost
              type="primary"
              className="ml-2"
              onClick={() => {
                toggle()
                form.resetFields()
                setFieldsValue({ id: '', name: '', branch: '' })
              }}
            >
              Cancel
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  )
}

AddParticipant.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  stateSelect: PropTypes.object,
  toggle: PropTypes.func,
  handleSearch: PropTypes.func,
  list: PropTypes.array,
}

export default AddParticipant
