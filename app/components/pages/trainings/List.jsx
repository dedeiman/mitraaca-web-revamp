/* eslint-disable consistent-return */
import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  Row, Col, Input,
  Card, Divider,
  Descriptions,
  Pagination, Modal,
  Button, Select,
  Empty, Typography,
  DatePicker, TimePicker,
} from 'antd'
import { Loader } from 'components/elements'
import {
  AGENT_CODE, AGENCY_CODE,
} from 'constants/ActionTypes'
import { isEmpty } from 'lodash'
import CancelClass from 'containers/pages/trainings/CancelClass'
import JoinClass from 'containers/pages/trainings/JoinClass'
import history from 'utils/history'
import Helper from 'utils/Helper'
import moment from 'moment'
import { Form } from '@ant-design/compatible'

const ContestList = ({
  groupRole, dataTraining, currentUser,
  handlePage, metaTraining,
  stateTraining, setStateTraining,
  stateModal, setStateModal,
  isFetching, updatePerPage,
  loadTraining, openClass,
  unjoinClass, stateSelects,
}) => {
  const isMobile = window.innerWidth < 768
  const isDesktopMD = window.innerWidth < 1536

  return (
    <React.Fragment>
      {(currentUser.permissions && currentUser.permissions.indexOf('training-class-list') > -1) && (
      <Card>
        <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
          <Col xs={24} md={12} lg={5}>
          <Select
              allowClear
              size="large"
              className="w-100"
              placeholder="Select Status"
              loading={stateSelects.statusLoad}
              value={stateTraining.status}
              onChange={val => setStateTraining({ ...stateTraining, status: val })}
            >
              {(stateSelects.statusList || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Col>
          <Col xs={24} md={12} lg={7}>
            <Select
              allowClear
              size="large"
              className="w-100"
              placeholder="Select Subject"
              loading={stateSelects.subjectLoad}
              value={stateTraining.subject}
              onChange={val => setStateTraining({ ...stateTraining, subject: val })}
            >
              {(stateSelects.subjectList || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Col>
          <Col xs={24} md={12} lg={5}>
            <Select
              allowClear
              optionFilterProp="label"
              size="large"
              className="w-100"
              mode="multiple"
              showArrow
              placeholder="Select Level"
              loading={stateSelects.levelLoad}
              value={stateTraining.level}
              onChange={val => setStateTraining({ ...stateTraining, level: val })}
              disabled={groupRole.code === AGENT_CODE}
            >
              {(stateSelects.levelList || []).map(item => (
                <Select.Option label={item.name} key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Col>
          <Col xs={24} md={12} lg={7}>
            <Select
              allowClear
              showSearch
              size="large"
              className="w-100"
              optionFilterProp="children"
              placeholder="Select Trainer"
              loading={stateSelects.trainerLoad}
              value={stateTraining.trainer}
              onChange={val => setStateTraining({ ...stateTraining, trainer: val })}
            >
              {(stateSelects.trainerList || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
              ))}
            </Select>
          </Col>
          <Col xs={24} md={12} lg={6}>
            <Form.Item className="mb-2">
              <Input
                allowClear
                size="large"
                className="w-100"
                placeholder="Search Location..."
                value={stateTraining.location}
                onChange={e => setStateTraining({ ...stateTraining, location: e.target.value })}
              />
            </Form.Item>
          </Col>
          <Col xs={24} md={12} lg={4}>
            <TimePicker
              size="large"
              inputReadOnly="true"
              className="w-100"
              format="HH:mm"
              value={stateTraining.time}
              onChange={time => setStateTraining({ ...stateTraining, time })}
            />
          </Col>
          <Col xs={24} md={12} lg={8}>
            <DatePicker.RangePicker
              size="large"
              className="w-100"
              value={isEmpty(stateTraining.date) ? [] : stateTraining.date}
              onChange={date => setStateTraining({ ...stateTraining, date })}
            />
          </Col>
        </Row>
        <Row className={`d-flex ${isMobile ? 'justify-content-start' : 'justify-content-end'}`}>
          <Col span={isMobile ? 2 : 3} align="end" justify="end">
            <Button type="primary" className="button-lg px-4" onClick={() => loadTraining(true)}>
              Search
            </Button>
          </Col>
        </Row>
      </Card>
      )}
      <Row gutter={[24, 24]} className={isMobile ? '' : 'mt-4'}>
        <Col xs={24} md={6}>
          <Select
            allowClear
            size="large"
            className="w-100"
            optionFilterProp="children"
            placeholder="Period"
            value={stateTraining.viewBy}
            onChange={(val) => {
              setStateTraining({ ...stateTraining, viewBy: val })
              setTimeout(() => {
                loadTraining(true)
              }, 300)
            }}
          >
            {[
              { id: '', name: 'All' },
              { id: 'month', name: 'Monthly' },
              { id: 'week', name: 'Weekly' },
            ].map(item => (
              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
            ))}
          </Select>
        </Col>
        {(currentUser.permissions && currentUser.permissions.indexOf('training-class-list') > -1) && (
        <Col xs={24} md={18} align="end" className="d-flex justify-content-end">
          <Row>
            {(currentUser.permissions && currentUser.permissions.indexOf('training-class-upload') > -1) && (
            <Col>
              <Button
                ghost
                type="link"
                className={isMobile ? 'pr-0 mb-2' : 'pr-4'}
                onClick={() => history.push('/training-class-menu/upload')}
              >
                Upload Training Class
              </Button>
            </Col>
            )}
            {(currentUser.permissions && currentUser.permissions.indexOf('training-class-register') > -1) && (
              <Col>
                <Button
                  ghost
                  type="primary"
                  className="px-4 border-lg d-flex align-items-center"
                  onClick={() => history.push('/training-class-menu/add')}
                >
                  <img src="/assets/plus.svg" alt="plus" className="mr-1" />
                  Register New Class
                </Button>
              </Col>
            )}
          </Row>
        </Col>
        )}
      </Row>

      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataTraining)) && (
          <Empty
            description="Tidak ada data."
          />
        )}
        {!isEmpty(dataTraining)
          ? (
            dataTraining.map((item, idx) => {
              const currentDateTime = moment.duration(moment(`${item.date ? moment(item.date).format('YYYY-MM-DD') : moment().format()}T${item.start_time}:00`).diff(moment())).as('minutes') < 30
              return (
                <Row gutter={24} key={`list-faq-${item.id}`}>
                  <Col xs={24} md={6} xl={4}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Date & Time" className="px-1">
                        {item.date ? moment(item.date).format('DD MMM YYYY') : '-'}
                        {' / '}
                        {item.start_time || '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Location" className="px-1">
                        <a target="_blank" rel="noopener noreferrer" href={item.location_url}>
                          <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                            {item.location || '-'}
                          </Typography.Paragraph>
                        </a>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Status" className="px-1">
                        <span className="text-capitalize">{item.status ? (item.status).replace('-', ' ') : '-'}</span>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Subject" className="px-1">
                        {item.subject ? Helper.getValue(item.subject.name) : '-'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Level" className="px-1">
                        {!isEmpty(item.agent_levels)
                          ? (item.agent_levels || []).map(level => <p key={Math.random()} className="mb-0">{level.name}</p>)
                          : '-'
                        }
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                  <Col xs={24} md={6} xl={3}>
                    <Descriptions layout="vertical" colon={false} column={1}>
                      <Descriptions.Item span={1} label="Trainer" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                          {item.trainer ? Helper.getValue(item.trainer.name) : '-'}
                        </Typography.Paragraph>
                        <Typography.Paragraph ellipsis={{ rows: 2 }} className="mb-0">
                          {item.assistant ? Helper.getValue(item.assistant.name) : '-'}
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>

                  <Col xs={24} md={8} xl={4}>
                    {(item.status === 'finished') || (item.status === 'cancelled')
                      ? (
                        <Descriptions layout="vertical" colon={false} column={2}>
                          <Descriptions.Item span={1} label="History" className="px-1">
                            <Link to={`training-class-menu/${item.id}/history`}>{item.total_participants ? `${item.total_participants} Agents Joined` : '0 Agents Joined'}</Link>
                          </Descriptions.Item>
                        </Descriptions>
                      )
                      : (
                        <Row gutter={12} className="h-100" align="middle">
                          {(groupRole.code === AGENCY_CODE) && (
                            <>
                              {(currentUser.permissions && currentUser.permissions.indexOf('training-class-change') > -1) && (
                                <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                                  <Button
                                    type="primary"
                                    className="d-flex align-items-center justify-content-between w-100"
                                    disabled={(item.status === 'on-scheduled')}
                                    onClick={() => history.push(`/training-class-menu/${item.id}/edit`)}
                                  >
                                    Change
                                    <i style={{ fontSize: '18px' }} className="ml-2 las la-sync" />
                                  </Button>
                                </Col>
                              )}
                            </>
                          )}
                          {((currentUser.permissions && currentUser.permissions.indexOf('training-class-open') > -1) || (groupRole.code === AGENT_CODE)) && (
                            <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                              <Button
                                type="primary"
                                className="d-flex align-items-center justify-content-between w-100"
                                onClick={() => {
                                  if (groupRole.code === AGENT_CODE) {
                                    if (item.is_joined) {
                                      unjoinClass(item)
                                    } else {
                                      setStateModal({
                                        ...stateModal,
                                        visibleJoin: true,
                                        dataJoin: {
                                          id: item.id || '',
                                          time: item.start_time || '',
                                          end_time: item.end_time || '',
                                          date: item.date ? moment(item.date).format('DD MMMM YYYY') : '',
                                          subject: item.subject ? Helper.getValue(item.subject.name) : '',
                                          location: item.location || '',
                                        },
                                      })
                                    }
                                  }
                                  if (groupRole.code !== AGENT_CODE) {
                                    if (item.status === 'on-scheduled') {
                                      history.push(`/training-class-menu/${item.id}/open`)
                                    } else {
                                      openClass(item.id)
                                    }
                                  }
                                }}
                                disabled={(() => {
                                  switch (groupRole.code) {
                                    case AGENT_CODE:
                                      return (
                                        (item.status === 'finished') || (item.status === 'cancelled') || ((item.status === 're-scheduled') && !currentDateTime)
                                      )
                                    case AGENCY_CODE:
                                      return (
                                        (item.status === 'finished') || (item.status === 'cancelled') || ((item.status === 'opened') && !currentDateTime) || ((item.status === 're-scheduled') && !currentDateTime)
                                      )
                                    default:
                                  }
                                })()}
                              >
                                {(groupRole.code === AGENT_CODE) ? `${item.is_joined ? 'Unj' : 'J'}oin` : 'Open'}
                                <i style={{ fontSize: '18px' }} className={`ml-2 las ${(groupRole.code === AGENT_CODE) ? 'la-sign-in-alt' : 'la-share-square'}`} />
                              </Button>
                            </Col>
                          )}
                          {(currentUser.permissions && currentUser.permissions.indexOf('training-class-cancel') > -1) && (
                            <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                              <Button
                                type="primary"
                                className="d-flex align-items-center justify-content-between w-100"
                                onClick={() => setStateModal({ ...stateModal, visibleCancel: true, dataCancel: item })}
                                disabled={!['opened', 're-scheduled'].includes(item.status)}
                              >
                                Cancel
                                <i style={{ fontSize: '18px' }} className="ml-2 las la-ban" />
                              </Button>
                            </Col>
                          )}
                        </Row>
                      )
                    }
                  </Col>
                  {(idx !== (dataTraining.length - 1)) && (
                    <Divider />
                  )}
                </Row>
              )
            })
          )
          : <div className="py-5" />
          }
      </Card>
      <Modal
        title={<p className="mb-0 title-card">Join Training Class</p>}
        visible={stateModal.visibleJoin}
        footer={null}
        closable={false}
        wrapClassName="bg-transparent"
        // onCancel={() => setStateModal({ ...stateModal, visibleJoin: false })}
      >
        <JoinClass
          data={stateModal.dataJoin}
          toggle={(isJoined) => {
            if (isJoined) loadTraining()
            setStateModal({ ...stateModal, visibleJoin: false })
          }}
        />
      </Modal>
      <Modal
        title={<p className="mb-0 title-card">Cancel Class</p>}
        visible={stateModal.visibleCancel}
        footer={null}
        closable={false}
        wrapClassName="bg-transparent"
        // onCancel={() => setStateModal({ ...stateModal, visibleCancel: false, dataCancel: {} })}
      >
        <CancelClass
          data={stateModal.dataCancel}
          reload={() => loadTraining()}
          toggle={() => setStateModal({ ...stateModal, visibleCancel: false, dataCancel: {} })}
        />
      </Modal>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        simple={isMobile}
        pageSize={metaTraining.per_page || 10}
        current={stateTraining.page ? Number(stateTraining.page) : 1}
        onChange={handlePage}
        total={metaTraining.total_count}
        onShowSizeChange={updatePerPage}
        showSizeChanger={false}
      />

    </React.Fragment>
  )
}

ContestList.propTypes = {
  currentUser: PropTypes.object,
  groupRole: PropTypes.object,
  dataTraining: PropTypes.array,
  metaTraining: PropTypes.object,
  stateTraining: PropTypes.object,
  stateModal: PropTypes.object,
  handlePage: PropTypes.func,
  loadTraining: PropTypes.func,
  setStateTraining: PropTypes.func,
  setStateModal: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  stateSelects: PropTypes.object,
  openClass: PropTypes.func,
  unjoinClass: PropTypes.func,
}

export default ContestList
