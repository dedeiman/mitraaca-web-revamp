import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Row, Col,
  Card, Input,
  DatePicker, Select,
  TimePicker, Radio,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const FormCustomer = ({
  detailTraining, isLoadSubmit,
  stateSelects, onSubmit,
  form, match, handleLocation,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={24} className={isMobile ? 'mb-4 mt-3' : 'mb-5'}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`Class Training ${match.params.id ? 'Edit' : 'Registration'}`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24} className="mb-5">
          <Col span={24} className="mb-4">
            <Card>
              <p className="title-card">Training ID</p>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <Form.Item>
                    {getFieldDecorator('training_id', {
                      initialValue: detailTraining.training_id || '',
                    })(
                      <Input
                        disabled
                        size="large"
                        className="field-lg"
                        placeholder="Auto Generate"
                      />,
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs={24} md={12} className={isMobile ? 'mb-4' : ''}>
            <Card className="h-100">
              <p className="title-card mb-4">Date, Time and Place</p>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <p className="mb-0">Location</p>
                  <Form.Item>
                    {getFieldDecorator('location', {
                      rules: Helper.fieldRules(['required'], 'Location'),
                      initialValue: detailTraining.location || '',
                      getValueFromEvent: e => e.target.value,
                    })(
                      <Input
                        size="large"
                        className="uppercase"
                        placeholder="Input Address"
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col hidden xs={24} md={12}>
                  <p className="mb-0">Url</p>
                  <Form.Item>
                    {getFieldDecorator('location_url', {
                      rules: Helper.fieldRules(['required']),
                      initialValue: detailTraining.location || '',
                      getValueFromEvent: e => e.target.value,
                    })(
                      <Input
                        size="large"
                        className="uppercase"
                        placeholder="Input Address"
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} md={12}>
                  <p className="mb-0">Date</p>
                  <Form.Item>
                    {getFieldDecorator('date', {
                      rules: Helper.fieldRules(['required'], 'Date'),
                      initialValue: detailTraining.date ? moment(detailTraining.date) : undefined,
                    })(
                      <DatePicker
                        size="large"
                        className="w-100"
                        disabledDate={current => (current && (current < moment().subtract(1, 'day')))}
                        format="DD MMMM YYYY"
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24}>
                  <p className="mb-0">Time</p>
                  <Form.Item>
                    {getFieldDecorator('time', {
                      rules: [
                        ...Helper.fieldRules(['required']),
                        {
                          message: '*Durasi minimal 1 menit',
                          validator: (rule, value, callback) => {
                            const startTime = moment(value[0]).format()
                            const endTime = moment(value[1]).format()
                            if (moment.duration(moment(endTime).diff(moment(startTime))).as('minutes') < 1) return callback(true)

                            return callback()
                          },
                        },
                      ],
                      initialValue: [detailTraining.start_time ? moment(detailTraining.start_time, 'HH:mm') : undefined, detailTraining.end_time ? moment(detailTraining.end_time, 'HH:mm') : undefined],
                    })(
                      <TimePicker.RangePicker
                        size="large"
                        inputReadOnly="true"
                        className="w-100"
                        format="HH:mm"
                        hideDisabledOptions
                        // disabledHours={() => {
                        //   const hours = []
                        //   const current = (getFieldValue('date') ? moment(getFieldValue('date')).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')) === moment().format('YYYY-MM-DD')
                        //   if (current) {
                        //     for (let i = 0; i < moment().hour(); i += 1) {
                        //       hours.push(i)
                        //     }
                        //   }
                        //   return hours
                        // }}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24}>
                  <p className="mb-0">Branch Organizer</p>
                  <Form.Item>
                    {getFieldDecorator('branch_organizer_id', {
                      rules: Helper.fieldRules(['required'], 'Branch Organizer'),
                      initialValue: detailTraining.branch_organizer ? detailTraining.branch_organizer.id : undefined,
                    })(
                      <Select
                        className="field-lg"
                        size="large"
                        placeholder="Select Branch Organizer"
                        loading={stateSelects.branchLoad}
                      >
                        {(stateSelects.branchList || []).map(item => (
                          <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                        ))}
                      </Select>,
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={0} className="h-100">
              <Col xs={24}>
                <Card className="h-100">
                  <p className="title-card">Class Details</p>
                  <Row gutter={24}>
                    <Col xs={24}>
                      <p className="mb-0">Subject</p>
                      <Row gutter={24}>
                        <Col xs={24} md={8}>
                          <Form.Item>
                            {getFieldDecorator('subject_type', {
                              rules: Helper.fieldRules(['required'], 'Subject Type'),
                              initialValue: detailTraining.subjectType ? detailTraining.subjectType.id : ((stateSelects.subjectTypeList || []).find(item => item.name === 'Agent') || {}).id,
                              getValueFromEvent: (val) => {
                                setFieldsValue({
                                  agent_branch_ids: undefined,
                                  agent_level_ids: undefined,
                                  subject_id: undefined,
                                })
                                handleLocation(`/training-subjects?subject_type_id=${val}`, 'subject')
                                return val
                              },
                            })(
                              <Select
                                allowClear
                                size="large"
                                className="field-lg"
                                placeholder="Select Type"
                                loading={stateSelects.subjectTypeLoad}
                              >
                                {(stateSelects.subjectTypeList || []).map(item => (
                                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                ))}
                              </Select>,
                            )}
                          </Form.Item>
                        </Col>
                        <Col xs={24} md={16}>
                          <Form.Item>
                            {getFieldDecorator('subject_id', {
                              rules: Helper.fieldRules(['required'], 'Subject'),
                              initialValue: detailTraining.subject ? detailTraining.subject.id : undefined,
                            })(
                              <Select
                                allowClear
                                size="large"
                                className="field-lg"
                                placeholder="Select Subject"
                                loading={stateSelects.subjectLoad}
                                disabled={!getFieldValue('subject_type')}
                              >
                                {(stateSelects.subjectList || []).map(item => (
                                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                ))}
                              </Select>,
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Agent Branch</p>
                      <Form.Item>
                        {getFieldDecorator('agent_branch_ids', {
                          rules: Helper.fieldRules(((getFieldValue('subject_type') === ((stateSelects.subjectTypeList || []).find(item => item.name === 'Agent') || {}).id) ? ['required'] : []), 'Agent Branch'),
                          initialValue: !isEmpty(detailTraining.agent_branches) ? (detailTraining.agent_branches).map(item => item.id) : undefined,
                        })(
                          <Select
                            showArrow
                            size="large"
                            mode="multiple"
                            placeholder="Select Branch"
                            optionFilterProp="children"
                            loading={stateSelects.branchLoad}
                            disabled={getFieldValue('subject_type') !== ((stateSelects.subjectTypeList || []).find(item => item.name === 'Agent') || {}).id}
                          >
                            {(stateSelects.branchList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Agent Level</p>
                      <Form.Item>
                        {getFieldDecorator('agent_level_ids', {
                          rules: Helper.fieldRules(((getFieldValue('subject_type') === ((stateSelects.subjectTypeList || []).find(item => item.name === 'Agent') || {}).id) ? ['required'] : []), 'Agent Level'),
                          initialValue: !isEmpty(detailTraining.agent_levels) ? (detailTraining.agent_levels).map(item => item.id) : undefined,
                        })(
                          <Select
                            showArrow
                            size="large"
                            mode="multiple"
                            placeholder="Select Level"
                            optionFilterProp="children"
                            loading={stateSelects.levelLoad}
                            disabled={getFieldValue('subject_type') !== ((stateSelects.subjectTypeList || []).find(item => item.name === 'Agent') || {}).id}
                          >
                            {(stateSelects.levelList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Trainer 1</p>
                      <Form.Item>
                        {getFieldDecorator('trainer_id', {
                          rules: Helper.fieldRules(['required'], 'Trainer'),
                          initialValue: detailTraining.trainer ? detailTraining.trainer.id : undefined,
                        })(
                          <Select
                            showSearch
                            allowClear
                            size="large"
                            optionFilterProp="children"
                            placeholder="Select Trainer 1"
                            loading={stateSelects.trainerLoad}
                          >
                            {(stateSelects.trainerList || []).map(item => (
                              <Select.Option key={item.id} value={item.id} disabled={item.id === getFieldValue('assistant_id')}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Trainer 2</p>
                      <Form.Item>
                        {getFieldDecorator('assistant_id', {
                          initialValue: detailTraining.assistant ? detailTraining.assistant.id : undefined,
                        })(
                          <Select
                            showSearch
                            allowClear
                            size="large"
                            optionFilterProp="children"
                            placeholder="Select Trainer 2"
                            loading={stateSelects.trainerLoad}
                          >
                            {(stateSelects.trainerList || []).map(item => (
                              <Select.Option key={item.id} value={item.id} disabled={item.id === getFieldValue('trainer_id')}>{item.name}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Kuota</p>
                      <Form.Item>
                        {getFieldDecorator('quota', {
                          rules: [
                            ...Helper.fieldRules(['required', 'number'], 'Kuota'),
                            { pattern: /^.{0,6}$/, message: '*Maksimal 6 digit' },
                            { pattern: /^[1-9][0-9]*$/, message: '*Minimal 1 slot' },
                          ],
                          initialValue: detailTraining.quota || '',
                          getValueFromEvent: e => (e.target.value).replace('+', ''),
                        })(
                          <Input
                            size="large"
                            placeholder="Input Kuota"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Ujian</p>
                      <Form.Item>
                        {getFieldDecorator('is_quiz_exists', {
                          rules: Helper.fieldRules(['required'], 'Kuota'),
                          initialValue: detailTraining.is_quiz_exists || false,
                        })(
                          <Radio.Group
                            size="large"
                          >
                            <Radio value>Ya</Radio>
                            <Radio value={false}>Tidak</Radio>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Description</p>
                      <Form.Item>
                        {getFieldDecorator('description', {
                          rules: [
                            ...Helper.fieldRules(['required'], 'Description'),
                            { min: 3, message: '*Minimal 3 karakter\n' },
                            { pattern: /^[a-zA-Z0-9,.\n ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                          ],
                          initialValue: detailTraining.description || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input.TextArea
                            rows={4}
                            className="uppercase"
                            size="large"
                            placeholder="Input Description"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="my-3">
          <Col span={24} className="d-flex justify-content-end align-items-center">
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadSubmit}
              disabled={isLoadSubmit}
              className={isMobile ? 'button-lg mr-3 mb-3' : 'button-lg w-15 mr-3'}
            >
              {match.params.id ? 'Save' : 'Submit'}
            </Button>
            <Button
              ghost
              type="primary"
              className={isMobile ? 'button-lg border-lg mr-3 mb-3' : 'button-lg border-lg w-15 mr-3'}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormCustomer.propTypes = {
  form: PropTypes.any,
  match: PropTypes.object,
  onSubmit: PropTypes.func,
  isLoadSubmit: PropTypes.bool,
  stateSelects: PropTypes.object,
  detailTraining: PropTypes.object,
  handleLocation: PropTypes.func,
}

export default FormCustomer
