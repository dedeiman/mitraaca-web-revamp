import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Button, Card,
  Upload,
} from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import config from 'app/config'

const UploadTraining = ({
  isFetching,
  stateTraining,
  setStateTraining,
  handleUpload,
}) => (
  <React.Fragment>
    <Row gutter={[24, 24]}>
      <Col span={24} className="d-flex justify-content-between align-items-center">
        <div className="title-page">Upload Training</div>
      </Col>
    </Row>
    <Row gutter={[24, 24]}>
      <Col xs={24} md={24}>
        <Card className="h-100" loading={isFetching}>
          <Row gutter={24}>
            <Col xs={24} md={24} className="mb-4">
              <p className="title-card">File</p>
              <Upload
                accept=".xlsx, .xls"
                name="icon"
                listType="picture-card"
                className="avatar-uploader banner-content"
                beforeUpload={() => false}
                fileList={stateTraining.fileList}
                onChange={(info) => { setStateTraining({ ...stateTraining, fileList: info.fileList, file: info.file }) }}
                showUploadList={{ showRemoveIcon: true, showPreviewIcon: false }}
              >
                {stateTraining.fileList.length < 1 && (
                  <div>
                    <p className="ant-upload-text mb-1">Taruh file di area ini</p>
                    <p className="ant-upload-text mb-1">atau</p>
                    <Button
                      ghost
                      size="small"
                      type="primary"
                    >
                      Upload Document
                    </Button>
                  </div>
                )}
              </Upload>
            </Col>
            <Col xs={24} className="mb-4">
              Need a Template Document?
              <a
                className="link-download ml-1"
                href={`${config.api_url}/training-classes/download-template`}
                download
              >
                Download Here
              </a>
            </Col>
          </Row>
        </Card>
      </Col>

      <Col span={24} className="d-flex justify-content-end align-items-center">
        <Button
          type="primary"
          className="mr-2"
          onClick={handleUpload}
        >
          {stateTraining.isFetching && <LoadingOutlined className="mr-2" />}
          Upload
        </Button>
      </Col>
    </Row>
  </React.Fragment>
)

UploadTraining.propTypes = {
  isFetching: PropTypes.bool,
  stateTraining: PropTypes.object,
  handleUpload: PropTypes.func,
}

export default UploadTraining
