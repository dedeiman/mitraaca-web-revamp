import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Button, Empty,
  Descriptions,
  Row, Col, Modal, Avatar,
  Checkbox,
} from 'antd'
import { PDFDownloadLink } from '@react-pdf/renderer'
import AddParticipant from 'containers/pages/trainings/AddParticipant'
import config from 'app/config'
import moment from 'moment'
import Helper from 'utils/Helper'
import history from 'utils/history'
import PdfViewer from './PdfViewer'

const DetailTraining = ({
  stateParticipants,
  detailTraining, updateParticipant,
  dataUpdate, handleChange,
  stateModal, setStateModal,
  getParticipants, onCheckAllChange,
  checkAll,
}) => (
  <React.Fragment>
    <Row gutter={[24, 24]} className="mb-5">
      <Col xs={24} md={12}>
        <Card className="h-100">
          <p className="title-card mb-4">Open Class Training</p>
          <Descriptions layout="vertical" colon={false} column={2}>
            <Descriptions.Item label="Tanggal" className="px-1">
              <p>{detailTraining.date ? moment(detailTraining.date).format('DD MMMM YYYY') : '-'}</p>
            </Descriptions.Item>
            <Descriptions.Item label="Subject" className="px-1">
              <p>{detailTraining.subject ? Helper.getValue(detailTraining.subject.name) : '-'}</p>
            </Descriptions.Item>
            <Descriptions.Item label="Waktu" className="px-1">
              <p>
                {detailTraining.start_time || '-'}
                {detailTraining.end_time ? ' - ' : ''}
                {detailTraining.end_time || ''}
              </p>
            </Descriptions.Item>
            <Descriptions.Item label="Tingkatan" className="px-1">
              <p>
                {(detailTraining.agent_levels || []).length
                  ? (detailTraining.agent_levels || []).map(item => (
                    <p className="mb-0">{item.name}</p>
                  ))
                  : '-'
                  }
              </p>
            </Descriptions.Item>
            <Descriptions.Item label="Lokasi" className="px-1">
              <a target="_blank" rel="noopener noreferrer" href={detailTraining.location_url} >{detailTraining.location || '-'}</a>
            </Descriptions.Item>
            <Descriptions.Item label="Pelatih" className="px-1">
              <p>{detailTraining.trainer ? Helper.getValue(detailTraining.trainer.name) : '-'}</p>
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </Col>
      <Col xs={24} md={12}>
        <Card className="h-100" bodyStyle={{ height: 'calc(100% - 35px)' }}>
          <p className="title-card">Show QR Code</p>
          <Row gutter={[24, 24]} justify="center" align="middle" className="h-100">
            <Col>
              <Button
                type="primary"
                size="large"
                onClick={() => setStateModal({ ...stateModal, visible: true })}
              >
                Show QR Code
              </Button>
            </Col>
            <Col>
              <Button
                type="primary"
                size="large"
              >
                <PDFDownloadLink document={<PdfViewer data={detailTraining} />} fileName="QRCode.pdf">
                  {({ loading }) => (loading ? 'Loading...' : 'Print QR Code')}
                </PDFDownloadLink>
              </Button>
            </Col>
          </Row>
        </Card>
      </Col>
      <Col span={24}>
        <Button
          ghost
          type="primary"
          className="d-flex align-items-center float-right"
          onClick={() => setStateModal({ ...stateModal, visibleAdd: true })}
        >
          <img src="/assets/plus.svg" alt="plus" className="mr-1" />
          Add Agent
        </Button>
      </Col>
      <Col span={24}>
        <Card loading={dataUpdate.load}>
          {(stateParticipants || []).length
            ? (
              <>
                <Checkbox onChange={onCheckAllChange} checked={checkAll} className="mb-4">
                  Check all
                </Checkbox>
                <Checkbox.Group className="w-100" value={dataUpdate.list} onChange={handleChange}>
                  {(stateParticipants || []).map((item, idx) => (
                    <Row>
                      <Col xs={3} md={1}>
                        <Checkbox value={item.id} />
                      </Col>
                      <Col xs={1}>
                        {idx + 1}
                      </Col>
                      <Col xs={20} md={16}>
                        <Descriptions layout="vertical" colon={false} column={4}>
                          <Descriptions.Item span={1} label="Cabang" className="px-1 profile-detail pb-0">
                            {item.branch ? Helper.getValue(item.branch.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={1} label="Agent Code" className="px-1 profile-detail pb-0">
                            {item.agent_id || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={1} label="Agent Name" className="px-1 profile-detail pb-0">
                            {item.name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={1} label="Action" className="px-1 profile-detail pb-0">
                            {(dataUpdate.list.includes(item.id)) ? 'Hadir' : 'Tidak Hadir'}
                          </Descriptions.Item>
                        </Descriptions>
                      </Col>
                    </Row>
                  ))}
                </Checkbox.Group>
              </>
            )
            : <Empty description="Tidak ada data." className="py-5" />
            }
        </Card>
      </Col>
      <Col span={24}>
        <Row justify="end">
          <Button
            type="primary"
            className="mr-3"
            onClick={updateParticipant}
            disabled={dataUpdate.length <= 0}
          >
            Save
          </Button>
          <Button
            ghost
            type="primary"
            onClick={() => history.goBack()}
          >
            Cancel
          </Button>
        </Row>
      </Col>
    </Row>
    <Modal
      visible={stateModal.visible}
      footer={null}
      closable={false}
      wrapClassName="bg-transparent"
      onCancel={() => setStateModal({ ...stateModal, visible: false })}
    >
      <div className="text-center w-100">
        <p className="title-card mb-0">Show QR Code Training Class</p>
        <p className="title-card">
          Training ID:
          {detailTraining.training_id}
        </p>
        <p>Silakan Scan QR untuk join Training Class</p>
        <Avatar size={250} shape="square" src={`${config.api_url}/training_classes/${detailTraining.id}/qr-code`} />
        <div>
          <Button
            ghost
            type="primary"
            onClick={() => setStateModal({ ...stateModal, visible: false })}
          >
            Tutup
          </Button>
        </div>
      </div>
    </Modal>
    <Modal
      title={<p className="mb-0 title-card">Add Agent</p>}
      visible={stateModal.visibleAdd}
      footer={null}
      closable={false}
      wrapClassName="bg-transparent"
      onCancel={() => setStateModal({ ...stateModal, visibleAdd: false })}
    >
      <AddParticipant
        idTraining={detailTraining.id}
        list={stateParticipants}
        toggle={(isReload) => {
          if (isReload) {
            getParticipants()
          }
          setStateModal({ ...stateModal, visibleAdd: false })
        }}
      />
    </Modal>
  </React.Fragment>
)

DetailTraining.propTypes = {
  detailTraining: PropTypes.object,
  stateModal: PropTypes.object,
  setStateModal: PropTypes.func,
  stateParticipants: PropTypes.array,
  dataUpdate: PropTypes.array,
  handleChange: PropTypes.func,
  updateParticipant: PropTypes.func,
  getParticipants: PropTypes.func,
  onCheckAllChange: PropTypes.func,
  checkAll: PropTypes.bool,
}

export default DetailTraining
