import React from 'react'
import PropTyopes from 'prop-types'
import config from 'app/config'
import {
  Document, Page,
  Text, View,
  Image, StyleSheet,
} from '@react-pdf/renderer'

const style = StyleSheet.create({
  wrapper: {
    widht: '100vw',
    hight: '100vh',
    textAlign: 'center',
    paddingTop: '35px',
  },
})
const PDFDocument = ({ data }) => (
  <Document>
    <Page size="A5">
      <View style={style.wrapper}>
        <Text style={{ color: '#2b57b7' }}>Show QR Code Training Class</Text>
        <Text style={{ color: '#2b57b7' }}>{data.training_id || '-'}</Text>
        <Text style={{ fontSize: '14px', marginTop: '15px' }}>Silakan Scan QR untuk join Training Class</Text>
        <Image src={`${config.api_url}/training_classes/${data.id}/qr-code`} />
      </View>
    </Page>
  </Document>
)

PDFDocument.propTypes = {
  data: PropTyopes.object,
}

export default PDFDocument
