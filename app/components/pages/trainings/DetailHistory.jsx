import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Button, Empty,
  TimePicker, Radio, Form,
  Descriptions, Input, Tag,
  Row, Col, Select, DatePicker,
} from 'antd'
import { LeftOutlined } from '@ant-design/icons'
import moment from 'moment'
import Helper from 'utils/Helper'
import history from 'utils/history'

const DetailHistoryTraining = ({
  stateParticipants,
  detailTraining,
  dataUpdate,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24} className="mb-5">
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/training-class-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="mb-4">
          <Card>
            <p className="title-card">Training ID</p>
            <Row gutter={24}>
              <Col xs={24} md={12}>
                <Form.Item>
                  <Input disabled size="large" className="field-lg" placeholder="Auto Generate" value={detailTraining.training_id || ''} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>
        <Col xs={24} md={12} className={isMobile ? 'mb-4' : ''}>
          <Card className="h-100">
            <p className="title-card mb-4">Date, Time and Place</p>
            <Row gutter={24}>
              <Col xs={24} md={12}>
                <p className="mb-0">Location</p>
                <Form.Item>
                  <Input disabled size="large" placeholder="Input Address" value={detailTraining.location || ''} />
                </Form.Item>
              </Col>
              <Col xs={24} md={12}>
                <p className="mb-0">Date</p>
                <Form.Item>
                  <DatePicker disabled size="large" className="w-100" format="DD MMMM YYYY" value={detailTraining.date ? moment(detailTraining.date) : ''} />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <p className="mb-0">Time</p>
                <Form.Item>
                  <TimePicker.RangePicker disabled size="large" className="w-100" format="HH:mm" value={[moment(`${detailTraining.date}T${detailTraining.start_time}`), moment(`${detailTraining.date}T${detailTraining.end_time}`)]} />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <p className="mb-0">Branch Organizer</p>
                <Form.Item>
                  <Select disabled className="field-lg" placeholder="Select Branch Organizer" loading={false} value={detailTraining.branch_organizer ? detailTraining.branch_organizer.name : undefined} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={0} className="h-100">
            <Col xs={24}>
              <Card className="h-100">
                <p className="title-card">Class Details</p>
                <Row gutter={24}>
                  <Col xs={24}>
                    <p className="mb-0">Subject</p>
                    <Row gutter={24}>
                      <Col xs={24} md={6}>
                        <Form.Item>
                          <Select disabled className="field-lg" placeholder="Select Type" loading={false} value={detailTraining.subject ? detailTraining.subject.type.name : undefined} />
                        </Form.Item>
                      </Col>
                      <Col xs={24} md={18}>
                        <Form.Item>
                          <Select disabled className="field-lg" placeholder="Select Subject" loading={false} value={detailTraining.subject ? detailTraining.subject.name : undefined} />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24}>
                    <p className="mb-0">Agent Branch</p>
                    <Form.Item>
                      <Select disabled showArrow size="large" mode="multiple" placeholder="Select Branch" loading={false} value={(detailTraining.agent_branches || []).map(item => item.name)} />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <p className="mb-0">Agent Level</p>
                    <Form.Item>
                      <Select disabled showArrow size="large" mode="multiple" placeholder="Select Level" loading={false} value={(detailTraining.agent_levels || []).map(item => item.name)} />
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-0">Trainer 1</p>
                    <Form.Item>
                      <Select disabled size="large" placeholder="Select Trainer 1" loading={false} value={detailTraining.trainer ? detailTraining.trainer.name : undefined} />
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-0">Trainer 2</p>
                    <Form.Item>
                      <Select disabled size="large" placeholder="Select Trainer 2" loading={false} value={detailTraining.assistant ? detailTraining.assistant.name : undefined} />
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-0">Kuota</p>
                    <Form.Item>
                      <Input disabled size="large" placeholder="Input Kuota" value={detailTraining.quota || ''} />
                    </Form.Item>
                  </Col>
                  <Col xs={24} md={12}>
                    <p className="mb-0">Ujian</p>
                    <Form.Item>
                      <Radio.Group disabled size="large" value={detailTraining.is_quiz_exists}>
                        <Radio value>Ya</Radio>
                        <Radio value={false}>Tidak</Radio>
                      </Radio.Group>
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <p className="mb-0">Description</p>
                    <Form.Item>
                      <Input.TextArea disabled rows={4} size="large" placeholder="Input Description" value={detailTraining.description || ''} />
                    </Form.Item>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={[24, 24]} className="mb-5">
        <Col span={24}>
          <Card style={isMobile ? { overflow: 'scroll' } : {}}>
            {(stateParticipants || []).length
              ? ((stateParticipants || []).map((item, idx) => (
                <Row style={isMobile ? { width: '700px' } : {}}>
                  <Col xs={1}>
                    {idx + 1}
                  </Col>
                  <Col xs={23} md={16}>
                    <Descriptions layout="vertical" colon={false} column={4}>
                      <Descriptions.Item span={1} label="Cabang" className="px-1 profile-detail pb-0">
                        {item.branch ? Helper.getValue(item.branch.name) : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={1} label="Agent Code" className="px-1 profile-detail pb-0">
                        {item.agent_id || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={1} label="Agent Name" className="px-1 profile-detail pb-0">
                        {item.name || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={1} label="Action" className="px-1 profile-detail pb-0">
                        {(item.is_present || dataUpdate.includes(item.id)) ? 'Hadir' : 'Tidak Hadir'}
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>
                </Row>
              ))) : <Empty description="Tidak ada data." className="py-5" />
            }
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  )
}

DetailHistoryTraining.propTypes = {
  detailTraining: PropTypes.object,
  stateParticipants: PropTypes.array,
  dataUpdate: PropTypes.array,
}

export default DetailHistoryTraining
