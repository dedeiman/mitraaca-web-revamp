/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Upload, AutoComplete,
  Button, Input, Select,
  DatePicker, Divider,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { InboxOutlined } from '@ant-design/icons'
import { capitalize, isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import NumberFormat from 'react-number-format'
import moment from 'moment'

const { Dragger } = Upload

const CardForm = ({
  toggle, form, onSubmit,
  stateForm, stateCard, handleSearch,
  data, handleUpload, stateFile,
  handleSearchProduct, stateSelect,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  const { title, isEdit, data: currentData } = data

  return (
    <Form onSubmit={onSubmit}>
      {(title.toLowerCase() === 'product') && (
        <Row gutter={24}>
          <Col span={24}>
            <p className="mb-0">Product</p>
            <Form.Item>
              {getFieldDecorator('product_id', {
                rules: Helper.fieldRules(['required'], 'Product'),
                initialValue: currentData.product ? currentData.product.display_name : undefined,
              })(
                <Select
                  showSearch
                  size="large"
                  placeholder="Select Product"
                  disabled={isEdit}
                  onSearch={handleSearchProduct}
                  filterOption={(input, option) => (option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)}
                >
                  {(stateForm.productList || []).map(item => (
                    <Select.Option
                      key={item.id}
                      value={item.id}
                    >
                      {`${(item.display_name).toUpperCase()} - ${(item.type.name).toUpperCase()}`}
                    </Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <p className="mb-0">Tanggal Efektif</p>
            <Form.Item>
              {getFieldDecorator('effective_date', {
                rules: Helper.fieldRules(['required'], 'Tanggal Efektif'),
                initialValue: currentData.effective_date ? moment(currentData.effective_date) : undefined,
              })(
                <DatePicker
                  size="large"
                  className="w-100"
                  disabled={isEdit}
                  format="DD MMMM YYYY"
                  disabledDate={current => (current && (current < moment().subtract(1, 'day')))}
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <p className="mb-0">Tanggal Expiry</p>
            <Form.Item>
              {getFieldDecorator('expiry_date', {
                initialValue: currentData.expiry_date ? moment(currentData.expiry_date) : undefined,
                rules: Helper.fieldRules(isEdit ? ['required'] : [], 'Tanggal Expiry'),
              })(
                <DatePicker
                  size="large"
                  className="w-100"
                  disabled={!isEdit}
                  format="DD MMMM YYYY"
                  disabledDate={current => (current && (current < moment().subtract(0, 'day')))}
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">Reason</p>
            <Form.Item>
              {getFieldDecorator('expiration_reason', {
                rules: Helper.fieldRules(isEdit ? ['required'] : [], 'Reason'),
                initialValue: currentData.expiration_reason || '',
              })(
                <Input.TextArea rows={4} size="large" disabled={!isEdit} style={{ 'text-transform': 'uppercase' }} />,
              )}
            </Form.Item>
          </Col>
        </Row>
      )}
      {((title.toLowerCase() === 'contract') || (title.toLowerCase() === 'license')) && (
        <Row gutter={24}>
          <Col span={24}>
            <p className="mb-0">{`${capitalize(title)} Type`}</p>
            <Form.Item>
              {getFieldDecorator(`${title.toLowerCase()}_type_id`, {
                rules: Helper.fieldRules(['required'], 'Contract Type'),
                initialValue: currentData[`${title.toLowerCase()}_type_id`] || undefined,
              })(
                <Select
                  size="large"
                  className="w-100"
                  disabled={isEdit && (title.toLowerCase() === 'contract')}
                  placeholder={`Select ${capitalize(title)} Type`}
                  loading={stateForm[`${title.toLowerCase()}Load`]}
                >
                  {(stateForm[`${title.toLowerCase()}List`] || []).map(item => (
                    <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">{`${capitalize(title)} No`}</p>
            <Form.Item>
              {getFieldDecorator(`${title.toLowerCase()}_number`, {
                rules: [
                  ...Helper.fieldRules(['required'], 'Contract No'),
                  { pattern: /^[a-zA-Z0-9./-]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                ],
                initialValue: currentData[`${title.toLowerCase()}_number`] || undefined,
                getValueFromEvent: e => e.target.value,
              })(
                <Input
                  size="large"
                  className="uppercase"
                  maxLength={30}
                  placeholder={`${capitalize(title)} No`}
                  disabled={isEdit && (title.toLowerCase() === 'contract')}
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <p className="mb-0">Tanggal Efektif</p>
            <Form.Item>
              {getFieldDecorator('start_date', {
                initialValue: currentData.start_date ? moment(currentData.start_date) : undefined,
                rules: [
                  ...Helper.fieldRules(['required'], 'Tanggal Efektif'),
                ],
                getValueFromEvent: (e) => {
                  if (getFieldValue('end_date') > moment(e).subtract(-3, 'year')) {
                    setFieldsValue({
                      end_date: moment(e, 'DD MMMM YYYY').add(3, 'year').add(1, 'day'),
                    })
                  }

                  return e
                },
              })(
                <DatePicker
                  size="large"
                  className="w-100"
                  format="DD MMMM YYYY"
                  disabled={isEdit && (title.toLowerCase() === 'contract')}
                  disabledDate={(current) => {
                    if (title.toLowerCase() === 'license') {
                      if (!isEmpty([stateCard.licenses])) {
                        return current && (current < moment().subtract(2, 'year').subtract(1, 'day'))
                      }
                      return current && (current < moment().subtract(1, 'day'))
                    }
                    return current && (current < moment().subtract(1, 'day'))
                  }}
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <p className="mb-0">Tanggal Expiry</p>
            <Form.Item>
              {getFieldDecorator('end_date', {
                initialValue: currentData.end_date ? moment(currentData.end_date) : undefined,
                rules: [
                  ...Helper.fieldRules(['required'], 'Tanggal Expiry'),
                  {
                    validator: (rule, value) => {
                      const departureDate = getFieldValue('start_date')
                      if (value && departureDate) {
                        const diffDate = departureDate.diff(moment(value, 'DD/MM/YYYY HH:mm:ss'))
                        if (diffDate > 0) {
                          return Promise.reject(new Error('*Tanggal Efektif tidak boleh lebih besar dari tanggal Expiry.'))
                        }
                      }

                      return Promise.resolve()
                    },
                  },
                ],
              })(
                <DatePicker
                  size="large"
                  className="w-100"
                  format="DD MMMM YYYY"
                  disabled={isEdit && (title.toLowerCase() === 'contract')}
                  disabledDate={(current) => {
                    if (title.toLowerCase() === 'license') {
                      if (!isEmpty([stateCard.licenses])) {
                        return (getFieldValue('start_date') && getFieldValue('start_date') < moment(current).subtract(3, 'year').subtract(1, 'day')) || (current && (current < moment().subtract(2, 'year').subtract(1, 'day')))
                      }
                      return current && (current < moment().subtract(0, 'day'))
                    }
                    return current && (current < moment().subtract(0, 'day'))
                  }}
                />,
              )}
            </Form.Item>
          </Col>
          { isEdit && (title.toLowerCase() === 'contract') && (
            <>
              <Col span={24}>
                <p className="mb-0">Tanggal Pemutusan</p>
                <Form.Item>
                  {getFieldDecorator('termination_date', {
                    initialValue: currentData.termination_date ? moment(currentData.termination_date) : undefined,
                    rules: Helper.fieldRules(['required'], 'Tanggal Pemutusan'),
                  })(
                    <DatePicker
                      size="large"
                      className="w-100"
                      format="DD MMMM YYYY"
                      disabledDate={current => (current && ((current < moment(currentData.start_date).subtract(0, 'day')) || (current > moment(currentData.end_date).subtract(0, 'day'))))}
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <p className="mb-0">Reason</p>
                <Form.Item>
                  {getFieldDecorator('termination_reasons', {
                    rules: isEdit ? [
                      ...Helper.fieldRules(['required'], 'Reason'),
                      { pattern: /^.{10,}$/, message: '*Minimal 10 karakter' },
                    ] : [],
                    initialValue: currentData.termination_reasons || '',
                  })(
                    <Input.TextArea rows={4} size="large" disabled={!isEdit} style={{ 'text-transform': 'uppercase' }} />,
                  )}
                </Form.Item>
              </Col>
            </>
          )}
        </Row>
      )}
      {(title.toLowerCase() === 'bank') && (
        <Row gutter={24}>
          <Col span={24}>
            <p className="mb-2">Agent</p>
            <Form.Item className="mb-0">
              {getFieldDecorator('agent_id', {
                rules: [...Helper.fieldRules(['required'])],
              })(
                <AutoComplete
                  allowClear
                  onSelect={val => setFieldsValue({
                    id: val,
                    name: ((stateSelect.agentList || []).find(item => item.agent_id === val).name).toUpperCase(),
                    branch: (stateSelect.agentList || []).find(item => item.agent_id === val).branch.name,
                  })}
                  onSearch={handleSearch}
                  loading={stateSelect.agentLoad}
                  options={(stateSelect.agentList || []).map(item => ({ value: item.agent_id, label: `${(item.agent_id || '').toUpperCase()} - ${(item.name || '').toUpperCase()}` }))}
                >
                  <Input size="large" placeholder="Search Agent" />
                </AutoComplete>,
              )}
            </Form.Item>
          </Col>
        </Row>
      )}
      {(title.toLowerCase() === 'taxation') && (
        <Row gutter={24}>
          <Col span={24}>
            <p className="mb-0">Tax Type</p>
            <Form.Item>
              {getFieldDecorator('type_id', {
                initialValue: currentData.type_id || undefined,
                rules: [
                  ...Helper.fieldRules(['required']),
                ],
              })(
                <Select
                  size="large"
                  className="w-100"
                  placeholder="Select Tax Type"
                  loading={stateForm.taxationLoad}
                >
                  {(stateForm.taxationList || []).map(item => (
                    <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">PTKP Status</p>
            <Form.Item>
              {getFieldDecorator('ptkp_status_id', {
                initialValue: currentData.ptkp_status_id || undefined,
              })(
                <Select
                  size="large"
                  className="w-100"
                  placeholder="Select Status PTKP"
                  loading={stateForm.ptkpLoad}
                >
                  {(stateForm.ptkpList || []).map(item => (
                    <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">Tax ID</p>
            <Form.Item>
              {getFieldDecorator('id', {
                initialValue: currentData.npwp_number || '',
                rules: [
                  ...Helper.fieldRules(['required']),
                  {
                    validator: (rule, value) => {
                      let npwp = value
                      npwp = npwp.split('-').join('')
                      npwp = npwp.split('.').join('')
                      npwp = npwp.split(' ').join('')

                      if (!npwp) return Promise.resolve()
                      if (/^.{15,30}$/.test(npwp) === false) return Promise.reject('*Minimal 15 Character')

                      return Promise.resolve()
                    },
                  },
                ],
              })(
                <NumberFormat
                  className="field-lg ant-input"
                  placeholder="Input Tax ID"
                  format="##.###.###.#-###.########"
                />,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">Name</p>
            <Form.Item>
              {getFieldDecorator('name', {
                rules: [
                  ...Helper.fieldRules(['required']),
                  { pattern: /^[a-zA-Z ]+$/, message: '*Wajib menggunakan huruf' },
                ],
                initialValue: currentData.name || '',
              })(
                <Input size="large" style={{ textTransform: 'uppercase' }} />,
              )}
            </Form.Item>
          </Col>
        </Row>
      )}
      {(title.toLowerCase() === 'document') && (
        <Row gutter={24}>
          <Col span={24}>
            <p className="mb-0">Document Category</p>
            <Form.Item>
              {getFieldDecorator('document_category_id', {
                rules: Helper.fieldRules(['required'], 'Document Category'),
                initialValue: currentData.document_category ? currentData.document_category.name : undefined,
              })(
                <Select
                  size="large"
                  className="w-100"
                  placeholder="Select Type"
                  loading={stateForm.documentLoad}
                >
                  {(stateForm.documentList || []).map(item => (
                    <Select.Option key={item.id} value={item.id}>{(item.name || '').toUpperCase()}</Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">Description</p>
            <Form.Item>
              {getFieldDecorator('description', {
                rules: [
                  ...Helper.fieldRules(['required'], 'Description'),
                  { min: 10, message: '*Minimal 10 karakter\n' },
                  { pattern: /^[a-zA-Z0-9-/\n/ ]*$/, message: '*Tidak boleh menggunakan spesial karakter\n' },
                ],
                initialValue: currentData.description || '',
              })(
                <Input.TextArea rows={4} size="large" placeholder="Description" className="uppercase" />,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <p className="mb-0">File</p>
            <Form.Item>
              {getFieldDecorator('file', {
                rules: Helper.fieldRules((!isEdit ? ['required'] : []), 'File'),
                initialValue: stateFile.file || [],
              })(
                <Dragger
                  accept="image/*,.pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                  name="file"
                  multiple="true"
                  listType="picture-card"
                  beforeUpload={() => false}
                  onChange={(info) => {
                    if (info.fileList.length > 5) {
                      return false
                    }

                    handleUpload(info)

                    return true
                  }}
                  fileList={stateFile.file}
                  showUploadList={{ showRemoveIcon: true, showPreviewIcon: false }}
                  style={{ marginBottom: '10px', display: (stateFile.file.length >= 5 ? 'none' : '') }}
                >
                  {stateFile.file.length < 5 && (
                    <div>
                      <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                      </p>
                      <p className="ant-upload-text">Click or drag file to this area to upload</p>
                      <p className="ant-upload-hint">
                        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                        band files
                      </p>
                    </div>
                  )}
                </Dragger>,
              )}
            </Form.Item>
          </Col>
        </Row>
      )}
      <Divider />
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
          >
            Submit
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

CardForm.propTypes = {
  toggle: PropTypes.func,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  stateForm: PropTypes.object,
  data: PropTypes.object,
  handleUpload: PropTypes.func,
  stateFile: PropTypes.object,
  stateCard: PropTypes.object,
  stateSelect: PropTypes.object,
  handleSearch: PropTypes.func,
  handleSearchProduct: PropTypes.func,
}

export default CardForm
