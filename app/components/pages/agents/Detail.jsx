/* eslint-disable no-nested-ternary */
/* eslint-disable no-duplicate-case */
import React from 'react'
import { Prompt } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  Button, Card,
  Row, Col, Modal,
  Divider, Select,
  Avatar, Descriptions,
  Empty, Tag, Skeleton, Pagination,
} from 'antd'
import { DoubleRightOutlined, LeftOutlined } from '@ant-design/icons'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'
import { isEmpty } from 'lodash'
import ChangeStatus from 'containers/pages/agents/ChangeStatus'
import VerifyOTP from 'containers/pages/agents/VerifyOTP'
import CardForm from 'containers/pages/agents/CardForm'

const DetailAgent = ({
  isFetching, detailAgent, donwloadHistory,
  stateButton, listButton, setListButton,
  avatar, setAvatar, loadMasterData,
  handleDetail, stateModal, setStateModal,
  stateCard, setStateCard, collectDataCard,
  isBlocking, handleBlockNavigation,
  handleUpdateCard, currentUser, sendVerification,
  handleCancelCard, setStateModalUpdate,
  stateModalUpdate, setStateButton,
  match, setBlocking, isVerifyOtp,
  handleResendVerifyOTP, isUpdatePayload, handlePageProduct, handlePageContract, handlePageLicense,
  handlePageBank, handlePageTaxation, handlePageDocument, handlePageTraining, handlePageLevel, handlePageContest,
  handlePageHistory,
}) => {
  const isMobile = window.innerWidth < 768
  const titleModalUpdate = stateCard.title.toLowerCase() !== 'product' ? 'Edit' : 'Expiry'
  const titleModalCreate = stateCard.title.toLowerCase() !== 'bank' ? 'Add' : 'Retrieve'


  return (
    <React.Fragment>
      <Prompt
        when={isBlocking}
        message={handleBlockNavigation}
      />
      <Row gutter={[24, 24]} className={isMobile ? 'mb-3 mt-3' : ''}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.push('/agent-search-menu')}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <Row className="w-100">
            <Col xs={24} md={13}><div className={`title-page ${isMobile ? 'mb-3' : ''}`}>AGENT</div></Col>
            <Col xs={24} md={11}>
              <Row span={4} className="d-flex justify-content-end align-items-center">
                <Col xs={24} xl={8}>
                  { detailAgent.status === 'non-verification' && detailAgent.type === 'mobile' && (
                    <Button
                      ghost
                      type="primary"
                      className="align-items-center btn-border-mitra"
                      onClick={sendVerification}
                    >
                      Send Verification
                    </Button>
                  )}
                </Col>
                <Col xs={24} xl={8}>
                  <Button
                    type="link"
                    size="large"
                    className="d-flex align-items-center"
                    onClick={() => setStateModal({ ...stateModal, visible: true })}
                  >
                    <i className="las la-random mr-2" />
                    Change Status
                  </Button>
                </Col>
                <Col xs={24} xl={8}>
                  {(currentUser.permissions && currentUser.permissions.indexOf('agent-edit') > -1) && (
                    <Button
                      type="link"
                      size="large"
                      className="d-flex align-items-center"
                      onClick={() => history.push(`/agent-search-menu/${detailAgent.id}/edit`)}
                    >
                      <i className="las la-edit mr-2" />
                      Edit Agent
                    </Button>
                  )}
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={[24, 24]} className="mb-4">
        <Col xs={24} md={12}>
          <Row gutter={0} className="h-100">
            <Col span={24} className="mb-4">
              <Card className="h-100" loading={isFetching}>
                <Row gutter={24}>
                  <Col span={24}>
                    <Row className="w-100 d-flex justify-content-between">
                      <Col xs={24} md={12}>
                        <p className="title-card m-0 text-uppercase">{detailAgent.name || '-'}</p>
                      </Col>
                      <Col xs={24} md={12}>
                        <Button
                          className={`text-white text-uppercase rounded-pill ${isMobile ? 'mt-2' : 'float-right'}`}
                          style={{ backgroundColor: '#51b72b', width: 'auto' }}
                        >
                          {detailAgent.status ? Helper.getValue((detailAgent.status).replace('-', ' ')) : 'Non Verification'}
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={24} className="d-block d-md-flex justify-content-between align-items-end">
                    <div>
                      <Avatar
                        src={avatar || '/assets/avatar-on-error.jpg'}
                        shape="square"
                        size={150}
                        className="img-contain"
                        onError={() => setAvatar('/assets/avatar-on-error.jpg')}
                      />
                    </div>
                    <div className="text-right">
                      <p className="mt-auto mb-0">JOIN DATE</p>
                      <p className="mb-0 text-uppercase">{detailAgent.created_at ? moment(detailAgent.created_at).format('DD MMM YYYY') : '-'}</p>
                    </div>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={24}>
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Personal Info Agent</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item span={2} label="Agent Name" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.name || '-'}
                  </Descriptions.Item>
                  {detailAgent.source !== 'corporate' && (
                    <>
                      <Descriptions.Item span={2} label="Nomor KTP" className="px-1 text-uppercase profile-detail pb-0">
                        {detailAgent.no_ktp || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Birth Place" className="px-1 text-uppercase profile-detail pb-0">
                        {detailAgent.birth_place || ' -'}
                      </Descriptions.Item>
                    </>
                  )}
                  {detailAgent.source !== 'corporate' && (
                    <>
                      <Descriptions.Item span={2} label="Nomor NPWP" className="px-1 text-uppercase profile-detail pb-0">
                        {detailAgent.npwp || '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Birth Place" className="px-1 text-uppercase profile-detail pb-0">
                        {detailAgent.birth_place || '-'}
                      </Descriptions.Item>
                    </>
                  )}
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Day of Birth" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.birthdate ? moment.utc(detailAgent.birthdate).format('DD MMM YYYY') : '-'}
                  </Descriptions.Item>
                  {detailAgent.source !== 'corporate' && (
                    <>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Gender" className="px-1 text-uppercase profile-detail pb-0">
                        {!isEmpty(detailAgent.gender) ? detailAgent.gender.name : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Religion" className="px-1 text-uppercase profile-detail pb-0">
                        {!isEmpty(detailAgent.religion) ? detailAgent.religion.name : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Marital Status" className="px-1 text-uppercase profile-detail pb-0">
                        {!isEmpty(detailAgent.marital_status) ? detailAgent.marital_status.name : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 2 : 1} label="Employment" className="px-1 text-uppercase profile-detail pb-0">
                        {!isEmpty(detailAgent.employment) ? detailAgent.employment.name : '-'}
                      </Descriptions.Item>
                    </>
                  )}
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Email" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.user ? Helper.getValue(detailAgent.user.email) : '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Phone Number" className="px-1 text-uppercase profile-detail pb-0">
                    {!isEmpty(detailAgent.phone_code) ? detailAgent.phone_code.phone_code : '-'} {detailAgent.user ? Helper.getValue(detailAgent.user.phone_number) : '-'}
                  </Descriptions.Item>
                  {detailAgent.source !== 'corporate' ? (
                    <Descriptions.Item span={2} label="Citizenship" className="px-1 text-uppercase profile-detail pb-0">
                      {((detailAgent.citizenship || '').toLowerCase()) || '-'}
                    </Descriptions.Item>
                  ) : <Descriptions.Item span={2} />}
                  <Descriptions.Item span={2} label="Alamat" className="px-1 text-uppercase profile-detail pb-0">
                    <p className="mb-0">
                      {detailAgent.province ? detailAgent.province.name : ''}
                      {' '}
                      -
                      {' '}
                      {detailAgent.address_zip_code}
                    </p>
                    <p>{detailAgent.address || '-'}</p>
                  </Descriptions.Item>
                  <Descriptions.Item span={2} label="Alamat Alternative" className="px-1 text-uppercase profile-detail pb-0">
                    <p className="mb-0">
                      {detailAgent.residence_province ? detailAgent.residence_province.name : ''}
                      -
                      {detailAgent.residence_zip_code}
                    </p>
                    <p>{detailAgent.residence_address || '-'}</p>
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={0} className="h-100">
            <Col span={24} className="mb-4">
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Agent ID</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent Type" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.type || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent Source" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.source || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Agent ID" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.agent_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Profile ID" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.agent_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="ID Syariah" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.syariah_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Reference ID" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.reference_id || '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={isMobile ? 2 : 1} label="Upline Agent ID" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.upline_id || '-'}
                    {detailAgent.upline ? ' - ' : ''}
                    {detailAgent.upline ? Helper.getValue(detailAgent.upline.name) : ''}
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
            <Col span={24}>
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Work Area</p>
                <Descriptions layout="vertical" colon={false} column={1}>
                  <Descriptions.Item span={1} label="Agent Level" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.level ? detailAgent.level.name : '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Branch" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.branch ? detailAgent.branch.name : '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Branch Perwakilan" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.branch_perwakilan ? detailAgent.branch_perwakilan.name : '-'}
                  </Descriptions.Item>
                  <Descriptions.Item span={1} label="Kode Ulas" className="px-1 text-uppercase profile-detail pb-0">
                    {detailAgent.review_code ? detailAgent.review_code.ulas_code : '-'}
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Card>
            <Row gutter={24}>
              <Col xs={24} md={6} xl={4} style={isMobile ? { padding: 0 } : { padding: 12 }}>
                {isMobile && (
                  <Select
                    onChange={(val) => {
                      setListButton({ ...listButton, active: val })
                      handleDetail(val)
                    }}
                    value={listButton.active}
                    className="field-lg w-100"
                  >
                    {(stateButton.list || []).map(item => (
                      <Select.Option key={`select_${item}`} value={item}>{item}</Select.Option>
                    ))}
                  </Select>
                )}
                {!isMobile && (listButton.list || []).map(item => (
                  <Button
                    className="button-profile shadow"
                    key={`button_${item}`}
                    type={listButton.active === item ? 'primary' : ''}
                    onClick={() => {
                      setListButton({ ...listButton, active: item })
                      handleDetail(item)
                    }}
                  >
                    {item}
                    {listButton.active === item ? <DoubleRightOutlined /> : null}
                  </Button>
                ))}
              </Col>
              <Col xs={24} md={18} xl={20} className="px-0 px-md-5 h-100">
                <Row className="d-flex justify-content-between align-items-center">
                  <Col xs={24} md={12}>
                    <p className="title-card pt-3 pt-md-0">
                      {((listButton.active === 'Training') || (listButton.active === 'Contest'))
                        ? `History ${listButton.active}`
                        : listButton.active
                      }
                    </p>
                  </Col>
                  <Col xs={24} md={12}>
                    {(() => {
                      switch (listButton.active) {
                        case 'Product':
                        case 'Contract':
                        case 'License':
                        case 'Document':
                        case 'Bank':
                          return (
                            <Button
                              type="link"
                              className={`p-0 d-flex align-items-center ${!isMobile ? 'float-right' : ''}`}
                              onClick={() => (
                                setStateCard({
                                  ...stateCard,
                                  visible: true,
                                  isEdit: false,
                                  data: {},
                                  title: (listButton.active).toLowerCase(),
                                })
                              )}
                            >
                              <i className="las la-plus-circle mr-1" style={{ fontSize: '24px' }} />
                              {`${listButton.active !== 'Bank' ? 'Add' : 'Retrieve'} New ${listButton.active}`}
                            </Button>
                          )
                        case 'History':
                          return (
                            <Button
                              ghost
                              type="primary"
                              className={`p-10 d-flex align-items-center ${!isMobile ? 'float-right' : ''}`}
                              onClick={() => donwloadHistory(detailAgent.id)}
                            >
                              Download History
                            </Button>
                          )
                        default:
                          return ''
                      }
                    })()}
                  </Col>
                </Row>
                <Card className="border-0 card-description shadow-none">
                  {(
                    ((listButton.active === 'Product') && isEmpty(stateButton.products))
                    || ((listButton.active === 'Contract') && isEmpty(stateButton.contracts))
                    || ((listButton.active === 'License') && isEmpty(stateButton.licenses))
                    || ((listButton.active === 'Level') && isEmpty(stateButton['level-histories']))
                    || ((listButton.active === 'Taxation') && isEmpty(stateButton.taxation))
                    || ((listButton.active === 'Document') && isEmpty(stateButton.documents))
                    || ((listButton.active === 'Training') && isEmpty(stateButton.training_histories))
                    || ((listButton.active === 'Bank') && isEmpty(stateButton.bank_accounts))
                    || ((listButton.active === 'Contest') && isEmpty(stateButton.contest_histories))
                    || ((listButton.active === 'History') && isEmpty(stateButton['audit-trails']))
                  ) && (
                    <Empty description="Tidak ada data" />
                  )}
                  {listButton.active === 'Product' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.products || []).map((item, i) => (
                        <React.Fragment key={`product_${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Product" className="px-1 text-uppercase profile-detail pb-0">
                              {item.product ? Helper.getValue(item.product.display_name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Efektif" className="px-1 text-uppercase profile-detail pb-0">
                              {item.effective_date ? moment(item.effective_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Expiry" className="px-1 text-uppercase profile-detail pb-0">
                              {item.expiry_date ? moment(item.expiry_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('agent-product-menu') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 1} className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'product',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                          {stateButton.products.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.products_pagination ? Number(stateButton.products_meta.current_page) : 1}
                              onChange={handlePageProduct}
                              total={stateButton.products_meta.total_count}
                              pageSize={stateButton.products_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Contract' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.contracts || []).map((item, i) => (
                        <React.Fragment key={`contract_${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={6}>
                            <Descriptions.Item span={isMobile ? 6 : 1} label="Contract" className="px-1 text-uppercase profile-detail pb-0">
                              {item.contract_type ? Helper.getValue(item.contract_type.name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 6 : 1} label="Nomor Contract" className="px-1 text-uppercase profile-detail pb-0">
                              {item.contract_number || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 6 : 1} label="Tanggal Efektif" className="px-1 text-uppercase profile-detail pb-0">
                              {item.start_date ? moment(item.start_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 6 : 1} label="Tanggal Expiry" className="px-1 text-uppercase profile-detail pb-0">
                              {item.end_date ? moment(item.end_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 6 : 1} label="Tanggal Pemutusan" className="px-1 text-uppercase profile-detail pb-0">
                              {item.termination_date ? moment(item.termination_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('agent-contract-menu') > -1) && (
                              <Descriptions.Item span={isMobile ? 6 : 1} className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'contract',
                                    })
                                  )}
                                  className="p-0"
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                          {stateButton.contracts.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.contracts_pagination ? Number(stateButton.contracts_meta.current_page) : 1}
                              onChange={handlePageContract}
                              total={stateButton.contracts_meta.total_count}
                              pageSize={stateButton.contracts_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'License' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.licenses || []).map((item, i) => (
                        <React.Fragment key={`lecense_${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="License Type" className="px-1 text-uppercase profile-detail pb-0">
                              {item.license_type ? Helper.getValue(item.license_type.name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="License No" className="px-1 text-uppercase profile-detail pb-0">
                              {item.license_number || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Awal" className="px-1 text-uppercase profile-detail pb-0">
                              {item.start_date ? moment(item.start_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="Tanggal Akhir" className="px-1 text-uppercase profile-detail pb-0">
                              {item.end_date ? moment(item.end_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('agent-license-menu') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 1} className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'license',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                          {stateButton.licenses.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.licenses_pagination ? Number(stateButton.licenses_meta.current_page) : 1}
                              onChange={handlePageLicense}
                              total={stateButton.licenses_meta.total_count}
                              pageSize={stateButton.licenses_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Taxation' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.taxation || []).map((item, i) => (
                        <React.Fragment key={`tax_${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Tax Type" className="px-1 text-uppercase profile-detail pb-0">
                              {item.type ? Helper.getValue(item.type.name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 3} label="Tax ID" className="px-1 text-uppercase profile-detail pb-0">
                              {item.npwp_number || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Tax Name" className="px-1 text-uppercase profile-detail pb-0">
                              {item.name || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Status PTKP" className="px-1 text-uppercase profile-detail pb-0">
                              {item.ptkp_status ? Helper.getValue(item.ptkp_status.name) : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('agent-taxation') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 1} className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'taxation',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                          {stateButton.taxation.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.taxation_pagination ? Number(stateButton.taxation_meta.current_page) : 1}
                              onChange={handlePageTaxation}
                              total={stateButton.taxation_meta.total_count}
                              pageSize={stateButton.taxation_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Document' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.documents || []).map((item, i) => (
                        <React.Fragment>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : (((item.files || []) || (item.file_url || [])).length > 1) ? 5 : 2} className="px-1 text-uppercase profile-detail pb-0">
                              {((item.files || []) || (item.file_url || [])).map(data => (
                                <Avatar
                                  shape="square"
                                  size={150}
                                  style={isMobile ? { marginRight: 'auto' } : ((item.files || item.file_url).length > 1) ? { marginRight: '20px', marginBottom: '20px' } : { marginRight: 'auto' }}
                                  src={data.file_url || data.thumbUrl}
                                  className="img-contain"
                                />
                              ))}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Kategori" className="px-1 text-uppercase profile-detail pb-0">
                              {item.document_category ? Helper.getValue(item.document_category.name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Deskripsi" className="px-1 text-uppercase profile-detail pb-0">
                              {item.description || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Upload By" className="px-1 text-uppercase profile-detail pb-0">
                              {item.uploader ? Helper.getValue(item.uploader.name) : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('agent-document-menu') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 2} label="Action" className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'document',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                          {stateButton.documents.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.documents_pagination ? Number(stateButton.documents_meta.current_page) : 1}
                              onChange={handlePageDocument}
                              total={stateButton.documents_meta.total_count}
                              pageSize={stateButton.documents_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Bank' && (
                    (stateButton.bank_accounts || []).map((item, i) => (
                      <React.Fragment key={`kontes_${item.contest_id}`}>
                        <Descriptions layout="vertical" colon={false} column={5}>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nama Bank" className="px-1 text-uppercase profile-detail pb-0">
                            {item.bank ? Helper.getValue(item.bank.name) : '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nama" className="px-1 text-uppercase profile-detail pb-0">
                            {item.name || '-'}
                          </Descriptions.Item>
                          <Descriptions.Item span={isMobile ? 5 : 1} label="Nomor Rekening" className="px-1 text-uppercase profile-detail pb-0">
                            {item.account_number || '-'}
                          </Descriptions.Item>
                        </Descriptions>
                        <Divider />
                        {stateButton.bank_accounts.length === i + 1
                        && (
                          <Pagination
                            className="py-3"
                            showTotal={() => 'Page'}
                            current={stateButton.bank_accounts_pagination ? Number(stateButton.bank_accounts_meta.current_page) : 1}
                            onChange={handlePageBank}
                            total={stateButton.bank_accounts_meta.total_count}
                            pageSize={stateButton.bank_accounts_meta.per_page || 10}
                          />
                        )
                        }
                      </React.Fragment>
                    ))
                  )}

                  {listButton.active === 'Training' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.training_histories || []).map((item, i) => (
                        <React.Fragment key={`training_${Math.random()}`}>
                          <Descriptions layout="vertical" colon={false} column={3}>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Subject" className="px-1 text-uppercase profile-detail pb-0">
                              {item.subject_name || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Tanggal Training" className="px-1 text-uppercase profile-detail pb-0">
                              {item.training_date ? moment(item.training_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Status" className="px-1 text-uppercase profile-detail pb-0">
                              {item.is_present ? 'HADIR' : 'TIDAK HADIR'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                          {stateButton.training_histories.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.training_histories_pagination ? Number(stateButton.training_histories_meta.current_page) : 1}
                              onChange={handlePageTraining}
                              total={stateButton.training_histories_meta.total_count}
                              pageSize={stateButton.training_histories_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Contest' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton.contest_histories || []).map((item, i) => (
                        <React.Fragment key={`kontes_${Math.random()}`}>
                          <Descriptions layout="vertical" colon={false} column={4}>
                            <Descriptions.Item span={isMobile ? 4 : 1} label="Kontes ID" className="px-1 text-uppercase profile-detail pb-0">
                              {item.contest_id || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 4 : 1} label="Nama Kontes" className="px-1 text-uppercase profile-detail pb-0">
                              {item.contest_name || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 4 : 1} label="Periode Kontes" className="px-1 text-uppercase profile-detail pb-0">
                              {item.contest_period || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 4 : 1} label="Reward" className="px-1 text-uppercase profile-detail pb-0">
                              {item.reward_name || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                          {stateButton.contest_histories.length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.contest_histories_pagination ? Number(stateButton.contest_histories_meta.current_page) : 1}
                              onChange={handlePageContest}
                              total={stateButton.contest_histories_meta.total_count}
                              pageSize={stateButton.contest_histories_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'History' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton['audit-trails'] || []).map((item, i) => (
                        <React.Fragment key={`kontes_${item.contest_id}`}>
                          <Descriptions layout="vertical" colon={false} column={3}>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Tanggal History" className="px-1 text-uppercase profile-detail pb-0">
                              {item.history_date ? moment(item.history_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Action" className="px-1 text-uppercase profile-detail pb-0">
                              {item.action || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="User Action" className="px-1 text-uppercase profile-detail pb-0">
                              {item.user_action || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                          {stateButton['audit-trails'].length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.audit_trails_pagination ? Number(stateButton.audit_trails_meta.current_page) : 1}
                              onChange={handlePageHistory}
                              total={stateButton.audit_trails_meta.total_count}
                              pageSize={stateButton.audit_trails_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}

                  {listButton.active === 'Level' && (
                    isFetching
                      ? <Skeleton />
                      : (stateButton['level-histories'] || []).map((item, i) => (
                        <React.Fragment key={`kontes_${item.contest_id}`}>
                          <Descriptions layout="vertical" colon={false} column={3}>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Tanggal Efektif" className="px-1 text-uppercase profile-detail pb-0">
                              {item.effective_date ? moment(item.effective_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="Level Name" className="px-1 text-uppercase profile-detail pb-0">
                              {item.level_name || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 3 : 1} label="User Action" className="px-1 text-uppercase profile-detail pb-0">
                              {item.user_action || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                          {stateButton['level-histories'].length === i + 1
                          && (
                            <Pagination
                              className="py-3"
                              showTotal={() => 'Page'}
                              current={stateButton.level_histories_pagination ? Number(stateButton.level_histories_meta.current_page) : 1}
                              onChange={handlePageLevel}
                              total={stateButton.level_histories_meta.total_count}
                              pageSize={stateButton.level_histories_meta.per_page || 10}
                            />
                          )
                          }
                        </React.Fragment>
                      ))
                  )}
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      {(currentUser.permissions && currentUser.permissions.indexOf('agent-edit') > -1) && (
        <Row className="my-3 mb-3">
          <Col span={24} className="d-flex justify-content-end align-items-center mb-3">
            <Button
              size="large"
              type="primary"
              disabled={isBlocking}
              onClick={handleUpdateCard}
              className={`button-lg mr-3 ${isMobile ? 'w-40' : 'w-15'}`}
            >
              Update
            </Button>
            <Button
              ghost
              size="large"
              type="primary"
              disabled={isBlocking}
              onClick={handleCancelCard}
              className={`button-lg border-lg ${isMobile ? 'w-40' : 'w-15'}`}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      )}

      <Modal
        title={<p className="mb-0 title-card">Verify OTP</p>}
        visible={stateModalUpdate.visible}
        footer={null}
        closable={false}
        maskClosable={false}
        width={500}
        onCancel={() => setStateModalUpdate({ ...stateModalUpdate, visible: false })}
      >
        <VerifyOTP
          data={{
            match,
            stateButton,
            setStateButton,
            setBlocking,
            isVerifyOtp,
            isUpdatePayload,
            setStateModalUpdate,
            stateModalUpdate,
            handleResendVerifyOTP,
          }}
          toggle={() => {
            setStateModalUpdate({ ...stateModalUpdate, visible: false })
            setBlocking(false)
            setStateButton({
              ...stateButton,
              isFetching: false,
            })
          }}
        />
      </Modal>

      <Modal
        title={<p className="mb-0 title-card">Change Status</p>}
        visible={stateModal.visible}
        footer={null}
        closable={false}
        width={650}
        onCancel={() => setStateModal({ ...stateModal, visible: false })}
      >
        <ChangeStatus
          toggle={() => setStateModal({ ...stateModal, visible: false })}
          id={detailAgent.id}
          loadMaster={() => loadMasterData()}
        />
      </Modal>

      <Modal
        title={<p className="mb-0 title-card text-capitalize">{`${stateCard.isEdit ? titleModalUpdate : titleModalCreate} ${stateCard.title}`}</p>}
        visible={stateCard.visible}
        footer={null}
        closable={false}
        onCancel={() => setStateCard({ ...stateCard, isEdit: false, visible: false })}
      >
        <CardForm
          toggle={() => setStateCard({ ...stateCard, visible: false })}
          data={stateCard}
          stateCard={stateButton}
          submitForm={(data, title, isEdit) => collectDataCard(data, title, isEdit)}
        />
      </Modal>
    </React.Fragment>
  )
}

DetailAgent.propTypes = {
  detailAgent: PropTypes.object,
  isFetching: PropTypes.bool,
  handleDetail: PropTypes.func,
  stateButton: PropTypes.object,
  stateModal: PropTypes.object,
  setStateModal: PropTypes.func,
  setAvatar: PropTypes.func,
  avatar: PropTypes.string,
  loadMasterData: PropTypes.func,
  stateCard: PropTypes.object,
  setStateCard: PropTypes.func,
  collectDataCard: PropTypes.func,
  isBlocking: PropTypes.bool,
  handleBlockNavigation: PropTypes.func,
  handleUpdateCard: PropTypes.func,
  currentUser: PropTypes.object,
  listButton: PropTypes.object,
  setListButton: PropTypes.func,
  handleCancelCard: PropTypes.func,
  sendVerification: PropTypes.func,
  donwloadHistory: PropTypes.func,
  setStateModalUpdate: PropTypes.func,
  stateModalUpdate: PropTypes.object,
  setStateButton: PropTypes.func,
  setBlocking: PropTypes.func,
  match: PropTypes.object,
  isVerifyOtp: PropTypes.bool,
  isUpdatePayload: PropTypes.object,
  handleResendVerifyOTP: PropTypes.func,
  handlePageProduct: PropTypes.func,
  handlePageContract: PropTypes.func,
  handlePageLicense: PropTypes.func,
  handlePageBank: PropTypes.func,
  handlePageTaxation: PropTypes.func,
  handlePageDocument: PropTypes.func,
  handlePageTraining: PropTypes.func,
  handlePageLevel: PropTypes.func,
  handlePageContest: PropTypes.func,
  handlePageHistory: PropTypes.func,
}

export default DetailAgent
