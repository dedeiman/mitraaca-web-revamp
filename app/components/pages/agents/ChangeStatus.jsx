/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Table,
  Select, DatePicker,
  Divider, Descriptions,
  Button, Input, AutoComplete,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'
import moment from 'moment'

const ChangeStatus = ({
  toggle, form, onSubmit,
  detailAgent, stateForm,
  handleSearchAgent, loadSubmitBtn,
  getDownline, setStateForm,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={24}>
        <Col span={24}>
          <Descriptions layout="vertical" colon={false} column={3}>
            <Descriptions.Item label="Profile ID" className="px-1 profile-detail pb-0">
              {detailAgent.profile_id || '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Agent ID" className="px-1 profile-detail pb-0">
              {detailAgent.agent_id || '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Nama Agent" className="px-1 profile-detail pb-0">
              {detailAgent.name || '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Tingkatan" className="px-1 profile-detail pb-0">
              {detailAgent.level ? detailAgent.level.name : '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Current Status" className="px-1 profile-detail pb-0">
              <Button
                className="text-white text-capitalize rounded-pill"
                style={{ backgroundColor: '#51b72b', width: '100%' }}
              >
                {(detailAgent.status || 'Non Verification').toUpperCase()}
              </Button>
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={12}>
          <p className="mb-0">Tanggal Efektif</p>
          <Form.Item>
            {getFieldDecorator('effective_date', {
              rules: Helper.fieldRules(['required']),
              initialValue: moment(),
            })(
              <DatePicker
                disabled
                size="large"
                className="w-100"
                format="DD MMMM YYYY"
                disabledDate={current => (current && (current < moment().subtract(1, 'day')))}
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <p className="mb-0">Change Status to</p>
          <Form.Item>
            {getFieldDecorator('status', {
              rules: Helper.fieldRules(['required']),
              initialValue: detailAgent.status,
              getValueFromEvent: (val) => {
                if (val.includes('restricted')) {
                  getDownline(detailAgent.agent_id)
                }
                return val
              },
            })(
              <Select
                size="large"
                className="w-100"
                disabled={getFieldValue('status') === 'passed-away'}
                placeholder="Select Status"
                loading={stateForm.statusLoad}
              >
                {(stateForm.statusList || []).map(item => (
                  <Select.Option key={item.id} value={item.slug}>{item.display_name}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
        {/*
        <Col span={24}>
          <p className="mb-0">Reason</p>
          <Form.Item>
            {getFieldDecorator('reason', {
              rules: Helper.fieldRules(['required', 'notSpecial']),
            })(
              <Input.TextArea
                rows={3}
                size="large"
                placeholder="Input Reason"
              />,
            )}
          </Form.Item>
        </Col>
        */}
      </Row>
      <Divider />
      {(getFieldValue('status') && getFieldValue('status').includes('restricted')) && (
      <Row gutter={24}>
        <Col span={24}>
          <p className="font-weight-bold">List Agent</p>
        </Col>
        <Col span={24}>
          <Table
            dataSource={stateForm.downlineList}
            loading={stateForm.downlineLoad}
            columns={[
              {
                title: 'Agent ID',
                dataIndex: 'agent_id',
              },
              {
                title: 'Agent Name',
                dataIndex: 'name',
              },
              {
                title: 'Join Date',
                dataIndex: 'created_at',
                render: item => moment(item).format('DD MMM YYYY'),
              },
              {
                title: 'Action',
                render: item => (
                  <Form.Item>
                    {getFieldDecorator(`action_${item.agent_id}`, {
                      rules: Helper.fieldRules(['required']),
                    })(
                      <Select
                        size="large"
                        className="w-100"
                        placeholder="Select Status"
                      >
                        {(stateForm.downlineStatusList || []).map(itemStatus => (
                          <Select.Option key={itemStatus.id} value={itemStatus.slug}>{itemStatus.display_name}</Select.Option>
                        ))}
                      </Select>,
                    )}
                  </Form.Item>
                ),
              },
            ]}
            rowKey="id"
            pagination={false}
          />
        </Col>
      </Row>
      )}
      {(getFieldValue('status') === 'passed-away') && (
      <Row gutter={24}>
        <Col span={24}>
          <p className="font-weight-bold">Delegate to:</p>
        </Col>
        <Col span={12}>
          <p className="mb-0">Agent ID</p>
          <Form.Item>
            {getFieldDecorator('agent_id', {
              getValueFromEvent: (val) => {
                setFieldsValue({
                  profile_id: '',
                  agent_name: '',
                  agent_level: {},
                })
                return val
              },
            })(
              <AutoComplete
                onSelect={(val) => {
                  const dataDelegate = stateForm.agentList.find(item => item.agent_id === val)
                  setFieldsValue({ profile_id: (dataDelegate || {}).profile_id || '' })
                  return setStateForm({ ...stateForm, dataDelegate })
                }}
                onSearch={handleSearchAgent}
                options={(stateForm.agentList || []).map(item => ({ value: item.agent_id, label: `${item.agent_id} - ${item.name}`, disabled: item.agent_id === detailAgent.agent_id }))}
              >
                <Input.Search size="large" placeholder="Search by Name" />
              </AutoComplete>,
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <p className="mb-0">Profile ID</p>
          <Form.Item>
            {getFieldDecorator('profile_id')(
              <Input
                size="large"
                placeholder="Input Profile ID"
                disabled
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={24}>
          <Descriptions layout="vertical" colon={false} column={1}>
            <Descriptions.Item label="Nama Agent" className="px-1 profile-detail pb-0">
              {stateForm.dataDelegate ? stateForm.dataDelegate.name : '-'}
            </Descriptions.Item>
            <Descriptions.Item label="Tingkatan" className="px-1 profile-detail pb-0">
              {stateForm.dataDelegate ? stateForm.dataDelegate.level.name : '-'}
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={24}>
          <p className="mb-0">Hubungan Dengan Pewaris</p>
          <Form.Item>
            {getFieldDecorator('relationship_id', {
              rules: Helper.fieldRules(['required']),
              getValueFromEvent: (val) => {
                const dataDelegate = (stateForm.agentList || []).find(item => item.agent_id === getFieldValue('agent_id'))
                setStateForm({ ...stateForm, dataDelegate })

                return val
              },
            })(
              <Select
                size="large"
                className="w-100"
                placeholder="Select Relationship"
                loading={stateForm.relationshipsLoad}
              >
                {(stateForm.relationshipsList || []).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
      </Row>
      )}
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
            disabled={loadSubmitBtn}
          >
            Submit
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

ChangeStatus.propTypes = {
  detailAgent: PropTypes.object,
  toggle: PropTypes.func,
  form: PropTypes.any,
  stateForm: PropTypes.object,
  setStateForm: PropTypes.func,
  handleSearchAgent: PropTypes.func,
  onSubmit: PropTypes.func,
  getDownline: PropTypes.func,
  loadSubmitBtn: PropTypes.bool,
}

export default ChangeStatus
