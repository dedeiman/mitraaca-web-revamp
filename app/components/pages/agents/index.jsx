import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import List from 'containers/pages/agents/List'
import Detail from 'containers/pages/agents/Detail'
import Form from 'containers/pages/agents/Form'
import LevelTree from 'containers/pages/agents/LevelTree'
import history from 'utils/history'

const Agents = ({ location, match, currentUser, stateCheck }) => {
  if (location.pathname === '/agent-search-menu') {
    if (stateCheck.isLocked === true && window.localStorage.getItem('isTrueSecondary') === 'false') {
      history.push(`/check-secondary-password${location.pathname}`)
    } else if (currentUser.permissions && currentUser.permissions.indexOf('agent-search-menu') > -1) {
      return <List location={location} />
    }
    return <Forbidden />
  }
  if (match.params.id && !location.pathname.includes('/edit') && !location.pathname.includes('/upload') && !location.pathname.includes('/tree')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('agent-detail') > -1) {
      return <Detail match={match} location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/tree')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('agent-tree-level') > -1) {
      return <LevelTree match={match} location={location} />
    }
    return <Forbidden />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/agent-search-menu/add')) {
    if (currentUser.permissions && currentUser.permissions.indexOf('agent-create-menu') > -1) {
      return <Form match={match} />
    }
    return <Forbidden />
  }

  return ''
}

Agents.propTypes = {
  currentUser: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  stateCheck: PropTypes.object,
  setStateCheck: PropTypes.func,
}

export default Agents
