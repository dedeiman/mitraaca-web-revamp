/* eslint-disable consistent-return */
/* eslint-disable react/jsx-closing-tag-location */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Avatar, Tree as TreeList,
  Button, Card, Select,
  Row, Col, AutoComplete,
  Typography, Empty, Input,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { Tree } from 'react-organizational-chart'
import { isEmpty, capitalize } from 'lodash'
import { BRANCH_CODE } from 'constants/ActionTypes'
import Helper from 'utils/Helper'

const LevelTree = ({
  form, currentUser, groupRole,
  handleBranch, onSubmit,
  stateCareer, setStateCareer,
  handleSearchAgent, handleExport,
}) => {
  const { getFieldDecorator, setFieldsValue } = form
  const { dataRoot } = stateCareer
  const isMobile = window.innerWidth < 768
  const element = []
  const treeData = []

  treeData.push({
    title: <Card
      style={{
        marginBottom: '10px',
        width: '350px',
        cursor: 'default',
      }}
    >
      <React.Fragment key={`list_tree_${Math.random()}`}>
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex justify-content-between align-items-center">
            <Avatar
              size="large"
              className="mr-3"
              style={{
                fontSize: '18px',
                backgroundColor: '#2b57b7',
                fontFamily: 'Poppins-Bold',
              }}
            >
              {capitalize((dataRoot.agent_name || 'ACA').replace(' ', '')[0])}
            </Avatar>
            <div>
              <p
                className="title-card mb-0"
                style={{ textAlign: 'left' }}
              >
                {(dataRoot.agent_name || 'ACA').toUpperCase()}
              </p>
              <p
                className="mb-0"
                style={{ textAlign: 'left' }}
              >
                {dataRoot.level_name || '-'}
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    </Card>,
    key: `${Math.random()}`,
    children: Helper.renderChildList(stateCareer.dataList.map(item => item)),
  })

  if (stateCareer.isTree) {
    element.push(
      <Col xs={24} md={24} key={Math.random()} className={isMobile ? 'mb-3' : ''}>
        <Card className="wrapper-career h-100" loading={stateCareer.dataLoad}>
          {!isEmpty(stateCareer.dataList) || !isEmpty(stateCareer.dataRoot)
            ? (
              <Tree
                lineWidth="2px"
                lineColor="#2b57b7"
                label={(
                  <Card className="card-career">
                    <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                      {dataRoot.level_name || '-'}
                    </Typography.Paragraph>
                    <Typography.Paragraph className="name" ellipsis={{ rows: 1 }}>
                      <Avatar className="initial">
                        {capitalize(
                          (dataRoot.type === 'company'
                            ? dataRoot.company_name
                            : dataRoot.agent_name
                          )
                            .replace(' ', '')[0],
                        )}
                      </Avatar>
                      {dataRoot.type === 'company'
                        ? dataRoot.company_name
                        : dataRoot.agent_name
                      }
                    </Typography.Paragraph>
                    <Typography.Paragraph className="label" ellipsis={{ rows: 1 }}>
                      {dataRoot.branch_name || '-'}
                    </Typography.Paragraph>
                  </Card>
                )}
              >
                {Helper.renderChild(stateCareer.dataList)}
              </Tree>
            )
            : <Empty description="Tidak ada Data." />
          }
        </Card>
      </Col>,
    )
  } else {
    element.push(
      <Col xs={8} md={8} key={Math.random()} className={isMobile ? 'mb-3' : ''}>
        {!isEmpty(stateCareer.dataList) || !isEmpty(stateCareer.dataRoot)
          ? (
            <TreeList
              treeData={treeData}
            />
          )
          : <Card><Empty description="Tidak ada Data." /></Card>
        }
      </Col>,
    )
  }

  return (
    <React.Fragment>
      <Form onSubmit={onSubmit}>
        <Row gutter={[24, 24]} className={isMobile ? 'mb-3 mt-3' : 'mb-3'}>
          <Col span={24}>
            <Button
              ghost
              className={`mr-md-3 ${stateCareer.isTree ? 'border-active-bottom' : ''}`}
              onClick={() => setStateCareer({ ...stateCareer, isTree: true })}
              type="link"
            >
              Agent Tree
            </Button>
            <Button
              ghost
              className={`${!stateCareer.isTree ? 'border-active-bottom' : ''}`}
              onClick={() => setStateCareer({ ...stateCareer, isTree: false })}
              type="link"
            >
              Agent List
            </Button>
          </Col>
          <Col xs={24}>
            <p className="m-0 font-weight-bold text-capitalize">{currentUser.name || '-'}</p>
            <p className="text-muted text-capitalize">
              {currentUser.role ? currentUser.role.name : '-'}
            </p>
          </Col>
          <Col xs={24} md={18} className="d-flex justify-content-between align-items-center">
            <Form.Item className="w-100 mb-0">
              {getFieldDecorator('key', {
                rules: [{ pattern: /^[a-zA-Z0-9- ]+$/, message: '*Wajib menggunakan huruf kapital & angka' }],
                initialValue: stateCareer.key || '',
              })(
                <AutoComplete
                  allowClear
                  onSelect={(val, key) => {
                    setFieldsValue({ key: key.label })
                    setStateCareer({ ...stateCareer, agentId: val, agent_id: key.id })
                  }}
                  onSearch={handleSearchAgent}
                  options={(stateCareer.agentList || []).map(item => ({ value: item.agent_id, id: item.id, label: `${item.agent_id} - ${(item.agent_name || '').toUpperCase()}` }))}
                >
                  <Input size="large" placeholder="Search by Name" />
                </AutoComplete>,
              )}
            </Form.Item>
            <Button htmlType="submit" type="primary ml-2 mt-1" size="large">Cari</Button>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item className="w-100 mb-0">
              {getFieldDecorator('branch', {
                initialValue: !stateCareer.branchLoad ? stateCareer.branch : undefined,
                getValueFromEvent: (val) => {
                  handleBranch(val)
                  return val
                },
              })(
                <Select
                  allowClear
                  size="large"
                  className="w-100 mt-1"
                  placeholder="All"
                  defaultValue={stateCareer.branchList[0]}
                  loading={stateCareer.branchLoad}
                >
                  {(stateCareer.branchList || []).map(item => (
                    <Select.Option
                      key={Math.random()}
                      value={item.id}
                      loading={stateCareer.branchLoad}
                      disabled={(groupRole.code === BRANCH_CODE) && !(currentUser.branch_perwakilan || []).map(data => data.branch_id).includes(item.id)}
                    >
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Row gutter={24}>
        <Col md={{ span: 5, offset: 19 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={handleExport}
          >
            Export Excel
          </Button>
        </Col>
      </Row>
      <Row gutter={24} className={`mb-5 mt-3 ${stateCareer.isTree ? '' : 'overflow-scroll'}`}>
        {element}
      </Row>
    </React.Fragment>
  )
}

LevelTree.propTypes = {
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  currentUser: PropTypes.object,
  stateCareer: PropTypes.object,
  setStateCareer: PropTypes.func,
  handleBranch: PropTypes.func,
  handleSearchAgent: PropTypes.func,
  handleExport: PropTypes.func,
  groupRole: PropTypes.object,
}

export default LevelTree
