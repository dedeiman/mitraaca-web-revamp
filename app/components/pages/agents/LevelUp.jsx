import React from 'react'
import PropTypes from 'prop-types'
import {
  Input,
  Row, Col,
  Select, Button,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

const LevelUp = ({
  toggle, form,
  data, stateForm,
  onSubmit, role,
}) => {
  const { getFieldDecorator } = form
  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={24}>
        <Col span={24}>
          <p className="mb-0">Agent Level</p>
          <Form.Item>
            {getFieldDecorator('to_level_id', {
              rules: Helper.fieldRules(['required']),
            })(
              <Select
                size="large"
                className="w-100"
                placeholder={data.level ? data.level.name : 'Select Level'}
                loading={stateForm.levelLoad}
              >
                {(stateForm.levelList || []).map(item => (
                  <Select.Option
                    key={item.id}
                    value={item.id}
                    disabled={(role.name === 'agency-admin') || (role.name === 'head-agency') ? (item.id === data.level_id) : (item.id <= data.level_id)}
                  >
                    {item.name}
                  </Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        </Col>
        <Col span={24}>
          <p className="mb-0">Reason</p>
          <Form.Item>
            {getFieldDecorator('reason', {
              rules: [
                ...Helper.fieldRules(['required']),
                { pattern: /^[a-zA-Z0-9,. ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                { pattern: /^.{10,}$/, message: '*Minimal 10 karakter' },
              ],
              getValueFromEvent: e => e.target.value,
            })(
              <Input.TextArea
                rows={3}
                size="large"
                className="uppercase"
                placeholder="Input Reason"
              />,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
          >
            Level Up
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

LevelUp.propTypes = {
  data: PropTypes.object,
  toggle: PropTypes.func,
  form: PropTypes.any,
  stateForm: PropTypes.object,
  onSubmit: PropTypes.func,
  role: PropTypes.object,
}

export default LevelUp
