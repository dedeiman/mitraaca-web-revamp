/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Radio,
  Row, Col, Avatar,
  DatePicker, Select,
  Card, Input, Upload,
  AutoComplete,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { InboxOutlined, LoadingOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import NumberFormat from 'react-number-format'
import Helper from 'utils/Helper'
import history from 'utils/history'
import moment from 'moment'

const FormCustomer = ({
  form, match, handleLocation, loadUlas,
  stateSelects, stateUlas, onSubmit, isFetching,
  handleUpload, file, handleSearchAgent,
  detailAgent, handleProfileId, stateCountry,
  stateCountryCode,
}) => {
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  const isEdit = !isEmpty(match.params.id)
  const isMobile = window.innerWidth < 768

  const prefixSelector = getFieldDecorator('phone_code_id', {
    initialValue: !isEmpty(detailAgent) ? (detailAgent.phone_code) ? (detailAgent.phone_code.phone_code) : '+62' : '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountry.list.slice(0, 1) || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  const prefixSelectorCode = getFieldDecorator('pic_phone_country_code', {
    initialValue: !isEmpty(detailAgent) ? (detailAgent.pic_phone_code) ? (detailAgent.pic_phone_code.phone_code) : '+62' : '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountryCode.list.slice(0, 1) || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  // Can not select days before today and today
  const disabledDate = (getFieldValue('source') !== 'corporate') ? current => current && current > moment().subtract(17, 'year') : ''

  return (
    <React.Fragment>
      <Row gutter={24} className={`${isMobile ? 'mt-3 mb-3' : 'mb-3'}`}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">{`${isEdit ? 'Edit' : 'Registrasi'} Agent`}</div>
        </Col>
      </Row>
      <Form onSubmit={onSubmit}>
        <Row gutter={24} className="mb-3">
          <Col xs={24} md={12}>
            <Row gutter={0} className="h-100">
              <Col xs={24} className="mb-4">
                <Card className="h-100">
                  <p className="title-card mb-4">Photo Profile Agent</p>
                  <Form.Item>
                    {getFieldDecorator('file', {
                      rules: isEdit ? null : Helper.fieldRules(['required'], 'Photo'),
                      getValueFromEvent: (info) => {
                        const isFileType = info.file.type === 'image/png' || info.file.type === 'image/jpeg' || info.file.type === 'image/jpg'
                        if (!isFileType) return undefined
                        return info
                      },
                    })(
                      <Upload
                        accept="image/*"
                        name="file"
                        listType="picture-card"
                        className="avatar-uploader banner-content"
                        showUploadList={false}
                        beforeUpload={() => false}
                        onChange={info => handleUpload(info, 'file')}
                      >
                        {file
                          ? (
                            <Avatar src={file} shape="square" className="banner-preview" />
                          )
                          : (
                            <div>
                              <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                              </p>
                              <p className="text-muted">Click or Drag file to this area</p>
                            </div>
                          )
                        }
                      </Upload>,
                    )}
                  </Form.Item>
                </Card>
              </Col>
              <Col xs={24} className={(getFieldValue('source') === 'corporate') && 'mb-4'}>
                <Card className="h-100">
                  <p className="title-card mb-4">Personal Info Agent</p>
                  <Row gutter={24}>
                    <Col xs={24}>
                      <p className="mb-0">Agent Name</p>
                      <Form.Item>
                        {getFieldDecorator('name', {
                          rules: [
                            ...Helper.fieldRules(['required', 'notSpecial'], 'Agent Name'),
                            { pattern: /^.{2,}$/, message: '*Minimal 2 karakter' },
                          ],
                          initialValue: detailAgent.name || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input
                            size="large"
                            className="uppercase"
                            placeholder="Input Agent Name"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24}>
                        <p className="mb-0">Nomor KTP</p>
                        <Form.Item>
                          {getFieldDecorator('no_ktp', {
                            rules: [
                              ...Helper.fieldRules(['required'], 'Nomor KTP'),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (!(/^[0-9+]+$/).test(value)) {
                                    return Promise.reject('*Nomor KTP harus menggunakan angka')
                                  }

                                  if (!value) return Promise.resolve()
                                  if (/^.{16,30}$/.test(value) === false) return Promise.reject('*Wajib 16 Character')

                                  return Promise.resolve()
                                },
                              },
                            ],
                            initialValue: detailAgent.no_ktp || '',
                          })(
                            <Input
                              size="large"
                              maxLength={16}
                              placeholder="Input Nomor KTP"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Birth Place</p>
                        <Form.Item>
                          {getFieldDecorator('birth_place', {
                            rules: Helper.fieldRules(['required', 'notSpecial'], 'Birth Place'),
                            initialValue: detailAgent.birth_place || '',
                            getValueFromEvent: (e) => {
                              const { value } = e.target
                              if (value !== '') return value
                              return value
                            },
                          })(
                            <Input
                              size="large"
                              className="uppercase"
                              placeholder="Input Birth Place"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    <Col xs={24} md={12}>
                      <p className="mb-0">Day of Birth</p>
                      <Form.Item>
                        {getFieldDecorator('birthdate', {
                          rules: Helper.fieldRules(['required'], 'Day of Birth'),
                          initialValue: !isEmpty(detailAgent) ? moment.utc(detailAgent.birthdate) : '',
                        })(
                          <DatePicker
                            size="large"
                            className="w-100"
                            format="DD MMMM YYYY"
                            defaultPickerValue={moment().subtract(17, 'year')}
                            disabledDate={disabledDate}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Gender</p>
                        <Form.Item>
                          {getFieldDecorator('gender_id', {
                            rules: Helper.fieldRules(['required'], 'Gender'),
                            initialValue: !isEmpty(detailAgent.gender) ? detailAgent.gender.id : undefined,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Gender"
                            >
                              {(stateSelects.gendersList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Religion</p>
                        <Form.Item>
                          {getFieldDecorator('religion_id', {
                            rules: Helper.fieldRules(['required'], 'Religion'),
                            initialValue: !isEmpty(detailAgent.religion) ? detailAgent.religion.id : undefined,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Religion"
                            >
                              {(stateSelects.religionsList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Marital Status</p>
                        <Form.Item>
                          {getFieldDecorator('marital_status_id', {
                            rules: Helper.fieldRules(['required'], 'Marital Status'),
                            initialValue: !isEmpty(detailAgent.marital_status) ? detailAgent.marital_status.id : undefined,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Status"
                              loading={stateSelects.maritalLoad}
                            >
                              {(stateSelects.maritalList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.display_name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Employment</p>
                        <Form.Item>
                          {getFieldDecorator('employment_id', {
                            rules: Helper.fieldRules(['required'], 'Employment'),
                            initialValue: !isEmpty(detailAgent.employment) ? detailAgent.employment.id : undefined,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Employment"
                              loading={stateSelects.employmentLoad}
                            >
                              {(stateSelects.employmentList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.display_name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    {getFieldValue('employment_id') === '938e11b9-8294-4619-8777-d110cee7642f' && (
                      <Col xs={24} md={24}>
                        <p className="mb-0">Other Employment</p>
                        <Form.Item>
                          {getFieldDecorator('gender_id', {
                            rules: Helper.fieldRules(['required'], 'Gender'),
                            initialValue: !isEmpty(detailAgent.gender) ? detailAgent.gender.id : undefined,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Gender"
                            >
                              {(stateSelects.gendersList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    <Col xs={24} md={12}>
                      <p className="mb-0">Email</p>
                      <Form.Item>
                        {getFieldDecorator('email', {
                          rules: Helper.fieldRules(['required', 'email'], 'Email'),
                          initialValue: detailAgent.user ? detailAgent.user.email : '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') return value
                            return value
                          },
                        })(
                          <Input
                            size="large"
                            className="uppercase"
                            placeholder="Input Email"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Phone Number</p>
                      <Form.Item>
                        {getFieldDecorator('phone_number',
                          {
                            initialValue: detailAgent.user ? detailAgent.user.phone_number : '',

                            rules: [
                              ...Helper.fieldRules(['required'], 'Phone number'),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (!(/^[0-9+]+$/).test(value)) {
                                    return Promise.reject('*Phone number harus menggunakan angka')
                                  }

                                  if (value.charAt(0) === 0) {
                                    return Promise.reject('*Cannot input first number with "0"')
                                  }

                                  if (value.length < 8) {
                                    return Promise.reject('*Minimal 8 digit')
                                  }

                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                          <Input
                            className="field-lg"
                            maxLength={16}
                            size="large"
                            autoComplete="off"
                            addonBefore={prefixSelector}
                            placeholder="Input Phone Number"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    {(getFieldValue('source') !== 'corporate') && (
                      <Col xs={24} md={12}>
                        <p className="mb-0">Citizenship</p>
                        <Form.Item>
                          {getFieldDecorator('citizenship', {
                            rules: Helper.fieldRules(['required'], 'Citizenship'),
                            initialValue: detailAgent.citizenship,
                            getValueFromEvent: e => e,
                          })(
                            <Select
                              size="large"
                              placeholder="Select Citizenship"
                              className="field-lg"
                            >
                              {[{ id: 'wni', name: 'WNI' }].map(item => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    )}
                    <Col xs={24} md={12} />
                    <Col xs={24} md={12}>
                      <p className="mb-0">Address</p>
                      <Form.Item className="mb-2">
                        {getFieldDecorator('province_id', {
                          rules: Helper.fieldRules(['required'], 'Province'),
                          initialValue: detailAgent.province_id || undefined,
                        })(
                          <Select
                            size="large"
                            placeholder="Select Province"
                            loading={stateSelects.provincyLoad}
                          >
                            {(stateSelects.provincyList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12} style={{ marginTop: '1.4rem' }}>
                      <Form.Item>
                        {getFieldDecorator('address_zip_code', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Zip Code'),
                            { pattern: /^.{0,6}$/, message: '*Maksimal 6 karakter' },
                          ],
                          initialValue: detailAgent.address_zip_code,
                        })(
                          <Input
                            size="large"
                            placeholder="Input Zip Code"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('address', {
                          rules: Helper.fieldRules(['required'], 'Address'),
                          initialValue: detailAgent.address || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input.TextArea
                            rows={3}
                            size="large"
                            className="uppercase"
                            placeholder="Input Address"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Alternatif Address</p>
                      <Form.Item className="mb-2">
                        {getFieldDecorator('residence_province_id', {
                          initialValue: detailAgent.residence_province_id || undefined,
                        })(
                          <Select
                            size="large"
                            placeholder="Select Province"
                            loading={stateSelects.provincyLoad}
                          >
                            {(stateSelects.provincyList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12} style={{ marginTop: '1.4rem' }}>
                      <Form.Item>
                        {getFieldDecorator('residence_zip_code', {
                          rules: [
                            ...Helper.fieldRules(['number'], 'Zip Code'),
                            { pattern: /^.{0,6}$/, message: '*Maksimal 6 karakter' },
                          ],
                          initialValue: detailAgent.residence_zip_code,
                        })(
                          <Input
                            size="large"
                            maxLength={6}
                            placeholder="Input Zip Code"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={24}>
                      <Form.Item>
                        {getFieldDecorator('residence_address', {
                          initialValue: detailAgent.residence_address || '',
                          getValueFromEvent: e => e.target.value,
                        })(
                          <Input.TextArea
                            rows={3}
                            size="large"
                            className="uppercase"
                            placeholder="Input Alternatif Address"
                          />,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              {(getFieldValue('source') === 'corporate') && (
                <Col xs={24}>
                  <Card className="h-100">
                    <p className="title-card mb-4">Company</p>
                    <Row gutter={24}>
                      <Col xs={24}>
                        <p className="mb-0">PIC Name</p>
                        <Form.Item>
                          {getFieldDecorator('pic_name', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required', 'notSpecial'] : []),
                              { pattern: /^.{2,}$/, message: '*Minimal 2 karakter' },
                            ],
                            initialValue: (detailAgent.pic_name || '').toUpperCase(),
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              size="large"
                              maxLength={30}
                              className="uppercase"
                              placeholder="Input PIC Name"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24}>
                        <p className="mb-0">PIC Phone</p>
                        <Form.Item>
                          {getFieldDecorator('pic_phone', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required'] : []),
                              {
                                validator: (rule, value) => {
                                  if (!value) return Promise.resolve()

                                  if (!(/^[0-9+]+$/).test(value)) {
                                    return Promise.reject('*PIC phone harus menggunakan angka')
                                  }

                                  if (value.charAt(0) === 0) {
                                    return Promise.reject('*Cannot input first number with "0"')
                                  }

                                  if (value.length < 8) {
                                    return Promise.reject('*Minimal 8 digit')
                                  }

                                  return Promise.resolve()
                                },
                              },
                            ],
                            initialValue: (detailAgent.pic_phone || ''),
                          })(
                            <Input
                              className="field-lg"
                              maxLength={16}
                              size="large"
                              addonBefore={prefixSelectorCode}
                              placeholder="Input PIC Phone Number"
                              disabled={getFieldValue('customer_type') === 'individu'}
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24}>
                        <p className="mb-0">Business license</p>
                        <Form.Item>
                          {getFieldDecorator('business_license_number', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required'] : []),
                            ],
                            initialValue: (detailAgent.business_license_number || '').toUpperCase(),
                          })(
                            <Input
                              size="large"
                              maxLength={20}
                              className="uppercase"
                              style={{ textTransform: 'uppercase' }}
                              placeholder="Input Business license"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              )}
            </Row>
          </Col>
          <Col xs={24} md={12}>
            <Row gutter={0} className="h-100">
              <Col xs={24} className="mb-4">
                <Card className="h-100">
                  <p className="title-card">Agent ID</p>
                  <Row gutter={24}>
                    <Col xs={24} md={24}>
                      <p className="mb-0">Agent Type</p>
                      <Form.Item>
                        {getFieldDecorator('type', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: detailAgent.type || 'mobile',
                        })(
                          <Radio.Group
                            size="large"
                            className="w-100"
                          >
                            <Radio value="mobile">MOBILE</Radio>
                            <Radio value="non_mobile">NON MOBILE</Radio>
                            <Radio value="travel">TRAVEL AGENT</Radio>
                          </Radio.Group>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Agent Source</p>
                      <Form.Item>
                        {getFieldDecorator('source', {
                          rules: Helper.fieldRules(['required']),
                          initialValue: detailAgent.source || 'individu',
                        })(
                          <Select
                            size="large"
                            placeholder="Select Source"
                          >
                            {(stateSelects.source || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Agent ID</p>
                      <Form.Item>
                        {getFieldDecorator('agent_id', {
                          rules: [
                            { pattern: /^[A-Z0-9]+$/, message: '*Hanya dapat diisi huruf kapital & angka' },
                          ],
                          initialValue: detailAgent.agent_id || '',
                        })(
                          <Input
                            size="large"
                            placeholder="Auto Generate"
                            disabled
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Profile ID</p>
                      <Form.Item>
                        {getFieldDecorator('profile_id', {
                          rules: [
                            { pattern: /^[a-zA-Z0-9]+$/, message: '*Hanya dapat diisi huruf kapital & angka' },
                          ],
                          initialValue: detailAgent.profile_id ? (detailAgent.profile_id).toUpperCase() : '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') {
                              handleProfileId(value)
                              return value
                            }
                            return value
                          },
                        })(
                          <Input
                            size="large"
                            placeholder="Input Profile ID"
                            className="uppercase"
                            maxLength={10}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">ID Syariah</p>
                      <Form.Item>
                        {getFieldDecorator('syariah_id', {
                          rules: [
                            ...Helper.fieldRules(['required'], 'ID Syariah'),
                            { pattern: /^[a-zA-Z0-9]+$/, message: '*Hanya dapat diisi huruf kapital & angka' },
                          ],
                          initialValue: detailAgent.syariah_id || '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value) return value
                            return value
                          },
                        })(
                          <Input
                            size="large"
                            placeholder="Input ID Syariah"
                            className="uppercase"
                            maxLength={10}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Reference ID</p>
                      <Form.Item>
                        {getFieldDecorator('reference_id', {
                          rules: [
                            { pattern: /^[a-zA-Z0-9]+$/, message: '*Hanya dapat diisi huruf kapital & angka' },
                          ],
                          initialValue: detailAgent.reference_id || '',
                          getValueFromEvent: (e) => {
                            const { value } = e.target
                            if (value !== '') return value
                            return value
                          },
                        })(
                          <Input
                            size="large"
                            className="uppercase"
                            placeholder="Input Reference ID"
                            maxLength={10}
                          />,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24} md={12}>
                      <p className="mb-0">Upline Agent ID</p>
                      <Form.Item>
                        {getFieldDecorator('upline_id', {
                          rules: [
                            { pattern: /^[A-Z0-9 -]+$/, message: '*Hanya dapat diisi huruf kapital & angka' },
                          ],
                          initialValue: detailAgent.upline_id || '',
                          getValueFromEvent: (val) => {
                            if (val) return val.toUpperCase()
                            return val
                          },
                        })(
                          <AutoComplete
                            allowClear
                            onSelect={val => setFieldsValue({ upline_id: val.toUpperCase() })}
                            onSearch={handleSearchAgent}
                            options={(stateSelects.agentList || []).map(item => ({ value: `${item.agent_id} - ${item.name}`, label: `${item.agent_id} - ${item.name}` }))}
                          >
                            <Input size="large" placeholder="Search by Name" />
                          </AutoComplete>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
              {((getFieldValue('source') === 'corporate') || (!isEmpty(detailAgent) ? (detailAgent.source === 'corporate') : false)) && (
                <Col xs={24} className="mb-4">
                  <Card className="h-100">
                    <p className="title-card mb-4">Tax Info</p>
                    <Row gutter={24}>
                      <Col xs={24}>
                        <p className="mb-0">Tax Type</p>
                        <Form.Item>
                          {getFieldDecorator('tax_type_id', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required'] : []),
                            ],
                          })(
                            <Select
                              size="large"
                              placeholder="Input Tax Type"
                              loading={stateSelects.taxLoad}
                            >
                              {(stateSelects.taxList || []).map(item => (
                                <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24}>
                        <p className="mb-0">Tax Name</p>
                        <Form.Item>
                          {getFieldDecorator('taxation_name', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required'] : []),
                            ],
                            getValueFromEvent: e => e.target.value,
                          })(
                            <Input
                              size="large"
                              maxLength={30}
                              className="uppercase"
                              placeholder="Input Tax Name"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24}>
                        <p className="mb-0">Tax ID</p>
                        <Form.Item>
                          {getFieldDecorator('taxation_id', {
                            rules: [
                              ...Helper.fieldRules(getFieldValue('source') === 'corporate' ? ['required'] : []),
                              {
                                validator: (rule, value) => {
                                  let npwp = isEmpty(value) ? '' : value
                                  npwp = npwp.split('-').join('')
                                  npwp = npwp.split('.').join('')
                                  npwp = npwp.split(' ').join('')

                                  if (!npwp) return Promise.resolve()
                                  if (/^.{15,30}$/.test(npwp) === false) return Promise.reject('*Minimal 15 Character')

                                  return Promise.resolve()
                                },
                              },
                            ],
                          })(
                            <NumberFormat
                              className="field-lg ant-input"
                              placeholder="Input Tax ID"
                              format="##.###.###.#-###.########"
                            />,
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24}>
                        <p className="mb-0">Status PTKP</p>
                        <Form.Item>
                          {getFieldDecorator('ptkp_status_id')(
                            <Select
                              size="large"
                              placeholder="Input Status PTKP"
                              loading={stateSelects.taxPtkpStatusesLoad}
                            >
                              {(stateSelects.taxPtkpStatusesList || []).map(item => (
                                <Select.Option key={item.id} value={item.name}>{(item.name).toUpperCase()}</Select.Option>
                              ))}
                            </Select>,
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              )}
              <Col xs={24}>
                <Card className="h-100">
                  <p className="title-card">Work Area</p>
                  <Row gutter={24}>
                    <Col xs={24}>
                      <p className="mb-0">Agent Level</p>
                      <Form.Item>
                        {getFieldDecorator('level_id', {
                          rules: Helper.fieldRules(['required'], 'Agent Level'),
                          initialValue: detailAgent.level_id || undefined,
                        })(
                          <Select
                            size="large"
                            placeholder="Select Level"
                            loading={stateSelects.levelLoad}
                          >
                            {(stateSelects.levelList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Branch</p>
                      <Form.Item>
                        {getFieldDecorator('branch_id', {
                          rules: Helper.fieldRules(['required'], 'Branch'),
                          initialValue: detailAgent.branch_id || undefined,
                          getValueFromEvent: (val) => {
                            setFieldsValue({ branch_perwakilan_id: undefined, review_code_id: undefined })
                            loadUlas(`/ulas?branch_id=${val}`, 'ulas')
                            handleLocation(`/branch-perwakilan/${val}`, 'perwakilan')
                            return val
                          },
                        })(
                          <Select
                            size="large"
                            placeholder="Select Branch"
                            loading={stateSelects.branchLoad}
                          >
                            {(stateSelects.branchList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Branch Perwakilan</p>
                      <Form.Item>
                        {getFieldDecorator('branch_perwakilan_id', {
                          initialValue: detailAgent.branch_perwakilan_id || undefined,
                        })(
                          <Select
                            size="large"
                            placeholder="Select Branch Perwakilan"
                            loading={stateSelects.perwakilanLoad}
                            disabled={!getFieldValue('branch_id')}
                          >
                            {(stateSelects.perwakilanList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                    <Col xs={24}>
                      <p className="mb-0">Kode Ulas</p>
                      <Form.Item>
                        {getFieldDecorator('review_code_id', {
                          rules: Helper.fieldRules(['required'], 'Kode Ulas'),
                          initialValue: detailAgent.review_code_id || undefined,
                        })(
                          <Select
                            size="large"
                            placeholder="Select Kode Ulas"
                            loading={stateSelects.ulasLoad}
                            disabled={!getFieldValue('branch_id')}
                          >
                            {(stateUlas.ulasList || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{(item.ulas_code)}</Select.Option>
                            ))}
                          </Select>,
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="my-3">
          <Col span={24} className="d-flex justify-content-end align-items-center mb-3">
            <Button
              type="primary"
              htmlType="submit"
              className={`button-lg mr-3 ${isMobile ? 'w-40' : 'w-15'}`}
              disabled={isFetching}
            >
              {isEdit ? 'Save' : 'Submit'}
              {isFetching && <LoadingOutlined />}
            </Button>
            <Button
              ghost
              type="primary"
              className={`button-lg border-lg ${isMobile ? 'w-40' : 'w-15'}`}
              onClick={() => history.goBack()}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>
    </React.Fragment>
  )
}

FormCustomer.propTypes = {
  form: PropTypes.any,
  match: PropTypes.object,
  onSubmit: PropTypes.func,
  file: PropTypes.string,
  isFetching: PropTypes.bool,
  stateSelects: PropTypes.object,
  stateUlas: PropTypes.object,
  handleLocation: PropTypes.func,
  loadUlas: PropTypes.func,
  handleUpload: PropTypes.func,
  handleSearchAgent: PropTypes.func,
  detailAgent: PropTypes.object,
  handleProfileId: PropTypes.func,
  stateCountry: PropTypes.object,
  stateCountryCode: PropTypes.object,
}

export default FormCustomer
