/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Button,
  Input,
} from 'antd'
import Helper from 'utils/Helper'
import { LoadingOutlined } from '@ant-design/icons'
import { Form } from '@ant-design/compatible'

const VerifyOTP = ({
  toggle, form, onSubmit,
  loadSubmitBtn, data,
}) => {
  const { getFieldDecorator } = form
  const {
    handleResendVerifyOTP,
    isVerifyOtp,
  } = data

  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={24}>
        <Col span={24}>
          <p className="mb-0">OTP</p>
          <Form.Item>
            {getFieldDecorator('otp', {
              rules: [
                ...Helper.fieldRules(['required']),
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    if (/^[a-zA-Z0-9]*$/.test(value) === false) return Promise.reject('*Tidak boleh menggunakan spesial karakter')

                    if (/^[0-9]*$/.test(value) === false) return Promise.reject('*Tidak boleh menggunakan huruf')

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Input
                size="large"
                placeholder="Input OTP"
                maxLength={6}
              />,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            className="mr-2"
            disabled={isVerifyOtp.isLoading}
            onClick={handleResendVerifyOTP}
          >
            {isVerifyOtp.isLoading ? isVerifyOtp.count : 'Resend Verification'}
            {isVerifyOtp.isLoading ? <LoadingOutlined className="ml-2" /> : ''}
          </Button>
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
            disabled={loadSubmitBtn}
          >
            Submit
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

VerifyOTP.propTypes = {
  data: PropTypes.object,
  toggle: PropTypes.func,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  loadSubmitBtn: PropTypes.bool,
}

export default VerifyOTP
