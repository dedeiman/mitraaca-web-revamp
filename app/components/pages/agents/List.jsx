import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Card,
  Divider, Descriptions,
  Pagination, Button,
  Empty, DatePicker, Select,
  Typography, Radio, Modal,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { Loader } from 'components/elements'
import { BRANCH_CODE } from 'constants/ActionTypes'
import { isEmpty } from 'lodash'
import LevelUp from 'containers/pages/agents/LevelUp'
import history from 'utils/history'
import Helper from 'utils/Helper'
import moment from 'moment'

const AgentList = ({
  groupRole, dataAgent,
  handlePage, metaAgent,
  stateAgent, setStateAgent,
  isFetching, updatePerPage,
  loadAgent, stateLevelUp,
  setStateLevelUp, currentUser,
  getPerwakilan,
}) => {
  const isMobile = window.innerWidth < 768
  const isTablet = window.innerWidth < 1100
  const isDesktopMD = window.innerWidth < 1536
  const isBranch = groupRole.code === BRANCH_CODE

  return (
    <React.Fragment>
      <Card className={isMobile ? 'mt-3' : ''}>
        <Row gutter={24}>
          <Col xs={24} md={4}>
            <Form.Item className="mb-2">
              <Select
                className="w-100"
                placeholder="Select Type"
                onChange={val => setStateAgent({ ...stateAgent, searchType: val })}
                loading={stateAgent.typeLoad}
                value={!stateAgent.typeLoad ? stateAgent.searchType : null}
              >
                {(stateAgent.typeList || []).map(item => (
                  <Select.Option key={item.id} value={item.value}>{item.label}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={4}>
            <Form.Item className="mb-2">
              <Input
                allowClear
                className="w-100 uppercase"
                value={stateAgent.search || ''}
                placeholder="Search Agent..."
                onChange={(e) => {
                  const { value } = e.target
                  const { searchType } = stateAgent
                  let key = ''
                  if (searchType === 'phone_number' || searchType === 'no_ktp') {
                    if ((value === '') || (/^[0-9]+$/.test(value))) {
                      key = value
                    }
                  } else if (searchType === 'Name') {
                    if ((value === '') || (/^[a-zA-Z ]+$/.test(value))) {
                      key = value.toUpperCase()
                    }
                  } else {
                    key = value
                  }
                  setStateAgent({ ...stateAgent, search: key })
                }}
                onPressEnter={() => loadAgent(true)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} md={7} xxl={7}>
            <Form.Item className="mb-2">
              <Select
                  allowClear
                  placeholder="Agent Type"
                  className="w-100"
                  value={stateAgent.agentType ? stateAgent.agentType : null}
                  onChange={val => setStateAgent({ ...stateAgent, agentType: val })}
              >
                <Option value="mobile">Mobile</Option>
                <Option value="non_mobile">Non Mobile</Option>
                <Option value="travel">Travel Agent</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={9} xxl={9}>
            <Form.Item className="mb-2" label="Join Date">
              <DatePicker.RangePicker
                format="DD MMM YYYY"
                value={!isEmpty(stateAgent.joinDate) ? [moment(stateAgent.joinDate[0]), moment(stateAgent.joinDate[1])] : ''}
                onChange={(date) => {
                  setStateAgent({
                    ...stateAgent,
                    joinDate: date ? [moment(date[0]).utc().format('YYYY-MM-DD'), moment(date[1]).utc().format('YYYY-MM-DD')] : [],
                  })
                }}
              />
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item className="mb-2">
              <Select
                className="w-100"
                placeholder="Select Branch"
                loading={stateAgent.branchLoad}
                value={!stateAgent.branchLoad ? stateAgent.branch : null}
                onChange={val => getPerwakilan(val)}
              >
                <Select.Option key={null} value={null}>Select All</Select.Option>
                {(stateAgent.branchList || []).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item className="mb-2">
              <Select
                className="w-100"
                placeholder="Select Perwakilan"
                disabled={!stateAgent.branch}
                loading={stateAgent.branchLoad}
                value={!stateAgent.branchLoad ? stateAgent.perwakilan : null}
                onChange={val => setStateAgent({ ...stateAgent, perwakilan: val })}
              >
                <Select.Option key={null} value={null}>Select All</Select.Option>
                {(stateAgent.perwakilanList || []).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item className="mb-2">
              <Select
                className="w-100"
                placeholder="Select Birth Month"
                value={!stateAgent.birthLoad ? stateAgent.birth : 0}
                onChange={val => setStateAgent({ ...stateAgent, birth: val })}
              >
                <Select.Option key={0} value={0}>Select All</Select.Option>
                {(moment.months() || []).map((item, idx) => (
                  <Select.Option key={Math.random()} value={idx + 1}>{item}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={6}>
            <Form.Item className="mb-2">
              <Select
                placeholder="Select Level"
                className="w-100"
                loading={stateAgent.levelLoad}
                value={!stateAgent.levelLoad ? stateAgent.level : null}
                onChange={val => setStateAgent({ ...stateAgent, level: val })}
              >
                <Select.Option key={null} value={null}>Select All</Select.Option>
                {(stateAgent.levelList || []).map(item => (
                  <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={24} align="end">
            <Button type="primary" className="button-lg px-4" onClick={() => loadAgent(true)}>
              Search
            </Button>
          </Col>
        </Row>
      </Card>
      <Row gutter={[24, 24]} className="mt-2">
        <Col xs={24}>
          <Row gutter={[24, 0]} className="d-flex align-items-center justify-content-end">
            {!isTablet ? <Col md={9} /> : ''}
            <Col xs={24} lg={8} xl={5}>
              {(currentUser.permissions && currentUser.permissions.indexOf('agent-tree-level-filter-all') > -1) && (
                <Button
                  ghost
                  type="primary"
                  className={`border-lg w-100 d-flex align-items-center justify-content-center ${isTablet ? 'mb-2' : ''}`}
                  onClick={() => {
                    history.push('/agent-search-menu/tree')
                    window.localStorage.setItem('isTree', true)
                  }}
                >
                  View Agent Tree All
                </Button>
              )}
            </Col>
            <Col xs={24} lg={8} xl={5}>
              {(currentUser.permissions && currentUser.permissions.indexOf('agent-tree-level-filter-all') > -1) && (
                <Button
                  ghost
                  type="primary"
                  className={`border-lg w-100 d-flex align-items-center justify-content-center ${isTablet ? 'mb-2' : ''}`}
                  onClick={() => {
                    history.push(`/agent-search-menu/tree?branch=${!isEmpty(currentUser.branch_perwakilan) ? currentUser.branch_perwakilan[0].branch_id : ''}`)
                    window.localStorage.setItem('isTree', true)
                  }}
                >
                  View Agent Tree by Branch
                </Button>
              )}
            </Col>
            <Col xs={24} lg={8} xl={5}>
              {(currentUser.permissions && currentUser.permissions.indexOf('agent-create-menu') > -1) && (
                <Button
                  ghost
                  type="primary"
                  onClick={() => history.push('/agent-search-menu/add')}
                  className={`border-lg w-100 d-flex align-items-center justify-content-center ${isTablet ? 'mb-2' : ''}`}
                >
                  <img src="/assets/plus.svg" alt="plus" className="mr-1" />
                  Register New Agent
                </Button>
              )}
            </Col>
          </Row>
        </Col>
      </Row>

      <Card>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(dataAgent)) && (
          <Empty
            description="Tidak ada data."
          />
        )}
        {!isEmpty(dataAgent)
          ? (
            dataAgent.map((item, idx) => (
              <React.Fragment key={`list-agent-${item.id}`}>
                <Row gutter={[24, 24]}>
                  <Col xs={24} md={isDesktopMD ? 19 : 14} xxl={isBranch ? 17 : 14}>
                    <Descriptions layout="vertical" colon={false} column={3}>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Agent ID" className="px-1">
                        {item.agent_id || '-'}
                        {/* item.profile_id ? ' - ' : ''}
                        {item.profile_id ? Helper.getValue(item.profile_id) : '' */}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Name" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 3 }} className="mb-0">
                          {item.name || '-'}
                        </Typography.Paragraph>
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Join Date" className="px-1">
                        {item.created_at ? moment.utc(item.created_at).format('DD MMM YYYY') : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Upline Agent ID" className="px-1">
                        {item.upline_id || '-'}
                        {item.upline ? ' - ' : ''}
                        {item.upline ? Helper.getValue(item.upline.name) : ''}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Level" className="px-1">
                        {item.level ? Helper.getValue(item.level.name) : '-'}
                      </Descriptions.Item>
                      <Descriptions.Item span={isMobile ? 6 : 1} label="Birth Date" className="px-1">
                        <Typography.Paragraph ellipsis={{ rows: 1 }} className="mb-0">
                          {item.birthdate ? moment.utc(item.birthdate).format('DD MMM YYYY') : '-'}
                        </Typography.Paragraph>
                      </Descriptions.Item>
                    </Descriptions>
                  </Col>

                  <Col xs={24} md={isDesktopMD ? 5 : 10} xxl={isBranch ? 7 : 10}>
                    <Row gutter={isMobile ? [12, 12] : 12} className="h-100" align="middle">
                      <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={isBranch ? 12 : 8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                        <Button
                          type="primary"
                          onClick={() => history.push(`/agent-search-menu/${item.id}`)}
                          className="d-flex justify-content-between align-items-center w-100"
                        >
                          View Profile
                          <img src="/assets/ic-agent.svg" alt="agent" />
                        </Button>
                      </Col>
                      <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={isBranch ? 12 : 8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                        <Button
                          type="primary"
                          onClick={() => {
                            history.push(`/agent-search-menu/tree?key=${item.agent_id}`)
                            window.localStorage.setItem('isTree', true)
                          }}
                          className="d-flex justify-content-between align-items-center w-100"
                        >
                          View Tree
                          <img src="/assets/chart-line.svg" alt="chart" />
                        </Button>
                      </Col>
                      {(currentUser.permissions && currentUser.permissions.indexOf('agent-level-up') > -1) && (
                        <Col xs={24} md={isDesktopMD ? 24 : 8} xxl={8} className={isDesktopMD ? 'px-0' : 'px-auto'}>
                          <Button
                            type="primary"
                            onClick={() => setStateLevelUp({ data: item, visible: true })}
                            className="d-flex justify-content-between align-items-center w-100"
                          >
                            Level Up
                            <img src="/assets/level-up.svg" alt="lvl-up" />
                          </Button>
                        </Col>
                      )}
                    </Row>
                  </Col>
                </Row>
                {(idx !== (dataAgent.length - 1)) && (
                  <Divider />
                )}
              </React.Fragment>
            ))
          )
          : <div className="py-5" />
          }
      </Card>

      <Pagination
        className="py-3"
        showTotal={() => 'Page'}
        simple={isMobile}
        pageSize={metaAgent.per_page || 10}
        current={stateAgent.page ? Number(stateAgent.page) : 1}
        onChange={handlePage}
        total={metaAgent.total_count}
        onShowSizeChange={updatePerPage}
        showSizeChanger={false}
      />
      <Modal
        title={<p className="mb-0 title-card">Level Up Agent</p>}
        visible={stateLevelUp.visible}
        footer={null}
        closable={false}
        onCancel={() => setStateLevelUp({ ...stateLevelUp, visible: false })}
      >
        <LevelUp
          data={stateLevelUp.data}
          toggle={(isReload) => {
            if (isReload) {
              loadAgent()
            }
            setStateLevelUp({ ...stateLevelUp, visible: false })
          }}
        />
      </Modal>
    </React.Fragment>
  )
}

AgentList.propTypes = {
  groupRole: PropTypes.object,
  dataAgent: PropTypes.array,
  metaAgent: PropTypes.object,
  stateAgent: PropTypes.object,
  handlePage: PropTypes.func,
  stateLevelUp: PropTypes.object,
  setStateLevelUp: PropTypes.func,
  loadAgent: PropTypes.func,
  setStateAgent: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  currentUser: PropTypes.object,
  getPerwakilan: PropTypes.func,
}

export default AgentList
