/* eslint-disable no-return-assign */
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Layout, Menu, Avatar, Button,
} from 'antd'
import history from 'utils/history'
import Helper from 'utils/Helper'

const { Sider } = Layout
const { SubMenu } = Menu

const SideBar = ({
  handleKeys, site, user, logout,
  isCollapsed, toggleCollapsed,
  avatarError, imgAvatar,
}) => {
  const arrMenu = [{
    key: 'agent-search-menu',
    name: 'SEARCH AGENT',
    ref: 'agent-search-menu',
    image: '/assets/ic-search-agent.svg',
    item: 'menu',
  }, {
    key: 'policy-search-menu',
    name: 'SEARCH POLICY',
    ref: 'policy-search-menu',
    image: '/assets/ic-search-policy.svg',
    item: 'menu',
  }, {
    key: 'approval-list-menu',
    name: 'APPROVAL LIST',
    ref: 'approval-list-menu',
    image: '/assets/ic-search-policy.svg',
    item: 'menu',
  }, {
    key: 'customer-menu',
    name: 'CUSTOMER',
    ref: 'customer-menu',
    image: '/assets/ic-costumer.svg',
    item: 'menu',
  }, {
    key: 'payment',
    name: 'PAYMENT',
    ref: 'payment-menu',
    image: '/assets/ic-payment.svg',
    item: 'sub-menu',
    subMenu: [{
      key: 'payment-filter',
      name: 'Keranjang',
      ref: 'payment-filter',
      item: 'menu',
    }, {
      key: 'payment-history',
      name: 'History Payment',
      ref: 'payment-history',
      item: 'menu',
    }],
  }, {
    key: 'profile',
    name: 'PROFILE',
    ref: 'profile-menu',
    image: '/assets/ic-search-agent.svg',
    item: 'sub-menu',
    subMenu: [{
      key: 'profile/profile-index',
      name: 'My Profile',
      ref: 'profile-index',
      item: 'menu',
    }, {
      key: 'profile/profile-level-tree-menu',
      name: 'Jenjang Karir',
      ref: 'profile-level-tree-menu',
      item: 'menu',
    }, {
      key: Helper.loginValidations(user.validations) ? 'profile/change#first' : 'profile/change',
      name: 'Change Password',
      ref: 'profile-password-menu',
      item: 'menu',
    }],
  }, {
    key: 'produksi',
    name: 'PRODUKSI',
    ref: 'production-menu',
    image: '/assets/ic-mask.svg',
    item: 'sub-menu',
    subMenu: [{
      key: 'production-create-io',
      name: 'Create IO',
      ref: 'production-create-io',
      item: 'menu',
    }, {
      key: 'production-create-sppa',
      name: 'Create SPPA',
      ref: 'production-create-sppa',
      item: 'menu',
    }, {
      key: 'production-search-io-menu',
      name: 'Search IO',
      ref: 'production-search-io-menu',
      item: 'menu',
    }, {
      key: 'production-search-sppa-menu',
      name: 'Search SPPA',
      ref: 'production-search-sppa-menu',
      item: 'menu',
    }],
  }, {
    key: 'contest-menu',
    name: 'CONTEST',
    ref: 'contest-menu',
    image: '/assets/ic-contest.svg',
    item: 'menu',
  }, {
    key: 'report',
    name: 'REPORT',
    ref: 'report-menu',
    image: '/assets/ic-report.svg',
    item: 'sub-menu',
    subMenu: [{
      key: 'report-administrator',
      name: 'Administrator',
      ref: 'report-administrator-agent-list',
      item: 'sub-menu',
      subMenu: [{
        key: 'report-administrator-agent-contest-winner',
        name: 'Agent Contest Winner',
        ref: 'report-administrator-agent-contest-winner',
        item: 'menu',
      }, {
        key: 'report-administrator-agent-restricted-product',
        name: 'Agent Restricted Product',
        ref: 'report-administrator-agent-restricted-product',
        item: 'menu',
      }, {
        key: 'report-administrator-agent-list',
        name: 'Agent List',
        ref: 'report-administrator-agent-list',
        item: 'menu',
      }, {
        key: 'report-administrator-approval',
        name: 'Approval',
        ref: 'report-administrator-approval',
        item: 'menu',
      }],
    }, {
      key: 'report-produksi',
      name: 'Produksi',
      item: 'sub-menu',
      ref: 'report-production',
      subMenu: [{
        key: 'report-production-sppa',
        name: 'SPPA',
        ref: 'report-production-sppa',
        item: 'menu',
      }, {
        key: 'report-production-list',
        name: 'Produksi',
        ref: 'report-production-list',
        item: 'menu',
      },{ 
        key: 'report-production-claim-settle',
        name: 'Claim Settle',
        ref: 'report-production-claim-settle',
        item: 'menu',
      }, {
        key: 'report-production-outstanding-claim',
        name: 'Outstanding Claim',
        ref: 'report-production-outstanding-claim',
        item: 'menu',
      }, {
        key: 'report-production-loss-business',
        name: 'Loss Business',
        ref: 'report-production-loss-business',
        item: 'menu',
      }],
    }, {
      key: 'report-finance',
      name: 'Finance',
      item: 'sub-menu',
      ref: 'report-finance-benefit-monthly',
      subMenu: [{
        key: 'report-finance-commission',
        name: 'Komisi Finance',
        ref: 'report-finance-commission',
        item: 'menu',
      }, {
        key: 'report-finance-benefit-monthly',
        name: 'Benefit Finance Monthly',
        ref: 'report-finance-benefit-monthly',
        item: 'menu',
      }, {
        key: 'report-finance-benefit-yearly',
        name: 'Benefit Finance Yearly',
        ref: 'report-finance-benefit-yearly',
        item: 'menu',
      }, {
        key: 'report-finance-career',
        name: 'Jenjang Karir',
        ref: 'report-finance-career',
        item: 'menu',
      }],
    }, {
      key: 'report-agent',
      name: 'Agent',
      item: 'sub-menu',
      ref: 'report-agent',
      subMenu: [{
        key: 'report-agent-commission-detail',
        name: 'Detail komisi',
        ref: 'report-agent-commission-detail',
        item: 'menu',
      }, {
        key: 'report-agent-expired-policy',
        name: 'Expired Policy',
        ref: 'report-agent-expired-policy',
        item: 'menu',
      }],
    }, {
      key: 'report-training',
      name: 'Training',
      item: 'sub-menu',
      ref: 'report-training',
      subMenu: [{
        key: 'report-training-list',
        name: 'Training',
        ref: 'report-training-list',
        item: 'menu',
      }],
    }],
  }, {
    key: 'training-class-menu',
    name: 'TRAINING CLASS',
    ref: 'training-class-menu',
    image: '/assets/ic-training.svg',
    item: 'menu',
  }, {
    key: 'materi-menu',
    name: 'MATERI',
    ref: 'materi-menu',
    image: '/assets/ic-materi.svg',
    item: 'menu',
  }, {
    key: 'faq-menu',
    name: 'FAQ',
    ref: 'faq-menu',
    image: '/assets/ic-faq.svg',
    item: 'menu',
  }, {
    key: 'pending-policy-list',
    name: 'PENDING POLICY',
    ref: 'pending-policy-list',
    image: '/assets/ic-mask.svg',
    item: 'menu',
  }, {
    key: 'profile/change',
    name: 'MY PASSWORD',
    ref: 'password-menu',
    image: '/assets/ic-password.svg',
    item: 'menu',
  }, {
    key: 'setting-menu',
    name: 'Setting Menu',
    ref: 'setting-menu',
    image: '/assets/ic-password.svg',
    item: 'menu',
  }]

  return (
    <Sider
      theme="light"
      collapsible
      breakpoint="sm"
      onCollapse={collapsed => toggleCollapsed(collapsed)}
      className="site-layout-background"
    >
      <Menu
        theme="light"
        mode="inline"
        selectedKeys={[site.activePage]}
        defaultOpenKeys={(window.localStorage.getItem('isPath') || '').split(',')}
        style={{ height: '100%', borderRight: 0 }}
        onClick={(e) => {
          if (e.key === '#') {
            logout()
            window.localStorage.removeItem('isPath')
          } else {
            handleKeys(e)
          }
        }}
        onOpenChange={e => handleKeys(e, true)}
      >
        <div className="py-4 text-center bg-blue">
          <Button type="link" className="p-0" onClick={() => history.push('/dashboard')}>
            <img src="/assets/logo-mitra-aca.png" alt="logo" />
          </Button>
        </div>
        <div className="py-4 text-center">
          <Avatar shape="circle" size={isCollapsed ? 'small' : 120} src={imgAvatar || '/assets/avatar-on-error.jpg'} onError={avatarError} />
          <div className="pt-3">
            <p className="profile-name text-truncate">{user.name}</p>
            <p className="profile-role text-truncate" style={{ textTransform: 'capitalize' }}>{user.role && (user.role.display_name).split('-').join(' ')}</p>
          </div>
        </div>
        {arrMenu.map((item, index) => (
          item.item === 'menu'
            ? (
              (user.permissions && user.permissions.indexOf(item.ref) > -1) && (
                <Menu.Item key={item.key} className="zoom-icon" icon={<img src={item.image} alt="logo" className="icon-menu" />}>
                  <Link
                    to={{
                      pathname: `/${item.key}`,
                      state: index,
                    }}
                    type="link"
                    className={`px-0 btn-signout restore-${index}`}
                  >
                    <span className="nav-text">{item.name}</span>
                  </Link>
                </Menu.Item>
              )
            )
            : (
              (user.permissions && user.permissions.indexOf(item.ref) > -1) && (
                <SubMenu
                  key={item.key}
                  className="zoom-icon"
                  title={(
                    <span>
                      <img src={item.image} alt="logo" className="icon-menu" />
                      <span className="nav-text">{item.name}</span>
                    </span>
                  )}
                >
                  {Helper.renderChildMenu(item.subMenu, user.permissions)}
                </SubMenu>
              )
            )
        ))}
        <Menu.Item key="setting-menu" className="zoom-icon" icon={<img src="/assets/ic-password.svg" alt="logo" className="icon-menu" />}>
          <Link
            to={{
              pathname: '/setting-menu',
            }}
            type="link"
            className="px-0 btn-signout"
          >
            <span className="nav-text">SETTING MENU</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="#" className="zoom-icon" icon={<img src="/assets/ic-signout.svg" alt="logo" className="icon-menu" />}>
          <Button type="link" className="px-0 btn-signout">
            <span className="nav-text">SIGN OUT</span>
          </Button>
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

SideBar.propTypes = {
  handleKeys: PropTypes.func,
  site: PropTypes.object,
  user: PropTypes.object,
  logout: PropTypes.func,
  isCollapsed: PropTypes.bool,
  toggleCollapsed: PropTypes.func,
  imgAvatar: PropTypes.string,
  avatarError: PropTypes.func,
}

export default SideBar
