import Sidebar from './organism/Sidebar'
import Forbidden from './organism/Forbidden'
import Loader from './Loader'

export {
  Sidebar,
  Loader,
  Forbidden,
}
