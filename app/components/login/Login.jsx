import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Form } from '@ant-design/compatible'
import {
  Input, Divider, Alert, Tooltip, Button,
} from 'antd'
import { LoadingOutlined, InfoCircleOutlined } from '@ant-design/icons'
import { Col } from 'react-bootstrap'
import Helper from 'utils/Helper'
import '@ant-design/compatible/assets/index.css'

const Login = ({
  onChange,
  onSubmit,
  form,
  errorMessage,
  isAuthenticating,
  onCloseError,
}) => {
  const { getFieldDecorator } = form
  return (
    <div className="form-container row m-0">
      <Col md={7} className="d-none d-md-block" />
      <Col>
        <Form className="form-signin" onSubmit={onSubmit} hideRequiredMark layout="vertical">
          <h3>Silakan masuk ke akun Mitraca Anda</h3>
          <Form.Item
            label={(
              <div className="d-flex justify-content-between align-items-center">
                <span className="mr-3">Email / Phone Number / NIK</span>
                <Tooltip
                  title={(
                    <ol>
                      <li>Agent, Login Menggunakan Email atau Phone Number</li>
                      <li>Agency / PO / Branch, Login Menggunakan NIK</li>
                    </ol>
                  )}
                >
                  <InfoCircleOutlined />
                </Tooltip>
              </div>
            )}
          >
            {getFieldDecorator('email', {
              rules: Helper.fieldRules(['required'], 'Email'),
            })(
              <Input onChange={onChange} className="input-auth" name="email" placeholder="Email / Phone Number / NIK" />,
            )}
          </Form.Item>
          <Form.Item label="Password">
            {getFieldDecorator('password', {
              rules: Helper.fieldRules(['required'], 'Password'),
            })(
              <Input.Password
                name="password"
                className="input-auth"
                onChange={onChange}
                placeholder="Password"
                iconRender={visible => (
                  visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                )}
              />,
            )}
          </Form.Item>

          {errorMessage && (
            <Alert
              message=""
              description={errorMessage}
              type="error"
              closable
              onClose={onCloseError}
              className="mb-3 text-left"
            />
          )}

          <div className="d-flex align-items-center justify-content-between">
            <Link to="/password/forgot" style={{ width: '50%', textAlign: 'left' }}>Forgot Password</Link>
            <Button block type="primary" style={{ width: '40%', height: 40, fontWeight: 'bold' }} disabled={isAuthenticating} htmlType="submit">
              {isAuthenticating && <LoadingOutlined className="mr-2" />}
              {'Login'}
            </Button>
          </div>

          <div className="pb-3" />

          <Divider />
          <div className="text-left">
            Butuh bantuan?
            <a href="tel:+39708999" className="ml-1">(021) 39708999</a>
          </div>
        </Form>
      </Col>
    </div>
  )
}

Login.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCloseError: PropTypes.func,
  form: PropTypes.any,
  errorMessage: PropTypes.string,
  isAuthenticating: PropTypes.bool,
}

export default Login
