import React from 'react'
import PropTypes from 'prop-types'
import {
  Input, Divider,
  Row, Col, Button,
  Card, DatePicker,
} from 'antd'
import {
  Form,
} from '@ant-design/compatible'
import { LoadingOutlined } from '@ant-design/icons'
import ReCAPTCHA from 'react-google-recaptcha'
import Helper from 'utils/Helper'
import config from 'app/config'
import moment from 'moment'
import '@ant-design/compatible/assets/index.css'

const Login = ({
  form, stateLoad,
  onSubmit, getOTP,
  currentState, resendOTP,
}) => {
  const { getFieldDecorator } = form
  return (
    <Row justify="center" className="vh-100 m-0">
      <Col span={16} className="pt-5">
        <Card className="my-5">
          <Form onSubmit={onSubmit} hideRequiredMark layout="vertical">
            <p className="title-card mb-4" style={{ fontSize: '24px' }}>Agent Verification</p>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <p className="mb-0" style={{ fontSize: '17px', color: 'rgba(0, 0, 0, 0.85)' }}>Mohon lakukan verifikasi data terlebih dahulu...</p>
              </Col>
              <Col span={12}>
                <Form.Item label="Agent ID">
                  {getFieldDecorator('agent_id', {
                    rules: Helper.fieldRules(['required']),
                    initialValue: currentState.agent_id || '',
                  })(
                    <Input
                      placeholder="Agent ID"
                      size="large"
                      value={currentState.agent_id}
                      disabled
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Secret Key">
                  {getFieldDecorator('secret_key', {
                    rules: Helper.fieldRules(['required']),
                    initialValue: currentState.secret_key || '',
                  })(
                    <Input
                      placeholder="Secret Key"
                      size="large"
                      value={currentState.secret_key}
                      disabled
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Birth Date">
                  {getFieldDecorator('birth_date', {
                    rules: Helper.fieldRules(['required']),
                    initialValue: currentState.birth_date ? moment(currentState.birth_date) : '',
                  })(
                    <DatePicker
                      placeholder="Birth Date"
                      className="w-100"
                      defaultValue={currentState.birth_date ? moment(currentState.birth_date) : ''}
                      size="large"
                      format="DD MMMM YYYY"
                      disabled
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Phone Number">
                  {getFieldDecorator('phone_number', {
                    rules: [
                      ...Helper.fieldRules(['required', 'number'], 'Phone Number'),
                    ],
                    initialValue: currentState.phone_number || '',
                  })(
                    <Input
                      placeholder="Phone Number"
                      size="large"
                      value={currentState.phone_number}
                      disabled
                    />,
                  )}
                </Form.Item>
              </Col>
              {
                ((window.localStorage.getItem('isOTP') !== 'false') && !stateLoad.otp) && (
                  <Col span={12}>
                    <Form.Item>
                      {getFieldDecorator('recaptcha', {
                        rules: Helper.fieldRules(['required'], 'Recaptcha'),
                      })(
                        <ReCAPTCHA sitekey={config.recaptcha} />,
                      )}
                    </Form.Item>
                  </Col>
                )
              }
              <Col span={24}>
                <Button size="large" type="primary" disabled={(stateLoad.loading || stateLoad.otp) || (window.localStorage.getItem('isOTP') === 'false')} onClick={getOTP}>
                  {stateLoad.loading && <LoadingOutlined className="mr-2" />}
                  Verify & Send OTP
                </Button>
                <Button size="large" type="primary" disabled={(stateLoad.loadingResend || !stateLoad.otp) && (window.localStorage.getItem('isOTP') !== 'false')} className="ml-3" onClick={() => resendOTP(currentState.agent_id, currentState.phone_number)}>
                  {stateLoad.loadingResend && <LoadingOutlined className="mr-2" />}
                  Resend OTP
                </Button>
              </Col>

              <Divider />

              <Col span={12}>
                <Form.Item label="OTP">
                  {getFieldDecorator('otp', {
                    rules: Helper.fieldRules(stateLoad.otp ? ['required'] : [], 'OTP'),
                  })(
                    <Input
                      placeholder="Input OTP"
                      size="large"
                      disabled={!stateLoad.otp && (window.localStorage.getItem('isOTP') !== 'false')}
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Button size="large" type="primary" disabled={stateLoad.authenticating || !stateLoad.otp} htmlType="submit">
                  {stateLoad.authenticating && <LoadingOutlined className="mr-2" />}
                  Submit
                </Button>
              </Col>

            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  )
}

Login.propTypes = {
  onSubmit: PropTypes.func,
  form: PropTypes.any,
  stateLoad: PropTypes.object,
  getOTP: PropTypes.func,
  resendOTP: PropTypes.func,
  currentState: PropTypes.object,
}

export default Login
