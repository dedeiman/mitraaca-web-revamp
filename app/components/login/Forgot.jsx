/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Input,
  Divider, Alert,
  Result, Select,
  Tooltip, Button,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { LoadingOutlined, InfoCircleOutlined } from '@ant-design/icons'
import { Col, Row } from 'react-bootstrap'
import qs from 'query-string'
import Helper from 'utils/Helper'
import history from 'utils/history'
import '@ant-design/compatible/assets/index.css'
import { isEmpty } from 'lodash'

const Login = ({
  onChange, onSubmit, form,
  errorMessage, isAuthenticating,
  onCloseError, isPassword, tokenExpired,
  location, handleResendOTP, isVerifyOtp,
}) => {
  const { getFieldDecorator, getFieldValue } = form
  if (tokenExpired) {
    return (
      <Result
        status="403"
        className="text-capitalize"
        title={tokenExpired}
        subTitle="Sorry, your token has been expired."
        extra={(
          <Button type="primary" onClick={() => history.push('/')}>
            Back To Login
          </Button>
        )}
      />
    )
  }

  return (
    <div className="form-container forgot row m-0">
      <Col md={7} className="d-none d-md-block" />
      <Col>
        <Form className="form-signin" onSubmit={onSubmit} hideRequiredMark layout="vertical">
          {(() => {
            switch (isPassword) {
              case 'forgot':
              case 'reset':
                return (
                  <h3>
                    {`Lupa ${qs.parse(location.search).is_secondary ? 'second' : ''} password Anda`}
                    <br />
                    {' '}
                    kami akan bantu
                  </h3>
                )
              default:
                return <h3>Masukkan Kode OTP</h3>
            }
          })()}

          {(() => {
            switch (isPassword) {
              case 'forgot':
                return (
                  <>
                    <Form.Item
                      label={(
                        <div className="d-flex justify-content-between align-items-center">
                          <span className="mr-3">Select Type</span>
                          <Tooltip
                            title={(
                              <ol>
                                <li>Agent, Lupa password Menggunakan Email atau Phone Number</li>
                                <li>Agency / PO / Branch, Lupa password Menggunakan NIK</li>
                              </ol>
                            )}
                          >
                            <InfoCircleOutlined />
                          </Tooltip>
                        </div>
                      )}
                    >
                      {getFieldDecorator('key', {
                        rules: Helper.fieldRules(['required'], 'Select Type'),
                      })(
                        <Select
                          showSearch
                          size="large"
                          placeholder="Select Type"
                          className="select-auth"
                        >
                          {(['email', 'phone_number', 'nik']).map(item => (
                            <Select.Option
                              key={item}
                              value={item}
                            >
                              {(item.split('_').join(' ')).toUpperCase()}
                            </Select.Option>
                          ))}
                        </Select>,
                      )}
                    </Form.Item>
                    <Form.Item
                      style={{ textTransform: 'capitalize' }}
                      label={getFieldValue('key') !== 'nik' ? (getFieldValue('key') || '').split('_').join(' ') : (getFieldValue('key') || '').toUpperCase()}
                    >
                      {getFieldDecorator('identifier', {
                        rules: Helper.fieldRules(['required'], 'Email / Phone Number / NIK'),
                      })(
                        <Input
                          name="identifier"
                          className={`input-auth ${getFieldValue('key') !== 'nik' ? 'capitalize' : ''}`}
                          onChange={onChange}
                          placeholder={
                            (() => {
                              if (!isEmpty(getFieldValue('key'))) {
                                if (getFieldValue('key') === 'nik') {
                                  return `Masukan ${(getFieldValue('key') || '').toUpperCase()}`
                                }
                                return `Masukan ${(getFieldValue('key') || '').split('_').join(' ')}`
                              }
                              return 'Masukan Email / Phone Number / NIK'
                            })()
                          }
                        />,
                      )}
                    </Form.Item>
                  </>
                )
              case 'reset':
                return (
                  <React.Fragment>
                    <Form.Item label="Password Baru Anda">
                      {getFieldDecorator((!qs.parse(location.search).is_secondary ? 'password' : 'alternative_password'), {
                        rules: [
                          ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Password'),
                          { pattern: /^.{8,}$/, message: '*Minimal 8 karakter\n' },
                          { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter\n' },
                          {
                            validator: (rule, value) => {
                              if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')

                              return Promise.resolve()
                            },
                          },
                        ],
                      })(
                        <Input.Password
                          onChange={onChange}
                          className="input-auth"
                          placeholder="Password"
                          iconRender={visible => (
                            visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                          )}
                        />,
                      )}
                    </Form.Item>
                    <Form.Item label="Konfirmasi Password">
                      {getFieldDecorator((!qs.parse(location.search).is_secondary ? 'password_confirmation' : 'alternative_password_confirmation'), {
                        rules: [
                          ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Confirmation Password'),
                          { pattern: /^.{8,}$/, message: '*Minimal 8 karakter\n' },
                          { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter\n' },
                          {
                            validator: (rule, value) => {
                              if (value !== getFieldValue((!qs.parse(location.search).is_secondary ? 'password' : 'alternative_password'))) return Promise.reject('*Password yang Anda masukkan tidak sama\n')
                              if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')

                              return Promise.resolve()
                            },
                          },
                        ],
                      })(
                        <Input.Password
                          onChange={onChange}
                          className="input-auth"
                          placeholder="Konfirmasi Password"
                          iconRender={visible => (
                            visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                          )}
                        />,
                      )}
                    </Form.Item>
                  </React.Fragment>
                )
              default:
                return (
                  <React.Fragment>
                    <Form.Item label="Masukkan Kode OTP">
                      {getFieldDecorator('otp_code', {
                        rules: Helper.fieldRules(['required'], 'otp_code'),
                      })(
                        <Input maxLength={6} className="input-auth" name="otp_code" placeholder="Masukkan Kode OTP" />,
                      )}
                    </Form.Item>
                  </React.Fragment>
                )
            }
          })()}

          {errorMessage && (
            <Alert
              message=""
              description={errorMessage}
              type="error"
              closable
              onClose={onCloseError}
              className="mb-3 text-left"
            />
          )}

          <Row>
            <Col>
              <div className="d-flex align-items-center justify-content-between mt-3">
                <Button block size="lg" ghost type="primary" disabled={isAuthenticating} onClick={() => history.goBack()} style={{ height: 40, fontWeight: 'bold' }}>
                  Cancel
                </Button>
              </div>
            </Col>
            <Col>
              <div className="d-flex align-items-center justify-content-between mt-3">
                <Button block size="lg" type="primary" disabled={isAuthenticating} htmlType="submit" style={{ height: 40, fontWeight: 'bold' }}>
                  {isAuthenticating && <LoadingOutlined className="mr-2" />}
                  {(() => {
                    switch (isPassword) {
                      case 'forgot':
                        return 'Kirim Link'
                      default:
                        return 'Submit'
                    }
                  })()}
                </Button>
              </div>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col xl="8">
              <div className="text-left">
                Butuh bantuan?
                <a href="tel:+39708999" className="ml-1">(021) 39708999</a>
              </div>
            </Col>
            {(() => {
              switch (isPassword) {
                case 'otp':
                  return (
                    <Col xl="4">
                      <div className="text-right">
                        <Button
                          size="sm"
                          disabled={isVerifyOtp.isLoading}
                          onClick={handleResendOTP}
                          style={isVerifyOtp.isLoading ? { cursor: 'not-allowed' } : { cursor: 'pointer' }}
                        >
                          {isVerifyOtp.isLoading ? `${isVerifyOtp.time.m < 10 ? `0${isVerifyOtp.time.m}` : isVerifyOtp.time.m} : ${isVerifyOtp.time.s < 10 ? `0${isVerifyOtp.time.s}` : isVerifyOtp.time.s}` : 'Resend OTP'}
                          {isVerifyOtp.isLoading ? <LoadingOutlined className="ml-2" /> : ''}
                        </Button>
                      </div>
                    </Col>
                  )
                default:
                  return ''
              }
            })()}
          </Row>
        </Form>
      </Col>
    </div>
  )
}

Login.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCloseError: PropTypes.func,
  form: PropTypes.any,
  errorMessage: PropTypes.string,
  tokenExpired: PropTypes.string,
  isAuthenticating: PropTypes.bool,
  isPassword: PropTypes.bool,
  location: PropTypes.object,
  isVerifyOtp: PropTypes.object,
  handleResendOTP: PropTypes.func,
}

export default Login
