import API from 'utils/API'
import {
  USER_REQUEST,
  USER_SUCCESS,
  USER_FAILURE,
  USER_UPDATED,
  USER_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import { message } from 'antd'

export const userRequest = () => ({
  type: USER_REQUEST,
})

export const userSuccess = (data, meta) => ({
  type: USER_SUCCESS,
  data,
  meta,
})

export const userFailure = (errorMessage, errorObject) => ({
  type: USER_FAILURE,
  errorMessage,
  errorObject,
})

export const userUpdate = data => ({
  type: USER_UPDATED,
  data,
})

export const userDetail = data => ({
  type: USER_DETAIL_SUCCESS,
  data,
})

export const fetchUser = params => (
  (dispatch) => {
    dispatch(userRequest())
    const url = `/users${params || ''}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(userSuccess(data, meta))
        } else {
          dispatch(userFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(userFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailUser = () => (
  (dispatch) => {
    dispatch(userRequest())
    const url = '/user/profile'
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(userDetail(data))
        } else {
          dispatch(userFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(userFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteUser = id => (
  (dispatch) => {
    dispatch(userRequest())

    return API.delete(`/users/${id}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(userUpdate(data))
        } else {
          dispatch(userFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(userFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const createUser = payload => (
  (dispatch) => {
    dispatch(userRequest())

    return new Promise(resolve => (
      API.post('/users', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(userFailure(meta.message, meta.errors))
            message.error(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(userFailure(err.message, err.errors))
        message.error(err.message)
      })
    ))
  }
)

export const updateUser = (payload, id) => (
  (dispatch) => {
    dispatch(userRequest())

    return new Promise(resolve => (
      API.put(`/users/${id}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(userFailure(meta.message, meta.errors))
            message.error(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(userFailure(err.message, err.errors))
        message.error(err.message)
      })
    ))
  }
)

export const getDetailUser = params => (
  () => {
    const url = `/user/profile/${params}`
    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(response.data)
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err.message) // eslint-disable-line no-console
      })
    ))
  }
)