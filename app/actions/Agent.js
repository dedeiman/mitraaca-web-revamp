import API from 'utils/API'
import {
  AGENT_REQUEST,
  AGENT_SUCCESS,
  AGENT_FAILURE,
  AGENT_UPDATED,
  AGENT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'
// import { message } from 'antd'

export const agentRequest = () => ({
  type: AGENT_REQUEST,
})

export const agentSuccess = (data, meta) => ({
  type: AGENT_SUCCESS,
  data,
  meta,
})

export const agentFailure = (errorMessage, errorObject) => ({
  type: AGENT_FAILURE,
  errorMessage,
  errorObject,
})

export const agentUpdate = data => ({
  type: AGENT_UPDATED,
  data,
})

export const agentDetail = data => ({
  type: AGENT_DETAIL_SUCCESS,
  data,
})

export const fetchAgent = params => (
  (dispatch) => {
    dispatch(agentRequest())
    Helper.sessionTimeout()
    const url = `agents${params || ''}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(agentSuccess(data, meta))
        } else {
          dispatch(agentFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(agentFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailAgent = id => (
  (dispatch) => {
    dispatch(agentRequest())
    Helper.sessionTimeout()
    const url = `agents/${id}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(agentDetail(data))
        } else {
          dispatch(agentFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(agentFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteAgent = id => (
  (dispatch) => {
    dispatch(agentRequest())

    return new Promise((resolve, reject) => (
      API.delete(`agents/${id}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(agentUpdate(data))
            resolve(data)
          } else {
            dispatch(agentFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(agentFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createAgent = payload => (
  (dispatch) => {
    dispatch(agentRequest())

    return new Promise((resolve, reject) => (
      API.post('agents', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(agentFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.errors)
          }
        },
      ).catch((err) => {
        dispatch(agentFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.errors)
      })
    ))
  }
)

export const updateAgent = (payload, id, isDetail) => (
  (dispatch) => {
    dispatch(agentRequest())

    return new Promise((resolve, reject) => (
      API.put(`agents/${id}${isDetail ? '/profiles' : ''}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(agentFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.errors)
          }
        },
      ).catch((err) => {
        dispatch(agentFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.errors)
      })
    ))
  }
)
