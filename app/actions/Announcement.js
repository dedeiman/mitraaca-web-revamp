import API from 'utils/API'
import config from 'app/config'
import {
  ANNOUNCEMENT_REQUEST,
  ANNOUNCEMENT_SUCCESS,
  ANNOUNCEMENT_FAILURE,
  ANNOUNCEMENT_UPDATED,
  ANNOUNCEMENT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'
// import { message } from 'antd'
const xApiKey = { headers: { 'x-api-key': config.api_key } }

export const announcementRequest = () => ({
  type: ANNOUNCEMENT_REQUEST,
})

export const announcementSuccess = (data, meta) => ({
  type: ANNOUNCEMENT_SUCCESS,
  data,
  meta,
})

export const announcementFailure = (errorMessage, errorObject) => ({
  type: ANNOUNCEMENT_FAILURE,
  errorMessage,
  errorObject,
})

export const announcementUpdate = data => ({
  type: ANNOUNCEMENT_UPDATED,
  data,
})

export const announcementDetail = data => ({
  type: ANNOUNCEMENT_DETAIL_SUCCESS,
  data,
})

export const fetchAnnouncement = params => (
  (dispatch) => {
    dispatch(announcementRequest())
    const url = `${config.api_url}/announcements/list${params || ''}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(announcementSuccess(data, meta))
        } else {
          dispatch(announcementFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(announcementFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailAnnouncement = id => (
  (dispatch) => {
    dispatch(announcementRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url}/announcements/list/${id}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(announcementDetail(data))
        } else {
          dispatch(announcementFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(announcementFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteAnnouncement = id => (
  (dispatch) => {
    dispatch(announcementRequest())
    const payload = {
      announcement_ids: [id],
    }
    return new Promise((resolve, reject) => (
      API.delete(`${config.api_url}/announcements`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(announcementSuccess(data, meta))
            resolve(data)
          } else {
            dispatch(announcementFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(announcementFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createAnnouncement = payload => (
  (dispatch) => {
    dispatch(announcementRequest())

    return new Promise((resolve, reject) => (
      API.post(`${config.api_url}/announcements`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(announcementFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(announcementFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)
