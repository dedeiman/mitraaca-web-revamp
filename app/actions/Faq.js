import API from 'utils/API'
import config from 'app/config'
import {
  FAQ_REQUEST,
  FAQ_SUCCESS,
  FAQ_FAILURE,
  FAQ_UPDATED,
  FAQ_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'
// import { message } from 'antd'
const xApiKey = { headers: { 'x-api-key': config.api_key } }

export const faqRequest = () => ({
  type: FAQ_REQUEST,
})

export const faqSuccess = (data, meta) => ({
  type: FAQ_SUCCESS,
  data,
  meta,
})

export const faqFailure = (errorMessage, errorObject) => ({
  type: FAQ_FAILURE,
  errorMessage,
  errorObject,
})

export const faqUpdate = data => ({
  type: FAQ_UPDATED,
  data,
})

export const faqDetail = data => ({
  type: FAQ_DETAIL_SUCCESS,
  data,
})

export const fetchFaq = params => (
  (dispatch) => {
    Helper.sessionTimeout()
    dispatch(faqRequest())
    const url = `${config.api_url_content}/faqs${params || ''}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(faqSuccess(data, meta))
        } else {
          dispatch(faqFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(faqFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailFaq = id => (
  (dispatch) => {
    dispatch(faqRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url_content}/faqs/${id}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(faqDetail(data))
        } else {
          dispatch(faqFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(faqFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteFaq = id => (
  (dispatch) => {
    dispatch(faqRequest())

    return new Promise((resolve, reject) => (
      API.delete(`${config.api_url_content}/faqs/${id}`, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(faqUpdate(data))
            resolve(data)
          } else {
            dispatch(faqFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(faqFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createFaq = payload => (
  (dispatch) => {
    dispatch(faqRequest())

    return new Promise((resolve, reject) => (
      API.post(`${config.api_url_content}/faqs`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(faqFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(faqFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const updateFaq = (payload, id) => (
  (dispatch) => {
    dispatch(faqRequest())

    return new Promise((resolve, reject) => (
      API.put(`${config.api_url_content}/faqs/${id}`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(faqFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(faqFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)
