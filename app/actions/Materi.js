import API from 'utils/API'
import config from 'app/config'
import {
  MATERI_REQUEST,
  MATERI_SUCCESS,
  MATERI_FAILURE,
  MATERI_UPDATED,
  MATERI_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'

const xApiKey = { headers: { 'x-api-key': config.api_key } }

export const materiRequest = () => ({
  type: MATERI_REQUEST,
})

export const materiSuccess = (data, meta) => ({
  type: MATERI_SUCCESS,
  data,
  meta,
})

export const materiFailure = (errorMessage, errorObject) => ({
  type: MATERI_FAILURE,
  errorMessage,
  errorObject,
})

export const materiUpdate = data => ({
  type: MATERI_UPDATED,
  data,
})

export const materiDetail = data => ({
  type: MATERI_DETAIL_SUCCESS,
  data,
})

export const fetchMateri = params => (
  (dispatch) => {
    dispatch(materiRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url_content}/materials${params || ''}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(materiSuccess(data, meta))
        } else {
          dispatch(materiFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(materiFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailMateri = id => (
  (dispatch) => {
    dispatch(materiRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url_content}/materials/${id}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(materiDetail(data))
        } else {
          dispatch(materiFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(materiFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteMateri = id => (
  (dispatch) => {
    dispatch(materiRequest())

    return new Promise((resolve, reject) => (
      API.delete(`${config.api_url_content}/materials/${id}`, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(materiUpdate(data))
            resolve(data)
          } else {
            dispatch(materiFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(materiFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createMateri = payload => (
  (dispatch) => {
    dispatch(materiRequest())

    return new Promise((resolve, reject) => (
      API.post(`${config.api_url_content}/materials`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(materiFailure(meta.message, meta.errors))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(materiFailure(err.message, err.errors))
        reject(err.errors)
      })
    ))
  }
)

export const updateMateri = (payload, id) => (
  (dispatch) => {
    dispatch(materiRequest())

    return new Promise((resolve, reject) => (
      API.put(`${config.api_url_content}/materials/${id}`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(materiFailure(meta.message, meta.errors))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(materiFailure(err.message, err))
        reject(err)
      })
    ))
  }
)
