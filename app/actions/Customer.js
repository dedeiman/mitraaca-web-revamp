import API from 'utils/API'
import {
  CUSTOMER_REQUEST,
  CUSTOMER_SUCCESS,
  CUSTOMER_FAILURE,
  CUSTOMER_UPDATED,
  CUSTOMER_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'
// import { message } from 'antd'

export const customerRequest = () => ({
  type: CUSTOMER_REQUEST,
})

export const customerSuccess = (data, meta) => ({
  type: CUSTOMER_SUCCESS,
  data,
  meta,
})

export const customerFailure = (errorMessage, errorObject) => ({
  type: CUSTOMER_FAILURE,
  errorMessage,
  errorObject,
})

export const customerUpdate = data => ({
  type: CUSTOMER_UPDATED,
  data,
})

export const customerDetail = data => ({
  type: CUSTOMER_DETAIL_SUCCESS,
  data,
})

export const fetchCustomer = params => (
  (dispatch) => {
    dispatch(customerRequest())
    const url = `customers${params || ''}`
    return new Promise((resolve, reject) => {
      Helper.sessionTimeout()
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(customerSuccess(data, meta))
          } else {
            dispatch(customerFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(customerFailure(err.message)) // eslint-disable-line no-console
      })
    })
  }
)

export const fetchDetailCustomer = id => (
  (dispatch) => {
    dispatch(customerRequest())
    Helper.sessionTimeout()
    const url = `customers/${id}`

    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(customerDetail(data))
          } else {
            dispatch(customerFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(customerFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)

export const deleteCustomer = id => (
  (dispatch) => {
    dispatch(customerRequest())

    return new Promise((resolve, reject) => (
      API.delete(`customers/${id}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(customerUpdate(data))
            resolve(data)
          } else {
            dispatch(customerFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(customerFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createCustomer = payload => (
  (dispatch) => {
    dispatch(customerRequest())

    return new Promise((resolve, reject) => (
      API.post('customers', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(customerFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(customerFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const updateCustomer = (payload, id) => (
  (dispatch) => {
    dispatch(customerRequest())

    return new Promise((resolve, reject) => (
      API.put(`customers/${id}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(customerFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(customerFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)
