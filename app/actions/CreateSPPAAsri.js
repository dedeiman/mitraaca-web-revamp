import API from 'utils/API'
import {
  CREATE_SPPA_ASRI_REQUEST,
  CREATE_SPPA_ASRI_SUCCESS,
  CREATE_SPPA_ASRI_FAILURE,
} from 'constants/ActionTypes'

export const createSPPAAsriRequest = () => ({
  type: CREATE_SPPA_ASRI_REQUEST,
})

export const createSPPAAsriSuccess = createIO => ({
  type: CREATE_SPPA_ASRI_SUCCESS,
  createIO,
})

export const createSPPAAsriFailure = errorMessage => ({
  type: CREATE_SPPA_ASRI_FAILURE,
  errorMessage,
})

export const createSPPA = (formData, params) => (dispatch) => {
  dispatch(createSPPAAsriRequest())

  return new Promise((resolve, reject) => {
    const url = `insurance-letters${params || ''}`
    const payload = formData

    return API.post(url, payload)
      .then((response) => {
        const { meta } = response.data

        if (!meta.status) {
          dispatch(createSPPAAsriFailure(meta.message, meta.errors))

          reject(response)
          return
        }

        resolve(response)
      })
      .catch((err) => {
        dispatch(createSPPAAsriFailure(err.message, err.errors))
        reject(err)
      })
  })
}

// eslint-disable-next-line arrow-body-style
export const updateSPPA = (id, formData) => (dispatch) => {
  return new Promise((resolve, reject) => {
    const url = `insurance-letters/${id}`
    const payload = formData

    return API.put(url, payload)
      .then((response) => {
        const { meta } = response.data

        if (!meta.status) {
          dispatch(createSPPAAsriFailure(meta.message, meta.errors))

          reject(response)
          return
        }

        resolve(response)
      })
      .catch((err) => {
        dispatch(createSPPAAsriFailure(err.message, err.errors))
        reject(err)
      })
  })
}
