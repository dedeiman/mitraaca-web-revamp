import API from 'utils/API'
import {
  CREATE_IO_CARGO_REQUEST,
  CREATE_IO_CARGO_SUCCESS,
  CREATE_IO_CARGO_FAILURE,
} from 'constants/ActionTypes'

export const createIOCargoRequest = () => ({
  type: CREATE_IO_CARGO_REQUEST,
})

export const createIOCargoSuccess = createIO => ({
  type: CREATE_IO_CARGO_SUCCESS,
  createIO,
})

export const createIOCargoFailure = errorMessage => ({
  type: CREATE_IO_CARGO_FAILURE,
  errorMessage,
})

export const createIO = (formData, params) => (dispatch) => {
  dispatch(createIOCargoRequest())

  return new Promise((resolve, reject) => {
    const url = `io${params || ''}`
    const payload = {
      // eslint-disable-next-line radix
      product_id: parseInt(formData.product_id),
      cargo: {
        name: formData.name,
        phone_number: formData.phone_number,
        email: formData.email,
        print_policy_book: formData.print_policy_book === true,
        vessel_information: {
          cargo_type_id: formData.cargo_type_id,
          vehicle_type_id: formData.vehicle_type_id,
        },
        cargo_information: {
          currency_id: formData.currency_id,
          sum_insured: parseFloat(formData.sum_insured),
          main_coverage: "ICC 'A'",
          other_information: formData.other_information,
        },
        cargo_premi_calculation: {
          rate: parseFloat(formData.rate),
          premi: parseFloat(formData.premi),
          discount_percentage: parseFloat(formData.discount_percentage),
          discount_amount: parseFloat(formData.discount_amount),
        },
      },
    }

    return API.post(url, payload)
      .then((response) => {
        const { meta } = response.data

        if (!meta.status) {
          dispatch(createIOCargoFailure(meta.message, meta.errors))

          reject(response)
          return
        }

        resolve(response)
      })
      .catch((err) => {
        dispatch(createIOCargoFailure(err.message, err.errors))
        reject(err)
      })
  })
}
