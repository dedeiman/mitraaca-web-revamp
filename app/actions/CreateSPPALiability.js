import {
  IS_PREVIEW_FORM_SPPA_LIABILITY,
  CREATE_SPPA_LIABILITY_REQUEST,
  CREATE_SPPA_LIABILITY_SUCCESS,
  CREATE_SPPA_LIABILITY_FAILURE,
  STORE_SIGNATURE_LIABILITY_REQUEST,
  STORE_SIGNATURE_LIABILITY_SUCCESS,
  STORE_SIGNATURE_LIABILITY_FAILURE,
} from 'constants/ActionTypes'
import Swal from 'sweetalert2'
import { find } from 'lodash'
import API from '../utils/API'

export const setPreviewMode = isPreviewMode => ({
  type: IS_PREVIEW_FORM_SPPA_LIABILITY,
  isPreviewMode,
})

export const createSPPALiabilityRequest = () => ({
  type: CREATE_SPPA_LIABILITY_REQUEST,
})

export const createSPPALiabilitySuccess = createSPPA => ({
  type: CREATE_SPPA_LIABILITY_SUCCESS,
  createSPPA,
})

export const createSPPALiabilityFailure = errorMessage => ({
  type: CREATE_SPPA_LIABILITY_FAILURE,
  errorMessage,
})

export const storeSignatureSPPALiabilityRequest = () => ({
  type: STORE_SIGNATURE_LIABILITY_REQUEST,
})

export const storeSignatureSPPALiabilitySuccess = createSPPA => ({
  type: STORE_SIGNATURE_LIABILITY_SUCCESS,
  createSPPA,
})

export const storeSignatureSPPALiabilityFailure = errorMessage => ({
  type: STORE_SIGNATURE_LIABILITY_FAILURE,
  errorMessage,
})

export const createSPPALiability = (formPayload, params) => (dispatch) => {
  dispatch(createSPPALiabilityRequest())
  return new Promise((resolve, reject) => {
    const url = `insurance-letters${params || ''}`
    const payload = {
      product_id: parseFloat(formPayload.product_id),
      liability: {
        sppa_type: formPayload.sppa_type,
        io_id: formPayload.io_id,
        policy_holder_id: formPayload.policy_holder_id,
        insured_id: formPayload.insured_id,
        name_on_policy: formPayload.name_on_policy,
        calculation: {
          start_period: formPayload.start_period,
          end_period: formPayload.end_period,
          discount: formPayload.discount,
          policy_book: formPayload.policy_book,
        },
        statement_insured: {
          is_legal_proceedings: formPayload.is_legal_proceedings,
          legal_proceeding_date: formPayload.legal_proceeding_date,
          legal_proceeding_description: formPayload.legal_proceeding_description,
        },
      },
    }

    return API.post(url, payload)
      .then((response) => {
        const { meta } = response.data
        if (!meta.status) {
          dispatch(createSPPALiabilityFailure(meta.message, meta.errors))
          reject(response)
          return
        }
        resolve(response)
      })
      .catch((err) => {
        dispatch(createSPPALiabilityFailure(err.message, err.errors))
        reject(err)
      })
  })
}

export const storeSignatureSPPALiability = (signatureDataUrl, sppaId) => (dispatch) => {
  dispatch(storeSignatureSPPALiabilityRequest())
  return new Promise((resolve, reject) => {
    const url = `insurance-letters/${sppaId}/signature`
    fetch(signatureDataUrl)
      .then(res => res.blob())
      .then((signatureImage) => {
        const payload = new FormData()
        payload.append('signature_file', signatureImage, 'signature.png')
        return API.put(url, payload)
          .then((response) => {
            const { meta } = response.data
            if (!meta.status) {
              dispatch(storeSignatureSPPALiabilityFailure(meta.message, meta.errors))
              reject(response)
              return
            }
            resolve(response)
          })
          .catch((err) => {
            dispatch(storeSignatureSPPALiabilityFailure(err.message, err.errors))
            reject(err)
          })
      })
  })
}

export const fetchRenewalOrIO = params => (
  // eslint-disable-next-line no-unused-vars
  dispatch => new Promise((resolve, reject) => {
    const url = `renewal-insurance-letters?sppa_type=${params.sppa_type}&product_id=${params.product_id}&document_number=${params.document_number}`
    API.get(url)
      .then(
        (response) => {
          const { data, meta } = response.data

          if (params.sppa_type === 'renewal') {
            resolve(find(data, { sppa_type: params.document_number }))
          }

          if (params.sppa_type === 'new') {
            resolve(find(data, { io_number: params.document_number }))
          }

          if (!meta.status) {
            Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Silakan kontak administrator kami</span>', 'error')
          }
        },
      )
      .catch((err) => {
        reject(err)
        Swal.fire('', '<span style="color:#2b57b7;font-weight:bold">Silakan kontak administrator kami</span>', 'error')
      })
  }))
