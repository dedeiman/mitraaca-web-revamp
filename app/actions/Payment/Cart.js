/* eslint-disable no-shadow */
import API from 'utils/API'
import config from 'app/config'

import {
  LIST_CART_REQUEST,
  LIST_CART_SUCCESS,
  LIST_CART_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

const apiUserURL = config.api_url

export const listCartRequest = () => ({
  type: LIST_CART_REQUEST,
})

export const listCartSuccess = data => ({
  type: LIST_CART_SUCCESS,
  listCart: data.data,
  metaCart: data.meta,
})

export const listCartFailure = errorMessage => ({
  type: LIST_CART_FAILURE,
  errorMessage,
})

export const listCartData = value => (
  (dispatch) => {
    dispatch(listCartRequest())
    Helper.sessionTimeout()
    const listCartData = new Promise((resolve, reject) => API.get(
      `${apiUserURL}/sppa-carts?status=${value.status ? value.status : ''}&no_sppa=${value ? value.no_sppa : ''}&cob=${value.class_bussiness ? value.class_bussiness : ''}&product=${value.type_product ? value.type_product : ''}&policy_holder=${value ? value.policy_holder : ''}&insured_name=${value ? value.insured : ''}&page=${value ? value.page : ''}`,
    ).then(
      (response) => {
        if (response.status === 200 || response.status === 201) {
          resolve({ status: true })
          dispatch(listCartSuccess(response.data))
        } else {
          resolve({
            status: false,
            message: response.data.message,
          })
          dispatch(listCartFailure(response.data.message))
        }
      },
    ).catch((err) => {
      reject(err)
    }))

    return listCartData
  }
)
