/* eslint-disable no-shadow */
import API from 'utils/API'
import config from 'app/config'

import {
  LIST_CHECKOUT_REQUEST,
  LIST_CHECKOUT_SUCCESS,
  LIST_CHECKOUT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

export const listCheckoutRequest = () => ({
  type: LIST_CHECKOUT_REQUEST,
})

export const listCheckoutSuccess = data => ({
  type: LIST_CHECKOUT_SUCCESS,
  data,
})

export const listCheckoutFailure = errorMessage => ({
  type: LIST_CHECKOUT_FAILURE,
  errorMessage,
})

export const getDraftDetailTransaction = id => (
  (dispatch) => {
    dispatch(listCheckoutRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url_payment}/transactions/${id}/draft`

    const listCartDraft = new Promise(() => API.get(url)
      .then(
        (res) => {
          if (res.status === 200 || res.status === 201) {
            dispatch(listCheckoutSuccess(res.data.data))
          } else {
            dispatch(listCheckoutFailure(res.data.message))
          }
        },
      ).catch(() => {
        dispatch(listCheckoutFailure('Cant load data.'))
      }))

    return listCartDraft
  }
)
