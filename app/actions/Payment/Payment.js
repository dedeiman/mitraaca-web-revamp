/* eslint-disable no-shadow */
import API from 'utils/API'
import config from 'app/config'
import { PAYMENT_LIST_FAILURE, PAYMENT_LIST_REQUEST, PAYMENT_LIST_SUCCESS } from '../../constants/ActionTypes'
import Helper from '../../utils/Helper'


export const listPaymentRequest = () => ({
  type: PAYMENT_LIST_REQUEST,
})

export const listPaymentSuccess = data => ({
  type: PAYMENT_LIST_SUCCESS,
  listCart: data,
})

export const listPaymentFailure = errorMessage => ({
  type: PAYMENT_LIST_FAILURE,
  errorMessage,
})

export const getPaymentMethod = () => (
  (dispatch) => {
    dispatch(listPaymentRequest())
    Helper.sessionTimeout()
    const url = `${config.api_url_payment}/payment-methods`
    return API.get(url).then((res) => {
      if (res.status === 200 || res.status === 201) {
        dispatch(listPaymentSuccess())
      } else {
        dispatch(listPaymentFailure(res.data.message))
      }
    }).catch(() => {
      dispatch(listPaymentFailure('Can\'t load data'))
    })
  }
)

// export const getDraftDetailTransaction = id => (
//   (dispatch) => {
//     dispatch(listCheckoutRequest())
//
//     return API.get(`/transactions/${id}/draft`).then((res) => {
//       if (res.status === 200 || res.status === 201) {
//         dispatch(listCheckoutSuccess(res.data.data))
//       } else {
//         dispatch(listCheckoutFailure(res.data.message))
//       }
//     }).catch((e) => {
//       dispatch(listCheckoutFailure('Cant load data.'))
//     })
//   }
// )
