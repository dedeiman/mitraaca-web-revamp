import API from 'utils/API'

import {
  CARGO_TYPE_REQUEST,
  CARGO_TYPE_SUCCESS,
  CARGO_TYPE_FAILURE,
} from 'constants/ActionTypes'

export const cargoTypeRequest = () => ({
  type: CARGO_TYPE_REQUEST,
})

export const cargoTypeSuccess = (data, meta) => ({
  type: CARGO_TYPE_SUCCESS,
  data,
  meta,
})

export const cargoTypeFailure = (errorMessage, errorObject) => ({
  type: CARGO_TYPE_FAILURE,
  errorMessage,
  errorObject,
})

export const fetchCargoType = params => (
  (dispatch) => {
    dispatch(cargoTypeRequest())
    const url = `cargo-types${params || ''}`

    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(cargoTypeSuccess(data, meta))
          } else {
            dispatch(cargoTypeFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(cargoTypeFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)
