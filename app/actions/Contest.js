import API from 'utils/API'
import config from 'app/config'
import {
  CONTEST_REQUEST,
  CONTEST_SUCCESS,
  CONTEST_FAILURE,
  CONTEST_UPDATED,
  CONTEST_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'

const xApiKey = { headers: { 'x-api-key': config.api_key } }

export const contestRequest = () => ({
  type: CONTEST_REQUEST,
})

export const contestSuccess = (data, meta) => ({
  type: CONTEST_SUCCESS,
  data,
  meta,
})

export const contestFailure = (errorMessage, errorObject) => ({
  type: CONTEST_FAILURE,
  errorMessage,
  errorObject,
})

export const contestUpdate = data => ({
  type: CONTEST_UPDATED,
  data,
})

export const contestDetail = data => ({
  type: CONTEST_DETAIL_SUCCESS,
  data,
})

export const fetchContest = params => (
  (dispatch) => {
    dispatch(contestRequest())
    const url = `${config.api_url_content}/contests${params || ''}`
    return new Promise((resolve, reject) => {
      Helper.sessionTimeout()
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(contestSuccess(data, meta))
          } else {
            dispatch(contestFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(contestFailure(err.message)) // eslint-disable-line no-console
      })
    })
  }
)

export const fetchDetailContest = id => (
  (dispatch) => {
    Helper.sessionTimeout()
    dispatch(contestRequest())
    const url = `${config.api_url_content}/contests/${id}`
    return API.get(url, xApiKey).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(contestDetail(data))
        } else {
          dispatch(contestFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(contestFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteContest = id => (
  (dispatch) => {
    dispatch(contestRequest())

    return new Promise((resolve, reject) => (
      API.delete(`${config.api_url_content}/contests/${id}`, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(contestUpdate(data))
            resolve(data)
          } else {
            dispatch(contestFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(contestFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createContest = payload => (
  (dispatch) => {
    dispatch(contestRequest())

    return new Promise((resolve, reject) => (
      API.post(`${config.api_url_content}/contests`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(contestFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(contestFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const updateContest = (payload, id) => (
  (dispatch) => {
    dispatch(contestRequest())

    return new Promise((resolve, reject) => (
      API.put(`${config.api_url_content}/contests/${id}`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(contestFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(contestFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const cancelWinner = (agentId, rewardId, uuid) => (
  () => (
    new Promise((resolve, reject) => (
      API.put(`${config.api_url_content}/contests/${uuid}/cancel-winner`, {
        agent_id: agentId,
        reward_id: rewardId,
      }, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err.message)
      })
    ))
  )
)

export const searchContest = payload => (
  () => (
    new Promise((resolve, reject) => (
      API.get(`${config.api_url_content}/contests/search/list?keyword=${payload}`, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err.message)
      })
    ))
  )
)

export const uploadContest = (payload, id) => (
  () => (
    new Promise((resolve, reject) => (
      API.post(`${config.api_url_content}/contests/${id}/upload-winners`, payload, xApiKey).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err.message)
      })
    ))
  )
)
