import Cookies from 'js-cookie'
import { isEmpty } from 'lodash'
import { purgeStoredState } from 'redux-persist'
import { mainPersistConfig } from 'store/configureStore'
import history from 'utils/history'
import config from 'app/config'
import Browser from 'utils/Browser'
import API from 'utils/API'
import {
  AUTHENTICATE_USER_REQUEST,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_FAILURE,
  UPDATE_AUTH_CURRENT_USER,
  USER_ROLE,
  SUCCESS_CHANGE_PASSWORD,
} from 'constants/ActionTypes'
import Swal from 'sweetalert2'
import { updateSiteConfiguration } from './Site'
import Helper from '../utils/Helper'


export const authenticateUserRequest = () => ({
  type: AUTHENTICATE_USER_REQUEST,
})

export const authenticateUserSuccess = data => ({
  type: AUTHENTICATE_USER_SUCCESS,
  currentUser: data,
})

export const authenticateUserFailure = errorMessage => ({
  type: AUTHENTICATE_USER_FAILURE,
  errorMessage,
})

export const updateAuthCurrentUser = currentUser => ({
  type: UPDATE_AUTH_CURRENT_USER,
  currentUser,
})

export const userRole = role => ({
  type: USER_ROLE,
  role,
})

export const successChangePassword = () => ({
  type: SUCCESS_CHANGE_PASSWORD,
})

export const removeToken = () => {
  Cookies.remove(config.auth_cookie_name, { path: '/' })
}

export const redirectToLogin = () => (
  Browser.setWindowHref('/')
)

export const getAccessToken = () => (
  Cookies.get(config.auth_cookie_name)
)

export const loginUser = (data, url) => (
  (dispatch) => {
    dispatch(authenticateUserSuccess(data))
    dispatch(userRole(data.role))

    Cookies.set(config.auth_cookie_name, data.auth_token, {
      path: '/',
      // domain: Browser.getRootDomain(),
    })

    window.localStorage.setItem('isTrueSecondary', false)
    window.localStorage.setItem('prevPath', window.location.pathname)
    history.push(url)
  }
)


export const clearCurrentUser = (url = '/') => (
  () => {
    purgeStoredState(mainPersistConfig).then(() => {
      removeToken()
      Browser.setWindowHref(url)
    })
  }
)

export const authenticateByCredentials = params => (
  (dispatch) => {
    dispatch(authenticateUserRequest())
    return API.post('/auth/login', params, { headers: { 'x-api-key': config.api_key } }).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          if (data.is_first_login === true) {
            dispatch(loginUser(data, '/profile/change#first'))
          } else {
            dispatch(loginUser(data,'/dashboard'))
          }
        } else {
          dispatch(updateAuthCurrentUser(null))
          dispatch(authenticateUserFailure(response.data.meta.message))
        }
      },
    ).catch((err) => {
      if (err.response && err.response.data) {
        dispatch(authenticateUserFailure(err.response.data.meta.message)) // eslint-disable-line no-console
      } else {
        dispatch(authenticateUserFailure(err.message)) // eslint-disable-line no-console
      }
    })
  }
)

export const authenticateByToken = () => (
  (dispatch) => {
    const accessToken = getAccessToken()

    if (isEmpty(accessToken)) {
      return redirectToLogin()
    }

    Helper.sessionTimeout()
    dispatch(authenticateUserRequest())

    return API.post(
      '/auth/authorization',
      { token: accessToken },
    ).then(
      (response) => {
        if (response.data.status) {
          dispatch(authenticateUserSuccess({ access_token: accessToken }))
        } else {
          dispatch(authenticateUserFailure())
          redirectToLogin()
        }
      },
    ).catch((err) => {
      console.error(err) // eslint-disable-line no-console
    })
  }
)

export const forgotPassword = (identifier, type) => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/reset',
        identifier,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          if (!data.is_otp_verification) {
            let successMessage = ''
            if (type !== 'nik') {
              successMessage = 'Silakan cek email Kamu'
            } else {
              switch (data.role.name) {
                case 'head-branch':
                case 'head-agency':
                case 'kepala-teknik':
                  successMessage = 'Silakan cek email Kamu'
                  break
                default:
                  successMessage = 'Anda dapat menghubugi Atasan untuk mengetahui password'
                  break
              }
            }

            Swal.fire(
              'Berhasil Mereset Password!',
              successMessage,
              'success',
            ).then(() => {
              if (data.is_otp_verification) {
                history.push('/password/otp')
              } else {
                history.push('/')
              }
            })
          } else {
            resolve(data)
          }
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const otpPassword = identifier => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        '/passwords/reset/verify-otp',
        identifier,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
          Swal.fire(
            meta.message,
            '',
            'success',
          ).then(() => {
            history.push(`/password/reset?tkn=${data.reset_password_token}`)
          })
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        reject(err)
      })
    ))
  )
)

export const checkSecondPassword = (payload, url) => (
  dispatch => (
    new Promise((resolve, reject) => (
      API.post(
        '/auth/loginsecondary',
        payload,
      ).then((response) => {
        const { meta } = response.data
        if (meta.message === 'logged in') {
          // resolve(data)
          // dispatch(successCheckSecondPasswrod())
          Swal.fire(
            'Berhasil Validasi Secondary Password!',
            '',
            'success',
          ).then(() => {
            window.localStorage.setItem('isTrueSecondary', true)
            history.push('/'+url.substring(url.lastIndexOf('/') + 1))
          })
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const resetPassword = (payload, isLoggedIn) => (
  dispatch => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/change',
        payload,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
          dispatch(successChangePassword())
          Swal.fire(
            'Berhasil Mengubah Password!',
            '',
            'success',
          ).then(() => {
            if (isLoggedIn) {
              dispatch(clearCurrentUser('/'))
            } else {
              history.push('/')
            }
          })
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)

export const verifyPassword = payload => (
  () => (
    new Promise((resolve, reject) => (
      API.post(
        'passwords/verify',
        payload,
      ).then((response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          reject(meta.message)
        }
      }).catch((err) => {
        let messageError = err.message
        if (err.response && err.response.data) {
          messageError = err.response.data.meta.message
        }
        reject(messageError)
      })
    ))
  )
)
