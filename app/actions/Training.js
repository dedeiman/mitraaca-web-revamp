import API from 'utils/API'
import {
  TRAINING_CLASS_REQUEST,
  TRAINING_CLASS_SUCCESS,
  TRAINING_CLASS_FAILURE,
  TRAINING_CLASS_UPDATED,
  TRAINING_CLASS_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import config from '../config'
import Helper from '../utils/Helper'

export const trainingRequest = () => ({
  type: TRAINING_CLASS_REQUEST,
})

export const trainingSuccess = (data, meta) => ({
  type: TRAINING_CLASS_SUCCESS,
  data,
  meta,
})

export const trainingFailure = (errorMessage, errorObject) => ({
  type: TRAINING_CLASS_FAILURE,
  errorMessage,
  errorObject,
})

export const trainingUpdate = data => ({
  type: TRAINING_CLASS_UPDATED,
  data,
})

export const trainingDetail = data => ({
  type: TRAINING_CLASS_DETAIL_SUCCESS,
  data,
})

export const fetchTraining = params => (
  (dispatch) => {
    dispatch(trainingRequest())
    Helper.sessionTimeout()
    const url = `training_classes${params || ''}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(trainingSuccess(data, meta))
        } else {
          dispatch(trainingFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(trainingFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailTraining = id => (
  (dispatch) => {
    dispatch(trainingRequest())
    Helper.sessionTimeout()
    const url = `training_classes/${id}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(trainingDetail(data))
        } else {
          dispatch(trainingFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(trainingFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteTraining = id => (
  (dispatch) => {
    dispatch(trainingRequest())

    return new Promise((resolve, reject) => (
      API.delete(`training_classes/${id}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(trainingUpdate(data))
            resolve(data)
          } else {
            dispatch(trainingFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(trainingFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createTraining = payload => (
  (dispatch) => {
    dispatch(trainingRequest())

    return new Promise((resolve, reject) => (
      API.post('training_classes', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(trainingFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(trainingFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const updateTraining = (payload, id) => (
  (dispatch) => {
    dispatch(trainingRequest())

    return new Promise((resolve, reject) => (
      API.put(`training_classes/${id}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(trainingFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(trainingFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const uploadTraining = payload => (
  () => (
    new Promise((resolve, reject) => (
      API.post(`${config.api_url}/training-classes/import`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            reject(meta.message)
          }
        },
      ).catch((err) => {
        reject(err.message)
      })
    ))
  )
)
