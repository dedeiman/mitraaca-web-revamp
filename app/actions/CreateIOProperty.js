import API from 'utils/API'
import {
  CREATE_IO_PROPERTY_REQUEST,
  CREATE_IO_PROPERTY_SUCCESS,
  CREATE_IO_PROPERTY_FAILURE,
} from 'constants/ActionTypes'

export const createIOPropertyRequest = () => ({
  type: CREATE_IO_PROPERTY_REQUEST,
})

export const createIOPropertySuccess = createIO => ({
  type: CREATE_IO_PROPERTY_SUCCESS,
  createIO,
})

export const createIOPropertyFailure = errorMessage => ({
  type: CREATE_IO_PROPERTY_FAILURE,
  errorMessage,
})

export const createIO = (formData, params) => (dispatch) => {
  dispatch(createIOPropertyRequest())

  return new Promise((resolve, reject) => {
    const url = `io${params || ''}`
    const payload = {
      product_id: Number(formData.product_id),
      asri: {
        name: formData.name,
        phone_number: formData.phone_number,
        email: formData.email,
        period_in_year: 1,
        print_policy_book: formData.print_policy_book === true,
        building_information: {
          class_construction: formData.class_construction,
          class_construction_description: formData.class_construction_description,
          building_area: Number(formData.building_area),
          // total_floor: Number(formData.total_floor),
        },
        address: {
          province_id: formData.province_id,
          city_id: formData.city_id,
          sub_district_id: formData.sub_district_id,
          village_id: formData.village_id,
          address: formData.address,
          same_address_with_insured: true,
          lat: Number(formData.lat),
          lng: Number(formData.lng),
          postal_code: formData.postal_code,
          total_floor: Number(formData.total_floor),
        },
        premi_calculation: {
          building_price: formData.building_price ? Number(formData.building_price) : 0,
          furniture_price: formData.furniture_price ? Number(formData.furniture_price) : 0,
          discount_percentage: formData.discount_percentage ? Number(formData.discount_percentage) : 0,
          discount_currency: formData.discount_currency ? Number(formData.discount_currency) : 0,
          additional_limits: formData.additional_limits || [],
        },
      },
    }

    return API.post(url, payload)
      .then((response) => {
        const { meta } = response.data

        if (!meta.status) {
          dispatch(createIOPropertyFailure(meta.message, meta.errors))

          reject(response)
          return
        }

        resolve(response)
      })
      .catch((err) => {
        dispatch(createIOPropertyFailure(err.message, err.errors))
        reject(err)
      })
  })
}
