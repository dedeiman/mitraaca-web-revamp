import API from 'utils/API'

import {
  PRODUCT_REQUEST,
  PRODUCT_SUCCESS,
  PRODUCT_DETAIL_SUCCESS,
  PRODUCT_FAILURE,
} from 'constants/ActionTypes'

export const productRequest = () => ({
  type: PRODUCT_REQUEST,
})

export const productSuccess = (data, meta) => ({
  type: PRODUCT_SUCCESS,
  data,
  meta,
})

export const productFailure = (errorMessage, errorObject) => ({
  type: PRODUCT_FAILURE,
  errorMessage,
  errorObject,
})

export const productDetail = data => ({
  type: PRODUCT_DETAIL_SUCCESS,
  data,
})

export const fetchProduct = params => (
  (dispatch) => {
    dispatch(productRequest())
    const url = `products${params || ''}`

    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(productSuccess(data, meta))
          } else {
            dispatch(productFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(productFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)

export const fetchDetailProduct = id => (
  (dispatch) => {
    dispatch(productRequest())
    const url = `products/${id}`
    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(productDetail(data))
          } else {
            dispatch(productFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(productFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)
