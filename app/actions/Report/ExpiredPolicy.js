/* eslint-disable camelcase */
import API from 'utils/API'
import {
  EXPIRED_REPORT_REQUEST,
  EXPIRED_REPORT_SUCCESS,
  EXPIRED_REPORT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

export const ExpiredRequest = () => ({
  type: EXPIRED_REPORT_REQUEST,
})

export const ExpiredSuccess = (data, meta) => ({
  type: EXPIRED_REPORT_SUCCESS,
  data,
  meta,
})

export const ExpiredFailure = error => ({
  type: EXPIRED_REPORT_FAILURE,
  error,
})

export const fetchExpired = ({
  policy_number = '',
  payment_date_from = '',
  payment_date_to = '',
  cob_id = '',
  page = 1,
  per_page = 10,
} = {}) => (
  (dispatch) => {
    dispatch(ExpiredRequest())
    Helper.sessionTimeout()
    return API.get(`agent-expired-policy-report?page=${page}&per_page=${per_page}&cob_id=${cob_id}&payment_date_from=${payment_date_from}&payment_date_to=${payment_date_to}&policy_number=${policy_number}`)
      .then((res) => {
        console.log(res)
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(ExpiredSuccess(data, meta))
        } else {
          dispatch(ExpiredFailure(meta.message))
        }
      }).catch((err) => {
        dispatch(ExpiredFailure(err.message))
      })
  }
)
