import API from 'utils/API'

export const fetchReportYearlyList = params => (
  () => new Promise((resolve, reject) => {
    return API.get(`report/insurance-letters/benefit-yearly${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

export const fetchDownloadReportYearly = params => (
  () => new Promise((resolve, reject) => {
    return API.get(`report/insurance-letters/benefit-yearly/export${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)