/* eslint-disable camelcase */
import API from 'utils/APIContent'
import config from 'app/config'
import moment from 'moment'
import {
  SPPA_REPORT_REQUEST,
  SPPA_REPORT_SUCCESS,
  SPPA_REPORT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

const apiUserURL = config.api_url

export const sppaReportRequest = () => ({
  type: SPPA_REPORT_REQUEST,
})

export const sppaReportSuccess = (data, meta) => ({
  type: SPPA_REPORT_SUCCESS,
  data,
  meta,
})

export const sppaReportFailure = error => ({
  type: SPPA_REPORT_FAILURE,
  error,
})

export const fetchSPPAReport = ({
  page = 1,
  per_page = 10,
  format = 'excel',
  from_date = moment().format('YYYY-MM-DD'),
  to_date = moment().add(7, 'day').format('YYYY-MM-DD'),
  branch_id = '',
  branch_perwakilan_id = '',
  product_id = '',
  cob_id = '',
  agent_keyword = '',
} = {}) => (
  (dispatch) => {
    dispatch(sppaReportRequest())
    Helper.sessionTimeout()

    return API.get(`${apiUserURL}/report/insurance-letters?page=${page}&per_page=${per_page}&format=${format}&from_date=${from_date}&to_date=${to_date}&agent_keyword=${agent_keyword}&branch_id=${branch_id}&branch_perwakilan_id=${branch_perwakilan_id}&product_id=${product_id}&cob_id=${cob_id}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(sppaReportSuccess(data, meta))
        } else {
          dispatch(sppaReportFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(sppaReportFailure(err.message))
    })
  }
)
