/* eslint-disable camelcase */
import API from 'utils/API'
import {
  OUTSTANDING_REPORT_REQUEST,
  OUTSTANDING_REPORT_SUCCESS,
  OUTSTANDING_REPORT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

export const CommisionRequest = () => ({
  type: OUTSTANDING_REPORT_REQUEST,
})

export const CommisionSuccess = (data, meta) => ({
  type: OUTSTANDING_REPORT_SUCCESS,
  data,
  meta,
})

export const CommisionFailure = error => ({
  type: OUTSTANDING_REPORT_FAILURE,
  error,
})

export const fetchOutstandingClaim = ({
  page = 1,
  per_page = 10,
  format = '',
  start_date = '',
  agent_keyword = '',
  branch_id = '',
  branch_perwakilan_id = '',
  product_id = '',
  policy_no = '',
  cob_id = '',
} = {}) => (
  (dispatch) => {
    dispatch(CommisionRequest())
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/outstanding-claim?page=${page}&per_page=${per_page}&cob_id=${cob_id}&format=${format}&start_date=${start_date}&start_date=${start_date}&agent_keyword=${agent_keyword}&branch_id=${branch_id}&branch_perwakilan_id=${branch_perwakilan_id}&product_id=${product_id}&policy_no=${policy_no}`)
      .then((res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(CommisionSuccess(data, meta))
        } else {
          dispatch(CommisionFailure(meta.message))
        }
      }).catch((err) => {
        dispatch(CommisionFailure(err.message))
      })
  }
)
