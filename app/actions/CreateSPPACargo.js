import API from 'utils/API'
import {
  CREATE_SPPA_CARGO_REQUEST,
  CREATE_SPPA_CARGO_SUCCESS,
  CREATE_SPPA_CARGO_FAILURE,
} from 'constants/ActionTypes'

export const createSPPACargoRequest = () => ({
  type: CREATE_SPPA_CARGO_REQUEST,
})

export const createSPPACargoSuccess = createSPPA => ({
  type: CREATE_SPPA_CARGO_SUCCESS,
  createSPPA,
})

export const createSPPACargoFailure = errorMessage => ({
  type: CREATE_SPPA_CARGO_FAILURE,
  errorMessage,
})

export const createSPPA = (formData, params) => (dispatch) => {
  dispatch(createSPPACargoRequest())

  return new Promise((resolve, reject) => {
    const url = `insurance-letters${params || ''}`
    const payload = {
      product_id: parseFloat(formData.product_id),
      cargo: {
        sppa_type: formData.sppa_type,
        io_id: formData.io_id,
        policy_holder_id: formData.policy_holder_id,
        insured_id: formData.insured_id,
        name_on_policy: formData.name_on_policy,
        qq: formData.qq,
        print_policy_book: formData.print_policy_book,
        vessel_information: {
          cargo_type_id: formData.cargo_type_id,
          vehicle_type_id: formData.vehicle_type_id,
          vehicle_id: formData.vehicle_id,
        },
        cargo_information: {
          bl_or_awb_number: formData.bl_or_awb_number,
          lc_number: formData.lc_number,
          invoice_number: formData.invoice_number,
          interest_detail: formData.interest_detail,
          interest_quantity: parseFloat(formData.interest_quantity),
          currency_id: formData.currency_id,
          sum_insured: parseFloat(formData.sum_insured),
          voyage_number: formData.voyage_number,
          est_time_departure: formData.est_time_departure,
          main_coverage: "ICC 'A'",
          other_information: formData.other_information,
          voyage_from: formData.voyage_from,
          voyage_to: formData.voyage_to,
          transhipment: formData.transhipment,
          destination_country_id: formData.destination_country_id,
          origin_port_id: formData.origin_port_id,
          destination_port_id: formData.destination_port_id,
        },
        cargo_premi_calculation: {
          rate: parseFloat(formData.rate),
          premi: parseFloat(formData.premi),
          discount_percentage: parseFloat(formData.discount_percentage),
          discount_amount: parseFloat(formData.discount_amount),
        },
      },
    }

    return API.post(url, payload)
      .then((response) => {
        const { meta } = response.data
        if (!meta.status) {
          dispatch(createSPPACargoFailure(meta.message, meta.errors))

          reject(response)
          return
        }
        resolve(response)
      })
      .catch((err) => {
        dispatch(createSPPACargoFailure(err.message, err.errors))
        reject(err)
      })
  })
}
